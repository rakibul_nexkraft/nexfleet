<?php
defined('YII_DEBUG') or define('YII_DEBUG',true);
 
// including Yii
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/console.php';
 
// we'll use a separate config file

 
// creating and running console application
require_once($yii);
Yii::createConsoleApplication($config)->run();
?>