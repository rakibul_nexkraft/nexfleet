<style>
    .top-container{
        height: 100%;
    }
    .dashboard-top .dashboard-bottom {
        padding: 0;
        margin: 0 auto 0;
    }

    .dashboard-top-container {
        max-width: 90%;
        margin: 0 auto 0 auto;
        background-color: #FFF;
        padding: 0;
        display: flex;
        align-items: stretch;
        flex-wrap: wrap;
    }

    .dashboard-item {
        width: 150px;
        height: 200px;
        background-color: #EFEFEF;
        color: #1a1a1a;
        /*line-height: 100px;*/
        text-align: center;
        font-weight: bold;
        /*font-size: 60px;*/
        /*padding: 20px;*/
        margin-bottom: 20px;
        flex: 1;
        display: flex;
    }
    .d-item{
        flex: 1;
    }
    .dashboard-item:nth-child(1){
        background-color: #fbfbfb;
    }
    .dashboard-item:nth-child(2){
        background-color: #fbe9fb;
    }
    .dashboard-item:nth-child(3){
        background-color: #daedfb;
    }
    .dashboard-item:nth-child(4){
        background-color: #d2fbec;
    }

    .dashboard-table-container{
        max-width: 90%;
        margin: -35px auto 0 auto;
        /*display: flex;*/
        /*flex-direction: column;*/
    }

    .dashboard-middle{
        max-width: 90%;
        margin: 0 auto 0 auto;
    }
    .table-item{
        flex: 1;
    }
    .form-fields-container{
        /*max-width: 90%;*/
        margin: 40px auto 0 auto;
        width: 70%;
        text-align: center;
        display: flex;
    }
    .form-field{
        /*width: 15%;*/
        flex: 1;
        text-align: center;
        vertical-align: middle;
        margin-top: 10px;
        /*padding: 20px;*/
        /*margin-bottom: 20px;*/
    }
    .margin-padding-0{
        margin: 0;padding: 0;
    }
    .top-table tr td{
        margin:0;
        padding:0;
    }
    .top-table tbody{

        border-top:0;
    }
    .top-table tr td:nth-child(1){
        font-weight: bold;
    }

    .dTableHead tr th{
        text-align: center;
        font-weight: bold;
        font-size: 1.2em;
    }



    .button {
        background-color:#F1F1F1;
        border-radius: 5px;
        color: black!important;
        padding: .5em;
        text-decoration: none;
        font-weight: bold;
        text-shadow: none;
    }

    .button:focus,
    .button:hover {
        background-color: #F1F1F1;
    }

    /* Table Header Fixed Body Scroll Fix*/
    table.scroll {
        width: 100%!important;
        border-spacing: 0!important;
        border: 1px solid #DEDEDE!important;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }

    table.scroll th,
    table.scroll td,
    table.scroll tr,
    table.scroll thead,
    table.scroll tbody { display: block!important; }

    table.scroll thead tr {
        /* fallback */
        width: 97%!important;
        /* minus scroll bar width */
        width: -webkit-calc(100% - 20px)!important;
        width:    -moz-calc(100% - 20px)!important;
        width:         calc(100% - 20px)!important;
    }

    table.scroll tr:after {
        content: ' '!important;
        display: block!important;
        visibility: hidden!important;
        clear: both!important;
    }

    table.scroll tbody {
        height: 250px!important;
        overflow-y: auto!important;
        overflow-x: hidden!important;
    }

    table.scroll tbody td,
    table.scroll thead th {
        height: 30px!important;
        line-height: 30px!important;
    }

    table.scroll tbody td,
    table.scroll thead th {
        width: 7%!important;
        float: left!important;
        border-right: 1px solid #DEDEDE!important;
        border-bottom: 1px solid #DEDEDE!important;
    }

    table.scroll tbody td:first-child,
    table.scroll thead th:first-child {
        width: 14%!important;
    }
    table.scroll tbody td:nth-child(2),
    table.scroll thead th:nth-child(2) {
        width: 9%!important;
    }

    thead tr th {
        height: 30px!important;
        line-height: 30px!important;
    }

    tbody { border-top: 2px solid #DEDEDE; }

    tbody td:last-child, thead th:last-child {
        border-right: none !important;
    }

    /* select 2 button */
    .select2-container .select2-choice{
        background-image: none!important;
        border-radius: 3px;
        border: 1px solid #CCCCCC;
    }
    .select2-default {
        color: #555 !important;
    }
    .select2-container .select2-choice .select2-arrow{
        border-left:0;
        background: #FFFFFF;
        background-image: none;
    }
    .select2-dropdown-open .select2-choice {
        background: #FFF;
    }
    .disabled{
        pointer-events: none;
    }

    td a:active, a:focus {
        outline: 0;
        border: none;
        -moz-outline-style: none
    }

    .vehicleSearchTableHeader th,
    .vehicleSearchTableBody td{
        padding:4px 0 4px 0;
    }

</style>
<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile('http://www.google.com/jsapi');
$cs->registerCoreScript('jquery');
//$cs->registerScriptFile($baseUrl.'/js/jquery.gvChart-1.0.1.min.js');
//$cs->registerScriptFile($baseUrl.'/js/pbs.init.js');
$cs->registerCssFile($baseUrl.'/css/jquery.css');

?>

<?php $this->pageTitle=Yii::app()->name; ?>
<div class=clearfix></div>
<h4>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i> Dashboard</h4>

<div class="top-container">
    <div class="dashboard-top" >
        <div class="dashboard-top-container">
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Available</h4>
                    <div id="topAvailable" style="font-size: 90px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px;padding: 10px;" id="availableVehicle">
                        <table class='top-table'>
                            <tr><td>Bus</td><td> : </td><td>0</td></tr>
                            <tr><td>Car</td><td> : </td><td>0</td></tr>
                            <tr><td>Jeep</td><td> : </td><td>0</td></tr>
                            <tr><td>Microbus</td><td> : </td><td>0</td></tr>
                            <tr><td>Pickup/Delivery Van</td><td> : </td><td>0</td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">On-Duty</h4>
                    <div id="topOnDuty" style="font-size: 90px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px;padding: 10px;" id="ondutyVehicle">
                        <table class='top-table'>
                            <tr><td>Bus</td><td> : </td><td>0</td></tr>
                            <tr><td>Car</td><td> : </td><td>0</td></tr>
                            <tr><td>Jeep</td><td> : </td><td>0</td></tr>
                            <tr><td>Microbus</td><td> : </td><td>0</td></tr>
                            <tr><td>Pickup/Delivery Van</td><td> : </td><td>0</td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Workshop</h4>
                    <div id="topWorkshop" style="font-size: 90px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px;padding: 10px;" id="workshopVehicle">
                        <table class='top-table'>
                            <tr><td>Bus</td><td> : </td><td>0</td></tr>
                            <tr><td>Car</td><td> : </td><td>0</td></tr>
                            <tr><td>Jeep</td><td> : </td><td>0</td></tr>
                            <tr><td>Microbus</td><td> : </td><td>0</td></tr>
                            <tr><td>Pickup/Delivery Van</td><td> : </td><td>0</td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Staff Pick/Drop</h4>
                    <div id="topStaffPickDrop" style="font-size: 90px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px;padding: 10px;" id="pickdropVehicle">
                        <table class='top-table'>
                            <tr><td>Bus</td><td> : </td><td>0</td></tr>
                            <tr><td>Car</td><td> : </td><td>0</td></tr>
                            <tr><td>Jeep</td><td> : </td><td>0</td></tr>
                            <tr><td>Microbus</td><td> : </td><td>0</td></tr>
                            <tr><td>Pickup/Delivery Van</td><td> : </td><td>0</td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-bottom" >
        <div class="dashboard-table-container">
            <div class="table-item">
                <div class="search-form" style="padding-left: 50px;border-radius: 3px;background-color: #FBFBFB;">
                    <form id="yw0" action="#" method="get" style="margin-bottom: 0">
                        <div class="form-fields-container">

                            <div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <?php
                                        $this->renderPartial('vehicleList');
                                        ?>
<!--                                        <button type="button" class="btn btn-default btn-mini" onclick="getVehicleList()">-->
<!--                                            <i class="icon-refresh"></i>-->
<!--                                        </button>-->
                                    </div>
                                </div>
                            </div>

                            <!--<div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <?php
/*                                        $vehicleList = array();

                                        $this->widget('ext.ESelect2.ESelect2',array(
                                            'name' => 'vehicleRegNo',
                                            'attribute'=>'vehicleRegNo',
                                            'htmlOptions'=>array(
                                                'style'=>'width:200px',
                                            ),
                                            'options' => array(
                                                'allowClear'=>true,
                                                'placeholder'=>'Select Vehicle Reg No',
//                                                'minimumInputLength' => 1,
                                                'class' => 'js-example-basic-single js-states form-control'
                                            ),
                                            'data' => $vehicleList
                                        ));
                                        */?>
                                        <button type="button" class="btn btn-default btn-mini" onclick="getVehicleList()">
                                            <i class="icon-refresh"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <select class="control-label" name="searchType" id="searchType" onchange="getVehicleList()" >
                                            <option value="available">Available</option>
                                            <option value="onduty" selected="selected">On-duty</option>
                                            <option value="workshop">Workshop</option>
                                            <option value="staffpickdrop">Staff Pick/Drop</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                            'name'=>'searchDate',
                                            'id'=>'searchDate',
                                            'value' => '',
                                            'options'=>array('autoSize'=>true,
                                                'dateFormat'=>'yy-mm-dd',
                                                'changeYear'=>true,
                                                'changeMonth'=>true,
                                            ),

                                            'htmlOptions'=>array('style'=>'width:206px;height:20px;border-radius:2px;','placeholder'=>'Select Date'
                                            ),
                                        )); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <?php
                                        $vehicleTypes = array('Bus'=>'Bus','Car'=>'Car','Jeep'=>'Jeep','Microbus'=>'Microbus','Pickup/Delivery Van'=>'Pickup/Delivery Van');

                                        $this->widget('ext.ESelect2.ESelect2',array(
                                            'name' => 'vehicleType',
                                            'attribute'=>'vehicleType',
                                            'htmlOptions'=>array(
                                                'style'=>'width:200px',
                                            ),
                                            'options' => array(
                                                'allowClear'=>true,
                                                'placeholder'=>'Select Vehicle Type',
//                                                'minimumInputLength' => 1,
                                                'class' => 'js-example-basic-single js-states form-control'
                                            ),
                                            'data' => $vehicleTypes
                                        ));
                                        ?>
<!--                                        <select class="control-label" name="vehicleType" id="vehicleType" >-->
<!--                                            <option value="">Select Vehicle Type</option>-->
<!--                                            <option value="Bus">Bus</option>-->
<!--                                            <option value="Car">Car</option>-->
<!--                                            <option value="Jeep">Jeep</option>-->
<!--                                            <option value="Microbus">Microbus</option>-->
<!--                                            <option value="Pickup/Delivery Van">Pickup/Delivery Van</option>-->
<!--                                        </select>-->
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="control-group">
                                    <button onclick="searchVehicle()" name="searchButton" id="searchButton" type="button" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-middle">
        <div class="table-item">
            <div style="width: 100%;margin-top: -29px;">

                <div class="grid-view" style="" id="vehicleSearchTable">
                    <table class="scroll">
                        <thead class="vehicleSearchTableHeader">
                        <tr>
                            <th style='text-align: center'>Vehicle Reg No</th>
                            <th style='text-align: center'>09AM</th>
                            <th style='text-align: center'>10AM</th>
                            <th style='text-align: center'>11AM</th>
                            <th style='text-align: center'>12PM</th>
                            <th style='text-align: center'>13PM</th>
                            <th style='text-align: center'>14PM</th>
                            <th style='text-align: center'>15PM</th>
                            <th style='text-align: center'>16PM</th>
                            <th style='text-align: center'>17PM</th>
                            <th style='text-align: center'>18PM</th>
                            <th style='text-align: center'>Type</th>
                        </tr>
                        </thead>
                        <tbody class="vehicleSearchTableBody">
                        <tr><td colspan=12>Loading Data...</td></tr>
                        </tbody>
                    </table>
                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <h4 style="border-bottom: 0px" id="myModalLabel">Requisition No - </h4>
                    </div>
                    <div class="modal-body">
                        <div id="requisitionData">
                            <table class='table table-hover'>
                                <tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Driver Phone : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>User Pin : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>User Name : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Dept Name : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>User Cell : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Start Point : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>End Point : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Time : </th><td style='width: 60%'></td></tr>
                            </table>
                        </div>
                        <!--        --><?php //$this->renderPartial('requisition_detail_modal',array('model'=>$model));?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
                <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-header">
                        <h4 style="border-bottom: 0px" id="myModalLabel2">Defect No - </h4>
                    </div>
                    <div class="modal-body">
                        <div id="defectData">
                            <table class="table table-hover">
                                <tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Vehicle Type : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Apply Date : </th><td style='width: 60%'> </td></tr>
                            </table>
                        </div>
                        <!--        --><?php //$this->renderPartial('requisition_detail_modal',array('model'=>$model));?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <input type="hidden" id="currentDate" value="--><?php //$date = new DateTime("now", new DateTimeZone('Asia/Dhaka'));echo $date->format('Y-m-d');?><!--" />-->
    <input type="hidden" id="currentDate" value="<?php echo date('Y-m-d')?>" />

</div>

<script>
    var execute = false;
    var searchVehicle = function (inputData) {
        $('.vehicleSearchTableBody').fadeTo(500,.5);
        var data;
        var currentDate = $("#currentDate").val();
        if(inputData){
            data = inputData;
        }else {
            var searchType = $("select#searchType").val();
            var searchDate = $("#searchDate").val();
            var vehicleRegNo = $("#vehicleRegNo").val();
            var vehicleType = $('#vehicleType').val();
            data = {
                searchType: searchType,
                searchDate: searchDate,
                vehicleRegNo: vehicleRegNo,
                vehicleType: vehicleType
            };
        }
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("site/searchByCriteria");?>',
            data:data,
            success: function(html){
                $('#vehicleSearchTable').html(html);
                $('select#searchType').val(data.searchType);
                $('input#searchDate').val(data.searchDate);
                $('#vehicleRegNo').val(data.vehicleRegNo);
                $('#vehicleType').val(data.vehicleType);
                $('.vehicleSearchTableBody').fadeTo(500,1);
            },
            complete: function (data) {
                if (!execute){
                    getVehicleList();
                    execute = true;
                }
            }
        });
    };
    searchVehicle({searchType: 'onduty', searchDate: $("#currentDate").val(), vehicleRegNo: $('#vehicleRegNo').val(), vehicleType: $('#vehicleType').val()});
    setInterval(function () {
        searchVehicle({searchType: $('select#searchType').val(), searchDate: $("#searchDate").val(), vehicleRegNo: $('#vehicleRegNo').val(), vehicleType: $('#vehicleType').val()})
    }, 300000);


    var getVehicleList = function () {
        $('select#vehicleRegNo').val("");
        $('div#s2id_vehicleRegNo a.select2-choice span.select2-chosen').text("Loading Vehicle List...");
        $('div#s2id_vehicleRegNo').removeClass("select2-allowclear");
        $('div#s2id_vehicleRegNo a.select2-choice').addClass("disabled");

        $('select#vehicleType').val("");
        $('div#s2id_vehicleType a.select2-choice span.select2-chosen').text("Select Vehicle Type");
        $('div#s2id_vehicleType').removeClass("select2-allowclear");

        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("site/getVehicleList");?>',
            data:{
                searchType: $('select#searchType').val(),
                searchDate: $('input#searchDate').val()
            },
            success: function (html) {
                $('#vehicleRegNo').html(html);
                $('div#s2id_vehicleRegNo a.select2-choice span.select2-chosen').text("Select Vehicle Reg No");
                $('div#s2id_vehicleRegNo a.select2-choice').removeClass("disabled");
            }
        });
    };

    var getVehicleCountTable = function(data) {
        var item = {};
        var html = "";
        var count = 0;
        for (i = 0; i < data.length; i++) {
            count = count + parseInt(data[i].countNo);
        }
        $.each(data, function (key, value) {
            if (value.type != "Pickup/Delivery Van" && value.type != "Refrigerator Van") {
                item[value.type] = value.countNo;
            } else {
                if (value.type === "Pickup/Delivery Van") {
                    item["PickupDeliveryVan"] = value.countNo;
                } else {
                    item["RefrigeratorVan"] = value.countNo;
                }
            }
        });
        html +=     "<table class='top-table'>" +
            "<tr><td>Bus</td><td> : </td><td>" + (item.Bus===undefined?0:item.Bus) + "</td></tr>" +
            "<tr><td>Car</td><td> : </td><td>" + (item.Car===undefined?0:item.Car) + "</td></tr>" +
            "<tr><td>Jeep</td><td> : </td><td>" + (item.Jeep===undefined?0:item.Jeep) + "</td></tr>" +
            "<tr><td>Microbus</td><td> : </td><td>" + (item.Microbus===undefined?0:item.Microbus) + "</td></tr>" +
            "<tr><td>Pickup/Delivery Van</td><td> : </td><td>" + (item.PickupDeliveryVan===undefined?0:item.PickupDeliveryVan) + "</td></tr>" +
            "</table>";
        return {html:html, count:count};
    };
    var showAvailableVehicle = function(){
        var item = {};
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataAvailable");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topAvailable').text(data.count);
                $('div#availableVehicle').html(data.html);
            },
            complete: function (data) {
                setTimeout(showAvailableVehicle, 300000);
            }

        });
    };
    showAvailableVehicle();
    setTimeout(showAvailableVehicle, 300000);

    var showOnDutyVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataOnDuty");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topOnDuty').text(data.count);
                $('div#ondutyVehicle').html(data.html);
            }
        });
    };
    showOnDutyVehicle();
    setInterval(showOnDutyVehicle, 300000);

    var showWorkshopVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataWorkshop");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topWorkshop').text(data.count);
                $('div#workshopVehicle').html(data.html);
            }
        });
    };
    showWorkshopVehicle();
    setInterval(showWorkshopVehicle, 300000);

    var showStaffPickDropVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataStaffPickDrop");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topStaffPickDrop').text(data.count);
                $('div#pickdropVehicle').html(data.html);
            }
        });
    };
    showStaffPickDropVehicle();
    setInterval(showStaffPickDropVehicle, 300000);

    function getRequisitionDetail(id){
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("site/getRequisitionDetail");?>',
            data:{id:id},
            dataType:'json',
            success: function(data){
                var html = "";
                if (data){
                    $('#myModalLabel').html("<h4 style='border-bottom: 0px'>Requisition No - "+data.id+"</h4>");
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'>" + data.vehicle_reg_no + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'>" + data.driver_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'>" + data.driver_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Phone : </th><td style='width: 60%'>" + data.driver_phone + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Pin : </th><td style='width: 60%'>" + data.user_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Name : </th><td style='width: 60%'>" + data.user_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Dept Name : </th><td style='width: 60%'>" + data.dept_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Cell : </th><td style='width: 60%'>" + data.user_cell + "</td></tr>" +
                        "<tr><th style='width: 40%'>Start Point : </th><td style='width: 60%'>" + data.start_point + "</td></tr>" +
                        "<tr><th style='width: 40%'>End Point : </th><td style='width: 60%'>" + data.end_point + "</td></tr>" +
                        "<tr><th style='width: 40%'>Time : </th><td style='width: 60%'>From <strong>" + data.start_date +" "+ data.start_time + "</strong> To <strong>" + data.end_date+" "+data.end_time + "</strong></td></tr>" +
                        "</table>";
                    $('#requisitionData').html(html);
                } else {
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><td>No data found</td></tr>" +
                        "</table>";
                    $('#requisitionData').html(html);
                }
            }
        });
    }
    function getDefectDetail(id){
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("site/getDefectDetail");?>',
            data:{id:id},
            dataType:'json',
            success: function(data){
                var html = "";
                if (data){
                    $('#myModalLabel2').html("<h4 style='border-bottom: 0px'>Defect No - "+data.id+"</h4>");
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'>" + data.vehicle_reg_no + "</td></tr>" +
                        "<tr><th style='width: 40%'>Vehicle Type : </th><td style='width: 60%'>" + data.type + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver PIN : </th><td style='width: 60%'>" + data.driver_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'>" + data.driver_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Apply Date : </th><td style='width: 60%'>" + data.apply_date + "</td></tr>" +
                        "</table>";
                    $('#defectData').html(html);
                } else {
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><td>No data found</td></tr>" +
                        "</table>";
                    $('#defectData').html(html);
                }
            }
        });
    }

</script>