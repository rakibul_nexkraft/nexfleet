<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />


    <!-- Stylesheets
     ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/workshop-assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/workshop-assets/css/customStyle.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/workshop-assets/css/demoStyle.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<?php echo Yii::app()->baseUrl; ?>/common/workshop-assets/css/main.css" rel="stylesheet" />
    <!--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">-->
    <!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

    <!-- External JavaScripts
     ============================================= -->
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/plugins.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>


    <!-- Document Title
     ============================================= -->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style>
        .margin-rl-auto {
            margin: 0 auto !important;
        }

        .sidepanel-close-btn {
            display: block !important;
            position: absolute;
            z-index: 12;
            top: 0;
            left: auto;
            right: 0;
            width: 40px;
            height: 40px;
            font-size: 18px;
            line-height: 40px;
            text-align: center;
            background-color: rgba(0, 0, 0, 0.1);
            border-radius: 0 0 0 2px;
            color: white;
        }

        .nav-tree a {
            padding-left: 20px !important;
        }

        .nav-tree li {
            margin-bottom: 10px;
        }

        .nav-tree li.active {
            border: 1px solid #1ABC9C !important;
            border-radius: 3px;
        }

        .right-margin {
            margin-right: 20px;
        }

        .body-overlay_custom {
            opacity: 0;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgb(0, 0, 0);
            z-index: 1100;
            pointer-events: none;
            -webkit-backface-visibility: hidden;
            -webkit-transition: opacity .2s ease;
            -o-transition: opacity .2s ease;
            transition: opacity .2s ease;
        }

        .spin-icon {
            -webkit-animation: spin 1.5s linear infinite;
            animation: spin 1.5s linear infinite;
        }
    </style>

</head>

<body class="stretched side-panel-left">

    <div class="body-overlay"></div>
    <div class="body-overlay_custom">
        <div style="display: flex;height: 100%;align-items: center;justify-content: center;">
            <i class="fa fa-cog spin-icon" style="color: #ff008a;font-size: 20px;line-height: 20px;"></i>
        </div>
    </div>

    <div id="side-panel" class="dark">

        <div id="side-panel-trigger-close" class="side-panel-trigger"><a class="sidepanel-close-btn" href="#"><i class="icon-line-cross"></i></a></div>

        <div class="side-panel-wrap">

            <div class="widget clearfix">

                <h4 style="margin-left: 20px">Pages</h4>

                <nav class="nav-tree nobottommargin">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label' => '<i class="icon-home"></i>Home', 'url' => array('site/test'), 'linkOptions' => array(), 'visible' => !Yii::app()->user->isGuest,),
                            array('label' => '<i class="icon-wallet"></i>Pick &amp; Drop', 'url' => array('site/index'), 'visible' => !Yii::app()->user->isGuest),
                            array('label' => '<i class="icon-briefcase"></i>Marketplace', 'url' => substr(Yii::app()->baseUrl, 0, strpos(Yii::app()->baseUrl, "workshop")) . 'marketplace/index.php', 'visible' => !Yii::app()->user->isGuest),
                        ),
                        'encodeLabel' => false,
                        'htmlOptions' => array('class' => ''),
                        'activeCssClass' => 'active',
                    ));
                    ?>
                </nav>

            </div>

        </div>

    </div>

    <!-- Document Wrapper
     ============================================= -->
    <div id="wrapper" class="clearfix margin-rl-auto">

        <style>
            .landing-form-overlay {
                position: relative
            }
            .profile-sidebar {
                padding: 20px 0 10px 0;
                background: #fff;
            }
            .profile-section-container {
                /*background: #394d61;*/
                background: #fff;
                color: #555;
            }
            .profile-img-box-flex {
                padding: 10px 0;
            }
            .profile-img-box {
                height: 150px;
                width: 150px;
                display: flex;
                margin: 0 auto;
            }
            .profile-img-box img {
                -webkit-border-radius: 50% !important;
                -moz-border-radius: 50% !important;
                border-radius: 50% !important;
            }
            .profile-usertitle {
                /*text-align: center;*/
            }
            .profile-usertitle-name {
                color: #0b3e7e;
                font-size: 18px;
                font-weight: 600;
                margin-bottom: 7px;
                position: relative;
                padding: 6px 0;
            }
            .profile-usertitle-name a {
                color: #555 !important;
            }
            .profile-usertitle-name a:hover {
                color: #4b81e1;
            }
            .profile-usertitle-job {
                text-transform: uppercase;
                color: #555;
                font-size: 12px;
                font-weight: 600;
                margin-bottom: 15px;
            }
            .profile-usertitle .table>tbody>tr:first-child>th,
            .profile-usertitle .table>tbody>tr:first-child>td {
                background: #fff1fa;
            }

            .profile-usertitle .table>tbody>tr:nth-child(2)>th,
            .profile-usertitle .table>tbody>tr:nth-child(2)>td {
                background: #fff1fa;
            }
            .profile-usertitle .table>tbody>tr:nth-child(3)>th,
            .profile-usertitle .table>tbody>tr:nth-child(3)>td {
                background: #fff1fa;
            }
            .profile-usertitle .table>tbody>tr:nth-child(4)>th,
            .profile-usertitle .table>tbody>tr:nth-child(4)>td {
                background: #fff1fa;
            }
            .table>thead>tr>th,
            .table>tbody>tr>th,
            .table>tfoot>tr>th,
            .table>thead>tr>td,
            .table>tbody>tr>td,
            .table>tfoot>tr>td {
                padding: 12px 12px;
                border: 0;
            }
            .profile-usertitle-name a#profile-edit-btn span {
                display: none;
                position: absolute;
                right: -16px;
                top: 0;
                margin-right: 20px;
            }
            .profile-usertitle-name:hover a#profile-edit-btn span {
                display: block;
            }
            .cart-product-price img {
                width: 28px;
                margin: -6px auto;
            }
            .driver-info-row img {
                width: 40%;
            }
            @media (max-width: 991px) {
                #side-panel-trigger {right: 16px;}
                #logo {text-align: left;}
            }
            .txt {
                position: relative;
                float: left;
                font-family: 'Raleway', sans-serif;
                font-size: 36px;
                line-height: 100%;
                margin-right: 40px;
            }
            .icon-zone {
                position: relative;
                top: -2px;
                margin-right: 6px;
                text-align: center;
                width: 14px;
            }
            .profile-img-square {
                padding: 4px;
                background: #fff1fa0f;
                box-shadow: 0 0 6px rgb(0, 0, 0, .6);
            }
            .profile-img-square img {
                border-radius: 2px;
                width: 100%;
            }
            /* Footer style */
            @media screen and (min-width: 768px) {
                .txt-right {text-align: right;}
                .txt-left {text-align: left;}
            }

            @media screen and (max-width: 767px) {
                .txt-right {text-align: center;}
                .txt-left {text-align: center;}
            }
        </style>
        
        <!-- Header
          ============================================= -->
        <header id="header" class="full-header static-sticky">

            <div id="header-wrap">

                <div class="container clearfix">

                    <!--<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>-->

                    <!-- Logo
                        ============================================= -->
                    <div id="logo" style="display: flex;align-items: center;">
                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?php echo Yii::app()->baseUrl; ?>/common/assets/images/brac-logo-big.png" alt="BRAC Logo"></a>
                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?php echo Yii::app()->baseUrl; ?>/common/assets/images/ifleet-logo-2.png" alt="BRAC Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Logout and user info
                        ============================================= -->
                    <div id="top-search" class="top-search">
                        <?php if (Yii::app()->user->name != 'Guest') { ?>
                            <span class="right-margin">
                                <?php echo Yii::app()->user->firstname . " " . Yii::app()->user->lastname; ?>
                            </span>
                            <span class="right-margin">|</span>
                            <?php if (isset(Yii::app()->user->department) && (Yii::app()->user->department)) { ?>
                                <span class="right-margin">
                                    <?php echo Yii::app()->user->department; ?>
                                </span>
                                <span class="right-margin">|</span>
                            <?php } ?>
                            <?php if (isset(Yii::app()->user->designation) && (Yii::app()->user->designation)) { ?>
                                <span class="right-margin">
                                    <?php echo Yii::app()->user->designation; ?>
                                </span>
                                <span class="right-margin">|</span>
                            <?php }?>
                        <?php
                            echo '<span class="right-margin">' . CHtml::link('<i class="icon-lock2"></i>', array('/user/logout')) . '</span>';
                        }
                        ?>

                    </div><!-- #logout end -->
                </div>
            </div>
        </header><!-- #header end -->

        <div class="clearfix"></div>

        <!-- Content
          ============================================= -->

            <?php 
             $user_rights = Yii::app()->user->getUserRights();
            ?>
        <?php if (Yii::app()->user->name != 'Guest') : ?>
            <section id="content">

                <div class="container-fluid clearfix" style="background: #f8f8f8;padding: 0">
                    <div id="side-navigation">
                        <div class="col_one_fifth left-side-bar">
                            <div class="">
                                <div class="profile-userpic profile-img-box-flex">
                                    <div class="profile-img-square">
                                        <?php
                                        // $image_url = substr(Yii::app()->baseUrl, 0, strpos(Yii::app()->baseUrl, "workshop")) . "photo/" . Yii::app()->user->username . ".png";
                                        $image_url = Yii::app()->baseUrl . "/photo/" . Yii::app()->user->username . ".png";
                                        if (!file_exists("../photo/" . Yii::app()->user->username . ".png")) {
                                            $image_url = Yii::app()->baseUrl . "/photo/" . "person-sample.png";
                                        }
                                        ?>
                                        <img src="<?php echo $image_url; ?>" style="" class="img-responsive" alt="User image">
                                    </div>
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                        <a href="<?php echo Yii::app()->createUrl("/user/profile"); ?>"><?php echo Yii::app()->user->firstname . " " . Yii::app()->user->lastname; ?></a>
                                        <a id="profile-edit-btn" href="<?php echo Yii::app()->createUrl("/user/profile/edit"); ?>"><span><i class="icon-edit"></i></span></a>
                                    </div>
                                    <table class="table table-responsive margin-top admin-info-responsive" style="border: 0;">
                                        <tr>
                                            <th class="profile-usertitle-job">Phone</th>
                                            <td><?php echo Yii::app()->user->phone; ?></td>
                                        </tr>
                                        <tr>
                                            <th class="profile-usertitle-job">Pin</th>
                                            <td><?php echo Yii::app()->user->username; ?></td>
                                        </tr>

                                    </table>
                                </div>
                            </div>

                            <div class="">
                                <div class="menubox">
                                    <?php $this->widget('zii.widgets.CMenu', array(
                                        'items' => array(
                                            array('label' => '<i class="icon-screen"></i> Home', 'url' => array('/workshop'), 'visible' => !Yii::app()->user->isGuest),
                                            array('label' => '<i class="icon-screen"></i> iFleet', 'url' => array('/'), 'visible' => !Yii::app()->user->isGuest,),
                                            array('label' => '<i class="icon-screen"></i> Setup <i class="icon-chevron-right"></i>', 'url' => "",
                                                'itemOptions' => array('class' => 'cursor-pointer'),
                                                'items' => array(
                                                    array('label' => '<i class="icon-user"></i> Role Management', 'url' => array('/role/admin'), 'visible' => !Yii::app()->user->isGuest && in_array('Role Management', $user_rights)),
                                                    array('label' => '<i class="icon-user"></i> Rights Management', 'url' => array('/rights/admin'), 'visible' => !Yii::app()->user->isGuest && in_array('Rights Management', $user_rights)),
                                                    array('label' => '<i class="icon-user"></i>Workflow', 'url' => array("/workflow/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Workflow', $user_rights)),
                                                )
                                            ),
                                            array('label' => '<i class="icon-user2"></i> User Management', 'url' => array("/user/workshopAdmin"), 'visible' => !Yii::app()->user->isGuest && in_array('User Management', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            /*array('label' => '<i class="icon-screen"></i> User Management <i class="icon-chevron-right"></i>', 'url' => "", 'visible' => !Yii::app()->user->isGuest && in_array('User Management', $user_rights),
                                                'itemOptions' => array('class' => 'cursor-pointer'),
                                                'items' => array(
                                                    array('label' => '<i class="icon-user"></i> User List', 'url' => array('/user/workshopAdmin'), 'visible' => !Yii::app()->user->isGuest && in_array('User List', $user_rights)),
                                                    array('label' => '<i class="icon-user"></i> Role Management', 'url' => array('/role/admin'), 'visible' => !Yii::app()->user->isGuest && in_array('Role Management', $user_rights)),
                                                    array('label' => '<i class="icon-user"></i> Rights Management', 'url' => array('/rights/admin'), 'visible' => !Yii::app()->user->isGuest && in_array('Rights Management', $user_rights)),
                                                )
                                            ),*/
                                            //array('label'=>'Vehicles', 'url'=>array('/vehicles/index'),'visible'=>!Yii::app()->user->isGuest),
                                            array('label' => '<i class="icon-user2"></i> Manufacturer', 'url' => array("/manufacturer/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Manufacturer', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Timesheet', 'url' => array("/timesheet/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Timesheet', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Supplier', 'url' => array("/supplier/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Supplier', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            /*array('label' => '<i class="icon-user2"></i> Product', 'url' => array("/product/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Product Type', 'url' => array("/productType/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Type', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),*/
                                            array('label' => '<i class="icon-user2"></i> Product Grouping', 'url' => array("/productGrouping/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Grouping', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Product Classification', 'url' => array("/productClassification/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Classification', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Product Attributes', 'url' => array("/productAttributes/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Attributes', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Mechanics', 'url' => array("/mechanic/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Mechanics', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Inventory', 'url' => array("/inventory/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Grouping', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Inventory History', 'url' => array("/inventoryHistory/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Grouping', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Invoice', 'url' => array("/invoice/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Grouping', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            array('label' => '<i class="icon-user2"></i> Invoice Line', 'url' => array("/invoiceLine/admin"), 'visible' => !Yii::app()->user->isGuest && in_array('Product Grouping', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),
                                            /*array('label' => '<i class="icon-user2"></i> Workflow', 'url' => array("/workflow/index"), 'visible' => !Yii::app()->user->isGuest && in_array('Workflow', $user_rights), 'itemOptions' => array('class' => 'cursor-pointer')),*/
                                           /* array(
                                                'label' => '<i class="fa fa-truck "></i> Vehicles<i class="icon-chevron-right"></i>', 'url' => "",
                                                'itemOptions' => array('class' => 'cursor-pointer'),
                                                'items' => array(
                                                    array('label' => '<i class="fa fa-truck "></i> Vehicles', 'url' => array('/vehicles/index'), 'visible' => !Yii::app()->user->isGuest),
                                                    array('label' => '<i class="fa fa-truck "></i> Vehicle Others', 'url' => array('/vehiclesisters/index'), 'visible' => !Yii::app()->user->isGuest),
                                                ), 'visible' => !Yii::app()->user->isGuest
                                            ),
                                            array('label' => '<i class="fa fa-truck "></i> Accidents', 'url' => array('/accidents/index'), 'visible' => !Yii::app()->user->isGuest),
                                            array('label' => '<i class="fa fa-truck "></i> Auctions', 'url' => array('/Auctions/index'), 'visible' => !Yii::app()->user->isGuest),
                                            //     array('label'=>'Reports', 'url'=>array('/site/page', 'view'=>'reports'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest,),
                                            array('label' => '<i class="fa fa-truck "></i> CNG Retests', 'url' => array('/cngretests/index'), 'visible' => !Yii::app()->user->isGuest,),

                                            array('label' => '<i class="fa fa-truck "></i> Defects', 'url' => array('/defects/index'), 'visible' => !Yii::app()->user->isGuest),

                                            array(
                                                'label' => '<i class="fa fa-user user-icon-style"></i> Inventory<i class="icon-chevron-right"></i>', 'url' => "",
                                                'itemOptions' => array('class' => 'cursor-pointer'),
                                                'items' => array(
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Spare/Material', 'url' => array('/wsitemnames/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Stock IN', 'url' => array('/wsitems/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Stock Position', 'url' => array('/wsstocks/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Misc Expenses', 'url' => array('/wsmsexpenses/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Battery Status', 'url' => array('/wsbatteries/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Tyre Status', 'url' => array('/wstyres/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> IOU', 'url' => array('/wsious/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Snacks Bill', 'url' => array('/wssnacks/index')),


                                                ), 'visible' => !Yii::app()->user->isGuest
                                            ),

                                            array(
                                                'label' => '<i class="fa fa-user user-icon-style"></i> Services<i class="icon-chevron-right"></i>', 'url' => "",
                                                'itemOptions' => array('class' => 'cursor-pointer'),
                                                'items' => array(
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Service List- IN', 'url' => array('/wsservices/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Service List- OUT', 'url' => array('/wsoutservices/index')),
                                                ), 'visible' => !Yii::app()->user->isGuest
                                            ),

                                            array(
                                                'label' => '<i class="fa fa-user user-icon-style"></i> Workshop Util<i class="icon-chevron-right"></i>', 'url' => "",
                                                'itemOptions' => array('class' => 'cursor-pointer'),
                                                'items' => array(
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Delivery Status', 'url' => array('/deliveryvehicles/index')),
                                                    //   array('label'=>'All Task', 'url'=>array('/Tasks/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Suppliers', 'url' => array('/wssupplier/index')),
                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Mechanics', 'url' => array('/mechanics/index')),

                                                    array('label' => '<i class="fa fa-user user-icon-style"></i> Vehicle Types', 'url' => array('/vehicletypes/index')),

                                                ), 'visible' => !Yii::app()->user->isGuest
                                            ),
                                            array('label' => '<i class="fa fa-file-excel"></i> Reports', 'url' => array('/site/page', 'view' => 'reports_ws'), 'itemOptions' => array('class' => 'icon_chart'), 'visible' => !Yii::app()->user->isGuest), */
                                        ),

                                        'encodeLabel' => false,
                                        'htmlOptions' => array('class' => 'left-url'),
                                        'submenuHtmlOptions' => array(
                                            'class' => 'cursor-pointer',
                                        ),
                                        'activeCssClass' => 'ui-tabs-active',

                                    )); ?>
                                </div>
                                <div class="menubox">

                                </div>
                            </div>
                        </div>

                        <div class="col_four_fifth col_last middle_margin page-container">
                            <div class="topmargin-sm">
                                <div id="snav-content">
                                    <?php echo $content; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- #content end -->
        <?php else : ?>
            <?php echo $content; ?>
        <?php endif; ?>
    </div><!-- #wrapper end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark notopborder" style="background: url('<?php echo Yii::app()->baseUrl; ?>/common/assets/images/footer-bg.jpg') repeat fixed; background-size: 100% 100%;">
        <div style="height: 10px;padding-bottom: 37px !important;">
            <div class="container-fluid">
                <!-- Footer Widgets
                    ============================================= -->
                <div class="footer-widgets-wrap clearfix" style="padding: 0">
                    <div class="row clearfix" id="footerSection" style="margin-top: 0;padding-top: 0;">
                        <div class="col-md-7 col-sm-6 txt-left">
                            <div class="widget clearfix">
                                <div class="clear-bottommargin-sm">
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <div class="copyrights-menu copyright-links clearfix" style="margin-top: 6px">
                                                Copyrights © 2019 All Rights Reserved by iFleet.</div>
                                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                        </div>
                        <div class="col-md-5 col-sm-6 txt-right">
                            <div class="copyrights-menu copyright-links nobottommargin" style="margin-top: 6px">
                                Made with
                                <svg width="14" height="14" viewBox="0 -2 24 24">
                                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" fill="red"></path>
                                </svg>
                                by<a class="link" href="http://www.nexkraft.com" style="color: white !important;">NexKraft Limited</a>
                            </div>
                        </div>
                    </div>
                </div><!-- .footer-widgets-wrap end -->
            </div>
        </div>
    </footer>

    <!-- Go To Top
     ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="width: 715px; margin: 95px auto;">
            <div class="modal-content" style="width: 850px;">
                <!-- <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Create Form</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <div class="modal-body" id="modal-body">

                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
                    <button type="button" onclick="bsModalClose()" class="btn btn-primary">Create</button>
                </div>
                            </div> -->
        </div>

    </div>

    <!-- Footer Scripts
     ============================================= -->
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/functions.js"></script>
    <!--    --><?php //echo Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/functions.js', CClientScript::POS_END); 
                ?>
    <script>
        $(".left-url>li").click(function() {
            $(this).children("ul").toggle("slow");

        });
        $(".left-url>li").focusout(function() {
            $(this).children("ul").slideUp();
        });

        /*$(".left-url>li" ).mouseenter(function() {
        	 $(this).children("ul").slideDown("slow");

        });
        $( ".left-url>li" ).mouseleave(function() {
        	$(this).children("ul").slideUp();

        });*/
        //function hideMenu(){
        //$( ".col_one_fifth" ).slideToggle( "slow" );
        //}

        function bsModalOpen(data, largeModal = false, type = null) {
            $('#modal-body').html(data);
            if (largeModal) {
                $('#statusModal').find('.modal-dialog').addClass('modal-lg');
            }
            $('#statusModal').modal({
                keyboard: false,
                backdrop: "static"
            });
        }

        function bsModalClose() {
            location.reload(true);
            $('#statusModal').modal("hide");
            setTimeout(function() {
                $('#modal-body').html("...");
                console.log("Cleared");
                $('#statusModal').find('.modal-dialog').removeClass('modal-lg');
            }, 500);
        }

        function showDataProcess(isProcessing) {
            if (isProcessing) {
                $('.body-overlay_custom').css({
                    "pointer-events": "all",
                    "opacity": .75
                });
            } else {
                $('.body-overlay_custom').css({
                    "pointer-events": "none",
                    "opacity": 0
                });
            }
        }
    </script>
</body>
</html>

