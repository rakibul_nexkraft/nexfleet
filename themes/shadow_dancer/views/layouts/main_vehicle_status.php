<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />


    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/select2.min.css" type="text/css">

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/main.css" rel="stylesheet" />
    <!--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">-->
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/css/navigation.css">

    <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl; ?>/common/assets/css/toastr.css" type="text/css" />

    <!-- External JavaScripts
    ============================================= -->
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/toastr.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/echarts.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/common/assets/js/select2.full.min.js"></script>



    <!-- Document Title
    ============================================= -->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style>
        .margin-rl-auto{
            margin: 0 auto !important;
        }

        .revo-slider-emphasis-text {
            font-size: 58px;
            font-weight: 700;
            letter-spacing: 1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }

        .tp-video-play-button { display: none !important; }

        .tp-caption { white-space: nowrap; }
    </style>

</head>

<body class="stretched">

<!-- Content
        ============================================= -->
<?php echo $content; ?><!-- #content end -->

<!-- Footer Style -->
<style>
    @media screen and (min-width: 768px) {
        .txt-right{
            text-align: right;
        }
        .txt-left{
            text-align: left;
        }
    }
    @media screen and (max-width: 767px) {
        .txt-right{
            text-align: center;
        }
        .txt-left{
            text-align: center;
        }
    }
</style>
<!-- Footer -->
<footer id="footer" class="dark notopborder" style="background: url('<?php echo Yii::app()->baseUrl; ?>/common/assets/images/footer-bg.jpg') repeat fixed; background-size: 100% 100%;">
    <div id="copyrights">
        <div class="container">
            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix" style="padding: 0">
                <div class="row clearfix" id="footerSection">
                    <div class="col-md-7 col-sm-6 txt-left">
                        <div class="widget clearfix">
                            <div class="clear-bottommargin-sm">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="copyrights-menu copyright-links clearfix nomargin">
                                            Copyrights © 2019 All Rights Reserved by iFleet.</div>
                                        <div class="visible-sm visible-xs bottommargin-sm"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="visible-sm visible-xs bottommargin-sm"></div>
                    </div>
                    <div class="col-md-5 col-sm-6 txt-right">
                        <div class="copyrights-menu copyright-links nobottommargin">
                            Made with
                            <svg width="14" height="14" viewBox="0 -2 24 24">
                                <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" fill="red"></path>
                            </svg>
                            by <a class="link" href="http://www.nexkraft.com">NexKraft Limited</a>
                        </div>
                    </div>
                </div>
            </div><!-- .footer-widgets-wrap end -->
        </div>
    </div>
    <!-- Copyrights
    ============================================= -->
    <!-- #copyrights end -->

</footer>

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>


<!-- Footer Scripts
============================================= -->


<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<!--<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>-->

<!--<script type="text/javascript" src="< ?php echo Yii::app()->baseUrl; ?>/common/assets/js/functions.js"></script>-->

</body>
</html>