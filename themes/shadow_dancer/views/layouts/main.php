<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/buttons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/icons.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tables.css" />

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/mbmenu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/mbmenu_iestyles.css" />

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/jquery.jscrollpane.css" />


<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<style type="text/css">
  /* The Modal (background) */
  .modalup {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: -94px;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    overflow: hidden;
  }

  .modalAlert {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1200; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0px;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
  }
  /* Modal Content */
  .modal-contentup,.modal-contentAlert {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    overflow: hidden;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s

  }
  .modal-contentup{ width: 50%;}
  /* Add Animation */
  @-webkit-keyframes animatetop {
    from {top:-300px; opacity:0} 
    to {top:0; opacity:1}
  }

  @keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
  }

  /* The Close Button */
  .close,.closeAlert {
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }
  .close {
    color: black;
    float: right;
    font-size: 28px;
    font-weight: bold;
  }
  .close:hover,
  .close:focus,.closeAlert:hover,.closeAlert:focus,#closeModelup:hover {
    color: red;
    text-decoration: none;
    cursor: pointer;
  }

  .modal-headerup,.modal-headerAlert {
    padding: 2px 16px;
    background-color: #FBB034;
    color: black;
    text-align: center;
  }

  .modal-bodyup,.modal-bodyAlert {padding: 2px 16px;}

  .modal-footerup,.modal-footerAlert {
    padding: 2px 16px;
    background-color: #FBB034;
    color: black;
  }
  
  #myModalup h6{line-height: 12px;}
  
  
  .containerForm{float:left;margin: 5px;overflow: hidden;}   
</style>

<script type="text/javascript">

  /*<![CDATA[*/
  jQuery(document).ready(function() {
    jQuery('.close').click(function(){ 
             //alert('ok');              
             jQuery('#myModalup').hide();

           });
    jQuery('#closeModelup').click(function(){ 
             //alert('ok');              
             jQuery('#myModalup').hide();

           });
    jQuery('.closeAlert').click(function(){ 
             //alert('ok');              
             jQuery('#myModalAlert').hide();

           });
    jQuery('#feadback_car').click(function(){                    
      var feadback_rating=jQuery("input[name=ratingCar]:checked").val();
      if(feadback_rating==5 || feadback_rating<=1 ){
       ratingPopup('car_rating_comment');            
     }

   });
    jQuery('#feadback_driver').click(function(){                    
      var feadback_rating=jQuery("input[name=ratingDriver]:checked").val();
      if(feadback_rating==5 || feadback_rating<=1 ){
        ratingPopup('driver_rating_comment');                  
      }

    });
    jQuery('#feadback_ifleet').click(function(){                    
      var feadback_rating=jQuery("input[name=ratingIfleet]:checked").val();
      if(feadback_rating==5 || feadback_rating<=1 ){
        ratingPopup('ifleet_rating_comment'); 
      }

    });
  });
  /*]]>*/
  function ratingPopup(id){
    var ratingPopup=jQuery('#ratingPopup').val();
    jQuery('.modalAlert').show();
    jQuery('#modalHeaderAlert').html('Notice');
    jQuery('#modalBodyAlert').html(ratingPopup);
    jQuery('#modalBodyAlert').css('text-align','center');
    jQuery('.closeAlert').click(function(){                  
      $("#"+id).focus();
    });   
  }
</script>
</head>

<body>
  <?php
  $user_inf=User::model()->findByAttributes(
    array('username'=>Yii::app()->user->name));
  //$criteria = new CDbCriteria();
  //$criteria->condition  = "user_pin=:upin";
  //$criteria->params = array(':upin'=>'01');
  //$criteria->order = "id DESC";
  //$criteria->limit = 1;
  //$user_last_requisition=Requisitions::model()->find($criteria);    
  //$requsetion_id=$user_last_requisition['id'];
  
  $feedback_rating=FeedbackRating::model()->findByPk($requsetion_id);
  
  if(isset($_GET['feedback'])&& $_GET['feedback']=='1'){
    $FeedbackSettings=FeedbackSettings::model()->findByPk(1);
    ?>
    <!-- Start Rating  -->
    <style>
      .modalup {display:block;}
    </style>
    <div id="myModalup" class="modalup">

      <!-- Modal content -->
      <div class="modal-contentup">
        <div class="modal-headerup">
          <span class="close">&times;</span>
          <h2><?php echo $FeedbackSettings['heading'];?></h2>
        </div>
        <div class="modal-bodyup">    
          <div class="row" style="margin-left: 20px">
            <div class="col-sm-12">
              <div class="col-sm-2" style="width:58%;float: left;">
                <h6>User Name: Md.Masud Kobir</h6>
                <h6>Driver Name: Md.Rofiqul Haq</h6>
                <h6>Driver Pin: 15827</h6>
                <h6>Route: Gulshan1-Gulshan2</h6>
                <h6>Date: <?php echo date('Y-m-d');?></h6>
              </div>
              <div class="col-sm-10" style="float:right;">
                <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/download.jpg','Driver Image',array('width'=>'100px')); ?>
              </div>
            </div>
          </div>
          <div class="row" style='margin-top:20px;margin-left: 20px'>
            <p><?php echo $FeedbackSettings['user_title'];?></p>
            <input type="hidden" id="ratingPopup" value="<?php echo $FeedbackSettings['rating_popup'];?>">
            <?php 

            $this->widget('CStarRating',
              array('name'=>'ratingCar',
                'id'=>'feadback_car',
                "ratingStepSize" => 0.5,
                "maxRating"=>"5",
                "minRating"=>'0.5',
                "starCount"=>"5",
                
                
                ));?> (<?php echo $FeedbackSettings['user_rating_title'];?>)</br></br>
              </div>
              <div class="row" style="margin-left: 20px">
                <?php echo CHtml::textArea('answer' . $no, $exam->answer, array(
                  'rows' => 3, 'cols' => 50, 'class' => "form-control",'id'=>'car_rating_comment', 'style'=>'width:99%;')
                );?>
              </div>
              <div class="row" style="margin-left: 20px">
                <p><?php echo $FeedbackSettings['driver_title'];?></p>
                <?php 
                $this->widget('CStarRating',
                  array('name'=>'ratingDriver',
                    'id'=>'feadback_driver',
                    "ratingStepSize" => 0.5,
                    "maxRating"=>"5",
                    "minRating"=>'0.5',
                    "starCount"=>"5",
                    ));?>(<?php echo $FeedbackSettings['driver_rating_title'];?>)</br></br>
                  </div>
                  <div class="row" style="margin-left: 20px">
                    <?php $no=2;echo CHtml::textArea('answer' . $no, $exam->answer, array(
                      'rows' => 3, 'cols' => 50, 'class' => "form-control",'id'=>'driver_rating_comment', 'style'=>'width: 99%')
                    );?>
                  </div>
                  <div class="row" style="margin-left: 20px">
                    <p><?php echo $FeedbackSettings['ifleet_title'];?></p>
                    <?php 
                    $this->widget('CStarRating',
                      array('name'=>'ratingIfleet',
                       'id'=>'feadback_ifleet',
                       "ratingStepSize" => 0.5,
                       "maxRating"=>"5",
                       "minRating"=>'0.5',
                       "starCount"=>"5",
                       ));?>(<?php echo $FeedbackSettings['ifleet_rating_title'];?>)</br></br>
                     </div>
                     <div class="row" style="margin-left: 20px">
                      <?php echo CHtml::textArea('answer' . $no, $exam->answer, array(
                        'rows' => 3, 'cols' => 50, 'class' => "form-control", 'id'=>'ifleet_rating_comment','style'=>'width: 99%')
                      );?>
                    </div>
                    

                  </div>
                  <div class="modal-footerup">
                    <h3></h3>
                    <div class="row" style="margin-left: 20px">
                      <?php  echo CHtml::button('Submit',array('style'=>'padding:5px;margin-bottom:5px'));
                      echo CHtml::button('Close',array('id'=>'closeModelup','style'=>'padding:5px;margin-bottom:5px;float:right'));
                      ?>
                    </div>
                  </div>
                </div>

              </div>




              <!-- END Rating  -->

              <?php 
            }
  //if()
            ?>

            <div id="myModalAlert" class="modalAlert">
              <!-- Modal content -->
              <div class="modal-contentAlert">
                <div class="modal-headerAlert">
                  <span class="closeAlert">&times;</span>
                  <h4 id="modalHeaderAlert"></h4>
                </div>
                <div class="modal-bodyAlert" id="modalBodyAlert">
                </div>
                <div class="modal-footerAlert">
                  <h3 id="modalFooterAlert">  </h3>
                </div>
              </div>

            </div>

            <div class="container" id="page">
             <?php 
             if($this->layout!='column_pop'):
               ?>
               <div id="topnav">
                <!--div class="topnav_text"><a href='#'>Home</a> | <a href=''>My Account</a> | <a href='#'>Settings</a> | <a href='/user/logout'>Logout</a> </div-->
                
                <?php
                $this->widget('zii.widgets.CMenu',array(
                  'items'=>array(
                    array('label'=>'Home', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest,),
                    array('label'=>'My Account', 'url'=>array('/user/profile'),'visible'=>!Yii::app()->user->isGuest,),
                    array('label'=>'Logout('.Yii::app()->user->name.')', 'url'=>array('/user/logout'),'visible'=>!Yii::app()->user->isGuest,),
                  ),
                ));
                ?>
                
              </div>
              <div id="header">
                <div id="logo" style="float:left;"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
                <!--<div style="float:right;color:#eee;font-size:26px;padding-right:20px;padding-top:15px;color:#fbb034;">iFleet - <span style=color:#13b5ea;>integrated</span> Fleet</div>-->
                <div style="float:right;color:#eee;font-size:26px;padding-right:20px;padding-top:15px;color:#13b5ea;letter-spacing:1px;font-weight:normal;">iFleet <span style=color:#fbb034;>[integrated Fleet] </span></div>
                <div class=clearfix></div>
              </div><!-- header -->
    <!--
<?php /*$this->widget('application.extensions.mbmenu.MbMenu',array(
            'items'=>array(
                array('label'=>'Dashboard', 'url'=>array('/site/index'),'itemOptions'=>array('class'=>'test')),
                array('label'=>'Theme Pages',
                  'items'=>array(
                    array('label'=>'Graphs & Charts', 'url'=>array('/site/page', 'view'=>'graphs'),'itemOptions'=>array('class'=>'icon_chart')),
					array('label'=>'Form Elements', 'url'=>array('/site/page', 'view'=>'forms')),
					array('label'=>'Interface Elements', 'url'=>array('/site/page', 'view'=>'interface')),
					array('label'=>'Error Pages', 'url'=>array('/site/page', 'view'=>'Demo 404 page')),
					array('label'=>'Calendar', 'url'=>array('/site/page', 'view'=>'calendar')),
					array('label'=>'Buttons & Icons', 'url'=>array('/site/page', 'view'=>'buttons_and_icons')),
                  ),
                ),
                array('label'=>'Gii Generated Module',
                  'items'=>array(
                    array('label'=>'Items', 'url'=>array('/theme/index')),
                    array('label'=>'Create Item', 'url'=>array('/theme/create')),
					array('label'=>'Manage Items', 'url'=>array('/theme/admin')),
                  ),
                ),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
            ),
            )); */?> --->
            <div id="mainmenu">
              <?php  if (Yii::app()->user->isAdmin() == "" && isset(Yii::app()->user->username) && !empty(Yii::app()->user->username)) {
                if(Yii::app()->user->isViewUser()){
                 $this->widget('zii.widgets.CMenu',array(
                  'items'=>array(
                    array('label'=>'Dashboard', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest,),
                    array('label'=>'Requisitions', 'url'=>array('/requisitions/index'),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>'Log Book', 'url'=>array('/movements/index'),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>'Pick & Drop', 'url'=>array('#'), 'items'=>array(
                      array('label'=>'Seat Allocation', 'url'=>array('/commonfleets/index'),'visible'=>!Yii::app()->user->isGuest),
                      array('label'=>'Routes', 'url'=>array('/routes/index')),
                      array('label'=>'Strike And Block', 'url'=>array('/pds/admin')),
                      
                    ),'visible'=>!Yii::app()->user->isGuest),

                    array('label'=>'Vehicles', 'url'=>array('#'), 'items'=>array(

                      array('label'=>'Vehicles', 'url'=>array('/vehicles/index'),'visible'=>!Yii::app()->user->isGuest),
                      array('label'=>'Vehicle Others', 'url'=>array('/vehiclesisters/index'),'visible'=>!Yii::app()->user->isGuest),
                      
                      array('label'=>'Daily Vehicle Check', 'url'=>array('/dailyVehicleCheck/index'),'visible'=>!Yii::app()->user->isGuest),
                    ),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>'Fuels', 'url'=>array('/fuels/index'),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>'Renewals', 'url'=>array('/renewals/index'),'visible'=>!Yii::app()->user->isGuest),
//array('label'=>'Graphs', 'url'=>array('/site/page', 'view'=>'graphs'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest,),
                    
                    array('label'=>'Crash Report', 'url'=>array('/accidents/index'),'visible'=>!Yii::app()->user->isGuest),
                    array('label'=>'Auctions', 'url'=>array('/Auctions/index'),'visible'=>!Yii::app()->user->isGuest),

                    
                    array('label'=>'Reports', 'url'=>array('/site/page', 'view'=>'reports'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest,),    
                    array('label'=>'CNG Retests', 'url'=>array('/cngretests/index'),'visible'=>!Yii::app()->user->isGuest,),    
                    
                    array('label'=>'Utilities', 'url'=>array('#'), 'items'=>array(
                      array('label'=>'Vehicle Types', 'url'=>array('/vehicletypes/index')),
                      array('label'=>'Fuel Stations', 'url'=>array('/fuelstations/index')),
                      array('label'=>'Duty Types', 'url'=>array('/dutytypes/index')),
                      array('label'=>'Bill Rates', 'url'=>array('/billrates/index')),     
                      array('label'=>'Departments', 'url'=>array('/departments/index')),
                      array('label'=>'Costs', 'url'=>array('/costs/index')),
                      array('label'=>'Dept Costs', 'url'=>array('/deptcosts/index')),
                      array('label'=>'Car Distributions', 'url'=>array('/cardistributions/index')),
                      
                      array('label'=>'Driver Time Settings', 'url'=>array('/timesettings/index')),
                      array('label'=>'Setup Holidays', 'url'=>array('/holidays/index')),
                      array('label'=>'TA-DA-Office Time Settings', 'url'=>array('/taDaTime/index')),
                    ),'visible'=>!Yii::app()->user->isGuest),
                    
                    array('label'=>'Workshop', 'url'=>array('#'), 'items'=>array(
                      array('label'=>'Defects', 'url'=>array('/defects/index')),
                      array('label'=>'Stock- IN', 'url'=>array('/wsitems/index')),
                      array('label'=>'Stocks', 'url'=>array('/wsstocks/index')),
                      array('label'=>'Misc Expenses', 'url'=>array('/wsmsexpenses/index')),
                      array('label'=>'Battery Status', 'url'=>array('/wsbatteries/index')),
                      array('label'=>'Tyre Status', 'url'=>array('/wstyres/index')),
                      array('label'=>'IOU', 'url'=>array('/wsious/index')),
                      array('label'=>'Snacks Bill', 'url'=>array('/wssnacks/index')),
                      array('label'=>'Item Distribution', 'url'=>array('/wsitemdists/index')),
            //array('label'=>'Requisitions', 'url'=>array('/wsrequisitions/index')),

                      array('label'=>'Delivery Status', 'url'=>array('/deliveryvehicles/index')),

                      array('label'=>'Field Repair', 'url'=>array('/fieldrepair/index')),

                      
                    ),'visible'=>!Yii::app()->user->isGuest),

                    array('label'=>'Workshop Util', 'url'=>array('#'), 'items'=>array(
                      array('label'=>'Items Name', 'url'=>array('/wsitemnames/index')),
                      array('label'=>'Service List- IN', 'url'=>array('/wsservices/index')),
                      array('label'=>'Service List- OUT', 'url'=>array('/wsoutservices/index')),
                      array('label'=>'Mechanics', 'url'=>array('/mechanics/index')),
                 //   array('label'=>'All Task', 'url'=>array('/Tasks/index')),
                      array('label'=>'Suppliers', 'url'=>array('/wssupplier/index')),
                    ),'visible'=>!Yii::app()->user->isGuest),

                    array('label'=>'Feedback', 'url'=>array('#'), 'items'=>array(
                    //array('label'=>'Rating', 'url'=>array('','feedback'=>'1')),
                      array('label'=>'Drivers Profile', 'url'=>array('/drivers/index')),
                      array('label'=>'Drivers Training', 'url'=>array('/driverTraining/index')),
                      array('label'=>'Miscellaneous Incidence', 'url'=>array('/miscellaneousIncidence/index')),
                      array('label'=>'Rating Report','url'=>array('FeedbackRating/index')),
                    )),
                  ),
));

}      
elseif(Yii::app()->user->username=='185036'){
 $this->widget('zii.widgets.CMenu',array(
   'items'=>array(
     array('label'=>'Defects', 'url'=>array('/defects/index'),'visible'=>!Yii::app()->user->isGuest),
     array('label'=>'Inventory', 'url'=>array('#'), 'items'=>array(
       
       array('label'=>'Stock IN', 'url'=>array('/wsitems/index'))),'visible'=>!Yii::app()->user->isGuest)                   
   )));
}
else{
 $this->widget('zii.widgets.CMenu',array(
   'items'=>array(
     array('label'=>'Dashboard', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest,),
			                  //array('label'=>'Vehicles', 'url'=>array('/vehicles/index'),'visible'=>!Yii::app()->user->isGuest),
     array('label'=>'Vehicles', 'url'=>array('#'), 'items'=>array(
       array('label'=>'Vehicles', 'url'=>array('/vehicles/index'),'visible'=>!Yii::app()->user->isGuest),
       array('label'=>'Vehicle Others', 'url'=>array('/vehiclesisters/index'),'visible'=>!Yii::app()->user->isGuest),
     ),'visible'=>!Yii::app()->user->isGuest),
     array('label'=>'Accidents', 'url'=>array('/accidents/index'),'visible'=>!Yii::app()->user->isGuest),
     array('label'=>'Auctions', 'url'=>array('/Auctions/index'),'visible'=>!Yii::app()->user->isGuest),
			               //     array('label'=>'Reports', 'url'=>array('/site/page', 'view'=>'reports'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest,),
     array('label'=>'CNG Retests', 'url'=>array('/cngretests/index'),'visible'=>!Yii::app()->user->isGuest,),

     array('label'=>'Defects', 'url'=>array('/defects/index'),'visible'=>!Yii::app()->user->isGuest),

     array('label'=>'Inventory', 'url'=>array('#'), 'items'=>array(
       array('label'=>'Spare/Material', 'url'=>array('/wsitemnames/index')),
       array('label'=>'Stock IN', 'url'=>array('/wsitems/index')),
       array('label'=>'Stock Position', 'url'=>array('/wsstocks/index')),
       array('label'=>'Misc Expenses', 'url'=>array('/wsmsexpenses/index')),
       array('label'=>'Battery Status', 'url'=>array('/wsbatteries/index')),
       array('label'=>'Tyre Status', 'url'=>array('/wstyres/index')),
       array('label'=>'IOU', 'url'=>array('/wsious/index')),
       array('label'=>'Snacks Bill', 'url'=>array('/wssnacks/index')),


     ),'visible'=>!Yii::app()->user->isGuest),

     array('label'=>'Services', 'url'=>array('#'), 'items'=>array(
       array('label'=>'Service List- IN', 'url'=>array('/wsservices/index')),
       array('label'=>'Service List- OUT', 'url'=>array('/wsoutservices/index')),
     ),'visible'=>!Yii::app()->user->isGuest),

     array('label'=>'Workshop Util', 'url'=>array('#'), 'items'=>array(
       array('label'=>'Delivery Status', 'url'=>array('/deliveryvehicles/index')),
			                     //   array('label'=>'All Task', 'url'=>array('/Tasks/index')),
       array('label'=>'Suppliers', 'url'=>array('/wssupplier/index')),
       array('label'=>'Mechanics', 'url'=>array('/mechanics/index')),

       array('label'=>'Vehicle Types', 'url'=>array('/vehicletypes/index')),

     ),'visible'=>!Yii::app()->user->isGuest),
     array('label'=>'Reports', 'url'=>array('/site/page', 'view'=>'reports_ws'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest),

   ),
 ));
}
}
?>

<?php
if (Yii::app()->user->isAdmin() == '1'){
  $this->widget('zii.widgets.CMenu',array(
   'items'=>array(
    array('label'=>'Dashboard', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest,),
    array('label'=>'Requisitions', 'url'=>array('/requisitions/index'),'visible'=>!Yii::app()->user->isGuest),
    array('label'=>'Log Book', 'url'=>array('/movements/index'),'visible'=>!Yii::app()->user->isGuest),
    array('label'=>'Pick & Drop', 'url'=>array('#'), 'items'=>array(
      array('label'=>'Seat Allocation', 'url'=>array('/commonfleets/index'),'visible'=>!Yii::app()->user->isGuest),
      array('label'=>'Routes', 'url'=>array('/routes/index')),
      array('label'=>'Strike And Block', 'url'=>array('/pds/admin')),
      array('label'=>'Pick ADMIN', 'url'=>Yii::app()->request->baseUrl.'/pickndrop/index.php/user/login?pin='.Yii::app()->user->name.'&fleet=1','visible'=>!Yii::app()->user->isGuest,),
    ),'visible'=>!Yii::app()->user->isGuest),

    array('label'=>'Vehicles', 'url'=>array('#'), 'items'=>array(

      array('label'=>'Vehicles', 'url'=>array('/vehicles/index'),'visible'=>!Yii::app()->user->isGuest),
      array('label'=>'Vehicle Others', 'url'=>array('/vehiclesisters/index'),'visible'=>!Yii::app()->user->isGuest),
      
      array('label'=>'Daily Vehicle Check', 'url'=>array('/dailyVehicleCheck/index'),'visible'=>!Yii::app()->user->isGuest),
    ),'visible'=>!Yii::app()->user->isGuest),
    array('label'=>'Fuels', 'url'=>array('/fuels/index'),'visible'=>!Yii::app()->user->isGuest),
    array('label'=>'Renewals', 'url'=>array('/renewals/index'),'visible'=>!Yii::app()->user->isGuest),
//array('label'=>'Graphs', 'url'=>array('/site/page', 'view'=>'graphs'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest,),
    
    array('label'=>'Crash Report', 'url'=>array('/accidents/index'),'visible'=>!Yii::app()->user->isGuest),
    array('label'=>'Auctions', 'url'=>array('/Auctions/index'),'visible'=>!Yii::app()->user->isGuest),

    
    array('label'=>'Reports', 'url'=>array('/site/page', 'view'=>'reports'),'itemOptions'=>array('class'=>'icon_chart'),'visible'=>!Yii::app()->user->isGuest,),		
    array('label'=>'CNG Retests', 'url'=>array('/cngretests/index'),'visible'=>!Yii::app()->user->isGuest,),		
    
    array('label'=>'Utilities', 'url'=>array('#'), 'items'=>array(
     array('label'=>'Vehicle Types', 'url'=>array('/vehicletypes/index')),
     array('label'=>'Fuel Stations', 'url'=>array('/fuelstations/index')),
     array('label'=>'Duty Types', 'url'=>array('/dutytypes/index')),
     array('label'=>'Bill Rates', 'url'=>array('/billrates/index')),			
     array('label'=>'Departments', 'url'=>array('/departments/index')),
     array('label'=>'Costs', 'url'=>array('/costs/index')),
     array('label'=>'Dept Costs', 'url'=>array('/deptcosts/index')),
     array('label'=>'Car Distributions', 'url'=>array('/cardistributions/index')),
     
     array('label'=>'Driver Time Settings', 'url'=>array('/timesettings/index')),
     array('label'=>'Setup Holidays', 'url'=>array('/holidays/index')),
     array('label'=>'TA-DA-Office Time Settings', 'url'=>array('/taDaTime/index')),
   ),'visible'=>!Yii::app()->user->isGuest),
    
    array('label'=>'Workshop', 'url'=>array('#'), 'items'=>array(
      array('label'=>'Defects', 'url'=>array('/defects/index')),
      array('label'=>'Defects Request', 'url'=>array('/DefectRequestAdmin/index')),
      array('label'=>'Stock- IN', 'url'=>array('/wsitems/index')),
      array('label'=>'Stocks', 'url'=>array('/wsstocks/index')),
      array('label'=>'Misc Expenses', 'url'=>array('/wsmsexpenses/index')),
      array('label'=>'Battery Status', 'url'=>array('/wsbatteries/index')),
      array('label'=>'Tyre Status', 'url'=>array('/wstyres/index')),
      array('label'=>'IOU', 'url'=>array('/wsious/index')),
      array('label'=>'Snacks Bill', 'url'=>array('/wssnacks/index')),
      array('label'=>'Item Distribution', 'url'=>array('/wsitemdists/index')),
            //array('label'=>'Requisitions', 'url'=>array('/wsrequisitions/index')),

      array('label'=>'Delivery Status', 'url'=>array('/deliveryvehicles/index')),

      array('label'=>'Field Repair', 'url'=>array('/fieldrepair/index')),

      
    ),'visible'=>!Yii::app()->user->isGuest),

    array('label'=>'Workshop Util', 'url'=>array('#'), 'items'=>array(
      array('label'=>'Items Name', 'url'=>array('/wsitemnames/index')),
      array('label'=>'Service List- IN', 'url'=>array('/wsservices/index')),
      array('label'=>'Service List- OUT', 'url'=>array('/wsoutservices/index')),
      array('label'=>'Mechanics', 'url'=>array('/mechanics/index')),
                 //   array('label'=>'All Task', 'url'=>array('/Tasks/index')),
      array('label'=>'Suppliers', 'url'=>array('/wssupplier/index')),
    ),'visible'=>!Yii::app()->user->isGuest),

    array('label'=>'Feedback', 'url'=>array('#'), 'items'=>array(
                    //array('label'=>'Rating', 'url'=>array('','feedback'=>'1')),
      array('label'=>'Drivers Profile', 'url'=>array('/drivers/index')),
      array('label'=>'Drivers Training', 'url'=>array('/driverTraining/index')),
      array('label'=>'Drivers SMS', 'url'=>array('/emailSms/index')),
      array('label'=>'Miscellaneous Incidence', 'url'=>array('/miscellaneousIncidence/index')),
      array('label'=>'Setting','url'=>array('FeedbackSettings/update','id'=>1)),
      array('label'=>'Report','url'=>array('FeedbackRating/index')),
      array('label'=>'User Rating','url'=>array('userRating/index')),
      array('label'=>'Digital Log History','url'=>array('digitalLogBookHistory/index')),

                    //array('label'=>'User Digital Log History','url'=>array('userdigitalLogBookHistory/index')),

      array('label'=>'Vehicle Place','url'=>array('vehiclePlaceHistory/index')),
      array('label'=>'Digital Log Book','url'=>array('/digitalLogBook/admin')),
      array('label'=>'DLB Remove, Submit ','url'=>array('/digitalLogBook/remove')),
      array('label'=>'DLB Vehicle User ','url'=>array('/manageVehicleUser/index')),
      array('label'=>'Notification','url'=>array('notification/index')),
      array('label'=>'New Dashboard', 'url'=>array('requisitions/dashboard'),'visible'=>!Yii::app()->user->isGuest,), 
      array('label'=>'Error Log', 'url'=>array('errorLog/index'),'visible'=>!Yii::app()->user->isGuest,),
      array('label'=>'39 Vehicle Status', 'url'=>array('/vehiclestatus/index'),'visible'=>!Yii::app()->user->isGuest,)


    )),

    array('label'=>'Workshop Management', 'url'=>array('/workshop')),
  ),
));

} ?>

</div> <!--mainmenu -->

<?php 
endif;
if(isset($this->breadcrumbs)):?>
  <?php $this->widget('zii.widgets.CBreadcrumbs', array(
   'links'=>$this->breadcrumbs,
   )); ?><!-- breadcrumbs -->
 <?php endif?>
 <div class=clearfix></div>

 <?php echo $content; ?>

 <div id="footer">
  <div style=float:left;>
    Copyright &copy; <?php echo date('Y'); ?> BRAC. All Rights Reserved.</div>
    
    <div style=float:right;>Powered By <a href="http://ict.brac.net">BRAC ICT</a>
    </div>
    <div class=clearfix></div>
    
  </div><!-- footer -->

</div><!-- page -->

</body>
</html>