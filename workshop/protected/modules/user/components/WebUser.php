<?php

class WebUser extends CWebUser
{
    private $_model;
    public function getRole()
    {
        return $this->getState('__role');
    }
    
    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }

//    protected function beforeLogin($id, $states, $fromCookie)
//    {
//        parent::beforeLogin($id, $states, $fromCookie);
//
//        $model = new UserLoginStats();
//        $model->attributes = array(
//            'user_id' => $id,
//            'ip' => ip2long(Yii::app()->request->getUserHostAddress())
//        );
//        $model->save();
//
//        return true;
//    }

    protected function afterLogin($fromCookie)
	{
        parent::afterLogin($fromCookie);
        $this->updateSession();
	}

    public function updateSession() {
        $user = Yii::app()->getModule('user')->user($this->id);
        $userAttributes = CMap::mergeArray(array(
                                                'email'=>$user->email,
                                                'username'=>$user->username,
                                                'picmarket_user'=>$user->picmarket_user,
                                                'create_at'=>$user->create_at,
                                                'lastvisit_at'=>$user->lastvisit_at,
                                           ),$user->profile->getAttributes());
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }
    }

    public function model($id=0) {
        return Yii::app()->getModule('user')->user($id);
    }

    public function user($id=0) {
        return $this->model($id);
    }

    public function getUserByName($username) {
        return Yii::app()->getModule('user')->getUserByName($username);
    }

    public function getAdmins() {
        return Yii::app()->getModule('user')->getAdmins();
    }

    public function isAdmin() {
        return Yii::app()->getModule('user')->isAdmin();
    }
     public function isPickDrop() {
        $userInfo=User::model()->findbyPk(Yii::app()->user->id);
            return $userInfo['picmarket_user'];
    }
    public function isViewUser() {
        $v_user = array('175191');
        return in_array(Yii::app()->user->username,$v_user);
    }
    function getUserRights(){
        $user = $this->loadUser(Yii::app()->user->id);
        $user_role_id=$user->role_id;
        $user_r_rights=RoleRights::model()->findALL('role_id=:roleid',array(':roleid'=>$user_role_id));
        return array_map(function($element) {
            $user_rights=Rights::model()->findByPk($element->rights_id);
            return $user_rights['rights_name'];
        }, $user_r_rights);
    }
    protected function loadUser($id=null)
    {
        if($this->_model===null)
        {
            if($id!==null)
                $this->_model=User::model()->findByPk($id);
        }
        return $this->_model;
    }

}