<div class="heading-block fancy-title nobottomborder topmargin">
</div>
<div class="fright">
    <a class="button create_button button-pink" href="<?php echo Yii::app()->createUrl("user/user/sisterconcernuser"); ?>">Sister Concern Users</a>
</div>
<div class="clear"></div>
<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
    <h4 class="heading-custom page_header_h4">Pending Requests</h4>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$model->search_request_pending(),
    'id'=>'search-request-pending',
    'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin border-round-bottom'),
    'filter'=>$model,

    'rowCssClass'=>array('cart_item'),
//    'dataProvider'=>$model->search(),
    'columns'=>array(
        array(
            'name' => 'username',
            'type'=>'raw',
            'value' => function($data){
                return CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id));
            },
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),
        ),
          array(  
            'header'=>'Photo',   
            'type' => 'raw',            
            'value'=>'CHtml::image(SeatRequest::model()->photo($data->username),"USER IMAGE",array("width"=>"29px"))', 
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),                          
                        
        ),
        array(
            'name' => 'create_at',
            'type'=>'raw',
            'filter'=>false,
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),
        ),
        array(
            'name' => 'lastvisit_at',
            'type'=>'raw',
            'filter'=>false,
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),
        ),
        array(
            'header' => 'Action',
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons'=>array(
                'update'=>array(
                    'url'=>'Yii::app()->createUrl("user/admin/update", array("id"=>$data->id))',
                )
            )
        )
    ),
)); ?>
