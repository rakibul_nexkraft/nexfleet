<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('index'),
	$model->username,
);
$this->layout='//layouts/column2';
$this->menu=array(
    array('label'=>UserModule::t('List User'), 'url'=>array('index')),
);
?>
<div class="col_full">
    <a class="button create_button button-pink" href="<?php echo $model->picmarket_user?Yii::app()->createUrl("/user/user/sisterconcernuser"):Yii::app()->createUrl("/user/user"); ?>" style="width: 130px !important;float: right;">Back to List</a>
</div>
<div class="clear"></div>
<div class="col_full page_header_div" style="border-bottom: 1px solid #f2ecec;">
    <h4 class="heading-custom page_header_h4"><?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname.' [Pin: '.$model->username.']'; ?></h4>
    <div class="clear"></div>
</div>

<div class="col_one_fourth profile-view-responsive" style="background: #fff;padding: 2rem; margin-top: 15px;">
    <a class="thumbnail">
        <!--<img alt="100%x180" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/extras/200x200.gif" style="height: 180px; width: 100%; display: block;">-->
        <?php
        //var_dump($model->username);
        echo CHtml::image(SeatRequest::model()->photo($model->username),array("style"=>"height:180px;width: 100%; display: block;","alt"=>"100%x180"));?>
    </a>
</div>
<div class="col_three_fourth col_last profile-view-responsive" style="background: #fff;">

    <?php

    // For all users
    $attributes = array(
        'username',
    );

    $profileFields=ProfileField::model()->forAll()->sort()->findAll();
    if ($profileFields) {
        foreach($profileFields as $field) {
            array_push($attributes,array(
                'label' => UserModule::t($field->title),
                'name' => $field->varname,
                'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),

            ));
        }
    }
    array_push($attributes,
        'create_at',
        array(
            'name' => 'lastvisit_at',
            'value' => (($model->lastvisit_at!='0000-00-00 00:00:00')?$model->lastvisit_at:UserModule::t('Not visited')),
        )
    );

    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>$attributes,
        'htmlOptions'=>array('class'=>'table table-hover')
    ));

    ?>
</div>
