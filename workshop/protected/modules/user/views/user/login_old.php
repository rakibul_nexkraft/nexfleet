<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
	UserModule::t("Login"),
);
?>

			<section id="content">

			<div class="content-wrap nopadding">

				<div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: #fff;"></div>

				<div class="section nobg full-screen nopadding nomargin">
					<div class="container vertical-middle divcenter clearfix">

						<div class="row center">
							<a href="index.html"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/iFleet logo difference-16.png" alt="Brac iFleet User Logo"></a>
						</div>

						<div class="panel panel-default divcenter noradius noborder" style="max-width: 400px;">
								<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
								<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
								<?php endif; ?>
							<div class="panel-body" style="padding: 40px;">
								<!--<form id="login-form" name="login-form" class="nobottommargin" action="#" method="post">-->

									<?php echo CHtml::beginForm(); ?>
									<?php echo CHtml::errorSummary($model); ?>
									<h3>Login to your Account</h3>
									<p class="note">Only for brac sister concern user</p>
									<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
									<div class="col_full">
									
										<?php echo CHtml::activeLabelEx($model,'username'); ?>
										
										<?php echo CHtml::activeTextField($model,'username',array('class'=>'form-control not-dark')) ?>
									</div>

									<div class="col_full">										
										<?php echo CHtml::activeLabelEx($model,'password'); ?>
									<?php echo CHtml::activePasswordField($model,'password',array('class'=>'form-control not-dark')) ?>
									</div>
									<div class="col_full">	
										<?php echo CHtml::activeCheckBox($model,'rememberMe'); ?>
										<?php echo CHtml::activeLabelEx($model,'rememberMe'); ?>
									</div>
									
									<div class="col_full nobottommargin">
										<button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
										<div class="clearfix"></div>
										<a href="<?php echo Yii::app()->createAbsoluteUrl("/user/registration");?>" class="fleft">New User Registration</a>
										<a href="#" class="fright">Forgot Password?</a>
									</div>
								<!--</form>-->
								<?php echo CHtml::endForm(); ?>

								
							</div>
						</div>

						<div class="row center dark"><small>Copyrights &copy; All Rights Reserved by Brac.</small></div>

					</div>
				</div>

			</div>

		</section><!-- #content end -->

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>