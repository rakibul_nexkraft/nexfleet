<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
if(UserModule::isAdmin()) {
	$this->layout='//layouts/column2';
	$this->menu=array(
	    array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
	    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
	);
}
?>

<!--<h4>< ?php echo UserModule::t("List User"); ?></h4>-->

<!--<div class="heading-block fancy-title nobottomborder topmargin title-bottom-border">
    <h4><a class="button button-3d button-mini button-rounded button-green" href="<?php /*echo Yii::app()->createUrl("/user/admin/create"); */?>">Create User</a> <?php /*echo UserModule::t("List of Users"); */?></h4>
    <div class="clear"></div>
</div>-->
<!--<div class="heading-block fancy-title nobottomborder topmargin">

</div>-->
<div class="fright">
    <a class="button create_button button-pink create-button-responsive" href="<?php echo Yii::app()->createUrl("user/admin/create"); ?>">Create User</a>
</div>
<div class="clearfix"></div>
<!--<div class="col_full" style="margin-bottom: 0">
    <h4 class="heading-custom page_header_h4 heading-label-responsive" style="text-transform: unset; margin: -4rem 0 0 0px;">List of Users</h4>
</div>-->
<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin">
    <h4 class="heading-custom page_header_h4">List of Users</h4>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$model->search(),
    'id'=>'search',
    'itemsCssClass' => 'table cart table_um',
    'htmlOptions' => array('class' => 'table-responsive bottommargin, admin-table-responsive border-round-bottom'),
    'filter' => $model,
    'rowCssClass'=>array('cart_item'),
//    'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id))',
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),
		),
        array(  
                'header'=>'Photo',  
                'type' => 'raw',            
                'value'=>'CHtml::image(SeatRequest::model()->photo($data->username),"",array("width"=>"50px"))', 
                'htmlOptions'=>array('style'=>'text-align: center'), 
                'headerHtmlOptions'=>array('style'=>'text-align: center'),              
            ),
    
		array(
            'name' => 'create_at',
            'type'=>'raw',
            'filter'=>false,
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),
        ),
		array(
            'name' => 'lastvisit_at',
            'type'=>'raw',
            'filter'=>false,
            'htmlOptions'=>array('style'=>'text-align: center'), 
            'headerHtmlOptions'=>array('style'=>'text-align: center'),
        ),
        array(
            'header' => 'Action',
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons'=>array(
                'update'=>array(
                    'url'=>'Yii::app()->createUrl("user/admin/update", array("id"=>$data->id))',
                )
            )
            
        )
	),
)); ?>
