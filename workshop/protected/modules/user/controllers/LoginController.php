<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{   
		if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if((isset($_POST['UserLogin']))||(isset($_GET['pin'])&&isset($_GET['fleet'])&&!empty($_GET['pin'])&&$_GET['fleet']==1))
			{
				if(isset($_POST['UserLogin'])){
					$model->attributes=$_POST['UserLogin'];
				}
				if(isset($_GET['pin']) && isset($_GET['fleet']) && !empty($_GET['pin']) && $_GET['fleet']==1){
					$model->username=$_GET['pin'];
					$model->password=$_GET['fleet'];
				}
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();

					if (Yii::app()->user->returnUrl=='/index.php')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
            }
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);
	}
	
	private function lastViset() {

		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}

}