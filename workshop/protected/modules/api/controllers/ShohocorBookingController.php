<?php

class ShohocorBookingController  extends Controller
{
	/*public function actionView()
	{
		//echo $_GET['id'];
		$success = 1;
		$data = $_GET['id'];
		$message = "";
		Yii::app()->printMessage->getPrint($success,$data,$message);
	
		
	}*/
	public function actionCreate() {
		if(empty($_POST['SeatBooking']['user_pin'])) {
			 Yii::app()->printMessage->getPrint(0,"","User pin cannot be empty.");
			 return;
		}
		$directions = $_POST['SeatBooking']['direction_travale']==2 ? "O" : "I"; 
    	if(!empty($_POST['SeatBooking']['sale_id'])) {
    		$sale_seat_id='s'.$_POST['SeatBooking']['sale_id'];
    		$seats_av = $this->seatAVCheck($sale_seat_id,$directions);
    	}
    	else{
    		$seats_av = $this->seatAVCheck($_POST['SeatBooking']['route_id'],$directions);
    	}
    	if($seats_av<= 0) {
			Yii::app()->printMessage->getPrint(0,"","Sorry, Seat Not Available, Try For Another Route.");
			return;
		}
    	$model = new SeatBooking;        
        if(isset($_POST['SeatBooking'])){
			$model->attributes=$_POST['SeatBooking'];
			$model->created_by = $_POST['SeatBooking']['user_pin'];           
        	$model->created_time = date("Y-m-d H:i:s"); 
			$b_user_history_last = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin='".$model->user_pin."' ORDER BY id DESC")->queryRow();
			if(empty($b_user_history_last)){
				Yii::app()->printMessage->getPrint(0,"","Sorry, You Have No  Balance.");
			 	return;			
			}
			else{
				$b_user_balance = $b_user_history_last['balance']-$model->price;
				if($b_user_balance<0) {
					Yii::app()->printMessage->getPrint(0,"","Sorry, You Have Not Enough Balance.");	
					return;
				}
			}
			$balance = !empty($tr_history_last) ? $tr_history_last['balance'] : 0;
			if($model->save()) {
				$price =  ($model->price*60)/100;
				$tr_history_last = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin = '0' ORDER BY id DESC")->queryRow();
				$balance = !empty($tr_history_last) ? $tr_history_last['balance'] : 0;
				$sale_entry= array(
					'user_pin' =>'0',
					'sale_id'=>$model->sale_id,
					'booking_id'=>0,
					'credit'=>$model->price,
					'debit' => 0,
					'payment_method'=>'',
					'transaction_id'=>'',
					'balance' =>$balance+$model->price,
					'sale_booking_id'=>$model->id,
					'created_by'=>$model->user_pin,
					'created_time'=>date("Y-m-d H:i:s"),
				);
				if(!empty($model->sale_id) && ($model->sale_id!=0)) {
					$seat_sale = SeatSale::model()->findByPk($model->sale_id);
					$tr_history_last = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin='".$seat_sale["user_pin"]."' ORDER BY id DESC")->queryRow();
					$balance = !empty($tr_history_last) ? $tr_history_last['balance'] : 0;
					$sale_entry= array(
					'credit'=>$price,
					'balance' =>$balance+$price,
					'user_pin' =>$seat_sale["user_pin"],					
					);					
				}
				$this->transactionBook($sale_entry);					
				$book_entry= array(
				'user_pin' =>$model->user_pin,
				'sale_id'=>0,
				'booking_id'=>$model->id,
				'credit'=>0,
				'debit'=>$model->price,
				'payment_method'=>'',
				'transaction_id'=>'',
				'balance' =>$b_user_balance,
				'sale_booking_id'=>0,
				'created_by'=>$model->user_pin,
				'created_time'=>date("Y-m-d H:i:s"),
				);
				$this->transactionBook($book_entry);
				$data = array("success"=>"Your Data Save Successfully.");		
				Yii::app()->printMessage->getPrint(1,$data,"");			
				return;
			}
		}
		Yii::app()->printMessage->getPrint(0,"","Please, Try Again!");			
		return;
	}
	private function transactionBook($sale_entry){
    	$model=new TransactionHistory;
    	$model->unsetAttributes(); 
    	$model->attributes=$sale_entry;
		$model->save();
    }
	public function actionList()
	{
		$data = '';		
		if(empty($_GET['user'])) {
			Yii::app()->printMessage->getPrint(0,$data,"User pin cannot be empty");
			return;
		}
		else if(empty($_GET['page'])) {
			Yii::app()->printMessage->getPrint(0,$data,"Page number cannot be empty");
			return;
		}
		else {			
			$booking = Yii::app()->db->createCommand("SELECT b.id, b.created_time as booking_date, r.route_no as route, b.date_from as `date`, b.direction_travale, b.price, b.status, b.transfer_user  FROM tbl_seat_booking as b INNER JOIN tbl_routes as r ON r.id=b.route_id WHERE b.user_pin='".$_GET['user']."' ORDER BY id desc LIMIT ".(($_GET['page']-1)*5).", 5 ")->queryAll();
			Yii::app()->printMessage->getPrint(1,$booking,"");
		}		
	}
	public function actionRoutelist() {		
		$data = '';
		$model = new SeatSale('search');
        $model->unsetAttributes();        
        $valid_routes = Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id INNER JOIN tbl_stoppage as s ON s.route_id=r.id GROUP BY r.route_no")->queryAll();           
           //  "HO OUT";
         if(date("H:i:s")>="08:30:00" && date("H:i:s")<="17:00:00"){
            $routes_out = Yii::app()->db->createCommand("SELECT r.*, sa.from_date, sa.travel_direction, sa.id as sid FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_stoppage as s ON s.route_id=r.id INNER JOIN tbl_seat_sale as sa ON sa.route_id=r.id WHERE sa.travel_direction=2 AND sa.from_date='".date("Y-m-d")."' GROUP BY sa.id")->queryAll();
        }  
            // "HO IN";
        if(date("H:i:s")>="08:30:00" && date("H:i:s")<="20:30:00"){
            $routes_in = Yii::app()->db->createCommand("SELECT r.*, sa.from_date, sa.travel_direction, sa.id as sid FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_stoppage as s ON s.route_id=r.id INNER JOIN tbl_seat_sale as sa ON sa.route_id=r.id WHERE sa.travel_direction=1 AND sa.from_date='".date("Y-m-d", strtotime("+1 day"))."' GROUP BY sa.id")->queryAll();
        }
        $route_list=array();
        foreach ($valid_routes as $key => $value) {
           $seats_oc = Routes::borderPass($value['route_no']);           
           $route_seat_sale_out = Yii::app()->db->createCommand("SELECT count(id) as rs FROM tbl_seat_booking WHERE route_id='".$value['id']."' AND sale_id=0 AND date_from='".date('Y-m-d')."' AND direction_travale=2 AND status=0")->queryRow();
           $route_seat_sale_in = Yii::app()->db->createCommand("SELECT count(id) as rs FROM tbl_seat_booking WHERE route_id='".$value['id']."' AND sale_id=0  AND date_from='".date("Y-m-d", strtotime("+1 day"))."' AND direction_travale=1 AND status=0")->queryRow();                 
           $seats_av_out=$value['actual_seat']-$seats_oc-$route_seat_sale_out['rs'];
           $seats_av_in=$value['actual_seat']-$seats_oc-$route_seat_sale_in['rs'];          
           if($seats_av_out>0 && date("H:i:s")>="08:30:00" && date("H:i:s")<="17:00:00") {
            $route_list[$value['id']."O"] = array(
                'id'=>$value['id'],
                'route_no'=>$value['route_no'],
                'av'=>$seats_av,
                'type'=>$value['type'],
                'market_price'=>$value['market_price'],
                'travel_direction'=>"HO OUT",
                'date' => date('Y-m-d'),
                );
            }
            if($seats_av_in>0 && date("H:i:s")>="08:30:00" && date("H:i:s")<="20:30:00") {
            $route_list[$value['id']."I"] = array(
                'id'=>$value['id'],
                'route_no'=>$value['route_no'],
                'av'=>$seats_av,
                'type'=>$value['type'],
                'market_price'=>$value['market_price'],
                'travel_direction'=>"HO IN",
                'date' => date("Y-m-d", strtotime("+1 day")),
                );
           }
        } 
        if(isset($routes_out) && count($routes_out)>0) {
            foreach ($routes_out as $key => $value) {
                $check_sale = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_booking WHERE sale_id='".$value["sid"]."' AND status=0")->queryRow();
                if(!$check_sale) {
                    if (array_key_exists($value['id']."O", $route_list)) {                           
                        $route_list[$value['id']."O"]['av'] = $route_list[$value['id']."O"]['av']+1; 
                         $route_list[$value['id']."O"]['travel_direction']='HO OUT';                 
                        $route_list['s'.$value['sid']."O"] = $route_list[$value['id']."O"];
                        unset($route_list[$value['id']."O"]);
                       
                    }
                    else {
                        $route_list['s'.$value['sid']."O"]=array(
                        'id'=>$value['id'],
                        'route_no'=>$value['route_no'],
                        'av'=>$route_list[$value['id']."O"]['av']+1,
                        'type'=>$value['type'],
                        'market_price'=>$value['market_price'],
                        'travel_direction'=>'HO OUT',
                        'date' => date('Y-m-d'),
                        );
                    } 
                }             
            }
        }      
        if(isset($routes_in) && count($routes_in)>0) {
            foreach ($routes_in as $key => $value) {
                $check_sale = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_booking WHERE sale_id='".$value["sid"]."' AND status=0")->queryRow();
                if(!$check_sale) {
                    if (array_key_exists($value['id']."I", $route_list)) {                         
                         $route_list[$value['id']."I"]['av']=$route_list[$value['id']."I"]['av']+1; 
                         $route_list[$value['id']."I"]['travel_direction']='HO IN'; 
                        $route_list['s'.$value['sid']."I"]= $route_list[$value['id']."I"];
                        unset($route_list[$value['id']."I"]); 
                    }
                    else {                                                
                        $route_list[$value['id']."I"]=array(
                        'id'=>$value['id'],
                        'route_no'=>$value['route_no'],
                        'av'=>$route_list[$value['id']."I"]['av']+1,
                        'type'=>$value['type'],
                        'market_price'=>$value['market_price'],
                        'travel_direction'=>'HO IN',
                        'date' => date("Y-m-d", strtotime("+1 day")),
                        );                   
                        
                    }
                }             
            }
        }

        foreach ($route_list as $key => $value) {
            $stoppage_rs = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$value['id']."' ORDER BY pickup_time")->queryAll();    	
            $stop = '[Route No: '.$value['route_no'].'] ';
    		if(count($stoppage_rs)>0) {
				foreach ($stoppage_rs as $key1 => $value_stoppage) {
					$stop.= $value_stoppage['stoppage'];
						if(count($stoppage_rs)>$key1+1)$stop.=  " -> ";
				} 
			}
                       
            $data [] = array('id'=>$key,
        				'text' => $stop. ", ".$value['travel_direction']."-".$value['type']."-".$value['market_price']."TK");  
             
        } 

        Yii::app()->printMessage->getPrint(1,$data,"");
        

	}
	public function actionBSRouteinfo() {
		if(empty($_GET['sid']) || $_GET['sid']==0) {
			Yii::app()->printMessage->getPrint(0,"","Sorry, invalid request ID.");
			return;
		}
		$directions = substr($_GET['sid'], -1); 
		$_GET['sid'] = substr($_GET['sid'], 0, -1); 
		$seats_av = $this->seatAVCheck($_GET['sid'],$directions);
		if($seats_av<= 0) {
			Yii::app()->printMessage->getPrint(0,"","Sorry, Seat Not Available, Try For Another Route.");
			return;
		}
        if($_GET['sid'][0]=='s') {
	        $_GET['sid']=substr($_GET['sid'],1);
	        $sale = Yii::app()->db->createCommand("SELECT s.id as sale_id, s.route_id, r.route_no, r.market_price as price, s.from_date as date_from, s.travel_direction as direction_travale  FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id WHERE s.id='".$_GET['sid']."'")->queryRow();
        }
        else {        	
        	$route = Routes::model()->findByPk($_GET['sid']);        	
        	$tr_dr = $directions=="O" ? 2 : ($directions=="I" ? 1 : "");
        	$date = $directions=="O" ? date('Y-m-d') : ($directions=="I" ? date("Y-m-d", strtotime("+1 day")) : "");
        	 $sale = array(
        	 	'sale_id'=>'',
        		'route_id'=>$route['id'],
        		'route_no'=>$route['route_no'],
        		'price'=>$route['market_price'],
        		'direction_travale'=>$tr_dr,
        		'date_from'=> $date,        		
        	);
        }
        $sale['av_seat'] =  $seats_av;
        Yii::app()->printMessage->getPrint(1,$sale,"");
		return;
	}

	private function seatAVCheck($sid,$directions){
    	$seat_count = 0; 
    	if($sid[0]=='s') {
	        $sid = substr($sid,1);
	        $sale = Yii::app()->db->createCommand("SELECT s.route_id as id, r.route_no, s.from_date,s.travel_direction, r.actual_seat FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id WHERE s.id='".$sid."'")->queryRow();
	        $seat_sale = Yii::app()->db->createCommand("SELECT count(id) as total_seat FROM tbl_seat_sale  WHERE route_id='".$sale['id']."' AND from_date='".$sale["from_date"]."' AND travel_direction='".$sale["travel_direction"]."' AND id NOT IN(SELECT sale_id FROM tbl_seat_booking WHERE status=0)")->queryRow();
	        $seat_count = $seat_sale['total_seat'];

        }
        else {        	
        	$sale = Routes::model()->findByPk($sid);        	
        }
        if($sale) {
        	$seats_oc = Routes::borderPass($sale['route_no']);
        	$sql = ($directions=="O") ? " AND date_from='".date('Y-m-d')."' AND direction_travale=2" : " AND date_from='".date("Y-m-d", strtotime("+1 day"))."' AND direction_travale=1";

            $route_seat_sale = Yii::app()->db->createCommand("SELECT count(id) as rs FROM tbl_seat_booking WHERE route_id='".$sale['id']."' AND sale_id=0 AND status=0 ".$sql)->queryRow(); 
        	return  $sale['actual_seat']-$seats_oc-$route_seat_sale['rs']+$seat_count;        	
        }
        return 0;
    }
    public function actionCancel() {    
    	if(empty($_GET['user'])) {
			Yii::app()->printMessage->getPrint(0,$data,"User pin cannot be empty");
			return;
		}
		if(empty($_GET['id'])) {
			Yii::app()->printMessage->getPrint(0,$data,"ID cannot be empty");
			return;
		}
    	$cancel = Yii::app()->db->createCommand("UPDATE tbl_seat_booking SET status=-1 WHERE id='".$_GET['id']."'")->execute();
    	if($cancel) {
    		$book = SeatBooking::model()->findByPk($_GET['id']);
    		$route = Routes::model()->findByPk($book['route_id']);
    		$data = array("success" => "Your seat booking cancel for route no. ".$route['route_no'].".");
    		Yii::app()->printMessage->getPrint(1,$data,"");			
			return; 		
    	}
    	else {
    		Yii::app()->printMessage->getPrint(0,$data,"Sorry, try again");
			return;    		
    	}
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}