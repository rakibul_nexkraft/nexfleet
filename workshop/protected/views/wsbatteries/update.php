<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsbatteriesController */
/* @var $model Wsbatteries */

$this->breadcrumbs=array(
	'Wsbatteries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Batteries', 'url'=>array('index'),'visible'=> in_array('List Batteries', $user_rights)),
	array('label'=>'New Battery', 'url'=>array('create'),'visible'=> in_array('New Battery', $user_rights)),
	array('label'=>'View Battery', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Battery', $user_rights)),
	array('label'=>'Manage Batteries', 'url'=>array('admin'),'visible'=> in_array('Manage Batteries', $user_rights)),
);
?>

<h4>Update Battery <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'remarkstype'=>$remarkstype)); ?>