<?php
/* @var $this HomepageController */
/* @var $model Homepage */

$this->breadcrumbs=array(
	'Homepages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Homepage', 'url'=>array('index')),
	array('label'=>'Create Homepage', 'url'=>array('create')),
	array('label'=>'View Homepage', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Homepage', 'url'=>array('admin')),
);
?>

<h1>Update Homepage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>