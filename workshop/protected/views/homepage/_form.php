<?php
/* @var $this HomepageController */
/* @var $model Homepage */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'homepage-form',
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

    <div class="col_full nobottommargin">
    	<p class="note">Fields with <span class="required">*</span> are required.</p>
    </div>

    <div class="col_full bottommargin">
    	<?php echo $form->errorSummary($model); ?>
    </div>
    <div class="panel panel-default divcenter noradius noborder clearfix">
        <div class="panel-body">
            <div class="col_half">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>150, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>
            <div class="col_half col_last">
                <?php echo $form->labelEx($model,'text'); ?>
                <?php echo $form->textField($model,'text',array('size'=>60,'maxlength'=>500, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'text'); ?>
            </div>
            <div class="clear"></div>

            <div class="col_half">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>150, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'title'); ?>
            </div>
            <div class="col_half col_last">
                <?php echo $form->labelEx($model,'image_url'); ?>
                <?php echo $form->fileField($model,'image_url',array('size'=>60,'maxlength'=>250, 'class' => 'form-control not-dark')); ?>
                <?php if($model->id) echo $this->getFileName("uploads/homepage/",$model->image_url,$model->id); ?>
                <?php echo $form->error($model,'image_url'); ?>
            </div>
            <div class="clear"></div>

            <div class="col_half">
                <?php echo $form->labelEx($model,'type'); ?><!-- $form->dropDownList($model,'form_id',Forms::model()->findAll()); -->
                <?php echo $form->dropDownList($model,'type',$model->content_type, array('class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'type'); ?>
            </div>
            <div class="clear"></div>

            <div class="col_full">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'button button-3d button-green nomargin')); ?>
                <a class="button button-3d button-rounded button-red" href="<?php echo Yii::app()->createUrl("homepage/index") ?>">Cancel</a>
            </div>

        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->