<?php
/* @var $this HomepageController */
/* @var $model Homepage */

$this->breadcrumbs=array(
	'Homepages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Homepage', 'url'=>array('index')),
	array('label'=>'Create Homepage', 'url'=>array('create')),
	array('label'=>'Update Homepage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Homepage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Homepage', 'url'=>array('admin')),
);
?>

<div class="heading-block fancy-title nobottomborder topmargin theme-bottom-border">
    <div class="fleft">
        <h4><?php echo '<strong>'.$model->type.'</strong> of '.$model->name ?></h4>
    </div>
    <div class="fright">
        <a class="button button-3d button-mini button-rounded button-green" href="<?php echo Yii::app()->createUrl("homepage/update", array("id"=>$model->id)); ?>">Edit</a>
        <a class="button button-3d button-mini button-rounded button-green" href="<?php echo Yii::app()->createUrl("homepage/index"); ?>">Back to Homepage</a>
    </div>
    <div class="clear"></div>
</div>

<div class="col_three_fourth">
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'htmlOptions'=>array('class'=>'table table-hover'),
        'attributes'=>array(
            'id',
            'name',
            'text',
            'title',
//            'image_url',
            'type',
            'created_by',
            'created_time',
            'updated_by',
            'updated_time',
        ),
    )); ?>
</div>

<div class="col_one_fourth col_last">
    <a class="thumbnail">
        <?php if(isset($model->image_url)) { ?>
            <img alt="100%x180" src="<?php echo Yii::app()->baseUrl.'/uploads/homepage/'.$model->image_url ?>" style="height: 180px; width: 100%; display: block;">
        <?php } else { ?>
            <img alt="100%x180" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/extras/200x200.gif" style="height: 180px; width: 100%; display: block;">
        <?php } ?>
    </a>
</div>
