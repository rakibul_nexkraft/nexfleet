<?php
/* @var $this WsrequisitionsController */
/* @var $model Wsrequisitions */

$this->breadcrumbs=array(
	'Wsrequisitions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Wsrequisitions', 'url'=>array('index')),
	array('label'=>'New Wsrequisition', 'url'=>array('create')),
	array('label'=>'Update Wsrequisition', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Wsrequisition', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Wsrequisitions', 'url'=>array('admin')),
);
?>

<h4>View Wsrequisition #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'requisition_date',
		'vehicle_reg_no',
		'vehicletype_id',
	//	'vmodel',
	//	'engine_no',
	//	'vin',
		'create_time',
		'created_by',
		'updated_time',
		'updated_by',
		'active',
	),
)); ?>
