<?php
/* @var $this WsrequisitionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wsrequisitions',
);

$this->menu=array(
	array('label'=>'New Wsrequisition', 'url'=>array('create')),
	array('label'=>'Manage Wsrequisitions', 'url'=>array('admin')),
);
?>

<h4>Wsrequisitions</h4>

<?php /* $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */ ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'wsrequisitions-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,

    'columns'=>array(
        //'id',
/*        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),*/
        'requisition_no',
        'requisition_date',
        'vehicle_reg_no',

       // 'vmodel',
       // 'engine_no',
       // 'vin',
        'created_by',
    ),
)); ?>

