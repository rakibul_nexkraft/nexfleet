<?php
/* @var $this WssnacksbiilamountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wssnacksbiilamounts',
);

$this->menu=array(
	array('label'=>'Create Snacks Biil Amount', 'url'=>array('create')),
	array('label'=>'Manage Snacks Biil Amount', 'url'=>array('admin')),
);
?>

<h1>List Snacks Biil Amount</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
