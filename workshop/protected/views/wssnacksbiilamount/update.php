<?php
/* @var $this WssnacksbiilamountController */
/* @var $model Wssnacksbiilamount */

$this->breadcrumbs=array(
	'Wssnacksbiilamounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Snacks Biil Amount', 'url'=>array('index')),
	array('label'=>'Create Snacks Biil Amount', 'url'=>array('create')),
	array('label'=>'View Snacks Biil Amount', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Snacks Biil Amount', 'url'=>array('admin')),
);
?>

<h1>Update Snacks Biil Amount <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>