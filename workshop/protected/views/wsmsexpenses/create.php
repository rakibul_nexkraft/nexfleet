<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsmsexpensesController */
/* @var $model Wsmsexpenses */

$this->breadcrumbs=array(
	'Wsmsexpenses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Misc Expenses', 'url'=>array('index'),'visible'=> in_array('List Misc Expenses', $user_rights)),
	array('label'=>'Manage Misc Expenses', 'url'=>array('admin'),'visible'=> in_array('Manage Misc Expenses', $user_rights)),
);
?>

<h4>New Misc Expense</h4>
<?php 
if(isset($_POST['id'])&& !empty($_POST['id'])){
	$id=$_POST['id'];
	$model=Wsmsexpenses::model()->findByAttributes(array('id'=>$id));
}

if(isset($_POST['attr'])&& !empty($_POST['attr'])){
	
	$model->attributes=(array)json_decode($_POST['attr']);
	
}
if(isset($_POST['erro'])&& !empty($_POST['erro'])){	
	$errors=(array)json_decode($_POST['erro']);	
echo $this->renderPartial('_form', array('model'=>$model,'remarkstype'=>$remarkstype,'erro'=>$errors)); 	
}
else{
echo $this->renderPartial('_form', array('model'=>$model,'remarkstype'=>$remarkstype));
}
 ?>