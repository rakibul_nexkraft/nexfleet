<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsmsexpensesController */
/* @var $model Wsmsexpenses */

$this->breadcrumbs=array(
	'Wsmsexpenses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Misc Expenses', 'url'=>array('index'),'visible'=> in_array('List Misc Expenses', $user_rights)),
	array('label'=>'New Misc Expense', 'url'=>array('create'),'visible'=> in_array('New Misc Expense', $user_rights)),
	array('label'=>'View Misc Expense', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Misc Expense', $user_rights)),
	array('label'=>'Manage Misc Expenses', 'url'=>array('admin'),'visible'=> in_array('Manage Misc Expenses', $user_rights)),
);
?>

<h4>Update Misc Expenses <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>