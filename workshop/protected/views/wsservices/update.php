<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsservicesController */
/* @var $model Wsservices */

$this->breadcrumbs=array(
	'Wsservices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Service List - Inside', 'url'=>array('index'),'visible'=> in_array('Service List - Inside', $user_rights)),
	array('label'=>'New Service - Inside', 'url'=>array('create'),'visible'=> in_array('New Service - Inside', $user_rights)),
	array('label'=>'View Service - Inside', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Service - Inside', $user_rights)),
	array('label'=>'Manage Service List - Inside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Inside', $user_rights)),
);
?>

<h4>Update Service - Inside <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>