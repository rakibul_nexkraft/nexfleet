<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsservicesController */
/* @var $model Wsservices */

$this->breadcrumbs=array(
	'Wsservices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Service List - Inside', 'url'=>array('index'),'visible'=> in_array('Service List - Inside', $user_rights)),
	array('label'=>'New Service - Inside', 'url'=>array('create'),'visible'=> in_array('New Service - Inside', $user_rights)),
	array('label'=>'Update Service - Inside', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Service - Inside', $user_rights)),
	array('label'=>'Delete Service - Inside', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Service - Inside', $user_rights)),
	array('label'=>'Manage Service List - Inside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Inside', $user_rights)),
);
?>

<h4>View Service - Inside #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'service_name',
		'service_category',
		'active',
		'updated_by',
		'updated_time',
	),
)); ?>
