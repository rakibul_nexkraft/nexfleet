<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsservicesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wsservices',
);

$this->menu=array(
	array('label'=>'New Service - Inside', 'url'=>array('create'),'visible'=> in_array('New Service - Inside', $user_rights)),
	array('label'=>'Manage Service List - Inside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Inside', $user_rights)),
);
?>

<h4>Service List - Inside</h4>

<?php
/*
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
 */
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'wsservices-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),

        'service_name',
        'service_category',
        'car',
        'micro_jeep',
        'pickup',
        'bus_coaster',
     //   'active',
/*
        'created_by',
        'created_time',
        'updated_by',
        'updated_time',
*/
    ),
)); ?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Wsservices'])));
?> &nbsp; &nbsp; &nbsp;

