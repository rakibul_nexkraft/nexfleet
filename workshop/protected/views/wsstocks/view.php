<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsstocksController */
/* @var $model Wsstocks */

$this->breadcrumbs=array(
	'Wsstocks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Stocks', 'url'=>array('index'),'visible'=> in_array('List Stocks', $user_rights)),
//	array('label'=>'Create Wsstocks', 'url'=>array('create')),
	//array('label'=>'Update Wsstock', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Stock', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Stock', $user_rights)),
	array('label'=>'Manage Stocks', 'url'=>array('admin'),'visible'=> in_array('Manage Stocks', $user_rights)),
);
?>

<h4>View Wsstock #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'wsitemname_id',
		'wsitemname',
        'vehicle_model',
        'parts_no',
		'stock_in',
		'stock_out',
		'available_stock',
		'temp_stock',
		'price',
		'total_amount',
		'purchase_date',
		'active',
		'wsitem_id'
	),
)); ?>
