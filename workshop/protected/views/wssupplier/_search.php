<?php
/* @var $this WssupplierController */
/* @var $model Wssupplier */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'id'); ?><br />
			<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'supplier_name'); ?><br />
			<?php echo $form->textField($model,'supplier_name',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'supplier_address'); ?><br />
			<?php echo $form->textField($model,'supplier_address',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'cell_no'); ?><br />
			<?php echo $form->textField($model,'cell_no',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<!--<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>-->

	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->