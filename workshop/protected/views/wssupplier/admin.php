<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssupplierController */
/* @var $model Wssupplier */

$this->breadcrumbs=array(
	'Supplier'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Supplier', 'url'=>array('index'),'visible'=> in_array('List Supplier', $user_rights)),
	array('label'=>'New Supplier', 'url'=>array('create'),'visible'=> in_array('New Supplier', $user_rights)),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('wssupplier-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h4>Manage Supplier</h4>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('bootstrap.widgets.TbGridView', array(
					'type'=>'striped bordered condensed',
					'id'=>'wssupplier-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'supplier_name',
						'supplier_address',
						'cell_no',
		/*
		'active',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
