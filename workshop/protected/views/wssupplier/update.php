<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssupplierController */
/* @var $model Wssupplier */

$this->breadcrumbs=array(
	'Supplier'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Supplier', 'url'=>array('index'),'visible'=> in_array('List Supplier', $user_rights)),
	array('label'=>'New Supplier', 'url'=>array('create'),'visible'=> in_array('New Supplier', $user_rights)),
	array('label'=>'View Supplier', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Supplier', $user_rights)),
	array('label'=>'Manage Supplier', 'url'=>array('admin'),'visible'=> in_array('Manage Supplier', $user_rights)),
);
?>

<h4>Update Supplier <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>