<?php
/* @var $this WssupplierController */
/* @var $model Wssupplier */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wssupplier-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div> -->

	<div class="row">
		<?php echo $form->labelEx($model,'supplier_name'); ?>
		<?php echo $form->textField($model,'supplier_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'supplier_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'supplier_address'); ?>
		<?php echo $form->textField($model,'supplier_address',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'supplier_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cell_no'); ?>
		<?php echo $form->textField($model,'cell_no'); ?>
		<?php echo $form->error($model,'cell_no'); ?>
	</div>
    <!--
	<div class="row">
		<?php //echo $form->labelEx($model,'active'); ?>
		<?php // echo $form->textField($model,'active'); ?>
		<?php //echo $form->error($model,'active'); ?>
	</div>


	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
    -->

    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->