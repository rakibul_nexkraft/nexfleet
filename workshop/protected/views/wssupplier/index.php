<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssupplierController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Supplier',
);

$this->menu=array(
	array('label'=>'New Supplier', 'url'=>array('create'),'visible'=> in_array('New Supplier', $user_rights)),
	array('label'=>'Manage Supplier', 'url'=>array('admin'),'visible'=> in_array('Manage Supplier', $user_rights)),
);
?>

<h4>Supplier</h4>

<div class="search-form">
    <?php $this->renderPartial('_search',array(
       'model'=>$model,
   )); ?>
</div><!-- search-form -->

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
 'type'=>'striped bordered condensed',
 'id'=>'wssuppliers-grid',
 'dataProvider'=>$dataProvider,

 'columns'=>array(
        //'id',
    array(
        'name' => 'id',
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
    ),
    'supplier_name',
    'supplier_address',
    'cell_no',
),
)); ?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Wssupplier'])));
?> &nbsp; &nbsp; &nbsp;
