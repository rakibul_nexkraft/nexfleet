<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssupplierController */
/* @var $model Wssupplier */

$this->breadcrumbs=array(
	'Supplier'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Supplier', 'url'=>array('index'),'visible'=> in_array('List Supplier', $user_rights)),
	array('label'=>'Manage Supplier', 'url'=>array('admin'),'visible'=> in_array('Manage Supplier', $user_rights)),
);
?>

<h4>New Supplier</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>