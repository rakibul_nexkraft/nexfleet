<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsiousController */
/* @var $model Wsious */

$this->breadcrumbs=array(
	'Wsiouses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List IOUs', 'url'=>array('index'),'visible'=> in_array('List IOUs', $user_rights)),
	array('label'=>'Manage IOUs', 'url'=>array('admin'),'visible'=> in_array('Manage IOUs', $user_rights)),
);
?>

<h4>New IOU</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>