<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsiousController */
/* @var $model Wsious */

$this->breadcrumbs=array(
	'Wsiouses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List IOUs', 'url'=>array('index'),'visible'=> in_array('List IOUs', $user_rights)),
	//array('label'=>'New IOU', 'url'=>array('create')),
	
	array('label'=>'New IOU', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/wsious/create','id'=>$model->id),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
			return false;
		}",
	)),'visible'=> in_array('New IOU', $user_rights))),
	array('label'=>'Update IOU', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update IOU', $user_rights)),
	array('label'=>'Delete IOU', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete IOU', $user_rights)),
	array('label'=>'Manage IOUs', 'url'=>array('admin'),'visible'=> in_array('Manage IOUs', $user_rights)),
);
?>

<h4>View Wsious #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'iou_date',
		'user_name',
		'user_pin',
		'designation',
		'iou_amount',
		'receive_date',
		'due_date_adjust',
		'adjust_date',
		'remarks',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>
