<?php
/* @var $this WsitemnamesController */
/* @var $model Wsitemnames */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsitemnames-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
<div class="container1">

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'parts_no'); ?>
		<?php echo $form->textField($model,'parts_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'parts_no'); ?>
	</div>

</div>
<div class="container2">

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_model'); ?>
		<?php echo $form->textField($model,'vehicle_model',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'vehicle_model'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_type'); ?>
		<?php echo $form->textField($model,'vehicle_type',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'vehicle_type'); ?>
	</div>

</div>

<div class="clearfix"></div>
	
	<!--<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>-->

	<!--<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>-->
	
	<div class="row buttons">
		<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		<?php
			$this->widget('bootstrap.widgets.TbButton',array(
				'label' =>'Save',
				'type' => 'primary',
				'buttonType'=>'submit', 
				'size' => 'medium',
			));
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->