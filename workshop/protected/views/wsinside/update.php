<?php
/* @var $this WsinsideController */
/* @var $model Wsinside */

$this->breadcrumbs=array(
	'Wsinsides'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Wsinside', 'url'=>array('index')),
	array('label'=>'Create Wsinside', 'url'=>array('create')),
	array('label'=>'View Wsinside', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Wsinside', 'url'=>array('admin')),
);
?>

<h1>Update Wsinside <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>