<?php
/* @var $this WsinsideController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wsinsides',
);

$this->menu=array(
	array('label'=>'Create Wsinside', 'url'=>array('create')),
	array('label'=>'Manage Wsinside', 'url'=>array('admin')),
);
?>

<h1>Wsinsides</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
