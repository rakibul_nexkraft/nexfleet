<?php
/* @var $this WsinsideController */
/* @var $model Wsinside */

$this->breadcrumbs=array(
	'Wsinsides'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Wsinside', 'url'=>array('index')),
	array('label'=>'Manage Wsinside', 'url'=>array('admin')),
);
?>

<h1>Create Wsinside</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>