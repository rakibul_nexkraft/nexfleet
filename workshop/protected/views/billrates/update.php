<?php
/* @var $this BillratesController */
/* @var $model Billrates */

$this->breadcrumbs=array(
	'Bill Rates'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Bill Rates List', 'url'=>array('index')),
		//array('label'=>'New Bill Rate', 'url'=>array('create')),
		array('label'=>'View Bill Rate', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Bill Rates', 'url'=>array('admin')),
	);
}
?>

<h4>Update Bill Rate : <?php echo $model->id; ?></h4>

<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php echo $this->renderPartial('_formU', array('model'=>$model)); ?>