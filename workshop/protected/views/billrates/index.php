<?php
/* @var $this BillratesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bill Rates',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	//array('label'=>'New Bill Rate', 'url'=>array('create')),
    	array('label'=>'Manage Bill Rates', 'url'=>array('admin')),
    );
}
?>

<h4>Bill Rates</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'billrates-grid',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
        'id',
        array(
            'name' => 'rate_type',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->rate_type),array("view","id"=>$data->id))',
        ),
        'rate_amount',
        'created_by',
        'created_time',
    ),
)); ?>