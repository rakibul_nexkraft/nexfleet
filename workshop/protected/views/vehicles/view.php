<?php
/* @var $this VehiclesController */
/* @var $model Vehicles */

$this->breadcrumbs=array(
	'Vehicles'=>array('index'),
	$model->reg_no,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Vehicles List', 'url'=>array('index')),
		array('label'=>'New Vehicle', 'url'=>array('create')),
		array('label'=>'Update Vehicle', 'url'=>array('update', 'reg_no'=>$model->reg_no)),
		array('label'=>'Delete Vehicle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','reg_no'=>$model->reg_no),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array(
	            'label'=>($model->active==1) ? 'Vehicle for Auction':'Activate Vehicle',
	            'url'=>'#',
	            'linkOptions'=>array('submit'=>array('deactivate','reg_no'=>$model->reg_no),
	                'confirm'=>($model->active!=2) ? 'Are you sure you want to submit this vehicle for auction?':'Are you sure you want to activate this vehicle?','csrf' => true),
	            'visible'=>($model->active==1 || $model->active==2)
	        ),
		array('label'=>'Manage Vehicles', 'url'=>array('admin')),
	);
}

?>

<h4>View Vehicle : <?php echo $model->reg_no; ?></h4>

<?php $model->vehicletype_id = $model->vehicletypes->type; ?>

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(		
		'reg_no',
		'vehicletype_id',
		'engine_no',		
		'cc',
		'vmodel',
		'chassis_no',
		'country',
		'purchase_date',
		'purchase_price',
		'supplier',
		'seat_capacity',
		'load_capacity',
		'ac',
		'driver_pin',
		'created_time',
		'created_by',
	//	'active',
	),
));*/ ?>

<?php $this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
                'reg_no',		'supplier',
                'vehicletype_id',	'seat_capacity',
                'engine_no',		'load_capacity',
                'cc',			'model_code',
                'vmodel',               'driver_pin',
                'chassis_no',		'location',
                'country',		'created_time',
                'purchase_date',	'created_by',
		        'purchase_price',	'allocated_to',
			    'ac',   array('name'=>'active','type'=>'raw','value'=>$model->activity($model->active)),
                array('name'=>'is_special','type'=>'raw','value'=>$model->check39($model->is_special))
            ),
        ),
    ),
)); ?>


<h4>View Accidents of this Vehicle</h4>
<?php

$config = array();

$dataProvider=new CArrayDataProvider($model->accidents,$config);
	
$this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,                
        'columns'=>array(	
		array('name'=>'Place','value'=>'$data->place'),
		array('name'=>'Date','value'=>'$data->accident_date'),	
		array('name'=>'Time','value'=>'$data->accident_time'),	
		array('name'=>'Reason','value'=>'$data->reason'),	
		array('name'=>'Damage Detail','value'=>'$data->demagedetail'),			
		//array('name'=>'Panel Lawyer','value'=>'(!isset($data->panel_lawyers->name))? "" : $data->panel_lawyers->name'),
		
    )
)); ?>

<!-- <h4>Fuel Conversion Log of this Vehicle</h4> -->
<?php
/*
$config = array();

$dataProvider=new CArrayDataProvider($model->renewals,$config);
	
$this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,                
        'columns'=>array(	
			array('name'=>'ID','value'=>'$data->id'),
			array('name'=>'Vehicle Reg No','value'=>'$data->vehicle_reg_no'),	
		//	array('name'=>'Conversion Date','value'=>'$data->conversion_date'),
		//	array('name'=>'Next Conversion Date','value'=>'$data->next_conversion_date'),
		//	array('name'=>'Converted To','value'=>'$data->conversion_in'),
		
		)
)); */ ?>

<h4>View Log of this Vehicle</h4>
<?php

$config = array('sort'=>array('defaultOrder'=>'updated_time DESC',));

$dataProvider=new CArrayDataProvider($model->vehicles_log,$config);

$data = $dataProvider->getData();

$this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,                
        'columns'=>array(	
			array('name'=>'ID','value'=>'$data->id'),
			array('name'=>'Vehicle Reg No','value'=>'$data->vehicle_reg_no'),
            array('name'=>'Model Code','value'=>'$data->model_code'),
            array('name'=>'Vehicletype','value'=>'$data->vehicletypes->type'),
			array('name'=>'Driver PIN','type'=>'raw','value'=>'$data->driver_pin'),
			array('name'=>'Status','type'=>'raw','value'=>'$data->status($data->active)'),
			array('name'=>'Updated Time','value'=>'$data->updated_time'),	
			array('name'=>'Updated By','value'=>'$data->updated_by'),
		
		)
)); ?>