<?php
/* @var $this VehiclesController */
/* @var $model Vehicles */

$this->breadcrumbs=array(
	'Vehicles'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>' Vehicles List', 'url'=>array('index')),
		array('label'=>'Manage Vehicles', 'url'=>array('admin')),
	);
}
?>

<h4>New Vehicle</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>