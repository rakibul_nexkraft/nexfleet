<?php
/* @var $this VehiclesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vehicles',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'New Vehicle', 'url'=>array('create')),
    	array('label'=>'Manage Vehicles', 'url'=>array('admin')),
    );
}
?>

<h4>Vehicles</h4>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="">
    <?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<!-- <div class="search-form" style="">
<?php  /*$this->renderPartial('_search',array(
    'model'=>$model,
)); */ ?>
</div> -->

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
 ));*/ ?>
<?php

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vehicles-grid',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
        array(
            'name' => 'reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->reg_no),array("view","reg_no"=>$data->reg_no))',
        ),
        array(
            'name' => 'vehicletype_id',
            'type'=>'raw',
            'value' => '$data->vehicletypes->type',
        ),
		'cc',
		'vmodel',
        'model_code',
		'chassis_no',
		'country',
		'purchase_date',
//		'purchase_price',
		'supplier',
		'seat_capacity',
		'load_capacity',
		'driver_pin',
		'location',
        'imei_no',
        'asset_id',
          array(                                        
                    'name' => 'actual_kpl',
                    'type'=>'raw',
                    'value' => '$data->actual_kpl==1?"Gash":$data->actual_kpl==2?"Fuel":""', 

                    
                ),
        'kpl_value',
        'tank_size',
        array(                                        
                    'name' => 'fuel_type',
                    'type'=>'raw',
                    'value' => '$data->fuelType($data->fuel_type)',                    
                    
        ),
        'user_pin',
       array(                                        
            'name' => 'mileage',
            'type'=>'raw',
            'value' => 'popDropBox($data->reg_no)',                    
            
        ),
        array(
            'name'=>'active',
            'type'=>'raw',
            'value'=>'$data->activity($data->active)'
        )
	),
));
?>
<?php
	echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Vehicles'])));
?> &nbsp; &nbsp; &nbsp;
<?php
 function popDropBox($data)
        {
       $sql = "SELECT end_meter  FROM tbl_movements where vehicle_reg_no = '$data' order by id desc limit 1 ";				
	$command = Yii::app()->db->createCommand($sql);				
	$result = $command->queryAll();
        
        return $result[0]['end_meter'];
     
        }
   function fuelType($type){
    if($type==1)return "CNG";
    if($type==2)return "Octane";
    if($type==3)return "Diesel";
    if($type==4)return "Petrol";
    if($type==5)return 'LPG';
    if($type==6)return "Others";
}
        ?>