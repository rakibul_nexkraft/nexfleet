<?php
/* @var $this VehiclesController */
/* @var $model Vehicles */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

     <div class="fl">
		<div class="row">
			<?php echo $form->label($model,'reg_no'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'reg_no',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
     </div>
     <div class="fl">

		<div class="row">
			<?php echo $form->label($model,'vehicletype_id'); ?>
			<div class="clearfix"></div>
			<?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...','class'=>'input-medium')); ?>
		</div>

     </div>
     <div class="fl">

		<div class="row">
			<?php echo $form->label($model,'engine_no'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'engine_no',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
     </div>
     <div class="fl">

		<div class="row">
			<?php echo $form->label($model,'cc'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'cc',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
     </div>
     <div class="fl">
		<div class="row">
			<?php echo $form->label($model,'vmodel'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'vmodel',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<!--div class="fl">
		<div class="row">
			<?php echo $form->label($model,'chassis_no'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'chassis_no',array('size'=>20,'maxlength'=>127)); ?>
		</div>
	</div>

	<div class="row">
		<?php echo $form->label($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_date'); ?>
		<?php echo $form->textField($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_price'); ?>
		<?php echo $form->textField($model,'purchase_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supplier'); ?>
		<?php echo $form->textField($model,'supplier',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seat_capacity'); ?>
		<?php echo $form->textField($model,'seat_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'load_capacity'); ?>
		<?php echo $form->textField($model,'load_capacity',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ac'); ?>
		<?php echo $form->textField($model,'ac'); ?>
	</div>-->
	
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'driver_pin'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'driver_pin',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	
	
	
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'location'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'location',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<!--<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>-->
    
	        <div class="fl">
	            <div class="row">
	                    <?php echo $form->label($model,'active'); ?>
	                    <div class="clearfix"></div>
	                    <?php echo $form->dropDownList($model,'active',array(1=>"Active",2=>"For Auction",3=>"Sold"),array('empty' => 'Select Status...','class'=>'input-medium')); ?>
	            </div>
	    	</div>
	   
        <div class="fl">

			<div class="row">
				<?php echo $form->label($model,'imei_no'); ?>
				<div class="clearfix"></div>
				<?php echo $form->textField($model,'imei_no',array('size'=>18,'class'=>'input-medium')); ?>
			</div>
     </div>

     <div class="fl">

		<div class="row">
			<?php echo $form->label($model,'asset_id'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'asset_id',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	 <div class="fl">
		<div class="row">
			
				<?php echo $form->labelEx($model,'actual_kpl'); ?>
				<div class="clearfix"></div>
		        <?php
		        echo $form->dropDownList($model,'actual_kpl', array("1"=>'Gas',"2"=>"Fuel"),array('empty' => 'Select Actual KPL')); ?>
					
			
		</div>
	</div>
	 <div class="fl">	
		<div class="row">
		<?php echo $form->labelEx($model,'fuel_type'); ?>
		<div class="clearfix"></div>
        <?php
        echo $form->dropDownList($model,'fuel_type', array("1"=>'CNG',"2"=>"Octane",'3'=>'Diesel','4'=>'Petrol','5'=>'LPG','6'=>'Others'),array('empty' => 'Select Fuel Type')); ?>

		
		</div>
	</div>
     </div>
<div class="clearfix"></div>
	
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

    </div> <!-- search-form -->