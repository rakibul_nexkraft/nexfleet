<?php
/* @var $this VehiclesController */
/* @var $model Vehicles */

$this->breadcrumbs=array(
	'Vehicles'=>array('index'),
	//$model->reg_no=>array('view','reg_no'=>$model->reg_no),
	$model->reg_no=>$this->createUrl('vehicles/view&amp;reg_no='.$model->reg_no),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Vehicles List', 'url'=>array('index')),
		array('label'=>'New Vehicle', 'url'=>array('create')),
		array('label'=>'View Vehicle', 'url'=>array('view', 'reg_no'=>$model->reg_no)),
		array('label'=>'Manage Vehicles', 'url'=>array('admin')),
	);
}
?>

<h4>Update Vehicle : <?php echo $model->reg_no; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>