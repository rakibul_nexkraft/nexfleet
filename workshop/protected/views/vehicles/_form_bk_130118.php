<?php
/* @var $this VehiclesController */
/* @var $model Vehicles */
/* @var $form CActiveForm */
?>

<div class="create-form form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vehicles-form',
	'enableAjaxValidation'=>false,
)); ?>


<?php
    if($model->active == 2){
?>
    <div style="font-weight:bold;">This vehicle is currently up for auction. You need to activate it to update.</div>
<br /><br /><br />
<div align="middle">
<?php
        $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Activate',
            'type' => 'primary', 
            'size' => 'medium',
            'htmlOptions' => array(
                'submit'=>array('deactivate','reg_no'=>$model->reg_no),
                'confirm'=>'Are you sure you want to activate this vehicle?',
                'csrf' => true
            )
        ));
?>
</div>

<?php
    }
    elseif($model->active == 1 || !(isset($model->active))) {
?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
<div class="container1">

	<div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

		<?php // echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'vehicletype_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'reg_no'); ?>
		<?php echo $form->textField($model,'reg_no',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'engine_no'); ?>
		<?php echo $form->textField($model,'engine_no',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'engine_no'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'cc'); ?>
		<?php echo $form->textField($model,'cc',array('maxlength'=>20)); ?>
		<?php echo $form->error($model,'cc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vmodel'); ?>
		<?php echo $form->textField($model,'vmodel',array('maxlength'=>20)); ?>
		<?php echo $form->error($model,'vmodel'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'model_code'); ?>
        <?php echo $form->textField($model,'model_code',array('maxlength'=>20)); ?>
        <?php echo $form->error($model,'model_code'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'chassis_no'); ?>
		<?php echo $form->textField($model,'chassis_no',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'chassis_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'purchase_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'purchase_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'mode'=>'datetime',
            'dateFormat' => 'yy-mm-dd', // save to db format
            'changeMonth'=>'true',
            'changeYear'=>'true',
            'yearRange'=>'2000:2020',
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
		<?php echo $form->error($model,'purchase_date'); ?>
	</div>
		
	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location'); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

</div>

    <div class="container2">
    
	<div class="row">
		<?php echo $form->labelEx($model,'purchase_price'); ?>
		<?php echo $form->textField($model,'purchase_price'); ?>
		<?php echo $form->error($model,'purchase_price'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'supplier'); ?>
		<?php echo $form->textField($model,'supplier',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'supplier'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'seat_capacity'); ?>
		<?php echo $form->textField($model,'seat_capacity'); ?>
		<?php echo $form->error($model,'seat_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'load_capacity'); ?>
		<?php echo $form->textField($model,'load_capacity',array('maxlength'=>20)); ?>
		<?php echo $form->error($model,'load_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ac'); ?>
		<?php echo $form->radioButton($model,'ac',array('value'=>'Yes','uncheckValue'=>null)); ?>Yes &nbsp; &nbsp;
		<?php echo $form->radioButton($model,'ac',array('value'=>'No','uncheckValue'=>null)); ?>No
		<?php echo $form->error($model,'ac'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
			<?php echo $form->textField($model, 'driver_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("vehicles/CallHrUser"),
	
        'success'=>"js:function(string){
                        alert(string.Fname);
                        
                    }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>
	-->
	
			<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
			<?php echo $form->textField($model, 'driver_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("drivers/getName"),
	
        'success'=>"js:function(string){        
                    $('#Vehicles_driver_name').val(string.name);
         }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name'); ?>
		<?php echo $form->error($model,'driver_name'); ?>
	</div>
	
	
			<div>
				<?php echo $form->labelEx($model,'helper_pin'); ?>
			<?php echo $form->textField($model, 'helper_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("vehicles/callHrUserForHelper"),
	
        'success'=>"js:function(string){
                        //alert(string.Fname);
                        var name = string.Fname+' '+string.Mname+' '+string.Lname;
                        $('#Vehicles_helper_name').val(name);                        
                        
                    }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'helper_pin'); ?>
	</div>
		<div class="row">
			<?php echo $form->labelEx($model,'helper_name'); ?>
			<?php echo $form->textField($model,'helper_name',array('size'=>40,'maxlength'=>40)); ?>
			<?php echo $form->error($model,'helper_name'); ?>
		</div>






	<!--div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div-->

</div>

    <div class="clearfix"></div>

	<!--div class="row buttons" style="width:100%; float:right;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php 
    }
    
    if($model->active == 3){
?>
    <div style="font-weight:bold; color:red;">This vehicle is sold.</div>
    
<?php }
    $this->endWidget();
?>

</div><!-- form -->