<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DeliveryvehiclesController */
/* @var $model Deliveryvehicles */

$this->breadcrumbs=array(
	'Deliveryvehicles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Deliveryvehicles', 'url'=>array('index'),'visible'=> in_array('List Deliveryvehicles', $user_rights)),
		array('label'=>'New Deliveryvehicle', 'url'=>array('create'),'visible'=> in_array('New Deliveryvehicle', $user_rights)),
		array('label'=>'View Deliveryvehicle', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Deliveryvehicle', $user_rights)),
		array('label'=>'Manage Deliveryvehicles', 'url'=>array('admin'),'visible'=> in_array('Manage Deliveryvehicles', $user_rights)),
	);
}
?>

<h4>Update Delivery Vehicles <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>