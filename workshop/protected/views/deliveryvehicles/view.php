<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DeliveryvehiclesController */
/* @var $model Deliveryvehicles */

$this->breadcrumbs=array(
	'Deliveryvehicles'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Deliveryvehicles', 'url'=>array('index'),'visible'=> in_array('List Deliveryvehicles', $user_rights)),
		//array('label'=>'New Deliveryvehicle', 'url'=>array('create')),
		array('label'=>'New Deliveryvehicle', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
			'type'=>'GET',
			'url'=>array('/Deliveryvehicles/create','id'=>$model->id),
			'datatype'=>'html',
			'success'=>"function(data){
				$('#popModal').modal('show');
				$('.modal-body').html(data);
				return false;
			}",
		))),'visible'=> in_array('New Deliveryvehicle', $user_rights)),
		array('label'=>'Update Deliveryvehicle', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Deliveryvehicle', $user_rights)),
		array('label'=>'Delete Deliveryvehicle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Deliveryvehicle', $user_rights)),
		array('label'=>'Manage Deliveryvehicles', 'url'=>array('admin'),'visible'=> in_array('Manage Deliveryvehicles', $user_rights)),
	);
}
?>

<h4>View Delivery Vehicles #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'defect_id',
		'vehicle_reg_no',
		'delivery_date',
		'KM_reading',
		'action_taken',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>


