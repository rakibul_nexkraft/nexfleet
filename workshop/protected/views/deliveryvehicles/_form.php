<?php
/* @var $this DeliveryvehiclesController */
/* @var $model Deliveryvehicles */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'deliveryvehicles-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="container1">
        <div class="row">
            <?php echo $form->labelEx($model,'defect_id'); ?>
            <?php echo $form->textField($model, 'defect_id',
                array('onblur'=>CHtml::ajax(array('type'=>'GET',
                    'dataType'=>'json',

                    'url'=>array("tasks/getVehicleData"),

                    'success'=>"js:function(string){
    
                        if(!string)
                        {
    
                            alert('Requisition ID not found or not approved');
                                return;
                                }
    
                                $('#Deliveryvehicles_vehicle_reg_no').val(string.vehicle_reg_no);
                                $('#Deliveryvehicles_driver_pin').val(string.driver_pin);
                                $('#Deliveryvehicles_driver_name').val(string.driver_name);
                                  }"

                ))),
                array('empty' => 'Select one of the following...')

            );
            ?>

            <?php echo $form->error($model,'defect_id'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
            <?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
            <?php echo $form->error($model,'vehicle_reg_no'); ?>
        </div>

    <!--    <div class="row">
            <?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
            <?php

            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'model'=>$model,
                'id'=>'defects_vehicle_reg_no',
                'attribute' => 'vehicle_reg_no',
                'source'=>$this->createUrl('vehicles/getRegNo'),
                // additional javascript options for the autocomplete plugin
                'options'=>array(
                    'minLength'=>'2',
                    'select'=>"js: function(event, ui) {
             // $('#Defects_vehicletype_id').val(ui.item['vehicletype_id']);
             // $('#Defects_driver_name').val(ui.item['driver_name']);
            // $('#Defects_driver_pin').val(ui.item['driver_pin']);
            // $('#Defects_last_meter').val(ui.item['start_meter']);
    
             }"
                ),
                'htmlOptions'=>array(
                    'style'=>'height:20px;'
                ),
            ));		?>


            <?php  echo $form->error($model,'vehicle_reg_no'); ?>
        </div>
    -->


        <div class="row">

            <?php echo $form->labelEx($model,'delivery_date'); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,'attribute'=>'delivery_date',
                'id'=>'defects_apply_date',
                // additional javascript options for the date picker plugin
                'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->delivery_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

                'htmlOptions'=>array('style'=>'width:206px;'
                ),
            )); ?>
            <?php echo $form->error($model,'delivery_date'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'KM_reading'); ?>
            <?php echo $form->textField($model,'KM_reading'); ?>
            <?php echo $form->error($model,'KM_reading'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'driver_pin'); ?>
            <?php echo $form->textField($model,'driver_pin'); ?>
            <?php echo $form->error($model,'driver_pin'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'driver_name'); ?>
            <?php echo $form->textField($model,'driver_name'); ?>
            <?php echo $form->error($model,'driver_name'); ?>
        </div>
    </div>
    <div class="container2">
        <div class="row">
            <?php echo $form->labelEx($model,'action_taken'); ?>
            <?php echo $form->textArea($model,'action_taken', array('rows'=>6, 'cols'=>50,'class' => 'span4')); ?>
            <?php echo $form->error($model,'action_taken'); ?>
        </div>
    </div>
    <div class="clearfix"></div>

    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->