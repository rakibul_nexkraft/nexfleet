<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssnacksController */
/* @var $model Wssnacks */

$this->breadcrumbs=array(
	'Wssnacks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Snacks', 'url'=>array('index'),'visible'=> in_array('List Snacks', $user_rights)),
	//array('label'=>'Create Wssnacks', 'url'=>array('create')),
	array('label'=>'New Snack', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/Wssnacks/create','id'=>$model->id),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
			return false;
		}",
	)),'visible'=> in_array('New Snack', $user_rights))),
	array('label'=>'Update Snack', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Snack', $user_rights)),
	array('label'=>'Delete Snack', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Snack', $user_rights)),
	array('label'=>'Manage Snacks', 'url'=>array('admin'),'visible'=> in_array('Manage Snacks', $user_rights)),
);
?>

<h4>View Wssnacks #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_name',
		'user_pin',
		'designation',
		'bill_date',
		'bill_amount',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>

