<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssnacksController */
/* @var $model Wssnacks */

$this->breadcrumbs=array(
	'Wssnacks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Snacks', 'url'=>array('index'),'visible'=> in_array('List Snacks', $user_rights)),
	array('label'=>'Manage Snacks', 'url'=>array('admin'),'visible'=> in_array('Manage Snacks', $user_rights)),
);
?>

<h4>New snack</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>