<?php
/* @var $this EmailSmsController */
/* @var $model EmailSms */

$this->breadcrumbs = array(
    'Email Sms' => array('index'),
    'Create',
);

//$this->menu=array(
//array('label'=>'List EmailSms', 'url'=>array('index')),
//array('label'=>'Manage EmailSms', 'url'=>array('admin')),
//);
?>


    <div class="center s002 ">
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                 class="floating-menu dropdown-toggle animated fadeInRight">
                <!--            <label class="floating-menu-label">Menu</label>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Email & SMS List', 'url' => array('/emailSms/index')),
                        array('label' => 'New Template', 'url' => array('/template/create')),
                        array('label' => 'Template List', 'url' => array('/template/index')),
                    ),
                ));

                ?>
            </div>
        </div>
    </div>
    <div class="col_full page_header_div" style="border-bottom: 1px solid #f2ecec;">
        <h4 class="heading-custom page_header_h4">Send New Email Or Sms</h4>
    </div>


<?php echo $this->renderPartial('_form', array('model' => $model)); ?>