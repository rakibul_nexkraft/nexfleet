<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Template
	</div>
	<div class="togglec" style="display: none;">
		<div id="template-div">
		<?php		
			$this->renderPartial('/template/template_form',array(				
				'model'=>$template,				
			));		
		?>		
		</div>
	</div>
</div>
<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Email and SMS</div>
		<div class="togglec" style="display: none;">
		<div id="email-sms-div">		
		<?php		
			$this->renderPartial('/emailSms/email_sms_form',array(
				'model'=>$emailSms,			
			));		
		?>
		</div>
	</div>
</div>





