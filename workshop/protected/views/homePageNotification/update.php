<?php
/* @var $this HomePageNotificationController */
/* @var $model HomePageNotification */

$this->breadcrumbs=array(
	'Home Page Notifications'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List HomePageNotification', 'url'=>array('index')),
	array('label'=>'Create HomePageNotification', 'url'=>array('create')),
	array('label'=>'View HomePageNotification', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage HomePageNotification', 'url'=>array('admin')),
);
?>

<h1>Update HomePageNotification <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>