<?php
/* @var $this HomePageNotificationController */
/* @var $model HomePageNotification */

$this->breadcrumbs=array(
	'Home Page Notifications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HomePageNotification', 'url'=>array('index')),
	array('label'=>'Manage HomePageNotification', 'url'=>array('admin')),
);
?>

<h1>Create HomePageNotification</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>