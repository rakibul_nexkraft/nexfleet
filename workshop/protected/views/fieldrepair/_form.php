<?php
/* @var $this FieldrepairController */
/* @var $model Fieldrepair */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fieldrepair-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<div class="container1">
    <div class="row">
        <?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
        <?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'fieldrepair_vehicle_reg_no',
            'attribute' => 'vehicle_reg_no',
            'source'=>$this->createUrl('vehicles/allRegNo1'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {             
         $('#Fieldrepair_vehicletype_id').val(ui.item['vehicletype_id']);
         $('#Fieldrepair_driver_name').val(ui.item['driver_name']);
         $('#Fieldrepair_driver_pin').val(ui.item['driver_pin']);
         $('#Fieldrepair_last_meter').val(ui.item['start_meter']);
		 
         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));		?>


        <?php  echo $form->error($model,'vehicle_reg_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'vehicletype_id'); ?>
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

        <?php // echo $form->textField($model,'vehicletype_id'); ?>
        <?php echo $form->error($model,'vehicletype_id'); ?>
    </div>

    <!--  <div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>
        <?php echo $form->textField($model,'driver_pin'); ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div> -->

    <div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>

        <?php echo $form->textField($model, 'driver_pin',
            array('onblur'=>CHtml::ajax(array('type'=>'GET',
                'dataType'=>'json',

                'url'=>array("fieldrepair/getName"),

                'success'=>"js:function(string){
                    $('#Fieldrepair_driver_name').val(string.name);
         }"

            ))),
            array('empty' => 'Select one of the following...')

        );
        ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'driver_name'); ?>
        <?php echo $form->textField($model,'driver_name'); ?>
        <?php echo $form->error($model,'driver_name'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'last_meter'); ?>
        <?php echo $form->textField($model,'last_meter',array('readonly'=>true)); ?>
        <label id="lblDiff" style=color:red;></label>
        <?php echo $form->error($model,'last_meter'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'meter_reading'); ?>
        <?php // echo $form->textField($model,'meter_reading',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->textField($model,'meter_reading',
            array('onblur'=>

                "js:
            var meterDiff = $('#Fieldrepair_meter_reading').val()-$('#Fieldrepair_last_meter').val();
            if(meterDiff <= '0'){alert('End Meter can\'t be less or equal to Start Meter');return false;}
            document.getElementById('lblDiff').innerHTML='Meter Difference: '+meterDiff"
            )
        ); ?>
        <?php echo $form->error($model,'meter_reading'); ?>
    </div>

    <div class="row">

        <?php echo $form->labelEx($model,'apply_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,
            'attribute'=>'apply_date',
            'id'=>'fieldrepair_apply_date',
            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->apply_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:206px;'
            ),
        )); ?>
        <?php echo $form->error($model,'apply_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'fieldrepair_description'); ?>
        <?php echo $form->textArea($model,'fieldrepair_description',array('rows'=>3,'cols'=>20)); ?>
        <?php echo $form->error($model,'fieldrepair_description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'onsite_name'); ?>
        <?php echo $form->textField($model,'onsite_name',array('size'=>60,'maxlength'=>255)); ?>
        <?php echo $form->error($model,'onsite_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'approve_by'); ?>
        <?php echo $form->textField($model,'approve_by',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'approve_by'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'program_name'); ?>
        <?php echo $form->textField($model,'program_name',array('size'=>60,'maxlength'=>512)); ?>
        <?php echo $form->error($model,'program_name'); ?>
    </div>
</div>


<div class="container2">

    <div class="row">
        <?php echo $form->labelEx($model,'spare_amount'); ?>
        <?php echo $form->textField($model,'spare_amount'); ?>
        <?php echo $form->error($model,'spare_amount'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'spare_description'); ?>
        <?php echo $form->textArea($model,'spare_description',array('rows'=>3,'cols'=>20,'maxlength'=>512)); ?>
        <?php echo $form->error($model,'spare_description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'labor_amount'); ?>
        <?php echo $form->textField($model,'labor_amount'); ?>
        <?php echo $form->error($model,'labor_amount'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'labor_description'); ?>
        <?php echo $form->textArea($model,'labor_description',array('rows'=>3,'cols'=>20,'maxlength'=>512)); ?>
        <?php echo $form->error($model,'labor_description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'machine_shop_amount'); ?>
        <?php echo $form->textField($model,'machine_shop_amount'); ?>
        <?php echo $form->error($model,'machine_shop_amount'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'machine_shop_description'); ?>
        <?php echo $form->textArea($model,'machine_shop_description',array('rows'=>3,'cols'=>20,'maxlength'=>512)); ?>
        <?php echo $form->error($model,'machine_shop_description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'vehicle_towchain_amount'); ?>
        <?php echo $form->textField($model,'vehicle_towchain_amount'); ?>
        <?php echo $form->error($model,'vehicle_towchain_amount'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'vehicle_towchain_description'); ?>
        <?php echo $form->textArea($model,'vehicle_towchain_description',array('rows'=>3,'cols'=>20,'maxlength'=>512)); ?>
        <?php echo $form->error($model,'vehicle_towchain_description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'remarks'); ?>
        <?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'remarks'); ?>
    </div>

<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'created_by'); ?>
<!--        --><?php //echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
<!--        --><?php //echo $form->error($model,'created_by'); ?>
<!--    </div>-->
<!---->
<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'created_time'); ?>
<!--        --><?php //echo $form->textField($model,'created_time'); ?>
<!--        --><?php //echo $form->error($model,'created_time'); ?>
<!--    </div>-->
<!---->
<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'updated_by'); ?>
<!--        --><?php //echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
<!--        --><?php //echo $form->error($model,'updated_by'); ?>
<!--    </div>-->
<!---->
<!--    <div class="row">-->
<!--        --><?php //echo $form->labelEx($model,'updated_time'); ?>
<!--        --><?php //echo $form->textField($model,'updated_time'); ?>
<!--        --><?php //echo $form->error($model,'updated_time'); ?>
<!--    </div>-->
</div>
<div class="clearfix"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<!--	<div class="row buttons">-->
<!--		--><?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
<!--	</div>-->

<?php $this->endWidget(); ?>

</div><!-- form -->