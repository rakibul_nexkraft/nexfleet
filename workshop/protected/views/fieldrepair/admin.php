<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this FieldrepairController */
/* @var $model Fieldrepair */

$this->breadcrumbs=array(
	'Fieldrepairs'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Field Repair', 'url'=>array('index'),'visible'=> in_array('List Field Repair', $user_rights)),
		array('label'=>'Create Field Repair', 'url'=>array('create'),'visible'=> in_array('Create Field Repair', $user_rights)),
	);
}

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('fieldrepair-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h4>Manage Fieldrepairs</h4>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'fieldrepair-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'vehicle_reg_no',
						'vehicletype_id',
//		'driver_name',
						'driver_pin',
//		'last_meter',
						'meter_reading',
						'fieldrepair_description',
						'apply_date',
		/*


		'onsite_name',
		'approve_by',
		'program_name',
		'spare_amount',
		'spare_description',
		'labor_amount',
		'labor_description',
		'machine_shop_amount',
		'machine_shop_description',
		'vehicle_towchain_amount',
		'vehicle_towchain_description',
		'remarks',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
