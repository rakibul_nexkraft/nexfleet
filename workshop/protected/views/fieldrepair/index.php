<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this FieldrepairController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fieldrepairs',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    //	array('label'=>'Create Fieldrepair', 'url'=>array('create')),
        array('label'=>'Create Field Repair', 'url'=>array('fieldrepair/create','id'=>$model->id),'visible'=> in_array('Create Field Repair', $user_rights)),
        array('label'=>'Manage Field Repair', 'url'=>array('admin'),'visible'=> in_array('Manage Field Repair', $user_rights)),
    //    array('label'=>'Pending List', 'url'=>array('pending')),
    );
}
?>

<h4>Field Repairs</h4>

<div class="search-form">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div>
<div id="def_container">
    <?php


    $var = $_REQUEST['Fieldrepair'];

    if($var):
        ?>



        <h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
        <?php if($var['apply_date']) echo "User: ".$var['apply_date']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
        <?php if($var['vehicle_reg_no'])echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

        <?php

        if($var['active']==0);
        else if($var['active']==1)$active= "Not Approved";
        else if($var['active']==2)$active= "Approved";
        if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
    <?php endif;?>







<?php //$this->widget('zii.widgets.CListView', array(
//	'dataProvider'=>$dataProvider,
//	'itemView'=>'_view',
//)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'defects-grid',
    'dataProvider'=>$dataProvider,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        array(
            'name'=>'vehicle_reg_no',
            'type'=>'raw',
            'value'=>'CHtml::link(CHtml::encode($data->vehicle_reg_no),array("view","id"=>$data->id))',
        ),
//        'vehicle_reg_no',
        //    'vehicletype_id',
        array(
            'name' => 'vehicletype_id',
            'type'=>'raw',
            'value' => '$data->vehicletypes->type',
        ),
//        'driver_name',
        'driver_pin',
        'last_meter',
        'meter_reading',
        'fieldrepair_description',
        'apply_date',
//        'onsite_name',
//        'program_name',
        'remarks',
    ),

)); ?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Fieldrepair'])));
?> &nbsp; &nbsp;
