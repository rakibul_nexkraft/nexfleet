<?php
/* @var $this FieldrepairController */
/* @var $data Fieldrepair */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicletype_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicletype_id); ?>
	<br />

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('driver_name')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->driver_name); ?>
<!--	<br />-->

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_meter')); ?>:</b>
	<?php echo CHtml::encode($data->last_meter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_reading')); ?>:</b>
	<?php echo CHtml::encode($data->meter_reading); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('apply_date')); ?>:</b>
    <?php echo CHtml::encode($data->apply_date); ?>
    <br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('apply_date')); ?>:</b>
	<?php echo CHtml::encode($data->apply_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fieldrepair_description')); ?>:</b>
	<?php echo CHtml::encode($data->fieldrepair_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('onsite_name')); ?>:</b>
	<?php echo CHtml::encode($data->onsite_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approve_by')); ?>:</b>
	<?php echo CHtml::encode($data->approve_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('program_name')); ?>:</b>
	<?php echo CHtml::encode($data->program_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spare_amount')); ?>:</b>
	<?php echo CHtml::encode($data->spare_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('spare_description')); ?>:</b>
	<?php echo CHtml::encode($data->spare_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('labor_amount')); ?>:</b>
	<?php echo CHtml::encode($data->labor_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('labor_description')); ?>:</b>
	<?php echo CHtml::encode($data->labor_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('machine_shop_amount')); ?>:</b>
	<?php echo CHtml::encode($data->machine_shop_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('machine_shop_description')); ?>:</b>
	<?php echo CHtml::encode($data->machine_shop_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_towchain_amount')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_towchain_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_towchain_description')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_towchain_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	*/ ?>

</div>