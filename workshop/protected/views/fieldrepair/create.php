<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this FieldrepairController */
/* @var $model Fieldrepair */

$this->breadcrumbs=array(
	'Fieldrepairs'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Field Repair', 'url'=>array('index'),'visible'=> in_array('List Field Repair', $user_rights)),
		array('label'=>'Manage Field Repair', 'url'=>array('admin'),'visible'=> in_array('Manage Field Repair', $user_rights)),
	);
}
?>

<h4>Create Field Repair</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>