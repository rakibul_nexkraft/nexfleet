<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WstyresController */
/* @var $model Wstyres */

$this->breadcrumbs=array(
	'Wstyres'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Tyres', 'url'=>array('index'),'visible'=> in_array('List Tyres', $user_rights)),
	//array('label'=>'New Tyre', 'url'=>array('create')),
	 /*array('label'=>'New Tyre', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/wstyres/create','id'=>$model->id),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
			return false;
		}",
	)))),*/
	array('label'=>'Update Tyre', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Tyre', $user_rights)),
	array('label'=>'Delete Tyre', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Tyre', $user_rights)),
	array('label'=>'Manage Tyres', 'url'=>array('admin'),'visible'=> in_array('Manage Tyres', $user_rights)),
	array('label'=>'Back', 'url'=>Yii::app()->request->urlReferrer)
);
?>

<h4>View Tyre #<?php echo $model->id; ?></h4>

<?php 
/*
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        'defect_id',
		'vehicle_reg_no',
		//'vehicle_type',
		array('name'=>'vehicle_type', 'value'=>$model->vehicletypes->type),
		'purchase_date',
		'requisition_no',
        'requisition_date',
        'particulars',
		'km_reading',
		'supplier',
		'challan_no',
        'challan_date',
        'quantity',
		'amount',
		'warrenty_km',
		'warrenty_valid_km_upto',
		'actual_use_km',
		'status',
        'remarks',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); 
*/?>
<?php 

$this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'group2'=>array(
			'ItemColumns' => 2,
			'attributes' => array(
				'id',				array('name'=>'vehicle_type', 'value'=>$model->vehicletypes->type),
				'defect_id',	'vehicle_reg_no',
				'purchase_date',	'requisition_no',
				'requisition_date',	'particulars',
				'supplier','km_reading',
				'challan_no',	'warrenty_km',	
				'challan_date','warrenty_valid_km_upto',			
				'quantity', 'actual_use_km',
				'unit_price','bill_date',
				'total_price','bill_no',
				'amount','status',
				'tyre_no','tyre_size',
				
				'remarks',
				
				'updated_by',
				'created_by',		'updated_time',
				'created_time',
				array('name'=>'Go To:',
					'type'=>'raw',
					'value'=>CHtml::link("Back",Yii::app()->request->urlReferrer),
				),
				
			),
		),
	),
)); ?>
<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  

?>
