<?php
/* @var $this WstyresController */
/* @var $model Wstyres */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>'',
	'id'=>'wstyres-form',
	'enableAjaxValidation'=>false,
    //'htmlOptions'=>array('onsubmit'=>"return tyreFormSubmit(this)"),//
)); 
//var_dump($model);
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    
	<?php 
    if(isset($erro) && !empty($erro)){ ?>
    <div class="errorSummary">
        <p>Please fix the following input errors:</p>
        <ul>
        <?php foreach ($erro as $key => $value) {
   

           echo "<li>".$value[0]."</li>";
        }?>
    </ul>
    </div>
    <?php }    ?>
    
    <?php echo $form->errorSummary($model); ?>
    
	<div class="containerForm">

<div class="row">
    <?php echo $form->labelEx($model,'defect_id'); ?>
    <?php echo $form->textField($model, 'defect_id',
        array('onblur'=>CHtml::ajax(array('type'=>'GET',
            'dataType'=>'json',

            'url'=>array("tasks/getVehicleData"),

            'success'=>"js:function(string){

					if(!string)
					{

						alert('Defect ID not found or not approved');
							return;
							}

							$('#Wstyres_vehicle_reg_no').val(string.vehicle_reg_no);
							$('#Wstyres_vehicle_type').val(string.vehicletype_id);
									  }"

        ))),
        array('empty' => 'Select one of the following...')

    );
    ?>

    <?php echo $form->error($model,'defect_id'); ?>
</div>

	 <div class="row">
		<?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php 	
		
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {        
         $('#Wstyres_vehicle_type').val(ui.item['vehicletype_id']);    
         }"        
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));		?>
		
		
		<?php  echo $form->error($model,'vehicle_reg_no'); ?>
	  </div> 
	
<div class="row">
		<?php echo $form->labelEx($model,'vehicle_type'); ?>
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicle_type', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

		<?php // echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'vehicle_type'); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'purchase_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'purchase_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->purchase_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'purchase_date'); ?>
    </div>

</div>
<div class="containerForm">

	<div class="row">
		<?php echo $form->labelEx($model,'requisition_no'); ?>
		<?php echo $form->textField($model,'requisition_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'requisition_no'); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'requisition_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'requisition_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->requisition_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'requisition_date'); ?>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,'particulars'); ?>
    <?php echo $form->textField($model,'particulars'); ?>
    <?php echo $form->error($model,'particulars'); ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'tyre_size'); ?>
    <?php echo $form->textField($model,'tyre_size'); ?>
    <?php echo $form->error($model,'tyre_size'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'km_reading'); ?>
        <?php echo $form->textField($model,'km_reading'); ?>
        <?php echo $form->error($model,'km_reading'); ?>
    </div>    
</div>
    <div class="containerForm">        

 <div class="row">
        <?php  echo $form->labelEx($model,'supplier'); ?>
        <?php           
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'id'=>'supplier',
        'model'=>$model,
        'attribute' => 'supplier',   
    'source'=>$this->createUrl('wssupplier/getSupplierName'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {                 
         }"        
    ),
    /*'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),*/
));     ?>
        <?php  echo $form->error($model,'supplier'); ?>
      </div> 
      <div class="row">
        <?php echo $form->labelEx($model,'challan_no'); ?>
        <?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'challan_no'); ?>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,'challan_date'); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'challan_date',

        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->challan_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
    <?php echo $form->error($model,'challan_date'); ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'bill_no'); ?>
    <?php echo $form->textField($model,'bill_no'); ?>
    <?php echo $form->error($model,'bill_no'); ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'bill_date'); ?>
    <?php  $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'bill_date',

        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->bill_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); 

    ?>
    <?php echo $form->error($model,'bill_date'); ?>
    </div>
    </div>
	<div class="containerForm">

	

    <div class="row">
    <?php echo $form->labelEx($model,'quantity'); ?>
    <?php  echo $form->textField($model,'quantity',array('id'=>'tyre_quntity','onkeyup'=>'totaPriceCount()'));?>
    <?php echo $form->error($model,'quantity'); ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'unit_price'); ?>
    <?php  echo $form->textField($model,'unit_price',array('id'=>'tyre_unit_price','onkeyup'=>'totaPriceCount()'));?>
    <?php echo $form->error($model,'unit_price'); ?>
    </div>
    <div class="row">
    <?php echo $form->labelEx($model,'total_price'); ?>
    <?php echo $form->textField($model,'total_price',array('id'=>'tyre_total_price'));?>
    <?php echo $form->error($model,'total_price'); ?>
    </div>
    <script>
        function totaPriceCount(){
          var qunt=document.getElementById('tyre_quntity').value;
          var unitP=document.getElementById('tyre_unit_price').value;
          //document.getElementById('tyre_total_price').value=(qunt*unitP).toFixed(2);
          document.getElementById('tyre_total_price').value=qunt*unitP;
        }
    </script>
	<div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php if($model->id) echo $form->textField($model,'amount'); 
		else  echo $form->textField($model,'amount'); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'warrenty_km'); ?>
		<?php echo $form->textField($model,'warrenty_km'); ?>
		<?php echo $form->error($model,'warrenty_km'); ?>
	</div>
 </div>
 <div class="containerForm">
	<div class="row">
		<?php echo $form->labelEx($model,'warrenty_valid_km_upto'); ?>
		<?php // echo $form->textField($model,'warrenty_valid_km_upto'); ?>
        <?php echo $form->textField($model,'warrenty_valid_km_upto', array('onfocus'=>'tot_km()')); ?>
		<?php echo $form->error($model,'warrenty_valid_km_upto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actual_use_km'); ?>
		<?php echo $form->textField($model,'actual_use_km'); ?>
		<?php echo $form->error($model,'actual_use_km'); ?>
	</div>


    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php //echo $form->textField($model,'licensetype',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->dropDownList($model, 'status', $remarkstype); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'tyre_no'); ?>
        <?php echo $form->textField($model,'tyre_no'); ?>
        <?php echo $form->error($model,'tyre_no'); ?>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,'remarks'); ?>
    <?php echo $form->textField($model,'remarks'); ?>
    <?php echo $form->error($model,'remarks'); ?>
    </div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
-->
</div>
    <div class="clear"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium',
            'htmlOptions' => array(
            'form' => 'wstyres-form',
        ),
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

<script>
    function tot_km()
    {

        //$('#Wstyres_warrenty_valid_km_upto').val($('#Wstyres_km_reading').val() + ($('#Wstyres_warrenty_km').val()));
     //   $('#Wstyres_warrenty_valid_km_upto').val((($('#Wstyres_km_reading').val()))+($('#Wstyres_warrenty_km').val()));
    }
    
</script>
<script type="text/javascript">
    function tyreFormSubmit(value){
        alert(value);
       var formData = new FormData(value);
       alert(formData);
       $.post('wstyres/create',{Wstyres:formData},function(data){alert('ok');});
        //$.ajax({
        //type: 'POST',
         //url: '<?php echo Yii::app()->createAbsoluteUrl("wstyres/create"); ?>',
        //data:formData,
        //success:function(data){
                //$('#content').html(data);
             //},
       // error: function(data) { // if error occured
             //alert("Error occured.please try again");
            //alert(data);
         //},

       // dataType:'html',

       // });
alert('ss');

        return false;
        
   }
</script>

</div><!-- form -->