<?php
/* @var $this WstyresController */
/* @var $model Wstyres */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search-form'
)); ?>
    <!---
	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_type'); ?>
		<?php echo $form->textField($model,'vehicle_type',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_date'); ?>
		<?php echo $form->textField($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_no'); ?>
		<?php echo $form->textField($model,'requisition_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'km_reading'); ?>
		<?php echo $form->textField($model,'km_reading'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supplier'); ?>
		<?php echo $form->textField($model,'supplier',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'challan_no'); ?>
		<?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'amount'); ?>
		<?php echo $form->textField($model,'amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warrenty_km'); ?>
		<?php echo $form->textField($model,'warrenty_km'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warrenty_valid_km_upto'); ?>
		<?php echo $form->textField($model,'warrenty_valid_km_upto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'actual_use_km'); ?>
		<?php echo $form->textField($model,'actual_use_km'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>-->

    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
        	<div class="row">
				<?php echo $form->label($model,'vehicle_reg_no'); ?><br />
				<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127,'id'=>'vehicle_reg_no')); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'requisition_no'); ?><br />
				<?php echo $form->textField($model,'requisition_no',array('size'=>60,'maxlength'=>127)); ?>
			</div>
		</div>		
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'supplier'); ?><br />
				<?php echo $form->textField($model,'supplier',array('size'=>60,'maxlength'=>127)); ?>
			</div>
		</div>
		<div class="fl">
            <div class="row">
                <?php echo $form->label($model,'from_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'from_date',  // name of post parameter
                    //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'to_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'to_date',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        //	'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->to_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </fieldset>

</div>
<div class="clearfix"></div>

<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit',
        'size' => 'medium',
        'htmlOptions' => array(
            'form'=>'search-form'
        ),
        
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->