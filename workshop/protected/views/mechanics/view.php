<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this MechanicsController */
/* @var $model Mechanics */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Mechanics', 'url'=>array('index'),'visible'=> in_array('List Mechanics', $user_rights)),
		array('label'=>'Create Mechanics', 'url'=>array('create'),'visible'=> in_array('Create Mechanics', $user_rights)),
		array('label'=>'Update Mechanics', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Mechanics', $user_rights)),
		array('label'=>'Delete Mechanics', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Mechanics', $user_rights)),
		array('label'=>'Manage Mechanics', 'url'=>array('admin'),'visible'=> in_array('Manage Mechanics', $user_rights)),
	);
}
?>

<h1>View Mechanics #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name_pin',
		'active',
	),
)); ?>
