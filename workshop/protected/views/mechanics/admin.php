<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this MechanicsController */
/* @var $model Mechanics */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Mechanics', 'url'=>array('index'),'visible'=> in_array('List Mechanics', $user_rights)),
		array('label'=>'Create Mechanics', 'url'=>array('create'),'visible'=> in_array('Create Mechanics', $user_rights)),
		array('label'=>'Mechanics Works', 'url'=>array('workList'),'visible'=> in_array('Mechanics Works', $user_rights)),
	);
}
Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('mechanics-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h1>Manage Mechanics</h1>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'mechanics-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'name_pin',
						'active',
						array(
							'class'=>'CButtonColumn',
						),
					),
				)); ?>
