<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this MechanicsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mechanics',
);
if(!Yii::app()->user->isViewUser()) {
  $this->menu=array(
  	array('label'=>'Create Mechanics', 'url'=>array('create'),'visible'=> in_array('Create Mechanics', $user_rights)),
  	array('label'=>'Manage Mechanics', 'url'=>array('admin'),'visible'=> in_array('Manage Mechanics', $user_rights)),
    array('label'=>'Mechanics Works', 'url'=>array('workList'),'visible'=> in_array('Mechanics Works', $user_rights)),
  );
}
?>

<h1>Mechanics</h1>

<?php //$this->widget('zii.widgets.CListView', array(
	//'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',
//)); ?>
<?php 


$this->widget('bootstrap.widgets.TbGridView', array(
 'type'=>'striped bordered condensed',
 'id'=>'machanic-grid',
 'dataProvider'=>$dataProvider,
 'filter'=>$model,

 'columns'=>array(
        //'id',
   array(
    'name' => 'id',
    'type'=>'raw',
    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
  ),
   
   'name_pin',
   'active',   
   
 ),
)); ?>
