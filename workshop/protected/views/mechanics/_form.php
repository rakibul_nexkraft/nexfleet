<?php
/* @var $this MechanicsController */
/* @var $model Mechanics */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mechanics-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name_pin'); ?>
		<?php echo $form->textField($model,'name_pin',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'name_pin'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>-->
	<div class="row">
        <?php echo $form->labelEx($model,'active'); ?>
        <?php //echo $form->textField($model,'licensetype',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->dropDownList($model, 'active', array('Not Active'=>'Not Active','Active'=>'Active')); ?>
        <?php echo $form->error($model,'active'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->