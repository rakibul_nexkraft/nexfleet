<?php
/* @var $this DefectsController */
/* @var $data Defects */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicletype_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicletype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_reading')); ?>:</b>
	<?php echo CHtml::encode($data->meter_reading); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apply_date')); ?>:</b>
	<?php echo CHtml::encode($data->apply_date); ?>
	<br />

<!--	<b>--><?php //echo CHtml::encode($data->getAttributeLabel('mechanic_name')); ?><!--:</b>-->
<!--	--><?php //echo CHtml::encode($data->mechanic_name); ?>
<!--	<br />-->

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('mechanic_pin')); ?>:</b>
	<?php echo CHtml::encode($data->mechanic_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('task_id')); ?>:</b>
	<?php echo CHtml::encode($data->task_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	*/ ?>

</div>