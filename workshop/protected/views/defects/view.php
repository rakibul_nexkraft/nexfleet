<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DefectsController */
/* @var $model Defects */

$this->breadcrumbs=array(
	'Defects'=>array('index'),
	$model->id,
);
if(Yii::app()->user->username!='185036' && !Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Defects', 'url'=>array('index'), 'visible'=> in_array('List Defects', $user_rights)),
		array('label'=>'New Defects', 'url'=>'#', 'visible'=> in_array('New Defects', $user_rights), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
			'type'=>'GET',
			'url'=>array('create','redirectUrl'=>Yii::app()->controller->action->id),
			'datatype'=>'html',
			'success'=>"function(data){
				$('#popModal').modal();
				$('.modal-body').html(data);
				return false;
			}",
		)))),
		array('label'=>'Update Defects', 'url'=>array('update', 'id'=>$model->id), 'visible'=> in_array('Update Defects', $user_rights)),
		array('label'=>'Delete Defects', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'), 'visible'=> in_array('Delete Defects', $user_rights)),
		array('label'=>'Manage Defects', 'url'=>array('admin'), 'visible'=> in_array('Manage Defects', $user_rights)),	
		array('label'=>'Add Inside Task', 'url'=>'#', 'visible'=> in_array('Add Inside Task', $user_rights), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
			'type'=>'GET',
			'url'=>array('/tasks/create','id'=>$model->id,'type'=>'inside'),
			'datatype'=>'html',
			'success'=>"function(data){
				$('#popModal').modal('show');
				$('.modal-body').html(data);
				return false;
			}",
		)))),
		array('label'=>'Add Outside Task', 'url'=>'#', 'visible'=> in_array('Add Outside Task', $user_rights), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
			'type'=>'GET',
			'url'=>array('/tasks/create','id'=>$model->id,'type'=>'outside'),
			'datatype'=>'html',
			'success'=>"function(data){
				$('#popModal').modal();
				$('.modal-body').html(data);
				return false;
			}",
		)))),
		array('label'=>'Add Other Task', 'url'=>'#', 'visible'=> in_array('Add Other Task', $user_rights), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
			'type'=>'GET',
			'url'=>array('/tasks/create','id'=>$model->id,'type'=>'other'),
			'datatype'=>'html',
			'success'=>"function(data){
				$('#popModal').modal();
				$('.modal-body').html(data);
				return false;
			}",
		)))),
    //array('label'=>'Issue Item',   'url'=>array('/wsitemdists/create','id'=>$model->id)),

		array('label'=>'Issue Item', 'url'=>'#', 'visible'=> in_array('Issue Item', $user_rights), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
			'type'=>'GET',
			'url'=>array('/wsitemdists/create','id'=>$model->id),
			'datatype'=>'html',
			'success'=>"function(data){
				$('#popModal').modal('show');
				$('.modal-body').html(data);
				$('#popModal').css('z-index','1050');
				$('.modal-backdrop').css('z-index','1000');
				return false;

			//$('#popModal').modal();
			//$('.modal-body').html(data);
			//return false;
			}",
		)))),


   // array('label'=>'Issue Item', 'url'=>array('wsitemdists/create','id'=>$model->id)),
	);
}
?>

<h4>View Defects #<?php echo $model->id; ?></h4>




<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		//'vehicletype_id',
		array('name'=>'vehicletype_id', 'value'=>$model->vehicletypes->type),
		'driver_name',
		'driver_pin',
		'last_meter',
		'meter_reading',
		'apply_date',
//		'defect_description',
//		'mechanic_name',
		//'mechanic_pin',
		'total_amount',
		'edt',
		'remarks',
        //'created_by',
		array('name'=>'created by','value'=>$user_profile->firstname ),
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>

<?php

/*$criteria=new CDbCriteria(array(       
	'condition' =>'id= $model->id',
));
*/
//$config = array();
//$dataProvider=new CArrayDataProvider($model->tasks,$config);


?>

<br/>

<h4>
	Defect Detail
</h4>

<?php
//$dataProvider=new CActiveDataProvider($model->defectid,$config);
$dataProvider=new CArrayDataProvider($model->defectdetail,$config);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'defectdetail-grid',
	'dataProvider'=>$dataProvider,
    //'filter'=>$model,
	'columns'=>array(
		array('name'=>'Job Name','value'=>'$data->job_name'),
//        array('name'=>'Action Taken','value'=>'$data->action_name'),
		array('name'=>'Mechanic Name','value'=>'$data->mechanic_name'),
	),
));

?>



<?php
/*
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tasks-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'defect_id',
		//'task_desc',
		'created_by',
		'created_time',
		'updated_by',

		//'updated_time',

		array(
			'class'=>'CButtonColumn',
		),
	),
));
*/
?>
<h4>
	Tasks
</h4>
<?php

$config = array();

$dataProvider=new CArrayDataProvider($model->tasks,$config);

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>  'CHtml::link($data->id, array("tasks/view","id"=>$data->id))',
		),
		array('name'=>'Service Name','value'=>'$data->service_name'),
		array('name'=>'Work Type','value'=>'$data->work_type'),
		array('name'=>'Quantity','value'=>'$data->quantity'),
		array('name'=>'Bill No','value'=>'$data->bill_no'),
		array('name'=>'Bill Amount','value'=>'$data->bill_amount'),
  //      array('name'=>'Task Description','value'=>'$data->task_desc'),
		array('name'=>'Created By','value'=>'$data->created_by'),
		

	)
));


?>

<h4>
	Spare/Material Issued
</h4>


<?php

$dataProvider=new CArrayDataProvider($model->wsitemdists,$config);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'wsitemdists-grid',
	'dataProvider'=>$dataProvider,
	//'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>  'CHtml::link($data->id, array("wsitemdists/view","id"=>$data->id))',
		),
		array('name'=>'Item Name','value'=>'$data->item_name'),
        //array('name'=>'Item Quantity ','value'=>'$data->available_stock'),
		array('name'=>'Item Size','value'=>'$data->item_size'),
		array('name'=>'Issue Quantity','value'=>'$data->quantity'),
		array('name'=>'Unit Price','value'=>'$data->unit_price'),
		array('name'=>'Total Price','value'=>'$data->total_price'),
		array('name'=>'Issue Date','value'=>'$data->issue_date'),
		array('name'=>'Issue To Name','value'=>'$data->mechanic_name'),
		
		
	//	'id',
//		'defect_id',		
//		'Item Name',
//		'Purchase Date',
//		'Quantity',
	//	'Unit Price',
/*		array(
			'class'=>'CButtonColumn',
		),*/
	),
));

?>

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();?>

<script type="text/javascript">
	$.get('<?php echo Yii::app()->createAbsoluteUrl("wsitemdists/create");?>',{id:'<?php echo $_GET['id'];?>',iditemdists:'<?php echo $_GET['iditemdists'];?>'},function(data){$('#popModal').modal('show');
		$('.modal-body').html(data);
		$('#popModal').css("z-index","999");
		$('.modal-backdrop').css("z-index","990");
		return false;});
	</script>

<?php  endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:82%; height:70%; padding:1px; vertical-align:middle; left:25% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>
