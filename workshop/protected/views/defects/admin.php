<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DefectsController */
/* @var $model Defects */

$this->breadcrumbs=array(
	'Defects'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Defects', 'url'=>array('index'), 'visible'=> in_array('List Defects', $user_rights)),
		array('label'=>'New Defects', 'url'=>array('create'), 'visible'=> in_array('New Defects', $user_rights)),
	);
}
Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('defects-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h4>Manage Defects</h4>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('bootstrap.widgets.TbGridView', array(
					'type'=>'striped bordered condensed',
					'id'=>'defects-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'vehicle_reg_no',
						'vehicletype_id',
						'driver_name',
						'driver_pin',
						'meter_reading',
						'apply_date',
		/*
		'mechanic_name',
		'mechanic_pin',
		'task_id',
		'created_by',
		'created_time',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update} {delete}',
			'buttons'=>array
			(
				'delete'=>array(
					'visible'=>'Yii::app()->user->username=="admin"',
				),
			),
		),
	),
)); ?>
