<?php
/* @var $this DefectsController */
/* @var $model Defects */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
       'id'=>'defects-form',
       'enableAjaxValidation'=>false,
   )); ?>

   <p class="note">Fields with <span class="required">*</span> are required.</p>

   <?php echo $form->errorSummary($model); ?>


   <div class="container1">

       <div class="row">
          <?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
          <?php

          $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
              'model'=>$model,
              'id'=>'defects_vehicle_reg_no',
              'attribute' => 'vehicle_reg_no',
              'source'=>$this->createUrl('vehicles/allRegNo1'),
    // additional javascript options for the autocomplete plugin
              'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {             
                   $('#Defects_vehicletype_id').val(ui.item['vehicletype_id']);
                   $('#Defects_driver_name').val(ui.item['driver_name']);
                   $('#Defects_driver_pin').val(ui.item['driver_pin']);
                   $('#Defects_last_meter').val(ui.item['start_meter']);
                   
               }"
           ),
              'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));		?>


        <?php  echo $form->error($model,'vehicle_reg_no'); ?>
    </div>
    <div class="row">
      <?php echo $form->labelEx($model,'vehicletype_id'); ?>
      <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
      echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

      <?php // echo $form->textField($model,'vehicletype_id'); ?>
      <?php echo $form->error($model,'vehicletype_id'); ?>
  </div>

  <!--  <div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>
        <?php echo $form->textField($model,'driver_pin'); ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div> -->

    <div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>

        <?php echo $form->textField($model, 'driver_pin',
            array('onblur'=>CHtml::ajax(array('type'=>'GET',
                'dataType'=>'json',

                'url'=>array("defects/getName"),

                'success'=>"js:function(string){
                    $('#Defects_driver_name').val(string.name);
                }"

            ))),
            array('empty' => 'Select one of the following...')

        );
        ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'driver_name'); ?>
        <?php echo $form->textField($model,'driver_name'); ?>
        <?php echo $form->error($model,'driver_name'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'last_meter'); ?>
        <?php echo $form->textField($model,'last_meter',array('readonly'=>true)); ?>
        <label id="lblDiff" style=color:red;></label>
        <?php echo $form->error($model,'last_meter'); ?>
    </div>

    <div class="row">
      <?php echo $form->labelEx($model,'meter_reading'); ?>
      <?php // echo $form->textField($model,'meter_reading',array('size'=>60,'maxlength'=>127)); ?>
      <?php echo $form->textField($model,'meter_reading',
        array('onblur'=>

            "js:
            var meterDiff = $('#Defects_meter_reading').val()-$('#Defects_last_meter').val();
            if(meterDiff <= '0'){alert('End Meter can\'t be less or equal to Start Meter');return false;}
            document.getElementById('lblDiff').innerHTML='Meter Difference: '+meterDiff"
        )
    ); ?>
    <?php echo $form->error($model,'meter_reading'); ?>
</div>

<div class="row">

    <?php echo $form->labelEx($model,'apply_date'); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'apply_date',
        'id'=>'defects_apply_date',
            // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->apply_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:206px;'
    ),
)); ?>
<?php echo $form->error($model,'apply_date'); ?>
</div>

</div>
<div class="container2">

    <div class="row">
        <?php echo $form->labelEx($model,'edt'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'edt',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->edt,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
    <?php echo $form->error($model,'edt'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'remarks'); ?>
    <?php echo $form->textField($model,'remarks'); ?>
    <?php echo $form->error($model,'remarks'); ?>
</div>

<!--    <div class="row">-->
    <!--        --><?php //echo $form->labelEx($model,'defect_description'); ?>
    <!--        --><?php //echo $form->textArea($model,'defect_description',array('rows'=>10,'cols'=>20)); ?>
    <!--        --><?php //echo $form->error($model,'defect_description'); ?>
    <!--    </div>-->



    <!--<div class="row">
        < ?php echo $form->labelEx($model,'mechanic_pin'); ?>
        < ?php echo $form->textField($model,'mechanic_pin'); ?>
        < ?php echo $form->error($model,'mechanic_pin'); ?>
    </div>

    <div class="row">
        < ?php echo $form->labelEx($model,'mechanic_pin'); ?>
        < ?php echo $form->textField($model, 'mechanic_pin',
            array('onblur'=>CHtml::ajax(array('type'=>'GET',
                'dataType'=>'json',

                'url'=>array("vehicles/CallHrUser"),

                'success'=>"js:function(string){
                        //alert(string.Fname);
                        var name = string.Fname+' '+string.Mname+' '+string.Lname;
                        $('#Defects_mechanic_name').val(name);
                   }"

            ))),
            array('empty' => 'Select one of the following...')

        );
        ?>

        < ?php echo $form->error($model,'mechanic_pin'); ?>
    </div>
-->
<!--    <div class="row">-->
    <!--        --><?php //echo $form->labelEx($model,'mechanic_name'); ?>
    <!--        --><?php ////echo $form->textField($model,'mechanic_name',array('size'=>60,'maxlength'=>127)); ?>
    <!---->
    <!--        --><?php
//        $data= CHtml::listData(Mechanics::model()->findAll(),'name_pin', 'name_pin');
//        $this->widget('ext.EchMultiSelect.EchMultiSelect', array(
//            //    $this->widget('ext.echmultiselect.echmultiselect', array(
//            'model' => $model,
//            'dropDownAttribute' => 'mechanic_name',
//            'data' => $data,
//            // 'dropDownHtmlOptions'=> array(
//            // 'style'=>'width:378px;',
//            // ),
//            'dropDownHtmlOptions'=> array(
//                'class'=>'span-10',
//                'id'=>'defects',
//            )
//        ));
//        ?>
<!---->
<!--        --><?php //echo $form->error($model,'mechanic_name'); ?>
<!--    </div>-->

<br/>
<br/>
<?php if (!empty($model->id)) : ?>
    <div class="row">
        <div>
            <button onclick="resetSubmitDetail()" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#detailModal">Add Detail</button>
        </div>
    </div>
<?php endif;?>
<div class="row">
    <div id="detailTable" <?php if (is_null($model->id)) : ?> class="hide" <?php endif;?> >
        <table class="table table-bordered table-hover" id="defectDetailTable" style="width: 600px">
            <thead>
                <tr>
                    <!--                    <th>ID</th>-->
                    <th style="width: 50%">Job Name</th>
                    <!--                    <th style="width: 35%">Action Taken</th>-->
                    <th style="width: 30%">Mechanic Name</th>
                    <th style="width: 20%"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $defect = $model->id;
                if (!is_null($defect)):
                    $sql = "SELECT dd.id, dd.defect_id, dd.job_name, dd.mechanic_id, dd.mechanic_name FROM tbl_defectdetail as dd LEFT JOIN tbl_defects as defects on dd.defect_id=defects.id where defects.id=$defect ORDER BY dd.job_name";
                    $command = Yii::app()->db->createCommand($sql);
                    $results = $command->queryAll();
                    $jobName=array();
                    foreach($results as $result):?>
                        <tr class="<?php echo 'row-'. $result['id']; ?>">
                            <!--                            <td>--><?php //echo $result['id']; ?><!--</td>-->                
                            <?php if(in_array($result['job_name'],$jobName)){ ?>
                             <td style="border-top:0px;">
                             <?php }
                             else { ?>
                              <td>
                                <?php echo $jobName[]=$result['job_name']; } ?>
                            </td>
                            <!--                            <td>--><?php //echo $result['action_name']; ?><!--</td>-->
                            <td data-mechanicid="<?php echo $result['mechanic_id']; ?>"><?php echo $result['mechanic_name']; ?></td>
                            <td>
                             <button data-rowid="<?php echo 'row-'. $result['id']; ?>" onclick="editDefectDetail(this)" type="button" class="btn btn-small btn-primary" data-toggle="modal" data-target="#detailModal">Edit </button>
                             <button data-rowid="<?php echo 'row-'. $result['id']; ?>" onclick="deleteDefectDetail(this)" type="button" class="btn btn-small btn-danger" data-toggle="modal" data-target="#deleteModal">Delete</button>
                         </td>
                     </tr>
                 <?php endforeach;?>
             <?php endif; ?>

         </tbody>
     </table>
 </div>

</div>

</div>
<div class="clearfix"></div>
	<!--<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?> &nbsp;&nbsp;&nbsp;
		<?php //echo CHtml::button('close',array('onclick'=>'$(this).parents("#popModal").hide(400); $(".modal-backdrop").remove();')); ?>
	</div> -->

    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<div id="detailModal" class="modal fade hide" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Defect Description</h4>
        </div>
        <div class="modal-body">
            <form role="form-horizontal">
                <input type="hidden" name="defectId" id="defectId" value="<?php echo $model->id ?>" />
                <div class="control-group">
                    <label style="font-weight: bold" class="control-label" for="jobName">Job Name</label>
                    <div class="controls">
                        <input type="text" class="input-xxlarge" name="jobName" id="jobName" placeholder="Enter job Name"/>
                    </div>
                    <span style="margin-top: 2px" id="errorJobName"></span>
                </div>
                <!--                <div class="control-group">-->
                    <!--                    <label style="font-weight: bold" class="control-label" for="actionName">Action Taken</label>-->
                    <!--                    <div class="controls">-->
                        <!--                        <textarea name="actionName" class="input-xxlarge" name="actionName" id="actionName" cols="3" rows="3" placeholder="Enter action name"></textarea>-->
                        <!--                    </div>-->
                        <!--                    <span  style="margin-top: 2px" id="errorActionName"></span>-->
                        <!--                </div>-->
                        <div class="control-group">
                            <label style="font-weight: bold" class="control-label" for="inputMessage">Mechanic Name</label>
                            <div class="controls">
                                <select class="span3" name="mechanicId" id="mechanicId" >
                                    <option value="">Choose an option</option>
                                    <?php
                                    $mechanics = CHtml::listData(Mechanics::model()->findAll(array('select'=>'id, name_pin', 'order'=>'id ASC')), 'id',  'name_pin');
                                    foreach ($mechanics as $mechanic_id => $mechanic_name){
                                        echo "<option value='$mechanic_id'>".$mechanic_name."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <span  style="margin-top: 2px" id="errorMechanicId"></span>
                        </div>
                        <input type="hidden" name="mechanicName" id="mechanicName" value="" />
                    </form>
                </div>
                <div class="modal-footer">
                    <input name="submitCreate" id="submitCreate" type="button" class="btn btn-primary btn-create-detail" data-dismiss="modal" value="Create" />
                    <input name="submitUpdate" id="submitUpdate" type="button" class="btn btn-primary btn-edit-detail hide" data-dismiss="modal" value="Update" />
                </div>
            </div>
        </div>

        <div id="deleteModal" class="modal fade hide" role="dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <h4>Are you sure you want to delete?</h4>
                    <input type="hidden" name="removeModalDetailId" id="removeModalDetailId" value="" />
                    <br />

                </div>
                <div class="modal-footer">
                    <button onclick="confirmDeleteDefectDetail()" type="button" class="btn btn-danger" data-dismiss="modal">Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function(){
                $("#submitCreate").click(function () {
                    if (validateForm()){
                        submitDetailForm();
                    } else {
                        return false;
                    }
                });
                $("#submitUpdate").click(function () {
                    var obj = $("#submitUpdate")[0];
                    if (validateForm()){
                        editDetailForm(obj);
                    } else {
                        return false;
                    }
                });
            });
            $('#mechanicId').change(function(){
                var name = $('#mechanicId option:selected').text();
                $("#mechanicName").val(name);
            });
            function saveDetailForm(detailObj){
                $.ajax({
                    type:'POST',
                    url:'<?php echo Yii::app()->createUrl("defectdetail/submitDetail");?>',
                    data:detailObj,
                    dataType:'json',
                    success: function(data){
                        var trHtml = '';
                        if (detailObj.id){
                            var element = $(".row-"+data.id)[0];
                            $(element).find('td:nth-child(1)').text(data.job_name);
//                    $(element).find('td:nth-child(2)').text(data.action_name);
$(element).find('td:nth-child(2)').data(data.mechanic_id);
$(element).find('td:nth-child(2)').text(data.mechanic_name);

}else {
    if (data){
        trHtml +=
        '<tr class="row-'+data.id+'">' +
        '<td>'+data.job_name+'</td>' +
//                            '<td>'+data.action_name+'</td>'+
'<td data-mechanicid="'+data.mechanic_id+'">'+data.mechanic_name+'</td>' +
'<td>'+
'<button data-rowid="row-'+data.id+'" onclick="editDefectDetail(this)" type="button" class="btn btn-small btn-primary" data-toggle="modal" data-target="#detailModal">Edit</button> '+
'<button data-rowid="row-'+data.id+'" onclick="deleteDefectDetail(this)" type="button" class="btn btn-small btn-danger" data-toggle="modal" data-target="#deleteModal">Delete</button>'+
'</td>'+
'</tr>';
$("#defectDetailTable").append(trHtml);
}
}
}
});
            }
            function editDetailForm(obj){
                var detailId = $(obj).data('detailid');
                var detailObj = getDetailData();
                if (detailId){
                    detailObj.id = detailId
                }
                saveDetailForm(detailObj);
            }
            function submitDetailForm(){
                var detailObj = getDetailData();
                saveDetailForm(detailObj);
            }
            function getDetailData(){
                var defect_id = $('#defectId').val();
                var job_name = $('#jobName').val();
//        var action_name = $('#actionName').val();
var mechanic_id = $('#mechanicId').val();
var mechanic_name = $('#mechanicName').val();
var detailObj = {
    defect_id: defect_id,
    job_name: job_name,
//            action_name: action_name,
mechanic_id: mechanic_id,
mechanic_name: mechanic_name
};
return detailObj;
}
function resetSubmitDetail(){
    $('#jobName').val('');
//        $('#actionName').val('');
$('#mechanicId').val('');
$('.btn-create-detail').removeClass('hide');
$('.btn-edit-detail').addClass('hide');
$("#errorJobName").html("");
//        $("#errorActionName").html("");
$("#errorMechanicId").html("");
}
function editDefectDetail(obj){
    $("#errorJobName").html("");
//        $("#errorActionName").html("");
$("#errorMechanicId").html("");
$('.btn-edit-detail').removeClass('hide');
$('.btn-create-detail').addClass('hide');
var rowId = $(obj).data('rowid');
var element = $(obj).closest('.'+rowId);
$('#jobName').val($(element).find('td:nth-child(1)').text());
//        $('#actionName').val($(element).find('td:nth-child(2)').text());
$('#mechanicId').val($(element).find('td:nth-child(2)').data('mechanicid'));
$('#mechanicName').val($(element).find('td:nth-child(2)').text());
$('.btn-edit-detail').data('detailid', rowId.split('-')[1]);
}
function deleteDefectDetail(obj){
    var rowId = $(obj).data('rowid');
    var detailId = rowId.split('-')[1];
    $('#removeModalDetailId').val(detailId);
}
function confirmDeleteDefectDetail(){
    var detailId = $('#removeModalDetailId').val();
    $.ajax({
        type:'POST',
        url:'<?php echo Yii::app()->createUrl("defectdetail/deleteDetail");?>',
        data:{detailId:detailId},
        success: function(resp){
            var data = $.parseJSON(resp);
            var element = $(".row-"+data)[0];
            $(element).remove()
        }
    });
}
function validateForm(){
    var jobName = $("#jobName").val();
//        var actionName = $("#actionName").val();
var mechanicId = $('#mechanicId option:selected').val();

var isValid = true;

if (jobName == ''){
    isValid = false;
    $("#errorJobName").html("<span style='color: red;font-size: 0.9em;' id='errorJobName'>Job name cannot be empty</span>");
}else {
    $("#errorJobName").html("");
}
//        if (actionName == ''){
//            isValid = false;
//            $("#errorActionName").html("<span style='color: red;font-size: 0.9em;' id='errorActionName'>Action taken cannot be empty</span>");
//        }else {
//            $("#errorActionName").html("");
//        }
if (mechanicId == ''){
    isValid = false;
    $("#errorMechanicId").html("<span style='color: red;font-size: 0.9em;' id='errorMechanicId'>Mechanic must be selected</span>");
}else {
    $("#errorMechanicId").html("");
}
if (isValid == true){
    return true;
} else {
    return false;
}
}
</script>