<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DefectsController */
/* @var $dataProvider CActiveDataProvider */
Yii::import('application.controllers.DefectdetailController');

$this->breadcrumbs=array(
	'Defects',
);

if(Yii::app()->user->username!='185036' && !Yii::app()->user->isViewUser()) {
    $this->menu=array(

       /* array('label'=>'New Defect', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
            'type'=>'GET',
            'url'=>array('/Defects/create','id'=>$model->id),
            'datatype'=>'html',
            'success'=>"function(data){
    			$('#popModal').modal('show');
    			$('.modal-body').html(data);
    			return false;
    		}",
        )))),*/
        array('label'=>'New Defect', 'url'=>array('defects/create','id'=>$model->id), 'visible'=> in_array('New Defect', $user_rights)),
        array('label'=>'Manage Defects', 'url'=>array('admin'), 'visible'=> in_array('Manage Defects', $user_rights)),
        array('label'=>'Pending List', 'url'=>array('pending'), 'visible'=> in_array('Pending List', $user_rights)),
    );
    
}
?>

<h4>Defects</h4>

<div class="search-form">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div>
<div id="def_container">
    <?php


    $var = $_REQUEST['Defects'];

    if($var):
        ?>



        <h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
        <?php if($var['apply_date']) echo "User: ".$var['apply_date']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
        <?php if($var['vehicle_reg_no'])echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

        <?php

        if($var['active']==0);
        else if($var['active']==1)$active= "Not Approved";
        else if($var['active']==2)$active= "Approved";
        if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
    <?php endif;?>









<?php /* $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */ ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
 'type'=>'striped bordered condensed',
 'id'=>'defects-grid',
 'dataProvider'=>$dataProvider,

 'columns'=>array(
        //'id',
    array(
        'name' => 'id',
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
    ),
    'vehicle_reg_no',
    //    'vehicletype_id',
    array(
        'name' => 'vehicletype_id',
        'type'=>'raw',
        'value' => '$data->vehicletypes->type',
    ),
    'driver_name',
    'driver_pin',
    'last_meter',
    'meter_reading',
    'total_amount',
    'apply_date',
//        Yii::app()->clientScript->registerScript(
//                "jQuery.ajax({
//                    type:'GET',
//                    url:'createUrl(\"defectdetail/getDefectDescription\")'
//                });"
//        ),
//        array('name'=>'defectdetail.job_name',  'value'=>'$data->defectdetail->job_name'),   // student id
//        array('name'=>'defectdetail.action_taken',  'value'=>'$data->defectdetail->action_name'),   // student name
    array(
        'name' => 'defect_description',
        'type'  => 'raw',
        'value' => function($data, $row){
            if (!empty($data->defect_description)){
                return ($data->defect_description.", ".$data->getAllDefectsByDefectId($data->id));
            }else{
                return $data->getAllDefectsByDefectId($data->id);
            }
        },
    ),
    array(
        'name' => 'mechanic_name',
        'type'  => 'raw',
        'value' => function($data, $row){
            if (!empty($data->mechanic_name)){
                return ($data->mechanic_name.", ".$data->getAllMechanicsByDefectId($data->id));
            }else{
                return $data->getAllMechanicsByDefectId($data->id);
            }
        },
    ),
//        'mechanic_name',
//        'defect_description',
    'remarks',
      //  'mechanic_pin',
        /*
          'mechanic_name',
          'mechanic_pin',
          'task_id',
          'created_by',
          'created_time',
          */
          /*
           'link'=>array(
                        'header'=>'',
                        //'title'=>'Create Jobcard',
                        'type'=>'raw',
                        'value'=> 'CHtml::button("New Jobcard",array("onclick"=>"document.location.href=\'".Yii::app()->controller->createUrl("jobcards/create",array("id"=>$data->id,"vehicle_reg_no"=>$data->vehicle_reg_no,"vehicletype_id"=>$data->vehicletype_id))."\'"))',
                ),
                */
               // array(
                //        'class'=>'CButtonColumn',
               // ),

            ),

        )); ?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Defects'],'param'=>'excelindex')));
?> &nbsp; &nbsp; &nbsp;

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
  <a class="close" data-dismiss="modal">&times;</a>
  <?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:700px; height:500px; left:45% !important;'),
)); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>