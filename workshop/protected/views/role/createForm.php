<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create User
	</div>
	<div class="togglec" style="display: none;">
		<div id="user-div">
			<?php		
			$this->renderPartial('application.modules.user.views.admin._form',array(				
					'model'=>$model,
					'profile'=>$profile,			
			));		
			?>		
		</div>
	</div>
</div>

<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Role
	</div>
	<div class="togglec" style="display: none;">
		<div id="role-div">
			<?php		
			$this->renderPartial('/role/_form',array(				
				'model'=>$role,				
			));		
			?>		
		</div>
	</div>
</div>

<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Rights
	</div>
	<div class="togglec" style="display: none;">
		<div id="rights-div">
			<?php		
			$this->renderPartial('/rights/_form',array(				
				'model'=>$rights,				
			));		
			?>		
		</div>
	</div>
</div>





