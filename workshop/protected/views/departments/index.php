<?php
/* @var $this DepartmentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Departments',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Department', 'url'=>array('create')),
		array('label'=>'Manage Departments', 'url'=>array('admin')),
	);
}
?>

<h4>Departments</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'departments-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
			'name' => 'name',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->name), array("view", "id"=>$data->id))',
		),
		//'name',
		'program',
		'ceated_by',
		'created_time',
		//'active',
	),
)); ?>