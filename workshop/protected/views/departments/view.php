<?php
/* @var $this DepartmentsController */
/* @var $model Departments */

$this->breadcrumbs=array(
	'Departments'=>array('index'),
	$model->name,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Departments List', 'url'=>array('index')),
		array('label'=>'New Department', 'url'=>array('create')),
		array('label'=>'Update Department', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Department', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Departments', 'url'=>array('admin')),
	);
}
?>

<h4>View Department : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'program',
		'ceated_by',
		'created_time',
		//'active',
	),
)); ?>
