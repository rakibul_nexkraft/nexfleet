<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsoutservicesController */
/* @var $model Wsoutservices */

$this->breadcrumbs=array(
	'Wsoutservices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Service List - Outside', 'url'=>array('index'),'visible'=> in_array('Service List - Outside', $user_rights)),
	array('label'=>'New Service - Outside', 'url'=>array('create'),'visible'=> in_array('New Service - Outside', $user_rights)),
	array('label'=>'View Service - Outside', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Service - Outside', $user_rights)),
	array('label'=>'Manage Service List - Outside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Outside', $user_rights)),
);
?>

<h1>Update Service - Outside <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>