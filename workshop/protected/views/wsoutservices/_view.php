<?php
/* @var $this WsoutservicesController */
/* @var $data Wsoutservices */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_name')); ?>:</b>
	<?php echo CHtml::encode($data->service_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('service_category')); ?>:</b>
	<?php echo CHtml::encode($data->service_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car')); ?>:</b>
	<?php echo CHtml::encode($data->car); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('micro_jeep')); ?>:</b>
	<?php echo CHtml::encode($data->micro_jeep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pickup')); ?>:</b>
	<?php echo CHtml::encode($data->pickup); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bus_coaster')); ?>:</b>
	<?php echo CHtml::encode($data->bus_coaster); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	*/ ?>

</div>