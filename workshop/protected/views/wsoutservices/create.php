<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsoutservicesController */
/* @var $model Wsoutservices */

$this->breadcrumbs=array(
	'Wsoutservices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Service List - Outside', 'url'=>array('index'),'visible'=> in_array('Service List - Outside', $user_rights)),
	array('label'=>'Manage Service List - Outside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Outside', $user_rights)),
);
?>

<h4>New Service - Outside</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>