<?php
/* @var $this VehicletypesController */
/* @var $model Vehicletypes */

$this->breadcrumbs=array(
	'Vehicle Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Vehicle Types List', 'url'=>array('index')),
	array('label'=>'New Vehicle Type', 'url'=>array('create')),
	array('label'=>'View Vehicle Type', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Vehicle Types', 'url'=>array('admin')),
);
}
?>

<h4>Update Vehicle Type : <?php echo $model->id; ?></h4>

<?php //echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php echo $this->renderPartial('_formU', array('model'=>$model)); ?>