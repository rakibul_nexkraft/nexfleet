<?php
/* @var $this VehicletypesController */
/* @var $model Vehicletypes */

$this->breadcrumbs=array(
	'Vehicle Types'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Vehicle Types List', 'url'=>array('index')),
	array('label'=>'Manage Vehicle Types', 'url'=>array('admin')),
);
}
?>

<h4>New Vehicle Type</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>