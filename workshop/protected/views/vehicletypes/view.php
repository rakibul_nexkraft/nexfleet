<?php
/* @var $this VehicletypesController */
/* @var $model Vehicletypes */

$this->breadcrumbs=array(
	'Vehicle Types'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Vehicle Types List', 'url'=>array('index')),
	array('label'=>'New Vehicle Type', 'url'=>array('create')),
	array('label'=>'Update Vehicle Type', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Vehicletype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
	array('label'=>'Manage Vehicle Types', 'url'=>array('admin')),
);
}
?>

<h4>View Vehicle Type : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'rate_per_km',
		'rate_per_km_personal',
		'rate_per_km_external',
		//'active',
	),
)); ?>
