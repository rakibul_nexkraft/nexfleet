<?php
/* @var $this RightsController */
/* @var $model Rights */

$this->breadcrumbs=array(
	'Rights'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Rights', 'url'=>array('index')),
	array('label'=>'Create Rights', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('rights-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Rights</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rights-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'rights_name',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
