<?php
/* @var $this RightsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rights',
);

$this->menu=array(
	array('label'=>'Create Rights', 'url'=>array('create')),
	array('label'=>'Manage Rights', 'url'=>array('admin')),
);
?>

<h1>Rights</h1>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,

        'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'rights_name',
        'created_by',
        'created_time',
        'updated_by',
        'updated_time'
        
    ),
)); 
?>

<!-- <?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?> -->
