<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */

$this->breadcrumbs=array(
	'Item Distributions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Item Distributions', 'url'=>array('index'),'visible'=> in_array('List Item Distributions', $user_rights)),
	array('label'=>'New Item Distribution', 'url'=>array('create'),'visible'=> in_array('New Item Distribution', $user_rights)),
	array('label'=>'Inventory of Spares', 'url'=>array('sapare'),'visible'=> in_array('Inventory of Spares', $user_rights)),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('wsitemdists-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h4>Manage Item Distributions</h4>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'wsitemdists-grid',
					'dataProvider'=>$model->search1(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'defect_id',
		//'task_id',
						'wsitemname_id',
						'item_name',
						'quantity',
						'item_size',
		/*
		'item_slno',
		'bill_no',
		'bill_date',
		'unit_price',
		'total_price',
		'active',
		'created_by',
		'created_time',
		'updated_by',
		'update_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
