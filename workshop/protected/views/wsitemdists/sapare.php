<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemdistsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Item Distributions',
);

$this->menu=array(
	array('label'=>'New Item Distribution', 'url'=>array('create'),'visible'=> in_array('New Item Distribution', $user_rights)),
    array('label'=>'Manage Item Distributions', 'url'=>array('admin'),'visible'=> in_array('Manage Item Distributions', $user_rights)),
    array('label'=>'Inventory of Spares', 'url'=>array('sapare'),'visible'=> in_array('Inventory of Spares', $user_rights)),
);
?>

<h4>Item Distributions</h4>
<div class="search-form">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div>
<div id=req_container>
    <?php

 /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
*/





?>

<?php
if(isset($_GET['Wsitemdists'])){
   $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'wsitemdists-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),

        //'defect_id',
        //array(
           // 'name' => 'vehicle_reg_no',
           // 'type'=>'raw',
            //'value' => '$data->defects->vehicle_reg_no',
        //),

        
      //  'wsrequisition_id',
      //  'wsitemname_id',
        'item_name',
        'vehicle_model',         
        'parts_no', 
        'vehicle_reg_no',
        array(
            'header' => 'Total Purchase Qty',               
            'value'=>'$data->getTotalPurchase($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model)',
        ),
        array(
            'header' => 'Date Wise Purchase Qty',  
                //'value'=>'$_GET["Wsitemdists"]["bill_from_date"]',             
            'value'=>'$data->getDateWisePurchase($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model,$_GET["Wsitemdists"]["bill_from_date"],$_GET["Wsitemdists"]["bill_to_date"])',
        ),
        array(
            'header' => 'Total Issue Qty',               
            'value'=>'$data->getTotalIssue($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model)',
        ),
        array(
            'header' => 'Date Wise Issue Qty',               
            'value'=>'$data->getDateWiseIssue($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model,$_GET["Wsitemdists"]["issue_from_date"],$_GET["Wsitemdists"]["issue_to_date"])',
        ),
        //'bill_no',
       // 'bill_date',
       // 'issue_date',
        
        
        //'item_size',
        'available_stock',
        'unit_price',
        //'quantity',       
        //'mechanic_pin',
        //mechanic_name',
        array(
            'name'=>'total_price',
            'footer'=>'Grand Total: ' . $model->getTotal($model->search()->getData(), 'total_price'),
        ),
        array(
            'header' => 'Consumtion Rate',               
            'value'=>'$data->getConsumtionRate($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model,$_GET["Wsitemdists"]["bill_from_date"],$_GET["Wsitemdists"]["bill_to_date"],$_GET["Wsitemdists"]["issue_from_date"],$_GET["Wsitemdists"]["issue_to_date"])',
        ),
/*
        'created_by',
        'created_time',
        'updated_by',
        'updated_time',
*/
    ),
)); } ?>
</div>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Wsitemdists'])));
?> &nbsp; &nbsp; &nbsp;