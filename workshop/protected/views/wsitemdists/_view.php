<?php
/* @var $this WsitemdistsController */
/* @var $data Wsitemdists */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<!--<b><?php // echo CHtml::encode($data->getAttributeLabel('task_id')); ?>:</b>
	<?php // echo CHtml::encode($data->task_id); ?>
	<br /> -->
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('defect_id')); ?>:</b>
	<?php echo CHtml::encode($data->defect_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wsitemname_id')); ?>:</b>
	<?php echo CHtml::encode($data->wsitemname_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_name')); ?>:</b>
	<?php echo CHtml::encode($data->item_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_size')); ?>:</b>
	<?php echo CHtml::encode($data->item_size); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_slno')); ?>:</b>
	<?php echo CHtml::encode($data->item_slno); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('bill_no')); ?>:</b>
	<?php echo CHtml::encode($data->bill_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bill_date')); ?>:</b>
	<?php echo CHtml::encode($data->bill_date); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('total_price')); ?>:</b>
	<?php echo CHtml::encode($data->total_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	*/ ?>

</div>