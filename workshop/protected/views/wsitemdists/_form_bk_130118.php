<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsitemdists-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="container1">
<!--	<div class="row">
		<?php echo $form->labelEx($model,'defect_id'); ?>
		
		<?php
	if(!$_REQUEST['id'])
	{
		?>
		
		
        <?php $task_id = Defects::model()->findAll(array('select'=>'id,id','order' => 'id ASC'));
        echo $form->dropDownList($model,'defect_id', CHtml::listData($defect_id,'id',  'id'),array('empty' => 'Select Defects...')); ?>

		<?php // echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'defect_id'); ?>	
	<?php
}
	?>
		<?php
	if($_REQUEST['id'])
	echo $form->textField($model,'defect_id',array('size'=>60,'maxlength'=>127,'value'=>$_REQUEST['id'])); 
	?>
		<?php //echo $form->textField($model,'task_id'); ?>
		<?php echo $form->error($model,'defect_id'); ?>
	</div>
-->

<div class="row">
    <?php echo $form->labelEx($model,'defect_id'); ?>
    <?php echo $form->textField($model, 'defect_id',
        array('onblur'=>CHtml::ajax(array('type'=>'GET',
            'dataType'=>'json',

            'url'=>array("tasks/getVehicleData"),

            'success'=>"js:function(string){

					if(!string)
					{

						alert('Requisition ID not found or not approved');
							return;
							}

						//	$('#Wsitemdists_vehicle_reg_no').val(string.vehicle_reg_no);
						//	$('#Wsitemdists_vehicletype_id').val(string.vehicletype_id);
									  }"

        ))),
        array('empty' => 'Select one of the following...')

    );
    ?>

    <?php echo $form->error($model,'defect_id'); ?>
</div>

    <div class="row">
		<?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'Wsitemdists_vehicle_reg_no',
            'attribute' => 'vehicle_reg_no',


            'source'=>$this->createUrl('vehicles/allRegNo1'),

            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {

         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));		?>
		
		
		<?php  echo $form->error($model,'vehicle_reg_no'); ?>
	  </div>

<div class="row">
    <?php echo $form->labelEx($model,'vehicle_model'); ?>
    <?php //echo $form->textField($model,'vehicle_model',array('size'=>20,'maxlength'=>127)); ?>

    <?php

    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'model'=>$model,
        'id'=>'Wsitemdists_vehicle_model',
        'attribute' => 'vehicle_model',
        'source'=>$this->createUrl('wsitemdists/getVehicleModel'),
        // additional javascript options for the autocomplete plugin
        'options'=>array(
            'minLength'=>'2',
            'select'=>"js: function(event, ui) {
         //$('#Defects_vehicletype_id').val(ui.item['vehicletype_id']);


         }"
        ),
        'htmlOptions'=>array(
            'style'=>'height:20px;'
        ),
    ));		?>
    <?php echo $form->error($model,'vehicle_model'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'parts_no'); ?>
    <?php // echo $form->textField($model,'parts_no',array('size'=>20,'maxlength'=>127)); ?>



    <?php

    $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'model'=>$model,
        'attribute' => 'parts_no',
        //'source'=>$this->createUrl('wsitemnames/getItemName'),
//            'source'=>$this->createUrl('wsitemdists/getItemNameStock'),
        'source'=>'js: function(request, response) {
    $.ajax({
        url: "'.$this->createUrl('wsitemdists/getPartNo').'",
        dataType: "json",
        data: {
            term: request.term,
            vehicle_model: $("#Wsitemdists_vehicle_model").val(),

        },
        success: function (data) {
                response(data);
        }
    })
 }',

        // additional javascript options for the autocomplete plugin
        'options'=>array(
            'minLength'=>'1',
            'select'=>"js: function(event, ui) {
    //  $('#Wsitemdists_wsitemname_id').val(ui.item['wsitemname_id']);

         }"
        ),
        /*'htmlOptions'=>array(
            'style'=>'height:20px;'
        ),*/
    ));		?>


    <?php echo $form->error($model,'parts_no'); ?>
</div>



</div>
<div class="container2">


		<div class="row">
		<?php echo $form->labelEx($model,'item_name'); ?>
		<?php 	
		
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			'model'=>$model,
            'attribute' => 'item_name',
			//'source'=>$this->createUrl('wsitemnames/getItemName'),
//            'source'=>$this->createUrl('wsitemdists/getItemNameStock'),
            'source'=>'js: function(request, response) {
    $.ajax({
        url: "'.$this->createUrl('wsitemdists/getItemNameStock').'",
        dataType: "json",
        data: {
            term: request.term,

            vehicle_reg_no: $("#Wsitemdists_vehicle_reg_no").val(),
            vehicle_model: $("#Wsitemdists_vehicle_model").val(),
            parts_no: $("#Wsitemdists_parts_no").val()


             //brand: $("#Ws_brand_id").val()
        },
        success: function (data) {
                response(data);
        }
    })
 }',

            // additional javascript options for the autocomplete plugin
			'options'=>array(
				'minLength'=>'1',
        'select'=>"js: function(event, ui) {             
      $('#Wsitemdists_wsitemname_id').val(ui.item['wsitemname_id']); 
      $('#Wsitemdists_vehicle_model').val(ui.item['vehicle_model']);
      $('#Wsitemdists_available_stock').val(ui.item['available_stock']);
      $('#Wsitemdists_wsitem_id').val(ui.item['wsitem_id']);
      //$('#Wsitemdists_unit_price').val(ui.item['unit_price']);
         }" 
    ),
    /*'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),*/
));		?>
		<?php  echo $form->error($model,'item_name'); ?>
	</div> 

	<div class="row">
		<?php echo $form->labelEx($model,'wsitemname_id'); ?>
		<?php echo $form->textField($model,'wsitemname_id',array('readonly'=>true)); ?>
		<?php echo $form->error($model,'wsitemname_id'); ?>
	</div>

  <!--  <div class="row">
    <?php echo $form->labelEx($model,'available_Stock'); ?>
    <?php echo $form->textField($model,'available_stock',array('readonly'=>true)); ?>
    <?php echo $form->error($model,'available_stock'); ?>
    </div> -->

    <div class="row">
        <?php echo $form->labelEx($model,'wsitem_id'); ?>
        <?php echo $form->textField($model,'wsitem_id',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'wsitem_id'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>
	
	 
	 <?php echo $form->labelEx($model,'unit'); ?>
	 <?php echo $form->dropDownList($model, 'unit', $unitlist); ?>
	    
	    <?php echo $form->error($model,'Unit');

    ?>

    <div class="row">
        <?php echo $form->labelEx($model,'issue_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'issue_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->issue_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'issue_date'); ?>
    </div>

<div class="row">
    <?php echo $form->labelEx($model,'mechanic_pin'); ?>
    <?php echo $form->textField($model, 'mechanic_pin',
        array('onblur'=>CHtml::ajax(array('type'=>'GET',
            'dataType'=>'json',

            'url'=>array("vehicles/CallHrUser"),

            'success'=>"js:function(string){
                        //alert(string.Fname);
                        var name = string.Fname+' '+string.Mname+' '+string.Lname;
                        $('#Wsitemdists_mechanic_name').val(name);
                   }"

        ))),
        array('empty' => 'Select one of the following...')

    );
    ?>

    <?php echo $form->error($model,'mechanic_pin'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'mechanic_name'); ?>
    <?php echo $form->textField($model,'mechanic_name',array('size'=>60,'maxlength'=>127)); ?>
    <?php echo $form->error($model,'mechanic_name'); ?>
</div>

<!--
	<div class="row">
		<?php echo $form->labelEx($model,'item_size'); ?>
		<?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'item_size'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_slno'); ?>
		<?php echo $form->textField($model,'item_slno',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'item_slno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bill_no'); ?>
		<?php echo $form->textField($model,'bill_no'); ?>
		<?php echo $form->error($model,'bill_no'); ?>
	</div>

   <div class="row">
    <?php echo $form->labelEx($model,'bill_date'); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'bill_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->bill_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
    <?php echo $form->error($model,'bill_date'); ?>
</div>
-->
    <!--


        <div class="row">
            <?php echo $form->labelEx($model,'total_price'); ?>
            <?php echo $form->textField($model,'total_price'); ?>
            <?php echo $form->error($model,'total_price'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'active'); ?>
            <?php echo $form->textField($model,'active'); ?>
            <?php echo $form->error($model,'active'); ?>
        </div>

       

	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
-->
</div>
    <div class="clear"></div>
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit',
        'size' => 'medium'

    ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->