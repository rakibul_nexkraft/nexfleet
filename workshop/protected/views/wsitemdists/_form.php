<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsitemdists-form',
	'enableAjaxValidation'=>false,
   'htmlOptions'=>array('onsubmit'=>"return myCheck(this)")
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="containerForm">
<!--	<div class="row">
		<?php echo $form->labelEx($model,'defect_id'); ?>
		
		<?php
	if(!$_REQUEST['id'])
	{
		?>
		
		
        <?php $task_id = Defects::model()->findAll(array('select'=>'id,id','order' => 'id ASC'));
        echo $form->dropDownList($model,'defect_id', CHtml::listData($defect_id,'id',  'id'),array('empty' => 'Select Defects...')); ?>

		<?php // echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'defect_id'); ?>	
	<?php
}
	?>
		<?php
	if($_REQUEST['id'])
	echo $form->textField($model,'defect_id',array('size'=>60,'maxlength'=>127,'value'=>$_REQUEST['id'])); 
	?>
		<?php //echo $form->textField($model,'task_id'); ?>
		<?php echo $form->error($model,'defect_id'); ?>
	</div>
-->

<div class="row">
    <?php echo $form->labelEx($model,'defect_id'); ?>
    <?php echo $form->textField($model, 'defect_id',
        array('onblur'=>CHtml::ajax(array('type'=>'GET',
            'dataType'=>'json',

            'url'=>array("tasks/getVehicleData"),

            'success'=>"js:function(string){

					if(!string)
					{

						alert('Requisition ID not found or not approved');
							return;
							}

						//	$('#Wsitemdists_vehicle_reg_no').val(string.vehicle_reg_no);
						//	$('#Wsitemdists_vehicletype_id').val(string.vehicletype_id);
									  }"

        ))),
        array('empty' => 'Select one of the following...')

    );
    ?>

    <?php echo $form->error($model,'defect_id'); ?>
</div>

    <div class="row">
        <?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
        <?php //$vehicletype_id = Wsstocks::model()->findAll(array('select'=>'vehicle_reg_no','order' => 'vehicle_reg_no ASC'));
        //echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'vehicle_reg_no','vehicle_reg_no'),array('empty' => 'Select Vehicles...'));?>
        <?php //echo $form->textField($model,'vehicle_reg_no',array('onblur'=>'itemsName(this.value)'));

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'Wsitemdists_vehicle_reg_no',
            'attribute' => 'vehicle_reg_no',
            'source'=>'js: function(request, response) {
    $.ajax({
        url: "'.$this->createUrl('wsitemdists/getVehicle').'",
        dataType: "json",
        data: {
            vehicle_reg_no: request.term,
            //vehicle_model: $("#wsitems_vehicle_model").val(),
            //parts_no: $("#wsitems_parts_no").val()

             //brand: $("#Ws_brand_id").val()
        },
        success: function (data) {
                response(data);

        }
    })
 }',

            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'1',
                'select'=>"js: function(event, ui) { 
                itemsName(ui.item['value']);                   
      //$('#Wsitemdists_vehicle_model').val(ui.item['value']);
     // $('#Wsitemdists_vehicle_model').val(ui.item['vehicle_model']);

         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));


         ?>
         <?php  /* echo $form->textField($model,'vehicle_reg_no',array('onkeyup'=>CHtml::ajax(array('type'=>'GET',
            'dataType'=>'json',
            'url'=>array("wsitems/getItemsData"),
            'success'=>"js:function(string){
                    alert('ok');
                    //if(!string)
                    //{

                       // alert('Requisition ID not found or not approved');
                           // return;
                           // }
                        //alert(string[0].id);
                          //console.log('Hello world!');
                        //$('#Wsitemdists_item_name').val(string.id);

                        //  $('#Wsitemdists_vehicle_reg_no').val(string.vehicle_reg_no);
                        //  $('#Wsitemdists_vehicletype_id').val(string.vehicletype_id);
                                      }"

        ))),
        array('empty' => 'Select one of the following...')

    );
        /*$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'Wsitemdists_vehicle_reg_no',
            'attribute' => 'vehicle_reg_no',


            'source'=>$this->createUrl('vehicles/allRegNo1'),

            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {

         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        )); */  ?>
        
        
        <?php  echo $form->error($model,'vehicle_reg_no'); ?>
      </div>
      <script type="text/javascript">
          function itemsName(v){
            $.post('<?php echo Yii::app()->createAbsoluteUrl("wsitems/getItemsData");?>',{vehicle_reg_no:v},function(data){
                if(!!$('#Wsitemdists_vehicle_reg_no').val()){
                    var obj = JSON.parse(data);
                    var listItem="<option value=''>Select One Item</option>";
                    for (i = 0; i < obj.length; i++) {
                    listItem+="<option value='"+obj[i].id+"'>"+obj[i].item_name1+"</option>";
                    }
                    $('#Wsitemdists_item_name').html(listItem);
                }
            });
            
          }
      </script>

<div class="row">
    <?php echo $form->labelEx($model,'vehicle_model'); ?>
    <?php //echo $form->textField($model,'vehicle_model',array('size'=>20,'maxlength'=>127)); ?>

    <?php
    echo $form->textField($model,'vehicle_model',array('onblur'=>'itemsNameModel(this.value)')); 

   /* $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'model'=>$model,
        'id'=>'Wsitemdists_vehicle_model',
        'attribute' => 'vehicle_model',
        'source'=>$this->createUrl('wsitemdists/getVehicleModel'),
        // additional javascript options for the autocomplete plugin
        'options'=>array(
            'minLength'=>'2',
            'select'=>"js: function(event, ui) {
         //$('#Defects_vehicletype_id').val(ui.item['vehicletype_id']);


         }"
        ),
        'htmlOptions'=>array(
            'style'=>'height:20px;'
        ),
    ));	*/	?>
    <?php echo $form->error($model,'vehicle_model'); ?>
</div>
<script type="text/javascript">
          function itemsNameModel(v){
            $.post('<?php echo Yii::app()->createAbsoluteUrl("wsitems/getItemsDataModel");?>',{vehicle_model:v},function(data){
                if(!!$('#Wsitemdists_vehicle_model').val()){
                    var obj = JSON.parse(data);
                    var listItem="<option value=''>Select One Item</option>";
                    for (i = 0; i < obj.length; i++) {
                    listItem+="<option value='"+obj[i].id+"'>"+obj[i].item_name1+"</option>";
                    }
                    $('#Wsitemdists_item_name').html(listItem);
                }
            });
            
          }
      </script>
<div class="row">
        <?php echo $form->labelEx($model,'item_name'); ?>
        <?php   
        if(!$model->isNewRecord) $list[0]=$model->item_name;
        echo $form->dropDownList($model,'item_name',$list,array('onchange'=>'itemvalues(this.value)')); 
       /* $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'attribute' => 'item_name',
            //'source'=>$this->createUrl('wsitemnames/getItemName'),
//            'source'=>$this->createUrl('wsitemdists/getItemNameStock'),
            'source'=>'js: function(request, response) {
    $.ajax({
        url: "'.$this->createUrl('wsitemdists/getItemNameStock').'",
        dataType: "json",
        data: {
            term: request.term,

            vehicle_reg_no: $("#Wsitemdists_vehicle_reg_no").val(),
            vehicle_model: $("#Wsitemdists_vehicle_model").val(),
            parts_no: $("#Wsitemdists_parts_no").val()


             //brand: $("#Ws_brand_id").val()
        },
        success: function (data) {
                response(data);
        }
    })
 }',

            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'1',
        'select'=>"js: function(event, ui) {             
      $('#Wsitemdists_wsitemname_id').val(ui.item['wsitemname_id']); 
      $('#Wsitemdists_vehicle_model').val(ui.item['vehicle_model']);
      $('#Wsitemdists_available_stock').val(ui.item['available_stock']);
      $('#Wsitemdists_wsitem_id').val(ui.item['wsitem_id']);
      //$('#Wsitemdists_unit_price').val(ui.item['unit_price']);
         }" 
    ),/*
    /*'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),*/
//));     ?>
        <?php  echo $form->error($model,'item_name'); ?>
    </div> 
    <script type="text/javascript">
          function itemvalues(v){
            
            $.post('<?php echo Yii::app()->createAbsoluteUrl("wsitems/getOneItemData");?>',{id:v},function(data){                
                var obj = JSON.parse(data);
                //alert(obj[0].availableStock);
                //alert(obj[0].unit_price);
                $('#Wsitemdists_vehicle_reg_no').val(obj[0].vehicle_reg_no);
                $('#Wsitemdists_vehicle_model').val(obj[0].vehicle_model);                
                $('#Wsitemdists_parts_no').val(obj[0].parts_no);
                $('#Wsitemdists_item_qunt').val(obj[0].availableStock);
                $('#Wsitemdists_item_size').val(obj[0].item_size);
                $('#Wsitemdists_unit_price').val(obj[0].unit_price);
                $('#Wsitemdists_supplier_name').val(obj[0].wssupplier_name);
                $('#Wsitemdists_bill_no').val(obj[0].bill_no);
                $('#Wsitemdists_bill_date').val(obj[0].bill_date);
                $('#Wsitemdists_wsitemname_id').val(obj[0].wsitemname_id);
                $('#Wsitemdists_wsitem_id').val(obj[0].id);

                //var listItem="";
                //for (i = 0; i < obj.length; i++) {
                //listItem+="<option value='"+obj[i].id+"'>"+obj[i].item_name+"<option>";
                //}
                //$('#Wsitemdists_item_name').html(listItem);
            });
            
          }
      </script>

<!--<div class="row">
    <?php //echo $form->labelEx($model,'parts_no'); ?>
    <?php // echo $form->textField($model,'parts_no',array('size'=>20,'maxlength'=>127)); ?>



    <?php

    /*$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'model'=>$model,
        'attribute' => 'parts_no',
        //'source'=>$this->createUrl('wsitemnames/getItemName'),
//            'source'=>$this->createUrl('wsitemdists/getItemNameStock'),
        'source'=>'js: function(request, response) {
    $.ajax({
        url: "'.$this->createUrl('wsitemdists/getPartNo').'",
        dataType: "json",
        data: {
            term: request.term,
            vehicle_model: $("#Wsitemdists_vehicle_model").val(),

        },
        success: function (data) {
                response(data);
        }
    })
 }',

        // additional javascript options for the autocomplete plugin
        'options'=>array(
            'minLength'=>'1',
            'select'=>"js: function(event, ui) {
    //  $('#Wsitemdists_wsitemname_id').val(ui.item['wsitemname_id']);

         }"
        ),*/
        /*'htmlOptions'=>array(
            'style'=>'height:20px;'
        ),*/
    //));		?>


    <?php //echo $form->error($model,'parts_no'); ?>
</div>-->
<div class="row">
<?php echo $form->labelEx($model,'parts_no'); ?>
<?php echo $form->textField($model,'parts_no',array('readonly'=>true)); ?>
<?php echo $form->error($model,'parts_no'); ?>
</div>


</div>
<div class="containerForm">

		
    <div class="row">
        <?php echo $form->labelEx($model,'item_qunt'); ?>
        <?php if(!$model->isNewRecord) $model->item_qunt=$model->available_stock;
        echo $form->textField($model,'item_qunt',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'item_qunt'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'item_size'); ?>
        <?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20,'readonly'=>true)); ?>
        <?php echo $form->error($model,'item_size'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'unit_price'); ?>
        <?php echo $form->textField($model,'unit_price',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'unit_price'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'supplier_name'); ?>
        <?php echo $form->textField($model,'supplier_name',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'supplier_name'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bill_no'); ?>
        <?php echo $form->textField($model,'bill_no',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'bill_no'); ?>
    </div>
</div>
<div class="containerForm">
   <div class="row">
    <?php echo $form->labelEx($model,'bill_date'); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'bill_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->bill_date,
            'changeYear'=>true,
            'changeMonth'=>true,
            'readonly'=>true,

        ),

        'htmlOptions'=>array('style'=>'width:150px;readonly:readonly'
        ),
    )); ?>
    <?php echo $form->error($model,'bill_date'); ?>
</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wsitemname_id'); ?>
		<?php echo $form->textField($model,'wsitemname_id',array('readonly'=>true)); ?>
		<?php echo $form->error($model,'wsitemname_id'); ?>
	</div>
<?php if($model->id){ ?>
   <!--<div class="row">
    <?php echo $form->labelEx($model,'available_Stock'); ?>
    <?php echo $form->textField($model,'available_stock',array('readonly'=>true)); ?>
    <?php echo $form->error($model,'available_stock'); ?>
    </div> -->
<?php } ?>
    <div class="row">
        <?php echo $form->labelEx($model,'wsitem_id'); ?>
        <?php echo $form->textField($model,'wsitem_id',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'wsitem_id'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127,'autofocus'=>'autofocus')); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>
	
	 
	 <?php //echo $form->labelEx($model,'unit'); ?>
	 <?php //echo $form->dropDownList($model, 'unit', $unitlist); ?>
	    
	    <?php //echo $form->error($model,'Unit');

    ?>

    
</div>
<div class="containerForm">
    <div class="row">
        <?php echo $form->labelEx($model,'issue_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'issue_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->issue_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'issue_date'); ?>
    </div>
<div class="row">
    <?php echo $form->labelEx($model,'mechanic_pin'); ?>
    <?php echo $form->textField($model, 'mechanic_pin',
        array('onblur'=>CHtml::ajax(array('type'=>'GET',
            'dataType'=>'json',

            'url'=>array("vehicles/CallHrUser"),

            'success'=>"js:function(string){
                    if(!!$('#Wsitemdists_mechanic_pin').val())
                        $('#Wsitemdists_mechanic_name').val(string.StaffName);
                   }"

        ))),
        array('empty' => 'Select one of the following...')

    );
    ?>

    <?php echo $form->error($model,'mechanic_pin'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'mechanic_name'); ?>
    <?php echo $form->textField($model,'mechanic_name',array('size'=>60,'maxlength'=>127)); ?>
    <?php echo $form->error($model,'mechanic_name'); ?>
</div>
<div class="row">
    <?php echo $form->labelEx($model,'remark'); ?>
    <?php echo $form->textField($model,'remark',array('size'=>60,'maxlength'=>127)); ?>
    <?php echo $form->error($model,'remark'); ?>
</div>
<?php //echo $model->defectID; //echo $form->textField($model,'defectID',array('readonly'=>true)); ?>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'item_size'); ?>
		<?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'item_size'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_slno'); ?>
		<?php echo $form->textField($model,'item_slno',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'item_slno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bill_no'); ?>
		<?php echo $form->textField($model,'bill_no'); ?>
		<?php echo $form->error($model,'bill_no'); ?>
	</div>

   <div class="row">
    <?php echo $form->labelEx($model,'bill_date'); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'bill_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->bill_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
    <?php echo $form->error($model,'bill_date'); ?>
</div>
-->
    <!--


        <div class="row">
            <?php echo $form->labelEx($model,'total_price'); ?>
            <?php echo $form->textField($model,'total_price'); ?>
            <?php echo $form->error($model,'total_price'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model,'active'); ?>
            <?php echo $form->textField($model,'active'); ?>
            <?php echo $form->error($model,'active'); ?>
        </div>

       

	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
-->
</div>
    <div class="clear"></div>
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit',
        'size' => 'medium',
        

    ));
    ?>
    </div>
    <?php if(isset($up) && $up==1){ ?>
    <script>
        function myCheck(v){
            var err='';
            if(!document.getElementById("Wsitemdists_defect_id").value){
                err=err + 'Defects Id Can not empty\n';
                
            }
            if(!document.getElementById("Wsitemdists_item_name").value){
                err=err + 'tem Name Can not empty\n';
                
            }
            if(!document.getElementById("Wsitemdists_quantity").value){
                err=err + 'Issue Quantity  Can not empty\n';
                
            }            
            if(err!=""){alert(err);return false;}
            
        }
    </script>

   <?php }
   else {
    ?>
    <script>
        function myCheck(v){
           // alert(document.getElementById("Wsitemdists_item_qunt").value);
            //alert(document.getElementById("Wsitemdists_quantity").value);
 
            var err='';
            if(!document.getElementById("Wsitemdists_defect_id").value){
                err=err + 'Defects Id Can not empty\n';
                
            }
            if(!document.getElementById("Wsitemdists_item_name").value){
                err=err + 'tem Name Can not empty\n';
                
            }
            if(!document.getElementById("Wsitemdists_quantity").value){
                err=err + 'Issue Quantity  Can not empty\n';
                
            }
            if(Number(document.getElementById("Wsitemdists_item_qunt").value)< Number(document.getElementById("Wsitemdists_quantity").value)){
                err=err + 'Insufficiency of this item in stock.';
            }
            if(err!=""){alert(err);err='';return false;}
            
        }
    </script>
<?php } ?>

<?php $this->endWidget(); ?>

</div><!-- form -->