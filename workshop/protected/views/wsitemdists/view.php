<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */

$this->breadcrumbs=array(
	'Item Distributions'=>array('index'),
	$model->id,
);
if(Yii::app()->user->username!='185036') {
	$this->menu=array(
		array('label'=>'List Item Distributions', 'url'=>array('index'),'visible'=> in_array('List Item Distributions', $user_rights)),
		array('label'=>'New Item Distribution', 'url'=>array('create'),'visible'=> in_array('New Item Distribution', $user_rights)),
		array('label'=>'Update Item Distribution', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Item Distribution', $user_rights)),
		array('label'=>'Delete Item Distribution', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Item Distribution', $user_rights)),
		array('label'=>'Manage Item Distributions', 'url'=>array('admin'),'visible'=> in_array('Manage Item Distributions', $user_rights)),
	);
}
?>

<h4>View Item Distribution #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'defect_id',
		//'task_id',
		'wsitemname_id',
		'item_name',
		'quantity',
		'issue_date',
		//'item_size',
		//'item_slno',
		//'bill_no',
		//'bill_date',

		'total_price',
		'mechanic_pin',
		'mechanic_name',
	//	'active',
	//	'created_by',
	//	'created_time',
	//	'updated_by',
	//	'update_time',
	),
)); ?>

<br />
<h5>Go to the Defect:
	<?php echo CHtml::link($model->defect_id, $this->createAbsoluteUrl('defects/view',array('id'=>$model->defect_id))); ?>
</h5>
