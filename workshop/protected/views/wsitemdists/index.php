<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemdistsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Item Distributions',
);

$this->menu=array(
	array('label'=>'New Item Distribution', 'url'=>array('create'),'visible'=> in_array('New Item Distribution', $user_rights)),
    array('label'=>'Manage Item Distributions', 'url'=>array('admin'),'visible'=> in_array('Manage Item Distributions', $user_rights)),
    array('label'=>'Inventory of Spares', 'url'=>array('sapare'),'visible'=> in_array('Inventory of Spares', $user_rights)),
);
?>

<h4>Item Distributions</h4>
<div class="search-form">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div>
<div id=req_container>
    <?php

 /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
*/





?>

<?php

$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'wsitemdists-grid',
    'dataProvider'=>$model->search1(),
    'filter'=>$model,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),

        'defect_id',        
        'item_name',
        'vehicle_reg_no',
        'defects.vehicle_reg_no',
        'vehicle_model',         
        'parts_no', 
        'quantity',
        'issue_date',
        'unit_price',
        'total_price',
        'mechanic_name',
        
    ),
));  ?>
</div>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel1','Wsitemdists'=>$_GET['Wsitemdists'])));
?> &nbsp; &nbsp; &nbsp;