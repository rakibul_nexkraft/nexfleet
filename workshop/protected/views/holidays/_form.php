<?php
/* @var $this HolidaysController */
/* @var $model Holidays */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'Holidays-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'start_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'start_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->start_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'start_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'end_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'end_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->start_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'end_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'remarks'); ?>
        <?php echo $form->textArea($model,'remarks',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'remarks'); ?>
    </div>

	<!--div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->