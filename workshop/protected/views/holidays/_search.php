<?php
/* @var $this HolidaysController */
/* @var $model Holidays */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div-->

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'start_date'); ?><br />
			<?php echo $form->textField($model,'start_date',array('maxlength'=>127)); ?>
		</div>
	</div>


    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'end_date'); ?><br />
            <?php echo $form->textField($model,'end_date',array('maxlength'=>127)); ?>
        </div>
    </div>

	<div class="row buttons" align="right">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->