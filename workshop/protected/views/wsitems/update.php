<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemsController */
/* @var $model Wsitems */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index'),'visible'=> in_array('Stock-In List Items', $user_rights)),
	array('label'=>'New Item', 'url'=>array('create'),'visible'=> in_array('Stock-In New Item', $user_rights)),
	array('label'=>'View Item', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('Stock-In View Item', $user_rights)),
	array('label'=>'Manage Items', 'url'=>array('admin'),'visible'=> in_array('Stock-In Manage Items', $user_rights)),
);
?>

<h4>Update Item <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'purchasetype'=>$purchasetype)); ?>