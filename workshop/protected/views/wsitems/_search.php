<?php
/* @var $this WsitemsController */
/* @var $model Wsitems */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'id'); ?><br />
			<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'item_name'); ?><br />
			<?php echo $form->textField($model,'item_name',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'vehicle_reg_no'); ?><br />
            <?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
        </div>
    </div>

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'vehicle_model'); ?><br />
			<?php echo $form->textField($model,'vehicle_model',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'parts_no'); ?><br />
            <?php echo $form->textField($model,'parts_no',array('size'=>18,'class'=>'input-medium')); ?>
        </div>
    </div>



	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'wsrequisition_requisition_no'); ?><br />
			<?php echo $form->textField($model,'wsrequisition_requisition_no',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

	<!-- <div class="fl">
		<div class="row">
			<?php echo $form->label($model,'wssupplier_id'); ?><br />
			<?php echo $form->textField($model,'wssupplier_id',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div> -->

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'wssupplier_name'); ?><br />
			<?php echo $form->textField($model,'wssupplier_name',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>

<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'from_date'); ?><br />
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,
                'attribute'=>'from_date',  // name of post parameter
                //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                'options'=>array(
                    'dateFormat'=>'yy-mm-dd',
                    //'defaultDate'=>$model->from_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:17px; width:138px;',
                ),
            ));
            ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'to_date'); ?><br />
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,
                'attribute'=>'to_date',
                //'value'=>Yii::app()->request->cookies['to_date']->value,
                'options'=>array(
                    //	'showAnim'=>'fold',
                    'dateFormat'=>'yy-mm-dd',
                    //'defaultDate'=>$model->to_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:17px; width:138px;'
                ),
            ));
            ?>
        </div>
    </div>
</fieldset>
	
	<!--<div class="row">
		<?php // echo $form->label($model,'vehicle_reg_no'); ?>
		<?php // echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_amount'); ?>
		<?php echo $form->textField($model,'bill_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_date'); ?>
		<?php echo $form->textField($model,'bill_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>18,'class'=>'input-medium')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warranty'); ?>
		<?php echo $form->textField($model,'warranty',array('size'=>18,'class'=>'input-medium')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit_price'); ?>
		<?php echo $form->textField($model,'unit_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_price'); ?>
		<?php echo $form->textField($model,'total_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>18,'class'=>'input-medium')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>18,'class'=>'input-medium')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>-->

	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->