<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemsController */
/* @var $model Wsitems */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	'New',
);

$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index'),'visible'=> in_array('Stock-In List Items', $user_rights)),
	array('label'=>'Manage Items', 'url'=>array('admin'),'visible'=> in_array('Stock-In Manage Items', $user_rights)),
);
?>

<h4>New Item</h4>
<?php 
if(isset($_POST['id'])&& !empty($_POST['id'])){
	$id=$_POST['id'];
	$model=Wsitems::model()->findByAttributes(array('id'=>$id));
}

if(isset($_POST['attr'])&& !empty($_POST['attr'])){
	
	$model->attributes=(array)json_decode($_POST['attr']);
	
}
if(isset($_POST['erro'])&& !empty($_POST['erro'])){	
	$errors=(array)json_decode($_POST['erro']);	
echo $this->renderPartial('_form', array('model'=>$model,'purchasetype'=>$purchasetype,'erro'=>$errors)); 	
}
else{
echo $this->renderPartial('_form', array('model'=>$model,'purchasetype'=>$purchasetype));
}
 ?>

<?php //echo $this->renderPartial('_form', array('model'=>$model,'purchasetype'=>$purchasetype)); ?>