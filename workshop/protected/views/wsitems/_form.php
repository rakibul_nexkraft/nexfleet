<?php
/* @var $this WsitemsController */
/* @var $model Wsitems */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'wsitems-form',
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php 
    if(isset($erro) && !empty($erro)){ ?>
    <div class="errorSummary">
        <p>Please fix the following input errors:</p>
        <ul>
        <?php foreach ($erro as $key => $value) {
   

           echo "<li>".$value[0]."</li>";
        }?>
    </ul>
    </div>
    <?php }    ?>

    <?php echo $form->errorSummary($model); ?>

<div class="containerForm">


    <div class="row">
        <?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
        <?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'wsitems_vehicle_reg_no',
            'attribute' => 'vehicle_reg_no',
            'source'=>$this->createUrl('vehicles/allRegNo1'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
        // $('#Defects_vehicletype_id').val(ui.item['vehicletype_id']);
        }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));     ?>


        <?php  echo $form->error($model,'vehicle_reg_no'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'wsrequisition_requisition_no'); ?>
        <?php echo $form->textField($model,'wsrequisition_requisition_no',array('size'=>20,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'wsrequisition_requisition_no'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'requisition_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'id'=>'requisition_datepicker',
            'model'=>$model,'attribute'=>'requisition_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->requisition_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),
        )); ?>
        <?php echo $form->error($model,'requisition_date'); ?>
    </div>

    <!--<div class="row">
        <?php echo $form->labelEx($model,'purchase_type'); ?>
        <?php echo $form->textField($model,'purchase_type',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'purchase_type'); ?>
    </div> -->

    <div class="row">
        <?php echo $form->labelEx($model,'purchase_type'); ?>
        <?php //echo $form->textField($model,'licensetype',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->dropDownList($model, 'purchase_type', $purchasetype); ?>
        <?php echo $form->error($model,'purchase_type'); ?>
    </div>

    
</div>
<div class="containerForm">
    <div class="row">
        <?php echo $form->labelEx($model,'item_size'); ?>
        <?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'item_size'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'vehicle_model'); ?>
        <?php // echo $form->textField($model,'vehicle_model',array('size'=>20,'maxlength'=>127)); ?>
        <?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'wsitems_vehicle_model',
            'attribute' => 'vehicle_model',
            'source'=>$this->createUrl('wsitems/getVehicleModel'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
         //$('#Defects_vehicletype_id').val(ui.item['vehicletype_id']);
        // $('#Defects_driver_name').val(ui.item['driver_name']);
         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));     ?>

        <?php /*$getvehicle = Wsitemnames::model()->findAll(array('select'=>'vehicle_model','distinct'=>'true','order' => 'vehicle_model ASC'));
        echo $form->dropDownList($model,'vehicle_model', CHtml::listData($getvehicle,'vehicle_model',  'vehicle_model'),     array('onchange'=>CHtml::ajax(array('type'=>'GET',
                'type'=>'GET', 'update'=>'#Wsitems_parts_no',
                'url'=>array("wsitems/UpdateAjaxStation")
            ))),
            array('empty' => 'Select one of the following...')); */
        ?>

        <?php echo $form->error($model,'vehicle_model'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'parts_no'); ?>
        <?php // echo $form->textField($model,'parts_no',array('size'=>20,'maxlength'=>127)); ?>
        <?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'wsitems_parts_no',
            'attribute' => 'parts_no',
            'source'=>$this->createUrl('wsitems/getPartNo'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
              // $('#Defects_driver_name').val(ui.item['driver_name']);
         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));     ?>

        <?php /* $getparts = Wsitemnames::model()->findAll(array('select'=>'parts_no','distinct'=>'false','order' => 'parts_no ASC'));
        echo $form->dropDownList($model,'parts_no', CHtml::listData($getparts,'parts_no',  'parts_no'),     array('onchange'=>CHtml::ajax(array('type'=>'GET',
                 'update'=>'#Wsitems_item_name',
                 'url'=>array("wsitems/UpdateAjaxStation1")
            ))));
*/
        ?>


        <?php echo $form->error($model,'parts_no'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'item_name'); ?>
        <?php
                //echo $form->textField($model,'item_name',array('onblur'=>'getItemName1(this.value)')); 
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'id'=>'item_name_ac',
            'attribute' => 'item_name',
            'source'=>'js: function(request, response) {
    $.ajax({
        url: "'.$this->createUrl('wsitems/getItemName').'",
        dataType: "json",
        data: {
            term: request.term,
            vehicle_model: $("#wsitems_vehicle_model").val(),
            parts_no: $("#wsitems_parts_no").val()

             //brand: $("#Ws_brand_id").val()
        },
        success: function (data) {
                response(data);

        }
    })
 }',

            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'1',
                'select'=>"js: function(event, ui) {                    
      $('#Wsitems_wsitemname_id').val(ui.item['wsitemname_id']);
     // $('#Wsitemdists_vehicle_model').val(ui.item['vehicle_model']);

         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        )); ?>


        <?php
/*
       $getname = Wsitemnames::model()->findAll(array('select'=>'name','distinct'=>'false','order' => 'id ASC'));
        echo $form->dropDownList($model,'item_name', CHtml::listData($getname,'name','name'),
            array('onchange'=>CHtml::ajax(array('type'=>'GET',
            'update'=>'#Wsitems_wsitemname_id',
                'url'=>array("wsitems/UpdateAjaxStation2"),

        ))));*/

        ?>


        <?php echo $form->error($model,'item_name'); ?>
    </div>
    <script type="text/javascript">
          function getItemName1(v){

            var vehicleM=$('#wsitems_vehicle_model').val();
            var partsN=$('#wsitems_parts_no').val();
            alert(v);
            $.post('<?php echo Yii::app()->createAbsoluteUrl("wsitems/getItemName");?>',{term:v,vehicle_model:vehicleM,parts_no:partsN},function(data){
                //if(!!$('#Wsitems_item_name').val()){
                    
                    var obj = JSON.parse(data);
                    var listItem=obj[0].wsitemname_id;
                    //alert(obj);
                    //}
                    $('#Wsitems_wsitemname_id').val(listItem);                
            });
            
          }
      </script>

    
</div>

<div class="containerForm">    
    <div class="row">
        <?php echo $form->labelEx($model,'wsitemname_id'); ?>
        <?php
//        $getname = Wsitemnames::model()->findAll(array('select'=>'name','distinct'=>'false','order' => 'id ASC'));
  //      echo $form->dropDownList($model,'wsitemname_id', CHtml::listData($getname,'id','id'));

        ?>
        <?php echo $form->textField($model,'wsitemname_id',array('size'=>20,'maxlength'=>20,'readonly'=>"true")); ?>
        <?php echo $form->error($model,'wsitemname_id'); ?>
    </div>
    <div class="row">
        <?php  echo $form->labelEx($model,'wssupplier_name'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'id'=>'wssupplier_name_ac',
            'model'=>$model,
            'attribute' => 'wssupplier_name',
            'source'=>$this->createUrl('wssupplier/getSupplierName'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {

                 $('#Wsitems_wssupplier_id').val(ui.item['id']);
         }"
            ),
            /*'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),*/
        ));     ?>
        <?php  echo $form->error($model,'wssupplier_name'); ?>
    </div>



    <div class="row">
        <?php // echo $form->labelEx($model,'wssupplier_id'); ?>
        <?php echo $form->hiddenField($model,'wssupplier_id',array('size'=>60,'maxlength'=>127,'type'=>"hidden")); ?>
        <?php // echo $form->error($model,'wssupplier_id'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'challan_no'); ?>
        <?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'challan_no'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'challan_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'id'=>'challan_datepicker',
            'model'=>$model,'attribute'=>'challan_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->challan_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),
        )); ?>
        <?php echo $form->error($model,'challan_date'); ?>
    </div>

    
</div>
<div class="containerForm"> 
    

    <div class="row">
        <?php echo $form->labelEx($model,'bill_no'); ?>
        <?php echo $form->textField($model,'bill_no'); ?>
        <?php echo $form->error($model,'bill_no'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'bill_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'id' => 'bill_datepicker',
            'model'=>$model,'attribute'=>'bill_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->bill_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'bill_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'bin'); ?>
        <?php echo $form->textField($model,'bin',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'bin'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'part'); ?>
        <?php echo $form->textField($model,'part',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'part'); ?>
    </div>
    
</div>
<div class="containerForm"> 


     <div class="row">
        <?php echo $form->labelEx($model,'remarks'); ?>
        <?php echo $form->textField($model,'remarks'); ?>
        <?php echo $form->error($model,'remarks'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'quantity'); ?>
        <?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'quantity'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'unit_price'); ?>
        <?php echo $form->textField($model,'unit_price'); ?>
        <?php echo $form->error($model,'unit_price'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'total_price'); ?>
        <?php echo $form->textField($model,'total_price', array('onfocus'=>'tot_price()')); ?>
        <?php echo $form->error($model,'total_price'); ?>
    </div>



</div>



<div class="clearfix"></div>
<div class="row fl" style="margin-right:10px;">
    <?php 
    /*
    
    $this->widget('ext.widgets.tabularinput.XTabularInput',array(
        'models'=>$wsitems,
        'inputView'=>'_tabularInput',
        'inputUrl'=>$this->createUrl('wsitems/addTabularInputs'),
        'removeTemplate'=>'<div class="action">{link}</div>',
        'addTemplate'=>'<div class="action">{link}</div>',
    ));
    */ 
    ?>

</div>
<div class="clearfix"></div>
    <div class="row fl" style="margin-right:10px;">
        <?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        <?php
            $this->widget('bootstrap.widgets.TbButton',array(
                'label' =>'Save',
                'type' => 'primary',
                'buttonType'=>'submit',
                'size' => 'medium',
            ));
        ?>
    </div>
    <?php 
    /*if(Yii::app()->request->isAjaxRequest) { ?>
    <div class="row">
        <?php
            $this->widget('bootstrap.widgets.TbButton',array(
                'label' =>'Close',
                'type' => '',
                'buttonType'=>'button', 
                'size' => 'medium',
                'htmlOptions'=>array('data-dismiss' => 'modal', 'data-target' => '#popModal'),
            ));
        ?>
    </div>
    
<?php 
    }
    */
    $this->endWidget();
    
?>

</div><!-- form -->

<script>
    function tot_price()
    {
        
    $('#Wsitems_total_price').val($('#Wsitems_quantity').val()*$('#Wsitems_unit_price').val());
}
    </script>