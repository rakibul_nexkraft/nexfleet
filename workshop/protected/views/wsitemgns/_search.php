<?php
/* @var $this WsitemgnsController */
/* @var $model Wsitemgns */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_name'); ?>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wsitemname_id'); ?>
		<?php echo $form->textField($model,'wsitemname_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wsrequisition_requisition_no'); ?>
		<?php echo $form->textField($model,'wsrequisition_requisition_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_type'); ?>
		<?php echo $form->textField($model,'purchase_type',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_date'); ?>
		<?php echo $form->textField($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item_size'); ?>
		<?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_model'); ?>
		<?php echo $form->textField($model,'vehicle_model',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_code'); ?>
		<?php echo $form->textField($model,'vehicle_code',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wssupplier_id'); ?>
		<?php echo $form->textField($model,'wssupplier_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wssupplier_name'); ?>
		<?php echo $form->textField($model,'wssupplier_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'challan_no'); ?>
		<?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'challan_date'); ?>
		<?php echo $form->textField($model,'challan_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_amount'); ?>
		<?php echo $form->textField($model,'bill_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_date'); ?>
		<?php echo $form->textField($model,'bill_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'warranty'); ?>
		<?php echo $form->textField($model,'warranty',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unit_price'); ?>
		<?php echo $form->textField($model,'unit_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_price'); ?>
		<?php echo $form->textField($model,'total_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->