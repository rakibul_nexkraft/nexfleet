<?php
/* @var $this WsitemgnsController */
/* @var $model Wsitemgns */

$this->breadcrumbs=array(
	'Wsitemgns'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List General Stock', 'url'=>array('index')),
	array('label'=>'New General Stock', 'url'=>array('create')),
	array('label'=>'Update General Stock', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete General Stock', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage General Stock', 'url'=>array('admin')),
);
?>

<h4>View General Stock #<?php echo $model->id; ?></h4>

<?php 
/* $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_name',
		'wsitemname_id',
		'wsrequisition_requisition_no',
		'purchase_type',
		'purchase_date',
		'item_size',
		'vehicle_model',
		'vehicle_code',
		'wssupplier_id',
		'wssupplier_name',
		'challan_no',
		'challan_date',
		'bill_amount',
		'bill_date',
		'quantity',
		'warranty',
		'unit_price',
		'total_price',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
));*/ ?>


<?php 

$this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
            
            'id','wsrequisition_requisition_no',
		'item_name','purchase_type',
		'wsitemname_id','purchase_date',
		'item_size','vehicle_model',
		'wssupplier_name' ,'vehicle_code',
    'wssupplier_id','quantity',
		'challan_date','unit_price',
		'challan_no','warranty',
		'bill_date','total_price',
	  'bill_amount',
		
	
			
			'updated_by',
			'created_by',		'updated_time',
			'created_time',		
			),
		),
    ),
));
