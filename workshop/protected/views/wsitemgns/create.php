<?php
/* @var $this WsitemgnsController */
/* @var $model Wsitemgns */

$this->breadcrumbs=array(
	'Wsitemgns'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List General Stock', 'url'=>array('index')),
	array('label'=>'Manage General Stock', 'url'=>array('admin')),
);
?>

<h4>New General Stock</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>