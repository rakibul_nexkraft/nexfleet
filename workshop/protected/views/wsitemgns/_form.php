<?php
/* @var $this WsitemgnsController */
/* @var $model Wsitemgns */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsitemgns-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="container1">

    <div class="row">
            <?php echo $form->labelEx($model,'item_name'); ?>
            <?php //echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>127)); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'id'=>'item_name_ac',
                'model'=>$model,
                'attribute' => 'item_name',
                'source'=>$this->createUrl('wsitemnames/getItemName'),
                // additional javascript options for the autocomplete plugin
                'options'=>array(
                    'minLength'=>'1',
                    'select'=>"js: function(event, ui) {
				$('#Wsitemgns_wsitemname_id').val(ui.item['wsitemname_id']);
				$('#Wsitemgns_vehicle_model').val(ui.item['vehicle_model']);
				$('#Wsitemgns_vehicle_code').val(ui.item['vehicle_type']);				}",

                ),
            ));
            ?>
            <?php echo $form->error($model,'item_name'); ?>
        </div>

	<div class="row">
            <?php echo $form->labelEx($model,'wsitemname_id'); ?>
            <?php echo $form->textField($model,'wsitemname_id',array('size'=>20,'maxlength'=>20,'readonly'=>true)); ?>
            <?php echo $form->error($model,'wsitemname_id'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'wsrequisition_requisition_no'); ?>
		<?php echo $form->textField($model,'wsrequisition_requisition_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'wsrequisition_requisition_no'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php 	
		
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {        
         //$('#Wsitrequisitions_vmodel').val(ui.item['vmodel']);         
         //$('#Wsrequisitions_vehicle_codeid').val(ui.item['vehicletype_id']);        
         }"        
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));		?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

     <div class="row">
            <?php echo $form->labelEx($model,'vehicle_model'); ?>
            <?php echo $form->textField($model,'vehicle_model',array('size'=>60,'maxlength'=>127)); ?>
            <?php echo $form->error($model,'vehicle_model'); ?>
     </div>

	<div class="row">
		<?php echo $form->labelEx($model,'purchase_type'); ?>
		<?php echo $form->textField($model,'purchase_type',array('size'=>20,'maxlength'=>20)); ?>
             
		<?php echo $form->error($model,'purchase_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'purchase_date'); ?>
		<?php //echo $form->textField($model,'purchase_date'); ?>
             <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'purchase_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->purchase_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_size'); ?>
		<?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'item_size'); ?>
	</div>

<!--
	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_code'); ?>
		<?php echo $form->textField($model,'vehicle_code',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'vehicle_code'); ?>
	</div>
-->

      <div class="row">
          <?php // echo $form->labelEx($model,'wssupplier_id'); ?>
          <?php echo $form->hiddenField($model,'wssupplier_id',array('size'=>60,'maxlength'=>127,'type'=>"hidden")); ?>
          <?php // echo $form->error($model,'wssupplier_id'); ?>
      </div>

        <div class="row">
            <?php echo $form->labelEx($model,'wssupplier_name'); ?>
            <?php echo $form->textField($model,'wssupplier_name',array('size'=>60,'maxlength'=>127)); ?>
            <?php echo $form->error($model,'wssupplier_name'); ?>
        </div>
</div>
<div class="container2">

	<div class="row">
		<?php echo $form->labelEx($model,'challan_no'); ?>
		<?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'challan_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'challan_date'); ?>
		<?php //echo $form->textField($model,'challan_date'); ?>
             <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'challan_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->challan_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'challan_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bill_amount'); ?>
		<?php echo $form->textField($model,'bill_amount'); ?>
		<?php echo $form->error($model,'bill_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bill_date'); ?>
		<?php //echo $form->textField($model,'bill_date'); ?>
             <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'bill_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->bill_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'bill_date'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'warranty'); ?>
        <?php echo $form->textField($model,'warranty',array('size'=>60,'maxlength'=>60)); ?>
        <?php echo $form->error($model,'warranty'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unit_price'); ?>
		<?php echo $form->textField($model,'unit_price'); ?>
		<?php echo $form->error($model,'unit_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_price'); ?>
		<?php echo $form->textField($model,'total_price', array('onfocus'=>'tot_price()')); ?>
		<?php echo $form->error($model,'total_price'); ?>
	</div>

<!--
	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>
-->
</div>
	<div class="clear"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>



<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    function tot_price()
    {

        $('#Wsitemgns_total_price').val($('#Wsitemgns_quantity').val()*$('#Wsitemgns_unit_price').val());
    }
</script>