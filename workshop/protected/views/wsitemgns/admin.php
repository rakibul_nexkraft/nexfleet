<?php
/* @var $this WsitemgnsController */
/* @var $model Wsitemgns */

$this->breadcrumbs=array(
	'Wsitemgns'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List General Stock', 'url'=>array('index')),
	array('label'=>'New General Stock', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('wsitemgns-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage General Stock</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'wsitemgns-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'item_name',
		'wsitemname_id',
		'wsrequisition_requisition_no',
		'purchase_type',
		'purchase_date',
		/*
		'item_size',
		'vehicle_model',
		'vehicle_code',
		'wssupplier_id',
		'wssupplier_name',
		'challan_no',
		'challan_date',
		'bill_amount',
		'bill_date',
		'quantity',
		'warranty',
		'unit_price',
		'total_price',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
