<?php
/* @var $this WsitemgnsController */
/* @var $model Wsitemgns */

$this->breadcrumbs=array(
	'Wsitemgns'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List General Stock', 'url'=>array('index')),
	array('label'=>'New General Stock', 'url'=>array('create')),
	array('label'=>'View General Stock', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage General Stock', 'url'=>array('admin')),
);
?>

<h4>Update General Stock <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>