<?php

class DefectdetailController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create', 'update', 'submitDetail'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','deleteDetail'),
                //'users'=>array('admin'),
                'expression'=>'Yii::app()->user->isAdmin()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    public function actionSubmitDetail()
    {
        $model = null;
        if(!empty($_POST['id'])){
            $model = $this->loadModel($_POST['id']);
        }else{
            $model = new Defectdetail;
        }

        $model->defect_id = $_POST['defect_id'];
        $model->job_name = $_POST['job_name'];
//        $model->action_name = $_POST['action_name'];
        $model->mechanic_id = $_POST['mechanic_id'];
        $model->mechanic_name = $_POST['mechanic_name'];

        if(isset($model->attributes)){
            if ($model->save()){
                echo json_encode($model->attributes);die();
            }
        }

    }

    public function actionDeleteDetail(){


        $id = $_POST['detailId'];
        $this->loadModel($id)->delete();
        echo json_encode($id);die();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=Defectdetail::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function getAllDefectsById($defect_id){
        $result_detail = "";
        $sql = "SELECT * FROM tbl_defectdetail WHERE defect_id=$defect_id";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results as $result){
            $result_detail .= $result['job_name'].", ";
        }

        return $result_detail;
    }
    public function getAllMechanicsById($defect_id){
        $result_detail = "";
        $sql = "SELECT * FROM tbl_defectdetail WHERE defect_id=$defect_id";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results as $result){
            $result_detail .= $result['mechanic_name'].", ";
        }

        return $result_detail;
    }


    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
//    protected function performAjaxValidation($model)
//    {
//        if(isset($_POST['ajax']) && $_POST['ajax']==='accidents-form')
//        {
//            echo CActiveForm::validate($model);
//            Yii::app()->end();
//        }
//    }








}
