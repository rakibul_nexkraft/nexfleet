<?php

class WsstocksController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','excel','excel3','excel2'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','report1','report2'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Wsstocks;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsstocks']))
		{
			$model->attributes=$_POST['Wsstocks'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsstocks']))
		{
			$model->attributes=$_POST['Wsstocks'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        //$dataProvider=new CActiveDataProvider('Wsitemnames');

        $model=new Wsstocks('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsstocks']))
            $model->attributes=$_GET['Wsstocks'];

        $this->render('index',array(
            //'dataProvider'=>$dataProvider,
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Wsstocks('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsstocks']))
			$model->attributes=$_GET['Wsstocks'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionReport1(){

       // $this->layout='column1';
        $model=new Wsstocks('availablelist');
        $model->unsetAttributes();// clear any default values
        if(isset($_GET['Wsstocks']))
            $model->attributes=$_GET['Wsstocks'];

        $this->render('report1',array(
            'model'=>$model,
            //  'dataProvider'=>$model->search(),
        ));

    }
     public function actionReport2(){

       // $this->layout='column1';
        $model=new Wsstocks('availablelist');
        $model->unsetAttributes();// clear any default values
        if(isset($_GET['Wsstocks']))
            $model->attributes=$_GET['Wsstocks'];

        $this->render('report2',array(
            'model'=>$model,
            //  'dataProvider'=>$model->search(),
        ));

    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Wsstocks::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wsstocks-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	    public function actionExcel()
    {
        $model=new Wsstocks;
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsstocks']))

            $model->attributes=$_GET['Wsstocks'];


        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->availablelist(),
            'title'=>'iFleet_Available List',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Available List',

            'columns'=>array(
                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),                                
                
                'wsitemname',
                'vehicle_model',
                'vehicle_reg_no',
                'available_stock',
                'price',
                


                /*
                'created_time',
                'created_by',
                'active',
                */
            ),

        ));
        Yii::app()->end();
    }

    public function actionExcel2()
	{
		$model=new Wsstocks('search');
		//$model=new Wsstocks;
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsstocks']))
			$model->attributes=$_GET['Wsstocks'];

		$this->widget('ext.phpexcel.EExcelView', array(
			//'dataProvider'=> $model->search(),
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Stock',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Stock',
			'columns'=>array(
       			 'id',
		      // array(
		            //'name' => 'id',
		           // 'type'=>'raw',
		            //'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
		        //),
		   
		        'wsitem_id',
		        'wsitemname_id',
				'wsitemname',
				'wsdist_id',
		        'vehicle_reg_no',
		        'vehicle_model',
		        'parts_no',
				'stock_in',
				'stock_out',
				'available_stock',		
				'price',
				'total_amount',
				'purchase_date',
				'issue_date',
			   'active',   
    
			),
		));
		Yii::app()->end();
	}

     public function actionExcel1()
    {
    	/*$data=array('id' =>'','wsitemname_id' =>'','wsitemname' =>'','wsitem_id' => '','wsdist_id' =>'','vehicle_reg_no' =>'','vehicle_model' =>'','parts_no' =>'','stock_in' =>'','stock_out' =>'','available_stock' =>'','price' =>'','total_amount' =>'','purchase_date' =>'','issue_date' =>'','active' =>'');
    	$search_reauest=json_encode($data);
    	$username=Yii::app()->user->username;
        $user_info=User::model()->findByAttributes(array('username'=>$username));      
        $user_mail=$user_info->email;            
        $model_name="Wsstocks";
        $create_time=date("Y-m-d h:i:s");
        $total_row=$_GET['totalSheet'];
        $query = "INSERT INTO tbl_large_excel_send (username,email,data_get,model_name,total_row,created_time)
        VALUES ('".$username."','".$user_mail."','".$search_reauest."','".$model_name."','".$total_row."','".$create_time."')";
        $save_query=Yii::app()->db->createCommand($query)->query();
                       
                       //$this->sendMailIcress($user_mail);

            

            $this->render('excelMessage');*/
            $model=new Wsstocks('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];
        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=>$model->search(),
            'title'=>'iFleet Stock List',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet Stock List',

            'columns'=>array(
                array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
   
        'wsitem_id',
        'wsitemname_id',
		'wsitemname',
		'wsdist_id',
        'vehicle_reg_no',
        'vehicle_model',
        'parts_no',
		'stock_in',
		'stock_out',
		'available_stock',
		/*'temp_stock',*/
		'price',
		'total_amount',
		'purchase_date',
		'issue_date',
	   'active',
		
            ),

        ));
        Yii::app()->end();
    }
    
    public function actionExcel3()
    {
        $model=new Wsstocks;
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsstocks']))
            $model->attributes=$_GET['Wsstocks'];
        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->availablelist(),
            'title'=>'iFleet_Available List',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Available List',

            'columns'=>array(
                'id',
	        'wsitemname',
	        'vehicle_model',
	        'parts_no',
	        'vehicle_reg_no',
	        array(
	                'header' => 'Total Purchase Qty',               
	                'value'=>'$data->getTotalPurchase($data->vehicle_reg_no,$data->wsitemname,$data->vehicle_model)',
	        ),
	        array(
	                'header' => 'Date Wise Purchase Qty',  
	                            
	                'value'=>'$data->getDateWisePurchase($data->vehicle_reg_no,$data->wsitemname,$data->vehicle_model,$_GET["Wsstocks"]["from_date"],$_GET["Wsstocks"]["to_date"])',
	        ),
	         array(
	                'header' => 'Total Issue Qty',               
	                'value'=>'$data->getTotalIssue($data->vehicle_reg_no,$data->wsitemname,$data->vehicle_model)',
	        ),
	          array(
	                'header' => 'Date Wise Issue Qty',               
	                'value'=>'$data->getDateWiseIssue($data->vehicle_reg_no,$data->wsitemname,$data->vehicle_model,$_GET["Wsstocks"]["issue_from_date"],$_GET["Wsstocks"]["issue_to_date"])',
	        ),
	        'available_stock',
	        'price',        
	        array(
	                        'name'=>'total_amount',
	                        'footer'=>'Grand Total: ' . $model->getTotal($model->availablelist()->getData(), 'total_amount'),
	        ),
	        array(
	                'header' => 'Consumtion Rate',               
	                'value'=>'$data->getConsumtionRate($data->vehicle_reg_no,$data->wsitemname,$data->vehicle_model,$_GET["Wsstocks"]["from_date"],$_GET["Wsstocks"]["to_date"],$_GET["Wsstocks"]["issue_from_date"],$_GET["Wsstocks"]["issue_to_date"])',
	        ),
	            ),

        ));
        Yii::app()->end();
    }

}
