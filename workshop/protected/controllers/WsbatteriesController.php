<?php

class WsbatteriesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','excel','update','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
                'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Wsbatteries;
		$remarkstype =Array(
            ''=>'',
            //'No Claim'=> 'No Claim',
            //'Warranty Claimed'=> 'Warranty Claimed',
            //'Replaced'=> 'Replaced'
        );
		$purchaseStatus=PurchaseStatus::model()->findAll();
		foreach ($purchaseStatus as $key => $value) {
			$remarkstype[$value->purchase_status]=$value->purchase_status;
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsbatteries']))
		{
			$purchase_date = $_POST['Wsbatteries']['purchase_date'];
            $warranty_valid_upto = date('Y-m-d', strtotime("+1 years", strtotime($purchase_date)));
            $_POST['Wsbatteries']['warrenty_valid_upto'] = $warranty_valid_upto;


            $model->attributes=$_POST['Wsbatteries'];
            
            //if(!empty($model->quantity)){
                $model->net_amount= $model->net_amount;
              //  }

                $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));
                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";
                $command = Yii::app()->db->createCommand($sql);
                $result = $command->queryAll();

                if(empty ($result))
                {
                    $temp_total = 0;
                } else{
                    $total = $result[0]['totalp'] + $model->net_amount;
                }


                $d[0]['total_amount'] = $total;

                Defects::model()->updateByPk($model->defect_id, array(
                    'total_amount' =>  $d[0]['total_amount']

                ));
            $model->created_by = Yii::app()->user->username;

			if($model->save()){
				Yii::app()->user->setFlash('success', '<strong>Success!</strong> New Battery saved successfully.');
				if(!isset($_GET['redirectUrl']))
				{
					//$this->redirect(array('index'));
					$this->redirect(array('index','id'=>$model->id));
				}
				else
			$this->redirect(array('view','id'=>$model->id));
		}
			
		}

        
	if(Yii::app()->request->isAjaxRequest)
			$this->renderPartial('create',array(
				'model'=>$model,
                'remarkstype'=>$remarkstype,
			), false, true);
			else{
		$this->render('index',array(
			'model'=>$model,
            'remarkstype'=>$remarkstype,
		));
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsbatteries']))
		{
            $purchase_date = $_POST['Wsbatteries']['purchase_date'];
            $warranty_valid_upto = date('Y-m-d', strtotime("+1 years", strtotime($purchase_date)));
            $_POST['Wsbatteries']['warrenty_valid_upto'] = $warranty_valid_upto;

			$model->attributes=$_POST['Wsbatteries'];
            $model->updated_by = Yii::app()->user->username;

			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
        $remarkstype =Array(
            ''=>'',
            'No Claim'=> 'No Claim',
            'Warranty Claimed'=> 'Warranty Claimed',
            'Replaced'=> 'Replaced'
        );

		$this->render('update',array(
			'model'=>$model,
            'remarkstype'=>$remarkstype,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $model=new Wsbatteries('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsbatteries']))
            $model->attributes=$_GET['Wsbatteries'];

        $this->render('index',array(
            //'dataProvider'=>$dataProvider,
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Wsbatteries('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsbatteries']))
			$model->attributes=$_GET['Wsbatteries'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Wsbatteries::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wsbatteries-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    public function actionExcel()
    {
        $model=new Wsbatteries('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'iFleet_Wsbatteries',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Wsbatteries',

            'columns'=>array(
                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),
                'defect_id',
                'vehicle_reg_no',
                array(
                    'name' => 'vehicle_type',
                    'type'=>'raw',
                    'value' => '$data->vehicletypes->type',
                ),
                //'vehicle_reg_no',
		                'particulars',
		         'battery_size',
		        'purchase_date',
		        'requisition_no',
		        'km_reading',
		        'supplier',
				'challan_no',
		        'challan_date',
		        'bill_no',
		        'bill_date',
		        'quantity',
		        'unit_price',
		        'price',
		        'discount',
				'net_amount',
                'warrenty_month',
                array(
                    'name' => 'warrenty_valid_upto',
                    'type'=>'raw',
                    'cssClassExpression' => '(strtotime(date("Y-m-d"))-strtotime($data->warrenty_valid_upto))>0 ? "highlight_cell" : ""',
                ),
                'status',


                /*
                'created_time',
                'created_by',
                'active',
                */
            ),

        ));
        Yii::app()->end();
    }
}
