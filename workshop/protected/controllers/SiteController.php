
<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('test', 'pickAndDropStats', 'marketplaceStats', 'requisitionStats', 'nextDayRequisitions','myDesk','autoLogin', 'getDriverDetail', 'getStoppageList'),
               'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'contact', 'error', 'page', 'getVehicleList', 'topDataAvailable', 'topDataOnDuty', 'topDataWorkshop', 'topDataStaffPickDrop', 'searchByCriteria', 'getRequisitionDetail', 'getDefectDetail','userRequisitions','userpickAndDrop','userShohochor','userBlance','admin_top_menu','userSummary','seatBookingForm','userReport','adminDashboard'),
                'users' => array('@'),
            ),
            /*array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),*/
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'

        //if (Yii::app()->user->isAdmin() =='1'){ 
           // $cs=Yii::app()->clientScript;
            //$cs->scriptMap=array('pnd-dashboard.css'=>Yii::app()->baseUrl.'/../common/assets/css/pnd-dashboard.css',);
           // $cs->registerCssFile('pnd-dashboard.css');           

            
        //} else {

            // $cs=Yii::app()->clientScript;
            // $cs->scriptMap=array(
            //     'pnd-dashboard.css'=>Yii::app()->baseUrl.'/../common/assets/css/pnd-dashboard.css',
            // );
            // $cs->registerCssFile('pnd-dashboard.css');

           /* $total_sell = Yii::app()->db->createCommand('SELECT COALESCE(SUM(t.price),0) total_sell FROM tbl_marketplace t WHERE t.username="'.Yii::app()->user->username.'" AND t.status = 1')->queryRow();
            $total_purchase = Yii::app()->db->createCommand('SELECT COALESCE(SUM(t.price),0) total_purchase FROM tbl_booking t WHERE t.booked_by="'.Yii::app()->user->username.'"')->queryRow();

            $user_data['total_sell'] = $total_sell['total_sell'];
            $user_data['total_purchase'] = $total_purchase['total_purchase'];

            $criteria_sell = new CDbCriteria;
            $criteria_sell->select = 't.*, b.id booking_id';
            $criteria_sell->join = 'LEFT JOIN tbl_booking b ON b.marketplace_id=t.id';
            $criteria_sell->condition = 't.username=:username';
            $criteria_sell->params = array(':username' => Yii::app()->user->username);
            $criteria_sell->order = "t.created_time DESC";
            $criteria_sell->limit=5;

            $recent_sell = Marketplace::model()->findAll($criteria_sell);

            $criteria_booking = new CDbCriteria;
            $criteria_booking->select = 't.*';
            $criteria_booking->join = 'LEFT JOIN tbl_marketplace m ON t.marketplace_id = m.id';
            $criteria_booking->condition = 't.booked_by=:booked_by_user';
            $criteria_booking->params = array(':booked_by_user' => Yii::app()->user->username);
            $criteria_booking->order = "t.created_time DESC";
            $criteria_booking->limit=5;

            $recent_booking = Booking::model()->findAll($criteria_booking);*/
            //$this->render('index', array('recent_sell' => $recent_sell, 'recent_booking' => $recent_booking, 'user_data' => $user_data));     
            
        
            $this->layout = "//layouts/main_home";
            $notice=HomePageNotification::model()->findAll();            
            $this->render('user_dashboard',array('notice'=>$notice));
        
        //}
    }
    public function actionTest()
    {
        $this->layout = "//layouts/main_home";

        $comments = Homepage::model()->findAll(array("condition"=>"type = 'COMMENT'"));//array("condition"=>"email_id =  $id","order"=>"id")
        $update_counter = Yii::app()->db->createCommand()->update('visitor_counter', array(
                'counter'=>new CDbExpression('counter + 1'),
            )
        );
        $routes = Yii::app()
            ->db
            ->createCommand(
                "select r.id, r.route_no, r.route_detail, r.actual_seat, count(*) current_seat_count 
                  from tbl_routes r join tbl_commonfleets c on r.route_no = c.present_route
                  group by r.route_no
                  order by r.route_no")
            ->queryAll();

        $marketplace = Yii::app()
            ->db
            ->createCommand(
                "select count(*) rcount from tbl_routes r 
                  join ifleetdb.tbl_marketplace m on r.id = m.route_id
                  where m.status is false and m.release_date = current_date()")
            ->queryRow();

        $r_avail = array_sum(array_map(function($r) {return $r['actual_seat'];}, $routes)) - array_sum(array_map(function($r) {return $r['current_seat_count'];}, $routes));
        $this->render('test', array(
            'comments' => $comments,
            'counts' => array(
            'route_seats' => $r_avail,
            'market_seats' => $marketplace['rcount']
            )
        ));
    }
    public function actionMyDesk()
    {
        $this->layout = "//layouts/myDesk";       
        $this->render('myDesk');
    }

    public function actionGetVehicleList(){
        $sql = "";
        $ws_sql = "SELECT d.vehicle_reg_no FROM tbl_defects d LEFT JOIN tbl_deliveryvehicles dv ON d.id=dv.defect_id LEFT JOIN tbl_vehicles v ON d.vehicle_reg_no=v.reg_no LEFT JOIN tbl_vehicletypes vt ON d.vehicletype_id=vt.id WHERE (v.`location`='HO' OR v.dedicate = 'Dedicate') AND dv.delivery_date IS NULL AND vt.type<>'Ambulance' AND vt.type<>'Refrigerator Van' ORDER BY d.`vehicletype_id` ASC ";
        $ws_vehicles = Yii::app()->db->createCommand($ws_sql)->queryAll();

        if ( (time() > strtotime("05:30:00") && time() < strtotime("09:00:00")) || (time() > strtotime("17:00:00") && time() < strtotime("21:00:00"))) {
            $pd_sql = "SELECT v.reg_no as vehicle_reg_no FROM tbl_vehicles v LEFT JOIN tbl_commonfleets c ON v.reg_no=c.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND c.application_type='Pick and Drop' AND c.approve_status='Approve' AND c.`vehicle_reg_no`<>''  AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC ";
            $pd_vehicles = Yii::app()->db->createCommand($pd_sql)->queryAll();
        }else{
            $pd_vehicles = array();
        }
        $v = array_merge($ws_vehicles, $pd_vehicles);
        $total = array_unique($v, SORT_REGULAR);

        if ($_GET['searchType'] == 'onduty'){
            /* GROUP BY used to avoid multiple requisition of same vehicle */
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.`location`='HO' AND ((v.`location`='HO' AND r.active=2) OR v.dedicate='Dedicate') AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'OnDutyDedicated'){
            /* GROUP BY used to avoid multiple requisition of same vehicle */
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.`location`<>'HO' AND v.dedicate = 'Dedicate' AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'available'){
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.`location`='HO' AND v.dedicate<>'Dedicate' AND r.start_date IS NULL AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'dedicated'){
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.dedicate = 'Dedicate' and r.start_date is null AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'workshop'){
            $sql = "SELECT d.vehicle_reg_no FROM tbl_defects d LEFT JOIN tbl_deliveryvehicles dv ON d.id=dv.defect_id LEFT JOIN tbl_vehicles v ON d.vehicle_reg_no=v.reg_no LEFT JOIN tbl_vehicletypes vt ON d.vehicletype_id=vt.id WHERE (v.`location`='HO' OR v.dedicate = 'Dedicate') AND dv.delivery_date IS NULL AND vt.type<>'Ambulance' AND vt.type<>'Refrigerator Van' ORDER BY d.`vehicletype_id` ASC ";
        }elseif ($_GET['searchType'] == 'staffpickdrop'){
            $sql = "SELECT v.reg_no as vehicle_reg_no FROM tbl_vehicles v LEFT JOIN tbl_commonfleets c ON v.reg_no=c.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND c.application_type='Pick and Drop' AND c.approve_status='Approve' AND c.`vehicle_reg_no`<>''  AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC ";
        }
        $vehicles = Yii::app()->db->createCommand($sql)->queryAll();
        if ($_GET['searchType'] == 'available' || $_GET['searchType'] == 'onduty'){
            for ($i=0;$i<count($vehicles);$i++){
                for ($j=0;$j<count($total);$j++){
                    if ($vehicles[$i]['vehicle_reg_no'] == $total[$j]['vehicle_reg_no']){
                        unset($vehicles[$i]);
                    }
                }
            }
        }
        if ($_GET['searchType'] == 'staffpickdrop'){
            for ($i=0;$i<count($vehicles);$i++){
                for ($j=0;$j<count($ws_vehicles);$j++){
                    if ($vehicles[$i]['vehicle_reg_no'] == $ws_vehicles[$j]['vehicle_reg_no']){
                        unset($vehicles[$i]);
                    }
                }
            }
        }

        $vehicleList = array();
        foreach($vehicles as $item){
            $vehicleList[$item['vehicle_reg_no']] = $item['vehicle_reg_no'];
        }
        $this->renderPartial('vehicleList', array('vehicles'=>$vehicles));
    }


    public function actionGetRequisitionDetail()
    {
        $model = Requisitions::model()->findByPk($_GET["id"]);
        echo json_encode($model->attributes);
        die();
    }

    public function actionGetDefectDetail()
    {
        $sql = "SELECT d.id, d.vehicle_reg_no,d.driver_name,d.driver_pin,d.apply_date, vt.`type` FROM tbl_defects d JOIN `tbl_vehicletypes` vt ON d.`vehicletype_id`=vt.`id` WHERE d.`id`='" . $_GET['id']."'";
        $c = Yii::app()->db->createCommand($sql);
        $result = $c->queryRow();
        echo json_encode($result);
        die();
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    public function getTopTable($count){
        return  "<table class='top-table'>" .
            "<tr><td>Bus</td><td> : </td><td>" . (isset($count['Bus'])?$count['Bus']:0) . "</td></tr>" .
            "<tr><td>Car</td><td> : </td><td>" . (isset($count['Car'])?$count['Car']:0) . "</td></tr>" .
            "<tr><td>Jeep</td><td> : </td><td>" . (isset($count['Jeep'])?$count['Jeep']:0) . "</td></tr>" .
            "<tr><td>Microbus</td><td> : </td><td>" . (isset($count['Microbus'])?$count['Microbus']:0) . "</td></tr>" .
            "<tr><td>Pickup/Delivery Van</td><td> : </td><td>" . (isset($count['Pickup/Delivery Van'])?$count['Pickup/Delivery Van']:0) . "</td></tr>" .
            "</table>";
    }

    public function actionRequisitionStats() {
        $pending = Yii::app()->db->createCommand("SELECT COUNT(*) as pending FROM tbl_requisitions r WHERE r.start_date = DATE_ADD(CURRENT_DATE(), INTERVAL  +1 DAY) AND active = 0")->queryRow();
        $notapproved = Yii::app()->db->createCommand("SELECT COUNT(*) as notapproved FROM tbl_requisitions r WHERE r.start_date = DATE_ADD(CURRENT_DATE(), INTERVAL  +1 DAY) AND active = 1")->queryRow();
        $approved = Yii::app()->db->createCommand("SELECT COUNT(*) as approved FROM tbl_requisitions r WHERE r.start_date = DATE_ADD(CURRENT_DATE(), INTERVAL  +1 DAY) AND active = 2")->queryRow();
        $next_day_data = new stdClass();
        $next_day_data->approved = $approved['approved'];
        $next_day_data->notapproved = $notapproved['notapproved'];
        $next_day_data->pending = $pending['pending'];

        $pending = Yii::app()->db->createCommand("SELECT COUNT(*) as pending FROM tbl_requisitions r WHERE r.start_date = CURRENT_DATE() AND active = 0")->queryRow();
        $notapproved = Yii::app()->db->createCommand("SELECT COUNT(*) as notapproved FROM tbl_requisitions r WHERE r.start_date = CURRENT_DATE() AND active = 1")->queryRow();
        $approved = Yii::app()->db->createCommand("SELECT COUNT(*) as approved FROM tbl_requisitions r WHERE r.start_date = CURRENT_DATE() AND active = 2")->queryRow();
        $today_data = new stdClass();
        $today_data->approved = $approved['approved'];
        $today_data->notapproved = $notapproved['notapproved'];
        $today_data->pending = $pending['pending'];
        echo json_encode(array(
            'todayData'=> $today_data,
            'nextDayData'=> $next_day_data
        ));
        die;
    }

    /**
     * @return string
     */
    public function actionPickAndDropStats(){
        $routes = Yii::app()
            ->db
            ->createCommand(
                "select r.id, r.route_no, r.route_detail, r.actual_seat, count(*) current_seat_count, vt.type 
                  from tbl_routes r join tbl_commonfleets c on r.route_no = c.present_route
                  join tbl_vehicletypes vt on c.vehicletype_id = vt.id 
                  group by r.route_no
                  order by r.route_no")
            ->queryAll();
        $html =
            "<table class='table table-responsive table-hover'>
                <tr>
                    <th style='width: 10%'>Route No </th>
                    <th style='width: 45%'>Route Detail</th>
                    <th style='width: 10%'>Total Seats</th>
                    <th style='width: 10%'>Available</th>
                    <th style='width: 10%'>Waiting</th>
                    <th style='width: 15%'>Vehicle Type</th>
                </tr>";
        foreach ($routes as $route){
            $stoppages = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage where route_id = '".$route['id']."' order by pickup_time asc")->queryAll();
            $stoppages = implode(", ", array_map(function ($s){return $s['stoppage'];}, $stoppages));

            $sql_queue="SELECT max(queue) as queue FROM tbl_seat_request WHERE status=1 AND route_id='".$route['id']."' AND queue >= 0";
            $queue=Yii::app()->db->createCommand($sql_queue)->queryRow();
            $available_seat = $route['actual_seat']-$route['current_seat_count'];

            $route_id = $route['id'];
            $html .=
                "<tr>".
                    "<td><a class='cursor-pointer' onclick='toggleDriverDetail($route_id)'>".$route['route_no']."</a></td>".
                    "<td>".$stoppages."</td>".
                    "<td>".$route['actual_seat']."</td>".
                    "<td>".$available_seat."</td>".
                    "<td>".($available_seat > 0 ? 0 : $queue['queue'] + 1)."</td>".
                    "<td>".$route['type']."</td>".
                "</tr>";
        }
        if (count($routes) == 0) $html .= "<tr><td colspan='3'><i>No record found</i></td></tr>";
        $html .= "</table>";
        echo $html;die;
    }

    public function actionGetDriverDetail() {
        $route_id = $_GET['routeId'];
        $driver = Yii::app()->db
            ->createCommand()
            ->select('d.*, v.reg_no, r.route_no, r.id route_id')
            ->from('tbl_drivers d')
            ->join('tbl_vehicles v', 'v.driver_pin = d.pin')
            ->join('tbl_routes r', 'r.vehicle_reg_no = v.reg_no')
            ->where('r.id = :routeId', array(':routeId'=>$route_id))
            ->limit(1)
            ->queryRow();

        $stoppages =  Yii::app()->db
            ->createCommand()
            ->select('s.*')
            ->from('tbl_stoppage s')
            ->where('s.route_id = :routeId', array(':routeId'=>$route_id))
            ->order('pickup_time asc')
            ->queryAll();

        if (empty($driver)){
            echo "<div class='col_full'><button class=\"btn btn-primary\" onclick=\"toggleDriverDetail()\">Back to List</button></div>";
            echo "<div class=\"promo promo-light bottommargin\">
						<h3>This route have no vehicle.</h3>
						<span>Call us at <span style='color: #1ABC9C;'>+880-172-XXXXXX</span> or Email us at <span style='color: #1ABC9C;'>support@ifleet.brac.net</span></span>
						
					</div>";
            die;
        }
        $html = $this->renderPartial('driverdetail', array(
            'driver'=>$driver,
            'stoppages'=>$stoppages
        ));
        echo $html;
        die;
    }

    /**
     * @return string
     */
    public function actionMarketplaceStats(){
        $routes = Yii::app()
            ->db
            ->createCommand(
                "select r.id, r.route_no, r.route_detail, count(*) seats_for_sale, vt.type from tbl_routes r 
                  join tbl_marketplace m on r.id = m.route_id
                  join tbl_vehicles v on r.vehicle_reg_no = v.reg_no
                  join tbl_vehicletypes vt on v.vehicletype_id = vt.id
                  where m.status is false and m.release_date = current_date() 
                  group by r.id
                  order by r.id")
            ->queryAll();
        $html =
            "<table class='table table-responsive table-hover'>
                <tr>
                    <th style='width: 15%'>Route No </th>
                    <th style='width: 60%'>Route Detail</th>
                    <th style='width: 10%'>Vehicle Type</th>
                    <th style='width: 15%'>Seats Available</th>
                </tr>";
        foreach ($routes as $route){
            $route_id = $route['id'];
            $stoppages = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage where route_id = '".$route['id']."' order by pickup_time asc")->queryAll();
            $stoppages = implode(", ", array_map(function ($s){return $s['stoppage'];}, $stoppages));
            $html .=
                "<tr>".
                    "<td><a class='cursor-pointer' onclick='toggleStoppageList($route_id)'> ".$route['route_no']."</a></td>".
                    "<td>".$stoppages."</td>".
                    "<td>".$route['type']."</td>".
                    "<td><a class='cursor-pointer' onclick='openMarketplaceWindow()' title='Click here for booking'>".$route['seats_for_sale']."</a></td>".
                "</tr>";
        }
        if (count($routes) == 0) $html .= "<tr><td colspan='3'><i>No record found</i></td></tr>";
        $html .= "</table>";
        echo $html;die;
    }

    public function actionGetStoppageList() {
        $route_id = $_GET['routeId'];
        $stoppages =  Yii::app()->db
            ->createCommand()
            ->select('s.*')
            ->from('tbl_stoppage s')
            ->where('s.route_id = :routeId', array(':routeId'=>$route_id))
            ->order('pickup_time asc')
            ->queryAll();
        $html = $this->renderPartial('stoppage_detail', array(
            'stoppages'=>$stoppages
        ));
        echo $html;
        die;
    }

    /**
     * Displays the login page
     */
    /*public function actionLogin()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }*/

    /**
     * Logs out the current user and redirect to homepage.
     */

    /*
     public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }*/
   public function actionAutoLogin(){ 
        /*$multipass = $_GET['multipass'];
        $ssoSubject = $this->decrypt_data($multipass, "sso.my8r4c.l1234");
        //var_dump($ssoSubject);exit();
        $ssoInfo = explode("|", $ssoSubject);
        $name = explode(":", $ssoInfo[0]); // name:
        $auth = explode(":", $ssoInfo[3]); // authenticated:true
        if ($auth[1] == "false") {
            $message= explode(":", $ssoInfo[10]);
            $this->redirect(Yii::app()->request->baseUrl.'/index.php//user/login?message="'.$message[1].'"');
        }
        else {
            $pin = $name[1];*/
            $pin = $_GET['pin'];
            $image_url='../photo/'.$pin.".png";
            $base64string=file_get_contents("http://api.brac.net/v1/staffs/".$pin."/photo?Key=960d5569-a088-4e97-91bf-42b6e5b6d101");
            file_put_contents($image_url, base64_decode($base64string)); 
    
            $uri = "http://api.brac.net/v1/staffs/".$pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
            $staffInfo = CJSON::decode(file_get_contents($uri));   
    
            $first_name=substr($staffInfo[0]['StaffName'],0,strrpos($staffInfo[0]['StaffName'],' '));
            $last_name=substr($staffInfo[0]['StaffName'],-(strlen($staffInfo[0]['StaffName'])-(strrpos($staffInfo[0]['StaffName'],' '))));
            
            
            $model=new UserLogin;
           
            $model->username = "myDesk";
            $model->password = "654@321";
            if($model->validate()) {  
            Yii::app()->user->username = $pin;                
            Yii::app()->user->email = $staffInfo[0]['EmailID'];               
            Yii::app()->user->picmarket_user = 1;                
            Yii::app()->user->lastvisit_at = date('Y-m-d H:i:s');   
             Yii::app()->user->firstname = $first_name;             
             Yii::app()->user->lastname = $last_name;             
             Yii::app()->user->level = (int)$staffInfo[0]['JobLevel'];             
             Yii::app()->user->department = $staffInfo[0]['ProjectName'];              
             Yii::app()->user->designation = $staffInfo[0]['DesignationName'];          
             Yii::app()->user->phone = $staffInfo[0]['MobileNo'];             
             Yii::app()->user->address = $staffInfo[0]['PresentAddress']; 
             Yii::app()->user->myDeskUser = 1;           
             $this->redirect(Yii::app()->request->baseUrl);
                       /* if (Yii::app()->user->returnUrl=='/index.php'){
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                        }
                        else{
                            $this->redirect(Yii::app()->user->returnUrl);
                        }*/
           // }
        }
        /*$uri = "http://api.brac.net/v1/staffs/".$model->username."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
        $uri = "http://api.brac.net/v1/staffs/".$model->username."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
        $staffInfo = CJSON::decode(file_get_contents($uri));
        //$stafname=explode(' ', "$staffInfo[0]['StaffName']");
        var_dump($staffInfo );

         $first_name=substr($staffInfo[0]['StaffName'],0,strpos($staffInfo[0]['StaffName'],' '));
         $last_name=substr($staffInfo[0]['StaffName'],strpos($staffInfo[0]['StaffName'],' '),-1);
        $model=new User;
        $model->username=$_GET['pin'];
        $model->password=$_GET['mydesk'];
        $model->email=$staffInfo[0]['EmailID'];
        $model->activkey=md5(1234);        
        $model->picmarket_user=1;
        $model->create_at=date('Y-m-d');
        $model->superuser=0;
        $model->status=1;
        $model->lastvisit_at=date('Y-m-d H:i:s');

        $model_profile= New Profile;
        $model_profile->lastname=$last_name;
        $model_profile->firstname=$first_name;
        $model_profile->level= (int)$staffInfo[0]['JobLevel'];
        $model_profile->department=$staffInfo[0]['ProjectName'];
        $model_profile->designation=$staffInfo[0]['DesignationName'];
        $model_profile->phone=$staffInfo[0]['MobileNo'];
        $model_profile->address=$staffInfo[0]['PresentAddress'];
        var_dump($model);
        
        //var_dump($staffInfo);
        /*$model->user_level = $staffInfo[0]['JobLevel'];
        $model->user_name= $staffInfo[0]['StaffName'];
        $model->email=$staffInfo[0]['EmailID'];
        $model->user_cell=$staffInfo[0]['MobileNo'];
        $model->user_department=$staffInfo[0]['ProjectName'];
        $model->user_designation=$staffInfo[0]['DesignationName'];
        $model->residence_address=$staffInfo[0]['PresentAddress'];
        $model->created_time = date('Y-m-d H:i:s');*/
    }

    public function decrypt_data($data, $key) {
        $ret=openssl_decrypt($data, "AES-128-CBC", $key, 0, "");
        return trim($ret);
    }
  
    public function actionUserRequisitions() {
        $i_req_app = Yii::app()->db->createCommand('SELECT COUNT(active) as app FROM tbl_requisitions WHERE active = 2')->queryRow();
        $i_req_n_app = Yii::app()->db->createCommand('SELECT COUNT(active) as napp FROM tbl_requisitions WHERE active = 1')->queryRow();
        $i_req_p_app = Yii::app()->db->createCommand('SELECT COUNT(active) as papp FROM tbl_requisitions WHERE active = 0')->queryRow();
        $u_req_app = Yii::app()->db->createCommand('SELECT COUNT(active) as app FROM tbl_requisitions WHERE active = 2 AND user_pin = "'.Yii::app()->user->username.'"')->queryRow();
        $u_req_n_app = Yii::app()->db->createCommand('SELECT COUNT(active) as napp FROM tbl_requisitions WHERE active = 1 AND user_pin = "'.Yii::app()->user->username.'"')->queryRow();
        $u_req_p_app = Yii::app()->db->createCommand('SELECT COUNT(active) as papp FROM tbl_requisitions WHERE active = 0 AND user_pin = "'.Yii::app()->user->username.'"')->queryRow();
        $u_req_app_five = Yii::app()->db->createCommand('SELECT * FROM tbl_requisitions WHERE active = 2 AND user_pin = "'.Yii::app()->user->username.'" ORDER BY id Limit 5')->queryALL();
        $pichar_array = array(
            'i_req_app' => $i_req_app['app'],
            'i_req_n_app' => $i_req_n_app['napp'],
            'i_req_p_app' => $i_req_p_app['papp'],
            'u_req_app' => $u_req_app['app'],
            'u_req_n_app' => $u_req_n_app['napp'],
            'u_req_p_app' => $u_req_p_app['papp'],
        );
       
        $this->render('user_requisitions', array('pichar_array' => $pichar_array,'u_req_app_five' => $u_req_app_five));
    }
    public function actionUserpickAndDrop() {
         $sms_email = Yii::app()->db->createCommand('SELECT s.* FROM tbl_seat_request as r RIGHT JOIN tbl_email_sms as s ON r.route_id = s.route_id WHERE r.user_pin ="'.Yii::app()->user->username.'"  AND r.status=1 AND r.queue=-1 ORDER BY s.created_time DESC Limit 5')->queryAll();
         
    $this->render('user_pick_drop',array('sms_email' => $sms_email));
    }
    public function actionUserShohochor() {
        $model = new SeatSale('search');
        $model->unsetAttributes();
        $use_route = Yii::app()->db->createCommand("SELECT r.* FROM  tbl_commonfleets as c INNER JOIN  tbl_routes as r ON r.route_no=c.present_route WHERE c.application_type<>'Cancel Seat' AND c.user_pin='".Yii::app()->user->username."'")->queryRow();

        $valid_routes = Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id INNER JOIN tbl_stoppage as s ON s.route_id=r.id GROUP BY r.route_no")->queryAll();    
        
           //  "HO OUT";
         if(date("H:i:s")>="08:30:00" && date("H:i:s")<="17:00:00"){
            $routes_out = Yii::app()->db->createCommand("SELECT r.*, sa.from_date, sa.travel_direction, sa.id as sid FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_stoppage as s ON s.route_id=r.id INNER JOIN tbl_seat_sale as sa ON sa.route_id=r.id WHERE sa.travel_direction=2 AND sa.from_date='".date("Y-m-d")."' GROUP BY sa.id")->queryAll();
        }    
            // "HO IN";
        if(date("H:i:s")>="08:30:00" && date("H:i:s")<="20:30:00"){
            $routes_in = Yii::app()->db->createCommand("SELECT r.*, sa.from_date, sa.travel_direction, sa.id as sid FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_stoppage as s ON s.route_id=r.id INNER JOIN tbl_seat_sale as sa ON sa.route_id=r.id WHERE sa.travel_direction=1 AND sa.from_date='".date("Y-m-d", strtotime("+1 day"))."' GROUP BY sa.id")->queryAll();
        }

        $route_list=array();
        foreach ($valid_routes as $key => $value) {
           $seats_oc = Routes::borderPass($value['route_no']);           
           $route_seat_sale_out = Yii::app()->db->createCommand("SELECT count(id) as rs FROM tbl_seat_booking WHERE route_id='".$value['id']."' AND sale_id=0 AND date_from='".date('Y-m-d')."' AND direction_travale=2 AND status=0")->queryRow();
           $route_seat_sale_in = Yii::app()->db->createCommand("SELECT count(id) as rs FROM tbl_seat_booking WHERE route_id='".$value['id']."' AND sale_id=0  AND date_from='".date("Y-m-d", strtotime("+1 day"))."' AND direction_travale=1 AND status=0")->queryRow();                 
           $seats_av_out=$value['actual_seat']-$seats_oc-$route_seat_sale_out['rs'];
           $seats_av_in=$value['actual_seat']-$seats_oc-$route_seat_sale_in['rs'];          
           if($seats_av_out>0 && date("H:i:s")>="08:30:00" && date("H:i:s")<="17:00:00") {
            $route_list[$value['id']."O"] = array(
                'id'=>$value['id'],
                'route_no'=>$value['route_no'],
                'av'=>$seats_av,
                'type'=>$value['type'],
                'market_price'=>$value['market_price'],
                'travel_direction'=>"HO OUT",
                'date' => date('Y-m-d'),
                );
            }
            if($seats_av_in>0 && date("H:i:s")>="08:30:00" && date("H:i:s")<="20:30:00") {
            $route_list[$value['id']."I"] = array(
                'id'=>$value['id'],
                'route_no'=>$value['route_no'],
                'av'=>$seats_av,
                'type'=>$value['type'],
                'market_price'=>$value['market_price'],
                'travel_direction'=>"HO IN",
                'date' => date("Y-m-d", strtotime("+1 day")),
                );
           }
        }  

        if(isset($routes_out) && count($routes_out)>0) {
            foreach ($routes_out as $key => $value) {
                $check_sale = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_booking WHERE sale_id='".$value["sid"]."' AND status=0")->queryRow();
                if(!$check_sale) {
                    if (array_key_exists($value['id']."O", $route_list)) {                           
                        $route_list[$value['id']."O"]['av'] = $route_list[$value['id']."O"]['av']+1; 
                         $route_list[$value['id']."O"]['travel_direction']='HO OUT';                 
                        $route_list['s'.$value['sid']."O"] = $route_list[$value['id']."O"];
                        unset($route_list[$value['id']."O"]);
                       
                    }
                    else {
                        $route_list['s'.$value['sid']."O"]=array(
                        'id'=>$value['id'],
                        'route_no'=>$value['route_no'],
                        'av'=>$route_list[$value['id']."O"]['av']+1,
                        'type'=>$value['type'],
                        'market_price'=>$value['market_price'],
                        'travel_direction'=>'HO OUT',
                        'date' => date('Y-m-d'),
                        );
                    } 
                }             
            }
        }
      
        if(isset($routes_in) && count($routes_in)>0) {
            foreach ($routes_in as $key => $value) {
                $check_sale = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_booking WHERE sale_id='".$value["sid"]."' AND status=0")->queryRow();
                if(!$check_sale) {
                    if (array_key_exists($value['id']."I", $route_list)) {                         
                         $route_list[$value['id']."I"]['av']=$route_list[$value['id']."I"]['av']+1; 
                         $route_list[$value['id']."I"]['travel_direction']='HO IN'; 
                        $route_list['s'.$value['sid']."I"]= $route_list[$value['id']."I"];
                        unset($route_list[$value['id']."I"]); 
                    }
                    else {                                                
                        $route_list[$value['id']."I"]=array(
                        'id'=>$value['id'],
                        'route_no'=>$value['route_no'],
                        'av'=>$route_list[$value['id']."I"]['av']+1,
                        'type'=>$value['type'],
                        'market_price'=>$value['market_price'],
                        'travel_direction'=>'HO IN',
                        'date' => date("Y-m-d", strtotime("+1 day")),
                        );                   
                        
                    }
                }             
            }
        }     
      
        $booking = Yii::app()->db->createCommand("SELECT b.*, r.route_no FROM tbl_seat_booking as b INNER JOIN tbl_routes as r ON r.id=b.route_id WHERE b.user_pin='".Yii::app()->user->username."' ORDER BY id desc LIMIT 5")->queryAll();
        $this->render('user_shohochor',array(
            'use_route'=>$use_route,
            'model'=>$model,
            'route_list'=>$route_list,
            'booking' => $booking
            ));
    }
    public function actionUserBlance() {
    $this->render('user_blance');
    }
    public function actionAdmin_top_menu() {
    $this->renderPartial('/site/admin_top_menu');
    }
    public function actionUserSummary(){ 
        $seats_av = 0;
        $route = Yii::app()->db->createCommand("SELECT r.* FROM tbl_commonfleets as c INNER JOIN tbl_routes as r on r.route_no=c.present_route WHERE c.user_pin='".Yii::app()->user->username."' AND c.application_type<>'Cancel Seat'")->queryRow();
        $seats = Routes::borderPass($route['route_no']); 
        $sho_seat = Yii::app()->db->createCommand("SELECT count(id) as av FROM tbl_seat_sale WHERE route_id='".$route['id']."' AND from_date='".date("Y-m-d")."'")->queryRow();
        if($sho_seat && $route)
        $seats_av = $sho_seat['av'] + $seats;  

        $sms_email = Yii::app()->db->createCommand("SELECT * FROM tbl_email_sms WHERE route_id='".$route['id']."' ORDER BY id DESC LIMIT 3")->queryAll();
        $this->render('user_summary',array('seats_av' => $seats_av,'sms_email'=>$sms_email));
    }
    public function actionSeatBookingForm(){    
        
        $sale = Yii::app()->db->createCommand("SELECT s.*, r.route_no FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id WHERE s.id='".$_POST['sid']."' ORDER BY id desc")->queryRow();
        
        $this->renderPartial('/site/booking_form', array(
            'sale'=>$sale
        ));
         return;
    }
    
    public function actionAdminDashboard(){
        // Yii::app()->user->picmarket_user=0;
        // $model=new SeatRequestAdmin('search');
        // $model->unsetAttributes();  // clear any default values
       $this->render('index_admin');
    }
    
}