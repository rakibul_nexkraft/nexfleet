<?php

class DeliveryvehiclesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','excel','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Deliveryvehicles;
        $model->created_by = Yii::app()->user->username;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Deliveryvehicles']))
		{
			$model->attributes=$_POST['Deliveryvehicles'];
			if($model->save())
			{
				$driver_pin=$model->driver_pin;
				$driver_cell=Drivers::model()->findByAttributes(
   					 array('pin'=>$driver_pin));
				$driver_cell=$driver_cell->phone;				
				$this->actionSmssend($model->defect_id,$driver_cell,$model->vehicle_reg_no,$model->created_time);
				$this->actionSmssend($model->defect_id,'01730350917',$model->vehicle_reg_no,$model->created_time);
				$this->actionSmssend($model->defect_id,'01730351384',$model->vehicle_reg_no,$model->created_time);
				Yii::app()->user->setFlash('success', '<strong>Success!</strong> New Deliveryvehicles saved successfully.');
				if(!isset($_GET['redirectUrl']))
				{
					$this->redirect(array('index'));
				}
				else
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}
		
if(Yii::app()->request->isAjaxRequest)
			$this->renderPartial('create',array(
				'model'=>$model,
			), false, true);
			else
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->updated_by = Yii::app()->user->username;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Deliveryvehicles']))
		{
			$model->attributes=$_POST['Deliveryvehicles'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {

        $model=new Deliveryvehicles('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Deliveryvehicles']))
            $model->attributes=$_GET['Deliveryvehicles'];

        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$model->search(),

        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Deliveryvehicles('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Deliveryvehicles']))
			$model->attributes=$_GET['Deliveryvehicles'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Deliveryvehicles::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='deliveryvehicles-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}



    public function actionExcel()
    {
        $model=new Deliveryvehicles('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'iFleet_Deliveryvehicles',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Deliveryvehicles',

            'columns'=>array(
                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),

                //'vehicle_reg_no',
                'defect_id',
                'vehicle_reg_no',
                'delivery_date',
                'KM_reading',
                'driver_pin',
                'driver_name',
                'action_taken'

                /*
                'created_time',
                'created_by',
                'active',
                */
            ),

        ));
        Yii::app()->end();
    }

    public function actionSmssend($requisition_id, $user_cell, $vehicle_reg_no,  $start_date)
    {
        //$r = file_get_contents("http://mydesk.brac.net/smsc/create?token=286cf3ae67c80ee7d034bb68167ab8455ea443a2&message=test&to_number=8801919197617");

        $client = new EHttpClient(
            'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
            )
        );
		$user_cell = substr($user_cell, -11);
		$driver_cell = substr($driver_cell, -11);

        if (strlen($user_cell) == 10)
            $user_cell = "0" . $user_cell;
        else
            $user_cell = $user_cell;


        if (strlen($driver_cell) == 10)
            $driver_cell = "0" . $driver_cell;
        else
            $driver_cell = $driver_cell;
      
        

            $message = "The vehicle number $vehicle_reg_no is ready to use. Please, contact with Brac Transport Workshop";   

        $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $message,
            'to_number' => $user_cell,
            'app_url' => 'http://ifleet.brac.net/',
        ));

        // $client->set

        $response = $client->request('POST');
        $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $driver_message,
            'to_number' => $driver_cell,
            'app_url' => 'http://ifleet.brac.net/',
        ));

        $response = $client->request('POST');


    }
}
