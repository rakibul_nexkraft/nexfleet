<?php

class WsitemdistsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getRequisitionData','getItemNameStock','getVehicleModel','getPartNo','getVehicle','getItemsData','getItemsDataModel'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel','sapare','excel1'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));


	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$model=new Wsitemdists;
        $model->created_by = Yii::app()->user->username;

        $unitlist = Array(
            'Litre'=> 'Litre',
            'Kg'=> 'Kg',
            'Pkt'=> 'Pkt',
            'Pc'=> 'Pc',
            'set'=>'set'
        );
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
			if(empty($_REQUEST['id']))			
			{
					throw new CHttpException(404,'Please Pick Item from Defect.');
			}
	

		if(isset($_POST['Wsitemdists']))
		{

			$model->attributes=$_POST['Wsitemdists'];

			
            $model->wsitemname_id;

            if($model->vehicle_reg_no)
			$sql = "SELECT available_stock,temp_stock FROM tbl_wsstocks where wsitemname_id = '$model->wsitemname_id'    and vehicle_reg_no='$model->vehicle_reg_no'  AND temp_stock<>0";
            else
                $sql = "SELECT available_stock,temp_stock FROM tbl_wsstocks where wsitemname_id = '$model->wsitemname_id' AND temp_stock<>0  and vehicle_reg_no=''";

			$command = Yii::app()->db->createCommand($sql);				
			$result = $command->queryAll();
				
			$available_stock = $result[0]['available_stock'];
			//$temp_stock = $result[0]['temp_stock'];
			//var_dump($result);
			//exit();
			if($model->quantity > $available_stock)
			{
					throw new CHttpException(404,'Insufficiency of this item in stock.');
				
			}

			else
			{
                if($model->vehicle_reg_no)
				    $sql = "SELECT id,stock_in,temp_stock,available_stock,price FROM tbl_wsstocks where wsitemname_id = '$model->wsitemname_id' AND stock_in!=0 AND vehicle_reg_no = '$model->vehicle_reg_no'" ;
                else
                    $sql = "SELECT id,stock_in,temp_stock,available_stock,price FROM tbl_wsstocks where wsitemname_id = '$model->wsitemname_id' AND stock_in!=0 AND vehicle_reg_no =''";

				$command = Yii::app()->db->createCommand($sql);				
				$result = $command->queryAll();					
				
				$carry = $model->quantity;
				$price = 0;
				$i=0;
				foreach($result as $item)
				{
					if($item['temp_stock'] !=0)
					{
					  if($carry <= $item['temp_stock'])
					  {
					  	 $temp_stock[$i]['id'] = $item['id'];
					     $temp_stock[$i]['quantity'] = $item['temp_stock'] - $carry;
					     $temp_stock[$i]['carry'] = $carry;
					     $price +=  $item['price']*$carry;				     
					     $temp_stock[$i]['price'] = $item['price'];
					     break;
					  }
					  else
					  {
					  	  $temp_stock[$i]['id'] = $item['id'];					     	
					  	  $temp_stock[$i]['carry'] = $item['temp_stock'];
					      $carry = $carry - $item['temp_stock'];					     
					      
					      $temp_stock[$i]['quantity'] = 0;
					      $price +=  $item['price']*$item['temp_stock'];
					      $temp_stock[$i]['price'] = $item['price'];
					  }
					  $i++;
					}
					
					  
				}			
			}
			
			$i=0;
	
									
			foreach($temp_stock as $item)
			{
				
				$id = $item['id'];
				$sql = "UPDATE tbl_wsstocks set temp_stock=".$temp_stock[$i]['quantity']. " where id = '$id' AND temp_stock!=0";			
				$command = Yii::app()->db->createCommand($sql);				
				$resultex = $command->execute();											
			
				$i++;
			}			
			
				
			
			$model->total_price=$price;	
			$modelID=$model->item_name;
			$model->available_stock=$model->item_qunt-$model->quantity;
			$wsitems=Wsstocks::model()->find('id=:ID',array(':ID'=>$modelID));
		  $model->item_name=$wsitems['wsitemname'];
		  $model->wsitemname_id=$wsitems['wsitemname_id'];
		  $model->total_price=$model->quantity*$model->unit_price;
		 
			if($model->save())
			{
						$i=0;
																		
						foreach($temp_stock as $item)
					  {
								
							$c=new Wsitemdistsubs;       				
	            $c->wsitemdist_id=$model->id;
	            if($temp_stock[$i]['carry']  != '0')
	            {
	            $c->quantity=$temp_stock[$i]['carry'];	            
	            $c->price=$temp_stock[$i]['price'];
	          }
						  $c->save();
						  $i++;
						}												

			
						$b=new Wsstocks;
                 $b->wsitem_id=$model->wsitem_id;

            $b->wsitemname_id=$model->wsitemname_id;
            $b->wsitemname=$model->item_name;
            $b->wsdist_id=$model->id;
            $b->vehicle_reg_no=$model->vehicle_reg_no;
            $b->vehicle_model=$model->vehicle_model;
            $b->parts_no=$model->parts_no;
            $b->stock_out=$model->quantity;


            if($available_stock)
            {

            $b->available_stock=$available_stock-$model->quantity;


            }
            else
                $b->available_stock=$model->quantity;

            $b->price=0;
            $b->total_amount=$price;
						$b->issue_date=$model->issue_date;

					  $b->save();

					  			//		  echo $model->defect_id;
					  /* Start Load and update model from other controller */
					  //$d = Defects::model()->findAll($model->defect_id);
                    $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));
					  
					$temp_id=$_POST['Wsitemdists']['defect_id'];
					  
					  
					  //$sql = "SELECT sum(total_price) as totalp FROM tbl_wsitemdists where defect_id = $temp_id ";
                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";
						$command = Yii::app()->db->createCommand($sql);				
						$result = $command->queryAll();
										  
			  		if(empty ($result))
						{
						$temp_total = 0;
						} else{
						$total = $result[0]['totalp'] + $price;	
						}
						
 					 //	$d->total_amount=$total;
                    $d[0]['total_amount'] = $total;
 					
						//$d->update(array('total_amount'));
                Defects::model()->updateByPk($model->defect_id, array(
                    //'total_amount' =>  $d->total_amount,
                    'total_amount' =>  $d[0]['total_amount']

                ));
						
						/* End Load and update model from other controller */				
						
					Yii::app()->user->setFlash('success', '<strong>Success!</strong> New Item Distribution saved successfully.');
					if(!isset($_GET['redirectUrl']))
					{
						$this->redirect(array('defects/view','id'=>$_REQUEST['id'],'iditemdists'=>$model->id));
					}	
            
						$this->redirect(array('view','id'=>$model->id));

			}

		}

        if(Yii::app()->request->isAjaxRequest)
            $this->renderPartial('create',array(
                'model'=>$model,
                'unitlist'=>$unitlist

            ), false, true);
        else
            $this->render('create',array(
                'model'=>$model,
                'unitlist'=>$unitlist

            ));

/*
	$this->render('create',array(
			'model'=>$model,
		));*/
}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$item_name=$model->item_name;
		   $unitlist = Array(
            'Litre'=> 'Litre',
            'Kg'=> 'Kg',
            'Pkt'=> 'Pkt',
            'Pc'=> 'Pc',
            'set'=>'set'
        );

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
	  //throw new CHttpException(404,'Update feature is not available Now.');
			
   if(isset($_POST['Wsitemdists'])){
   	
   		$model->attributes=$_POST['Wsitemdists'];
   		$model->item_name=$item_name;
		$model->available_stock=$model->item_qunt;
			
			$sql = "SELECT defect_id,total_price FROM tbl_wsitemdists where id = $model->id ";
			$command = Yii::app()->db->createCommand($sql);				
			$result = $command->queryAll();
			
		    $old_def_id = $result[0]['defect_id'];
			$new_def_id =  $model->defect_id;
			
			$total_price=$result[0]['total_price'];
			
			if($new_def_id != $old_def_id)
			{
				$sql = "UPDATE tbl_defects set total_amount=ROUND(total_amount+$total_price) where id = '$new_def_id'";
				$command = Yii::app()->db->createCommand($sql);				
				$resultex = $command->execute();
				
				$sql = "UPDATE tbl_defects set total_amount=ROUND(total_amount-$total_price) where id = '$old_def_id'";
				$command = Yii::app()->db->createCommand($sql);				
				$resultex = $command->execute();
			}
			
			 //$model->total_price=$model->quantity*$model->unit_price;
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		
			
   	}
   	//die;
		/*	
		if(isset($_POST['Wsitemdists']))
		{
			$model->attributes=$_POST['Wsitemdists'];
			
			$sql = "SELECT available_stock FROM tbl_wsstocks where wsitemname_id = $model->wsitemname_id  order by id desc limit 1";
			$command = Yii::app()->db->createCommand($sql);				
			$result = $command->queryAll();
				
			$available_stock = $result[0]['available_stock'];
			
			if($model->quantity > $available_stock)			
			{
					throw new CHttpException(404,'Your given quantity exceed available stock.');
			}
			else
			{
				$sql = "SELECT id,stock_in,temp_stock,available_stock,price FROM tbl_wsstocks where wsitemname_id = $model->wsitemname_id";
				$command = Yii::app()->db->createCommand($sql);				
				$result = $command->queryAll();					
				
				$carry = $model->quantity;
				$price = 0;
				
				foreach($result as $item)
				{
					  if($carry <= $item['temp_stock'])
					  {
					  	echo $item['temp_stock'] - $carry;
					     $temp_stock[] = $item['temp_stock'] - $carry;
					     $price +=  $item['price']*$carry;				     
					     break;
					  }
					  else
					  {
					     	$temp_stock[] = 0;
					     $carry = $carry - $item['temp_stock'];
					     
					      $price +=  $item['price']*$item['temp_stock'];
					  }
				}			
			}
			
			$i=0;
			
			foreach($result as $item)
			{
				$id = $item['id'];
				$sql = "UPDATE tbl_wsstocks set temp_stock='$temp_stock[$i]' where id = '$id'";
				
				$command = Yii::app()->db->createCommand($sql);				
				$result = $command->execute();							
				$i++;
			}
			
			$model->total_price=$price;
			
						$b=new Wsstocks;       				
            $b->wsitemname_id=$model->wsitemname_id;
            $b->stock_out=$model->quantity;            
            $b->available_stock=$available_stock-$model->quantity;
            $b->price=0;
            $b->total_amount=$price;            
															
					  $b->save();
					  
					  $d=new Defects;
					 	$sql = "SELECT sum(total_price) as totalp FROM tbl_wsitemdists where defect_id = $model->defect_id ";
						$command = Yii::app()->db->createCommand($sql);				
						$result = $command->queryAll();
					  
			  		if(empty ($result))
						{
						$temp_total = 0;
						} else{
						$total = $result[0]['totalp'] + $price;	
						}
						echo $total;
						die;
						
						$d->total_amount=$total;            
						$d->save();
			
			
			if($model->save())
			{
					  $b=new Wsstocks;            
            $b->wsitemname_id=$model->wsitemname_id;
            $b->vehicle_model=$model->vehicle_model;
            $b->stock_out=$model->quantity;
            $b->price=0;
            $b->available_stock=$available_stock-$model->quantity;
            $b->total_amount=$price;
            $b->purchase_date=$model->created_time;    
						
            $b->update();
				$this->redirect(array('view','id'=>$model->id));
			}
		}*/

		$this->render('update',array(
			'model'=>$model,
			 'unitlist'=>$unitlist
		));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		
		throw new CHttpException(404,'You are not authorized to delete.');

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $model=new Wsitemdists('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsitemdists']))
            $model->attributes=$_GET['Wsitemdists'];

        $this->render('index',array(
            //'dataProvider'=>$dataProvider,
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
    }
public function actionSapare()
    {
        $model=new Wsitemdists('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsitemdists']))
            $model->attributes=$_GET['Wsitemdists'];

        $this->render('sapare',array(
            //'dataProvider'=>$dataProvider,
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
    }
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Wsitemdists('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsitemdists']))
			$model->attributes=$_GET['Wsitemdists'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


    public function actionGetRequisitionData() {
        $wsrequisition_id = $_REQUEST['Wsitemdists']['wsrequisition_id'];


        if (!empty($wsrequisition_id)) {

            $sql = "SELECT vehicle_reg_no	FROM tbl_wsrequisitions  WHERE id =  '$wsrequisition_id' ";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            if(!empty($result[0]['vehicle_reg_no']))
            {
                $vehicle_reg_no = $result[0]['vehicle_reg_no'];
                echo CJSON::encode($result[0]); exit;
            }
        } else {

            return false;
        }
    }




    public function actionGetVehicleModel() {
        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';
            //$sql = "SELECT v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.vehicle_model  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";
            $sql = "SELECT distinct v.vehicle_model as value FROM tbl_wsstocks as v  WHERE v.vehicle_model LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }

 /*   public function actionGetPartNo() {
        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';
            //$sql = "SELECT v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.vehicle_model  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";
            $sql = "SELECT distinct v.parts_no as value FROM tbl_wsstocks as v  WHERE v.parts_no LIKE '$qterm' and v.available_stock !=0 or v.temp_stock!=0 ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }*/

    public function actionGetPartNo() {


        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';

           // $vehicle_reg_no = $_GET['vehicle_reg_no'];
            $vehicle_model = $_GET['vehicle_model'];

            $sql = "SELECT distinct v.parts_no as value  FROM tbl_wsstocks as v WHERE v.parts_no LIKE '$qterm'
            and v.vehicle_model = '$vehicle_model' and v.available_stock !=0 and v.temp_stock!=0 ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }


    public function actionGetItemNameStock() {



        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';

		  	$vehicle_reg_no = $_GET['vehicle_reg_no'];
            $vehicle_model = $_GET['vehicle_model'];
            $parts_no  = $_GET['parts_no'];

            if($vehicle_reg_no){
                $clause = " v.vehicle_reg_no='$vehicle_reg_no' AND ";
            }
            else{
                $clause = " v.vehicle_reg_no='' AND ";
            }

            if($vehicle_model & $parts_no)
            {
                $clause .= " v.vehicle_model = '$vehicle_model' AND  v.parts_no = '$parts_no' AND ";
            }

            elseif($vehicle_model)
            {
                $clause .= " v.vehicle_model = '$vehicle_model' AND ";
            }
            elseif($parts_no)
            {
                $clause = " v.parts_no = '$parts_no' AND ";
            }

            if($clause)
           $sql = "SELECT distinct v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.wsitem_id as wsitem_id, v.vehicle_model, v.available_stock  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm'
             and $clause v.available_stock !=0 and v.temp_stock!=0 ";





            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }



    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Wsitemdists::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wsitemdists-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionExcel()
	{
		$model=new Wsitemdists('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['criteria']))
			$model->attributes=$_GET['criteria'];

		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Item_Distribution',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Item_Distribution',
			'columns'=>array(
				array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),

        //'defect_id',
        //array(
           // 'name' => 'vehicle_reg_no',
           // 'type'=>'raw',
            //'value' => '$data->defects->vehicle_reg_no',
        //),

        
      //  'wsrequisition_id',
      //  'wsitemname_id',
        'item_name',
        'vehicle_model',         
        'parts_no', 
        'vehicle_reg_no',
        array(
                'header' => 'Total Purchase Qty',               
                'value'=>'$data->getTotalPurchase($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model)',
        ),
        array(
                'header' => 'Date Wise Purchase Qty',  
                //'value'=>'$_GET["Wsitemdists"]["bill_from_date"]',             
                'value'=>'$data->getDateWisePurchase($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model,$_GET["criteria"]["bill_from_date"],$_GET["criteria"]["bill_to_date"])',
        ),
        array(
                'header' => 'Total Issue Qty',               
                'value'=>'$data->getTotalIssue($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model)',
        ),
        array(
                'header' => 'Date Wise Issue Qty',               
                'value'=>'$data->getDateWiseIssue($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model,$_GET["criteria"]["issue_from_date"],$_GET["criteria"]["issue_to_date"])',
        ),
        //'bill_no',
       // 'bill_date',
       // 'issue_date',
        
        
        //'item_size',
        'available_stock',
        'unit_price',
        //'quantity',       
        //'mechanic_pin',
        //mechanic_name',
        array(
                        'name'=>'total_price',
                        
                        'footer'=>'Grand Total: ' . $model->getTotal($model->search()->getData(), 'total_price'),
        ),
        array(
                'header' => 'Consumtion Rate',               
                'value'=>'$data->getConsumtionRate($data->vehicle_reg_no,$data->wsitemname_id,$data->vehicle_model,$_GET["criteria"]["bill_from_date"],$_GET["criteria"]["bill_to_date"],$_GET["criteria"]["issue_from_date"],$_GET["criteria"]["issue_to_date"])',
        ),
			),
		));
		Yii::app()->end();
	}
	public function actionExcel1()
	{
		$model=new Wsitemdists('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsitemdists']))
			$model->attributes=$_GET['Wsitemdists'];

		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search1(),
			'title'=>'iFleet_Item_Distribution',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Item_Distribution',
			'columns'=>array(
		//'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),

        'defect_id',        
        'item_name',
        'vehicle_reg_no',
        'vehicle_model',         
        'parts_no', 
        'quantity',
        'issue_date',
        'unit_price',
        'total_price',
        'mechanic_name',
     
    ),
		));
		Yii::app()->end();
	}

	public function actionGetVehicle(){
     	$vehicle_reg_no='%'.$_GET['vehicle_reg_no'].'%';
     	 //$sql = "SELECT vehicle_reg_no as value FROM tbl_defects WHERE vehicle_reg_no LIKE '$vehicle_reg_no' GROUP BY vehicle_reg_no ORDER BY vehicle_reg_no";
     	//$sql = "SELECT 	reg_no as value FROM tbl_vehicles WHERE reg_no LIKE '$vehicle_reg_no' ORDER BY reg_no";
     	 $sql="SELECT vehicle_reg_no as value FROM tbl_defects WHERE vehicle_reg_no LIKE '$vehicle_reg_no' GROUP BY vehicle_reg_no  
     	 	 UNION 
     	 	 SELECT reg_no as value FROM tbl_vehicles WHERE reg_no LIKE '$vehicle_reg_no' 
     	 ";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            if($result){
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }

     }
     //public function actionItemlist(){
     	//$this->renderPartial('itemlist', array('vehicles'=>$vehicles));    

     //}
      public function actionGetItemsData() {
      $vehicle_reg_no = $_GET['vehicle_reg_no'];
        
      $sql = "SELECT wsitemname as item_name1,id,SUM(temp_stock) as available_stock FROM  tbl_wsstocks  WHERE vehicle_reg_no = '$vehicle_reg_no' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL GROUP BY wsitemname_id ORDER BY wsitemname";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            
        $itemList = array();
        
        foreach($result as $item1){
            $itemList[$item1['id']] = $item1['item_name1'];            
        }
      
        $this->renderPartial('itemlist', array('itemList'=>$itemList));    

     }
     public function actionGetItemsDataModel() {
      $vehicle_model = $_GET['vehicle_model'];        
       $sql = "SELECT wsitemname as item_name1,id,SUM(temp_stock) as available_stock FROM  tbl_wsstocks  WHERE vehicle_model = '$vehicle_model' AND vehicle_reg_no ='' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL GROUP BY wsitemname_id ORDER BY wsitemname";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            $itemList = array();
        
        foreach($result as $item1){
            $itemList[$item1['id']] = $item1['item_name1'];            
        }
      
        $this->renderPartial('itemlist', array('itemList'=>$itemList));

     }
}
