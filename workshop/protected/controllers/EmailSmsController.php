<?php

class EmailSmsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','template','excel','lateSummery'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','routeList','routeDelete','routeView','createForm','createData'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EmailSms;
		$model->created_time=date('Y-m-d H:i:s');
		$model->created_by=Yii::app()->user->name;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailSms']))
		{
			$model->attributes=$_POST['EmailSms'];
			if(isset($_SESSION['routeList']) && count($_SESSION['routeList'])>0){	
				$routes_id = array_keys($_SESSION['routeList']);	
				$routes_no = '"'.implode('","',array_values($_SESSION['routeList'])).'"';
				$pick_drop_user = Commonfleets::model()->findAll(
                    array(                       
                        'condition'=>'application_type="Pick and Drop" AND present_route IN('.$routes_no.')'    
                    ));	
                $this->smsAndEmail($model,$pick_drop_user);
                $this->smsAndEmailSave($_POST['EmailSms'],$routes_id);
                unset($_SESSION['routeList']);
                $this->redirect(array('index'));
            }
			elseif($model->route_id==10000){
				//var_dump($model->route_id);exit();
				$routes=Routes::model()->findAll();
				function routeId($n)
					{
					    return $n['id'];
					}

				$routes_id = array_map('routeId',$routes);
				$pick_drop_user=Commonfleets::model()->findAllByAttributes(array('application_type'=>'Pick and Drop'));
				$this->smsAndEmail($model,$pick_drop_user);
				$this->smsAndEmailSave($_POST['EmailSms'],$routes_id);
				$this->redirect(array('index'));
			}
			else{

				$route_no=Stoppage::routeNo($model->route_id);
				$pick_drop_user=Commonfleets::model()->findAllByAttributes(array('present_route'=>$route_no,'application_type'=>'Pick and Drop'));
				$this->smsAndEmail($model,$pick_drop_user);
			}	
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	private function smsAndEmail($model,$pick_drop_user) {
		if(count($pick_drop_user)>0) {				
			$message=$body=$model->body;
			$sub=$model->sub;
			//foreach ($pick_drop_user as $key => $value) {
				$email="ehsan@nexkraft.com";
				//$email=$value['user_email'];
				$cell="1684888352";
				//$cell=$value['user_cell'];
				$user_cell = substr($cell, -11);
		
		        if (strlen($user_cell) == 10)
		            $user_cell = "0" . $user_cell;
		        else
		            $user_cell = $user_cell;
		        if($model->type==3){
				   $this->sendMailIcress($email,$body,$sub);
				   $this->sendSMS($user_cell,$message);
				}
				else if($model->type==2){				   
				   $this->sendSMS($user_cell,$message);				   
				}
				else if($model->type==1){
				    $this->sendMailIcress($email,$body,$sub);
				   
				}
				else{

				}
			
			//}

		}
		else {}
	}
	private function smsAndEmailSave($data,$routes_id) {
		foreach ($routes_id as $key => $value) {
			$model = new EmailSms;
			$model->created_time = date('Y-m-d H:i:s');
			$model->created_by = Yii::app()->user->name;
			$model->attributes = $data;
			$model->route_id= $value;
			$model->save();
		}
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated_time=date('Y-m-d H:i:s');
		$model->updated_by=Yii::app()->user->name;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailSms']))
		{
			$model->attributes=$_POST['EmailSms'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('EmailSms');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$model=new EmailSms('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmailSms']))
			$model->attributes=$_GET['EmailSms'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EmailSms('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmailSms']))
			$model->attributes=$_GET['EmailSms'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=EmailSms::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-sms-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionTemplate(){
		$template_id=$_POST['templateId'];
		$template=Template::model()->findByPk($template_id);
		
		if($template){
			echo CJSON::encode($template);
		}
		else{
			$template=array('error'=>'Template Not Found!');
			echo CJSON::encode($template);
		}	
		
	}
	public function actionLateSummery()	{	
		$username=Yii::app()->user->username;
 		$sql="SELECT r.* FROM tbl_commonfleets as c INNER JOIN tbl_routes as r ON r.route_no=c.present_route WHERE c.application_type='Pick and Drop' AND c.user_pin='$username'" ;
 		$rs_sql=Yii::app()->db->createCommand($sql)->queryRow();
 		
		if($rs_sql){
			$route_id=$rs_sql['id'];
			$sql_late_email="SELECT * FROM tbl_email_sms WHERE route_id='$route_id' AND type IN(1,3) order by created_time DESC Limit 10";
			$sql_late_email_rs=Yii::app()->db->createCommand($sql_late_email)->queryAll();	
			  // clear any default values			
			echo $this->renderPartial('email_late_summery', array(
				'model'=>$sql_late_email_rs,		

			)); 
		}
		else{
			echo "No Route Assign";
		}
	}
	public function actionExcel()
	{
		$model = new EmailSms ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'Email & SMS Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'Email_SMS_Information',
            //'stream'=>false,
           'columns'=>array(
           		
				array(  
                'name'=>'id',   
                'type' => 'raw',                 
                'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',         
                
            	),
	        array(  
	                'name'=>'template_id',   
	                'type' => 'raw',                  
	                'value' => '$data->template($data->template_id)',         
	                
	            ),
	       array(  
	                'name'=>'route_id',   
	                'type' => 'raw',                  
	                'value' => 'Stoppage::routeNo($data->route_id)',         
	               
	            ),
	       array(  
	                'name'=>'body',   
	                'type' => 'raw',            
	                'value' => 'nl2br($data->body)',         
	                
	            ),
	        array(  
	                'name'=>'type',   
	                'type' => 'raw',                 
	                'value' => '$data->type($data->type)',         
	                
	            ),
					
		
			),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
	public function actionRouteList() {	
		Yii::app()->getSession()->open();
		//$route=Routes::model()->findByPk($_POST['r']);
		$_SESSION['routeList'][$_POST['rid']]=$_POST['r'];
		echo $route_list = $this->sessionRouteView();
	}
	public function actionCreateForm(){
		$emailSms = new EmailSms;
		$template = new Template;		
		$this->render('createForm',array(
			'emailSms'=>$emailSms,
			'template'=>$template,			
		));
	}

	public function actionCreateData() {	    
		$model = new EmailSms;
		$model->created_time = date('Y-m-d H:i:s');
		$model->created_by = Yii::app()->user->name;		
		if(isset($_POST['EmailSms'])){
			$model->attributes=$_POST['EmailSms'];
			if(isset($_SESSION['routeList']) && count($_SESSION['routeList'])>0) {	
				$routes_id = array_keys($_SESSION['routeList']);	
				$routes_no = '"'.implode('","',array_values($_SESSION['routeList'])).'"';
				$pick_drop_user = Commonfleets::model()->findAll(
                    array(                       
                        'condition'=>'application_type="Pick and Drop" AND present_route IN('.$routes_no.')'    
                    ));	
                $this->smsAndEmail($model,$pick_drop_user);
                $this->smsAndEmailSave($_POST['EmailSms'],$routes_id);
                unset($_SESSION['routeList']);
                $this->redirect(array('index'));
            }
			elseif($model->route_id==10000) {				
				$routes = Routes::model()->findAll();
				function routeId($n)
					{
					    return $n['id'];
					}
				$routes_id = array_map('routeId',$routes);
				$pick_drop_user = Commonfleets::model()->findAllByAttributes(array('application_type'=>'Pick and Drop'));
				$this->smsAndEmail($model,$pick_drop_user);
				$this->smsAndEmailSave($_POST['EmailSms'],$routes_id);
				$this->redirect(array('index'));
			}
			else{
				$route_no = Stoppage::routeNo($model->route_id);
				$pick_drop_user = Commonfleets::model()->findAllByAttributes(array('present_route'=>$route_no,'application_type'=>'Pick and Drop'));
				$this->smsAndEmail($model,$pick_drop_user);
			}	
			
			if($model->save())
				echo 1;
			else
			$this->renderPartial('email_sms_form', array('model'=>$model));
		}

	}
	public function actionRouteDelete() {
		if($_POST['rid']==10000) {
			unset($_SESSION['routeList']);
		}
		else {
			unset($_SESSION['routeList'][$_POST['rid']]);
		}
		if(count($_SESSION['routeList'])<=0){
			unset($_SESSION['routeList']);
		}	
		echo $route_list = $this->sessionRouteView();
	}
	public function actionRouteView(){			
		echo $route_list = $this->sessionRouteView();
	}
	private function sessionRouteView() {
		$route_list='';
		function stoppagelist($n) {
			return $n['stoppage'];
		}
		if(isset($_SESSION['routeList']) && count($_SESSION['routeList'])>0) {		
			foreach($_SESSION['routeList'] as $key => $value) {
				$stoppage=Stoppage::model()->findAllByAttributes(array('route_id'=>$key));
				
				$stoppage = implode(', ',array_map('stoppagelist',$stoppage));
				$route_list.="<tr id='$key'>
					<td>$value</td><td>$stoppage</td><td><button form='form1' onclick='routeDelete($key)'>X</button></td>
					</tr>";	
			}
		}
		else {
			return "<tr>
						<td colspan='3'>No Route Selected</td>
				    </tr>";	
		}
		return $route_list;
	}
	public function sendSMS($user_cell, $message) {

        $client = new EHttpClient(
            'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
            )
        );
        $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $message,
            'to_number' => $user_cell,
            'app_url' => 'http://ifleet.brac.net/',
        ));

        

        return $response = $client->request('POST');
        
    }
    public function sendMailIcress($email,$body,$sub)
    {
        try {
            $soapClient = new SoapClient("http://imail.brac.net:8080/isoap.comm.imail/EmailWS?wsdl");

            $job = new jobs;

            //$job->subject='Transport Requisition Notification';
            $job->jobContentType = 'html';
            $job->fromAddress = 'ifleet@brac.net';
            $job->udValue1 = 'iFleet';
            $job->requester = 'iFleet';

            //   $job->jobRecipients[0]=new jobRecipients;
            //   $job->jobRecipients[0]->recipientEmail="shouman.das@gmail.com";

            $job->jobRecipients[0] = new jobRecipients;
            //$job->jobRecipients[0]->recipientEmail = $model->email;
            $job->jobRecipients[0]->recipientEmail = $email;


            //$model1 = $hrdata->getHrUser($model->pin);
            //$rec_mail = $model1[0]['Email'];

           
            $job->subject = $sub;
            $job->body = nl2br($body);
            $jobs = array('jobs' => $job);
            $send_email = $soapClient->__call('sendEmail', array($jobs));
            $send_email->return->status;
            
        } catch (SoapFault $fault) {
           $error = 1;
            print($fault->faultcode . "-" . $fault->faultstring);
        }
    }

}
class jobs
{

    public $appUserId; // string
    public $attachments; // attachment
    public $bcc; // string
    public $body; // string
    public $caption; // string
    public $cc; // string
    public $complete; // boolean
    public $feedbackDate; // dateTime
    public $feedbackEmail; // string
    public $feedbackName; // string
    public $feedbackSent; // boolean
    public $fromAddress; // string
    public $fromText; // string
    public $gateway; // string
    public $jobContentType; // string
    public $jobId; // long
    public $jobRecipients; // jobRecipients
    public $mode; // string
    public $numberOfItem; // int
    public $numberOfItemFailed; // int
    public $numberOfItemSent; // int
    public $priority; // string
    public $requester; // string
    public $status; // string
    public $subject; // string
    public $toAddress; // string
    public $toText; // string
    public $udValue1; // string
    public $udValue2; // string
    public $udValue3; // string
    public $udValue4; // string
    public $udValue5; // string
    public $udValue6; // string
    public $udValue7; // string
    public $vtemplate; // string

}

class jobRecipients
{

    public $failCount; // int
    public $image; // base64Binary
    public $job; // jobs
    public $jobDetailId; // long
    public $recipientEmail; // string
    public $sent; // boolean
    public $sentDate; // dateTime
    public $toText; // string

}
