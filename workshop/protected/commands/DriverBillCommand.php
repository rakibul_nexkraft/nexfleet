<?php 

date_default_timezone_set("Asia/Dhaka");
class DriverBillCommand extends CConsoleCommand {
    public function actionIndex() {     
      
        $request_query="SELECT * FROM  tbl_digital_log_book_history Where ((trip_id_end=1 AND day_end ='No') OR (day_end='Yes' AND trip_id_end<>1)) AND bill=0 AND trip_id<>'' GROUP BY trip_id";
        $request_data=Yii::app()->db->createCommand($request_query)->queryAll();
      //var_dump($trip_id);
      //var_dump($request_data);exit;
      //$i=168;
        foreach ($request_data as $rkey => $rvalue) {
            
           // $sql="SELECT * FROM tbl_digital_log_book_history Where  trip_id ='".$rvalue['trip_id']."' order by created_time_mobile desc";
           //$sql="SELECT * FROM tbl_digital_log_book_history Where  trip_id ='T20190514063249433' order by created_time_mobile desc";
           
           // $sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
           // foreach ($sql_rs as $keytr => $valuetr) {
            	//$i=0;
            	//if($i==0){
            	$requisition_id=$rvalue['requisition_id'];            	
            	$user_pin=$rvalue['user_pin'];
            	$user_name=$rvalue['user_name'];
            	$Job_level='';
            	$user_cell='0';
            	$user_email='';
            	$user_address='';
            	if(!empty($user_pin)){
            	 $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
            	 $staffInfo = CJSON::decode(file_get_contents($uri));
           		 $Job_level = $staffInfo[0]['JobLevel'];
           		 $user_cell=$staffInfo[0]['MobileNo'];
            	$user_email=$staffInfo[0]['EmailID'];
            	$user_address=$staffInfo[0]['EmailID'];
           		}           
            	$user_department=$rvalue['department'];
            	$vehicle_reg_no=$rvalue['vehicle_reg_no'];
            	$vehicletype_id=$rvalue['vehicle_type_id'];            	
            	$driver_pin=$rvalue['driver_pin'];
            	$driver_name=$rvalue['driver_name'];
            	$dutytype_id=$rvalue['dutytype_id'];
            	$duty_day=$rvalue['duty_day'];
            	//echo $valuetr['driver_start_time_vts'];
            	if(!empty($rvalue['driver_start_time_vts']) && $rvalue['driver_start_time_vts']!="0000-00-00 00:00:00"){
            		 $rvalue['driver_start_time_vts'];
            		$statr_DT=explode(" ",$rvalue['driver_start_time_vts']);
            		
            		$start_date=$statr_DT[0];
            		if(!empty($statr_DT[1]))
            		$start_time=$statr_DT[1];
            		
            	}
            	else{
            		$statr_DT=explode(" ", $rvalue['driver_start_time_mobile']);
            		$start_date=$statr_DT[0];
            		if(!empty($statr_DT[1]))
            		$start_time=$statr_DT[1];
            	}
            	if(!empty($rvalue['driver_end_time_vts']) && $rvalue['driver_end_time_vts']!="0000-00-00 00:00:00"){
            		$end_DT=explode(" ", $rvalue['driver_end_time_vts']);
            		$end_date=$end_DT[0];
            		if(!empty($end_DT[1]))
            		$end_time=$end_DT[1];
            	}
            	else{
            		$end_DT=explode(" ", $rvalue['driver_end_time_mobile']);
            		$end_date=$end_DT[0];
            		if(!empty($end_DT[1]))
            		$end_time=$end_DT[1];
            	}
                $start_time_array=explode(":",  $start_time);
                if(isset($start_time_array[2])){
                        $start_time_array[2]="00";
                        $start_time=implode(":", $start_time_array);
                  }
                $end_time_array=explode(":",  $end_time);
                if(isset($end_time_array[2])){
                        $end_time_array[2]="00";
                        $end_time=implode(":", $end_time_array);
                      }

            	$a = new DateTime($start_time);
				$b = new DateTime($end_time);
				$interval = $a->diff($b);
				$total_time=$interval->format("%H:%I");
            	$duty_day=$rvalue['duty_day'];
            	$end_point=empty($rvalue['location_vts'])?$rvalue['location_mobile']:$rvalue['location_vts'];
            	$end_meter=$rvalue['meter_number'];




            	$sql="SELECT * FROM tbl_digital_log_book_history Where  trip_id ='".$rvalue['trip_id']."' order by created_time_mobile ASC";
           
           		$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();

               $start_point=empty($sql_rs[0]['location_vts'])?$sql_rs[0]['location_mobile']:$sql_rs[0]['location_vts'];
               $start_meter=$sql_rs[0]['meter_number'];
               
               $night_halt=0;
               $nh_amount=0;
               $morning_ta=0;
               $lunch_ta=0;
               $night_ta=0;
              foreach ($sql_rs as $key => $value) {
              	if($value['night_halt_bill']!=0){
              		$night_halt=1;
              		$nh_amount=$value['night_halt_bill'];
              	}
              	if($value['morning_ta']!=0){
              		$morning_ta=$value['morning_ta'];
              	}
              	if($value['lunch_ta']!=0){
              		 $lunch_ta=$value['lunch_ta'];
              	}
              	if($value['night_ta']!=0){
              		$night_ta=$value['night_ta'];
              	}
                $id_history=$value['id'];
                $sql_update_history="UPDATE tbl_digital_log_book_history SET bill=1  Where  id='$id_history'";        
                $sql_update_history_rs=Yii::app()->db->createCommand($sql_update_history)->execute();
              	
              }
            $sup_mile_amount=0;
          	$total_run=$end_meter-$start_meter; 
            $rph=$rvalue['running_per_km_rate'];

            $sql_dutytypes="SELECT * FROM tbl_dutytypes WHERE id=".$dutytype_id;
            $sql_dutytypes_rs=Yii::app()->db->createCommand($sql_dutytypes)->queryRow();

            $sql_vehicletypes="SELECT * FROM tbl_vehicletypes WHERE id=".$vehicletype_id;
            $sql_vehicletypes_rs=Yii::app()->db->createCommand($sql_vehicletypes)->queryRow();
             
           if($dutytype_id==1){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
           else if($dutytype_id==2){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_personal']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
           else if($dutytype_id==3){            
            $hour=explode(":", $total_time);
            $hour_bill=$hour[0]*$sql_vehicletypes_rs['rph_visitor'];
            if($sql_vehicletypes_rs['minimum_rate']>$hour_bill){
                $bill_amount=$sql_vehicletypes_rs['rate_per_km_external']*$total_run;
                $bill_amount=$sql_vehicletypes_rs['minimum_rate']+$bill_amount* $sql_dutytypes_rs['service_charge'];
            }
            else{
                 $bill_amount=$sql_vehicletypes_rs['rate_per_km_external']*$total_run;
                $bill_amount=$hour_bill+$bill_amount* $sql_dutytypes_rs['service_charge'];
            }
           }
           else if($dutytype_id==4){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
            else if($dutytype_id==5){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
           else{
            $bill_amount=$rph*$total_run;
           }
            //echo $bill_amount;
           $over_hour_minute=explode(":", $rvalue['over_time']);
           if(isset($over_hour_minute[1])){
            $total_ot_hour=$over_hour_minute[0].".".$over_hour_minute[1];
            }
           else{
            $total_ot_hour=$rvalue['over_time'];
           }
           $start_time_hour_minute=explode(":", $start_time);
           if(isset($start_time_hour_minute[1])){
            $start_time=$start_time_hour_minute[0].".".$start_time_hour_minute[1];
            }
           else{
            $start_time=$start_time;
           }

           $end_time_hour_minute=explode(":", $end_time);
           if(isset($end_time_hour_minute[1])){
            $end_time=$end_time_hour_minute[0].".".$end_time_hour_minute[1];
            }
           else{
            $end_time=$end_time;
           }
            $total_ot_amount=$rvalue['over_time_bill'];
            $created_by=$rvalue['created_by'];
            $created_time=date("Y-m-d h:i:s");
            $trip_id=$rvalue['trip_id'];

            $sql="INSERT INTO tbl_digital_log_book(
            id,
            requisition_id,
            user_pin,
            user_name,
            user_level,
            user_dept,
            email,
            user_cell,
            user_address,
            vehicle_reg_no,
            vehicletype_id,
            vehicle_location,
            driver_pin,
            driver_name,
            helper_pin,
            helper_name,
            helper_ot,
            dutytype_id,
            dutyday,
            start_date,
            end_date,
            start_time,
            end_time,
            total_time,
            start_point,
            end_point,
            start_meter,
            end_meter,
            total_run,
            rph,
            night_halt,
            nh_amount,
            morning_ta,
            lunch_ta,
            night_ta,
            sup_mile_amount,
            bill_amount,
            total_ot_hour,
            total_ot_amount,
            created_by,
            created_time,
            trip_id)
             VALUES(
             null,
             '".$requisition_id."',
             '".$user_pin."',
             '".$user_name."',
             '".$Job_level."',
             '".$user_department."',
             '".$user_email."',
             '".$user_cell."',
             '',
             '".$vehicle_reg_no."',
             '".$vehicletype_id."',
             '".$start_point."',
             '".$driver_pin."',
             '".$driver_name."',
             '',
             '',
             '',
             '".$dutytype_id."',
             '".$duty_day."',
             '".$start_date."',
             '".$end_date."',
             '".$start_time."',
             '".$end_time."',
             '".$total_time."',
             '".$start_point."',
             '".$end_point."',
             '".$start_meter."',
             '".$end_meter."',
             '".$total_run."',
             '".$rph."',
             '".$night_halt."',
             '".$nh_amount."',
             '".$morning_ta."',
             '".$lunch_ta."',
             '".$night_ta."',
             '',
             '".$bill_amount."',
             '".$total_ot_hour."',
             '".$total_ot_amount."',
             '".$created_by."',
             '".$created_time."',
             '".$trip_id."'
         )";
         echo $sql_rs=Yii::app()->db->createCommand($sql)->execute();
         //$i++;
        }
       
    }
        
}
?>