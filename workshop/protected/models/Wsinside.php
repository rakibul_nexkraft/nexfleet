<?php

/**
 * This is the model class for table "{{wsinside}}".
 *
 * The followings are the available columns in table '{{wsinside}}':
 * @property integer $id
 * @property integer $jobcard_id
 * @property integer $wsservicetype_id
 * @property integer $wssupplier_id
 * @property integer $wsrequisition_id
 * @property integer $item_id
 * @property double $unit_price
 * @property string $item_size
 * @property string $quantity
 * @property string $req_quantity
 * @property string $issue_date
 * @property string $remarks
 * @property integer $active
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsinside extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsinside the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsinside}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('jobcard_id, wsservicetype_id, wssupplier_id, wsrequisition_id, item_id, unit_price, item_size, quantity, req_quantity, issue_date, active ', 'required'),
			array('jobcard_id, wsservicetype_id, wssupplier_id, wsrequisition_id, item_id, active', 'numerical', 'integerOnly'=>true),
			array('unit_price', 'numerical'),
			array('item_size, quantity, req_quantity, created_by, updated_by', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, jobcard_id, wsservicetype_id, wssupplier_id, wsrequisition_id, item_id, unit_price, item_size, quantity, req_quantity, issue_date, remarks, active, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'jobcard_id' => 'Jobcard',
			'wsservicetype_id' => 'Wsservicetype',
			'wssupplier_id' => 'Wssupplier',
			'wsrequisition_id' => 'Wsrequisition',
			'item_id' => 'Item',
			'unit_price' => 'Unit Price',
			'item_size' => 'Item Size',
			'quantity' => 'Quantity',
			'req_quantity' => 'Req Quantity',
			'issue_date' => 'Issue Date',
			'remarks' => 'Remarks',
			'active' => 'Active',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('jobcard_id',$this->jobcard_id);
		$criteria->compare('wsservicetype_id',$this->wsservicetype_id);
		$criteria->compare('wssupplier_id',$this->wssupplier_id);
		$criteria->compare('wsrequisition_id',$this->wsrequisition_id);
		$criteria->compare('item_id',$this->item_id);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('item_size',$this->item_size,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('req_quantity',$this->req_quantity,true);
		$criteria->compare('issue_date',$this->issue_date,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}