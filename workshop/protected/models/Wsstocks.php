<?php

/**
 * This is the model class for table "{{wsstocks}}".
 *
 * The followings are the available columns in table '{{wsstocks}}':
 * @property integer $id
 * @property integer $wsitems_id
 * @property double $stock_in
 * @property double $stock_out
 * @property double $available_stock
 * @property double $price
 * @property double $total_amount
 * @property string $created_time
 * @property integer $active
 */
class Wsstocks extends CActiveRecord
{
	public $from_date;
	public $to_date;
	public $issue_from_date;
	public $issue_to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsstocks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsstocks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' wsitemname_id, price, total_amount', 'required'),
			array('wsitemname, vehicle_reg_no,wsitem_id, vehicle_model, parts_no,wsdist_id,from_date, to_date, issue_from_date, issue_to_date', 'length', 'max'=>127),
			array('issue_date','safe'),
			array('id, wsitemname_id, active', 'numerical', 'integerOnly'=>true),
			array('stock_in, stock_out, available_stock, temp_stock, price, total_amount', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, wsitemname_id, wsitemname, stock_in, stock_out, available_stock, price, total_amount, purchase_date, active,from_date, to_date,issue_from_date, issue_to_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(		
		'wsitems'=>array(self::BELONGS_TO, 'wsitems', 'wsitemname_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'wsitem_id' => 'Wsitem ID',
			'wsitemname_id' => 'Wsitemname ID',
			'wsdist_id' => 'Distribution ID',
			'wsitemname' => 'Item Name',
            'vehicle_reg_no' => 'Vehicle Reg No',
            'vehicle_model' => 'Vehicle Model',
            'parts_no' => 'Parts No',
			'stock_in' => 'Stock In',
			'stock_out' => 'Stock Out',
			'available_stock' => 'Available Stock',
			'price' => 'Price/Per Unit',
			'total_amount' => 'Total Amount',
			'purchase_date' => 'Purchase Date',
			'issue_date' => 'Issue Date',
			'from_date' => 'From Purchase Date',
			'to_date' => 'To Purchase Date',
			'issue_from_date'=>'Issue From Date',
			'issue_to_date'=>'Issue To Date',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('wsitemname_id',$this->wsitemname_id);
		$criteria->compare('wsitemname',$this->wsitemname,true);
        $criteria->compare('wsitem_id',$this->wsitem_id);
    $criteria->compare('wsdist_id',$this->wsdist_id,true);
        $criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
        $criteria->compare('vehicle_model',$this->vehicle_model,true);
        $criteria->compare('parts_no',$this->parts_no,true);
		$criteria->compare('stock_in',$this->stock_in);
		$criteria->compare('stock_out',$this->stock_out);
		$criteria->compare('available_stock',$this->available_stock);
		$criteria->compare('price',$this->price);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('issue_date',$this->issue_date,true);
		$criteria->compare('active',$this->active);

		if(!empty($_REQUEST['Wsstocks']['from_date']) && empty($_REQUEST['Wsstocks']['to_date']))
		{
			$criteria->addCondition('purchase_date = "'.$_REQUEST['Wsstocks']['from_date'].'"');
		}
		elseif(!empty($_REQUEST['Wsstocks']['to_date']) && empty($_REQUEST['Wsstocks']['from_date']))
		{
			$criteria->addCondition('purchase_date <= "'.$_REQUEST['Wsstocks']['to_date'].'"');
		}
		elseif(!empty($_REQUEST['Wsstocks']['to_date']) && !empty($_REQUEST['Wsstocks']['from_date']))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('purchase_date BETWEEN "'.$_REQUEST['Wsstocks']['from_date'].'" AND "'.$_REQUEST['Wsstocks']['to_date'].'"');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}

    public function availablelist(){

        $criteria = new CDbCriteria;

       /* SELECT max(id), `wsitemname`,`vehicle_model`, `temp_stock` as `available_stock`,`price`
        FROM `tbl_wsstocks` group by `wsitemname_id` order by id desc*/        

       $criteria->select ='id,wsitemname,wsitem_id,vehicle_model,parts_no,vehicle_reg_no,SUM(temp_stock) as available_stock,price,total_amount';
        

        $criteria->addcondition('temp_stock != 0');
;
           
      	if($_REQUEST['Wsstocks_page'])
      	{
      		
      		if($_REQUEST['Wsstocks']['vehicle_reg_no'])
      		{
						$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
      		}
      		else 
      		{
      			$not_veh_reg_no = '';
      			$criteria->addcondition('vehicle_reg_no = :not_veh_reg');
      			$criteria->params = array(':not_veh_reg' => $not_veh_reg_no);
      		}
      	}
      	
      	      	           	     
        //$criteria->params = array(':veh_reg' => $this->vehicle_reg_no,':not_veh_reg' => '');
        //$criteria->params += array(':temp1' => $temp_v);
        

        $criteria->group = 'wsitemname_id';
        //$criteria->order = 'id desc';    

        $criteria->compare('id',$this->id,true);
        $criteria->compare('wsitemname',$this->wsitemname,true);
        $criteria->compare('wsitem_id',$this->wsitem_id);
        $criteria->compare('vehicle_model',$this->vehicle_model,true);
        $criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
        $criteria->compare('parts_no',$this->parts_no,true);        
        //$criteria->compare('parts_no',$this->parts_no,true);
        $criteria->compare('temp_stock',$this->temp_stock,true);
        $criteria->compare('price',$this->price,true);

        if(!empty($_REQUEST['Wsstocks']['from_date']) && empty($_REQUEST['Wsstocks']['to_date']))
		{
			$criteria->addCondition('purchase_date = "'.$_REQUEST['Wsstocks']['from_date'].'"');
		}
		elseif(!empty($_REQUEST['Wsstocks']['to_date']) && empty($_REQUEST['Wsstocks']['from_date']))
		{
			$criteria->addCondition('purchase_date <= "'.$_REQUEST['Wsstocks']['to_date'].'"');
		}
		elseif(!empty($_REQUEST['Wsstocks']['to_date']) && !empty($_REQUEST['Wsstocks']['from_date']))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('purchase_date BETWEEN "'.$_REQUEST['Wsstocks']['from_date'].'" AND "'.$_REQUEST['Wsstocks']['to_date'].'"');
		}

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>50000,
            ),
			'sort'=>array('defaultOrder'=>'id DESC')
        ));

    }
    public function searchExcel($offset_data,$data)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$offset_data=$offset_data*2000;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('wsitemname_id',$this->wsitemname_id);
		$criteria->compare('wsitemname',$this->wsitemname,true);
        $criteria->compare('wsitem_id',$this->wsitem_id,true);
    	$criteria->compare('wsdist_id',$this->wsdist_id,true);
        $criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
        $criteria->compare('vehicle_model',$this->vehicle_model,true);
        $criteria->compare('parts_no',$this->parts_no,true);
		$criteria->compare('stock_in',$this->stock_in);
		$criteria->compare('stock_out',$this->stock_out);
		$criteria->compare('available_stock',$this->available_stock);
		$criteria->compare('price',$this->price);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('issue_date',$this->issue_date,true);
		$criteria->compare('active',$this->active);

		$criteria->order = 'id DESC';
		$criteria->limit = 2000;
		$criteria->offset = $offset_data;

		$search_movements_table=Wsstocks::model()->findAll($criteria);
		return $search_movements_table;
		
	}
	public function getTotal($records, $column)
        {
                $total = 0;
                foreach ($records as $record) {
                        $total += $record->$column;
                }
                return $total;
        }
    public function getTotalPurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('wsitemname',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';
			$purchesTotal=Wsstocks::model()->findAll($criteria);
			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->stock_in;
			}
			
               return $total;
        }
       public function getDateWisePurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model,$bill_from_date,$bill_to_date)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('wsitemname',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->compare('purchase_date',$bill_from_date);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';

		if(!empty($bill_from_date) && empty($bill_to_date))
		{
			$criteria->addCondition('purchase_date = "'.$bill_from_date.'"');
		}
		elseif(!empty($bill_to_date) && empty($bill_from_date))
		{
			$criteria->addCondition('purchase_date <= "'.$bill_to_date.'"');
		}
		elseif(!empty($bill_to_date) && !empty($bill_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('purchase_date BETWEEN "'.$bill_from_date.'" AND "'.$bill_to_date.'"');
		}
		else{
			//$criteria->addCondition("purchase_date = '0000-00-00'");
		}


			$purchesTotal=Wsstocks::model()->findAll($criteria);

			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->stock_in;
			}
			
               return $total;
        }
        public function getTotalIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('item_name',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';
			$purchesTotal=Wsitemdists::model()->findAll($criteria);
			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->quantity;
			}
			
               return $total;
        }
        public function getDateWiseIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model,$issue_from_date,$issue_to_date)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('item_name',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->compare('purchase_date',$bill_from_date);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';

		if(!empty($issue_from_date) && empty($issue_to_date))
		{
			$criteria->addCondition('issue_date = "'.$issue_from_date.'"');
		}
		elseif(!empty($issue_to_date) && empty($issue_from_date))
		{
			$criteria->addCondition('issue_date <= "'.$issue_to_date.'"');
		}
		elseif(!empty($issue_to_date) && !empty($issue_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('issue_date BETWEEN "'.$issue_from_date.'" AND "'.$issue_to_date.'"');
		}
		else{
			//$criteria->addCondition("purchase_date = '0000-00-00'");
		}


			$purchesTotal=Wsitemdists::model()->findAll($criteria);

			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->quantity;
			}
			
               return $total;
        }
        public function getConsumtionRate($vehicle_reg_no,$wsitemname_id,$vehicle_model,$bill_from_date,$bill_to_date,$issue_from_date,$issue_to_date)
        {
        	if(!empty($bill_from_date) && !empty($issue_from_date)){

        		$DateWisePurchase=$this->getDateWisePurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model,$bill_from_date,$bill_to_date);
        		$DateWiseIssue=$this->getDateWiseIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model,$issue_from_date,$issue_to_date);
        		return ($DateWiseIssue*100)/$DateWisePurchase;
        	}
        	else{
        		 $total_purchase=$this->getTotalPurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model);
        		$total_issue=$this->getTotalIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model);

        		return ($total_issue*100)/$total_purchase;
        	}
        	

        }
}