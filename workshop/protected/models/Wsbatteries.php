<?php

/**
 * This is the model class for table "{{wsbatteries}}".
 *
 * The followings are the available columns in table '{{wsbatteries}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $vehicle_type
 * @property string $purchase_date
 * @property string $DO_no_date
 * @property string $supplier
 * @property string $bill_number
 * @property double $bill_amount
 * @property string $warrenty_month
 * @property string $warrenty_valid_upto
 * @property string $remarks
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsbatteries extends CActiveRecord
{

    public $from_date;
    public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsbatteries the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsbatteries}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, vehicle_type, supplier,', 'required'),
			array('km_reading, defect_id', 'numerical', 'integerOnly'=>true),
			array('price, discount, net_amount,unit_price', 'numerical'),
            array('challan_date, purchase_date, requisition_date,bill_date', 'safe'),
			array('vehicle_reg_no, defect_id, vehicle_type, challan_no, supplier, requisition_no, challan_no, warrenty_month, warrenty_valid_upto, status, created_by, updated_by,battery_size, particulars, quantity,remarks,bill_no,battery_no', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, defect_id, vehicle_reg_no, vehicle_type, purchase_date, requisition_no, supplier, challan_no, net_amount, warrenty_month, warrenty_valid_upto, status, created_by, created_time, updated_by, updated_time,from_date,to_date', 'safe', 'on'=>'search'),
            array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
            array('updated_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicle_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'defect_id' => 'Defect ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicle_type' => 'Vehicle Type',
			'particulars' => 'Particulars',			
            'battery_size' => 'Size',
			'purchase_date' => 'Purchase Date',
			'requisition_no' => 'Requisition No',
            'requisition_date' => 'Requisition Date',
            'km_reading' => 'Km Reading',
			'supplier' => 'Supplier',

			'challan_no' => 'Challan No',
            'challan_date' => 'Challan Date',
            'bill_no'=>"Bill No",
            'bill_date'=>"Bill Date",
            'quantity' => 'Quantity',
            'unit_price'=>"Unit Price",            
            'price' => 'Total Price',
            'discount' => 'Discount',
			'net_amount' => 'Net Amount',
			'warrenty_month' => 'Warrenty Month',
			'warrenty_valid_upto' => 'Warrenty Valid Upto',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
            'from_date' => 'Purchase Date (From)',
            'to_date' => 'Purchase Date (To)',
            'battery_no'=>'Battery No',
            'remarks' => 'Remarks',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('defect_id',$this->defect_id,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicle_type',$this->vehicle_type,true);
		$criteria->compare('particulars',$this->particulars,true);
		$criteria->compare('battery_size',$this->battery_size,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('requisition_no',$this->requisition_no,true);
        $criteria->compare('requisition_date',$this->requisition_date);
        $criteria->compare('km_reading',$this->km_reading);
		$criteria->compare('supplier',$this->supplier,true);
		$criteria->compare('challan_no',$this->challan_no,true);
        $criteria->compare('challan_date',$this->challan_date);
        $criteria->compare('bill_no',$this->bill_no,true);
        $criteria->compare('bill_date',$this->bill_date);
        $criteria->compare('quantity',$this->quantity);
        $criteria->compare('unit_price',$this->unit_price);
        $criteria->compare('price',$this->price);
        $criteria->compare('discount',$this->discount);
		$criteria->compare('net_amount',$this->net_amount);
		$criteria->compare('warrenty_month',$this->warrenty_month,true);
		$criteria->compare('warrenty_valid_upto',$this->warrenty_valid_upto,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('purchase_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('purchase_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('purchase_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}