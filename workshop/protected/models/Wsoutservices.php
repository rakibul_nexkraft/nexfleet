<?php

/**
 * This is the model class for table "{{wsoutservices}}".
 *
 * The followings are the available columns in table '{{wsoutservices}}':
 * @property integer $id
 * @property string $service_name
 * @property string $service_category
 * @property string $quantity
 * @property integer $car
 * @property integer $micro_jeep
 * @property integer $pickup
 * @property integer $bus_coaster
 * @property integer $active
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsoutservices extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsoutservices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsoutservices}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('service_name, service_category, active, updated_by, updated_time', 'required'),
			array('car, micro_jeep, pickup, bus_coaster, active', 'numerical', 'integerOnly'=>true),
			array('service_name, service_category, quantity, updated_by', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, service_name, service_category, quantity, car, micro_jeep, pickup, bus_coaster, active, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'service_name' => 'Service Name',
			'service_category' => 'Service Category',
			'quantity' => 'Quantity',
			'car' => 'Car',
			'micro_jeep' => 'Micro Jeep',
			'pickup' => 'Pickup',
			'bus_coaster' => 'Bus Coaster',
			'active' => 'Active',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('service_name',$this->service_name,true);
		$criteria->compare('service_category',$this->service_category,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('car',$this->car);
		$criteria->compare('micro_jeep',$this->micro_jeep);
		$criteria->compare('pickup',$this->pickup);
		$criteria->compare('bus_coaster',$this->bus_coaster);
		$criteria->compare('active',$this->active);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}