<?php

/**
 * This is the model class for table "{{email_sms}}".
 *
 * The followings are the available columns in table '{{email_sms}}':
 * @property integer $id
 * @property integer $template_id
 * @property integer $route_id
 * @property string $body
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 * @property integer $type
 */
class EmailSms extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmailSms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{email_sms}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' route_id, sub, body, created_by, type', 'required'),
			array('template_id, route_id, type', 'numerical', 'integerOnly'=>true),
			array('created_by, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,sub, template_id, route_id, body, created_by, created_time, updated_by, updated_time, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'template_id' => 'Template',
			'route_id' => 'Route',
			'sub'=>'Subject',
			'body' => 'Body',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;



		

		$criteria->compare('id',$this->id);
		$criteria->compare('template_id',$this->template_id);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('sub',$this->sub);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('type',$this->type);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
	        	'pageSize'=>20,
	    	),
	    	'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
	public function template($tempId){
		$template=Template::model()->findByPk($tempId);
		if($template)
		return $template['title'];
		return '';

	}
	public function type($type){
		if($type==3){return "Email & SMS";}
		if($type==1){return "Email";}
		if($type==2){return "SMS";}

	}
}