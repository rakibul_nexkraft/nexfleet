<?php

/**
 * This is the model class for table "{{vehicles}}".
 *
 * The followings are the available columns in table '{{vehicles}}': 
 * @property string $reg_no
 * @property integer $vehicletype_id
 * @property string $engine_no
 
 * @property string $cc
 * @property string $vmodel
 * @property string $chassis_no
 * @property string $country
 * @property string $purchase_date
 * @property integer $purchase_price
 * @property string $supplier
 * @property integer $seat_capacity
 * @property string $load_capacity
 * @property integer $ac
 * @property integer $driver_pin
 * @property string $created_time
 * @property string $created_by
 * @property integer $active
 * @property string $allocated_to
 * @property string $is_special
 */
class Vehicles extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Vehicles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vehicles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicletype_id, engine_no, reg_no, cc, vmodel, chassis_no, purchase_date, purchase_price, supplier, driver_pin, dedicate, imei_no, asset_id', 'required'),
			array('vehicletype_id, purchase_price, seat_capacity, driver_pin, active,helper_pin,actual_kpl,	fuel_type, is_special', 'numerical', 'integerOnly'=>true),
			array('engine_no, reg_no, model_code, driver_name, imei_no, chassis_no, user_pin, country, supplier, location, dedicate, created_by, helper_name, asset_id', 'length', 'max'=>127),
			array('cc, vmodel, load_capacity,ac,tank_size,kpl_value', 'length', 'max'=>20),
            array('imei_no', 'unique', 'message' => "This IMEI NO already exists."),
            array('asset_id', 'unique', 'message' => "This asset id already exists."),
			array('allocated_to', 'length', 'max'=>100),
			array('reg_no','unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('vehicletype_id, engine_no, reg_no, cc,user_pin,imei_no, vmodel, chassis_no, country, purchase_date, purchase_price, supplier, seat_capacity, load_capacity, ac, driver_pin, created_time, created_by, active,actual_kpl,	fuel_type,tank_size,kpl_value', 'safe', 'on'=>'search'),
            array('created_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
			'renewals'=>array(self::HAS_MANY, 'Renewals', 'vehicle_reg_no'),
			'accidents'=>array(self::HAS_MANY, 'Accidents', 'vehicle_reg_no'),
            'vehicles_log'=>array(self::HAS_MANY, 'VehiclesLog', 'vehicle_reg_no'),
                  //  'movements'=>array(self::HAS_MANY, 'Movements', 'vehicle_reg_no'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(	
		    'reg_no' => 'Reg No',
			'vehicletype_id' => 'Vehicle Type',
			'engine_no' => 'Engine No',
			'cc' => 'CC',
			'vmodel' => 'Yr of MFG',
            'model_code' => 'Model Code',
			'chassis_no' => 'Chassis No',
			'country' => 'Country of Origin',
			'purchase_date' => 'Purchase Date',
			'purchase_price' => 'Purchase Price',
			'supplier' => 'Supplier',
			'seat_capacity' => 'Seat Capacity',
			'load_capacity' => 'Load Capacity',
			'ac' => 'Ac',
			'driver_pin' => 'Driver PIN',
			'location' => 'Location',
			'created_time' => 'Creation Time',
			'created_by' => 'Created By',
			'active' => 'Status',
            'mileage' => 'Mileage',
            'dedicate'=>'Duty Type',
            'imei_no'=>'IMEI No',
            'user_pin'=>'User Pin',
            'asset_id'=>'Asset Id',
            'actual_kpl'=>'Actual KPL Type',	
            'fuel_type'=>'Fuel Type',
            'tank_size'=>'Tank Size',
            'kpl_value'=>'Actual KPL Value',
            'allocated_to'=>'Allocated Department',
            'is_special'=>'Belongs to 39 vehicle group?'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('reg_no',$this->reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('engine_no',$this->engine_no,true);		
		$criteria->compare('cc',$this->cc,true);
		$criteria->compare('vmodel',$this->vmodel,true);
        $criteria->compare('model_code',$this->model_code,true);
		$criteria->compare('chassis_no',$this->chassis_no,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('purchase_price',$this->purchase_price);
		$criteria->compare('supplier',$this->supplier,true);
		$criteria->compare('seat_capacity',$this->seat_capacity);
		$criteria->compare('load_capacity',$this->load_capacity,true);
		$criteria->compare('ac',$this->ac);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('imei_no',$this->imei_no);
		$criteria->compare('user_pin',$this->user_pin);
		$criteria->compare('asset_id',$this->asset_id);
		$criteria->compare('actual_kpl',$this->actual_kpl);
		$criteria->compare('fuel_type',$this->fuel_type);
		$criteria->compare('tank_size',$this->tank_size);
		$criteria->compare('kpl_value',$this->kpl_value);	

        $criteria->order = 'created_time DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
			),
		));
	}


    /* protected function afterFind(){
        parent::afterFind();
        $this->purchase_date=date('d F, Y', strtotime(str_replace("-", "", $this->purchase_date)));
    }

        protected function beforeSave(){
        if(parent::beforeSave()){
            $this->purchase_date=date('Y-m-d', strtotime(str_replace(",", "", $this->purchase_date)));
            //$this->purchase_date=date('d-m-y', strtotime(str_replace(",", "", $this->purchase_date)));
            //var_dump($this->purchase_date);
            return TRUE;
        }
        else return false;
    }*/

  /*  protected function afterFind ()
    {
        list($y, $m, $d) = explode('-', $this->purchase_date);
        $mk=mktime(0, 0, 0, $m, $d, $y);
        $this->purchase_date = date ('d-m-Y', $mk);

        return parent::afterFind ();
    }

    protected function beforeSave ()
    {
        list($d, $m, $y) = explode('-', $this->purchase_date);
        $mk=mktime(0, 0, 0, $m, $d, $y);
        $this->purchase_date = date ('Y-m-d', $mk);
       // $this->purchase_date = date ('d-m-Y H:i:s');
        return parent::beforeSave ();
    }*/
    
 public function activity($ac)
    {
        if($ac == 1)
            return '<span style="color:green;">Active</span>';
        elseif($ac == 2)
            return '<span style="color:#ff6600;">For Auction</span>';
        elseif($ac == 3)
            return '<span style="color:red;">Sold</span>';
    }

    public function getVihicleCoordinates($reg_no) {
        $sql = "SELECT asset_id FROM tbl_vehicles WHERE reg_no = '$reg_no'";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $asset_id = $result[0]['asset_id'];
//        $run_one = Yii::app()->curl->run('http://api.finder-lbs.com/api/54f2cb4b421aa9493557fc09/current_location?email=bracblc123@gmail.com&asset='.$asset_id);
//        if(!$run_one->hasErrors()) {
//
//            echo '<pre>';
//            print_r(json_decode($run_one->getData()));
//            echo '</pre>';
//            echo $run_one->getInfo();
//
//        } else {
//            echo '<pre>';
//            var_dump($run_one->getErrors());
//            echo '</pre>';
//        }
        if($asset_id == null) return null;

       $request = file_get_contents("https://api.finder-lbs.com/api/54f2cb4b421aa9493557fc09/current_location?email=bracblc123@gmail.com&asset=".$asset_id);
       $result = json_decode($request,true);
       return $result[ 'response']['last_given_location']['coordinates'];
    }
    public function getVehicleCountOnHoNekaton() {
        $sql = "SELECT reg_no, asset_id, vehicletype_id FROM tbl_vehicles WHERE asset_id <> '' AND (vehicletype_id = 1 OR vehicletype_id = 2 OR vehicletype_id = 3)";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $hoJeepCount = 0;
        $hoCarCount = 0;
        $hoMicrobusCount = 0;
        $nekatonJeepCount = 0;
        $nekatonCarCount = 0;
        $nekatonMicrobusCount = 0;
        $hoLat = 23.780;
        $hoLng = 90.410;
        $nekatonLat = 23.729;
        $nekatonLng = 90.436;
        //var_dump($result);
        $latLng = array();
        foreach ($result as $item) {
            if(strlen($item['asset_id']) <6) { continue; }
            $coordinates = $this->getVihicleCoordinates($item['reg_no']);
            $lat = number_format($coordinates[1],3);
            $lng = number_format($coordinates[0],3);
            if($hoLat == $lat && $hoLng == $lng) {
                if($item['vehicletype_id'] == 1){
                    $hoJeepCount++;
                }
                if($item['vehicletype_id'] == 2){
                    $hoCarCount++;
                }
                if($item['vehicletype_id'] == 3){
                    $hoMicrobusCount++;
                }
            }
            if($nekatonLat == $lat && $nekatonLng == $lng) {
                if($item['vehicletype_id'] == 1){
                    $nekatonJeepCount++;
                }
                if($item['vehicletype_id'] == 2){
                    $nekatonCarCount++;
                }
                if($item['vehicletype_id'] == 3){
                    $nekatonMicrobusCount++;
                }
            }
            array_push($latLng,$lat.' '.$lng);
        }
        $vehicleCounts = array($hoJeepCount,$hoCarCount,$hoMicrobusCount,$nekatonJeepCount,$nekatonCarCount,$nekatonMicrobusCount);
        return $vehicleCounts;
    }
public function fuelType($type){
    if($type==1)return "CNG";
    if($type==2)return "Octane";
    if($type==3)return "Diesel";
    if($type==4)return "Petrol";
    if($type==5)return 'LPG';
    if($type==6)return "Others";
	}
	public function check39($i){
        if($i==0)return "No";
        if($i==1)return "Yes";
    }
}