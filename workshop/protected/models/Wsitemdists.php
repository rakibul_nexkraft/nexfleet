<?php

/**
 * This is the model class for table "{{wsitemdists}}".
 *
 * The followings are the available columns in table '{{wsitemdists}}':
 * @property integer $id
 * @property integer $task_id
 * @property integer $wsitem_id
 * @property string $item_name
 * @property string $quantity
 * @property string $item_size
 * @property string $item_slno
 * @property double $bill_no
 * @property string $bill_date
 
 * @property double $total_price
 * @property integer $active
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $update_time
 */
class Wsitemdists extends CActiveRecord
{
	public $item_qunt;
	
	public $defectID;
	public $bill_from_date;
	public $bill_to_date;
	public $issue_from_date;
	public $issue_to_date;
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsitemdists the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsitemdists}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('task_id, wsitemname_id, item_name, quantity, item_size, item_slno, bill_no, bill_date, total_price ', 'required'),
			array('defect_id, item_name, quantity ', 'required'),			
			array('task_id, defect_id, wsrequisition_id,wsitemname_id, wsitem_id, mechanic_pin, active', 'numerical', 'integerOnly'=>true),
			array('total_price', 'numerical'),
			array('vehicle_reg_no, bill_no, item_slno, vehicle_model,parts_no, mechanic_name,remark, supplier_name,available_stock, unit, created_by, updated_by', 'length', 'max'=>127),
			array('item_size,item_qunt,unit_price', 'length', 'max'=>20),
            array('issue_date, bill_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, task_id, defect_id, wsitemname_id, item_name, quantity, item_size, item_slno, bill_no, bill_date,  total_price, active, created_by, created_time, updated_by, update_time,bill_from_date,bill_to_date,issue_from_date,issue_to_date', 'safe', 'on'=>'search'),
            array('created_time','default','value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
            array('update_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'wsitemdistsubs'=>array(self::HAS_MANY, 'Wsitemdistsubs', 'wsitemdist_id'),
		'defects'=>array(self::BELONGS_TO, 'Defects', 'defect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'task_id' => 'Task',
			'defect_id' => 'Defect',			
			'wsitemname_id' => 'Itemname ID',
            'wsitem_id' => 'Wsitem ID',
			'wsrequisition_id' => 'WsRequisition ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			
			'item_name' => 'Item Name',
            'vehicle_model' => 'Model Code',
            'parts_no' => 'Parts No',
            'item_qunt'=>'Item Quntity',
            'supplier_name'=>'Supplier Name',
			'quantity' => 'Quantity Issued',
			'item_size' => 'Item Size',
			'item_slno' => 'Item Slno',
            'issue_date' => 'Issue Date',
			'bill_no' => 'Bill No',
			'bill_date' => 'Bill Date',
			'unit_price'=>'Unit Price',
			'total_price' => 'Total Price',
            'mechanic_pin' => 'Issue To - PIN',
            'mechanic_name' => 'Issue To - Name',
			'active' => 'Active',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'update_time' => 'Update Time',
			'remark'=>'Remarks',
			'bill_to_date'=>'To Purchase Date',
			'bill_from_date'=>'Purchase Date',
			'issue_to_date'=>'To Issue Date',
			'issue_from_date'=>'Issue Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('defect_id',$this->defect_id);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('wsitemname_id',$this->wsitemname_id);
        $criteria->compare('wsitem_id',$this->wsitem_id,true);
		$criteria->compare('item_name',$this->item_name,true);
        $criteria->compare('vehicle_model',$this->vehicle_model,true);
        $criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
        $criteria->compare('parts_no',$this->parts_no,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('available_stock',$this->available_stock);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('item_size',$this->item_size,true);
		$criteria->compare('item_slno',$this->item_slno,true);
        $criteria->compare('issue_date',$this->issue_date);
		$criteria->compare('bill_no',$this->bill_no);
		$criteria->compare('bill_date',$this->bill_date);
		$criteria->compare('total_price',$this->total_price);
        $criteria->compare('mechanic_pin',$this->mechanic_pin);
        $criteria->compare('mechanic_name',$this->mechanic_name);
		$criteria->compare('active',$this->active);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('update_time',$this->update_time,true);

		/*if(!empty($this->bill_from_date) && empty($this->bill_to_date))
		{
			$criteria->addCondition('bill_date = "'.$this->bill_from_date.'"');
		}
		elseif(!empty($this->bill_to_date) && empty($this->bill_from_date))
		{
			$criteria->addCondition('bill_date <= "'.$this->bill_to_date.'"');
		}
		elseif(!empty($this->bill_to_date) && !empty($this->bill_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('bill_date BETWEEN "'.$this->bill_from_date.'" AND "'.$this->bill_to_date.'"');
		}
		else{}*/

		if(!empty($this->issue_from_date) && empty($this->issue_to_date))
		{
			$criteria->addCondition('issue_date = "'.$this->issue_from_date.'"');
		}
		elseif(!empty($this->issue_to_date) && empty($this->issue_from_date))
		{
			$criteria->addCondition('issue_date <= "'.$this->issue_to_date.'"');
		}
		elseif(!empty($this->issue_to_date) && !empty($this->issue_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('issue_date BETWEEN "'.$this->issue_from_date.'" AND "'.$this->issue_to_date.'"');
		}
		else{}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>1000000,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
	public function search1()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.defect_id',$this->defect_id);
		$criteria->compare('t.task_id',$this->task_id);
		$criteria->compare('t.wsitemname_id',$this->wsitemname_id);
        $criteria->compare('t.wsitem_id',$this->wsitem_id,true);
		$criteria->compare('t.item_name',$this->item_name,true);
        $criteria->compare('t.vehicle_model',$this->vehicle_model,true);
        //$criteria->compare('t.vehicle_reg_no',$this->vehicle_reg_no,true);
        $criteria->compare('t.parts_no',$this->parts_no,true);
		$criteria->compare('t.quantity',$this->quantity);
		$criteria->compare('t.available_stock',$this->available_stock);
		$criteria->compare('t.unit_price',$this->unit_price);
		$criteria->compare('t.item_size',$this->item_size,true);
		$criteria->compare('t.item_slno',$this->item_slno,true);
        $criteria->compare('t.issue_date',$this->issue_date);
		$criteria->compare('t.bill_no',$this->bill_no);
		$criteria->compare('t.bill_date',$this->bill_date);
		$criteria->compare('t.total_price',$this->total_price);
        $criteria->compare('t.mechanic_pin',$this->mechanic_pin);
        $criteria->compare('t.mechanic_name',$this->mechanic_name);
		$criteria->compare('t.active',$this->active);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.updated_by',$this->updated_by,true);
		$criteria->compare('t.update_time',$this->update_time,true);

		if(!empty($this->bill_from_date) && empty($this->bill_to_date))
		{
			$criteria->addCondition('t.bill_date = "'.$this->bill_from_date.'"');
		}
		elseif(!empty($this->bill_to_date) && empty($this->bill_from_date))
		{
			$criteria->addCondition('t.bill_date <= "'.$this->bill_to_date.'"');
		}
		elseif(!empty($this->bill_to_date) && !empty($this->bill_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('t.bill_date BETWEEN "'.$this->bill_from_date.'" AND "'.$this->bill_to_date.'"');
		}
		else{}

		if(!empty($this->issue_from_date) && empty($this->issue_to_date))
		{
			$criteria->addCondition('t.issue_date = "'.$this->issue_from_date.'"');
		}
		elseif(!empty($this->issue_to_date) && empty($this->issue_from_date))
		{
			$criteria->addCondition('t.issue_date <= "'.$this->issue_to_date.'"');
		}
		elseif(!empty($this->issue_to_date) && !empty($this->issue_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('t.issue_date BETWEEN "'.$this->issue_from_date.'" AND "'.$this->issue_to_date.'"');
		}
		else{}
		$criteria->with = array('defects');
	    $criteria->together = true;
	    $criteria->compare('defects.vehicle_reg_no',$this->vehicle_reg_no,true);
		//$criteria->condition ="$this->defect_id";
		//$criteria->select = array('defects.id as fr');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'t.id DESC')
		));
	}
	public function getTotal($records, $column)
        {
                $total = 0;
                foreach ($records as $record) {
                        $total += $record->$column;
                }
                return $total;
        }
    public function getTotalPurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('wsitemname_id',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';
			$purchesTotal=Wsstocks::model()->findAll($criteria);
			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->stock_in;
			}
			
               return $total;
        }
    public function getTotalIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('wsitemname_id',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';
			$purchesTotal=self::model()->findAll($criteria);
			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->quantity;
			}
			
               return $total;
        }
    public function getDateWisePurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model,$bill_from_date,$bill_to_date)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('wsitemname_id',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->compare('purchase_date',$bill_from_date);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';

		if(!empty($bill_from_date) && empty($bill_to_date))
		{
			$criteria->addCondition('purchase_date = "'.$bill_from_date.'"');
		}
		elseif(!empty($bill_to_date) && empty($bill_from_date))
		{
			$criteria->addCondition('purchase_date <= "'.$bill_to_date.'"');
		}
		elseif(!empty($bill_to_date) && !empty($bill_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('purchase_date BETWEEN "'.$bill_from_date.'" AND "'.$bill_to_date.'"');
		}
		else{
			//$criteria->addCondition("purchase_date = '0000-00-00'");
		}


			$purchesTotal=Wsstocks::model()->findAll($criteria);

			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->stock_in;
			}
			
               return $total;
        }
  public function getDateWiseIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model,$issue_from_date,$issue_to_date)
        {
                
                $criteria=new CDbCriteria;
                //$stocks=new Wsstocks;

	        $criteria->compare('vehicle_reg_no',$vehicle_reg_no);
			$criteria->compare('wsitemname_id',$wsitemname_id);
			$criteria->compare('vehicle_model',$vehicle_model);
			//$criteria->compare('purchase_date',$bill_from_date);
			//$criteria->select = 'sum(stock_in) AS sumPurchesTotal';

		if(!empty($issue_from_date) && empty($issue_to_date))
		{
			$criteria->addCondition('issue_date = "'.$issue_from_date.'"');
		}
		elseif(!empty($issue_to_date) && empty($issue_from_date))
		{
			$criteria->addCondition('issue_date <= "'.$issue_to_date.'"');
		}
		elseif(!empty($issue_to_date) && !empty($issue_from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('issue_date BETWEEN "'.$issue_from_date.'" AND "'.$issue_to_date.'"');
		}
		else{
			//$criteria->addCondition("purchase_date = '0000-00-00'");
		}


			$purchesTotal=self::model()->findAll($criteria);

			$total=0;
			foreach ($purchesTotal as $key => $value) {
				$total=$total+$value->quantity;
			}
			
               return $total;
        }
        public function getConsumtionRate($vehicle_reg_no,$wsitemname_id,$vehicle_model,$bill_from_date,$bill_to_date,$issue_from_date,$issue_to_date)
        {
        	if(!empty($bill_from_date) && !empty($issue_from_date)){

        		$DateWisePurchase=$this->getDateWisePurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model,$bill_from_date,$bill_to_date);
        		$DateWiseIssue=$this->getDateWiseIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model,$issue_from_date,$issue_to_date);
        		return ($DateWiseIssue*100)/$DateWisePurchase;
        	}
        	else{
        		 $total_purchase=$this->getTotalPurchase($vehicle_reg_no,$wsitemname_id,$vehicle_model);
        		$total_issue=$this->getTotalIssue($vehicle_reg_no,$wsitemname_id,$vehicle_model);

        		return ($total_issue*100)/$total_purchase;
        	}
        	

        }
}