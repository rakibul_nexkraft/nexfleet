<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawMultSeries);

    function drawMultSeries() {
        var data = new google.visualization.DataTable();
        data.addColumn('timeofday', 'Time of Day');
        data.addColumn('number', 'Approved');
        data.addColumn('number', 'Rejected');

        data.addRows([
            [{v: [8 ], f: '8 '}, 1, .25],
            [{v: [9, 0, 0], f: '9 '}, 2, .5],
            [{v: [10, 0, 0], f:'10 '}, 3, 1],
            [{v: [11, 0, 0], f: '11 '}, 4, 2.25],
            [{v: [12, 0, 0], f: '12 '}, 5, 2.25],
            [{v: [13, 0, 0], f: '1 '}, 6, 3],
            [{v: [14, 0, 0], f: '2 '}, 7, 4],
            [{v: [15, 0, 0], f: '3 '}, 8, 5.25],
            [{v: [16, 0, 0], f: '4 '}, 9, 7.5],
            [{v: [17, 0, 0], f: '5 '}, 10, 10],
        ]);

        var cssClassNames = {
        };
        var options = {
            title: '',
            pieHole: 0.4,
            allowHtml: true,
            cssClassNames : cssClassNames,
            height:400, // example height, to make the demo charts equal size
            width:450,
            bar: {groupWidth: "40" },
            colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
            backgroundColor: 'transparent',
            datalessRegionColor: '#37474f',
            legend: {alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '14'},},
            hAxis: {
                title: '',

                viewWindow: {
                    min: [7, 30, 0],
                    max: [17, 30, 0]
                                    }
            },
            vAxis: {
                title: ''

            }

        };

        var chart = new google.visualization.ColumnChart(
            document.getElementById('chart_div'));

        chart.draw(data, options);
    }
</script>-->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Approved',     80],
                ['Pendings',      14],
                ['Queue',  4],
                ['Cancel',  2]
            ]);

            var cssClassNames = {
            };
            var options = {
                title: '',
                is3D: true,
                pieHole: 0.4,
                allowHtml: true,
                cssClassNames : cssClassNames,
                height:400, // example height, to make the demo charts equal size
                width:500,
                bar: {groupWidth: "40" },
                colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
                backgroundColor: 'transparent',
                datalessRegionColor: '#37474f',
                legend: {alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '14'},},
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
            chart.draw(data, options);
        }
    </script>


<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Requisitions',     80],
            ['Approved',      78],
            ['Pendings',  2]
        ]);

        var cssClassNames = {
        };
        var options = {
            pieHole: 0.4,
            allowHtml: true,
            cssClassNames : cssClassNames,
            height:400, // example height, to make the demo charts equal size
            width:500,
            bar: {groupWidth: "40" },
            colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
            backgroundColor: 'transparent',
            datalessRegionColor: '#37474f',
            legend: {alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '14'},},
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>-->
<section id="page-title" class="page-title-center page-title-nobg noborder nopadding bottommargin-lg">

    <div class="box-container" style="background: #fff; padding-left: 35px;">
        <div class="box-header" style="border-bottom: none !important;">
            <p class="box-header-text"><i class="fa fa-chart-bar"></i> Shohochor</p>
        </div>
        <div class="box-body">
            <div class="col_one_fourth box-border box-border--item-color-one border-custom pick_drop_summery cursor-pointer"
                 style="">
                <div style="background: #97bb9e;" class="col_one_fourth left-badge"></div>
                <div class="box-header-label">Expenditure</div>
                 <?php $exp=Yii::app()->db->createCommand("SELECT SUM(price) as exp FROM tbl_seat_booking WHERE user_pin='".Yii::app()->user->username."'")->queryRow();
                
                 ?>
                <a href="">
                    <div class="box-bottom-number">
                    <?php 
                    echo (!empty($exp['exp'])) ? $exp['exp'] : 0; 
                    ?> </div>
                </a>
            </div>


            <div class="col_one_fourth box-border box-border--item-color-tow border-custom pick_drop_summery cursor-pointer">
                <div style="background: #7e57c1;" class="col_one_fourth left-badge"></div>
                <div class="box-header-label">Earning</div>
                 <?php $ear=Yii::app()->db->createCommand("SELECT SUM(s.price) as ear FROM tbl_seat_booking as b INNER JOIN tbl_seat_sale as s ON s.id=b.sale_id WHERE s.user_pin='".Yii::app()->user->username."'")->queryRow();

                 ?>
                <a href="">
                    <div class="box-bottom-number"><?php 
                    echo (!empty($ear['ear'])) ? $ear['ear']: 0; 
                   ?></div>
                </a>
            </div>


            <div class="col_one_fourth box-border box-border--item-color-three border-custom pick_drop_summery cursor-pointer">
                <div style="background: #84b269;" class="col_one_fourth left-badge"></div>
                <div class="box-header-label">
                    <?php echo CHtml::link('Balance',array('/transactionHistory/userBlance'));?>
                </div>

                <a href="">
                    <div class="box-bottom-number"><?php  
                    $b = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin='".Yii::app()->user->username."' ORDER BY id DESC")->queryRow();
                        echo $balance = empty($b) ? 0.0 : $b['balance'];?>
                        
                    </div>
                </a>
            </div>


            <div class="col_one_fourth box-border box-border--item-color-four border-custom pick_drop_summery cursor-pointer">
                <div style="background: #c5bf89;" class="col_one_fourth left-badge"></div>
                <div class="box-header-label">Seat Available</div>
                
                <a href=''>
                    <div class="box-bottom-number"><?php echo $seats_av;?></div>
                </a>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</section>
<div class="col_full custom-box-design background-remove" >
    <!--<h3 class=" heading-block center" style="padding-top: 15px;">Your Requisition Status</h3>-->
    <div class="col_half userpickAndDrop-chart-container">
        <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
            <h4 class="heading-custom page_header_h4">Route Status</h4>
        </div>
        <!--<p class=" heading-block bottommargin-sm center history-header box-header-text" style="margin-top: 20px; font-weight: 700; font-family: 'Raleway', sans-serif ;text-transform: uppercase; color: #333333">Your Route Status</p>-->
        <div id="piechart_3d" class="mydashboardpiechart-responsive" style="margin-top: -85px;"></div>
    </div>

<!--    <div class="col_half userpickAndDrop-chart-container col_last">
        <div id="chart_div"></div>
    </div>-->

    <div class="col_half userpickAndDrop-chart-container col_last"  style="" >
        <div class="col_full" >
            <!--<div class="heading-block bottommargin-sm center history-header box-header-text">
                <h3 style="padding-top: 15px">Your Route Recent SMS OR Email</h3>
            </div>-->
            <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
                <h4 class="heading-custom page_header_h4">Route Recent SMS OR Email</h4>
            </div>
            <div class="col_full" style="padding: 20px 10px;max-height: 283px;overflow-y: auto;overflow-x: auto;">
                <table class="table table-approved-req  table-responsive table-hover noborder">
                    <thead class="tbl-header">
                    <tr>
                        <th>Subject</th>
                        <th>Body</th>
                        <th>Type</th>
                        <th>Date & Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    //var_dump($u_req_app_five);
                    //$sms_email=Yii::app()->db->createCommand("SELECT * FROM tbl_email_sms LIMIT 5")->queryRow();
                    //var_dump($sms_emai);exit();
                    if (count($sms_email) <= 0){
                        echo
                            '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                            '<tr>'.
                            '</tr>';
                    }
                    foreach ($sms_email as $value){
                        echo
                            '<tr>'.
                            '<td>'.$value['sub'].'</td>'.
                            '<td>'.$value['body'].'</td>'.
                            '<td>'.(($value['type']==3) ? "Email & SMS" :(($value['type']==1) ? "Email" : "SMS" )).'</td>'.
                            '<td>'.$value['created_time'].'</td>'.
                            '</tr>';
                    } ?>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</div>
<div class="clear"></div>
</div>

  