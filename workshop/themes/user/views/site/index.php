<style type="text/css">
    #all_route{
        display: none;
    }
    .cell-hover:hover{
        background: rgba(53,217,125,0.1);
    }
    .history-box{
        position: relative;
        border: 1px solid rgba(0,0,0,0.075);
        border-radius: 3px;
        text-align: center;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        background-color: #F5F5F5;
    }
    .history-extended{
        background-color: #FFF;
        text-align: left;
    }
    .history-header{
        /* border-bottom: 1px solid #dedede; */
        padding: 10px 20px;
        background-color: #2a57ff;
        background-image: linear-gradient(45deg, #fc3ba2, blue);
        box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
        /* width: 95%; */
        border-radius: 5px;
        margin: 0 auto;
        position: relative;
        top: 54px;
        color: white;
        z-index: 1;
    }
    .history-header > h3{
        color: white;
    }
    .tbl-header{
        /*box-shadow: 0 3px 2px -2px rgba(0,0,0,.3); */
        /* border-radius: 5px; */
        /* border: 1px solid #dedede; */
        background-color: #f2f2f2;
    }
    .heading-block:after {
        display: none;
    }
</style>

<section id="page-title" class="page-title-center page-title-nobg noborder nopadding">
    <div class="col_one_third border-custom pick_drop_summery cursor-pointer" onclick="routenDetail()" >
        <div class="col_full heading-block bg-grad-available nobottommargin center" style="border-radius: 25px;">
            <div class="col_one_third nobottommargin" style="display: flex;justify-content: center;align-items: center;">
                <i class="icon-tasks" style="background: transparent;font-size: 48px;"></i>
            </div>
            <div class="col_two_third nobottommargin col_last">
                <div class="heading-text text-nowrap text-truncate" style="margin: auto;padding: 20px;">Route<br>Summery</div>
            </div>
        </div>
    </div>
    <div class="col_one_third border-custom cursor-pointer" onclick="marketPlaceDetail()" >
        <div class="col_full heading-block bg-grad-onduty nobottommargin center" style="border-radius: 25px;">
            <div class="col_one_third nobottommargin" style="display: flex;justify-content: center;align-items: center;">
                <i class="icon-bar-chart" style="background: transparent;font-size: 48px;"></i>
            </div>
            <div class="col_two_third nobottommargin col_last">
                <div class="heading-text text-nowrap text-truncate" style="margin: auto;padding: 20px;">Market<br>Place</div>
            </div>
        </div>
    </div>
    <div class="col_one_third col_last border-custom cursor-pointer" onclick="lateSummery()" >
        <div class="col_full heading-block bg-grad-workshop nobottommargin center" style="border-radius: 25px;">
            <div class="col_one_third nobottommargin" style="display: flex;justify-content: center;align-items: center;">
                <i class="icon-paste" style="background: transparent;font-size: 48px;"></i>
            </div>
            <div class="col_two_third nobottommargin col_last">
                <div class="heading-text text-nowrap text-truncate" style="margin: auto;padding: 20px;">Market<br>Place</div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</section>


<!--<pre>< ?php print_r($userSeat->attributes) ?></pre>-->
<!--<section id="page-title" class="page-title-center page-title-nobg noborder" style="padding-bottom: 0px">
    <div class="col_one_third">
          <a href="javascript:void(0)" role="button" onclick="routenDetail()" class="button button-desc button-3d button-rounded button-green center">Route Summery<span><i class="i-light  icon-tasks" style="background-color:rgba(53,217,125,0.07);"></i></span>

          </a>
    </div>
    <div class="col_one_third">
          <a href="javascript:void(0)" role="button" onclick="marketPlaceDetail()" class="button button-desc button-3d button-rounded button-green center">Market Place
            <span><i class=" i-light icon-bar-chart" style="background-color:rgba(53,217,125,0.07)"></i></span>
          </a>

    </div>
    <div class="col_one_third col_last">
          <a href="javascript:void(0)" role="button" onclick="lateSummery()" class="button button-desc button-3d button-rounded button-green center">Late Summery
            <span><i class=" i-light icon-paste" style="background-color:rgba(53,217,125,0.07)"></i></span>
          </a>

    </div>
    <div class="clearfix"></div>
</section>-->
<section id="page-title" class="page-title-center page-title-nobg noborder" >
    <!--<div class="col_half ">
        <label for="" style="font-size: 1.5rem">Recent Purchases <?php echo $aaa ?></label>
        <table class="table table-hover">
            <thead style="box-shadow: 0 3px 2px -2px rgba(0,0,0,.3);">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($recent_booking) == 0){
                    echo
                        '<tr>'.
                            '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '</tr>';
                }
                foreach ($recent_booking as $booking){
                    echo
                        '<tr>'.
                            '<td>'.$booking['release_date'].'</td>'.
                            '<td>'.$booking['route_id'].'</td>'.
                            '<td>'.$booking['travel_direction'].'</td>'.
                            '<td>'.$booking['price'].'</td>'.
                        '</tr>';
                } ?>
            </tbody>
        </table>
    </div>
   <div class="col_half col_last">
        <label for="" style="font-size: 1.5rem">Recent Sell</label>
        <table class="table table-hover">
            <thead style="box-shadow: 0 3px 2px -2px rgba(0,0,0,.3);">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($recent_sell) == 0){
                    echo
                        '<tr>'.
                            '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '</tr>';
                }
                foreach ($recent_sell as $sell){
                    echo
                        '<tr>'.
                            '<td>'.$sell['release_date'].'</td>'.
                            '<td>'.$sell['route_id'].'</td>'.
                            '<td>'.$sell['travel_direction'].'</td>'.
                            '<td>'.$sell['price'].'</td>'.
                        '</tr>';
                } ?>
            </tbody>
        </table>
    </div>-->
</section>
<script>
    function routenDetail(){
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/seatRequest/dashboardRouteDetail");?>',{},function(data){
                        
                            //openModel('<h3>'+data+'</h3>');
            bsModalOpen('<h3>'+data+'</h3>');

                        
                });
    }
    function marketPlaceDetail(){
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/seatRequest/userMarketPlaceSummery");?>',{},function(data){

            bsModalOpen('<h3>'+data+'</h3>');

                        
                });

    }
    function lateSummery(){
         $.post('<?php echo Yii::app()->createAbsoluteUrl("/emailSms/lateSummery");?>',{},function(data){
             bsModalOpen('<h3>'+data+'</h3>');
                        
                        
                });

    }
</script>