<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/26/2019
 * Time: 5:12 PM
 */
?>

<div class="col_full">
    <button class="btn btn-primary" onclick="toggleDriverDetail()">Back to List</button>
</div>
<div class="clear"></div>
<div class="col_one_fifth">
    <img class="img-thumbnail" src="<?php echo Yii::app()->baseUrl;?>/../common/assets/images/ifleet-logo.png">

</div>
<div class="col_four_fifth col_last">
    <?php
    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$driver,
        'htmlOptions'=>array('class'=>'table table-hover')
    ));
    ?>
    <?php //var_dump($driver) ?>
</div>
<div class="clear"></div>

<div class="col_full">
    <?php
        $this->renderPartial('stoppage_detail', array(
            'stoppages'=>$stoppages
        ))
    ?>
</div>