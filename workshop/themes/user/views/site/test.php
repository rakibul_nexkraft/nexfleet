<section id="slider" class="slider-parallax revslider-wrap ohidden clearfix">

    <!--
    #################################
        - THEMEPUNCH BANNER -
    #################################
    -->
    <div class="tp-banner-container">
        <div class="tp-banner" >
            <ul>    <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000000" data-saveperformance="off" data-title="Chart" style="background-color: #E9E8E3;"><!--background-color: #F6F6F6;-->
                    <!-- LAYERS -->

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                         data-x="75"
                         data-y="50"
                         data-customin="x:-200;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="400"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="">
                        <div id="nextDayPieChart" style="width:350px; height:350px;"></div>
                    </div>
                    <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                         data-x="650"
                         data-y="50"
                         data-customin="x:-200;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1500"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style="">
                        <div id="todayPieChart" style="width:350px; height:350px;"></div>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-delay="10000000"  data-saveperformance="off"  data-title="Marketplace Summary" style="background-color: #E9E8E3;">
                    <!-- LAYERS -->

                    <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                         data-x="630"
                         data-y="78"
                         data-customin="x:250;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="400"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/homepage/ecommerce-and-marketplace.png" alt="Pick and drop illustration"></div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                         data-x="0"
                         data-y="110"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=" color: #333;">Book and sell unused seats</div>

                    <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
                         data-x="0"
                         data-y="140"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1200"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=" color: #333; line-height: 1.15;">Marketplace</div>

                    <div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text tleft"
                         data-x="0"
                         data-y="240"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1400"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=" color: #333; max-width: 550px; white-space: normal;">We guaranty you to provide safest commute in this city with our expert drivers.</div>

                    <div class="tp-caption customin ltl tp-resizeme"
                         data-x="0"
                         data-y="340"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1550"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=""><a class="button button-border button-large button-rounded tright nomargin"><span>Available Seats</span> <i class="icon-angle-right"></i> <strong><?php echo $counts['market_seats'] ?></strong></a></div>

                    <!--<div class="tp-caption customin utb tp-resizeme revo-slider-caps-text uppercase"
                         data-x="510"
                         data-y="0"
                         data-customin="x:0;y:-236;z:0;rotationZ:0;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="600"
                         data-start="2100"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=""><img src="<?php /*echo Yii::app()->theme->baseUrl; */?>/images/slider/rev/shop/tag.png" alt="Bag"></div>-->

                </li>

                <li data-transition="slideup" data-slotamount="1" data-masterspeed="1500" data-delay="10000000"  data-saveperformance="off"  data-title="Pick and Drop Summary" style="background-color: #E9E8E3;">
                    <!-- LAYERS -->

                    <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                         data-x="630"
                         data-y="78"
                         data-customin="x:250;y:0;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="400"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=""><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/homepage/ecommerce-and-marketplace.png" alt="Pick and drop illustration"></div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                         data-x="0"
                         data-y="110"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1000"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=" color: #333;">Get your office commute daily</div>

                    <div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
                         data-x="0"
                         data-y="140"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1200"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=" color: #333; line-height: 1.15;">Pick and Drop</div>

                    <div class="tp-caption customin ltl tp-resizeme revo-slider-desc-text tleft"
                         data-x="0"
                         data-y="240"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1400"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=" color: #333; max-width: 550px; white-space: normal;">Join us and find your daily commute to office in any route. We cover all over the dhaka city.</div>

                    <div class="tp-caption customin ltl tp-resizeme"
                         data-x="0"
                         data-y="340"
                         data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                         data-speed="700"
                         data-start="1550"
                         data-easing="easeOutQuad"
                         data-splitin="none"
                         data-splitout="none"
                         data-elementdelay="0.01"
                         data-endelementdelay="0.1"
                         data-endspeed="1000"
                         data-endeasing="Power4.easeIn" style=""><a href="#" class="button button-border button-large button-rounded tright nomargin"><span>Available Route</span> <i class="icon-angle-right"></i> <strong><?php echo $counts['route_seats'] ?></strong></a></div>
                </li>
            </ul>
        </div>
    </div>

    <script type="text/javascript">

        var tpj=jQuery;
        tpj.noConflict();

        tpj(document).ready(function() {

            var apiRevoSlider = tpj('.tp-banner').show().revolution(
                {
                    sliderType:"standard",
                    jsFileLocation:"include/rs-plugin/js/",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {},
                    responsiveLevels:[1200,992,768,480,320],
                    gridwidth:1140,
                    gridheight:500,
                    lazyType:"none",
                    shadow:0,
                    spinner:"off",
                    autoHeight:"off",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        disableFocusListener:false,
                    },
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        onHoverStop:"off",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "ares",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: false,
                            tmp: '<div class="tp-title-wrap">	<span class="tp-arr-titleholder">{{title}}</span> </div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            }
                        }
                    }
                });

        });

    </script>
</section>

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap" style="padding-bottom:0">

        <div class="promo promo-light promo-full bottommargin-lg header-stick notopborder">
            <div class="container clearfix">
                <h3>Call us anytime at <span>+8801XXXXXXXXX</span> or Email us at <span>info@brac.net</span></h3>
                <span>We provide best transportation system in the world</span>
                <a href="#" class="button button-dark button-xlarge button-rounded">Be a member</a>
            </div>
        </div>

        <div class="container clearfix">

            <div class="col_one_fourth nobottommargin">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    <div class="fbox-icon cursor-pointer">
                        <a onclick="getStats('PND')"><i class="i-alt noborder icon-shop"></i></a>
                    </div>
                    <h3>Seat Reservation<span class="subtitle">Start booking</span></h3>
                </div>
            </div>

            <div class="col_one_fourth nobottommargin">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    <div class="fbox-icon cursor-pointer">
                        <a onclick="getStats('MKT')"><i class="i-alt noborder icon-wallet"></i></a>
                    </div>
                    <h3>Marketplace<span class="subtitle">Book your today's seat</span></h3>
                </div>
            </div>

            <div class="col_one_fourth nobottommargin">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    <div class="fbox-icon">
                        <a><i class="i-alt noborder icon-megaphone"></i></a>
                    </div>
                    <h3>Requisition<span class="subtitle">Travel anytime anywhere</span></h3>
                </div>
            </div>

            <div class="col_one_fourth nobottommargin col_last">
                <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder">
                    <div class="fbox-icon cursor-pointer">
                        <a onclick="getStats('SUP')"><i class="i-alt noborder icon-fire"></i></a>
                    </div>
                    <h3>Support<span class="subtitle">Call between 8:30AM to 5PM</span></h3>
                </div>
            </div>

            <div class="clear"></div><!--<div class="line bottommargin-lg">-->
        </div>
    </div>

    <div class="section topmargin-sm nobottommargin">

        <div class="container clearfix">

            <div class="heading-block center">
                <h3>Feedback</h3>
                <span>Check out some of our Customer Reviews</span>
            </div>

            <!--<canvas id="barChart2"></canvas>-->
            <!--<div style="width: 400px;height: 400px;">
                <canvas id="nextDayPieChart"></canvas>
            </div>-->
            <?php if (count($comments)): ?>
                <ul class="testimonials-grid grid-3 clearfix nobottommargin">
                    <?php foreach ($comments as $comment): ?>
                        <li>
                            <div class="testimonial">
                                <div class="testi-image">
                                    <a href="#"><img src="<?php echo Yii::app()->baseUrl. '/uploads/homepage/'. $comment->image_url; ?>" alt="<?php $comment->name ?>'s comment"></a>
                                </div>
                                <div class="testi-content">
                                    <p><?php echo $comment->text; ?></p>
                                    <div class="testi-meta">
                                        <?php echo $comment->name; ?>
                                        <span><?php echo $comment->title; ?></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
            <div class="testimonials-grid grid-3 clearfix nobottommargin center">
                <div style="height: 147px;">
                    <div class="testimonial">

                        <div class="testi-content">
                            <p>Currently there are no user comments available.</p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>

    </div>
</section><!-- #content end -->
<style>
    #loader{
        text-align: center;
        background: url(<?php echo Yii::app()->theme->baseUrl;?>/images/ajax-loader.gif) no-repeat center;
        height: 300px;
    }
    #loader2{
        text-align: center;
        background: url(<?php echo Yii::app()->theme->baseUrl;?>/images/ajax-loader.gif) no-repeat center;
        height: 300px;
    }
</style>
<div class="modal fade" id="statsModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>-->
            <div class="modal-body">
                <div id="routeList">
                    <h4 class="theme-bottom-border"> Available Seats</h4>
                    <div id="loader" style="display: block"></div>
                    <div id="tableData" style="height: 300px;overflow-y: auto; display: none;"></div>
                </div>
                <div id="driverInfo">
                    <div id="loader2" style="display: block"></div>
                    <div id="driverDetail" style="height: 300px;overflow-y: auto; display: none;"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button button-mini button-column button-3d button-green" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<!--<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>-->

<script type="text/javascript">
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "0",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    <?php if(isset(Yii::app()->user->username)){ ?>
    var showPickDropNotification = function () {
        jQuery.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("homepage/notification");?>',
            dataType: 'json',
            success: function(dataArr){
                if(dataArr.length > 0){
                    dataArr.forEach(function (data) {
                        toastr.info(data['description'], data['title']);
                    })
                }
            }
        });
    };
    showPickDropNotification();
    setInterval(showPickDropNotification, 60000);
    <?php } ?>

    // based on prepared DOM, initialize echarts instance
    var myChart = echarts.init(document.getElementById('nextDayPieChart'));
    var myChart2 = echarts.init(document.getElementById('todayPieChart'));

    var getRequisitionStats = function () {
        jQuery.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("site/requisitionStats");?>',
            dataType: 'json',
            success: function(data){
                setChartOptions(data);
            }
        });
    };
    getRequisitionStats();
    setInterval(getRequisitionStats, 300000);
    var setChartOptions = function (data) {
        var dataNextday = [
            {value:data['nextDayData']['approved'], name:'Approved', itemStyle: {color: '#bc5090'}},
            {value:data['nextDayData']['notapproved'], name:'Not Approved', itemStyle: {color: '#003f5c'}},
            {value:data['nextDayData']['pending'], name:'Pending', itemStyle: {color: '#ffa600'}}
        ];
        var dataToday = [
            {value:data['todayData']['approved'], name:'Approved'},
            {value:data['todayData']['notapproved'], name:'Not Approved'},
            {value:data['todayData']['pending'], name:'Pending'}
        ];
        var nextdayTotal = data['approved'] + data['notapproved'] + data['pending'];
        var nextdayTotal = data['approved'] + data['notapproved'] + data['pending'];
        var optionNextday = {
            title : {
//            text: 'TITLE',
                subtext: 'Next day requisition',
                x:'center',
                sublink: 'https://facebook.com',
                target: 'blank',
                subtextStyle: {
                    lineHeight: 18,
                    fontSize: 16,
                    verticalAlign: 'middle',
                    rich:{
                        a: {
                            verticalAlign: 'middle',
                        },
                        b: {
                            verticalAlign: 'middle',
                        },
                        c: {
                            verticalAlign: 'middle',
                        }
                    }
                }
            },
            /*legend: {
             orient: 'vertical',
             left: 'left',
             data: ['Available','On duty','Pending']
             },*/
            tooltip: {},
            series : [
                {
                    name: 'Next Day',
                    type: 'pie',
                    radius : '75%',
                    center: ['50%', '60%'],
					label: {
						normal: {
							formatter: '{b} \n\n{c}',
							position: 'inside'
						}
					},
                    data:dataNextday,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        var optionToday = {
            title : {
//            text: 'TITLE',
                subtext: 'Today requisition',
                x:'center',
                sublink: 'https://facebook.com',
                target: 'blank',
                subtextStyle: {
                    lineHeight: 18,
                    fontSize: 16,
                    verticalAlign: 'middle',
                    rich:{
                        a: {
                            verticalAlign: 'middle',
                        },
                        b: {
                            verticalAlign: 'middle',
                        },
                        c: {
                            verticalAlign: 'middle',
                        }
                    }
                }
            },
//        legend: {
//            orient: 'horizontal',
////            left: 'left',
//            bottom: '-10',
//            data: ['Available','On duty','Pending'],
//            align: 'auto'
//        },
            tooltip: {},
            series : [
                {
                    name: 'Today',
                    type: 'pie',
                    radius : '75%',
                    center: ['50%', '60%'],
					label: {
						normal: {
							formatter: '{b} \n\n{c}',
							position: 'inside'
						}
					},
                    data:dataToday,
                    itemStyle: {
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    }
                }
            ]
        };
        // use configuration item and data specified to show chart
        myChart.setOption(optionNextday);
        myChart2.setOption(optionToday);
    };
</script>
<script>
    var getStats = function (type) {
        document.getElementById('driverDetail').innerHTML = "";
        document.getElementById("routeList").style.display = "block";
        document.getElementById("driverInfo").style.display = "none";
        if (type == 'PND'){
            document.getElementById('loader').style.display = 'block';
            document.getElementById('tableData').style.display = 'none';
            $('#statsModal').modal();
            $.ajax({
                type:'GET',
                url:'<?php echo Yii::app()->createUrl("site/pickAndDropStats");?>',
                success: function(html){
                    document.getElementById('tableData').innerHTML = html;
                    document.getElementById('loader').style.display = 'none';
                    document.getElementById('tableData').style.display = 'block';
                }
            });
        } else if (type == 'MKT') {
            document.getElementById('loader').style.display = 'block';
            document.getElementById('tableData').style.display = 'none';
            $('#statsModal').modal();
            $.ajax({
                type:'GET',
                url:'<?php echo Yii::app()->createUrl("site/marketplaceStats");?>',
                success: function(html){
                    document.getElementById('tableData').innerHTML = html;
                    document.getElementById('loader').style.display = 'none';
                    document.getElementById('tableData').style.display = 'block';
                }
            });
        }  else if (type == 'SUP') {
            document.getElementById('loader').style.display = 'block';
            document.getElementById('tableData').style.display = 'none';
            $('#statsModal').modal();
            setTimeout(function () {
                var html =
                    "<table class='table table-responsive table-hover'>" +
                        "<tbody>" +
                            "<tr>" +
                                "<th style='width: 55%'>From </th>" +
                                "<th style='width: 45%'>Contact No</th>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Operation No</td>" +
                                "<td>01XXXXXXXXX</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Extension</td>" +
                                "<td>XXXX</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Pick and drop</td>" +
                                "<td>01XXXXXXXXX</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>Billing</td>" +
                                "<td>01XXXXXXXXX</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>";
                document.getElementById('tableData').innerHTML = html;
                document.getElementById('loader').style.display = 'none';
                document.getElementById('tableData').style.display = 'block';
            }, 500);
        }
    };
    var toggleDriverDetail = function (routeId){
        var x = document.getElementById("routeList");
        if (x.style.display === "none") {
            x.style.display = "block";
            document.getElementById("driverInfo").style.display = "none";
        } else {
            x.style.display = "none";
            document.getElementById("driverInfo").style.display = "block";

            document.getElementById('loader2').style.display = 'block';
            document.getElementById('driverDetail').style.display = 'none';
            $.ajax({
                type:'GET',
                url:'<?php echo Yii::app()->createUrl("site/getDriverDetail");?>',
                data:{routeId: routeId},
                dataType:'html',
                success: function(html){
                    document.getElementById('driverDetail').innerHTML = html;
                    document.getElementById('loader2').style.display = 'none';
                    document.getElementById('driverDetail').style.display = 'block';
                }
            });
        }
    };

    var toggleStoppageList = function (routeId){
        var x = document.getElementById("routeList");
        if (x.style.display === "none") {
            x.style.display = "block";
            document.getElementById("driverInfo").style.display = "none";
        } else {
            x.style.display = "none";
            document.getElementById("driverInfo").style.display = "block";

            document.getElementById('loader2').style.display = 'block';
            document.getElementById('driverDetail').style.display = 'none';

            $.ajax({
                type:'GET',
                url:'<?php echo Yii::app()->createUrl("site/getStoppageList");?>',
                data:{routeId: routeId},
                dataType:'html',
                success: function(html){
                    document.getElementById('driverDetail').innerHTML = html;
                    document.getElementById('loader2').style.display = 'none';
                    document.getElementById('driverDetail').style.display = 'block';
                }
            });
        }
    };
    function openMarketplaceWindow(){
        window.open('http://localhost/nexfleet/marketplace/index.php/marketplace/bookUserSeat', '_blank');
    }
</script>