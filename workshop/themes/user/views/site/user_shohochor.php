<style>
    .box-body {
        padding-left: 20px;
        text-align: center;
        padding-top: 20px
    }
</style>

<section class="book-seat-responsive" >

    <div class="<?php echo $r=(Yii::app()->user->picmarket_user=='1') ? 'col_half' : 'col_full';?>" style="background: #fff;padding: 20px; box-shadow: 0 4px 25px 0 rgba(0,0,0,.1);border-radius: 5px;min-height: 430px;">


        <h5>BOOK SEAT</h5>
        <?php  $this->renderPartial('/seatBooking/_routeSearch',array(
                'model' => $model,
                'route_list' => $route_list
            ));  ?>
        

    </div>

    <?php  if( Yii::app()->user->picmarket_user=='1') { ?>
    <div class="col_half col_last " style="background: #fff;padding: 20px; box-shadow: 0 4px 25px 0 rgba(0,0,0,.1);border-radius: 5px;min-height: 430px;">

        <h5>SELL SEAT</h5>
        <?php
        if($use_route){
            $this->renderPartial('/seatSale/seat_sale_form',array(
                'model'=>$model,
                'use_route'=>$use_route,            
            ));             
        }
        else {
        ?>
        <div class="center s002 page_header_div">        
            <h4 class="heading-custom page_header_h4">Sorry, you do not have approved route.</h4>
        </div>
        <?php } ?>
    </div>
    
     <?php } ?> 
</section>
<style>
    .fs-12{
        font-size: 12px;
    }
</style>
<div style="clear: both">
<div id="route-request-list" class="custom-box-design booking-history responsive-booking-history">
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">

        <h4 class="heading-custom page_header_h4">Last Five Booking History</h4>
    </div>
    <div class="table-responsive bottommargin responsive-sell-booking-table responsive-booking-history-table border-round-bottom">

        <table class="table cart">
            <thead>
            <tr>
                <th class="cart-product-price">Booking Date</th>
               
                <th class="cart-product-price">Route</th>
                <th class="cart-product-price">Date</th>             
                <th class="cart-product-price">Travel Direction</th>
                <th class="cart-product-price">Price</th>
                <th class="cart-product-price">Transfer to</th>
                <th class="cart-product-price">Cancel</th>
                <th class="cart-product-price">Transfer</th>
               
            </tr>
            </thead>
            <tbody>
            <?php if (count($booking) > 0): ?>
                <?php foreach ($booking as $index => $booking): ?>
                    <tr class="cart_item">
                        <td class="cart-product-price">
                            <?php
                            //$created_time = date_create($booking->created_time);
                            //echo date_format($created_time,"d/m/Y h:i A");
                            echo $booking['created_time']; ?>
                        </td>
                        <td class="cart-product-price"><?php echo $booking['route_no']; ?></td>
                        
                        <td class="cart-product-price"><?php echo $booking['date_from']; ?></td>                     
                        <td class="cart-product-price"><?php echo ($booking['direction_travale']==1) ? "HO IN" : "HO OUT"; ?></td></td>
                        <td class="cart-product-price"><?php echo $booking['price']; ?></td>
                        <td class="cart-product-price"><?php echo $booking['transfer_user']; ?></td>
                        <td class="cart-product-price">
                        <?php if($booking['status']=='-1') { ?>
                            Cancel
                        <?php } else { ?>
                        <input class="btn-search button-pink" id="search-button" onclick="bookingCancel(<?php echo $booking['id']; ?>)" name="" type="submit" value="Cancel">
                        <?php } ?>
                        </td>
                       <td class="cart-product-price">
                        <?php if($booking['status']=='-1') { ?>
                         
                        <?php } else { ?>
                         <input class="btn-search button-pink" id="search-button" onclick="bookingTransfer(<?php echo $booking['id']; ?>)" name="" type="submit" value="Transfer">
                        <?php } ?>
                           
                        </td>
                       
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7" class="empty"><span class="empty">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        
    </div>
</div>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {defaultDate: "today",minDate: "today"});</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
    {
        searchEnabled:true,
        itemSelectText: '',
    });

</script>
<script type="text/javascript">
    function bookingCancel(id) {
         $.post('<?php echo Yii::app()->createAbsoluteUrl("seatBooking/seatCancel");?>',{id:id},function(data){
                bsModalOpen(data);
                //alert(data);
            });
    }
    function bookingTransfer(id) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("seatBooking/seatTransfer");?>',{id:id},function(data){
                bsModalOpen(data);
                //alert(data);
            });
        
    }
     function userInfo(id){  
      var v=$("#user-pin").val();   
       $.post('<?php echo Yii::app()->createAbsoluteUrl("seatBooking/seatTransfer");?>',{id:id,user_pin:v},function(data){
                bsModalOpen(data);
                //alert(data);
        });
    }
    function transferOther(){        
        $("#transfer-other").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/seatBooking/transferOther");?>', 
            type: 'post', 
            success:  function(data) {                     
                    bsModalOpen(data);                       
            }
        });
    
    }

</script>
