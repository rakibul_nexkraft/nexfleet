<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Last 6-Month Rating', 'IFleet', 'Driver', 'Vehicle'],
            ['January', 5, 4, 2],
            ['December', 4, 3, 4],
            ['November', 2, 3, 5],
            ['October', 4, 3, 1],
            ['September', 2, 5, 3],
            ['Auguest', 3, 4, 5],

        ]);

        var cssClassNames = {
        };
        var options = {
            pieHole: 0.4,
            allowHtml: true,
            cssClassNames : cssClassNames,
            height:400, // example height, to make the demo charts equal size
            width:450,
            bar: {groupWidth: "40" },
            colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
            backgroundColor: 'transparent',
            datalessRegionColor: '#37474f',
            legend: {alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '14'},},
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
    }
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Requisitions',     80],
            ['Approved',      78],
            ['Pendings',  2]
        ]);

        var cssClassNames = {
        };
        var options = {
            pieHole: 0.4,
            allowHtml: true,
            cssClassNames : cssClassNames,
            height:400, // example height, to make the demo charts equal size
            width:500,
            bar: {groupWidth: "40" },
            colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
            backgroundColor: 'transparent',
            datalessRegionColor: '#37474f',
            legend: {alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '14'},},
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>
<div class="col_full custom-box-design" >
    <h3 class=" heading-block center" style="padding-top: 15px;">iFleet, Driver and Vehicle Rating</h3>
    <div class="col_half userpickAndDrop-chart-container" style="padding-left: 10px">
        <div id="columnchart_material"></div>
    </div>
    <div class="col_half col_last userpickAndDrop-chart-container">
        <div id="piechart" ></div>
    </div>
</div>
<div class="clear"></div>
</div>
<div class="col_full custom-box-design" >
    <div class="heading-block  bottommargin-sm center history-header box-header-text">
        <h3 style="padding-top: 15px">Your Route Recent SMS OR Email</h3>
    </div>
    <div class="col_full" style="padding: 20px 20px 0;">
        <table class="table table-approved-req  table-responsive table-hover">
            <thead class="tbl-header">
            <tr>
                <th>Subject</th>
                <th>Body</th>
                <th>Type</th>
                <th>Date & Time</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //var_dump($u_req_app_five);
            if (count($sms_email) <= 0){
                echo
                    '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                    '<tr>'.
                    '</tr>';
            }
            foreach ($sms_email as $value){
                echo
                    '<tr>'.
                    '<td>'.$value['sub'].'</td>'.
                    '<td>'.$value['body'].'</td>'.
                    '<td>'.$value['type'].'</td>'.
                    '<td>'.$value['created_time'].'</td>'.
                    '</tr>';
            } ?>
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
</div>

  