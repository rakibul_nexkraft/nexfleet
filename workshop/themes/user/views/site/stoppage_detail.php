<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/26/2019
 * Time: 7:42 PM
 */?>
<div class="col_full">
    <button class="btn btn-primary" onclick="toggleStoppageList()">Back to List</button>
</div>
<div class="clear"></div>
<?php
if (count($stoppages) > 0)
    echo "<h4>Stoppages</h4>";
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>new CArrayDataProvider($stoppages),
    'columns'=>array(
        array(
            'name'=>'stoppage',
            'value'=>'$data["stoppage"]',
            'header'=>'Stoppage Name',
            'type'=>'raw'
        ),
        array(
            'name'=>'pickup_time',
            'value'=>'$data["pickup_time"]',
            'header'=>'Pickup Time',
            'type'=>'raw'
        ),
    ),
    'summaryText'=>'',
    'itemsCssClass' => 'table',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),
));
?>
