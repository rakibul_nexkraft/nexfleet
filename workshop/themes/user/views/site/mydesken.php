<?php
public function decrypt_data($data, $iv, $key) {

        $cypher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');



        if (is_null($iv)) {

            $ivlen = mcrypt_enc_get_iv_size($cypher);

            $iv = substr($data, 0, $ivlen);

            $data = substr($data, $ivlen);

        }



        // initialize encryption handle

        if (mcrypt_generic_init($cypher, $key, $iv) != -1) {

            // decrypt

            $decrypted = mdecrypt_generic($cypher, $data);



            // clean up

            mcrypt_generic_deinit($cypher);

            mcrypt_module_close($cypher);



            return $decrypted;

        }



        return false;

    }

private function ssoSessionExists($key, $data) {

        $cleanData = preg_replace('/[\x00-\x1F\x7F]/', '', $data);

        $cleanData = str_replace($key, '', $cleanData);

        if ($cleanData == 'session:false')

            return false;

        else

            return true;

    }



/**

     * Displays the login page

     */

    public function actionLogin() {



        $model = new UserLogin;

         



        $multipass = Yii::app()->request->getParam('multipass');

        $ssoSessionCheckUrl = Yii::app()->params['sso']['ssoSessionUrl'] . '?site=' . Yii::app()->params['sso']['appUrl'];



        if (!isset($multipass)) {

            if (Yii::app()->user->isGuest) {

                Yii::app()->request->redirect($ssoSessionCheckUrl);

            }

        } else {

            $key = Yii::app()->params['sso']['appKey'];

            $ssoSubject = base64_decode($multipass);

            $ssoSubjectDecode = $this->decrypt_data($ssoSubject, $key, $key);



            if ($this->ssoSessionExists($key, $ssoSubjectDecode)) {

                $ssoInfo = explode("|", $ssoSubjectDecode);

                $username = explode(":", $ssoInfo[0]); 

              

                $auth = explode(":", $ssoInfo[3]); 

                if ($auth[1] == "true") {

                    if ($this->getSetApplicationUser($username[1])) {                    

                        $pass = User::model()->find(array('condition' => 'username=:u', 'params' => array(':u' => $username[1])))->password;

                        $identity = new UserIdentity($username[1], $pass);

                        $identity->authenticate();

                        if ($identity->errorCode === UserIdentity::ERROR_NONE) {

                            Yii::app()->user->login($identity, 0);

                            $this->redirect(Yii::app()->user->returnUrl);

                        } else {

                            echo $identity->errorCode;

                        }

                    }

                }

            }

        }



      

        $this->render('/user/login', array('model' => $model));

         Yii::app()->end();

    }

?>
