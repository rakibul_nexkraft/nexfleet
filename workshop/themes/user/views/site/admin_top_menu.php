<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>AOS.init();</script>


<style type="text/css">
    #all_route{
        display: none;
    }
    .cell-hover:hover{
        background: rgba(53,217,125,0.1);
    }
    .history-box{
        position: relative;
        border: 1px solid rgba(0,0,0,0.075);
        border-radius: 3px;
        text-align: center;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        background-color: #F5F5F5;
    }
    .history-extended{
        background-color: #FFF;
        text-align: left;
    }
    .history-header{
        /* border-bottom: 1px solid #dedede; */
        padding: 10px 20px;

        /*background-color: #2a57ff;*/
        /*background-image: linear-gradient(45deg, #fc3ba2, blue);*/
        /*box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);*/
        /* width: 95%; */
        font-family: Poppins,Helvetica,sans-serif;

        font-size: 1.2em;
        font-weight: 700;
        border-radius: 5px;
        margin: 0 auto;
        position: relative;
        top: 54px;
        color: black;
        z-index: 1;
    }
    .history-header > h3{
        color: black !important;
    }
    .tbl-header{
        /*box-shadow: 0 3px 2px -2px rgba(0,0,0,.3); */
        /* border-radius: 5px; */
        /* border: 1px solid #dedede; */
        background-color: #f2f2f2;
    }
    .heading-block:after {
        display: none;
    }
</style>
<?php 
    $count_request = Yii::app()->db->createCommand("SELECT count(id) as request FROM tbl_seat_request WHERE  action=0")->queryRow();
    $count_maternity_leave = Yii::app()->db->createCommand("SELECT count(m.id) as request FROM tbl_maternity_leave_request as m INNER JOIN tbl_seat_request as s ON m.user_pin=s.user_pin WHERE m.status=1  AND s.status=1 AND s.queue=-1 AND m.from_date<='".date("Y-m-d")."' AND m.to_date>='".date("Y-m-d")."'")->queryRow();
    $booking=Yii::app()->db->createCommand("SELECT count(id) as booking FROM tbl_seat_booking WHERE date_from='".date("Y-m-d")."' AND status=0")->queryRow();
    $selling=Yii::app()->db->createCommand("SELECT count(id) as sale FROM tbl_seat_sale WHERE from_date='".date("Y-m-d")."'")->queryRow();
  $valid_routes = Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id INNER JOIN tbl_stoppage as s ON s.route_id=r.id GROUP BY r.id")->queryAll();
  $seats_av=0;
   foreach ($valid_routes as $key => $value) {
        //    $seats_oc=Routes::borderPass($value['route_no']); 
           $seats_oc=0; 
           $seats_av+= $value['actual_seat']-$seats_oc;
    }
    if($selling){$seats_av+=$selling['sale'];}
 ?>

<section id="page-title" class="page-title-center page-title-nobg noborder nopadding admin-dashboard-responsive">


    <div class="col_one_fourth box-border box-border--item-color-one border-custom pick_drop_summery cursor-pointer box-border-responsive" data-aos="fade-down-right" data-aos-duration="1000">
        <div style="background: #97bb9e;" class="col_one_fourth left-badge"></div>
        <div class="box-header-label" ><a href="<?php echo Yii::app()->createAbsoluteUrl("/seatRequestAdmin/index");?>">Route  Request</div>

        <a href="<?php echo Yii::app()->createAbsoluteUrl("/seatRequestAdmin/index?SeatRequestAdmin%5Bstatus%5D=0");?>">
            <div class="box-bottom-number"><?php echo $count_request['request']; ?></div>
        </a>
    </div>


    <div class="col_one_fourth box-border box-border--item-color-tow border-custom pick_drop_summery cursor-pointer box-border-responsive" data-aos="fade-down-left" data-aos-duration="1000">
        <div style="background: #7e57c1;" class="col_one_fourth left-badge"></div>
        <div class="box-header-label">Today Open Seats</div>

       
            <a href="<?php echo Yii::app()->createAbsoluteUrl("/seatSale/seatOpen");?>">
                <div class="box-bottom-number">
            <?php echo $seats_av; ?></div>
        </a>
    </div>


    <div class="col_one_fourth box-border box-border--item-color-three border-custom pick_drop_summery cursor-pointer box-border-responsive"  data-aos="fade-down-right" data-aos-duration="1000">
        <div style="background: #84b269;" class="col_one_fourth left-badge"></div>
        <div class="box-header-label">Today Booking</div>

        <a href="<?php echo Yii::app()->createAbsoluteUrl("/seatBooking/seatToday");?>">
            <div class="box-bottom-number"><?php echo  $booking['booking']; ?></div>
        </a>
    </div>


    <div class="col_one_fourth box-border box-border--item-color-four border-custom pick_drop_summery cursor-pointer box-border-responsive" data-aos="fade-down-left" data-aos-duration="1000" >
        <div style="background: #c5bf89;" class="col_one_fourth left-badge"></div>
        <div class="box-header-label">Maternity Leave</div>

        <a href='<?php echo Yii::app()->createAbsoluteUrl("/maternityLeaveRequestAdmin/index");?>'>
        	<div class="box-bottom-number"><?php echo $count_maternity_leave['request']; ?></div>
    	</a>
    </div>
    <div class="clear"></div>
</section>


