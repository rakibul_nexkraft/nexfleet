<?php

/**
 * This is the model class for table "{{seat_request}}".
 *
 * The followings are the available columns in table '{{seat_request}}':
 * @property integer $id
 * @property string $user_pin
 * @property string $user_name
 * @property string $user_department
 * @property integer $route_id
 * @property string $expected_date
 * @property string $remarks
 * @property integer $status
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class SeatRequest extends CActiveRecord
{	public $zone;
	public $from;
	public $to;
	public $vehicle_type_id;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SeatRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seat_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, user_name, user_department,user_level, route_id, expected_date, created_by', 'required'),
			array('route_id, status, zone,stoppage_id,queue,vehicle_type_id,maternity_leave', 'numerical', 'integerOnly'=>true),
			array('user_pin, user_name, user_department, created_by, updated_by,user_cell,email, user_designation,residence_address', 'length', 'max'=>128),
			array('remarks,from,to', 'length', 'max'=>256),
			array('created_time, updated_time,zone,user_designation,residence_address,stoppage_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, user_name, user_department, route_id, expected_date, remarks, status, created_by, created_time, updated_by, updated_time,zone,user_designation,residence_address,stoppage_id,queue,from,to,vehicle_type_id,maternity_leave', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_pin' => 'User Pin',
			'user_name' => 'User Name',
			'user_department' => 'User Department',
			'user_level'=>'user_level',
			'user_cell'=>'User Cell',
			'email'=>'Email',
			'route_id' => 'Route',
			'expected_date' => 'Expected Date',
			'remarks' => 'Remarks',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'zone'=>'Zone',
			'user_designation'=>'User Designation',
			'residence_address'=>'Residence Address',
			'stoppage_id'=>'Stoppage',
			'queue'=>'Queue',
			'from'=>"From",
			'to'=>'To',
			'vehicle_type_id'=>'Vehicle Type Id',
			'maternity_leave'=>'Apply Maternity Use '
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		//if(isset($_GET['user_pin']))
		$this->user_pin=Yii::app()->user->username;

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_department',$this->user_department,true);
		$criteria->compare('user_level',$this->user_level,true);
		$criteria->compare('user_cell',$this->user_cell,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('expected_date',$this->expected_date,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('maternity_leave',$this->maternity_leave,true);
		
		$criteria->order="t.id DESC";
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
	        	'pageSize'=>40,
	    	),
		));
	}

	//public function routeDetail($id){
		//$detail=Routes::model()->findByPk($id);
		//return $detail['route_detail'];
	//}
	public function stoppageDetail($id){
		$detail=Stoppage::model()->findByPk($id);
		return $detail['stoppage'];
	}
	public function stoppagePickupTime($id){
		$detail=Stoppage::model()->findByPk($id);
		return $detail['pickup_time'];
	}
	public function getAllVehicle() {
        $data         = array();
        $packageModel = Vehicles::model()->findAll();
        foreach($packageModel as $get){
            $data[$get->reg_no] = $get->reg_no;
        }
        return $data;
    }
    public function routeDetail($id,$func,$stopage_id=null){		
		if($func==1){ 
			$detail=Routes::model()->findByPk($id);
			return $detail['route_no'];
		}
		else if($func==2){
			$stoppage_list_sql="SELECT * FROM tbl_stoppage WHERE route_id='$id'";
			$stoppage_list_rs=Yii::app()->db->createCommand($stoppage_list_sql)->queryAll();
			if(count($stoppage_list_rs)>0){
				$stoppage_list='';
				foreach ($stoppage_list_rs as $key => $value) {
					if($stopage_id==$value['id']){
						$stoppage_list.="<span style='color:#47ae4b'>".$value['stoppage']."</span>";
					}
					else{
						$stoppage_list.=$value['stoppage'];
					}
					if(count($stoppage_list_rs)>$key+1){$stoppage_list.="-";}
				}
				return $stoppage_list;
			}
			return "No Stoppage";
		}
		else if($func==3){
			$detail=Routes::model()->findByPk($id);
			$stoppage_list_sql="SELECT * FROM tbl_stoppage WHERE route_id='$id'";
			$stoppage_list_rs=Yii::app()->db->createCommand($stoppage_list_sql)->queryAll();
			if(count($stoppage_list_rs)>0){
				$stoppage_list='';
				foreach ($stoppage_list_rs as $key => $value) {
					if($stopage_id==$value['id']){
						$stoppage_list.="<span style='color:#47ae4b'>".$value['stoppage']."</span>";
					}
					else{
						$stoppage_list.=$value['stoppage'];
					}
					if(count($stoppage_list_rs)>$key+1){$stoppage_list.="-";}
				}
				return "Route No:".$detail['route_no']." Detail: ".$stoppage_list;
			}
			return "Route No:".$detail['route_no']." Detail: No Stoppage";
			
		}

		
	}
	public function status($status){
		if($status==0) return "Pending";
		if($status==1) return "Approved";
		if($status==2) return "Maternity Leave User";
		if($status==3) return "Cancel Request";
		if($status==4) return "Route Change Request";
		if($status==-1) return "Cancel";
	}
	public function queueCheck($queue,$status){
		if($status==1 && $queue==-1){
			return "Assigned";
		}
		else if($status==1 && $queue!=0){
			return $queue;
		}
		else {
			return "";
		}

	}
	public function assignCheck($id){return $id;}
	
}