<?php

/**
 * This is the model class for table "{{maternity_leave_request}}".
 *
 * The followings are the available columns in table '{{maternity_leave_request}}':
 * @property integer $id
 * @property string $user_pin
 * @property string $from_date
 * @property string $to_date
 * @property integer $status
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_time
 * @property string $update_time
 */
class MaternityLeaveRequest extends CActiveRecord
{	public $from;
	public $to;
	public $vehicle_type_id;
	public $expected_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaternityLeaveRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{maternity_leave_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, from_date, to_date, created_by', 'required'),
			array('status,vehicle_type_id', 'numerical', 'integerOnly'=>true),
			array('user_pin, created_by, updated_by,from,to,expected_date', 'length', 'max'=>128),
			array('created_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, from_date, to_date, status, created_by, updated_by, created_time, update_time,from,to,vehicle_type_id,expected_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_pin' => 'User Pin',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
			'status' => 'Status',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_time' => 'Created Time',
			'update_time' => 'Update Time',
			'from'=>"From",
			'to'=>'To',
			'vehicle_type_id'=>'Vehicle Type Id',
			'expected_date' => 'Expected Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		//if(isset($_GET['user_pin']))
		$this->user_pin=Yii::app()->user->username;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('from_date',$this->from_date,true);
		$criteria->compare('to_date',$this->to_date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function status($status){
		if($status==0)return "Pending";
		if($status==1)return "Approved";
		if($status==3)return "Cancel Request";
		if($status==-1)return "Cancel";
	}
}