<?php

/**
 * This is the model class for table "{{maternity_leave_request}}".
 *
 * The followings are the available columns in table '{{maternity_leave_request}}':
 * @property integer $id
 * @property string $user_pin
 * @property string $from_date
 * @property string $to_date
 * @property integer $status
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_time
 * @property string $update_time
 */
class MaternityLeaveRequestAdmin extends CActiveRecord
{	
	public $user_name;
	public $user_department;
	public $user_level;
	public $user_cell;
	public $user_email;
	public $route;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaternityLeaveRequestAdmin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{maternity_leave_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, from_date, to_date, created_by', 'required'),
			array('status,user_level', 'numerical', 'integerOnly'=>true),
			array('user_pin, created_by, updated_by,user_name,user_department,user_cell,user_email,route', 'length', 'max'=>128),
			array('created_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, from_date, to_date, status, created_by, updated_by, created_time, update_time,user_name,user_department,user_level,user_cell,user_email,route', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_pin' => 'User Pin',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
			'status' => 'Status',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_time' => 'Created Time',
			'update_time' => 'Update Time',
			'user_name'=>'User Name',
			'user_department'=>'User Department',
			'user_level'=>'user_level',
			'user_cell'=>'User Cell',
			'user_email'=>'User Email',
			'route'=>'Route'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.user_pin',$this->user_pin,true);
		$criteria->compare('t.from_date',$this->from_date,true);
		$criteria->compare('t.to_date',$this->to_date,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.updated_by',$this->updated_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.update_time',$this->update_time,true);
		$criteria->compare('rt .user_name',$this->user_name,true);
		$criteria->compare('rt .user_dept',$this->user_department,true);
		$criteria->compare('rt .user_level',$this->user_level,true);
		$criteria->compare('rt .user_cell',$this->user_cell,true);
		$criteria->compare('rt .user_email',$this->user_email,true);
		$criteria->compare('rt.present_route',$this->route,true);
		$criteria->join="INNER JOIN tbl_commonfleets as rt ON t.user_pin=rt.user_pin";
		$criteria->order="t.id DESC";
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
	        	'pageSize'=>40,
	    	),
		));
	}
	public function userName($user_pin){
		$sql="SELECT * FROM tbl_seat_request WHERE user_pin='".$user_pin."' order by created_time Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['user_name'];

	}
	public function userDepartment($user_pin){
		$sql="SELECT * FROM tbl_seat_request WHERE user_pin='".$user_pin."' order by created_time Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['user_department'];

	}
	public function userLevel($user_pin){
		$sql="SELECT * FROM tbl_seat_request WHERE user_pin='".$user_pin."' order by created_time Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['user_level'];

	}
	public function userCell($user_pin){
		$sql="SELECT * FROM tbl_seat_request WHERE user_pin='".$user_pin."' order by created_time Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['user_cell'];

	}
	public function userEmail($user_pin){
		$sql="SELECT * FROM tbl_seat_request WHERE user_pin='".$user_pin."' order by created_time Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['email'];

	}
	public function userRoute($user_pin){
		$sql="SELECT * FROM tbl_commonfleets WHERE user_pin='".$user_pin."' Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['present_route'];

	}
	public function userRouteID($user_pin){
		$sql="SELECT * FROM tbl_commonfleets WHERE user_pin='".$user_pin."' Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		$sql="SELECT * FROM tbl_routes WHERE route_no='".$sql_rs['present_route']."' Limit 1";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['id'];

	}
	public function assignUser($id){
		$maternity_leave=MaternityLeaveRequest::model()->findByPk($id);
		if(!empty($maternity_leave['maternity_user_pin'])){			
			$usrpin=$maternity_leave['maternity_user_pin'];
			return CHtml::link($usrpin,array('maternityLeaveRequestAdmin/mrUser','id'=>$id));
		}
		else{
			$sql="SELECT * FROM tbl_maternity_leave_assign WHERE meternity_request_id=$id";
			$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
			return CHtml::link(count($sql_rs),array('maternityLeaveRequestAdmin/mrUser','id'=>$id));

		}
	} 

}