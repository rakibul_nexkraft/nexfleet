<?php

/**
 * This is the model class for table "{{commonfleets}}".
 *
 * The followings are the available columns in table '{{commonfleets}}':
 * @property integer $id
 * @property integer $user_pin
 * @property string $user_name
 * @property integer $user_level
 * @property string $user_dept
 * @property string $user_designation
 * @property integer $user_cell
 * @property string $user_email
 * @property integer $telephone_ext
 * @property string $application_type
 * @property string $residence_address
 * @property string $recommended_by
 * @property string $recommended_by_desig
 * @property string $previous_route
 * @property string $preferred_route
 * @property string $present_route
 * @property string $expected_date
 * @property string $mt_leave_from
 * @property string $mt_leave_to
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property string $user_remarks
 * @property string $fleet_remarks
 * @property string $created_by
 * @property string $created_time
 * @property string $approve_status
 * @property integer $active
 */
class Commonfleets extends CActiveRecord
{
	public $from_date;
	public $to_date;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Commonfleets the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{commonfleets}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name, user_dept, application_type', 'required'),
			array('user_pin', 'unique'),
			array('user_level, user_cell, telephone_ext, vehicletype_id, active', 'numerical', 'integerOnly'=>true),
            array('user_email','email'),
			array('user_name, application_type, recommended_by', 'length', 'max'=>40),
			array('previous_route, preferred_route, present_route, user_cell', 'length', 'max'=>11),
			array('residence_address, user_remarks, fleet_remarks, user_dept, user_designation, user_email, recommended_by_desig, approve_status', 'length', 'max'=>128),
			array('vehicle_reg_no, created_by, updated_by, user_pin, pick_up_time', 'length', 'max'=>127),
			array('stoppage_point','length','max'=>300),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('updated_time, mt_leave_from, mt_leave_to, user_designation, user_cell, telephone_ext, application_type, residence_address, preferred_route, expected_date','safe'),
			array('id, user_pin, user_name, user_level, user_dept, user_designation, user_cell, user_email, telephone_ext, application_type, residence_address, recommended_by, recommended_by_desig, preferred_route, present_route, expected_date, mt_leave_from, mt_leave_to, vehicle_reg_no, vehicletype_id, user_remarks, fleet_remarks, created_by, created_time, updated_by, updated_time, approve_status, active, from_date, to_date, stoppage_point, pick_up_time', 'safe', 'on'=>'search'),
            array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Serial No.',
			'user_pin' => 'User PIN',
			'user_name' => 'User Name',
			'user_level' => 'Level',
			'user_dept' => 'Department',
			'user_designation' => 'Designation',
			'user_cell' => 'Cell Number',
			'user_email' => 'Email ID',
			'telephone_ext' => 'Telephone Ext',
			'application_type' => 'Application Type',
			'residence_address' => 'Residence Address',
			'recommended_by' => 'Recommended By',
			'recommended_by_desig' => 'Recommended Designation',
            'previous_route' => 'Previous Route',
			'preferred_route' => 'Preferred Route',
			'present_route' => 'Present Route',
			'expected_date' => 'Expected Date',
			'mt_leave_from' => 'Maternity Leave From',
			'mt_leave_to' => 'Maternity Leave To',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicle Type',
			'user_remarks' => 'User Remarks',
			'fleet_remarks' => 'Remarks of Transport Department',
			'created_by' => 'Created By',
			'created_time' => 'Creation Time',
			'updated_by' => 'Status Changed By',
			'updated_time' => 'Status Change Time',
			'approve_status' => 'Approve Status',
			'active' => 'Active',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
            'stoppage_point' => 'Stoppage Point',
            'pick_up_time' => 'Pick Up Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_pin',$this->user_pin);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_level',$this->user_level);
		$criteria->compare('user_dept',$this->user_dept,true);
		$criteria->compare('user_designation',$this->user_designation,true);
		$criteria->compare('user_cell',$this->user_cell);
		$criteria->compare('user_email',$this->user_email,true);
		$criteria->compare('telephone_ext',$this->telephone_ext);
		$criteria->compare('application_type',$this->application_type,true);
		$criteria->compare('residence_address',$this->residence_address,true);
		$criteria->compare('recommended_by',$this->recommended_by,true);
		$criteria->compare('recommended_by_desig',$this->recommended_by_desig,true);
		$criteria->compare('preferred_route',$this->preferred_route,false);
		$criteria->compare('present_route',$this->present_route,false);
		$criteria->compare('expected_date',$this->expected_date,true);
		$criteria->compare('mt_leave_from',$this->mt_leave_from,true);
		$criteria->compare('mt_leave_to',$this->mt_leave_to,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('user_remarks',$this->user_remarks,true);
		$criteria->compare('fleet_remarks',$this->fleet_remarks,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('approve_status',$this->approve_status,true);
		$criteria->compare('active',$this->active);
		
		if(!empty($this->from_date) && empty($this->to_date))
		{
			$criteria->addCondition('DATE(updated_time) = "'.$this->from_date.'"');
		}
		elseif(!empty($this->to_date) && empty($this->from_date))
		{
			$criteria->addCondition('DATE(updated_time) <= "'.$this->to_date.'"');
		}
		elseif(!empty($this->to_date) && !empty($this->from_date))
		{
			//$criteria->condition = "updated_time  >= '$this->from_date' and updated_time <= '$this->to_date'";
			$criteria->addCondition('DATE(updated_time) BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
		}

        //$criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
				
			),
		));
	}

    public function getMaternityLeaveCount($route_no){
        $sql = "SELECT COUNT(application_type) FROM tbl_commonfleets WHERE present_route = '$route_no' AND application_type = 'Maternity Leave'";
        $result = Yii::app()->db->createCommand($sql)->queryAll();
        $total = $result[0]['COUNT(application_type)'];
        return $total;
    }
}