<?php

/**
 * This is the model class for table "{{seat_request}}".
 *
 * The followings are the available columns in table '{{seat_request}}':
 * @property integer $id
 * @property string $user_pin
 * @property string $user_name
 * @property string $user_department
 * @property integer $user_level
 * @property string $user_cell
 * @property string $email
 * @property integer $route_id
 * @property string $expected_date
 * @property string $remarks
 * @property integer $status
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class SeatRequestAdmin extends CActiveRecord
{	public $zone;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SeatRequestAdmin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seat_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, user_name, user_department, user_level, route_id, expected_date, created_by', 'required'),
			array('user_level, zone, route_id, status,queue,stoppage_id,maternity_leave', 'numerical', 'integerOnly'=>true),
			array('user_pin, user_name, user_department, user_cell, email, created_by, updated_by, 	user_designation,residence_address', 'length', 'max'=>128),
			array('remarks', 'length', 'max'=>256),
			array('created_time, updated_time,zone, user_designation,residence_address,stoppage_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, user_name, user_department, user_level, user_cell, email, route_id, expected_date, remarks, status, created_by, created_time, updated_by, updated_time,zone,queue,	user_designation,residence_address,stoppage_id,maternity_leave', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_pin' => 'Pin',
			'user_name' => 'Name',
			'user_department' => 'Department',
			'user_level' => 'Level',
			'user_cell' => 'Cell',
			'email' => 'Email',
			'route_id' => 'Route Detail',
			'expected_date' => 'Expected Date',
			'remarks' => 'Remarks',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'zone'=>'Zone',
			'queue'=>'Queue',
			'user_designation'=>'User Designation',
			'residence_address'=>'Residence Address',
			'stoppage_id'=>'Stoppage',
			'maternity_leave'=>'Maternity seat use'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_department',$this->user_department,true);
		$criteria->compare('user_level',$this->user_level);
		$criteria->compare('user_cell',$this->user_cell,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('expected_date',$this->expected_date,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('queue',$this->queue);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('maternity_leave',$this->maternity_leave,true);
		//$criteria->order="t.id DESC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
	        	'pageSize'=>40,
	    	),
	    	'sort'=>array('defaultOrder'=>'t.id DESC')
		));
	}
	public function routeDetail($id){
		$detail=Routes::model()->findByPk($id);
		return "Route Number: ".$detail['route_no'].", Route: ".$detail['route_detail'];
	}
	public function status($status){
		if($status==0) return "Pending";
		if($status==1) return "Approved";
		if($status==2) return "Queue";
		if($status==3) return "Cancel Request";
		if($status==-1) return "Cancel";
	}
	public function queueCheck($queue,$status){
		if($status==1 && $queue==-1){
			return "Assigned";
		}
		else if($status==1 && $queue!=0){
			return $queue;
		}
		else {
			return "";
		}

	}
	public function assignCheck($id){return $id;}
}