<?php
/* @var $this SeatRequestAdminController */
/* @var $model SeatRequestAdmin */

$this->breadcrumbs=array(
	'Seat Request Admins'=>array('index'),
	$model->id,
);

//$this->menu=array(
	//array('label'=>'List Seat Request Admin', 'url'=>array('index')),
	//array('label'=>'Create Seat Request Admin', 'url'=>array('create')),
	//array('label'=>'Update Seat Request Admin', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Seat Request Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                      array('label'=>'Seat Request List', 'url'=>array('/seatRequestAdmin/index')),
                      array('label'=>'Update', 'url'=>array('/seatRequestAdmin/update','id'=>$model->id)), 
                      array('label'=>'New Seat Request', 'url'=>array('/seatRequestAdmin/create')),
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">View Seat Request Admin ID#<?php echo $model->id; ?></h4>
        </div>    
    </div>


<div class="s002">
  <div class="table-responsive bottommargin">
		<?php 
			$this->widget('zii.widgets.XDetailView', array(
			'htmlOptions' => array('class' => 'table cart1'), 
			'data'=>$model,
			'attributes'=>array(
				'group2'=>array(
					'ItemColumns' => 2,
                    'attributes' => array(
					'id',
					'user_pin',
					'user_name',
					'user_department',
					'user_level',
					'user_cell',
					'email',
					'route_id',
					//array('label'=>'Route Detail',
			            //'type'=>'raw',
						//'value'=>$model->routeDetail($model->route_id),
					//),
					array('label'=>'Stoppage',
			            'type'=>'raw',
						'value'=>SeatRequest::model()->stoppageDetail($model->stoppage_id),
					),
					array('label'=>'Pickup Time',
			            'type'=>'raw',
						'value'=>SeatRequest::model()->stoppagePickupTime($model->stoppage_id),
					),
					'expected_date',
					'remarks',
					array('name'=>'status',
			            'type'=>'raw',
						'value'=>$model->status($model->status),
					),
					'created_by',
					'created_time',
					'updated_by',
					'updated_time',
				),
				),
			),
		)); ?>
	</div>
</div>
