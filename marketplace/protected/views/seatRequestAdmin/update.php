<?php
/* @var $this SeatRequestAdminController */
/* @var $model SeatRequestAdmin */

$this->breadcrumbs=array(
	'Seat Request Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

//$this->menu=array(
	//array('label'=>'List Seat Request Admin', 'url'=>array('index')),
	//array('label'=>'Create Seat Request Admin', 'url'=>array('create')),
	//array('label'=>'View Seat Request Admin', 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
<div class="center s002 ">  
	<div class="col_one_sixth">	
	 <div class="toggle toggle-border">  
	 	<div class="togglet">Menu</div>
		<div class="togglec" style="display: none;">
			<?php 
			$this->widget('zii.widgets.CMenu', array(
    		'items'=>array(
                array('label'=>'Seat Request List', 'url'=>array('/seatRequestAdmin/index')),
                array('label'=>'View', 'url'=>array('/seatRequestAdmin/view','id'=>$model->id)), 
                array('label'=>'New Seat Request', 'url'=>array('/seatRequestAdmin/create')),
                ),
			));

			?>
		</div>
	  </div>
	 </div>
	 <div class="col_five_sixth .col_last">	
	  <h4 class="heading-custom">Update Seat Request  <?php echo $model->id; ?></h4>
    </div>
	
</div>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>