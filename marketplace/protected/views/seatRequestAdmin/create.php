<?php
/* @var $this SeatRequestAdminController */
/* @var $model SeatRequestAdmin */

$this->breadcrumbs=array(
	'Seat Request Admins'=>array('index'),
	'Create',
);

//$this->menu=array(
	//array('label'=>'List Seat Request Admin', 'url'=>array('index')),
	//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
<div class="center s002 ">  
	<div class="col_one_sixth">	
	 <div class="toggle toggle-border">  
	 	<div class="togglet">Menu</div>
		<div class="togglec" style="display: none;">
			<?php 
		
			$this->widget('zii.widgets.CMenu', array(
    		'items'=>array(
                array('label'=>'Seat Request List', 'url'=>array('/seatRequestAdmin/index')),
                 
                ),
			));

			?>

			
		</div>
	  </div>
	 </div>
	 <div class="col_five_sixth .col_last">	
	  <h4 class="heading-custom">Create Seat Request (Admin)</h4>
    </div>
	
</div>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
