<?php
echo "<h3 class='center'>View Zone: $model->name</h3>";
?>
<?php 
echo "<div class = 'table-responsive bottommargin overflow-auto' style='overflow-x:auto;max-hight:400px'>";
echo $this->widget('zii.widgets.CDetailView', array(

    'htmlOptions' => array('class' => 'table cart1'),       
    
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'remark',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); 
echo "</div>";
?>