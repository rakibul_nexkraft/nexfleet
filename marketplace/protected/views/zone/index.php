<?php
/* @var $this ZoneController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Zones',
);


?>

<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                               
                        array('label'=>'Create Zone', 'url'=>array('/zone/create')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Zones</h4>
        </div>    
    </div>
    


<div class="center s002 ">
<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'enableAjaxValidation'=>true,
    'method'=>'get',
)); ?>
    <div class="inner-form">
        <div class='col_one_fourth'>
            <label>ID</label>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>                                                           
                <?php echo $form->textField($model,'id',array('placeholder'=>'ID'));
                ?>
                                                            
            </div>
        </div>
        <div class='col_one_fourth'> 
           <label>Remarks</label>
           <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>                                                           
                <?php echo $form->textField($model,'remark',array('placeholder'=>'Remark'));
                ?>
                                                            
            </div>
        </div>
   
        <div class='col_one_fourth'>
            <label>Zone</label>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>                                                           
                <?php echo $form->textField($model,'name',array('placeholder'=>'Zone'));
                ?>
                                                            
            </div>
        </div>
        <div class='col_one_fourth'>
                 <label>Search</label>
                <div class='input-field first-wrap'>
                      <?php echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button'));?>        
                </div>
      </div>
    </div> 
<?php $this->endWidget(); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'zone-grid',
    'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),       
    'rowCssClass'=>array('cart_item'),
    'dataProvider'=>$model->search(),
    'pager' => array(
    'firstPageLabel'=>'First',
    'lastPageLabel'=>'Last',
     ),
    'filter'=>$model,

    'columns'=>array(
         array(
            'name' => 'id',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),$url="javascript:void(0)",array("role"=>"button",onClick=>"zoneDetail($data->id)"))',
            'htmlOptions'=>array('class'=>'cart-product-price'),
                'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
        array(
            'name' => 'name',
            'type' => 'raw',
            'value' => '$data->name',
            'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
       
        array(
            'name' => 'remark',
            'type' => 'raw',
            'value' => '$data->remark',
            'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
        
       array(
           'class'=>'CButtonColumn',
           'template'=>'{update},{delete}',
           'buttons'=>array(
             'update'=>array(
                  'label'=>'UPDATE',
              ),
             'delete'=>array(
                  'label'=>'DELETE',                  
                'visible'=>'Yii::app()->user->username=="admin"?true:false',
              ),
            ),
           
           //'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/image/delete.png',
           //'updateButtonImageUrl'=>Yii::app()->baseUrl.'/image/update.jpg',
        ),
    ),
    
)); ?>
</div>
<div class="center s002 ">
    <?php

    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Zone'])));
?> 
</div>
<script>
    function zoneDetail(zoneId){
        $.get('<?php echo Yii::app()->createAbsoluteUrl("zone/zoneView");?>',{id:zoneId,},function(data){
                           openModel(data);
                    });
        }
   
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>