<?php
/* @var $this ZoneController */
/* @var $model Zone */

$this->breadcrumbs=array(
	'Zones'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);


?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'List Zone', 'url'=>array('/zone/index')),       
                        array('label'=>'Create Zone', 'url'=>array('/zone/create')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Update Zone ID# <?php echo $model->id;?></h4>
        </div>    
    </div>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>