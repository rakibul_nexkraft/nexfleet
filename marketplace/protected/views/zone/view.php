<?php
/* @var $this ZoneController */
/* @var $model Zone */

$this->breadcrumbs=array(
	'Zones'=>array('index'),
	$model->name,
);

/*$this->menu=array(
	array('label'=>'List Zone', 'url'=>array('index')),
	array('label'=>'Create Zone', 'url'=>array('create')),
	array('label'=>'Update Zone', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Zone', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Zone', 'url'=>array('admin')),
);*/
?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'List Zone', 'url'=>array('/zone/index')),       
                        array('label'=>'Create Zone', 'url'=>array('/zone/create')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">View Zone ID#<?php echo $model->id; ?></h4>
        </div>    
    </div>

<div class="s002">
	<div class = 'table-responsive bottommargin overflow-auto'>

	<?php $this->widget('zii.widgets.CDetailView', array(
		'htmlOptions' => array('class' => 'table cart1'),     
		'data'=>$model,
		'attributes'=>array(
			'id',
			'name',
			'remark',
			'created_by',
			'created_time',
			'updated_by',
			'updated_time',
		),
	)); ?>
	</div>
</div>