<?php
/* @var $this MarketplaceController */
/* @var $model Marketplace */

$this->breadcrumbs=array(
	'Marketplaces'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Marketplace', 'url'=>array('index')),
	array('label'=>'Create Marketplace', 'url'=>array('create')),
	array('label'=>'Update Marketplace', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Marketplace', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Marketplace', 'url'=>array('admin')),
);
?>

<h1>View Marketplace #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'route_id',
		'zone_id',
		'price',
		'username',
		'status',
	),
)); ?>
