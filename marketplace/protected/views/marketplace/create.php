<?php
/* @var $this MarketplaceController */
/* @var $model Marketplace */

$this->breadcrumbs=array(
	'Marketplaces'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Marketplace', 'url'=>array('index')),
	array('label'=>'Manage Marketplace', 'url'=>array('admin')),
);
?>

<h1>Create Marketplace</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>