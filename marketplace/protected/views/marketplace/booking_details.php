<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
    <div class="row clearfix">
        <div class="col-md-12 clearfix">
            <div class="table-responsive">
                <h4>Booking Details</h4>
                <table class="table cart">
                    <tbody>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Booking ID</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount"><?php echo $model['id'] ?></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Route No</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount"><?php echo $model['route_no'] ?></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Route Detail</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount"><?php echo $model['route_detail'] ?></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Zone</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount"><?php echo '['.$model['zone_id'].'] '.$model['zone_name'] ?></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Booking Date</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount">
                                    <?php
                                    $travel_date = date_create($model['release_date']);
                                    echo date_format($travel_date, 'd/m/Y')
                                    ?>
                                </span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Travel Direction</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount"><?php echo $model['travel_direction']; ?></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Sold By</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount"><?php echo $model['sold_by'] ?></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Price</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount color lead"><strong>BDT <?php echo $model['price'] ?></strong></span>
                            </td>
                        </tr>
                        <tr class="cart_item">
                            <td class="cart-product-name">
                                <strong>Purchase Date</strong>
                            </td>

                            <td class="cart-product-name">
                                <span class="amount">
                                    <?php
                                    $travel_date = date_create($model['release_date']);
                                    echo date_format($travel_date, 'd/m/Y h:i A'); ?>
                                </span>
                            </td>
                        </tr>
                    </tbody>

                </table>

            </div>
        </div>
    </div>
    <!--<div class="row clearfix">
        <div class="col-md-12 clearfix">
            <div class="table-responsive">
                <h4>Cart Totals</h4>

                <table class="table cart">
                    <tbody>
                    <tr class="cart_item">
                        <td class="cart-product-name">
                            <strong>Cart Subtotal</strong>
                        </td>

                        <td class="cart-product-name">
                            <span class="amount">$106.94</span>
                        </td>
                    </tr>
                    <tr class="cart_item">
                        <td class="cart-product-name">
                            <strong>Shipping</strong>
                        </td>

                        <td class="cart-product-name">
                            <span class="amount">Free Delivery</span>
                        </td>
                    </tr>
                    <tr class="cart_item">
                        <td class="cart-product-name">
                            <strong>Total</strong>
                        </td>

                        <td class="cart-product-name">
                            <span class="amount color lead"><strong>$106.94</strong></span>
                        </td>
                    </tr>
                    </tbody>

                </table>

            </div>
        </div>
    </div>-->