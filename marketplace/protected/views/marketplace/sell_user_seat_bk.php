<style>
    .w100{
        width: 100%;
    }
    .choices__item--selectable{
        padding: 0 20px 0 45px;
    }
    .bg-danger{
        background-color: #ff6771 !important;
    }
    .choices__list--dropdown{
        z-index: 1000;
    }
</style>
<!--<pre>< ?php var_dump($staffInfo); ?></pre>-->
<div class="s002 topmargin">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'marketplace-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php if (!empty($routeInfo)): ?>
        <div class="col_full">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4>Route Information</h4>
            </div>
        </div>
        <div class="clear"></div>

        <div class="col_half ">
            <?php echo $form->labelEx($model,'zone_id'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->zone_id = $routeInfo['zone_id'];
                    $zone_list = Zone::model()->findAll(array('order' => 'name ASC'));
                    echo $form->hiddenField($model,'zone_id',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    echo $form->dropDownList($model,'zone_id', CHtml::listData($zone_list, 'id', 'name'),array('empty' => 'Select Zone', 'readOnly' => true, 'data-trigger'=>"", 'disabled'=>"disabled"));
                    ?>
                </div>
            </div>
        </div>

        <div class="col_half col_last">
            <?php echo $form->labelEx($model,'route_id'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->route_id = $routeInfo['id'];
                    echo $form->hiddenField($model,'route_id',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    ?>
                    <input type="text" value="<?php echo $routeInfo['route_detail']; ?>" readonly="readonly">
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="col_half">
            <?php echo $form->labelEx($model,'price'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->price = $routeInfo['price'];
                    echo $form->textField($model,'price',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    ?>
                </div>
            </div>
        </div>
        <?php $slot_data = $this->checkNextSlotAndCommuteDirection() ?>
        <div class="col_half col_last">
            <?php echo $form->labelEx($model,'release_date'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->release_date = $slot_data->date;

                    echo $form->textField($model,'release_date',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    ?>

                </div>
            </div>
        </div>
        <div class="clear"></div>

        <div class="col_half col_last">
            <?php echo $form->labelEx($model,'travel_direction'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
    //                $model->checkCommute = $this->checkNextAvailableTime();
                    $model->travel_direction = $slot_data->travel_direction;

                    echo $form->textField($model,'travel_direction',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    ?>

                </div>
            </div>
        </div>
        <div class="clear"></div>
<!--        < ?php echo $slot_data->currtime ?>-->
        <?php
        if($errorSold) {
            echo '<div class="col_full"><div class="style-msg errormsg"><div class="sb-msg"><i class="icon-remove"></i><strong>Oh snap!</strong> You have already released your seat for sell.</div></div></div>';
        }
        ?>

        <?php if($slot_data->slot_open): ?>
            <div>
                <?php
                $model->username = $userPin;
                echo $form->hiddenField($model,'username',array('placeholder'=>'', 'tabindex'=>-1));
                $model->price = $routeInfo['price'];
                echo $form->hiddenField($model,'price',array('placeholder'=>'', 'tabindex'=>-1));
                echo $form->hiddenField($model,'travel_direction',array('placeholder'=>'', 'tabindex'=>-1));
                echo $form->hiddenField($model,'release_date',array('placeholder'=>'', 'tabindex'=>-1));?>
            </div>
        <?php endif; ?>

        <div class="col_full">
            <div class="input-field first-wrap">
                <?php
                if($slot_data->slot_open){
                    echo CHtml::submitButton("Submit",array('class'=>'btn-search','id'=>'search-button'));
                } else {
                    echo "<span class='btn btn-danger w100'>".$slot_data->travel_direction."</span>";
                } ?>
            </div>
        </div>

    <?php else: ?>
        <div class="col_full">
            <div class="style-msg errormsg">
                <div class="sb-msg">
                    <i class="icon-remove"></i><strong>Oh snap!</strong> You have no allocated seat. Please request from here.
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('seatRequest/index') ?>" class="btn btn-success btn-xs" style="float: right;">Request Seat</a></div>
            </div>
        </div>
    <?php endif; ?>
    <?php $this->endWidget(); ?>
</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: false,
            itemSelectText: '',
        });

</script>