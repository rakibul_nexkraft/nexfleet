<?php
/* @var $this MarketplaceController */
/* @var $model Marketplace */

$this->breadcrumbs=array(
	'Marketplaces'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Marketplace', 'url'=>array('index')),
	array('label'=>'Create Marketplace', 'url'=>array('create')),
	array('label'=>'View Marketplace', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Marketplace', 'url'=>array('admin')),
);
?>

<h1>Update Marketplace <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>