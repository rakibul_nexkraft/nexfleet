<style>
    .dialog-ovelay {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.50);
        z-index: 999999
    }
    .dialog-ovelay .dialog {
        width: 400px;
        margin: 100px auto 0;
        background-color: #fff;
        box-shadow: 0 0 20px rgba(0,0,0,.2);
        border-radius: 3px;
        overflow: hidden
    }
    .dialog-ovelay .dialog header {
        padding: 10px 8px;
        background-color: #f6f7f9;
        border-bottom: 1px solid #e5e5e5
    }
    .dialog-ovelay .dialog header h3 {
        font-size: 14px;
        margin: 0;
        color: #555;
        display: inline-block
    }
    .dialog-ovelay .dialog header .fa-close {
        float: right;
        color: #c4c5c7;
        cursor: pointer;
        transition: all .5s ease;
        padding: 0 2px;
        border-radius: 1px
    }
    .dialog-ovelay .dialog header .fa-close:hover {
        color: #b9b9b9
    }
    .dialog-ovelay .dialog header .fa-close:active {
        box-shadow: 0 0 5px #673AB7;
        color: #a2a2a2
    }
    .dialog-ovelay .dialog .dialog-msg {
        padding: 12px 10px
    }
    .dialog-ovelay .dialog .dialog-msg p{
        margin: 0;
        font-size: 15px;
        color: #333
    }
    .dialog-ovelay .dialog footer {
        border-top: 1px solid #e5e5e5;
        padding: 8px 10px
    }
    .dialog-ovelay .dialog footer .controls {
        direction: rtl
    }
    .dialog-ovelay .dialog footer .controls .button {
        padding: 5px 15px;
        border-radius: 3px
    }
    .link {
        padding: 5px 10px;
        cursor: pointer
    }
    ol.rules li{
        margin-left: 20px;
        padding: 2px 10px;
    }
    /*.button {
        cursor: pointer
    }
    .button-default {
        background-color: rgb(248, 248, 248);
        border: 1px solid rgba(204, 204, 204, 0.5);
        color: #5D5D5D;
    }
    .button-danger {
        background-color: #f44336;
        border: 1px solid #d32f2f;
        color: #f5f5f5
    }*/

</style>
<!--<pre>< ?php print_r($userSeat->attributes) ?></pre>-->
<section id="page-title" class="page-title-center" >
    <div class="clearfix">
        <h1>Marketplace</h1>
        <span>Seats Available Today: <?php echo $seatCount; ?></span>

    </div>
</section>

<?php if (isset($userSeat)): ?>
    <section id="page-title" class="page-title-center page-title-nobg noborder" >
        <div class="col_half ">
            <ul class="list-group" style="text-align: left">
                <li class="list-group-item"><strong>MY ROUTE:</strong> <span class="" style="margin-top: 0;"><?php echo empty($userSeat['present_route'])?'Not Assigned':$userSeat['present_route']; ?></span></li>
                <li class="list-group-item"><strong>ROUTE DETAIL:</strong> <span class="" style="margin-top: 0;"><?php echo empty($userSeat['route_detail'])?'Not Assigned':$userSeat['route_detail']; ?></span></li>
            </ul>
        </div>
        <div class="col_half col_last">
            <div class="col_half" >
                <a id="bookSeat" class="button button-border button-rounded " style="width: 190px;height: 40px;" href="<?php echo Yii::app()->baseUrl;?>/index.php/marketplace/bookUserSeat">
                    Book Seat
                </a>
            </div>
            <div class="col_half col_last">
                <a id="sellSeat" class="button  button-border button-rounded" style="width: 190px;height: 40px;" href="<?php echo Yii::app()->baseUrl;?>/index.php/marketplace/sellUserSeat">
                    Sell Seat
                </a>
            </div>
            <div class="clear"></div>
        </div>
    </section>
<?php else: ?>
    <section id="page-title" class="page-title-center page-title-nobg noborder" >
        <div class="clearfix">
            <span>Currently you have no allocated seat. </span>
            <ol class="breadcrumb">
                <li><?php echo CHtml::link("Request A Seat",array("seatRequest/index"),array("class"=>"btn btn-success"));?></li>
            </ol>
        </div>
    </section>
<?php endif; ?>

<script>
    function Confirm(title, msg, $true, $false, $link) { /*change*/
        var $content =
            "<div class='dialog-ovelay'>" +
                "<div class='dialog'>" +
                    "<header>" +
                        " <h3> " + title + " </h3> " +
                        "<i class='fa fa-close'></i>" +
                    "</header>" +
                    "<div class='dialog-msg'>" +
                        "<p> " + msg + " </p> " +
                        "<ol class='rules' style='margin-top: 10px;list-style: decimal'> " +
                            "<li>User will get 60%" +
                            "<li>iFleet will get 40%" +
                        "</ol>" +
                        "<p class='topmargin-sm'><input type='checkbox' onchange='sellConfirmCheck()' id='sellConfirmCheck' /> I agree with the <a href='#'>Terms &amp; Conditions</a></p>" +
                    "</div>" +
                    "<footer>" +
                        "<div class='controls'>" +
                            " <button class='btn btn-danger btn-sm doAction' disabled='disabled' id='doAction'>" + $true + "</button> " +
                            " <button class='btn btn-default btn-sm cancelAction' id='cancelAction'>" + $false + "</button> " +
                        "</div>" +
                    "</footer>" +
                "</div>" +
            "</div>";
        $('body').prepend($content);
        $('.doAction').click(function () {
            window.location.href = $link;
            $(this).parents('.dialog-ovelay').fadeOut(500, function () {
                $(this).remove();
            });

            return true;
        });
        $('.cancelAction, .fa-close').click(function () {
            $(this).parents('.dialog-ovelay').fadeOut(500, function () {
                $(this).remove();
            });
            return false;
        });
    }

    $('a#sellSeat').click(function (e) {
        var link = document.getElementById("sellSeat").getAttribute("href");
        if (Confirm('Confirmation', 'Do you really want to sell your seat?', 'Confirm', 'Cancel', link)){
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    });

    function sellConfirmCheck(){
        if (document.getElementById("sellConfirmCheck").checked){
            console.log("Confirm checked");
            document.getElementById("doAction").removeAttribute("disabled");
        } else {
            console.log("Confirm not checked");
            document.getElementById("doAction").setAttribute("disabled", "disabled");
        }
    }
</script>
