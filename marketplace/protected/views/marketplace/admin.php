<?php
/* @var $this MarketplaceController */
/* @var $model Marketplace */

$this->breadcrumbs=array(
	'Marketplaces'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Marketplace', 'url'=>array('index')),
	array('label'=>'Create Marketplace', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('marketplace-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Marketplaces</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'marketplace-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'route_id',
		'zone_id',
		'price',
		'username',
		'status',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
