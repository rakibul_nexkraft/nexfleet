<?php
/* @var $this RoutesController */
/* @var $data Routes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('route_no')); ?>:</b>
	<?php echo CHtml::encode($data->route_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('route_detail')); ?>:</b>
	<?php echo CHtml::encode($data->route_detail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seat_capacity')); ?>:</b>
	<?php echo CHtml::encode($data->seat_capacity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('available_seat')); ?>:</b>
	<?php echo CHtml::encode($data->available_seat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>