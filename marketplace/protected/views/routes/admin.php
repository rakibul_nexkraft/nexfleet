<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Routes List', 'url'=>array('index')),
	array('label'=>'New Route', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('routes-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Routes</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'routes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
			'name' => 'route_no',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->route_no), array("view", "id"=>$data->id))',
		),
		'route_detail',
		'actual_seat',
		array(
			'name'=>'seat_capacity',
			'type'=>'raw',
			'value'=>'$data->borderPass($data->route_no)',
		),
		array(
			'name'=>'available_seat',
			'type'=>'raw',
			'value'=>'$data->availableSeat($data->actual_seat, $data->borderPass($data->route_no), $data->route_no)',
		),
		
		array(
			'name'=>'on_mat_leave',
			'type'=>'raw',
			'value'=>'$data->onMatLeave($data->route_no)',
		),
		//'remarks',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
