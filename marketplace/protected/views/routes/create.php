<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	'Create',
);

?>
<div class="center s002 ">  
	<div class="col_one_sixth">	
	 <div class="toggle toggle-border">  
	 	<div class="togglet">Menu</div>
		<div class="togglec" style="display: none;">
			<?php 
			$this->widget('zii.widgets.CMenu', array(
    		'items'=>array(
                array('label'=>'List Zone', 'url'=>array('/zone/index')),      			
    			),
			));

			?>
		</div>
	  </div>
	 </div>
	 <div class="col_five_sixth .col_last">	
	  <h4 class="heading-custom">Create Zone</h4>
    </div>
	
</div>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>