<?php
/* @var $this RoutesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Routes',
);

//$this->menu=array(
	//array('label'=>'New Route', 'url'=>array('create')),
	//array('label'=>'Manage Routes', 'url'=>array('admin')),
//);
?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                               
                        array('label'=>'Create Route', 'url'=>array('/routes/create')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Routes</h4>
        </div>    
    </div>
    <div class="center s002 ">
		<?php $form=$this->beginWidget('CActiveForm', array(
		    'action'=>Yii::app()->createUrl($this->route),
		    'enableAjaxValidation'=>true,
		    'method'=>'get',
		)); ?>
	    <div class="inner-form">
	        <div class='col_one_fourth'>
	            <label>Route No</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	                
	               <?php echo $form->textField($model,'route_no',array('placeholder'=>'Route No','id'=>"search",'list'=>'browsers'));
	                
	               ?>                                       
	            </div>
	        </div>
	       	<?php $route=Routes::model()->findAll();
	       		//var_dump($route);
	       	?>

	       	<datalist id="browsers">
	        <?php 
	        	foreach ($route as $key => $value) {			
	        		echo "<option value='".$value['route_no']."'>";
	        	}
	        ?>
			</datalist>
	   
	        
	        <div class='col_one_fourth'>
	                 <label>Search</label>
	                <div class='input-field first-wrap'>
	                      <?php echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button'));?>        
	                </div>
	       </div>
	       <div class='col_one_fourth'>
	       </div>
	       <div class='col_one_fourth'>
	       </div>
	    </div> 
		<?php $this->endWidget(); ?>
	</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'routes-grid',
	
    'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),       
    'rowCssClass'=>array('cart_item'),
    'dataProvider'=>$model->search(),
    'pager' => array(
    'firstPageLabel'=>'First',
    'lastPageLabel'=>'Last',
     ),
    'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
			'name' => 'route_no',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->route_no), array("view", "id"=>$data->id))',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name' => 'route_detail',
			'type' => 'raw',
			'value' => 'SeatRequest::routeDetail($data->id,3)',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name' => 'actual_seat',
			'type' => 'raw',
			'value' => '$data->actual_seat',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		
		
		array(
			'name'=>'seat_capacity',
			'type'=>'raw',
			'value'=>'$data->borderPass($data->route_no)',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name'=>'available_seat',
			'type'=>'raw',
			'value'=>'$data->availableSeat($data->actual_seat, $data->borderPass($data->route_no), $data->route_no)',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		
		array(
			'name'=>'on_mat_leave',
			'type'=>'raw',
			'value'=>'$data->onMatLeave($data->route_no)',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
        array(
            'name'=>'vehicle_reg_no',
            'type'=>'raw',
            'value'=>'$data->vehicle_reg_no',
            'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
        array(
            'name'=>'zone_id',
            'type'=>'raw',
            'value'=>'$data->route_per_zone->name',
            'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
        array(
            'name'=>'price',
            'type'=>'raw',
            'value'=>'$data->price',
            'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
        array(
            'name'=>'market_price',
            'type'=>'raw',
            'value'=>'$data->market_price',
            'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
        ),
		 array(
           'class'=>'CButtonColumn',
           'template'=>'{update},{delete}',
           'buttons'=>array(
             'update'=>array(
                  'label'=>'UPDATE',
              ),
             'delete'=>array(
                  'label'=>'DELETE',                  
                'visible'=>'Yii::app()->user->username=="admin"?true:false',
              ),
            ),
           
           //'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/image/delete.png',
           //'updateButtonImageUrl'=>Yii::app()->baseUrl.'/image/update.jpg',
        ),
		//'remarks',
	),
));
?>
</div>
<div class="center s002 ">
<?php
echo CHtml::link('Export to Excel',array('routes/excel'),array('style'=>'margin-right:50px;'));

/*$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#routes-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	'title' => 'Transport Department - Routes List',
));*/
?>
</div>