<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);


?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'List Zone', 'url'=>array('/routes/index')),       
                        array('label'=>'Create Zone', 'url'=>array('/routes/create')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Update Route ID# <?php echo $model->id;?></h4>
        </div>    
    </div>

<h4>Update Routes : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>