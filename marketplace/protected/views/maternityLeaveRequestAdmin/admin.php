<?php
/* @var $this MaternityLeaveRequestAdminController */
/* @var $model MaternityLeaveRequestAdmin */

$this->breadcrumbs=array(
	'Maternity Leave Request Admins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request Admin', 'url'=>array('index')),
	array('label'=>'Create Maternity LeaveRequest Admin', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('maternity-leave-request-admin-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Maternity Leave Request Admin</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'maternity-leave-request-admin-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_pin',
		'from_date',
		'to_date',
		'status',
		'created_by',
		/*
		'updated_by',
		'created_time',
		'update_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
