<div id="route-request-list">
   	<div class="center s002 "> 
   		<h4 class="heading-custom">Maternity Leave User Information</h4>   	
	</div>	
	<div class="table-responsive bottommargin">
		<table class="table cart1">
			
			<thead>
				<tr>
					<th class="cart-product-price">Name</th>
					<!--<th class="cart-product-price">ID</th>-->	
					<th class="cart-product-price">User Pin</th>		
					<th class="cart-product-price">Department Name</th>						
					<th class="cart-product-price">User Used Route</th>						
											
					<!--<th class="cart-product-price">Updated Time</th>			
					<th class="cart-product-price">Update</th>-->			
				</tr>
			</thead>
			<tbody>				
				<tr class="cart_item">
					<td class="cart-product-price"><?php echo $model->userName($model->user_pin); ?></td>
					<!--<td class="cart-product-price"><?php// echo $value['id'];?></td>-->	
					<td class="cart-product-price"><?php echo $model->user_pin;?></td>
					<td class="cart-product-price"><?php echo $model->userDepartment($model->user_pin);?></td>
					<td class="cart-product-price"><?php echo $model->userRoute($model->user_pin);?></td>									
				</tr>
			</tbody>
		</table>
	</div>
	<?php
	$sql="SELECT * FROM tbl_maternity_leave_assign WHERE meternity_request_id='".$model->id."' AND status=1";
	$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
	if($sql_rs){
	?>
	<div class="center s002 "> 
   		<h4 class="heading-custom">Maternity Leave Route Use User Information</h4>   	
	</div>
	<div class="table-responsive bottommargin">
		<table class="table cart1">
			
			<thead>
				<tr>
					<th class="cart-product-price">ID</th>
					<!--<th class="cart-product-price">ID</th>-->	
					<th class="cart-product-price">User Pin</th>		
					<th class="cart-product-price">User Name</th>		
					<th class="cart-product-price">Department Name</th>						
					<th class="cart-product-price">Level</th>						
					<th class="cart-product-price">User Cell</th>						
					<th class="cart-product-price">User Email</th>						
					<th class="cart-product-price">Route No</th>						
					<th class="cart-product-price">Seat Request Status</th>						
					<th class="cart-product-price">Queue</th>				
					<th class="cart-product-price">Assing User</th>		
				</tr>
			</thead>
			<tbody>		
			<?php 
				$seatRequest=SeatRequest::model()->findByPk($sql_rs['seat_request_id']);	
						
				?>		
				<tr class="cart_item">
					<td class="cart-product-price"><?php echo $sql_rs['id']; ?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_name; ?></td>
					<!--<td class="cart-product-price"><?php// echo $value['id'];?></td>-->	
					<td class="cart-product-price"><?php echo $seatRequest->user_pin;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_department;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_level;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_cell;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->email;?></td>				
					<td class="cart-product-price"><?php echo Stoppage::routeNo($seatRequest->route_id);?></td>		
					<td class="cart-product-price"><?php echo SeatRequest::status($seatRequest->status)?></td>							
					<td class="cart-product-price"><?php echo $seatRequest->queue;?></td>				
					<td class="cart-product-price">
						<div class="input-field first-wrap">

		    				<?php  
		    				$button=($sql_rs['status']==0 ?"Update":($sql_rs['status']==1?"Assign":""));
		    				echo CHtml::submitButton($button,array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'maternityUserAssing("'.$sql_rs["id"].'","'.$seatRequest->user_pin.'")')); 
		    				?>

						</div>								
				</tr>
			

			</tbody>
		</table>
	</div>	

	<?php 	}

	$sql="SELECT * FROM tbl_maternity_leave_assign WHERE meternity_request_id='".$model->id."'";
	$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
	?>
	<div class="center s002 "> 
   		<h4 class="heading-custom">Request User List</h4>   	
	</div>
	<div class="table-responsive bottommargin">
		<table class="table cart1">
			
			<thead>
				<tr>
					<th class="cart-product-price">ID</th>
					<!--<th class="cart-product-price">ID</th>-->	
					<th class="cart-product-price">User Pin</th>		
					<th class="cart-product-price">User Name</th>		
					<th class="cart-product-price">Department Name</th>						
					<th class="cart-product-price">Level</th>						
					<th class="cart-product-price">User Cell</th>						
					<th class="cart-product-price">User Email</th>						
					<th class="cart-product-price">Route No</th>						
					<th class="cart-product-price">Seat Request Status</th>						
					<th class="cart-product-price">Queue</th>				
					<th class="cart-product-price">Assing User</th>		
				</tr>
			</thead>
			<tbody>		
			<?php if(count($sql_rs>0)){
					foreach ($sql_rs as $key => $value) {
					$seatRequest=SeatRequest::model()->findByPk($value['seat_request_id']);	
						
				?>		
				<tr class="cart_item">
					<td class="cart-product-price"><?php echo $value['id']; ?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_name; ?></td>
					<!--<td class="cart-product-price"><?php// echo $value['id'];?></td>-->	
					<td class="cart-product-price"><?php echo $seatRequest->user_pin;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_department;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_level;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->user_cell;?></td>
					<td class="cart-product-price"><?php echo $seatRequest->email;?></td>				
					<td class="cart-product-price"><?php echo Stoppage::routeNo($seatRequest->route_id);?></td>		
					<td class="cart-product-price"><?php echo SeatRequest::status($seatRequest->status)?></td>							
					<td class="cart-product-price"><?php echo $seatRequest->queue;?></td>				
					<td class="cart-product-price">
						<div class="input-field first-wrap">

		    				<?php  
		    				$button=($value['status']==0 ?"Update":($value['status']==1?"Assign":""));
		    				echo CHtml::submitButton($button,array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'maternityUserAssing("'.$value["id"].'","'.$seatRequest->user_pin.'")')); 
		    				?>

						</div>								
				</tr>
			<?php }}
				else{
			?>
				<tr class="cart_item">
					<td class="cart-product-price" colspan="2">No list Found!</td>
										
				</tr>
			<?php
				}
			?>

			</tbody>
		</table>
	</div>	
	
</div>	
<script type="text/javascript">
	function maternityUserAssing(id,userPin){
		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Do You Want to Update User pin: "+userPin+"? </h5><div id='notice'></div><input type='submit'  onClick='maternityAssignUpdated("+id+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";		
		openModel(html);
	}
	function maternityAssignUpdated(id){
		
		$.post('<?php echo Yii::app()->createAbsoluteUrl("/maternityLeaveRequestAdmin/maternityAssignUpdated");?>',{id:id},function(data){
							openModel(data);
							//alert(data);
					});

	}
	
</script>