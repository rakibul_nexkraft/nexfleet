<?php
/* @var $this MaternityLeaveRequestAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Maternity Leave Request Admins',
);

//$this->menu=array(
	//array('label'=>'Create Maternity Leave Request Admin', 'url'=>array('create')),
	//array('label'=>'Manage Maternity Leave Request Admin', 'url'=>array('admin')),
//);
?>
<div id="route-request-list">
	<div class="center s002 ">
		<?php $form=$this->beginWidget('CActiveForm', array(
		    'action'=>Yii::app()->createUrl($this->route),
		    'enableAjaxValidation'=>true,
		    'method'=>'get',
		)); ?>
	    <div class="inner-form">
	    	<div class='col_one_fourth'>
	            <label>User Pin</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	               <?php echo $form->textField($model,'user_pin',array('size'=>18,'class'=>'input-medium')); ?>
	                
	                                                            
	            </div>
        	</div>
        	<div class='col_one_fourth'>
	            <label>User Name</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	               <?php echo $form->textField($model,'user_name',array('size'=>18,'class'=>'input-medium')); ?>
	                                                            
	            </div>
        	</div>
        	<div class='col_one_fourth'>
	            <label>Department</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	               <?php echo $form->textField($model,'user_department',array('size'=>18,'class'=>'input-medium'));?>
	                                                            
	            </div>
        	</div>
	        <div class='col_one_fourth'>
	            <label>Route No</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	                
	               <?php echo $form->textField($model,'route',array('placeholder'=>'Route No','id'=>"search",'list'=>'browsers'));
	                
	               ?>                                       
	            </div>
	        </div>
	       	<?php $route=Routes::model()->findAll();
	       		//var_dump($route);
	       	?>

	       	<datalist id="browsers">
	        <?php 
	        	foreach ($route as $key => $value) {			
	        		echo "<option value='".$value['route_no']."'>";
	        	}
	        ?>
			</datalist>
	   
	    </div>
	    <div class="inner-form">
	    	<div class='col_one_fourth'>
	            <label>STATUS</label>           
	            <div class="input-field fouth-wrap">
					<div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
	                        </path>
	                    </svg>
	                </div>
                <select data-trigger="" name="MaternityLeaveRequestAdmin[status]">                   
                     <option value='' selected>A Route Select </option>                       
                     <option value="0">Pending</option>
                     <option value="1">Approved</option>
                     <option value="-1">Cancel</option>        
                </select>
               
				         
	 		</div>           
	     </div>     
	        
			<div class='col_one_fourth'>
				  <label>From Date</label>
					<div class="input-field first-wrap">
					    <div class="icon-wrap ">
						    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
							    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
							    </path>
			                </svg>
						</div>
						<?php echo $form->textField($model,'from_date',array('placeholder'=>'From Date','id'=>'depart','class'=>'datepicker')); ?>
					</div>
	        </div>
	        <div class='col_one_fourth'>
	        	<label>To Date</label>
					<div class="input-field first-wrap">
					    <div class="icon-wrap ">
						    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
							    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
							    </path>
			                </svg>
						</div>
						<?php echo $form->textField($model,'to_date',array('placeholder'=>'To Date','id'=>'depart1','class'=>'datepicker')); ?>
					</div>
	        </div>
	        <div class='col_one_fourth'>
	                 <label>Search</label>
	                <div class='input-field first-wrap'>
	                      <?php echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button'));?>        
	                </div>
	       </div>
	       
	       
	    </div> 
		<?php $this->endWidget(); ?>
	</div>


   	<div class="center s002 ">  

   		<h4 class="heading-custom">Maternity Leave Request (Admin)</h4>
   	
	</div>



<?php //$this->widget('zii.widgets.CListView', array(
	//'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',
//)); ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model->search(),
		//'id',
		'filter'=>$model,
		'columns'=>array(
		array(	
				 'name'=>'id',				
				 'type' => 'raw',
				 'value' => '$data->id',
				//'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(	
				 'name'=>'user_pin',				
				 'type' => 'raw',
				 'value' => '$data->user_pin',				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
	
		array(	
				 'name'=>'user_name',				
				 'type' => 'raw',
				 'value' => '$data->userName($data->user_pin)',				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		array(	
				 'name'=>'user_department',				
				 'type' => 'raw',
				 'value' => '$data->userDepartment($data->user_pin)',				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		array(	
				 'name'=>'user_level',				
				 'type' => 'raw',
				 'value' => '$data->userLevel($data->user_pin)',				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		array(	
				 'name'=>'user_cell',				
				 'type' => 'raw',
				 'value' => '$data->userCell($data->user_pin)',				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		
		array(	
				 'name'=>'user_email',				
				 'type' => 'raw',
				 'value' => '$data->userEmail($data->user_pin)',				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		
		array(	
				 'name'=>'route',				
				 'type' => 'raw',
				 'value'=>'$data->userRoute($data->user_pin)',
			  'filter'=>Routes::model()->getAllRouteNo(),				
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		
			array(	
				 'name'=>'from_date',				
				 'type' => 'raw',
				 'value'=>'$data->from_date',			 			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(	
				 'name'=>'to_date',				
				 'type' => 'raw',
				 'value'=>'$data->to_date',			 			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		array(	
				 'name'=>'status',				
				 'type' => 'raw',
				 		
				'value'=>'CHtml::dropDownList("$model","$data->status",array("0"=>"Pending","1"=>"Approved","-1"=>"Cancel"),$htmlOptions=array("onChange"=>"status($data->id,$(this).val(),$data->status)","class"=>"status"))',
				'filter'=>array('0'=>'Pending','1'=>'Approved','-1'=>'Cancel'),			 			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		array(	
				 'header'=>'Assign User',				
				 'type' => 'raw',
				 'value'=>'$data->assignUser($data->id)',
				//'value'=>'$data->status==1 ? CHtml::link("Assign User", array("assignUser","id"=>$data->id)):""',
				'filter'=>array('0'=>'Pending','1'=>'Approved','-1'=>'Cancel'),			 			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		
		
		//'created_by',
		/*
		'updated_by',
		'created_time',
		'update_time',
		*/
		//array(
			//'class'=>'CButtonColumn',
			//'template'=>'{view}{update}',
		//),
	),
)); ?>
<script>
    function status(id,value,preValue){
        
        var message ="Do you want to change maternity leave status for maternity leave ID# "+id+"?";
    
        var con=confirm(message);
        if(con===true){     
             jQuery.post('<?php echo $this->createUrl('statusChange');?>',{id:id,value:value,preValue:preValue},function(data){
                //var size_to_value=JSON.parse(data);
                //alert(data);
                //alert(data);
                 openModel(data);
            });
   
        }
        else{
            //$(this).val()=preValue;
            //alert(preValue);
             ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }
        

    }
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
   	const choices = new Choices('[data-trigger]',
    {
        searchEnabled: false,
        itemSelectText: '',
    });

</script>