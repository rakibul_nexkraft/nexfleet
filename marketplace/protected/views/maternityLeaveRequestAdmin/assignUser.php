<?php

$this->breadcrumbs=array(
	'Maternity Leave Request Admin Assign User'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request Admin', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Request Admin', 'url'=>array('create')),
	array('label'=>'View Maternity Leave Request Admin', 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>'Manage Maternity Leave Request Admin', 'url'=>array('admin')),
);
?>
<style>
	.assign{
		color: red;
	}
    
</style>
<h1> Maternity Leave User Information : </h1>
<?php echo "<em>Name:</em> ".$model->userName($model->user_pin); ?></br>
<?php echo "<em>User Pin:</em> ".$model->user_pin; ?></br>
<?php echo "<em>Department Name:</em> ".$model->userDepartment($model->user_pin); ?></br>
<?php echo "<em>User Used Route:</em> ".$model->userRoute($model->user_pin); ?></br>
<h1> Maternity Leave's Assign User Information : </h1>
<?php
 $maternity_leave_assign=MaternityLeaveAssign::model()->findByAttributes(array('maternity_user_pin'=>$model->user_pin),'status=1');

if($maternity_leave_assign){
	$seat_request_user=SeatRequestAdmin::model()->findByPk($maternity_leave_assign['seat_request_id']);
	 echo "<em>Name:</em> ".$seat_request_user['user_name']."</br>";
	 echo "<em>User Pin:</em> ".$seat_request_user['user_pin']."</br>";
	 echo "<em>Department Name:</em> ".$seat_request_user['user_department']."</br>";
	 echo "<em>User Used Route:</em> ".SeatRequestAdmin::routeDetail($seat_request_user['route_id'])."</br>";
}
else{
		$maternity_leave_assign["id"]=0;
		echo "No User Assign Found For Maternity Leave";
}

	
?>
<h1>Select User for Assign Maternity Leave</h1>
<?php


?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'seat-request-admin-grid',
	'dataProvider'=>$seat_request,
	'rowCssClassExpression'=>'($data["user_pin"]=='.$maternity_leave_assign["user_pin"].') ? "assign":""',
	//'filter'=>$seat_request,
	'columns'=>array(
		
		array(
			'name' => 'id',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
		),
		'user_pin',
		'user_name',
		'user_department',
		'user_level',
		'user_cell',
		
		'email',
		array('name'=>'route_id',
			'type'=>'raw',
			'value'=>'$data->routeDetail($data->route_id)',
			'filter'=>Stoppage::model()->getAllRouteNo(),
		),
		'expected_date',
		'remarks',
		
		//'created_by',
		//'created_time',
		//'updated_by',
		//'updated_time',
		
		array('name'=>'queue',
            'type'=>'raw',			
			'value'=>'$data->queueCheck($data->queue,$data->status)',
			//'filter'=>array('-1'=>'Assigned','0'=>'Not Approved','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'),
		),
		array('header'=>'Assign',
			  'type'=>'raw',
			   'value'=>'get($data,'.$maternity_leave_assign["id"].','.$model->id.')',/*{
			   	if($maternity_leave_assign){var_dump($maternity_leave_assign);
			   		//return 'CHtml::link("Update",array("MaternityLeaveAssign/update","id"=>'.$maternity_leave_assign["id"].'))';
			   		return "of";}
			   	else{ 
			   		var_dump($maternity_leave_assign);
			   		//CHtml::link("Submit",array("MaternityLeaveAssign/create","seatid"=>$data->id,"maternityid"=>'.$model->id.'));
			   		return CHtml::link("Submit",array("MaternityLeaveAssign/create","seatid"=>$data->id,"maternityid"=>'.$model->id.'));}

			   },
			'if('.$maternity_leave_assign["status"].'==1) { echo CHtml::link("Update",array("MaternityLeaveAssign/update","id"=>'.$maternity_leave_assign["id"].'));
					} 
					else{
					echo CHtml::link("Submit",array("MaternityLeaveAssign/create","seatid"=>$data->id,"maternityid"=>'.$model->id.'));
					}',	*/			
					
		),
		
	),
));
function get($data,$m,$id){
	if($m!=0){
		return CHtml::link("Update",array("MaternityLeaveAssign/update","id"=>$m)); 
	}
	else {
		$maternity_assign_id=MaternityLeaveAssign::model()->findByAttributes(array('user_pin'=>$data->user_pin),'status=1');
		if($maternity_assign_id){
			return $maternity_assign_id['maternity_user_pin'];
		}
		else{
			return CHtml::link("Submit",array("MaternityLeaveAssign/create","seatid"=>$data->id,"maternityid"=>$id));
		}
	}
	  
}
//(isset('.$maternity_leave_assign["status"].')) && ('.$maternity_leave_assign["status"].'==1)
?>
