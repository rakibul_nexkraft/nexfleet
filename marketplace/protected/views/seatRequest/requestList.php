<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seat Requests',
);
?>
<div id="route-request-list">
   	<div class="center s002 ">  

   		<h4 class="heading-custom">ROUTES REQUEST LIST</h4>
   	
	</div>

	<?php  $this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model->search(),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(
			//'id',
			
			array(	
				 'header'=>'User',				
				'value'=>'$data->user_pin',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'user_name',
			//'user_department',
			array('header'=>'Route No',				
				'value'=>'$data->routeDetail($data->route_id,1)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Route Detail',	
			
				'type'=>'raw',
				'value'=>'$data->routeDetail($data->route_id,2,$data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//array('header'=>'Stoppage',
				
				//'type'=>'raw',
				//'value'=>'$data->stoppageDetail($data->stoppage_id)',
				//'htmlOptions'=>array('class'=>'cart-product-price'),
				//'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			//),
			array('header'=>'Pickup Time',
				'type'=>'raw',
				'value'=>'$data->stoppagePickupTime($data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'route_id',
			
			array('header'=>'Expected Date',				
				'value'=>'$data->expected_date',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			
		
			//'remarks',
			array('header'=>'Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Queue',
	            'type'=>'raw',		
	            'value'=>'$data->queueCheck($data->queue,$data->status)',	
			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Apply Maternity Leave Use',
	            'type'=>'raw',			
				'value'=>'($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Update',
	            'type'=>'raw',			
				'value'=>'CHtml::submitButton("Update",array("class"=>"btn-search1","id"=>"search-button1","onClick"=>"routeInformationUpdate($data->id)","name"=>""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'status',
			/*'created_by',
			'created_time',
			'updated_by',
			'updated_time',
			*/
			/*array(
				'class'=>'CButtonColumn',
				'template'=>'{view}{update}{delete}',
				'deleteConfirmation'=>"js:'Do you really want to cancel request record with ID '+$(this).parent().parent().children(':nth-child(1)').text()+'?'",
				 'buttons'=>array(
	        		'delete' => array(
	            	'label'=>'Cancel Request', 

	        		),
	        	),
			),*/

		),
	)
	//array(
        //'htmlOptions' => array('class' => 'table cart')
    //)
	); 
	?>
	<?php 

	$sql_waiting="SELECT * FROM tbl_seat_request WHERE status=1 AND queue>=1 AND user_pin='" .Yii::app()->user->username."' AND maternity_leave=0";
	$sql_waiting_rs=Yii::app()->db->createCommand($sql_waiting)->queryRow();
	if($sql_waiting_rs){
		  	$route_id=$sql_waiting_rs['route_id'];	
			$sql_maternity_leave="SELECT mt.user_pin ,mt.from_date,mt.to_date,mt.id as mid, sr.id as sid, sr.user_pin  as maternity_user_pin, sr.route_id FROM tbl_maternity_leave_request AS mt INNER JOIN tbl_seat_request AS sr ON mt.user_pin=sr.user_pin WHERE  mt.status=1 AND sr.status=1 AND sr.queue=-1 AND sr.route_id='$route_id'";
			$sql_maternity_leave_rs=Yii::app()->db->createCommand($sql_maternity_leave)->queryAll();
			
	?>	
	<div class="center s002"> 
	   	<h4 class="heading-custom">AVAILABLE MATERNITY LEAVE LIST</h4>	   	
	</div>
	<div class="table-responsive bottommargin">
		<table class="table cart1">
			
			<thead>
				<tr>
					<th class="cart-product-price">SI.</th>
					<!--<th class="cart-product-price">ID</th>-->	
					<th class="cart-product-price">FROM DATE</th>		
					<th class="cart-product-price">TO DATE</th>						
					<th class="cart-product-price">Route NO</th>						
					<th class="cart-product-price">Apply</th>						
					<!--<th class="cart-product-price">Updated Time</th>			
					<th class="cart-product-price">Update</th>-->			
				</tr>
			</thead>
			<tbody>
				<?php if(count($sql_maternity_leave_rs)>0){ ?>
				<?php $i=1;foreach ($sql_maternity_leave_rs as $key => $value) {	?>
				<tr class="cart_item">

					<td class="cart-product-price"><?php echo $i++;?></td>
					<!--<td class="cart-product-price"><?php// echo $value['id'];?></td>-->	
					<td class="cart-product-price"><?php echo $value['from_date'];?></td>
					<td class="cart-product-price"><?php echo $value['to_date'];?></td>
					<td class="cart-product-price"><?php echo SeatRequest::routeDetail($value['route_id'],1);?></td>
					<td class="cart-product-price">
						<div class="input-field first-wrap">
		    				<?php  echo CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'maternityUse("'.$value["mid"].'","'.$value['from_date'].'","'.$value['to_date'].'")')); ?>
						</div>
					</td>
					
				</tr>
			<?php }} 
			else{ ?>
				<tr class="cart_item">
					<td class="cart-product-price" colspan="6"><?php echo "No List Found!";?></td>
				</tr>
			<?php }?>
			</tbody>

		</table>
	</div>
	
	<?php } ?>
</div>
<?php 

	$sql_waiting="SELECT * FROM tbl_seat_request WHERE status=2  AND user_pin='" .Yii::app()->user->username."' AND maternity_leave=1";
	$sql_waiting_rs=Yii::app()->db->createCommand($sql_waiting)->queryRow();
	if($sql_waiting_rs){
		  	$route_id=$sql_waiting_rs['route_id'];	
			$sql_maternity_leave="SELECT mt.user_pin ,mt.from_date,mt.to_date,mt.id as mid, sr.id as sid, sr.user_pin  as maternity_user_pin, sr.route_id FROM tbl_maternity_leave_request AS mt INNER JOIN tbl_seat_request AS sr ON mt.user_pin=sr.user_pin WHERE  mt.status=1 AND sr.status=1 AND sr.queue=-1 AND sr.route_id='$route_id'";
			$sql_maternity_leave_rs=Yii::app()->db->createCommand($sql_maternity_leave)->queryAll();
			
	?>	
	<div class="center s002"> 
	   	<h4 class="heading-custom">AVAILABLE MATERNITY LEAVE LIST</h4>	   	
	</div>
	<div class="table-responsive bottommargin">
		<table class="table cart1">
			
			<thead>
				<tr>
					<th class="cart-product-price">SI.</th>
					<!--<th class="cart-product-price">ID</th>-->	
					<th class="cart-product-price">FROM DATE</th>		
					<th class="cart-product-price">TO DATE</th>						
					<th class="cart-product-price">Route NO</th>						
					<!--<th class="cart-product-price">Apply</th>						
					<th class="cart-product-price">Updated Time</th>			
					<th class="cart-product-price">Update</th>-->			
				</tr>
			</thead>
			<tbody>
				<?php if(count($sql_maternity_leave_rs)>0){ ?>
				<?php $i=1;foreach ($sql_maternity_leave_rs as $key => $value) {	?>
				<tr class="cart_item">

					<td class="cart-product-price"><?php echo $i++;?></td>
					<!--<td class="cart-product-price"><?php// echo $value['id'];?></td>-->	
					<td class="cart-product-price"><?php echo $value['from_date'];?></td>
					<td class="cart-product-price"><?php echo $value['to_date'];?></td>
					<td class="cart-product-price"><?php echo SeatRequest::routeDetail($value['route_id'],1);?></td>
					<!--<td class="cart-product-price">
						<div class="input-field first-wrap">
		    				<?php  //echo CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'maternityUse("'.$value["mid"].'","'.$value['from_date'].'","'.$value['to_date'].'")')); ?>
						</div>
					</td>-->
					
				</tr>
			<?php }} 
			else{ ?>
				<tr class="cart_item">
					<td class="cart-product-price" colspan="6"><?php echo "No List Found!";?></td>
				</tr>
			<?php }?>
			</tbody>

		</table>
	</div>
	
	<?php } ?>
</div>
<script>
	function routeInformationUpdate(requestId){
		
		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Choose Your Request</h5><div id='notice'></div><input type='radio' name='request' value='0'>Request For Approved<br><input type='radio' name='request' value='3'>Request For Cancel<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";
		
		openModel(html);

	}
	function submitRequestUpdated(requestId){
		 var v=$("input[name='request']:checked").val();		
			if(v==null){
		    	$("#notice").html("<h5 style='color:red;margin-bottom:5px;'>Please, Select A Request</h5>");
			}
			else{

				$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>',{requestId:requestId,updateRequest:v},function(data){
							openModel(data);
							//alert(data);
					});
				
			}
	}
	function maternityUse(mid,fromDate,toDate){	

		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Do You Want to Use From Date: "+fromDate+" To Date: "+toDate+"? </h5><h5 style='margin-bottom:5px;'>Your Route Request Will be Canceled. </h5><div id='notice'></div><input type='submit'  onClick='maternityUpdated("+mid+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";
		
		openModel(html);
	
		}
	function maternityUpdated(maternityId){
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/maternityUpdated");?>',{maternityId:maternityId},function(data){
							openModel(data);
							//alert(data);
					});
				
			
	}	
		
</script>