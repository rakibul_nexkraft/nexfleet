<?php
/* @var $this MaternityLeaveAssignController */
/* @var $model MaternityLeaveAssign */

$this->breadcrumbs=array(
	'Maternity Leave Assigns'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Maternity Leave Assign', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Assign', 'url'=>array('create')),
	array('label'=>'Update Maternity Leave Assign', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Maternity Leave Assign', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Maternity Leave Assign', 'url'=>array('admin')),
);
?>

<h1>View Maternity Leave Assign #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'seat_request_id',
		'user_pin',
		'from',
		'to',
		'maternity_user_pin',
		'meternity_request_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		'status',
	),
)); ?>
