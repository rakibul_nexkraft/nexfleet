<?php
/* @var $this MaternityLeaveAssignController */
/* @var $model MaternityLeaveAssign */

$this->breadcrumbs=array(
	'Maternity Leave Assigns'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Maternity Leave Assign', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Assign', 'url'=>array('create')),
	array('label'=>'View Maternity Leave Assign', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Maternity Leave Assign', 'url'=>array('admin')),
);
?>

<h1>Update Maternity Leave Assign <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>