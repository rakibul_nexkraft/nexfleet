<?php
/* @var $this MaternityLeaveAssignController */
/* @var $model MaternityLeaveAssign */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'maternity-leave-assign-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'seat_request_id'); ?>
		<?php echo $form->textField($model,'seat_request_id',array('readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'seat_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from'); ?>
		<?php echo $form->textField($model,'from',array('readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to'); ?>
		<?php echo $form->textField($model,'to',array('readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maternity_user_pin'); ?>
		<?php echo $form->textField($model,'maternity_user_pin',array('size'=>60,'maxlength'=>128,'readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'maternity_user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meternity_request_id'); ?>
		<?php echo $form->textField($model,'meternity_request_id',array('readonly'=>'readonly')); ?>
		<?php echo $form->error($model,'meternity_request_id'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php //echo $form->textField($model,'status'); 
			echo $form->dropDownList($model, 'status',
			array('0' => 'Pending','1' => 'Approved','-1' => 'Cancel'));	
		?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->