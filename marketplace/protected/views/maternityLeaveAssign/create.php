<?php
/* @var $this MaternityLeaveAssignController */
/* @var $model MaternityLeaveAssign */

$this->breadcrumbs=array(
	'Maternity Leave Assigns'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Maternity Leave Assign', 'url'=>array('index')),
	array('label'=>'Manage Maternity Leave Assign', 'url'=>array('admin')),
);
?>

<h1>Create Maternity Leave Assign</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>