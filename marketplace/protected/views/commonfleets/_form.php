<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */
/* @var $form CActiveForm */
?>

<div class="s002">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'commonfleets-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	<?php echo $form->errorSummary($model); ?>

	<div class="inner-form">  
	    <div class="col_one_third">
	    <?php echo $form->labelEx($model,'user_pin'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model, 'user_pin',	    
	    		array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    	'url'=>array("vehicles/CallHrUser"),
	
       			'success'=>"js:function(string){

		        	$('#Commonfleets_user_name').val(string.StaffName);
		            $('#Commonfleets_user_level').val(string.JobLevel);                                    
		            
		            var myarr = string.ProjectName;
		            //var myarr = myarr.split(' ');
		            var myvar = myarr.replace('Department','');
		            $('#Commonfleets_user_dept').val(myvar);                        
		            $('#Commonfleets_user_designation').val(string.DesignationName);
		            $('#Commonfleets_user_cell').val(string.MobileNo);
		            $('#Commonfleets_user_email').val(string.EmailID);
		            $('#Commonfleets_residence_address').val(string.PresentAddress);


		            //             //alert(string.Fname);
		            //             var name = string.Fname+' '+string.Mname+' '+string.Lname;
		            //             $('#Commonfleets_user_name').val(name);
		            //             $('#Commonfleets_user_level').val(string.Level);
		                                                
		                        
		            //             var myarr = string.Project;
														// //var myarr = myarr.split(' ');
														// var myvar = myarr.replace('Department','');
		            //             $('#Commonfleets_user_dept').val(myvar);                        
		            //             $('#Commonfleets_user_designation').val(string.Designation);
														// $('#Commonfleets_user_cell').val(string.Mobile);
														// $('#Commonfleets_user_email').val(string.Email);
														// $('#Commonfleets_residence_address').val(string.ContactAddress);
														
		                        
		                    }"

				    ))),
				    array('empty' => 'Select one of the following...')
			    
			    );
			 ?>
		
			<?php echo $form->error($model,'user_pin'); ?>
		</div>
	 </div>
	 <div class="col_one_third">  
           <?php echo $form->labelEx($model,'user_name'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                  <?php echo $form->textField($model,'user_name',array('size'=>40,'maxlength'=>40)); ?>
				  <?php echo $form->error($model,'user_name'); ?>
            </div>
         
        </div>
       <div class="col_one_third">  
           <?php echo $form->labelEx($model,'user_level'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                 <?php echo $form->textField($model,'user_level'); ?>
				 <?php echo $form->error($model,'user_level'); ?>
            </div>
         
        </div>		
	</div>
	<div class="inner-form">  
	    <div class="col_one_third">
	    <?php echo $form->labelEx($model,'user_dept'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model,'user_dept',array('size'=>11,'maxlength'=>40)); ?>
				<?php echo $form->error($model,'user_dept'); ?>
		</div>
	 </div>
	 <div class="col_one_third">  
          <?php echo $form->labelEx($model,'user_designation'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                  <?php echo $form->textField($model,'user_designation',array('size'=>11,'maxlength'=>40)); ?>
				<?php echo $form->error($model,'user_designation'); ?>
            </div>
         
        </div>
       <div class="col_one_third">  
           <?php echo $form->labelEx($model,'user_cell'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                 <?php echo $form->textField($model,'user_cell'); ?>
				 <?php echo $form->error($model,'user_cell'); ?>
            </div>
         
        </div>		
	</div>	
	<div class="inner-form">  
	    <div class="col_one_third">
	    <?php echo $form->labelEx($model,'user_email'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model,'user_email',array('size'=>11,'maxlength'=>40)); ?>
				<?php echo $form->error($model,'user_email'); ?>
		</div>
	 </div>
	 <div class="col_one_third">  
          <?php echo $form->labelEx($model,'telephone_ext'); ?>
	
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                 	<?php echo $form->textField($model,'telephone_ext'); ?>
					<?php echo $form->error($model,'telephone_ext'); ?>
            </div>
         
        </div>
       <div class="col_one_third">  
           <?php echo $form->labelEx($model,'application_type'); ?>
            <div class="input-field fouth-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
                        </path>
                    </svg>
                </div>		        
				  <select data-trigger="" name="Commonfleets[application_type]">
                    <option value=''>Select one of the following... </option>
                    <?php 
                    if(!empty($model->present_route))
					{
                    foreach($this->application_type as $key=>$value){
                    	if($value==$model->application_type){
                    		echo "<option value='".$key."' selected>".$value."</option>";
                    	}
                    	else{                       
                        	echo "<option value='".$key."'>".$value."</option>";
                    	}
                      }   
                     }
                	else
					{
						echo "<option value='Pick and Drop'>Pick and Drop</option>";
					}
                    ?>
                </select>
				<?php echo $form->error($model,'application_type'); ?>
            </div>
         
        </div>		
	</div>	
	<div class="inner-form">  
	    <div class="col_one_third">
	    <?php echo $form->labelEx($model,'residence_address'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model,'residence_address',array('size'=>60,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'residence_address'); ?>
		</div>
	 </div>
	<div class="col_one_third">
	    <?php echo $form->labelEx($model,'present_route'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model,'present_route',array('size'=>11,'maxlength'=>11,'readonly'=>true)); ?>
				  <?php echo $form->error($model,'present_route'); ?>
		</div>
	 </div>
       <div class="col_one_third">  
           <?php echo $form->labelEx($model,'stoppage_point'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                 <?php echo $form->textField($model,'stoppage_point',array('size'=>60)); ?>
				<?php echo $form->error($model,'stoppage_point'); ?>
            </div>
         
        </div>		
	</div>	
	<div class="inner-form">  
	    <div class="col_one_third">
	    <?php echo $form->labelEx($model,'pick_up_time'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php $this->widget('ext.jui.EJuiDateTimePicker', array(
						'model'     => $model,
						'attribute' => 'pick_up_time',
						'language'=> 'en',//default Yii::app()->language
						'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
						
						'options'   => array(
							//'dateFormat' => 'dd.mm.yy',
							'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
							//'ampm' => 'true',
						),
					));
					?>
				<?php echo $form->error($model,'pick_up_time'); ?>
		</div>
	 </div>
	<div class="col_one_third">
	   <?php echo $form->labelEx($model,'expected_date'); ?>
			<div class="input-field first-wrap">		
		 		 <div class="icon-wrap ">
				    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
					    </path>
	                </svg>
				</div>
                <?php echo $form->textField($model,'expected_date',array('placeholder'=>'Expected Date','id'=>'depart2','class'=>'datepicker')); ?>        		
				  <?php echo $form->error($model,'expected_date'); ?>
		</div>
	 </div>
       <div class="col_one_third">  
           <?php echo $form->labelEx($model,'mt_leave_from'); ?>
            <div class="input-field first-wrap">
			    <div class="icon-wrap ">
				    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
					    </path>
	                </svg>
				</div>
				<?php 
				if(($model->mt_leave_from=="0000-00-00")){$model->mt_leave_from="";}
				echo $form->textField($model,'mt_leave_from',array('placeholder'=>'Maternity Leave From','id'=>'depart','class'=>'datepicker')); ?>
				<?php echo $form->error($model,'mt_leave_from'); ?>
			</div>
         
        </div>		
	</div>
	<div class="inner-form">  
	   <div class="col_one_third">  
           <?php echo $form->labelEx($model,'mt_leave_to'); ?>
            <div class="input-field first-wrap">
			    <div class="icon-wrap ">
				    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
					    </path>
	                </svg>
				</div>

				<?php if(($model->mt_leave_to=="0000-00-00")){$model->mt_leave_to="";}
				echo $form->textField($model,'mt_leave_to',array('placeholder'=>'Maternity Leave To','id'=>'depart1','class'=>'datepicker')); ?>
				<?php echo $form->error($model,'mt_leave_to'); ?>
			</div>
         
        </div>
	<div class="col_one_third">
	     <?php echo $form->labelEx($model,'user_remarks'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model,'user_remarks',array('size'=>60,'maxlength'=>128)); ?>
        		<?php echo $form->error($model,'user_remarks'); ?>
		</div>
	 </div>
       <div class="col_one_third">  
          <?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
            <div class="input-field first-wrap">
			   <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
				<?php echo $form->textField($model,'vehicle_reg_no',array('placeholder'=>'Vehicle Reg. No.','id'=>"search",'list'=>'browsers'));                         
                   ?> 
                   <?php  echo $form->error($model,'vehicle_reg_no'); ?>  
			</div>
         
        </div>		
	</div>
	<datalist id="browsers">
                <?php $vehicle=Vehicles::model()->findAll(array('condition'=>'active=1'));
                    foreach ($vehicle as $key => $value) { 
                    	if($value['reg_no']==$model->vehicle_reg_no) {echo "<option value='".$value['reg_no']."' selected>";
                    	} 
                    	else{         
                        echo "<option value='".$value['reg_no']."'>";
                        }
                    }
                ?>
        </datalist> 
	<div class="inner-form">  
		
	<div class="col_two_third">
	     <?php echo $form->labelEx($model,'fleet_remarks'); ?>
			<div class="input-field first-wrap">		
		 		<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>
        		<?php echo $form->textField($model,'fleet_remarks',array('size'=>60,'maxlength'=>128)); ?>
				<?php echo $form->error($model,'fleet_remarks'); ?>
		</div>
	 </div>
       <div class="col_one_third">  
         <?php echo $form->labelEx($model,'approve_status'); ?>
            <div class="input-field fouth-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
                        </path>
                    </svg>
                </div>		        
				  <select data-trigger="" name="Commonfleets[approve_status]">
                    <option value=''>Select one of the following...</option>
                    <?php 
                   		
                    foreach($this->approve_status as $key=>$value){
                    	if($key==$model->approve_status){
                    		echo "<option value='".$key."' selected>".$value."</option>";
                    	}
                    	else{                       
                        	echo "<option value='".$key."'>".$value."</option>";
                    	}
                      }   
                   
                    ?>
                </select>
				<?php echo $form->error($model,'approve_status'); ?>
            </div>
         
        </div>		
	</div>
	<div class="inner-form"> 
	    <div class="col_one_third">  
         <?php echo $form->labelEx($model,'vehicletype_id'); ?>
            <div class="input-field fouth-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
                        </path>
                    </svg>
                </div>		        
				  <select data-trigger="" name="Commonfleets[vehicletype_id]">
                    <option value=''>Select Vehicles...</option>
                    <?php 
                   		$vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
                    	foreach($vehicletype_id as $key=>$value){
                    	if($value['id']==$model->vehicletype_id){
                    		echo "<option value='".$value['id']."' selected>".$value['type']."</option>";
                    	}
                    	else{                       
                        	echo "<option value='".$value['id']."'>".$value['type']."</option>";
                    	}
                      }   
                   
                    ?>
                </select>
				<?php echo $form->error($model,'vehicletype_id'); ?>
            </div>
         
         
        </div>
        <div class='col_one_third'>
	        <label>Save</label>
	        <div class='input-field first-wrap'>
	        	<?php echo CHtml::submitButton("Save",array('class'=>'btn-search','id'=>'search-button'));?>        
	        </div>
	    </div>
	     <div class='col_one_third'>
	     </div>
      </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
   	const choices = new Choices('[data-trigger]',
    {
        searchEnabled: false,
        itemSelectText: '',
    });

</script>