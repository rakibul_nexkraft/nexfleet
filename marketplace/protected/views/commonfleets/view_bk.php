<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */

$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>' Seat Allocations List', 'url'=>array('index')),
	array('label'=>'New Seat Allocation', 'url'=>array('create')),
	array('label'=>'Update Seat Allocation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Seat Allocation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
);
?>

<h4>View Seat Allocation : <?php echo $model->id; ?></h4>
<?php //$model->vehicletype_id = $model->vehicletypes->type; ?>
<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'group2'=>array(
            'ItemColumns' => 2,
			'attributes' => array(
				'id',						'stoppage_point',
				'user_pin',					'pick_up_time',
				'user_name',				'expected_date',
				'user_level',				'mt_leave_from',
				'user_dept',				'mt_leave_to',
				'user_designation',			'vehicle_reg_no',
				'user_cell',				array('name'=>'vehicletype_id','type'=>'raw','value'=>$model->vehicletypes->type),
				'user_email',				'user_remarks',
				'telephone_ext',			'fleet_remarks',
				'application_type',			'created_by',			
				'residence_address',		'created_time',
				'preferred_route',			'approve_status',
				'present_route',			'updated_by',
				array(),					'updated_time',
				//'recommended_by',
				//'recommended_by_desig',		//'active',
			),
		),
	),
)); 

if(!(Yii::app()->request->isAjaxRequest))
{
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#yw0', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Details Seat Allocation',
));
}
?>
