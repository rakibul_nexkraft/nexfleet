<br />
To: <?php echo $model->user_name ?><br />
<br />
Ref: Transport Requisition Request Notification - Serial No# <?php echo $model->id; ?><br />
<br />
<br />
Dear Concern,<br />
<br />
This is to let you know that your application for availing yourself of pick and drop facilities has been received by Transport Department. But there is no available seat in the desired route right now. 
So your application has been kept in the queue, and you will be notified later subject to availability of seat in the desired route.<br />
<br />
Regards,<br />
Transport Department<br />
<br />
<br />
P.S. This is a computer generated document, hence, required no signature.