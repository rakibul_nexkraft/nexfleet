<?php
/* @var $this CommonfleetsController */
/* @var $data Commonfleets */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_pin')); ?>:</b>
	<?php echo CHtml::encode($data->user_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_level')); ?>:</b>
	<?php echo CHtml::encode($data->user_level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_dept')); ?>:</b>
	<?php echo CHtml::encode($data->user_dept); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_designation')); ?>:</b>
	<?php echo CHtml::encode($data->user_designation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_cell')); ?>:</b>
	<?php echo CHtml::encode($data->user_cell); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('user_email')); ?>:</b>
	<?php echo CHtml::encode($data->user_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephone_ext')); ?>:</b>
	<?php echo CHtml::encode($data->telephone_ext); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('application_type')); ?>:</b>
	<?php echo CHtml::encode($data->application_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residence_address')); ?>:</b>
	<?php echo CHtml::encode($data->residence_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recommended_by')); ?>:</b>
	<?php echo CHtml::encode($data->recommended_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recommended_by_desig')); ?>:</b>
	<?php echo CHtml::encode($data->recommended_by_desig); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('preferred_route')); ?>:</b>
	<?php echo CHtml::encode($data->preferred_route); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('present_route')); ?>:</b>
	<?php echo CHtml::encode($data->present_route); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expected_date')); ?>:</b>
	<?php echo CHtml::encode($data->expected_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mt_leave_from')); ?>:</b>
	<?php echo CHtml::encode($data->mt_leave_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mt_leave_to')); ?>:</b>
	<?php echo CHtml::encode($data->mt_leave_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicletype_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicletype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->user_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fleet_remarks')); ?>:</b>
	<?php echo CHtml::encode($data->fleet_remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approve_status')); ?>:</b>
	<?php echo CHtml::encode($data->approve_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>