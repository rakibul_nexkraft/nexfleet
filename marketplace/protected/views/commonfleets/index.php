<!--<script>
	var $excelForm = $( '#commanfleets_excel' );
	$( '.excel_form a', $excelForm ).click( function() {
		$excelForm.attr( 'action', this.href ).submit();
		return false;
	});
</script>-->


<?php
/* @var $this CommonfleetsController */
/* @var $dataProvider CActiveDataProvider */
//print_r($border_pass['present_route']);
$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
);

//$this->menu=array(
	//array('label'=>'New Seat Allocation', 'url'=>array('create')),
	//array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
//);
?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                               
                        array('label'=>'Seat Allocations List ', 'url'=>array('/commonfleets/index')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Seat Allocations List</h4>
        </div>    
    </div>


<div class="center s002 ">
		<?php $form=$this->beginWidget('CActiveForm', array(
		    'action'=>Yii::app()->createUrl($this->route),
		    'enableAjaxValidation'=>true,
		    'method'=>'get',
		)); ?>
	    <div class="inner-form">
	    	<div class='col_one_fourth'>
	            <label>User Pin</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	               <?php echo $form->textField($model,'user_pin',array('size'=>18,'class'=>'input-medium')); ?>
	                
	                                                            
	            </div>
        	</div>
        	<div class='col_one_fourth'>
	            <label>User Name</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	               <?php echo $form->textField($model,'user_name',array('size'=>18,'class'=>'input-medium')); ?>
	                                                            
	            </div>
        	</div>
        	<div class='col_one_fourth'>
	            <label>Department</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	               <?php echo $form->textField($model,'user_dept',array('size'=>18,'class'=>'input-medium'));?>
	                                                            
	            </div>
        	</div>
	        <div class='col_one_fourth'>
	            <label>Route No</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	                
	               <?php echo $form->textField($model,'present_route',array('placeholder'=>'Route No','id'=>"search",'list'=>'browsers'));
	                
	               ?>                                       
	            </div>
	        </div>
	       	<?php $route=Routes::model()->findAll();
	       		//var_dump($route);
	       	?>

	       	<datalist id="browsers">
	        <?php 
	        	foreach ($route as $key => $value) {			
	        		echo "<option value='".$value['route_no']."'>";
	        	}
	        ?>
			</datalist>
	   
	    </div>
	    <div class="inner-form">
	    	<div class='col_one_fourth'>
	            <label>Application Type</label>
	            <div class="input-field first-wrap">
	                <div class="icon-wrap">
	                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
	                        </path>
	                    </svg>
	                </div>                                                           
	                
	               <?php echo $form->textField($model,'application_type',array('placeholder'=>'Application Type','id'=>"search-type",'list'=>'browsers1'));
	                
	               ?>                                       
	            </div>
	        </div>
	        <datalist id="browsers1">         				
	        	<option value="Pick and Drop"></option>        
	        	<option value="Change Route"></option>        
	        	<option value="Cancel Seat"></option>        
	        	<option value="Maternity Leave"></option>        
			</datalist>
			<div class='col_one_fourth'>
				  <label>From Date</label>
					<div class="input-field first-wrap">
					    <div class="icon-wrap ">
						    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
							    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
							    </path>
			                </svg>
						</div>
						<?php echo $form->textField($model,'from_date',array('placeholder'=>'From Date','id'=>'depart','class'=>'datepicker')); ?>
					</div>
	        </div>
	        <div class='col_one_fourth'>
	        	<label>To Date</label>
					<div class="input-field first-wrap">
					    <div class="icon-wrap ">
						    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
							    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
							    </path>
			                </svg>
						</div>
						<?php echo $form->textField($model,'to_date',array('placeholder'=>'To Date','id'=>'depart1','class'=>'datepicker')); ?>
					</div>
	        </div>
	        <div class='col_one_fourth'>
	                 <label>Search</label>
	                <div class='input-field first-wrap'>
	                      <?php echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button'));?>        
	                </div>
	       </div>
	       
	       
	    </div> 
		<?php $this->endWidget(); ?>
	</div>




<?php
$var = $_GET['Commonfleets'];
if(!empty($var)){
?>
<?php if(!empty($var['from_date']) && !empty($var['to_date'])) echo "<h4>Date: ".$var['from_date']." to ". $var['to_date']."</h4>"; ?>
<?php if(!empty($var['from_date']) && empty($var['to_date'])) echo "<h4>Date: ".$var['from_date']."</h4>"; ?>
<?php if(empty($var['from_date']) && !empty($var['to_date'])) echo "<h4>Date: 0000-00-00 to ". $var['to_date']."</h4>"; ?>
<?php if(!empty($var['user_pin'])) echo "User: ".$var['user_pin']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['vehicle_reg_no'])) echo "Vehicle Regitration Number: ".$var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['approve_status'])) echo "Approve Status: ".$var['approve_status']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['application_type'])) echo "Application Type: ".$var['application_type']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['present_route'])) {
    echo "Seat Capacity: ". Routes::model()->getSeatCapacity($var['present_route'])."<br>";
    echo "Maternity Leave: ". Commonfleets::model()->getMaternityLeaveCount($var['present_route']);
    }
    ?>
<?php } ?>

<?php /*$form=$this->beginWidget('CActiveForm', array(
	'id'=>'excel-form',
	'enableAjaxValidation'=>false,
));*/ ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'commonfleets-grid',
	'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),       
    'rowCssClass'=>array('cart_item'),
	'dataProvider'=>$dataProvider,
	'pager' => array(
    'firstPageLabel'=>'First',
    'lastPageLabel'=>'Last',
     ),
	'columns'=>array(
		array(
			'name' => 'id',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name' => 'user_pin',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->user_pin), array("view", "id"=>$data->id))',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name' => 'user_name',
			'type' => 'raw',
			'value' => '$data->user_name',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name' => 'user_level',
			'type' => 'raw',
			'value' => '$data->user_level',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name' => 'user_dept',
			'type' => 'raw',
			'value' => '$data->user_dept',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
	
        array(
			'name' => 'user_cell',
			'type' => 'raw',
			'value' => '$data->user_cell',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		//'user_designation',
		  array(
			'name' => 'user_email',
			'type' => 'raw',
			'value' => '$data->user_email',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
       array(
			'name' => 'application_type',
			'type' => 'raw',
			'value' => '$data->application_type',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		 array(
			'name' => 'present_route',
			'type' => 'raw',
			'value' => '$data->present_route',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		 array(
			'name' => 'stoppage_point',
			'type' => 'raw',
			'value' => '$data->stoppage_point',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		//'preferred_route',
        array(
			'name' => 'pick_up_time',
			'type' => 'raw',
			'value' => '$data->pick_up_time',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
        
	//	'updated_time',
        array(
			'name' => 'approve_status',
			'type' => 'raw',
			'value' => '$data->approve_status',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		
		
		/*'user_email',
		'telephone_ext',
		'residence_address',
		'recommended_by',
		'recommended_by_desig',
		'present_route',
		'expected_date',
		'mt_leave_from',
		'mt_leave_to',
		'vehicle_reg_no',
		'vehicletype_id',
		'user_remarks',
		'fleet_remarks',
		'created_by',
		'created_time',
		'approve_status',
		'active',
		*/
	),
)); 
?>
</div>
<div class="center s002 ">
<?php
	echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Commonfleets'])));
?> &nbsp; &nbsp; &nbsp;

<?php //$this->endWidget(); ?>

<?php /*
	$row = $model->findAll();
	foreach ($row as $item)
		echo ($item->user_name)."<br />";*/ ?>

<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#req_container', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Seat Allocation List',
));
?>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>