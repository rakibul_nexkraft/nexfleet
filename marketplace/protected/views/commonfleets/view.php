<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */

$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
	$model->id,
);

//$this->menu=array(
	//array('label'=>' Seat Allocations List', 'url'=>array('index')),
	//array('label'=>'New Seat Allocation', 'url'=>array('create')),
	//array('label'=>'Update Seat Allocation', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Seat Allocation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
//);
?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'Seat Allocations List', 'url'=>array('/commonfleets/index')),                       
                             
                        array('label'=>'Update', 'url'=>array('/commonfleets/update','id'=>$model->id)),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">View Seat Allocation ID#<?php echo $model->id; ?></h4>
        </div>    
    </div>



    <div class="s002">
        <div class="table-responsive bottommargin">
            <?php //$model->vehicletype_id = $model->vehicletypes->type; ?>
            <?php $this->widget('zii.widgets.XDetailView', array(
                'htmlOptions' => array('class' => 'table cart1'), 
                'data'=>$model,
                'attributes'=>array(
                    'group2'=>array(
                        'ItemColumns' => 2,
                        'attributes' => array(
                            'id',						'stoppage_point',
                            'user_pin',					'pick_up_time',
                            'user_name',				'expected_date',
                            'user_level',				'mt_leave_from',
                            'user_dept',				'mt_leave_to',
                            'user_designation',			'vehicle_reg_no',
                            'user_cell',				array('name'=>'vehicletype_id','type'=>'raw','value'=>$model->vehicletypes->type),
                            'user_email',				'user_remarks',
                            'telephone_ext',			'fleet_remarks',
                            'application_type',			'created_by',
                            'residence_address',		'created_time',
                            'preferred_route',			'approve_status',
                            'present_route',			'updated_by',
                            array(),					'updated_time',
                            //'recommended_by',
                            //'recommended_by_desig',		//'active',
                        ),
                    ),
                ),
            ));?>

        </div>

      
    </div>
    <div class="s002">
      <!--<div class="detail-view-right">
            <a id="showHistoryModal" href="#historyModal" role="button" class="btn btn-small btn-primary" data-toggle="modal">History</a>
        </div>-->

    <?php

    if(!(Yii::app()->request->isAjaxRequest))
    {
        $this->widget('application.extensions.print.printWidget', array(
            //'coverElement' => '.container', //main page which should not be seen
            'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
            'printedElement' => '#yw0', //element to be printed
            'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
            'title' => 'Transport Department - Details Seat Allocation',
        ));
    }
    ?>
    </div>
</div>
<div id="historyModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="width: 600px">
    <div class="modal-header">
    </div>
    <div class="modal-body">
        <h4>View Route History of # <?php echo $model->user_pin;?></h4>
        <br/>
        <table class="table table-bordered table-hover history-table" id="defectDetailTable">
            <thead>
            <tr>
                <!--                    <th>ID</th>-->
                <th style="width: 20%">Allocated Date</th>
                <th style="width: 25%">Route</th>
            </tr>
            </thead>
            <tbody>
            <?php
            //            $user_pin = $model->user_pin;
            $commonfleet_id = $model->id;
            $sql = "SELECT routehistory.id, routehistory.user_pin, routehistory.user_name, routehistory.previous_route, routehistory.commonfleets_id, routehistory.created_time FROM tbl_routehistory AS routehistory LEFT JOIN tbl_commonfleets AS commonfleets ON routehistory.commonfleets_id=commonfleets.id WHERE commonfleets.id=$commonfleet_id";
            $results = Yii::app()->db->createCommand($sql)->queryAll();
            if (count($results)<1){
//                print_r($results);
                echo "<tr><td><i>No data</i></td></tr>";
            }
            foreach ($results as $result): ?>
                <tr>
                    <td><?php $date=date_create($result['created_time']); echo date_format($date, "M d, Y")?></td>
                    <td><?php echo $result['previous_route']?></td>
                </tr>
            <?php endforeach;
            ?>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
<!--<script>-->
<!--    $(document).ready(function () {-->
<!--        $('#showHistoryModal').click(function () {-->
<!--            $.ajax({-->
<!--                type:'GET',-->
<!--                url:'< ?php //echo Y ii::app()->createUrl("routehistory/routeHistory");?>//',-->
<!--//                data:{-->
<!--//                    commonfleetId:'< ?php //echo $model->id?>//',-->
<!--//                    userPin:'--><?php ////echo $model->user_pin?><!--//',-->
<!--//                },-->
<!--//                success:function (resp) {-->
<!--//                    var data = $.parseJSON(resp);-->
<!--//                    console.log(data);-->
<!--//                }-->
<!--//            });-->
<!--//        });-->
<!--//    })-->
<!--//</script>-->