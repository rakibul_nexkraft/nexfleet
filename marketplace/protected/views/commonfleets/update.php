<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */

$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>' Seat Allocations List', 'url'=>array('index')),
	array('label'=>'New Seat Allocation', 'url'=>array('create')),
	array('label'=>'View Seat Allocation', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
	array('label'=>'Routes List', 'url'=>array('routes/index')),
);*/
?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'Seat Allocations List', 'url'=>array('/commonfleets/index')),                       
                             
                        array('label'=>'Update', 'url'=>array('/commonfleets/view','id'=>$model->id)),       
                        
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Update Seat Allocation ID# <?php echo $model->id;?></h4>
        </div>    
    </div>



<?php echo $this->renderPartial('_form', array('model'=>$model,'application_type'=>$application_type,'approve_status'=>$approve_status)); ?>