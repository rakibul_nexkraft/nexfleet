<?php
/* @var $this StoppageController */
/* @var $model Stoppage */

$this->breadcrumbs=array(
	'Stoppages'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Stoppage', 'url'=>array('index')),
	array('label'=>'Create Stoppage', 'url'=>array('create')),
);

/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('stoppage-grid', {
		data: $(this).serialize()
	});
	return false;
});
");*/
?>

<h1>Manage Stoppages</h1>
<div class="search-form">
	<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'enableAjaxValidation'=>true,
	'method'=>'get',
)); ?>


<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'route_id'); ?>  <br />
		<?php //echo $form->textField($model,'route_id',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); 
		$this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'route_id',
            'data'=>$model->getAllRouteNo(),
            'options'  => array(
                'style'=>'width:100%',
                'allowClear'=>true,
                'placeholder'=>'Select Route No',
                'minimumInputLength' => 1,
            ),
        ));
		?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'stoppage'); ?>  <br />
		<?php echo $form->textField($model,'stoppage',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
	</div>
</div>





<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">


<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'pickup_time'); ?><br />
		<?php
			$this->widget('ext.jui.EJuiDateTimePicker', array(
                'model'     => $model,
                'attribute' => 'pickup_time',
                'language'=> 'en',//default Yii::app()->language
                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
                
                'options'   => array(
                    //'dateFormat' => 'dd.mm.yy',
                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                    //'ampm' => 'true',
                ),
                'htmlOptions'=>array('autocomplete'=>'off'),
            ));
		?>
	</div>
</div>
</fieldset>






	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

</div>


<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'stoppage-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
	
		array('name'=>'id',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->id),array("view", "id"=>$data->id))',			
		),
		array('name'=>'route_id',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->routeNo($data->route_id)),array("/routes/view", "id"=>$data->route_id))',
			'filter'=>$model->getAllRouteNo(),
		),
		//'route_id',
		'stoppage',
		'pickup_time',
		'created_by',
		'created_time',
		
		'updated_by',
		'updated_time',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
