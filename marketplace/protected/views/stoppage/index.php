<?php
/* @var $this StoppageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Stoppages',
);


?>
<div id="route-request-list">
    <div class="center s002 ">  
        <div class="col_one_sixth"> 
            <div class="toggle toggle-border">  
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php 
                    $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                               
                        array('label'=>'Create Stoppage', 'url'=>array('/stoppage/create')),       
                        
                        ),
                    ));

                    ?>
                </div>
            </div>
        </div>
        <div class="col_five_sixth .col_last"> 
            <h4 class="heading-custom">Stoppages</h4>
        </div>    
    </div>


<div class="center s002 ">
<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'enableAjaxValidation'=>true,
    'method'=>'get',
)); ?>

	<div class="inner-form">
        <div class='col_one_fourth'>
            <label>Route No</label>
            <div class="input-field fouth-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
                        </path>
                    </svg>
                </div>                                                          
                <?php $route=Routes::model()->findAll(array('order' => 'id ASC'));?>
				<select data-trigger="" name="Stoppage[route_no]">
                    <option value=''>A Route Select </option>
                    <?php foreach($route as $key=>$value){
                    	
                    		 echo "<option value='".$value['id']."'>".$value['route_no']."</option>";
                    	
                       
                    }
                    ?>
                </select>
               
                                                            
            </div>
        </div>
        <div class='col_one_fourth'> 
          <label>Stoppage</label>
           <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>                                                           
                <?php echo $form->textField($model,'stoppage',array('placeholder'=>'Stoppage'));
                ?>
                                                            
            </div>
        </div>
   
        <div class='col_one_fourth'>
           <label>Pickup Time</label>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>                                                           
               <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
				                'model'     => $model,
				                'attribute' => 'pickup_time',
				                'language'=> 'en',//default Yii::app()->language
				                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
				                
				                'options'   => array(
				                    //'dateFormat' => 'dd.mm.yy',
				                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
				                    //'ampm' => 'true',
				                ),
				                'htmlOptions'=>array('autocomplete'=>'off'),
				            ));
						
                ?>
                                                            
            </div>
        </div>
        <div class='col_one_fourth'>
                 <label>Search</label>
                <div class='input-field first-wrap'>
                      <?php echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button'));?>        
                </div>
      </div>
    </div> 
<?php $this->endWidget(); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'stoppage-grid',
	'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),       
    'rowCssClass'=>array('cart_item'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'pager' => array(
    'firstPageLabel'=>'First',
    'lastPageLabel'=>'Last',
     ),
	'columns'=>array(
	
		array('name'=>'id',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->id),array("view", "id"=>$data->id))',
			'htmlOptions'=>array('class'=>'cart-product-price'),
             'headerHtmlOptions'=>array('class'=>'cart-product-price'),			
		),
		array('name'=>'route_id',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->routeNo($data->route_id)),array("/routes/view", "id"=>$data->route_id))',
			'filter'=>$model->getAllRouteNo(),
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		//'route_id',
		array(
			'name'=>'stoppage',
			'type'=>'raw',
			'value'=>'$data->stoppage',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		array(
			'name'=>'pickup_time',
			'type'=>'raw',
			'value'=>'$data->pickup_time',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		
		array(
			'name'=>'created_by',
			'type'=>'raw',
			'value'=>'$data->created_by',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		
		array(
			'name'=>'created_time',
			'type'=>'raw',
			'value'=>'$data->created_time',
			'htmlOptions'=>array('class'=>'cart-product-price'),
            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
		),
		
		
		//'updated_by',
		//'updated_time',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); 
?>
	<div class="center s002 ">
	<?php
	echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Stoppage'])));
	?>
	</div>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>


<script>
    const choices = new Choices('[data-trigger]',
    {
        searchEnabled: true,
        itemSelectText: '@',
    });

</script>