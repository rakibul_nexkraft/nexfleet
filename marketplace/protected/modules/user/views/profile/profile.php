<style>
    .table tr td span {
        display: inline-block;
        width: 150px;
        font-weight: bold;
        color: #333;
    }
    .table tr td span i {
        position: relative;
        top: 1px;
        width: 14px;
        text-align: center;
        margin-right: 7px;
    }
</style>


<?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
<div class="style-msg successmsg">
    <div class="sb-msg">
        <button type="button" class="close" data-dismiss="successmsg" aria-hidden="true">×</button>
        <i class="icon-thumbs-up"></i><?php echo Yii::app()->user->getFlash('profileMessage'); ?>
    </div>
</div>
<?php endif; ?>
<div id="portfolio" class="portfolio-1 clearfix" style="position: relative;margin-top: 20px">
    <article class="portfolio-item pf-media pf-icons clearfix nobottomborder nobottompadding" style="left: 0px; top: 0px;">
        <div class="portfolio-desc">
            <h3><a href="#"><?php echo Yii::app()->user->firstname. " " .Yii::app()->user->lastname; ?></a></h3>
            <span><?php echo Yii::app()->user->designation; ?></span>
            <span><?php echo Yii::app()->user->department; ?></span>

            <!--<span class="topmargin-sm nobottommargin" style="font-size: 12px">RANKING</span>
            <div class="entry-c">
                <ul class="entry-meta">
                    <li class="color">4.6</li>
                    <li><i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star3"></i> <i class="icon-star-half-full"></i></li>
                </ul>
            </div>-->
            <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, quaerat. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, quaerat.</p>-->


            <!--<ul class="iconlist">
                <li><i class="icon-ok"></i> <strong>Created Using:</strong> PHP, HTML5, CSS3</li>
                <li><i class="icon-ok"></i> <strong>Completed on:</strong> 12th January, 2014</li>
                <li><i class="icon-ok"></i> <strong>By:</strong> John Doe</li>
            </ul>
            <a href="portfolio-single.html" class="button button-3d noleftmargin">Launch Project</a>-->
        </div>
    </article>
</div>
<!--<div class="col-md-6" style="text-align: right;">
    <br>
    <a href="portfolio-single.html" class="button button-3d noleftmargin button-mini">Edit Profile</a>
    <a href="portfolio-single.html" class="button button-3d noleftmargin button-mini">Change Password</a>
</div>-->

<!--<div class="col_half topmargin nobottommargin">
    <div class="fancy-title">
        <h4>Description</h4>
    </div>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, dolores, facere, corrupti delectus ex quidem adipisci tempore.</p>
</div>

<div class="clear"></div>-->

<div class="col_full portfolio-single-content nobottommargin topmargin">

    <div class="fancy-title uppercase">
        <h6>Contact Information</h6>
    </div>
    <ul class="portfolio-meta bottommargin">
        <li><span><i class="icon-user"></i>Phone:</span> <?php echo Yii::app()->user->phone; ?></li>
        <li><span><i class="icon-calendar3"></i>Address:</span> <?php echo Yii::app()->user->address; ?></li>
        <li><span><i class="icon-lightbulb"></i>Email:</span> <?php echo $model->email; ?></li>
    </ul>

</div>
<div class="col_full portfolio-single-content nobottommargin topmargin">
    <div class="fancy-title uppercase">
        <h6>Basic Information</h6>
    </div>
    <ul class="portfolio-meta bottommargin">
        <li><span><i class="icon-user"></i>Member From:</span> <?php $date = date_create($model->create_at); echo date_format($date, "dS F Y"); ?></li>
        <!--<li><span><i class="icon-lightbulb"></i>Date of Birth:</span> < ?php echo CHtml::encode($model->create_at); ?></li>-->
    </ul>
</div>

<div class="line"></div>

<div class="col_full">
    <a href="<?php echo Yii::app()->baseUrl; ?>/index.php/user/profile/edit" class="button button-3d noleftmargin button-mini">Edit Profile</a>
    <a href="<?php echo Yii::app()->createUrl("/user/profile/changepassword"); ?>" class="button button-3d noleftmargin button-mini">Change Password</a>
</div>