<?php
$this->breadcrumbs=array(
	UserModule::t("Users"),
);
if(UserModule::isAdmin()) {
	$this->layout='//layouts/column2';
	$this->menu=array(
	    array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
	    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
	);
}
?>

<!--<h4>< ?php echo UserModule::t("List User"); ?></h4>-->

<!--<div class="heading-block fancy-title nobottomborder topmargin title-bottom-border">
    <h4><a class="button button-3d button-mini button-rounded button-green" href="<?php /*echo Yii::app()->createUrl("/user/admin/create"); */?>">Create User</a> <?php /*echo UserModule::t("List of Users"); */?></h4>
    <div class="clear"></div>
</div>-->
<div class="heading-block fancy-title nobottomborder topmargin theme-bottom-border">
    <div class="fleft">
        <h4>List of Users</h4>
    </div>
    <div class="fright">
        <a class="button button-3d button-mini button-rounded button-green" href="<?php echo Yii::app()->createUrl("user/admin/create"); ?>">Create User</a>
    </div>
    <div class="clear"></div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$model->search(),
    'id'=>'search',
    'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),
    'filter' => $model,
    'rowCssClass'=>array('cart_item'),
//    'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id))',
		),
		array(
            'name' => 'create_at',
            'type'=>'raw',
            'filter'=>false,
        ),
		array(
            'name' => 'lastvisit_at',
            'type'=>'raw',
            'filter'=>false,
        ),
        array(
            'header' => 'Action',
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons'=>array(
                'update'=>array(
                    'url'=>'Yii::app()->createUrl("user/admin/update", array("id"=>$data->id))',
                )
            )
        )
	),
)); ?>
