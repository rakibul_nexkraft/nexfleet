<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('index'),
	$model->username,
);
$this->layout='//layouts/column2';
$this->menu=array(
    array('label'=>UserModule::t('List User'), 'url'=>array('index')),
);
?>
<div class="heading-block fancy-title nobottomborder title-bottom-border">
    <h4><a class="button button-3d button-mini button-rounded button-green" href="<?php echo $model->picmarket_user?Yii::app()->createUrl("/user/user/sisterconcernuser"):Yii::app()->createUrl("/user/user"); ?>">Back to List</a> <?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname.' [Pin: '.$model->username.']'; ?></h4>
    <div class="clear"></div>
</div>


<div class="col_three_fourth">

    <?php

    // For all users
    $attributes = array(
        'username',
    );

    $profileFields=ProfileField::model()->forAll()->sort()->findAll();
    if ($profileFields) {
        foreach($profileFields as $field) {
            array_push($attributes,array(
                'label' => UserModule::t($field->title),
                'name' => $field->varname,
                'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),

            ));
        }
    }
    array_push($attributes,
        'create_at',
        array(
            'name' => 'lastvisit_at',
            'value' => (($model->lastvisit_at!='0000-00-00 00:00:00')?$model->lastvisit_at:UserModule::t('Not visited')),
        )
    );

    $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>$attributes,
        'htmlOptions'=>array('class'=>'table table-hover')
    ));

    ?>
</div>
<div class="col_one_fourth col_last">
    <a class="thumbnail">
        <img alt="100%x180" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/extras/200x200.gif" style="height: 180px; width: 100%; display: block;">
    </a>
</div>