<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Registration");
$this->breadcrumbs = array(
    UserModule::t("Registration"),
);
?>
<section id="content">

    <div class="content-wrap nopadding">


        <div class="section nobg  nopadding nomargin">
            <div class="container  divcenter clearfix">

                <div class="col_full center">
                    <a href="#"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/iFleet logo difference-16.png" alt="Brac iFleet User Logo"></a>
                </div>
                <div class="clearfix"></div>
                <div class="col_full center bottommargin">
                    <h3 class="nobottommargin"><?php echo UserModule::t("Registration"); ?></h3>
                    <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
                </div>
                <div class="panel panel-default divcenter noradius noborder">
                    <?php if (Yii::app()->user->hasFlash('registration')): ?>
                        <?php echo Yii::app()->user->getFlash('registration'); ?>
                    <?php else: ?>
                    <div class="panel-body">
                        <?php $form = $this->beginWidget('UActiveForm', array(
                            'id' => 'registration-form',
                            'enableAjaxValidation' => true,
                            'disableAjaxValidationAttributes' => array('RegistrationForm_verifyCode'),
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                            ),
                            'htmlOptions' => array('enctype' => 'multipart/form-data'),
                        ));
                        ?>

                        <?php echo $form->errorSummary(array($model, $profile)); ?>
                        <input type="hidden" name="RegistrationForm[picmarket_user]" value="1">
                        <div class="col_half">
                            <?php echo $form->labelEx($model, 'username'); ?>

                            <?php echo $form->textField($model, 'username', array('class' => 'form-control not-dark', 'required' => 'required')); ?>
                            <?php echo $form->error($model, 'username'); ?>
                        </div>
                        <div class="col_half col_last">
                            <?php echo $form->labelEx($model, 'email'); ?>
                            <?php echo $form->textField($model, 'email', array('class' => 'form-control not-dark', 'required' => 'required')); ?>
                            <?php echo $form->error($model, 'email'); ?>
                        </div>
                        <div class="col_half">
                            <?php echo $form->labelEx($model, 'password'); ?>
                            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control not-dark', 'required' => 'required')); ?>
                            <?php echo $form->error($model, 'password'); ?>
                            <p class="hint">
                                <?php echo UserModule::t("Minimal password length 4 symbols."); ?>
                            </p>
                        </div>

                        <div class="col_half col_last">
                            <?php echo $form->labelEx($model, 'verifyPassword'); ?>
                            <?php echo $form->passwordField($model, 'verifyPassword', array('class' => 'form-control not-dark', 'required' => 'required')); ?>
                            <?php echo $form->error($model, 'verifyPassword'); ?>
                        </div>


                        <?php
                        $profileFields = $profile->getFields();
                        if ($profileFields) {
                            foreach ($profileFields as $key=>$field) {
                                ?>
                                <div class="col_half <?php echo $key%2?'col_last':''; ?>">
                                    <?php echo $form->labelEx($profile, $field->varname); ?>
                                    <?php
                                    if ($widgetEdit = $field->widgetEdit($profile)) {
                                        echo $widgetEdit;
                                    } elseif ($field->range) {
                                        echo $form->dropDownList($profile, $field->varname, Profile::range($field->range), array('class' => 'form-control not-dark'));
                                    } elseif ($field->field_type == "TEXT") {
                                        echo $form->textArea($profile, $field->varname, array('class' => 'form-control not-dark', 'required' => 'required'));
                                    } else {
                                        echo $form->textField($profile, $field->varname, array('class' => 'form-control not-dark', 'required' => 'required'));
                                    }
                                    ?>
                                    <?php echo $form->error($profile, $field->varname); ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                        <div class="clear"></div>
                        <?php if (UserModule::doCaptcha('registration')): ?>
                            <div class="col_half col_last">
                                <?php echo $form->labelEx($model, 'verifyCode'); ?>

                                <?php $this->widget('CCaptcha'); ?>
                                <?php echo $form->textField($model, 'verifyCode', array('class' => 'form-control not-dark')); ?>
                                <?php echo $form->error($model, 'verifyCode'); ?>

                                <p class="hint"><?php echo UserModule::t("Please enter the letters as they are shown in the image above."); ?>
                                    <br/><?php echo UserModule::t("Letters are not case-sensitive."); ?></p>
                            </div>
                        <?php endif; ?>
                        <div class="clear"></div>
                        <div class="col_full nobottommargin">
                            <?php echo CHtml::submitButton(UserModule::t("Register"), array('class' => 'button button-3d button-black nomargin')); ?>

                        </div>

                        <?php $this->endWidget(); ?>
                        <?php endif; ?>

                    </div>

                    <div class="row center dark">
                        <small>Copyrights &copy; All Rights Reserved by Brac.</small>
                    </div>

                </div>
            </div>
        </div>


</section><!-- #content end -->
