<div class="heading-block fancy-title nobottomborder topmargin theme-bottom-border">
    <div class="fleft">
        <h4>Pending Requests</h4>
    </div>
    <div class="fright">
        <a class="button button-3d button-mini button-rounded button-green" href="<?php echo Yii::app()->createUrl("user/user/sisterconcernuser"); ?>">Sister Concern Users</a>
    </div>

    <div class="clear"></div>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$model->search_request_pending(),
    'id'=>'search-request-pending',
    'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),
    'filter'=>$model,

    'rowCssClass'=>array('cart_item'),
//    'dataProvider'=>$model->search(),
    'columns'=>array(
        array(
            'name' => 'username',
            'type'=>'raw',
            'value' => function($data){
                return CHtml::link(CHtml::encode($data->username),array("user/view","id"=>$data->id));
            }
        ),
        array(
            'name' => 'create_at',
            'type'=>'raw',
            'filter'=>false,
        ),
        array(
            'name' => 'lastvisit_at',
            'type'=>'raw',
            'filter'=>false,
        ),
        array(
            'header' => 'Action',
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons'=>array(
                'update'=>array(
                    'url'=>'Yii::app()->createUrl("user/admin/update", array("id"=>$data->id))',
                )
            )
        )
    ),
)); ?>
