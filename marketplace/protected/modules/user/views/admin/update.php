<?php
$this->breadcrumbs=array(
	(UserModule::t('Users'))=>array('admin'),
	$model->username=>array('view','id'=>$model->id),
	(UserModule::t('Update')),
);
$this->menu=array(
    array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
    array('label'=>UserModule::t('View User'), 'url'=>array('view','id'=>$model->id)),
    array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);
?>
    <div class="heading-block fancy-title nobottomborder topmargin title-bottom-border">
        <h4>Update User: <?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname.' / Pin: '.$model->username.''; ?></h4>
        <div class="clear"></div>
    </div>

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>