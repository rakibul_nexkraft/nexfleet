<?php

class LogoutController extends Controller
{
	public $defaultAction = 'logout';
	
	/**
	 * Logout the current user and redirect to returnLogoutUrl.
	 */
	public function actionLogout()
	{
		if(Yii::app()->user->isAdmin()==1){

			Yii::app()->user->logout();	
			
			$url=explode('/pickndrop', Yii::app()->request->baseUrl);	
			$this->redirect($url[0]."/index.php/user/logout");
		}
		else{
			Yii::app()->user->logout();		
			$this->redirect(Yii::app()->controller->module->returnLogoutUrl);
	    }
	}

}