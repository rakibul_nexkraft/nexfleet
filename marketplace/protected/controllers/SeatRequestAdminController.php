<?php
date_default_timezone_set("Asia/Dhaka");
//Yii::app()->theme = 'pick';
class SeatRequestAdminController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	//public $layout='//layouts/columnpick';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','zone','callHrUser','excel'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','statusChange','route'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SeatRequestAdmin;
		$model->created_by = Yii::app()->user->username;           
        $model->created_time = date("Y-m-d H:i:s");

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequestAdmin']))
		{
			$model->attributes=$_POST['SeatRequestAdmin'];
			if($model->status==1){//For Approved
				$route=Routes::model()->findByPk($model->route_id);
				$route_no=$route['route_no'];
				$vehicle_reg_no=$route['vehicle_reg_no'];
				$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);				
				$actual_seat=$route['actual_seat'];
				$occupied_seat=Routes::model()->borderPass($route_no);
				$av_seat=$actual_seat-$occupied_seat;
				if($av_seat>0){// seat available
					$this->assign($model);
					$model->queue=-1;
				}

				else{// seat not available
					$sql="SELECT max(queue) as maxq FROM tbl_seat_request WHERE route_id='".$model->route_id."' GROUP BY route_id";
					$sql_rs=Yii::app()->db->createCommand($sql)->queryRow(); 
					$model->queue=$sql_rs['maxq']+1;
				}
        	
        	}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$previous_status=$model->status;
		$model->updated_time=date("Y-m-d H:i:s");
		$model->updated_by=Yii::app()->user->username; 
		$route=Routes::model()->findByPk($model->route_id);
		$route_no=$route['route_no'];
		$model->zone=$route['zone_id'];
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequestAdmin']))
		{
			$model->attributes=$_POST['SeatRequestAdmin'];

			if($model->status==1 && $model->queue!=-1){ //Approved and not assign				
				$vehicle_reg_no=$route['vehicle_reg_no'];
				$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);
				
				$actual_seat=$route['actual_seat'];
				$occupied_seat=Routes::model()->borderPass($route_no);
				$av_seat=$actual_seat-$occupied_seat;
				

				if($av_seat>0){
					$this->assign($model);
					$model->queue=-1;
				}

				else{
					$sql="SELECT max(queue) as maxq FROM tbl_seat_request WHERE route_id='".$model->route_id."' GROUP BY route_id";
					$sql_rs=Yii::app()->db->createCommand($sql)->queryRow(); 
					$model->queue=$sql_rs['maxq']+1;
				}

				
			}
			else if($model->status!=1 && $model->queue==-1){// change status approved to other, but previous assign
				$sql="DELETE FROM tbl_commonfleets WHERE user_pin='".$model->user_pin."'";
				$sql_rs=Yii::app()->db->createCommand($sql)->execute(); //Delete assign
				$model->queue=0; //not queue and not assign 
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>0 order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){
					$i=0;
					foreach ($sql_rs as $key => $value) {
						if($i==0){// first queue assign
							$model_assign=$this->loadModel($value['id']);							
							$model_assign->updated_time=date("Y-m-d H:i:s");
							$model_assign->updated_by=Yii::app()->user->username; 
							$this->assign($model_assign);
							$model_assign->queue=-1;
							$model_assign->save();
							unset($model_assign);
						}
						else{ //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);

						}
						$i++;
					}
				}

			}
			else if($model->status!=1 && $model->queue>0){
				$q=$model->queue;
				$model->queue=0;
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>$q order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){
					
					foreach ($sql_rs as $key => $value) {
						
					 //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);

						
						
					}
				}

			}
			else{//same as request
				$model->queue=0;
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function assign($model){
		$this->assignRequest($model);
		$route=Routes::model()->findByPk($model->route_id);
		$vehicle_reg_no=$route['vehicle_reg_no'];
		$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);
		$route_no=$route['route_no'];
		$stoppage=Stoppage::model()->findByPk($model->stoppage_id);
		$model1=new Commonfleets;
		$model1->user_pin=$model->user_pin;
		$model1->user_name=$model->user_name;
		$model1->user_level=$model->user_level;
		$model1->user_dept=$model->user_department;
		$model1->user_designation=$model->user_designation;
		$model1->user_cell=(int) trim($model->user_cell);
		$model1->user_email=trim($model->email);
		$model1->telephone_ext=0;
		$model1->application_type="Pick and Drop";					
		$model1->residence_address=$model->residence_address;
		$model1->recommended_by='';
		$model1->recommended_by_desig='';
		$model1->previous_route='';
		$model1->preferred_route='';
		$model1->present_route=$route_no;
		$model1->expected_date=$model->expected_date;
		$model1->mt_leave_from="0000-00-00";
		$model1->mt_leave_to="0000-00-00";
		$model1->vehicle_reg_no=$vehicle_reg_no;
		$model1->vehicletype_id=$vehicle['vehicletype_id'];
		$model1->user_remarks=$model->remarks;
		$model1->fleet_remarks='';
		$model1->created_by=Yii::app()->user->username;
		$model1->created_time=date("Y-m-d H:i:s");
		$model1->approve_status="Approve";
		$model1->stoppage_point=$stoppage['stoppage'];
		$model1->pick_up_time=$stoppage['pickup_time'];					
		$model1->active=0;		
		$model1->save();
	}	

	public function assignRequest($model){
		$sql="DELETE FROM tbl_commonfleets WHERE user_pin='".$model->user_pin."'";
		$sql_rs=Yii::app()->db->createCommand($sql)->execute();
		
		$sql="UPDATE  tbl_seat_request SET status=-1, queue=0 WHERE user_pin='".$model->user_pin."' AND status=1 AND queue=-1";
		$sql_rs_seat=Yii::app()->db->createCommand($sql)->execute();	 
		
		
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{ 
		//$dataProvider=new CActiveDataProvider('SeatRequestAdmin');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$model=new SeatRequestAdmin('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequestAdmin']))
			$model->attributes=$_GET['SeatRequestAdmin'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SeatRequestAdmin('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequestAdmin']))
			$model->attributes=$_GET['SeatRequestAdmin'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SeatRequestAdmin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seat-request-admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionCallHrUser()
    {
    	$user_pin = $_REQUEST['SeatRequestAdmin']['user_pin'];
    	


        $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
        

        $staffInfo = CJSON::decode(file_get_contents($uri)); 
        

        echo CJSON::encode($staffInfo[0]);die;
       
    }
	public function actionZone(){
		$id=$_POST['id'];
		$sql="SELECT id,route_no,route_detail from tbl_routes where zone_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		//var_dump($aql_rs);exit();
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}
	}
	public function actionStatusChange(){
		$id=$_POST['id'];
		$value=$_POST['value'];
		
		$model=$this->loadModel($id);
		$updated_time=date("Y-m-d H:i:s");
		$updated_by=Yii::app()->user->username; 
		
		

		if($model->status==4){
			$sql="SELECT * FROM tbl_seat_request WHERE  status=1  AND user_pin='".$model->user_pin."'";
			$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
			if($sql_rs){
				$sql_update="UPDATE tbl_seat_request SET status=0 WHERE id='".$sql_rs['id']."'";
				$rs_sql_update=Yii::app()->db->createCommand($sql_update)->execute();
			}
		}
		
		$model->status=$_POST['value'];
		if($model->status==1 && $model->queue!=-1){ //Approved and not assign
				$route=Routes::model()->findByPk($model->route_id);
				$vehicle_reg_no=$route['vehicle_reg_no'];
				$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);
				$route_no=$route['route_no'];
				$actual_seat=$route['actual_seat'];
				$occupied_seat=Routes::model()->borderPass($route_no);
				$av_seat=$actual_seat-$occupied_seat;
				

				if($av_seat>0){
					$this->assign($model);
					$model->queue=-1;
				}

				else{
					$sql="SELECT max(queue) as maxq FROM tbl_seat_request WHERE route_id='".$model->route_id."' GROUP BY route_id";
					$sql_rs=Yii::app()->db->createCommand($sql)->queryRow(); 
					$model->queue=$sql_rs['maxq']+1;
				}

				
		}
		else if($model->status!=1 && $model->queue==-1){// change status approved to other, but previous assign
				$sql="DELETE FROM tbl_commonfleets WHERE user_pin='".$model->user_pin."'";
				$sql_rs=Yii::app()->db->createCommand($sql)->execute(); //Delete assign
				$model->queue=0; //not queue and not assign 
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>0 order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){
					$i=0;
					foreach ($sql_rs as $key => $value) {
						if($i==0){// first queue assign
							$model_assign=$this->loadModel($value['id']);							
							$model_assign->updated_time=date("Y-m-d H:i:s");
							$model_assign->updated_by=Yii::app()->user->username; 
							$this->assign($model_assign);
							$model_assign->queue=-1;
							$model_assign->save();
							unset($model_assign);
						}
						else{ //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);

						}
						$i++;
					}
				}

		}
		else if($model->status!=1 && $model->queue>0){
				$q=$model->queue;
				$model->queue=0;
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>$q order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){					
					foreach ($sql_rs as $key => $value) {						
					 //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);					
						
					}
				}

			}
		else{//same as request
				$model->queue=0;
		}



		//$sql="UPDATE tbl_seat_request SET status=$value,updated_time='$updated_time',updated_by='$updated_by'  WHERE id=$id";
		
		//$sql_rs=Yii::app()->db->createCommand($sql)->execute();
		if($model->save()){
			echo "Data save successfully";
		}
		else{
			echo "Try again!";
		}
	}
public function actionRoute(){
		$id=$_POST['id']; 
		$sql="SELECT id,pickup_time,stoppage from tbl_stoppage where route_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}

	}
public function actionExcel()
	{
		$model = new SeatRequestAdmin ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Seat Request Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Zones_Information',
            //'stream'=>false,
           'columns'=>array(
           		
				'id',	
				'user_pin',
				'user_name',
				'user_department',	
				'user_level',	
				'user_cell',
				'email',	
				array(	
							'name'=>'route_id',	
							'type' => 'raw',			
							'value'=>'SeatRequest::routeDetail($data->route_id,3)',	
							
						),
				'expected_date',
	
		
		
		
	
		array(	
					'name'=>'queue',	
					'type' => 'raw',			
					'value'=>'$data->queueCheck($data->queue,$data->status)',
					
				),
		array(	
					'name'=>'status',	
					'type' => 'raw',			
					'value'=>'$data->status==0?"Pending":($data->status==1?"Approved":($data->status==3?"Cancel Request":($data->status==-1?"Cancel":"Not Found")))',
					
				),
		
			),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
}
