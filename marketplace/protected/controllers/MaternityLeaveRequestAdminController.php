<?php
date_default_timezone_set("Asia/Dhaka");
class MaternityLeaveRequestAdminController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','mrUser','maternityAssignUpdated'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','statusChange','assignUser'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MaternityLeaveRequestAdmin;
		$model->created_by = Yii::app()->user->username;           
        $model->created_time = date("Y-m-d H:i:s");

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MaternityLeaveRequestAdmin']))
		{
			$model->attributes=$_POST['MaternityLeaveRequestAdmin'];
			if($model->save()){
				if($model->status==1){
					$sql="UPDATE tbl_commonfleets SET mt_leave_from='$model->from_date',mt_leave_to='$model->to_date' WHERE user_pin='$model->user_pin'";					
				}
				else{
					$sql="UPDATE tbl_commonfleets SET mt_leave_from='',mt_leave_to='' WHERE user_pin='$model->user_pin'";					
					}
				$sql_rs=Yii::app()->db->createCommand($sql)->execute();
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->update_time=date("Y-m-d H:i:s");
		$model->updated_by=Yii::app()->user->username; 

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MaternityLeaveRequestAdmin']))
		{
			$model->attributes=$_POST['MaternityLeaveRequestAdmin'];

			if($model->save()){
				if($model->status==1){
					$sql="UPDATE tbl_commonfleets SET mt_leave_from='$model->from_date',mt_leave_to='$model->to_date' WHERE user_pin='$model->user_pin'";					
				}
				else{
					$sql="UPDATE tbl_commonfleets SET mt_leave_from='',mt_leave_to='' WHERE user_pin='$model->user_pin'";					
					}
				$sql_rs=Yii::app()->db->createCommand($sql)->execute();
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('MaternityLeaveRequestAdmin');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$model=new MaternityLeaveRequestAdmin('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaternityLeaveRequestAdmin']))
			$model->attributes=$_GET['MaternityLeaveRequestAdmin'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MaternityLeaveRequestAdmin('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaternityLeaveRequestAdmin']))
			$model->attributes=$_GET['MaternityLeaveRequestAdmin'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=MaternityLeaveRequestAdmin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='maternity-leave-request-admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionStatusChange(){
		 $id=$_POST['id'];
		
		$value=$_POST['value'];
		
		$model=$this->loadModel($id);
		$updated_time=date("Y-m-d H:i:s");
		$updated_by=Yii::app()->user->username; 
		$model->status=$_POST['value'];
		
		if($model->save()){
			if($model->status==1){
					 $sql="UPDATE tbl_commonfleets SET mt_leave_from='$model->from_date', mt_leave_to='$model->to_date' WHERE user_pin='$model->user_pin'";					
				}
			else{
				 $sql="UPDATE tbl_commonfleets SET mt_leave_from='', mt_leave_to='' WHERE user_pin='$model->user_pin'";					
			}
			$sql_rs=Yii::app()->db->createCommand($sql)->execute();			
			echo "Data save successfully";
		}
		else{
			echo "Try again!";
		}
	}
	public function actionAssignUser($id){
		$model=$this->loadModel($id);
		$route_id=$model->userRouteID($model->user_pin);

		$seat_request=new CActiveDataProvider('SeatRequestAdmin', array(
            'criteria'=>array(                
                'condition'=>'status = 1 AND queue>0 AND route_id="'.$route_id.'"',
                'order'=>'id DESC'
            ),
            'pagination'=>array('pageSize'=>10),
        ));
		$this->render('assignUser',array(
			'model'=>$model,
			'seat_request'=>$seat_request
		));

	}
	public function actionMrUser($id){
		$model=MaternityLeaveRequestAdmin::model()->findByPk($id);	
		$this->render('mrUser',array(
			 'model'=>$model,		
		));
	}
	public function actionMaternityAssignUpdated(){
		 $id=$_POST['id'];

		 $model=MaternityLeaveAssign::model()->findByPk($id);
		 $model->status=1;
		 $model->updated_by=Yii::app()->user->username;
		 $model->updated_time=date("Y-m-d H:i:s");

		 $sql_previous_maternity="SELECT sr.id,sr.route_id,sr.queue FROM tbl_maternity_leave_assign as ma INNER JOIN tbl_seat_request as sr ON ma.seat_request_id=sr.id WHERE ma.meternity_request_id='".$model->meternity_request_id."' AND ma.status=1 AND sr.status=2";
		 $rs_previous_maternity=Yii::app()->db->createCommand($sql_previous_maternity)->queryRow();
		 
		 if($rs_previous_maternity){
		 	$sql_previous_user_que="SELECT * FROM tbl_seat_request WHERE route_id='".$rs_previous_maternity["route_id"]."' AND status=1 AND queue>='".$rs_previous_maternity["queue"]."'";
		 	 $sql_previous_user_que=Yii::app()->db->createCommand($sql_previous_user_que)->queryAll();
		 	 if(count($sql_previous_user_que)>0){
		 	 	foreach ($sql_previous_user_que as $key => $value) {
		 	 		 $sql_previous_user_que_update="UPDATE  tbl_seat_request SET queue='".($value['queue']+1)."' WHERE id='".$value['id']."'";
		 			$rs_previous_user_que_update=Yii::app()->db->createCommand($sql_previous_user_que_update)->execute();
		 	 	}
		 	 }
		 	 $sql_user_maternity_previous="UPDATE tbl_seat_request SET status=1 WHERE id='".$rs_previous_maternity["id"]."'";
		 	 $rs_previous_user_que_update=Yii::app()->db->createCommand($sql_user_maternity_previous)->execute();
		 	 

		}

		 $sql_maternity_user="UPDATE tbl_maternity_leave_assign SET status=0 WHERE meternity_request_id='".$model->meternity_request_id."' AND status=1";
		 $rs_maternity_user=Yii::app()->db->createCommand($sql_maternity_user)->execute();

		 $model_request=MaternityLeaveRequestAdmin::model()->findByPk($model->meternity_request_id);

		 $model_request->maternity_user_pin=$model->maternity_user_pin;

		 $model_request->updated_by=Yii::app()->user->username;

		 $model_request->update_time=date("Y-m-d H:i:s");

		 $sql="SELECT * FROM tbl_commonfleets WHERE user_pin='".$model_request['user_pin']."'";
		 $sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		 $sql_update="UPDATE tbl_commonfleets SET maternity_assign_id='".$model_request->id."' WHERE user_pin='".$model_request['user_pin']."'";
		 $rs_sql_update=Yii::app()->db->createCommand($sql_update)->execute();

		 $model_request->maternity_id=$sql_rs['id'];

		 $sql_seat_request="SELECT * FROM tbl_seat_request WHERE id='".$model->seat_request_id."'";
		 $rs_seat_request=Yii::app()->db->createCommand($sql_seat_request)->queryRow();

		 $sql_seat_request_waiting_list="SELECT * FROM tbl_seat_request WHERE route_id='".$rs_seat_request['route_id']."' AND status=1 AND queue>'".$rs_seat_request['queue']."'";
		 $rs_seat_request_waiting_list=Yii::app()->db->createCommand($sql_seat_request_waiting_list)->queryAll();
		 

		if(count($rs_seat_request_waiting_list)>0){
		 	foreach ($rs_seat_request_waiting_list as $key => $value) {
		 	 $sql_update="UPDATE  tbl_seat_request SET queue='".($value['queue']-1)."' WHERE id='".$value['id']."'";
		 	$rs_sql_update=Yii::app()->db->createCommand($sql_update)->execute();
		 	}
		 }

		
		 $sql_update1="UPDATE  tbl_seat_request SET status=2 WHERE id='".$model->seat_request_id."'";
		 $rs_sql_update1=Yii::app()->db->createCommand($sql_update1)->execute();
		
		 $model->user_pin=$model_request->user_pin;
		 if($model->save() && $model_request->save() ){
		 	echo "<h3>Data Save Successfully!</h3>";
		 }
		 else{
		 	var_dump($model->getErrors());
		 	echo "<h3>Try Aganin!</h3>";
		 }
		
	}
}
