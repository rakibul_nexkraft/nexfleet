<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions

                'actions'=>array('test', 'pickAndDropStats', 'marketplaceStats', 'requisitionStats', 'nextDayRequisitions','autoLogin'),
                'users'=>array('*'),

            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'contact', 'error', 'page', 'getVehicleList', 'topDataAvailable', 'topDataOnDuty', 'topDataWorkshop', 'topDataStaffPickDrop', 'searchByCriteria', 'getRequisitionDetail', 'getDefectDetail'),
                'users' => array('@'),
            ),
            /*array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),*/
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'




        if (Yii::app()->user->isAdmin() =='1'){
            $total_selling = Yii::app()->db->createCommand('SELECT count(*) total_selling FROM tbl_marketplace t WHERE t.release_date="'.date('Y-m-d').'"')->queryRow();
            $total_booking = Yii::app()->db->createCommand('SELECT count(*) total_booking FROM tbl_booking t WHERE t.release_date="'.date('Y-m-d').'"')->queryRow();

            $user_data['total_selling'] = $total_selling['total_selling'];
            $user_data['total_booking'] = $total_booking['total_booking'];

            $criteria_sell = new CDbCriteria;
            $criteria_sell->select = 't.*, b.id booking_id';
            $criteria_sell->join = 'LEFT JOIN tbl_booking b ON b.marketplace_id=t.id';
            $criteria_sell->order = "t.created_time DESC";
            $criteria_sell->limit=5;

            $recent_sell = Marketplace::model()->findAll($criteria_sell);

            $criteria_booking = new CDbCriteria;
            $criteria_booking->select = 't.*';
            $criteria_booking->join = 'LEFT JOIN tbl_marketplace m ON t.marketplace_id = m.id';
            $criteria_booking->order = "t.created_time DESC";
            $criteria_booking->limit=5;

            $recent_booking = Booking::model()->findAll($criteria_booking);
            $this->render('index_admin', array('recent_sell' => $recent_sell, 'recent_booking' => $recent_booking, 'user_data' => $user_data));
        } else {
            $cs=Yii::app()->clientScript;
            $cs->scriptMap=array(
                'pnd-dashboard.css'=>Yii::app()->baseUrl.'/../common/assets/css/pnd-dashboard.css',
            );
            $cs->registerCssFile('pnd-dashboard.css');

            $total_sell = Yii::app()->db->createCommand('SELECT COALESCE(SUM(t.price),0) total_sell FROM tbl_marketplace t WHERE t.username="'.Yii::app()->user->username.'" AND t.status = 1')->queryRow();
            $total_purchase = Yii::app()->db->createCommand('SELECT COALESCE(SUM(t.price),0) total_purchase FROM tbl_booking t WHERE t.booked_by="'.Yii::app()->user->username.'"')->queryRow();

            $user_data['total_sell'] = $total_sell['total_sell'];
            $user_data['total_purchase'] = $total_purchase['total_purchase'];

            $criteria_sell = new CDbCriteria;
            $criteria_sell->select = 't.*, b.id booking_id';
            $criteria_sell->join = 'LEFT JOIN tbl_booking b ON b.marketplace_id=t.id';
            $criteria_sell->condition = 't.username=:username';
            $criteria_sell->params = array(':username' => Yii::app()->user->username);
            $criteria_sell->order = "t.created_time DESC";
            $criteria_sell->limit=5;

            $recent_sell = Marketplace::model()->findAll($criteria_sell);

            $criteria_booking = new CDbCriteria;
            $criteria_booking->select = 't.*';
            $criteria_booking->join = 'LEFT JOIN tbl_marketplace m ON t.marketplace_id = m.id';
            $criteria_booking->condition = 't.booked_by=:booked_by_user';
            $criteria_booking->params = array(':booked_by_user' => Yii::app()->user->username);
            $criteria_booking->order = "t.created_time DESC";
            $criteria_booking->limit=5;

            $recent_booking = Booking::model()->findAll($criteria_booking);
            $this->render('index', array('recent_sell' => $recent_sell, 'recent_booking' => $recent_booking, 'user_data' => $user_data));
        }
    }

    public function actionTest()
    {
        $this->layout = "//layouts/main_home";

        $comments = Homepage::model()->findAll(array("condition"=>"type = 'COMMENT'"));//array("condition"=>"email_id =  $id","order"=>"id")
        $update_counter = Yii::app()->db->createCommand()->update('visitor_counter', array(
                'counter'=>new CDbExpression('counter + 1'),
            )
        );
        $routes = Yii::app()
            ->db
            ->createCommand(
                "select r.id, r.route_no, r.route_detail, r.actual_seat, count(*) current_seat_count 
                  from tbl_routes r join tbl_commonfleets c on r.route_no = c.present_route
                  group by r.route_no
                  order by r.route_no")
            ->queryAll();

        $marketplace = Yii::app()
            ->db
            ->createCommand(
                "select count(*) rcount from tbl_routes r 
                  join ifleetdb.tbl_marketplace m on r.id = m.route_id
                  where m.status is false and m.release_date = current_date()")
            ->queryRow();
        $r_avail = array_sum(array_map(function ($r){return $r['actual_seat'];}, $routes)) - array_sum(array_map(function ($r){return $r['current_seat_count'];}, $routes));
        $this->render('test', array(
            'comments' => $comments,
            'counts' => array(
                'route_seats' => $r_avail,
                'market_seats' => $marketplace['rcount']
            )
        ));
    }

    public function actionGetVehicleList(){
        $sql = "";
        $ws_sql = "SELECT d.vehicle_reg_no FROM tbl_defects d LEFT JOIN tbl_deliveryvehicles dv ON d.id=dv.defect_id LEFT JOIN tbl_vehicles v ON d.vehicle_reg_no=v.reg_no LEFT JOIN tbl_vehicletypes vt ON d.vehicletype_id=vt.id WHERE (v.`location`='HO' OR v.dedicate = 'Dedicate') AND dv.delivery_date IS NULL AND vt.type<>'Ambulance' AND vt.type<>'Refrigerator Van' ORDER BY d.`vehicletype_id` ASC ";
        $ws_vehicles = Yii::app()->db->createCommand($ws_sql)->queryAll();

        if ( (time() > strtotime("05:30:00") && time() < strtotime("09:00:00")) || (time() > strtotime("17:00:00") && time() < strtotime("21:00:00"))) {
            $pd_sql = "SELECT v.reg_no as vehicle_reg_no FROM tbl_vehicles v LEFT JOIN tbl_commonfleets c ON v.reg_no=c.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND c.application_type='Pick and Drop' AND c.approve_status='Approve' AND c.`vehicle_reg_no`<>''  AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC ";
            $pd_vehicles = Yii::app()->db->createCommand($pd_sql)->queryAll();
        }else{
            $pd_vehicles = array();
        }
        $v = array_merge($ws_vehicles, $pd_vehicles);
        $total = array_unique($v, SORT_REGULAR);

        if ($_GET['searchType'] == 'onduty'){
            /* GROUP BY used to avoid multiple requisition of same vehicle */
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.`location`='HO' AND ((v.`location`='HO' AND r.active=2) OR v.dedicate='Dedicate') AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'OnDutyDedicated'){
            /* GROUP BY used to avoid multiple requisition of same vehicle */
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.`location`<>'HO' AND v.dedicate = 'Dedicate' AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'available'){
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.`location`='HO' AND v.dedicate<>'Dedicate' AND r.start_date IS NULL AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'dedicated'){
            $sql = "SELECT v.reg_no as vehicle_reg_no  FROM tbl_vehicles v LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no AND r.start_date<='".$_GET['searchDate']."' AND r.end_date>='".$_GET['searchDate']."' LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND v.dedicate = 'Dedicate' and r.start_date is null AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' ORDER BY v.reg_no ASC";
        }
        elseif ($_GET['searchType'] == 'workshop'){
            $sql = "SELECT d.vehicle_reg_no FROM tbl_defects d LEFT JOIN tbl_deliveryvehicles dv ON d.id=dv.defect_id LEFT JOIN tbl_vehicles v ON d.vehicle_reg_no=v.reg_no LEFT JOIN tbl_vehicletypes vt ON d.vehicletype_id=vt.id WHERE (v.`location`='HO' OR v.dedicate = 'Dedicate') AND dv.delivery_date IS NULL AND vt.type<>'Ambulance' AND vt.type<>'Refrigerator Van' ORDER BY d.`vehicletype_id` ASC ";
        }elseif ($_GET['searchType'] == 'staffpickdrop'){
            $sql = "SELECT v.reg_no as vehicle_reg_no FROM tbl_vehicles v LEFT JOIN tbl_commonfleets c ON v.reg_no=c.vehicle_reg_no LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id WHERE v.active=1 AND c.application_type='Pick and Drop' AND c.approve_status='Approve' AND c.`vehicle_reg_no`<>''  AND vt.`type`<>'Ambulance' AND vt.`type`<>'Refrigerator Van' GROUP BY v.reg_no ORDER BY v.reg_no ASC ";
        }
        $vehicles = Yii::app()->db->createCommand($sql)->queryAll();
        if ($_GET['searchType'] == 'available' || $_GET['searchType'] == 'onduty'){
            for ($i=0;$i<count($vehicles);$i++){
                for ($j=0;$j<count($total);$j++){
                    if ($vehicles[$i]['vehicle_reg_no'] == $total[$j]['vehicle_reg_no']){
                        unset($vehicles[$i]);
                    }
                }
            }
        }
        if ($_GET['searchType'] == 'staffpickdrop'){
            for ($i=0;$i<count($vehicles);$i++){
                for ($j=0;$j<count($ws_vehicles);$j++){
                    if ($vehicles[$i]['vehicle_reg_no'] == $ws_vehicles[$j]['vehicle_reg_no']){
                        unset($vehicles[$i]);
                    }
                }
            }
        }

        $vehicleList = array();
        foreach($vehicles as $item){
            $vehicleList[$item['vehicle_reg_no']] = $item['vehicle_reg_no'];
        }
        $this->renderPartial('vehicleList', array('vehicles'=>$vehicles));
    }


    public function actionGetRequisitionDetail()
    {
        $model = Requisitions::model()->findByPk($_GET["id"]);
        echo json_encode($model->attributes);
        die();
    }

    public function actionGetDefectDetail()
    {
        $sql = "SELECT d.id, d.vehicle_reg_no,d.driver_name,d.driver_pin,d.apply_date, vt.`type` FROM tbl_defects d JOIN `tbl_vehicletypes` vt ON d.`vehicletype_id`=vt.`id` WHERE d.`id`='" . $_GET['id']."'";
        $c = Yii::app()->db->createCommand($sql);
        $result = $c->queryRow();
        echo json_encode($result);
        die();
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                    "Reply-To: {$model->email}\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    public function getTopTable($count){
        return  "<table class='top-table'>" .
            "<tr><td>Bus</td><td> : </td><td>" . (isset($count['Bus'])?$count['Bus']:0) . "</td></tr>" .
            "<tr><td>Car</td><td> : </td><td>" . (isset($count['Car'])?$count['Car']:0) . "</td></tr>" .
            "<tr><td>Jeep</td><td> : </td><td>" . (isset($count['Jeep'])?$count['Jeep']:0) . "</td></tr>" .
            "<tr><td>Microbus</td><td> : </td><td>" . (isset($count['Microbus'])?$count['Microbus']:0) . "</td></tr>" .
            "<tr><td>Pickup/Delivery Van</td><td> : </td><td>" . (isset($count['Pickup/Delivery Van'])?$count['Pickup/Delivery Van']:0) . "</td></tr>" .
            "</table>";
    }

    public function actionNextDayRequisitions() {
        $pending = Yii::app()->db->createCommand("SELECT COUNT(*) as pending FROM tbl_requisitions r WHERE r.start_date = (CURDATE()+1) AND active = 0")->queryAll();
        $notapproved = Yii::app()->db->createCommand("SELECT COUNT(*) as notapproved FROM tbl_requisitions r WHERE r.start_date = (CURDATE()+1) AND active = 1")->queryAll();
        $approved = Yii::app()->db->createCommand("SELECT COUNT(*) as approved FROM tbl_requisitions r WHERE r.start_date = (CURDATE()+1) AND active = 2")->queryAll();
        $next_day_data = new stdClass();
        $next_day_data->approved = $approved;
        $next_day_data->notapproved = $notapproved;
        $next_day_data->pending = $pending;
        echo json_encode(array(
            'nextDayData'=> $next_day_data
        ));
    }

    public function actionRequisitionStats() {
        $pending = Yii::app()->db->createCommand("SELECT COUNT(*) as pending FROM tbl_requisitions r WHERE r.start_date = (CURDATE()+1) AND active = 0")->queryRow();
        $notapproved = Yii::app()->db->createCommand("SELECT COUNT(*) as notapproved FROM tbl_requisitions r WHERE r.start_date = (CURDATE()+1) AND active = 1")->queryRow();
        $approved = Yii::app()->db->createCommand("SELECT COUNT(*) as approved FROM tbl_requisitions r WHERE r.start_date = (CURDATE()+1) AND active = 2")->queryRow();
        $next_day_data = new stdClass();
        $next_day_data->approved = $approved['approved'];
        $next_day_data->notapproved = $notapproved['notapproved'];
        $next_day_data->pending = $pending['pending'];

        $pending = Yii::app()->db->createCommand("SELECT COUNT(*) as pending FROM tbl_requisitions r WHERE r.start_date = CURDATE() AND active = 0")->queryRow();
        $notapproved = Yii::app()->db->createCommand("SELECT COUNT(*) as notapproved FROM tbl_requisitions r WHERE r.start_date = CURDATE() AND active = 1")->queryRow();
        $approved = Yii::app()->db->createCommand("SELECT COUNT(*) as approved FROM tbl_requisitions r WHERE r.start_date = CURDATE() AND active = 2")->queryRow();
        $today_data = new stdClass();
        $today_data->approved = $approved['approved'];
        $today_data->notapproved = $notapproved['notapproved'];
        $today_data->pending = $pending['pending'];
        echo json_encode(array(
            'todayData'=> $today_data,
            'nextDayData'=> $next_day_data
        ));
        die;
    }

    /**
     * @return string
     */
    public function actionPickAndDropStats(){
        $routes = Yii::app()
            ->db
            ->createCommand(
                "select r.id, r.route_no, r.route_detail, r.actual_seat, count(*) current_seat_count 
                  from tbl_routes r join tbl_commonfleets c on r.route_no = c.present_route
                  group by r.route_no
                  order by r.route_no")
            ->queryAll();
        $html =
            "<table class='table table-responsive table-hover'>
                <tr>
                    <th style='width: 15%'>Route No </th>
                    <th style='width: 60%'>Route Detail</th>
                    <th style='width: 25%'>Seats Available</th>
                </tr>";
        foreach ($routes as $route){
            $html .=
                "<tr>".
                    "<td>".$route['route_no']."</td>".
                    "<td>".$route['route_detail']."</td>".
                    "<td>".($route['actual_seat']-$route['current_seat_count'])."</td>".
                "</tr>";
        }
        if (count($routes) == 0) $html .= "<tr><td colspan='3'><i>No record found</i></td></tr>";
        $html .= "</table>";
        echo $html;die;
    }

    /**
     * @return string
     */
    public function actionMarketplaceStats(){
        $routes = Yii::app()
            ->db
            ->createCommand(
                "select r.id, r.route_no, r.route_detail, count(*) seats_for_sale from tbl_routes r 
                  join tbl_marketplace m on r.id = m.route_id
                  where m.status is false and m.release_date = current_date() 
                  group by r.id
                  order by r.id")
            ->queryAll();
        $html =
            "<table class='table table-responsive table-hover'>
                <tr>
                    <th style='width: 15%'>Route No </th>
                    <th style='width: 60%'>Route Detail</th>
                    <th style='width: 25%'>Seats Available</th>
                </tr>";
        foreach ($routes as $route){
            $html .=
                "<tr>".
                    "<td>".$route['route_no']."</td>".
                    "<td>".$route['route_detail']."</td>".
                    "<td>".$route['seats_for_sale']."</td>".
                "</tr>";
        }
        if (count($routes) == 0) $html .= "<tr><td colspan='3'><i>No record found</i></td></tr>";
        $html .= "</table>";
        echo $html;die;
    }

    /**
     * Displays the login page
     */
    /*public function actionLogin()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }*/

    /**
     * Logs out the current user and redirect to homepage.
     */

    /*
     public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }*/
    public function actionAutoLogin(){ 
        
        $image_url='../photo/'.$_GET['pin'].".png";
        $base64string=file_get_contents("http://api.brac.net/v1/staffs/".$_GET['pin']."/photo?Key=960d5569-a088-4e97-91bf-42b6e5b6d101");
        file_put_contents($image_url, base64_decode($base64string)); 

       $uri = "http://api.brac.net/v1/staffs/".$_GET['pin']."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
       $staffInfo = CJSON::decode(file_get_contents($uri));
       $first_name=substr($staffInfo[0]['StaffName'],0,strpos($staffInfo[0]['StaffName'],' '));
       $last_name=substr($staffInfo[0]['StaffName'],strpos($staffInfo[0]['StaffName'],' '),-1);
        
        $model=new UserLogin;
        
        $model->username="myDesk";
        $model->password="654@321";
        if($model->validate()) {  
        Yii::app()->user->username=$_GET['pin'];                
        Yii::app()->user->email=$staffInfo[0]['EmailID'];               
        Yii::app()->user->picmarket_user=1;                
        Yii::app()->user->lastvisit_at=date('Y-m-d H:i:s');   
         Yii::app()->user->firstname= $first_name;             
         Yii::app()->user->lastname= $last_name;             
         Yii::app()->user->level= (int)$staffInfo[0]['JobLevel'];             
         Yii::app()->user->department=$staffInfo[0]['ProjectName'];              
         Yii::app()->user->designation=$staffInfo[0]['DesignationName'];          
         Yii::app()->user->phone=$staffInfo[0]['MobileNo'];             
         Yii::app()->user->address=$staffInfo[0]['PresentAddress'];             
         Yii::app()->user->myDeskUser=1; 
             if(isset($_GET['link'])){
                if($_GET['link']==1){
                    $this->redirect(Yii::app()->baseUrl."/index.php/marketplace/sellHistory");                  
                       
                }
                if ($_GET['link']==2) {
                     $this->redirect(Yii::app()->baseUrl."/index.php/marketplace/bookingHistory");
                }
             }
             else{ 
                        if (Yii::app()->user->returnUrl=='/index.php')
                            $this->redirect(Yii::app()->controller->module->returnUrl);
                        else
                            $this->redirect(Yii::app()->user->returnUrl);
            }
        }
       
    }
}