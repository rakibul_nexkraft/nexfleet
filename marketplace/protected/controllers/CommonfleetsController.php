<?php

class CommonfleetsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

	public $application_type = array(
		'Pick and Drop' =>'Pick and Drop',
		'Change Route'=>'Change Route',
		'Cancel Seat'=>'Cancel Seat',
		'Maternity Leave'=>'Maternity Leave'
	);

	public $approve_status = array(
		'Approve' =>'Approve',
		'Pending'=>'Pending',
		'Cancel'=>'Cancel'
	);
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','smssend'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$this->renderPartial('view',array(
				'model'=>$this->loadModel($id),
			),false,true);
			Yii::app()->end();
		}

		else
		{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Commonfleets;
        $routehistory = null;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Commonfleets']))
		{
			$apptype = $_POST['Commonfleets']['application_type'];
			$status = $_POST['Commonfleets']['approve_status'];

			# Increasing 'Pass Given'/'seat_capacity' on 'Pick and Drop' Approval
			if ($apptype == 'Pick and Drop' && $status == 'Approve'){

				$_POST['Commonfleets']['present_route'] = $_POST['Commonfleets']['preferred_route'];	//On approval inserting 'present_route'
			}

			$getname = Yii::app()->user->username;

			# Get username who created
            $_POST['Commonfleets']['created_by'] = $getname;

			$model->attributes=$_POST['Commonfleets'];

			# Get who and when approved
			if($model->approve_status == 'Approve')
			{
                $routehistory = new Routehistory;

				$model->updated_by = $getname;
				$model->updated_time = new CDbExpression('NOW()');
			}
			
			
          
			if($model->save())
			{
                if($routehistory != null){
                    $routehistory->user_pin = $_POST['Commonfleets']['user_pin'];
                    $routehistory->user_name = $_POST['Commonfleets']['user_name'];
                    $routehistory->previous_route = $_POST['Commonfleets']['preferred_route'];
                    $routehistory->commonfleets_id = $model->id;
                    $routehistory->created_time = new CDbExpression('NOW()');
                    if (isset($routehistory->attributes)){
                        $routehistory->save();
                    }
                }

                if(!empty($model->user_cell)){
                $this->actionSmssend($model->user_cell,$model->approve_status,$model->vehicle_reg_no,$model->preferred_route);
            	}
				# Send email to the applicant only when application is approved or pending
				if(!empty($model->user_email) && $model->application_type == "Pick and Drop" && !empty($model->approve_status) && $model->approve_status!='Cancel')
				{

                                    $this->sendMailIcress($model);

					// && !empty($model->vehicle_reg_no)
					//$message = 'Test Mail';

					/*Yii::app()->mailer->Host = '172.25.100.113';
					Yii::app()->mailer->IsSMTP();
					Yii::app()->mailer->From = 'ifleet@brac.net';
					Yii::app()->mailer->FromName = 'iFleet Administrator';
					//Yii::app()->mailer->AddReplyTo('brac.ictweb@gmail.com');
					//Yii::app()->mailer->AddAddress('shouman.das@gmail.com');
					Yii::app()->mailer->AddAddress($model->user_email);
					Yii::app()->mailer->Subject = 'Transport Seat Allocation Status Notification';

					# Select email templet when application is approved
					if($model->approve_status=='Approve')
						Yii::app()->mailer->Body = $this->renderPartial('_email_pickndrop', array('model'=>$model), true);

					# Select email templet when application is pending
					elseif($model->approve_status=='Pending')
						Yii::app()->mailer->Body = $this->renderPartial('_email_pickndrop_pending', array('model'=>$model), true);

					Yii::app()->mailer->Send();
					*/
				}

				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
            'application_type'=>$this->application_type,
            'approve_status'=>$this->approve_status,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

        $routehistory = null;

        $routehistory_first = null;

		# Old Status
		$status_first = $model->approve_status;
		$apptype_first = $model->application_type;
		$route_no_first = $model->preferred_route;
		$present_route_no_first = $model->present_route;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['Commonfleets']))
		{
			# New Status
            $apptype = $_POST['Commonfleets']['application_type'];
			$status = $_POST['Commonfleets']['approve_status'];
            $route_no = $_POST['Commonfleets']['preferred_route'];
			$present_route_no = $_POST['Commonfleets']['present_route'];

			# Issue [Changing route no. without even changing application type and approval status]: Prevent changing route no. when application type and approval status not changed in update
			if($apptype == $apptype_first && $status == $status_first){
				$_POST['Commonfleets']['preferred_route'] = $route_no_first;
			}

			# Issue [Multiple seat allocation]: Prevent changing approval status and route number of a previously approved seat when application type not changed
			elseif($status != 'Approve' && $status_first == 'Approve' && $apptype == $apptype_first){
				$_POST['Commonfleets']['approve_status'] = $status_first;
				$_POST['Commonfleets']['preferred_route'] = $route_no_first;
			}

			# Feature: Approval of seat for pick n drop when employee in not assigned to other route
            elseif($apptype == 'Pick and Drop' && $status == 'Approve' && empty($present_route_no)){
				$_POST['Commonfleets']['present_route'] = $_POST['Commonfleets']['preferred_route'];
//                $_POST['Commonfleets']['previous_route'] = $_POST['Commonfleets']['preferred_route'];
                $routehistory = new Routehistory;
			}

			# Allow route changing application only when employee is assigned to a route no. and make it available under "Pick and Drop"
			elseif($apptype == 'Change Route' && !empty($present_route_no) && $status == 'Approve'){
//                $_POST['Commonfleets']['previous_route'] = $_POST['Commonfleets']['present_route'];
				$_POST['Commonfleets']['present_route'] = $_POST['Commonfleets']['preferred_route'];

                $routehistory = new Routehistory;
				//$_POST['Commonfleets']['application_type'] = 'Pick and Drop';
			}

			# Change if routehistory not working properly
//            if ($status_first != 'Approve' && $status == 'Approve'){
////			    $sql = "SELECT * FROM tbl_routehistory";
//			    $routehistory = new Routehistory;
//            }

			$model->attributes=$_POST['Commonfleets'];
			if($model->approve_status == 'Approve' && ($status_first != $model->approve_status || $model->application_type!=$apptype_first))
			{
				$model->updated_by=Yii::app()->user->username;
				$model->updated_time=new CDbExpression('NOW()');
			}
			

			if($model->save())
			{
                if($routehistory != null){
                    $routehistory->user_pin = $_POST['Commonfleets']['user_pin'];
                    $routehistory->user_name = $_POST['Commonfleets']['user_name'];
                    $routehistory->previous_route = $_POST['Commonfleets']['preferred_route'];
                    $routehistory->commonfleets_id = $model->id;
                    $routehistory->created_time = new CDbExpression('NOW()');
                    if (isset($routehistory->attributes)){
                        $routehistory->save();
                    }
                }
                if(!empty($model->user_cell)){
                $this->actionSmssend($model->user_cell,$model->approve_status,$model->vehicle_reg_no,$model->preferred_route);
            	}
				if(!empty($model->user_email) && (($model->application_type!='Maternity Leave' || $model->application_type!='Cancel Seat') && $model->approve_status != 'Pending')){
					if($model->approve_status!=$status_first || $model->application_type!=$apptype_first){

							$this->sendMailIcress($model);
					}
				}
                if($apptype == 'Change Route' && $status == 'Approve'){
                    $model->application_type = 'Pick and Drop';
                    $model->save();
                }

                # Feature: Widrow currently allocated seat after the approval of seat cancellation
                elseif($apptype == 'Cancel Seat' && $status == 'Approve'){
                    $model->previous_route = $_POST['Commonfleets']['present_route'];
                    $model->present_route = null;
                    $model->save();
                }
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		if(Yii::app()->request->isAjaxRequest)
		{
			$this->renderPartial('update',array(
				'model'=>$model,
				'application_type'=>$this->application_type,
				'approve_status'=>$this->approve_status,
			),false,true);
			Yii::app()->end();
		}

		else{
			$this->render('update',array(
				'model'=>$model,
				'application_type'=>$this->application_type,
				'approve_status'=>$this->approve_status,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Commonfleets');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/

		$model=new Commonfleets('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Commonfleets']))
			$model->attributes=$_GET['Commonfleets'];
		$this->render('index',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
			'application_type'=>$this->application_type,
            'approve_status'=>$this->approve_status,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Commonfleets('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Commonfleets']))
			$model->attributes=$_GET['Commonfleets'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Commonfleets::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='commonfleets-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionExcel()
	{
		$model=new Commonfleets('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['criteria']))
			$model->attributes=$_GET['criteria'];

		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Seat_Allocation',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Seat_Allocation',
			'columns'=>array(
				'id',
				'user_pin',
				'user_name',
				'user_level',
				'user_dept',
				'user_cell',
				'user_email',
				'application_type',
				'present_route',
				'preferred_route',
				'approve_status',
			),
		));
		Yii::app()->end();
	}


	public function sendMailIcress($model)
	{

		try{
  	   		//$soapClient = new SoapClient("http://192.168.20.114:8282/isoap.comm.imail/EmailWS?wsdl"
  	   		$soapClient = new SoapClient("http://172.25.100.41:8080/isoap.comm.imail/EmailWS?wsdl"  );


				$job = new jobs;

				$job->subject='Transport Seat Allocation Status Notification';
				$job->jobContentType='html';
				$job->fromAddress='ifleet@brac.net';
				$job->udValue1='iFleet Administrator';
				$job->requester='iFleet';

				$job->jobRecipients[0]=new jobRecipients;
				$job->jobRecipients[0]->recipientEmail=$model->user_email;


				if($model->approve_status ==  'Approve')
				{
					if($model->application_type == "Pick and Drop")
						$job->body = $this->renderPartial('_email_pickndrop', array('model'=>$model), true);
					else if($model->application_type == "Change Route")
						$job->body =  $this->renderPartial('_email_routechange', array('model'=>$model), true);
					else if($model->application_type == "Cancel Seat")
						$job->body =  $this->renderPartial('_email_cancelseat', array('model'=>$model), true);
					else if($model->application_type == "Maternity Leave")
						$job->body =  $this->renderPartial('_email_matleave', array('model'=>$model), true);
				}
				if($model->approve_status == 'Pending')
				{
						if($model->application_type == "Pick and Drop")
								$job->body = $this->renderPartial('_email_pickndrop_pending', array('model'=>$model), true);
							else if($model->application_type == "Change Route")
								$job->body = $this->renderPartial('_email_routechange_pending', array('model'=>$model), true);
				}

				$jobs = array('jobs'=>$job);
			  $send_email =$soapClient->__call('sendEmail',array($jobs));


			  }
					catch (SoapFault $fault) {
						    $error = 1;
						    print($fault->faultcode."-".$fault->faultstring);
					}
			}


	 public function actionSmssend($user_cell,$status,$vehicle_reg_no,$preferred_route)
    {
        //$r = file_get_contents("http://mydesk.brac.net/smsc/create?token=286cf3ae67c80ee7d034bb68167ab8455ea443a2&message=test&to_number=8801919197617");

        $client = new EHttpClient(
            'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
            )
        );
		$user_cell = substr($user_cell, -11);
		$driver_cell = substr($driver_cell, -11);

        if (strlen($user_cell) == 10)
            $user_cell = "0" . $user_cell;
        else
            $user_cell = $user_cell;
        $message="Brac iFleet Pick & Drop Status:".$status." Vehicle No:".$vehicle_reg_no." Route:".$preferred_route;
         $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $message,
            'to_number' => $user_cell,
            'app_url' => 'http://ifleet.brac.net/',
        ));
//        $response = $client->request('POST');   //UNCOMMENT TO SEND SMS
        
    }

}

class jobs {
  public $appUserId; // string
  public $attachments; // attachment
  public $bcc; // string
  public $body; // string
  public $caption; // string
  public $cc; // string
  public $complete; // boolean
  public $feedbackDate; // dateTime
  public $feedbackEmail; // string
  public $feedbackName; // string
  public $feedbackSent; // boolean
  public $fromAddress; // string
  public $fromText; // string
  public $gateway; // string
  public $jobContentType; // string
  public $jobId; // long
  public $jobRecipients; // jobRecipients
  public $mode; // string
  public $numberOfItem; // int
  public $numberOfItemFailed; // int
  public $numberOfItemSent; // int
  public $priority; // string
  public $requester; // string
  public $status; // string
  public $subject; // string
  public $toAddress; // string
  public $toText; // string
  public $udValue1; // string
  public $udValue2; // string
  public $udValue3; // string
  public $udValue4; // string
  public $udValue5; // string
  public $udValue6; // string
  public $udValue7; // string
  public $vtemplate; // string
}

class jobRecipients {
  public $failCount; // int
  public $image; // base64Binary
  public $job; // jobs
  public $jobDetailId; // long
  public $recipientEmail; // string
  public $sent; // boolean
  public $sentDate; // dateTime
  public $toText; // string
}