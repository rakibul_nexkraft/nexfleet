<?php

class MarketplaceController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'sellUserSeat',  'bookUserSeat', 'getRouteDetails', 'submitBookingRequest','bookingDetails'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','bookingHistory','sellHistory'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','sellHistoryAdmin', 'bookingHistoryAdmin'),
//				'users'=>array('admin'),
                'expression'=>'Yii::app()->user->isAdmin()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Marketplace;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Marketplace']))
        {
            $model->attributes=$_POST['Marketplace'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Marketplace']))
        {
            $model->attributes=$_POST['Marketplace'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $userSeat = Yii::app()->db->createCommand("SELECT c.*, r.route_detail FROM tbl_commonfleets c JOIN tbl_routes r ON c.present_route = r.route_no WHERE c.user_pin = '".Yii::app()->user->username."'")->queryRow();

        $availableSeats = Marketplace::model()->findAll(
            'status=0 AND release_date=:releaseDate And created_by!=:bookByUser',
            array(
                ':releaseDate'=>$this->checkNextSlotAndCommuteDirection()->date,
                ':bookByUser'=>Yii::app()->user->username)
        );

//        $this->render("user_operation", array('userSeat'=>$userSeat, 'seatCount'=>count($availableSeats)));

        $this->render('index', array('userSeat'=>$userSeat, 'seatCount'=>count($availableSeats)));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Marketplace('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Marketplace']))
            $model->attributes=$_GET['Marketplace'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=Marketplace::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='marketplace-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionBookUserSeat() {
//	    $model = new Marketplace;

        //$model=new Marketplace('search');
        //$model->unsetAttributes();  // clear any default values
        $model=new Marketplace;
        $model->attributes=$_POST['Marketplace'];
        if(isset($_POST['Marketplace'])){

            if(!$this->checkNextSlotAndCommuteDirection()->slot_open){
                $this->render('book_user_seat',array(
                    'model'=>$model,
                    'routes_rs'=>array(),
                ));
                return;
            }

            /* CHECK STOPPAGE LIST */
            $sql_stoppage="SELECT * FROM tbl_stoppage ";
            $sql_stoppage_where = "";
            if(!empty($_POST['Marketplace']['start_point'])&&!empty($_POST['Marketplace']['end_point'])) {
                $sql_stoppage_where .= " WHERE ";
                $sql_stoppage_where .= " stoppage ='" . $_POST['Marketplace']['start_point'] . "' OR stoppage='" . $_POST['Marketplace']['end_point'] . "' ";
            } else if(!empty($_POST['Marketplace']['start_point'])) {
                $sql_stoppage_where .= " WHERE ";
                $sql_stoppage_where .= " stoppage ='" . $_POST['Marketplace']['start_point'] . "' ";
            } else if(!empty($_POST['Marketplace']['end_point'])) {
                $sql_stoppage_where .= " WHERE ";
                $sql_stoppage_where .= " stoppage ='" . $_POST['Marketplace']['end_point'] . "' ";
            }
            $route_id="";
            $route_id_rs=Yii::app()->db->createCommand($sql_stoppage.$sql_stoppage_where)->queryAll();
            if(count($route_id_rs)>0){
                $route_id = implode(",", array_map(function ($r){return $r['route_id'];}, $route_id_rs));
            }
//            var_dump($sql_stoppage.$sql_stoppage_where. " <<<<<<<<<<<<");
//            var_dump($route_id);die;
            if (strlen($route_id) == 0){
                $this->render('book_user_seat',array(
                    'model'=>$model,
                    'routes_rs'=>array(),
                ));
                return;
            }

//            echo "==== ".$route_id." ====";

            /* SEARCH ROUTES IN MARKETPLACE FROM THE STOPPAGE LIST */
            $sql_route="SELECT m.*, r.route_no, r.route_detail, v.reg_no, r.actual_seat, vt.type FROM tbl_marketplace m JOIN tbl_routes r ON m.route_id=r.id INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON v.vehicletype_id=vt.id ";

            $sql_route_where = " WHERE status = 0 AND m.travel_direction='".$_POST['Marketplace']['travel_direction']."' AND m.release_date='".$_POST['Marketplace']['release_date']."' AND m.route_id IN($route_id) ";

            if(!empty($_POST['Marketplace']['vehicle_type_id'])){
                $sql_route_where .= "  AND v.vehicletype_id='".$_POST['Marketplace']['vehicle_type_id']."' ";
            }
//            echo $sql_stoppage.'<br>';
//            $sql_route_where .= " AND m.route_id IN($route_id)";
//            echo $sql_route.$sql_route_where.'<br>';
            $routes_rs=Yii::app()->db->createCommand($sql_route.$sql_route_where)->queryAll();

//            die;
            $this->render('book_user_seat',array(
                'model'=>$model,
                'routes_rs'=>$routes_rs,
            ));
            return;

        }

        $this->render("book_user_seat", array('model'=>$model,));
    }

    public function actionGetRouteDetails() {
        $marketplace_id = $_POST['marketplaceId'];
        $route_id = $_POST['routeId'];
        $stoppageId = $_POST['stoppageId'];
        $releaseDate = $_POST['releaseDate'];
        $html =
            "<div class='center s002 '>
	   			<h4 class='heading-custom'>CONFIRM SUBMISSION</h4>	   	
	   		 </div>";
        if(!empty($route_id)){//route id have
            $route=Routes::model()->findByPk($route_id);
            if(!empty($route)){//Route found
                $total_seat=$route['actual_seat'];
                $borderPass=Routes::borderPass($route['route_no']);
                $av_seat=$total_seat-$borderPass;
                $loop_seats=$total_seat%4==0?$total_seat:$total_seat+(4-$total_seat%4);

                $stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='".$route_id."' ORDER BY pickup_time";
                $stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();

                $driver_info_sql="SELECT d.* FROM tbl_vehicles as v INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin WHERE v.reg_no='".$route['vehicle_reg_no']."'";
                $driver_info_rs=Yii::app()->db->createCommand($driver_info_sql)->queryRow();

                $html.='<div class="accordion accordion-border clearfix">';
                $html.='<div class="col_one_third">';
                $html.='<div class="information-box">';
                $html.='<div class="steering clearfix">';
                $html.='<img  src="'.Yii::app()->theme->baseUrl.'/images/car/steering.jpg" alt="Steering">';
                $html.='</div>';
                $html.='<div class="clearfix"></div>';
                if($total_seat>0){//check total seat

                    for($i=1; $i<=$loop_seats;$i++) {//seat loop start
                        if($i==1){
                            $html.='<div class="seat-plan">';
                        }
                        if($i<=$borderPass){
                            $html.='<div class="seat-location">'.$i.'</div>';
                        }
                        else{
                            if($i>$total_seat){
                                $html.='<div class="seat-location-white">X</div>';
                            }
                            else{
                                $html.='<div class="seat-location-white">'.$i.'</div>';
                            }
                        }
                        if($i%2==0 && $i%4!=0){
                            $html.='<div class="seat-location-gap"></div>';
                        }
                        if($i%4==0){
                            $html.='</div>';
                            $html.='<div class="clearfix"></div>';
                            if(($i+1)<=$loop_seats)
                                $html.='<div class="seat-plan">';
                        }
                    }// seat loop end
                    $html.='<div class="min-hight"></div>';
                }//check total seat
                $html.='</div>';
                $html.='</div>';
                //stoppage time div start
                $html.='<div class="col_one_third">';
                $html.='<div class="information-box">';
                $html.='<div class="center s002 ">';

                $html.='<h4 class="heading-custom">Route '.$route['route_no'].' Stoppages List</h4>';

                $html.='</div>';
                $html.='<div class="clearfix"></div>';
                $html.='<div class="stoppage-table">';
                $html.='<div class="table-responsive">';

                $html.='<table class="table cart">';
                $html.='<thead>';
                $html.='<tr>';
                $html.='<th class="cart-product-price"><strong>Stoppage</strong></th>';
                $html.='<th class="cart-product-price"><strong>Time</strong></th>';

                $html.='</tr>';
                $html.='</thead>';
                $html.='<tbody>';
                if(count($stoppage_rs)>0){
                    foreach ($stoppage_rs as $key => $value) {
                        $html.='<tr class="cart_item">';
                        $html.='<th  class="cart-product-price">';
                        $html.= $value['stoppage'];
                        $html.='</th >';

                        $html.='<th  class="cart-product-price">';
                        $html.= $value['pickup_time'];
                        $html.='</th >';

                        $html.='</tr>';
                    }
                }
                else{
                    $html.='<tr class="cart_item">';
                    $html.='<th colspan="2"  class="cart-product-price">';
                    $html.= 'Stoppage Not Found';
                    $html.='</th >';
                    $html.='</tr>';

                }
                $html.='</tbody>';

                $html.='</table>';

                $html.='</div>';
                $html.='</div>';
                $html.='</div>';
                $html.='</div>';
                // Information div start

                $html.='<div class="col_one_third col_last">';
                $html.='<div class="information-box-last">';
                $html.='<div class="driver-info">';
                if($driver_info_rs){
                    $html.='<div class="driver-info-row">';
                    $html.='<strong>Driver Name :</strong> '. $driver_info_rs['name'];
                    $html.='</div>';
                    $html.='<div class="driver-info-row">';
                    $html.='<strong>Driver Pin :</strong> ' . $driver_info_rs['pin'];
                    $html.='</div>';
                    $html.='<div class="driver-info-row">';
                    $html.='<strong>Driver Phone :</strong> ' . $driver_info_rs['phone'];
                    $html.='</div>';
                }
                else{
                    $html.='<div class="driver-info-row">';
                    $html.='<strong>Driver Information :</strong>  Not Found!';
                    $html.='</div>';
                }
                $html.='<div class="driver-info-row">';
                $html.='<strong>Route No:</strong> '.$route['route_no'];
                $html.='</div>';
                $html.='<div class="driver-info-row">';
                $html.='<strong>Price :</strong> '.$route['market_price'];
                $html.='</div>';
                $html.='</div>';

                $html.='</div>';
                $html.='<div class="information-submit-button-div">';
                $html.=CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'submitBookingRequest('.$marketplace_id.','.$route_id.','.$stoppageId.',"'.$releaseDate.'")'));
                $html.='</div>';
                $html.='</div>';

                $html.='</div>';


            }//Route found
            else{//Route not found
                $html.='<div class="accordion accordion-border clearfix">
    								Route Not Found!
    							</div>';
            }//Route not found
        } //route id have
        else{//route id not have
            $html.='<div class="accordion accordion-border clearfix">
    								Route ID Not Found!
    							</div>';
        }//route id not have
        echo $html;
    }

    public function actionSubmitBookingRequest() {
        $model=new Booking();

        $marketplace_id = $_POST['marketplaceId'];
        $route_id = $_POST['routeId'];
        $stoppageId = $_POST['stoppageId'];
        $releaseDate = $_POST['releaseDate'];

        $route = Routes::model()->findByPk($route_id);
        $seat_detail = Marketplace::model()->findByPk($marketplace_id);

        $booking_status = new stdClass();

        if(!$seat_detail->status){
            $seat_detail->status = true;
            $seat_detail->updated_by = Yii::app()->user->username;
            $seat_detail->updated_time = date('Y-m-d H:i:s');
            if($seat_detail->save()){
                //DO COOL THINGS
                $model->route_id = $route_id;
                $model->zone_id = $route->zone_id;
                $model->price = $route->market_price;
                $model->status = true;
                $model->sold_by = $seat_detail->username;
                $model->booked_by = Yii::app()->user->username;
                $model->release_date = $seat_detail->release_date;
                $model->travel_direction = $seat_detail->travel_direction;
                $model->marketplace_id = $seat_detail->id;
                $model->created_by = Yii::app()->user->username;
                $model->created_time = date('Y-m-d H:i:s');

                if($model->save()){//route id have
                    $booking_status->message = "Booking successful!";
                    $booking_status->status = true;
                    $booking_status->id = $model->id;

                    $pickdrop_notification = new PickdropNotification();
                    $pickdrop_notification->username = $seat_detail->username;
                    $pickdrop_notification->title = "Marketplace";
                    $pickdrop_notification->description = "Your seat is booked by ".Yii::app()->user->firstname." ".Yii::app()->user->lastname.".";
                    $pickdrop_notification->web_url = PickdropNotification::MARKETPLACE_SELL_DETAIL_URL;
                    $pickdrop_notification->key_id = $model->id;
                    $pickdrop_notification->save();
                } else{
                    $seat_detail->status = false;
                    $seat_detail->save();
                    $booking_status->message = "Oops! Some parameters are missing. Please try again";
                    $booking_status->status = false;
//                    $error= $model->getErrors();
//                    $booking_status->message = json_encode($error);
//                    var_dump($error);
                    //var_dump(implode(',',$error));
                    //echo "Sorry, Please Try Again!";
                }
            }
        } else {
            $booking_status->message = "Booking failed! This seat has already been booked";
            $booking_status->status = false;
        }
        echo json_encode($booking_status);die;
    }

    public function actionSellUserSeat() {
//	    $userSeat = Yii::app()->db->createCommand("SELECT c.*, r.route_detail FROM tbl_commonfleets c JOIN tbl_routes r ON c.present_route = r.route_no WHERE c.user_pin = '".Yii::app()->user->username."'")->queryRow();

        //$_GET['userPin']=98520;//CHANGE IT WHEN DONE
        /*$uri = "http://api.brac.net/v1/staffs/".Yii::app()->user->username."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
        $staffInfo = CJSON::decode(file_get_contents($uri));
        $staffInfo = $staffInfo[0];*/

        $staffInfo = Yii::app()->user;

        $userSeat = Yii::app()->db->createCommand('SELECT present_route FROM tbl_commonfleets WHERE user_pin = "'.$staffInfo->username.'"')->queryRow();
        $routeInfo = Yii::app()->db->createCommand()
            ->select('r.*')
            ->from('tbl_routes r')
            ->where('r.route_no=:routeNo', array(':routeNo'=>$userSeat['present_route']))
            ->queryRow();

        $model = new Marketplace;

        if(isset($_POST['Marketplace'])) {
//            var_dump($_POST['Marketplace']);die;
            $next_slots = $this->getSellSlotAndDirection();
            $selected_slots = $_POST['Marketplace']['next_slots'];
            $errors = array();
            foreach ($selected_slots as $key=>$val) {
                $count = 0;
                foreach ($next_slots as $next_slot){
                    $slot = implode(",",get_object_vars ( $next_slot ));
                    if($val != $slot){
                        $count++;
                        if ($count == 2) {
                            $this->render("sell_user_seat", array('model'=>$model,'routeInfo'=>$routeInfo, 'staffInfo'=>$staffInfo, 'userPin'=>Yii::app()->user->username));
                        }
                    }
                }

                $slot_data = explode(",",$val);
                $sqlAlreadySold = "SELECT * FROM tbl_marketplace WHERE travel_direction = '".$slot_data[1]."' AND release_date = '". $slot_data[0]."' AND username = '".Yii::app()->user->username."'";

                $checkAlreadySold = Yii::app()->db->createCommand($sqlAlreadySold)->queryRow();
                if($checkAlreadySold != null) {
                    $errorSold = new stdClass();
                    $errorSold->isSold = true;
                    $errorSold->direction = $slot_data[1];
                    $errorSold->release_date = $slot_data[0];
                    array_push($errors, $errorSold);
                }
            }
            if(count($errors)>0){
                $this->render("sell_user_seat", array('model'=>$model,'routeInfo'=>$routeInfo, 'staffInfo'=>$staffInfo, 'userPin'=>Yii::app()->user->username, 'errorSold'=>$errors));
                return;
            }

            foreach ($selected_slots as $key=>$val){
                $slot_data = explode(",",$val);
                $model = new Marketplace;
                $model->attributes = $_POST['Marketplace'];
                $model->username = Yii::app()->user->username;
                $model->created_by = Yii::app()->user->username;
                $model->created_time = new CDbExpression('NOW()');
                $model->release_date = $slot_data[0];
                $model->travel_direction = $slot_data[1];
                $msg = new stdClass();
                $model->save();
//                if ($model->save()){
//                    $msg->message = "successful";
//                } else {
//                    $error = $model->getErrors();
//                    $msg->message = json_encode($error);
//                    var_dump($error);
//                }
            }
//            die;
            $this->redirect('sellHistory');
            /*if($this->checkNextSlotAndCommuteDirection()->date != $_POST['Marketplace']['release_date']){
                $this->render("sell_user_seat", array('model'=>$model,'routeInfo'=>$routeInfo, 'staffInfo'=>$staffInfo, 'userPin'=>Yii::app()->user->username));
                return;
            }*/
//                $this->redirect(array('view','id'=>$model->id));
        }


        $this->render("sell_user_seat", array('model'=>$model,'routeInfo'=>$routeInfo, 'staffInfo'=>$staffInfo, 'userPin'=>Yii::app()->user->username,));
    }

    public function actionSellHistory() {

        $criteria = new CDbCriteria;
        $criteria->select = 't.*, b.id booking_id';
        $criteria->join = 'LEFT JOIN tbl_booking b ON b.marketplace_id=t.id';
        $criteria->condition = 't.username=:username';
        $criteria->params = array(':username' => Yii::app()->user->username);
        $criteria->order = "t.created_time DESC";


        $model = Marketplace::model()->findAll($criteria);

        $this->render('sell_history',array(
            'model'=>$model,
        ));
    }

    public function actionBookingHistory(){
        $criteria = new CDbCriteria;
//        $criteria->select = 't.*, r.route_detail route_detail';
        $criteria->select = 't.*';
        $criteria->join = 'LEFT JOIN tbl_marketplace m ON t.marketplace_id = m.id';
//        $criteria->join .= ' JOIN tbl_routes r ON t.route_id = r.id';
        $criteria->condition = 't.booked_by=:booked_by_user';
        $criteria->params = array(':booked_by_user' => Yii::app()->user->username);
        $criteria->order = "t.created_time DESC";

        $model = Booking::model()->findAll($criteria);

        $this->render('booking_history',array(
            'model'=>$model,
        ));
    }

    public function actionSellHistoryAdmin() {
        $model=new Marketplace('sell-history-admin');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Marketplace']))
            $model->attributes=$_GET['Marketplace'];

        $this->render('sell_history_admin',array(
            'model'=>$model,
        ));
    }

    public function actionBookingHistoryAdmin() {
        $model=new Booking('booking-history-admin');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Booking']))
            $model->attributes=$_GET['Booking'];

        $this->render('booking_history_admin',array(
            'model'=>$model,
        ));
    }

    public function actionBookingDetails() {
//        $model = Booking::model()->findByPk($_GET['id']);

        /*LEFT JOIN tbl_users soldBY ON b.sold_by = soldBY.username
                LEFT JOIN tbl_users bookedBY ON b.booked_by = bookedBY.username*/

        $username = Yii::app()->user->username;

        $sql = "SELECT b.*, r.route_no, r.route_detail, z.name zone_name FROM tbl_booking b 
                LEFT JOIN tbl_routes r ON b.route_id = r.id
                LEFT JOIN tbl_zone z ON b.zone_id = z.id
                WHERE b.id = ".$_GET['id']. (Yii::app()->user->isAdmin() =='1' ? "" : " AND b.booked_by = '$username'");

        $model = Yii::app()->db->createCommand($sql)->queryRow();

        $this->render('booking_details',array(
            'model'=>$model,
        ));
    }


    public function actionGetRouteDetailAndAvailability() {
        $date = $this->checkNextSlotAndCommuteDirection()->date;

        $route_detail = Routes::model()->find(
            'route_no=:routeId',
            array(':routeId'=>$_POST['Booking']['route_id'])
        );
        $availableSeats = Marketplace::model()->findAll(
            'travel_direction=:travelDirection And route_id=:routeId And release_date=:releaseDate And created_by!=:bookByUser',
            array(
                ':travelDirection'=>$_POST['Booking']['travel_direction'],
                ':routeId'=>$route_detail->id,
                ':releaseDate'=>$date,
                ':bookByUser'=>Yii::app()->user->username)
        );
        $obj = array('route_detail' => $route_detail->attributes, 'seatCount'=>count($availableSeats));

        echo json_encode($obj);die;
    }

    public function checkNextSlotAndCommuteDirection() {

        $slot_data = new stdClass();

        $inSlot12am = strtotime("today 12am");
        $noSlot4am = strtotime("today 4am");
        $outSlot9am = strtotime("today 9am");
        $noSlot4pm = strtotime("today 4pm");
        $inSlot6pm = strtotime("today 6pm");
        $inSlot1159pm = strtotime("today 11.59.59pm");


//        $currentTime = strtotime(date("Y-m-d H:i:s"));
        $currentTime = strtotime("today 3.59.25pm");

        $travel_direction = "";
//        $date = date("Y-m-d");

        if($currentTime >= $inSlot12am && $currentTime < $noSlot4am){
            $date = date("Y-m-d");
            $travel_direction = $travel_direction . "HO IN";
            $slot_open = true;
        } else if ($currentTime >= $noSlot4am && $currentTime < $outSlot9am){
            $date = date("Y-m-d");
            $travel_direction = $travel_direction . "Slot is closed. Try next slot.";
            $slot_open = false;
        } else if ($currentTime >= $outSlot9am && $currentTime < $noSlot4pm){
            $date = date("Y-m-d");
            $travel_direction = $travel_direction . "HO OUT";
            $slot_open = true;
        } else if ($currentTime >= $noSlot4pm && $currentTime < $inSlot6pm){
            $date = date("Y-m-d");
            $travel_direction = $travel_direction . "Slot is closed. Try next slot.";
            $slot_open = false;
        } else if ($currentTime >= $inSlot6pm && $currentTime <= $inSlot1159pm){
            $date = date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day'));
            $travel_direction = $travel_direction . "HO IN";
            $slot_open = true;
        }
        $slot_data->date = $date;
        $slot_data->travel_direction = $travel_direction;
        $slot_data->slot_open = $slot_open;
        $slot_data->currtime = date("Y-m-d H:i:s");
        return $slot_data;
    }

    public function getSellSlotAndDirection() {

        $inSlot12am = strtotime("today 12am");
        $outSlot4am = strtotime("today 4am");
        $outSlot4pm = strtotime("today 4pm");
        $inSlot1159pm = strtotime("today 11.59.59pm");

        $slots = array();

//        $currentTime = strtotime(date("Y-m-d H:i:s"));
        $currentTime = strtotime("today 3.59.25pm");

        if($currentTime >= $inSlot12am && $currentTime < $outSlot4am){
            $s1 = new stdClass();
            $s1->date = date("Y-m-d");
            $s1->travel_direction = "HO IN";
            array_push($slots, $s1);
            $s2 = new stdClass();
            $s2->date = date("Y-m-d");
            $s2->travel_direction = "HO OUT";
            array_push($slots, $s2);

        } else if ($currentTime >= $outSlot4am && $currentTime <= $outSlot4pm){
            $s1 = new stdClass();
            $s1->date = date("Y-m-d");
            $s1->travel_direction = "HO OUT";
            array_push($slots, $s1);
            $s2 = new stdClass();
            $s2->date = date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day'));
            $s2->travel_direction = "HO IN";
            array_push($slots, $s2);
        } else if ($currentTime > $outSlot4pm && $currentTime <= $inSlot1159pm){
            $s1 = new stdClass();
            $s1->date = date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day'));
            $s1->travel_direction = "HO IN";
            array_push($slots, $s1);
            $s2 = new stdClass();
            $s2->date = date('Y-m-d', strtotime(date("Y-m-d") . ' +1 day'));
            $s2->travel_direction = "HO OUT";
            array_push($slots, $s2);
        }
        return $slots;
    }
}
