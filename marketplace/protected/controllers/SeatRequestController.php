<?php
date_default_timezone_set("Asia/Dhaka");
class SeatRequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','requestList','maternityUpdated'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','zone','route','routeInformation','userRouetRquest','routeInformationUpdate','userRouteRquestUpdate'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SeatRequest;
		/*$user_pin=$_GET['user_pin'];
		$model->user_pin = $user_pin;
		$model->created_by = $user_pin;

		//$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
		$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
        $staffInfo = CJSON::decode(file_get_contents($uri));
    
        $model->user_level = $staffInfo[0]['JobLevel'];
        $model->user_name= $staffInfo[0]['StaffName'];
        $model->email=$staffInfo[0]['EmailID'];
        $model->user_cell=$staffInfo[0]['MobileNo'];
        $model->user_department=$staffInfo[0]['ProjectName'];
        $model->user_designation=$staffInfo[0]['DesignationName'];
        $model->residence_address=$staffInfo[0]['PresentAddress'];
		$model->created_time = date('Y-m-d H:i:s');*/

		$user_pin=Yii::app()->pickanddrop->username;
		$model->user_pin = $user_pin;
		$model->created_by = $user_pin;

		
    
        $model->user_level = Yii::app()->pickanddrop->level;
        $model->user_name= Yii::app()->pickanddrop->firstname." ".Yii::app()->pickanddrop->lastname;
        $model->email=Yii::app()->pickanddrop->email;
        $model->user_cell=Yii::app()->pickanddrop->phone;
        $model->user_department=Yii::app()->pickanddrop->department;
        $model->user_designation=Yii::app()->pickanddrop->designation;
        $model->residence_address=Yii::app()->pickanddrop->address;
		$model->created_time = date('Y-m-d H:i:s');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequest']))
		{
			$model->attributes=$_POST['SeatRequest'];
			if($model->save()){				
				$this->redirect(array('view','id'=>$model->id,'user_pin'=>$user_pin));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequest']))
		{
			$model->attributes=$_POST['SeatRequest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		$model->status=3;
		$model->save();
		$this->actionIndex();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('SeatRequest');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		$model->attributes=$_POST['SeatRequest'];
		if(isset($_POST['SeatRequest'])){
			$sql="SELECT * FROM tbl_stoppage WHERE ";
			if(!empty($_POST['SeatRequest']['from'])&&!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."' OR stoppage='".$_POST['SeatRequest']['to']."'";
			else if(!empty($_POST['SeatRequest']['from']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."'";
			else if(!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['to']."'";
			else {
				$sql="ALL";
			}
			$route_id="";
			$sql_route="SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id";
			$sql_vehicle_type=0;
			if(!empty($_POST['SeatRequest']['vehicle_type_id'])){
				$sql_route.=" WHERE v.vehicletype_id='".$_POST['SeatRequest']['vehicle_type_id']."'";
				$sql_vehicle_type=1;
			}
//			echo $sql.'<br>';
			if($sql!="ALL"){
				$route_id_rs=Yii::app()->db->createCommand($sql)->queryAll();
				if(count($route_id_rs)>0){
					foreach ($route_id_rs as $key => $value) {
						$route_id.=$value['route_id'].",";
					}
					
				}
				$route_id=rtrim($route_id,',');
				if($sql_vehicle_type==0)
					$sql_route.=" WHERE r.id IN($route_id)";
				else $sql_route.=" AND r.id IN($route_id)";
			}
//            echo $sql_route.'<br>';
			$routes_rs=Yii::app()->db->createCommand($sql_route)->queryAll();

//			die;
			$this->render('index',array(
			'model'=>$model,
			'routes_rs'=>$routes_rs,			
			));
			return;
			
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequest']))
			$model->attributes=$_GET['SeatRequest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SeatRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seat-request-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionZone(){
		$id=$_POST['id'];
		$sql="SELECT id,route_no,route_detail from tbl_routes where zone_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		//var_dump($aql_rs);exit();
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}
	}
	public function actionRoute(){
		$id=$_POST['id']; 
		$sql="SELECT id,pickup_time,stoppage from tbl_stoppage where route_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}

	}
    public function actionRouteInformation(){
    	$route_id=$_POST['route_id'];
    	$user_pin=Yii::app()->user->username;
    	$check_route_sql="SELECT * FROM tbl_seat_request WHERE  user_pin='$user_pin'";
    	$check_route_sql=Yii::app()->db->createCommand($check_route_sql)->queryRow();
    	$stoppageId=$_POST['stoppageId'];
    	$expectedDate=$_POST['expectedDate'];
    	$changeRoute=$_POST['changeRoute'];
    	if(empty($check_route_sql)|| !empty($changeRoute)){ 
    	$html="<div class='center s002 '>
	   			<h4 class='heading-custom'>SUBMIT YOUR INFORMATION</h4>	   	
	   		 </div>";
    	if(!empty($route_id)){//route id have
    		$route=Routes::model()->findByPk($route_id);
    		if(!empty($route)){//Route found
    			$total_seat=$route['actual_seat'];
    			$borderPass=Routes::borderPass($route['route_no']);
    			$av_seat=$total_seat-$borderPass;
    			$loop_seats=$total_seat%4==0?$total_seat:$total_seat+(4-$total_seat%4);

    			$stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='".$route_id."' ORDER BY pickup_time";
    			$stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();

    			$driver_info_sql="SELECT d.* FROM tbl_vehicles as v INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin WHERE v.reg_no='".$route['vehicle_reg_no']."'";
    			$driver_info_rs=Yii::app()->db->createCommand($driver_info_sql)->queryRow();

    			$html.='<div class="accordion accordion-border clearfix">';
	   			$html.='<div class="col_one_third">';
	   			$html.='<div class="information-box">';
	   			$html.='<div class="steering clearfix">';
	   			$html.='<img  src="'.Yii::app()->theme->baseUrl.'/images/car/steering.jpg" alt="Steering">';
	   			$html.='</div>';
	   			$html.='<div class="clearfix"></div>';
	   			if($total_seat>0){//check total seat
	   				
	   				for($i=1; $i<=$loop_seats;$i++) {//seat loop start	   					
	   					if($i==1){
	   						$html.='<div class="seat-plan">';
	   					}
	   					if($i<=$borderPass){
	   						$html.='<div class="seat-location">'.$i.'</div>';
	   					}
	   					else{
	   						if($i>$total_seat){
	   						$html.='<div class="seat-location-white">X</div>';
	   						}
	   						else{
	   							$html.='<div class="seat-location-white">'.$i.'</div>';
	   						}
	   					}
	   					if($i%2==0 && $i%4!=0){
	   						   $html.='<div class="seat-location-gap"></div>';
	   					}	   						
	   					if($i%4==0){	   					
	   						$html.='</div>';
	   					$html.='<div class="clearfix"></div>';
	   						if(($i+1)<=$loop_seats)	   					
	   					$html.='<div class="seat-plan">';
	   					}   					
	   				}// seat loop end
	   				$html.='<div class="min-hight"></div>';
	   			}//check total seat
	   				$html.='</div>';
	   			$html.='</div>';
	   			//stoppage time div start
	   		$html.='<div class="col_one_third">';
	   			$html.='<div class="information-box">';
	   					$html.='<div class="center s002 ">';  

   							$html.='<h4 class="heading-custom">Route '.$route['route_no'].' Stoppages List</h4>';
   	
   						$html.='</div>';
   						$html.='<div class="clearfix"></div>';
   						$html.='<div class="stoppage-table">';
   							$html.='<div class="table-responsive">';

						$html.='<table class="table cart">';
							$html.='<thead>';
								$html.='<tr>';
									$html.='<th class="cart-product-price"><strong>Stoppage</strong></th>';
									$html.='<th class="cart-product-price"><strong>Time</strong></th>';
									
								$html.='</tr>';
							$html.='</thead>';
							$html.='<tbody>';
							if(count($stoppage_rs)>0){
								foreach ($stoppage_rs as $key => $value) {							
								$html.='<tr class="cart_item">';
									$html.='<th  class="cart-product-price">';
										$html.= $value['stoppage'];
									$html.='</th >';

									$html.='<th  class="cart-product-price">';
										$html.= $value['pickup_time'];
									$html.='</th >';

								$html.='</tr>';
								}
							}
							else{
								$html.='<tr class="cart_item">';
								$html.='<th colspan="2"  class="cart-product-price">';
								$html.= 'Stoppage Not Found';
								$html.='</th >';
								$html.='</tr>';

							}						
							$html.='</tbody>';

						$html.='</table>';

					$html.='</div>';
   						$html.='</div>';
	   			$html.='</div>';
	   		$html.='</div>';
	   		// Information div start
	   	
	   		$html.='<div class="col_one_third col_last">';
	   			$html.='<div class="information-box-last">';
	   				$html.='<div class="driver-info">';
	   				if($driver_info_rs){
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Name :</strong> '. $driver_info_rs['name'];
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Pin :</strong> ' . $driver_info_rs['pin'];
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Phone :</strong> ' . $driver_info_rs['phone'];
		   				$html.='</div>';
		   			}
		   			else{
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Information :</strong>  Not Found!';
		   				$html.='</div>';
		   			}
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Route No:</strong> '.$route['route_no'];
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Price :</strong> '.$route['price'];
		   				$html.='</div>';
	   				$html.='</div>';
	   				
	   			$html.='</div>';
	   			$html.='<div class="information-submit-button-div">';
	   				if(empty($changeRoute)){
	   				$html.=CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeSubmit('.$route_id.','.$stoppageId.',"'.$expectedDate.'")')); }
	   				if(!empty($changeRoute)){
	   					$html.=CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeSubmit('.$route_id.','.$stoppageId.',"'.$expectedDate.'","'.$changeRoute.'")')); }
	   			$html.='</div>';
	   		$html.='</div>';
	   		
	   	$html.='</div>';


    			
    		}//Route found
    		else{//Route not found
    			$html.='<div class="accordion accordion-border clearfix">
    								Route Not Found!
    							</div>';
    		}//Route not found
    	} //route id have
    	else{//route id not have
    		$html.='<div class="accordion accordion-border clearfix">
    								Route ID Not Found!
    							</div>';
    	}//route id not have
    	echo $html;
    }
    else{
    	
    	$route=Routes::model()->findByPk($route_id);
    	$model=$this->loadModel($check_route_sql['id']);
    	
    	$stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='$route_id' ORDER BY created_time";
    	$stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();
    	echo "<div class='center s002 '>
	   			<h4 class='heading-custom'>UPDATE YOUR APPLIED ROUTE NO: ".$route['route_no']." INFORMATION</h4>	   	
	   		 </div>";
	   
	  echo "<div class='center s002 '>";
		$form=$this->beginWidget('CActiveForm', array(
		   'id'=>'request-update-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('onSubmit'=>'return updateSeatRequest()'),
	)); 			
       		echo "<input type='hidden' name='SeatRequest[id]' id='SeatRequest-id' value='$model->id'>";
		echo "<div class='inner-form'> 
       		<div class='col_half'>
       		<label>Expected Date</label>
       		<div class='input-field first-wrap'>

		    			<div class='icon-wrap'>
			    			<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
				    			<path d='M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z'>				    	
				    			</path>
                			</svg>
						</div>";

					echo $form->textField($model,'expected_date',array('placeholder'=>'Expected Date','id'=>'depart1','class'=>'datepicker')); 
                
     		echo "</div></div>";
     	
     		echo "<div class='col_half'> 
	    		    <label>Stoppage</label>
				<div class='input-field first-wrap'>
		    		<div class='icon-wrap'>
			    		<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
							<path d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z'>						
							</path>
		        		</svg>
					</div>";						
				
				 
           
            echo "<select data-trigger='' name='SeatRequest[stoppage_id]' id='SeatRequest-stoppage-id'>";
            	
            	 foreach($stoppage_rs as $key=>$value)
				   {
	        			if($value['id']==$model->stoppage_id) 
	        				echo "<option value='".$value['id']."'  selected>".$value['stoppage']."</option>";		
	        			else	
	        		echo "<option value='".$value['id']."'>".$value['stoppage']."</option>";	
	        	}
				
			echo "</select>";	
			echo "</div></div></div>";
			echo "<div class='inner-form'> 
       		<div class='col_half'> 
	    		     <label>Status</label>
				<div class='input-field first-wrap'>
		    		<div class='icon-wrap'>
			    		<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
							<path d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z'>						
							</path>
		        		</svg>
					</div>";				
           
            echo "<select data-trigger='' name='SeatRequest[status]' id='SeatRequest-status'>";
            	
            	
	        			if($model->status==1){
	        				echo "<option value='1'  selected>Approved</option>";		
	        				echo "<option value='3'  >Cancel Request</option>";
	        			}
	        			if($model->status==0){
	        				echo "<option value='0'  selected>Pending</option>";       				
	        			}

	        			if($model->status==-1){
	        				echo "<option value='-1'  selected>Cancel</option>";       				
	        				echo "<option value='0'>Approved Request</option>";       				
	        			}	
	        			if($model->status==3){	        				     				
	        				echo "<option value='1'>Cancel Request</option>";       				
	        			}	
	        			if($model->status==2){
	        				echo "<option value='2'  selected>Maternity Leave Use</option>";       				
	        			}        				
	        	
				
			echo "</select>";	
			echo "</div></div>";
     	 echo "<div class='col_half'>
     		 <label>Update</label>
       		<div class='input-field first-wrap'>";
		          echo CHtml::submitButton("Update",array('class'=>'btn-search','id'=>'search-button')); 		
		echo "</div></div></div>";
     
    $this->endWidget();   
 	echo "</div>";
 	if(empty($changeRoute)&&$route_id!=$check_route_sql['route_id']){
	 		echo "<div class='center s002 '>";
	 		echo "<div class='input-field first-wrap'>";
		    echo CHtml::submitButton("Change Route Request",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeInformation('.$route_id.',"'.$stoppageId.'","'.$expectedDate.'","'.$check_route_sql['route_id'].'")')); 
		    echo "</div>";
	 		echo "</div>";
 		}
    	
    }
}
   public function actionUserRouetRquest(){
 
   		$model=new SeatRequest;
		/*$user_pin=$_GET['user_pin'];
		$model->user_pin = $user_pin;
		$model->created_by = $user_pin;

		$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
		$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
        $staffInfo = CJSON::decode(file_get_contents($uri));
    
        $model->user_level = $staffInfo[0]['JobLevel'];
        $model->user_name= $staffInfo[0]['StaffName'];
        $model->email=$staffInfo[0]['EmailID'];
        $model->user_cell=$staffInfo[0]['MobileNo'];
        $model->user_department=$staffInfo[0]['ProjectName'];
        $model->user_designation=$staffInfo[0]['DesignationName'];
        $model->residence_address=$staffInfo[0]['PresentAddress'];
		$model->created_time = date('Y-m-d H:i:s');*/

		$user_pin=Yii::app()->user->username;

		$model->user_pin = $user_pin;
		$model->user_name= Yii::app()->user->firstname." ".Yii::app()->user->lastname;
		$model->user_department=Yii::app()->user->department;
		$model->user_level = Yii::app()->user->level;
		$model->user_cell=Yii::app()->user->phone;
		$model->email=Yii::app()->user->email;
		$route_id=$_POST['route_id']; 
		$model->route_id=$route_id;		
		$model->stoppage_id=$_POST['stoppageId']; 
		$model->expected_date=$_POST['expectedDate'];
		if($_POST['changeRoute'])
		$model->status=4; 
		else	
		$model->status=0; 	
		$model->queue=0; 
		$model->created_by = $user_pin; 
		$model->created_time = date('Y-m-d H:i:s');       
        $model->user_designation=Yii::app()->user->designation;
        $model->residence_address=Yii::app()->user->address;	
       
		 	
    	if($model->save()){//route id have
    		echo "1";

    	}//route id have
    	else{//route id not have
    		$error= $model->getErrors();
    		//var_dump($error);
    		//var_dump(implode(',',$error));
    		echo "Sorry, Please Try Again!";
    	}

   }
   public function actionRequestList()
	{
		//$dataProvider=new CActiveDataProvider('SeatRequest');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		//echo "string";
		//exit();
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequest']))
			$model->attributes=$_GET['SeatRequest'];

		$this->render('requestList',array(
			'model'=>$model,
		));
	}
	public function actionRouteInformationUpdate()
	{	
		$id=$_POST['requestId'];
		$model=$this->loadModel($id);
		$model->status=$_POST['updateRequest'];
		if($model->status==0){
			$model->maternity_leave=0;
		}
		$model->updated_by=Yii::app()->user->username;
		$model->updated_time=date('Y-m-d H:i:s');
		if($model->save()){
			echo "Your Request Save Successfully!";
		}
		else{
			echo "Please, Try Again!";
		}
				
		

		
	}
	public function actionUserRouteRquestUpdate(){
		$id=$_POST['id'];
		$expDate=$_POST['expDate'];
		$seatRequestStoppageId=$_POST['seatRequestStoppageId'];
		$seatRequestStatus=$_POST['seatRequestStatus'];

		$model=$this->loadModel($id);
		$model->expected_date=$expDate;
		$model->stoppage_id=$seatRequestStoppageId;
		$model->status=$seatRequestStatus;
		$model->updated_by=Yii::app()->user->username;
		$model->updated_time=date('Y-m-d H:i:s');		
		if($model->save()){
			echo "1";
		}
		else{
    		$error= $model->getErrors();
    		//var_dump($error);
    		echo "Please, Try Again!";

    		
    	}			
		
	}
	public function actionMaternityUpdated(){
		$maternityId=$_POST['maternityId'];
		$sql_user_seat_request="SELECT * FROM tbl_seat_request WHERE user_pin='".Yii::app()->user->username."' AND status=1 AND queue>=1";
		$rs_user_seat_request=Yii::app()->db->createCommand($sql_user_seat_request)->queryRow();

		$seat_request_id=$rs_user_seat_request['id'];
		$maternity_user_pin=$rs_user_seat_request['user_pin'];
		$updated_time=date('Y-m-d H:i:s');

		$sql_update_user_seat_request="UPDATE tbl_seat_request SET maternity_leave=1 WHERE user_pin='".Yii::app()->user->username."' AND status=1 AND queue>=1";
		$rs_update_user_seat_request=Yii::app()->db->createCommand($sql_update_user_seat_request)->execute();

		$sql_maternity_leave_assign="INSERT INTO tbl_maternity_leave_assign (id,seat_request_id,maternity_user_pin,meternity_request_id,created_by,created_time) VALUES(null,'$seat_request_id','$maternity_user_pin',$maternityId,'$maternity_user_pin','$updated_time')";
		$rs_maternity_leave_assign=Yii::app()->db->createCommand($sql_maternity_leave_assign)->execute();
		if($rs_maternity_leave_assign){
			echo "<h3>Please, Wait For Admin Approval.</h3>";
		}
		else{
			echo "<h3>Please, Try Again!.</h3>";
		}
		
		
	}
}
