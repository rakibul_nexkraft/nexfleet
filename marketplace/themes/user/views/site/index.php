<style type="text/css">
    #all_route{
        display: none;
    }
    .cell-hover:hover{
        background: rgba(53,217,125,0.1);
    }
    .history-box{
        position: relative;
        border: 1px solid rgba(0,0,0,0.075);
        border-radius: 3px;
        text-align: center;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        background-color: #F5F5F5;
    }
    .history-extended{
        background-color: #FFF;
        text-align: left;
    }
    .history-header{
        /* border-bottom: 1px solid #dedede; */
        padding: 10px 20px;
        background-color: #2a57ff;
        background-image: linear-gradient(45deg, #fc3ba2, blue);
        box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
        /* width: 95%; */
        border-radius: 5px;
        margin: 0 auto;
        position: relative;
        top: 54px;
        color: white;
        z-index: 1;
    }
    .history-header > h3{
        color: white;
    }
    .tbl-header{
        /*box-shadow: 0 3px 2px -2px rgba(0,0,0,.3); */
        /* border-radius: 5px; */
        /* border: 1px solid #dedede; */
        background-color: #f2f2f2;
    }
    .heading-block:after {
        display: none;
    }
</style>
<section id="page-title" class="page-title-center page-title-nobg noborder nopadding margin-top">
    <div class="col_one_third border-custom pick_drop_summery cursor-pointer">
        <div class="col_full heading-block bg-grad-available nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">Expenditure</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm"><?php echo $user_data['total_purchase'] ?></div>
        </div>
    </div>
    <div class="col_one_third border-custom">
        <div class="col_full heading-block bg-grad-onduty nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">Earning</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm"><?php echo $user_data['total_sell'] ?></div>
        </div>
    </div>
    <div class="col_one_third col_last border-custom">
        <div class="col_full heading-block bg-grad-pickandrop nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">Withdraw</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm">0</div>
        </div>
    </div>
    <div class="clear"></div>
</section>
<!--<pre>< ?php print_r($userSeat->attributes) ?></pre>-->
<section id="section-about" class="page-section section nobg nomargin nopadding topmargin">

    <div class="heading-block bottommargin-sm center history-header">
        <h3>Top Purchase &amp; Sell</h3>
    </div>
    <div class="col_full" style="padding: 20px 20px 0;border: 1px solid #dedede;border-radius: 5px;">
        <div class="col_half topmargin-sm">
            <!--<label for="" style="font-size: 1.5rem">Last 5 Booking</label>-->
            <div class="heading-block fancy-title nobottomborder" style="margin-bottom: 10px;"><!--title-bottom-border-->
                <h4>Recent Purchases</h4>
            </div>
            <table class="table table-responsive table-hover">
                <thead class="tbl-header">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($recent_booking) == 0){
                    echo
                        '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '<tr>'.
                        '</tr>';
                }
                foreach ($recent_booking as $booking){
                    echo
                        '<tr>'.
                        '<td>'.$booking['release_date'].'</td>'.
                        '<td>'.$booking['route_id'].'</td>'.
                        '<td>'.$booking['travel_direction'].'</td>'.
                        '<td>'.$booking['price'].'</td>'.
                        '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
        <div class="col_half col_last topmargin-sm">
            <div class="heading-block fancy-title nobottomborder" style="margin-bottom: 10px;">
                <h4>Recent Sale</h4>
            </div>
            <table class="table table-responsive table-hover">
                <thead class="tbl-header">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($recent_sell) == 0){
                    echo
                        '<tr>'.
                        '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '</tr>';
                }
                foreach ($recent_sell as $sell){
                    echo
                        '<tr>'.
                        '<td>'.$sell['release_date'].'</td>'.
                        '<td>'.$sell['route_id'].'</td>'.
                        '<td>'.$sell['travel_direction'].'</td>'.
                        '<td>'.$sell['price'].'</td>'.
                        '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
        <div class="clear"></div>
    </div>
</section>