
<!--<pre>< ?php print_r($userSeat->attributes) ?></pre>-->
<section id="page-title" class="page-title-center page-title-nobg noborder" style="padding-bottom: 0px">
    <div class="col_one_third" style="/* border: 1px solid #DEDEDE; */padding: 10px 20px;border-radius: 3px;background: #00A9D4;box-shadow: inset 0 0 1px 1px rgba(0,0,0,.1);">
        <div class="col_one_third" style="margin-bottom: 0;">
            <i class="i-circled i-light icon-bar-chart" style=""></i>
        </div>
        <div class="col_two_third col_last" style="margin-bottom: 0;">
            <h4 style="color: white;margin-bottom: 0;">Today's Booking</h4>
            <span style="color: white;font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px black;margin-top: 4px;font-size: 22px;"><?php echo $user_data['total_booking'] ?></span>
        </div>
    </div>
    <div class="col_one_third" style="/* border: 1px solid #DEDEDE; */padding: 10px;border-radius: 3px;background: #FFC139;box-shadow: inset 0 0 1px 1px rgba(0,0,0,.1);">
        <div class="col_one_third" style="margin-bottom: 0;">
            <i class="i-circled i-light icon-bar-chart"></i>
        </div>
        <div class="col_two_third col_last" style="margin-bottom: 0;">
            <h4 style="color: white;margin-bottom: 0;">Today's Selling</h4>
            <span style="color: white;font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px black;margin-top: 4px;font-size: 22px;"><?php echo $user_data['total_selling'] ?></span>
        </div>
    </div>
    <div class="col_one_third col_last" style="/* border: 1px solid #DEDEDE; */padding: 10px;border-radius: 3px;background: #FF6B6D;box-shadow: inset 0 0 1px 1px rgba(0,0,0,.1);">
        <div class="col_one_third" style="margin-bottom: 0;">
            <i class="i-circled i-light icon-bar-chart"></i>
        </div>
        <div class="col_two_third col_last" style="margin-bottom: 0;">
            <h4 style="color: white;margin-bottom: 0;">Revenue</h4>
            <span style="color: white;font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px black;margin-top: 4px;font-size: 22px;">0.00</span>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<section id="page-title" class="page-title-center page-title-nobg noborder" >
    <div class="col_half ">
        <label for="" style="font-size: 1.5rem">Last 5 Booking</label>
        <table class="table table-hover">
            <thead style="box-shadow: 0 3px 2px -2px rgba(0,0,0,.3);">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($recent_booking) == 0){
                    echo
                        '<tr>'.
                            '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '</tr>';
                }
                foreach ($recent_booking as $booking){
                    echo
                        '<tr>'.
                            '<td>'.$booking['release_date'].'</td>'.
                            '<td>'.$booking['route_id'].'</td>'.
                            '<td>'.$booking['travel_direction'].'</td>'.
                            '<td>'.$booking['price'].'</td>'.
                        '</tr>';
                } ?>
            </tbody>
        </table>
    </div>
    <div class="col_half col_last">
        <label for="" style="font-size: 1.5rem">Last 5 Selling</label>
        <table class="table table-hover">
            <thead style="box-shadow: 0 3px 2px -2px rgba(0,0,0,.3);">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (count($recent_sell) == 0){
                    echo
                        '<tr>'.
                            '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '</tr>';
                }
                foreach ($recent_sell as $sell){
                    echo
                        '<tr>'.
                            '<td>'.$sell['release_date'].'</td>'.
                            '<td>'.$sell['route_id'].'</td>'.
                            '<td>'.$sell['travel_direction'].'</td>'.
                            '<td>'.$sell['price'].'</td>'.
                        '</tr>';
                } ?>
            </tbody>
        </table>
    </div>
</section>