
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" rel="stylesheet" />
    <!--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">-->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins.js"></script>

	<!-- Document Title
	============================================= -->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style>
        .margin-rl-auto{
            margin: 0 auto !important;
        }
        .sidepanel-close-btn{
            display: block !important;
            position: absolute;
            z-index: 12;
            top: 0;
            left: auto;
            right: 0;
            width: 40px;
            height: 40px;
            font-size: 18px;
            line-height: 40px;
            text-align: center;
            background-color: rgba(0,0,0,0.1);
            border-radius: 0 0 0 2px;
            color: white;
        }
        .nav-tree a{
            padding-left: 20px !important;
        }
        .nav-tree li{
            margin-bottom: 10px;
        }
        .nav-tree li.active{
            border: 1px solid #1ABC9C !important;
            border-radius: 3px;
        }
        .icon-zone{
            position: relative;
            top: -2px;
            margin-right: 6px;
            text-align: center;
            width: 14px;
        }
    </style>

</head>

<body class="stretched side-panel-left">

    <div class="body-overlay"></div>

    <div id="side-panel" class="dark">

        <div id="side-panel-trigger-close" class="side-panel-trigger"><a class="sidepanel-close-btn" href="#"><i class="icon-line-cross"></i></a></div>

        <div class="side-panel-wrap">

            <div class="widget clearfix">

                <h4 style="margin-left: 20px">Pages</h4>

                <nav class="nav-tree nobottommargin">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items'=>array(
                            array('label'=>'<i class="icon-home"></i>Home', 'url'=>substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"marketplace")).'pickndrop/index.php/site/test', 'linkOptions'=>array(), 'visible'=>!Yii::app()->user->isGuest,),
                            array('label'=>'<i class="icon-wallet"></i>Pick &amp; Drop', 'url'=>substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"marketplace")).'pickndrop/index.php', 'visible'=>!Yii::app()->user->isGuest),
                            array('label'=>'<i class="icon-briefcase"></i>Marketplace', 'url'=>array('site/index'), 'visible'=>!Yii::app()->user->isGuest),
                        ),
                        'encodeLabel' => false,
                        'htmlOptions'=>array('class'=>''),
                        'activeCssClass'=>'active',
                    ));
                    ?>
                </nav>

            </div>

            <!--<div class="widget quick-contact-widget clearfix">

                <h4>Contact US</h4>
                <div id="quick-contact-form-result" data-notify-type="success" data-notify-msg="<i class=icon-ok-sign></i> Message Sent Successfully!"></div>
                <form id="quick-contact-form" name="quick-contact-form" action="include/quickcontact.php" method="post" class="quick-contact-form nobottommargin" onsubmit="return false;">
                    <div class="form-process"></div>
                    <input type="text" class="required sm-form-control input-block-level" id="quick-contact-form-name" name="quick-contact-form-name" value="" placeholder="Full Name" />
                    <input type="text" class="required sm-form-control email input-block-level" id="quick-contact-form-email" name="quick-contact-form-email" value="" placeholder="Email Address" />
                    <textarea class="required sm-form-control input-block-level short-textarea" id="quick-contact-form-message" name="quick-contact-form-message" rows="4" cols="30" placeholder="Message"></textarea>
                    <input type="text" class="hidden" id="quick-contact-form-botcheck" name="quick-contact-form-botcheck" value="" />
                    <button type="submit" id="quick-contact-form-submit" name="quick-contact-form-submit" class="button button-small button-3d nomargin" value="submit" disabled="disabled">Send Email</button>
                </form>

                <script type="text/javascript">

                    jQuery("#quick-contact-form").validate({
                        submitHandler: function(form) {
                            jQuery(form).find('.form-process').fadeIn();
                            jQuery(form).ajaxSubmit({
                                target: '#quick-contact-form-result',
                                success: function() {
                                    jQuery(form).find('.form-process').fadeOut();
                                    jQuery(form).find('.sm-form-control').val('');
                                    jQuery('#quick-contact-form-result').attr('data-notify-msg', jQuery('#quick-contact-form-result').html()).html('');
                                    SEMICOLON.widget.notifications(jQuery('#quick-contact-form-result'));
                                }
                            });
                        }
                    });

                </script>

            </div>-->

        </div>

    </div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix margin-rl-auto">

        <style>
            .landing-form-overlay{position: relative}

            /* Profile sidebar */
            .profile-sidebar {
                padding: 20px 0 10px 0;
                background: #fff;
            }

            .profile-img-box-flex{
                display: flex;
            }
            .profile-img-box{
                height: 150px;
                width: 150px;
                display: flex;
                margin: 0 auto;
            }

            .profile-img-box img{
                -webkit-border-radius: 50% !important;
                -moz-border-radius: 50% !important;
                border-radius: 50% !important;
            }

            /*.profile-userpic img {
                float: none;
                margin: 0 auto;
                width: 50%;
                height: 50%;
                -webkit-border-radius: 50% !important;
                -moz-border-radius: 50% !important;
                border-radius: 50% !important;
            }*/

            .profile-usertitle {
                text-align: center;
                margin-top: 20px;
            }

            .profile-usertitle-name {
                color: #0b3e7e;
                font-size: 18px;
                font-weight: 600;
                margin-bottom: 7px;
                position: relative;
            }
            .profile-usertitle-name a{
                color: #0b3e7e;
            }
            .profile-usertitle-name a:hover{
                color: #4b81e1;
            }

            .profile-usertitle-job {
                text-transform: uppercase;
                color: #4fb057;
                font-size: 12px;
                font-weight: 600;
                margin-bottom: 15px;
            }

            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                padding: 10px 12px;
            }

            .profile-usertitle-name a#profile-edit-btn span{
                display: none;
                position: absolute;
                right: -16px;
                top: 0;
                margin-right: 20px;
            }
            .profile-usertitle-name:hover a#profile-edit-btn span{
                display: block;
            }

        </style>

        <!-- Header
		============================================= -->
        <header id="header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <!--<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>-->

                    <!-- Logo
                    ============================================= -->
                    <div id="logo" style="display: flex;align-items: center;">
                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?php echo Yii::app()->baseUrl; ?>/../common/assets/images/ifleet-logo-2.png" alt="BRAC Logo"></a>
                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?php echo Yii::app()->baseUrl; ?>/../common/assets/images/ifleet-logo-2.png" alt="BRAC Logo"></a>
                        <span class="txt" style="font-size: 38px;color: #868686;font-weight:bold;margin-left: 10px;">iFleet</span>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <!--<ul>
                            <li><a href="index.html"><div>Home</div></a></li>
                            <li class="current"><a href="#"><div>Features</div></a></li>
                            <li><a href="#"><div>Pages</div></a></li>
                            <li><a href="#"><div>Portfolio</div></a></li>
                            <li><a href="#"><div>Blog</div></a></li>
                            <li><a href="shop.html"><div>Shop</div></a></li>
                            <li><a href="#"><div>Shortcodes</div></a></li>
                        </ul>-->

                        <!-- Top Cart
                        ============================================= -->
                        <!--<div id="top-cart">
                            <a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>5</span></a>
                            <div class="top-cart-content">
                                <div class="top-cart-title">
                                    <h4>Shopping Cart</h4>
                                </div>
                                <div class="top-cart-items">
                                    <div class="top-cart-item clearfix">
                                        <div class="top-cart-item-image">
                                            <a href="#"><img src="images/shop/small/1.jpg" alt="Blue Round-Neck Tshirt" /></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="#">Blue Round-Neck Tshirt</a>
                                            <span class="top-cart-item-price">$19.99</span>
                                            <span class="top-cart-item-quantity">x 2</span>
                                        </div>
                                    </div>
                                    <div class="top-cart-item clearfix">
                                        <div class="top-cart-item-image">
                                            <a href="#"><img src="images/shop/small/6.jpg" alt="Light Blue Denim Dress" /></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="#">Light Blue Denim Dress</a>
                                            <span class="top-cart-item-price">$24.99</span>
                                            <span class="top-cart-item-quantity">x 3</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="top-cart-action clearfix">
                                    <span class="fleft top-checkout-price">$114.95</span>
                                    <button class="button button-3d button-small nomargin fright">View Cart</button>
                                </div>
                            </div>
                        </div>--><!-- #top-cart end -->

                        <!-- Top Search
                        ============================================= -->
                        <!--<div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div>--><!-- #top-search end -->

                        <div id="side-panel-trigger" class="side-panel-trigger"><a href="#" style="font-size: 2.5rem;"><i class="icon-reorder"></i></a></div>
                        <!--<a href="#" id="overlay-menu-close" class="visible-lg-block visible-md-block side-panel-trigger"><i class="icon-line-cross"></i></a>-->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->
		
	    <?php if (Yii::app()->user->isPickDrop() == '1' && Yii::app()->user->isAdmin() !='1'){ ?>
		<!-- Content
		============================================= -->
		<section id="content">

            <div class="container clearfix">

                <div id="side-navigation">

                    <div class="col_one_fifth">

                        <div class="row margin-top">
                            <div class="profile-sidebar" style="border: 1px solid #dddddd;border-radius: 5px;box-shadow: 0px 15px 10px -15px #d2d2d2;">
                                <div class="profile-userpic profile-img-box-flex">
                                    <div class="profile-img-box">
                                        <!--<img src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/magazine/thumb/person1_web.jpg" style="" class="img-responsive" alt="User image">-->
                                        <?php
                                        $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"marketplace"))."photo/".Yii::app()->user->username.".png";
                                         if (!file_exists($image_url)) {
                                            $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"marketplace"))."photo/"."person1_web.jpg";
                                        }

                                        ?>
                                        <img src="<?php echo $image_url; ?>" style="" class="img-responsive" alt="User image">
                                    </div>
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                        <a href="<?php echo Yii::app()->createUrl("/user/profile"); ?>"><?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname;?></a>
                                          <?php if(Yii::app()->user->myDeskUser==0){ ?>
                                        <a id="profile-edit-btn" href="<?php echo Yii::app()->createUrl("/user/profile/edit"); ?>"><span><i class="icon-edit"></i></span></a>
                                         <?php } ?>
                                    </div>
                                    <table class="table table-responsive margin-top">
                                        <tr>
                                            <th class="profile-usertitle-job">Username</th>
                                            <td><?php echo Yii::app()->user->username;?></td>
                                        </tr>
                                        <tr>
                                            <th class="profile-usertitle-job">Designation</th>
                                            <td><?php echo Yii::app()->user->designation;?></td>
                                        </tr>
                                        <tr>
                                            <th class="profile-usertitle-job">User Balance</th>
                                            <td>400</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row topmargin-sm">
                            <!--<ul class="sidenav">
                                <li class="ui-tabs-active"><a href="#snav-content1"><i class="icon-screen"></i>Seat Request<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content2"><i class="icon-magic"></i>Maternity Leave<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content3"><i class="icon-tint"></i>Market Place<i class="icon-chevron-right"></i></a></li>
                                <li><a href="#snav-content4"><i class="icon-gift"></i>History<i class="icon-chevron-right"></i></a></li>
                                <li><a href=""><i class="icon-gift"></i>LogOut<i class="icon-chevron-right"></i></a></li>

                            </ul>-->
                            <?php $this->widget('zii.widgets.CMenu',array(
                                'items'=>array(
                                    array('label'=>'<i class="icon-screen"></i> Dashboard<i class="icon-chevron-right"></i>', 'url'=>array('/'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    /*array('label'=>'<i class="icon-screen"></i>Seat Request<i class="icon-chevron-right"></i>', 'url'=>"",'items'=>array(
                                        array('label'=>'Choose Route', 'url'=>array('/seatRequest/index'),'visible'=>!Yii::app()->user->isGuest),
                                        array('label'=>'Request Route List', 'url'=>array('/seatRequest/requestList'),'visible'=>!Yii::app()->user->isGuest),),

                                        'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<i class="icon-magic"></i>Maternity Leave<i class="icon-chevron-right"></i>', 'url'=>"",'items'=>array(
                                        array('label'=>'Leave Request', 'url'=>array('/maternityLeaveRequest/create'),'visible'=>!Yii::app()->user->isGuest),
                                        array('label'=>'Leave Request List', 'url'=>array('/maternityLeaveRequest/index'),'visible'=>!Yii::app()->user->isGuest),),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        ),*/
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/market-place.png" alt="Market Place"></span> Market Place<i class="icon-chevron-right"></i>', 'url'=>array('/marketplace/index'),'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="History"></span> History<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Sell History"></span> Sell History<i class="icon-chevron-right"></i>', 'url'=>array('marketplace/sellHistory')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Booking History"></span> Booking History<i class="icon-chevron-right"></i>', 'url'=>array('marketplace/bookingHistory')),
                                        )
                                    ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/logout.png" alt="Logout"></span> Logout<i class="icon-chevron-right"></i>', 'url'=>array('/user/logout'),'visible'=>!Yii::app()->user->isGuest,
                                        ),

                                ),
                                'encodeLabel' => false,
                                'htmlOptions'=>array('class'=>'left-url'),
                                'activeCssClass'=>'ui-tabs-active',
                            ));?>
                        </div>
                    </div>

                    <div class="col_four_fifth col_last middle_margin">
                        <!--<div class="row">
                            <h1 class="center h1-text-shadow">More Routes, More Tickets</h1>
                            <h4 class="center nolineheight right-second-heading ">No. 1 online Ticketing Network</h4>

                        </div>-->
                        <div class="row">
                            <div id="snav-content">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- The Modal -->
                <div class="modal" id="myModal">
                    <div class="modal-dialog modal-dialog-scrollable">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                              <h1 class="modal-title"></h1>
                              <?php echo CHtml::button('x',array('data-dismiss'=>'modal','class'=>'close btn btn-success','onClick'=>'closedModel()'));
                                ?>
                              <div class="clearfix"></div>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                              <h3>Some text to enable scrolling..</h3>


                            </div>
                            <div class="clearfix"></div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <?php echo CHtml::button('Close',array('data-dismiss'=>'modal','class'=>'btn btn-success','onClick'=>'closedModel()'));
                                ?>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- End Model-->
            </div>
		</section><!-- #content end -->
	<?php } 
	else if (Yii::app()->user->isPickDrop() != '1' && Yii::app()->user->isAdmin() =='1'){ ?>
		<!-- Content
		============================================= -->
		<section id="content">

			


            <div class="container clearfix">
                <div id="side-navigation">
                    <!--<div class=''><button onClick="hideMenu()">Hide Menu</button></div>-->
                    <div class="col_one_fifth">
                        <div class="row margin-top">
                            <div class="profile-sidebar" style="border: 1px solid #dddddd;border-radius: 5px;box-shadow: 0px 15px 10px -15px #d2d2d2;">
                                <div class="profile-userpic profile-img-box-flex">
                                    <div class="profile-img-box">
                                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/magazine/thumb/person1_web.jpg" style="" class="img-responsive" alt="User image">
                                    </div>
                                </div>
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                        <a href="<?php echo Yii::app()->createUrl("/user/profile"); ?>"><?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname;?></a>
                                        <a id="profile-edit-btn" href="<?php echo Yii::app()->createUrl("/user/profile/edit"); ?>"><span><i class="icon-edit"></i></span></a>
                                    </div>
                                    <table class="table table-responsive margin-top">
                                        <tr>
                                            <th class="profile-usertitle-job">Username</th>
                                            <td><?php echo Yii::app()->user->username;?></td>
                                        </tr>
                                        <tr>
                                            <th class="profile-usertitle-job">Designation</th>
                                            <td><?php echo Yii::app()->user->designation;?></td>
                                        </tr>
                                        <!--<tr>
                                            <th class="profile-usertitle-job">User Balance</th>
                                            <td>400</td>
                                        </tr>-->
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row topmargin-sm">
                            <?php $this->widget('zii.widgets.CMenu',array(

                                'items'=>array(
                                    array('label'=>'<i class="icon-screen"></i> Dashboard<i class="icon-chevron-right"></i>', 'url'=>array('/'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/seat-allocation.png" alt="Seat Request List"></span> Seat Request List<i class="icon-chevron-right"></i>', 'url'=>array('/seatRequestAdmin/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/leave-request-list.png" alt="Maternity Leave List"></span> Maternity Leave List<i class="icon-chevron-right"></i>', 'url'=>array('/maternityLeaveRequestAdmin/index'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<i class="icon-home"></i> iFleet<i class="icon-chevron-right"></i>', 'url'=>substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"marketplace")).'index.php/user/login?pin='.Yii::app()->user->name.'&fleet=1','visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<i class="icon-user2"></i> User<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<i class="icon-user"></i> Admin Users<i class="icon-chevron-right"></i>', 'url'=>array('/user/user')),
                                            array('label'=>'<i class="icon-user"></i> Sister Concern Users<i class="icon-chevron-right"></i>', 'url'=>array('/user/user/sisterconcernuser')),
                                        )
                                    ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/zone.png" alt="Zone"></span> Zone<i class="icon-chevron-right"></i>', 'url'=>array('/zone/index'),'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/route.png" alt="Route"></span> Route<i class="icon-chevron-right"></i>', 'url'=>array('/routes/index'),'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/stoppage.png" alt="Stoppage"></span> Stoppage<i class="icon-chevron-right"></i>', 'url'=>array('/stoppage/index'),'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/seat-allocation.png" alt="Seat Allocation"></span> Seat Allocation<i class="icon-chevron-right"></i>', 'url'=>array('/commonfleets/index'),'visible'=>!Yii::app()->user->isGuest,
                                        ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="History"></span> History<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Sell History"></span> Sell History<i class="icon-chevron-right"></i>', 'url'=>array('marketplace/sellHistoryAdmin')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Booking History"></span> Booking History<i class="icon-chevron-right"></i>', 'url'=>array('marketplace/bookingHistoryAdmin')),
                                        )
                                    ),
                                    array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/logout.png" alt="Logout"></span> Logout<i class="icon-chevron-right"></i>', 'url'=>array('/user/logout'),'visible'=>!Yii::app()->user->isGuest,
                                        ),

                                ),
                                'encodeLabel' => false,
                                'htmlOptions'=>array('class'=>'left-url'),
                                'activeCssClass'=>'ui-tabs-active',

                            ));?>


                        </div>
                    </div>

                    <div class="col_four_fifth col_last middle_margin">
                        <div class="row">
                            <h1 class="center h1-text-shadow">More Routes, More Tickets</h1>
                            <h4 class="center nolineheight right-second-heading ">No. 1 online Ticketing Network</h4>

                        </div>
                        <div class="row">
                            <div id="snav-content">
                                <?php echo $content; ?>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- The Modal -->
                <div class="modal" id="myModal">
                    <div class="modal-dialog modal-dialog-scrollable">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                                <h1 class="modal-title"></h1>
                                <?php echo CHtml::button('x',array('data-dismiss'=>'modal','class'=>'close btn btn-success','onClick'=>'closedModel()'));
                                ?>
                                <div class="clearfix"></div>
                            </div>

                            <!-- Modal body -->
                            <div class="modal-body">
                                <h3>Some text to enable scrolling..</h3>
                            </div>

                            <div class="clearfix"></div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <?php echo CHtml::button('Close',array('data-dismiss'=>'modal','class'=>'btn btn-success','onClick'=>'closedModel()'));
                                ?>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- End Model-->

            </div>

		</section><!-- #content end -->
	<?php }
	else if (Yii::app()->user->isPickDrop() == '1' && Yii::app()->user->isAdmin() =='1')
		{
		echo "<h1>Please, Contact With Brack Transport Department.</h1>";
		$this->widget('zii.widgets.CMenu',array(
                'items'=>array(                    
                    array('label'=>'<i class="icon-line2-logout"></i>Logout<i class="icon-chevron-right"></i>', 'url'=>array('/user/logout'),'visible'=>!Yii::app()->user->isGuest,
                		),

                ),
                'encodeLabel' => false,
                'htmlOptions'=>array('class'=>'left-url'),
                'activeCssClass'=>'ui-tabs-active',
    			
                    ));
		}
		else{
			echo $content; 
		}
		//var_dump($_SESSION);
	?>


		<!-- Footer
		============================================= -->
		

	</div><!-- #wrapper end -->

    <!-- Footer Style -->
    <style>
        @media screen and (min-width: 768px) {
            .txt-right{
                text-align: right;
            }
            .txt-left{
                text-align: left;
            }
        }
        @media screen and (max-width: 767px) {
            .txt-right{
                text-align: center;
            }
            .txt-left{
                text-align: center;
            }
        }
    </style>
    <!-- Footer -->
    <footer id="footer" class="dark notopborder" style="background: url('<?php echo Yii::app()->theme->baseUrl; ?>/images/footer-bg.jpg') repeat fixed; background-size: 100% 100%;">
        <div id="copyrights">
            <div class="container">
                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix" style="padding: 0">
                    <div class="row clearfix" id="footerSection">
                        <div class="col-md-7 col-sm-6 txt-left">
                            <div class="widget clearfix">
                                <div class="clear-bottommargin-sm">
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <div class="copyrights-menu copyright-links clearfix">
                                                Copyrights © 2019 All Rights Reserved by iFleet.</div>
                                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                        </div>
                        <div class="col-md-5 col-sm-6 txt-right">
                            <div class="copyrights-menu copyright-links nobottommargin">
                                Made with
                                <svg width="14" height="14" viewBox="0 -2 24 24">
                                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" fill="red"></path>
                                </svg>
                                by<a class="link" href="http://www.nexkraft.com">NexKraft Limited</a>
                            </div>
                        </div>
                    </div>
                </div><!-- .footer-widgets-wrap end -->
            </div>
        </div>
        <!-- Copyrights
        ============================================= -->
        <!-- #copyrights end -->

    </footer>

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
	

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js"></script>
<!--    --><?php //echo Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/functions.js', CClientScript::POS_END); ?>
	<script>
		function closedModel(){
			$('#myModal').css('display','none');
		}
		function openModel(data,type=null){
			$('#myModal').css('display','block');			
			$('.modal-body').html(data);
		}
        $(".left-url>li" ).click(function() {
            $(this).children("ul").toggle("slow");

        });
        $( ".left-url>li" ).focusout(function() {
            $(this).children("ul").slideUp();

        });
		
	</script>
	

</body>
</html>