<?php

/**
 * This is the model class for table "{{error_log}}".
 *
 * The followings are the available columns in table '{{error_log}}':
 * @property integer $id
 * @property string $message
 * @property string $location
 * @property string $logid
 * @property string $tripid
 * @property string $vehicle_reg_no
 * @property string $driver_pin
 * @property string $message_type
 * @property string $message_details
 * @property string $created_time
 */
class ErrorLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ErrorLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{error_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('message,driver_pin, created_time,network_status', 'required'),
			array('message, location', 'length', 'max'=>255),
			array('user_message', 'length', 'max'=>2000),
			array('logid, tripid, vehicle_reg_no, driver_pin, message_type', 'length', 'max'=>128),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, message, location, logid, tripid, vehicle_reg_no, driver_pin, message_type, message_details, created_time,network_status,requisition_id,user_message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'message' => 'Message',
			'location' => 'Location',
			'logid' => 'Logid',
			'tripid' => 'Tripid',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'driver_pin' => 'Driver Pin',
			'message_type' => 'Message Type',
			'message_details' => 'Message Details',
			'created_time' => 'Created Time',
			'network_status'=>'Network Status',
			'user_message'=>'User Message',
			'requisition_id'=>'Requisition Id',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('logid',$this->logid,true);
		$criteria->compare('tripid',$this->tripid,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('driver_pin',$this->driver_pin,true);
		$criteria->compare('message_type',$this->message_type,true);
		$criteria->compare('message_details',$this->message_details,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('network_status',$this->network_status,true);
		$criteria->compare('user_message',$this->user_message,true);
		$criteria->compare('requisition_id',$this->requisition_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,			
			'pagination'=>array(
				'pageSize'=>45,
				
			),
			 'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}