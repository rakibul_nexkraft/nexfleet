<?php

/**
 * This is the model class for table "{{cngretests}}".
 *
 * The followings are the available columns in table '{{cngretests}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $vehicle_type
 * @property string $conversion_date
 * @property string $duedate_retest
 * @property string $actual_date_retest
 * @property string $next_duedate_retest
 * @property string $created_by
 * @property string $created_time
 * @property integer $active
 * @property string $updated_by
 * @property string $updated_time
 */
class Cngretests extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cngretests the static model class
	 */
    
        public $from_date;
        public $to_date;
        public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cngretests}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, vehicle_type, remarks, created_by, updated_by', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
                        array('conversion_date, duedate_retest, actual_date_retest, next_duedate_retest, created_time, updated_time','safe'),
			array('id, vehicle_reg_no, vehicle_type, conversion_date, duedate_retest, actual_date_retest, next_duedate_retest, created_by, created_time, active, updated_by, updated_time, from_date, to_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicle_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicle_type' => 'Vehicle Type',
			'conversion_date' => 'Conversion Date',
			'duedate_retest' => 'Due Date of Retest',
			'actual_date_retest' => 'Actual Date of Retest',
			'next_duedate_retest' => 'Next Due Date of Retest',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'active' => 'Active',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
                        'from_date' => 'From Due Date',
                        'to_date' => 'To Due Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicle_type',$this->vehicle_type,true);
		$criteria->compare('conversion_date',$this->conversion_date,true);
		$criteria->compare('duedate_retest',$this->duedate_retest,true);
		$criteria->compare('actual_date_retest',$this->actual_date_retest,true);
		$criteria->compare('next_duedate_retest',$this->next_duedate_retest,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
                
                if(!empty($this->from_date) && empty($this->to_date))
		{
			$criteria->addCondition('DATE(duedate_retest) = "'.$this->from_date.'"');
		}
		elseif(!empty($this->to_date) && empty($this->from_date))
		{
			$criteria->addCondition('DATE(duedate_retest) <= "'.$this->to_date.'"');
		}
		elseif(!empty($this->to_date) && !empty($this->from_date))
		{
			//$criteria->condition = "updated_time  >= '$this->from_date' and updated_time <= '$this->to_date'";
			$criteria->addCondition('DATE(duedate_retest) BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pageSize'=>45,
                        ),
		));
	}
}