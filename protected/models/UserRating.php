<?php

/**
 * This is the model class for table "{{user_rating}}".
 *
 * The followings are the available columns in table '{{user_rating}}':
 * @property integer $id
 * @property string $requisition_id
 * @property string $user_pin
 * @property string $driver_pin
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class UserRating extends CActiveRecord
{	
	public $from_date;
    public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserRating the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_rating}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('requisition_id, user_pin, driver_pin, u_rating', 'required'),
			array('requisition_id, user_pin, driver_pin, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, requisition_id, user_pin, driver_pin, created_time, updated_by, updated_time,from_date,to_date,u_rating', 'safe', 'on'=>'search'),
			//array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
            array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'requisition_id' => 'Requisition ID',
			'user_pin' => 'User Pin',
			'driver_pin' => 'Driver Pin',
			'u_rating'=>'User Rating',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'from_date' =>'Date From',
			'to_date' =>'To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('requisition_id',$this->requisition_id);
		$criteria->compare('user_pin',$this->user_pin);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('u_rating',$this->u_rating);
		$criteria->compare('created_time',$this->created_time);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('DATE(created_time) = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('DATE(created_time) <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('DATE(created_time) BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
	public function averageRating(){
		//$query="SELECT user_pin,AVG(u_rating) as u_rating FROM tbl_user_rating GROUP BY user_pin;";
		//$query=Yii::app()->db->createCommand($query)->queryAll();
		//var_dump($query);die();
		$criteria = new CDbCriteria();
		$criteria->select = "user_pin, AVG(u_rating) as u_rating";
		$criteria->group='user_pin';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
               'pageSize'=>40,
           ),
            'sort'=>array('defaultOrder'=>'id DESC')
	));
		//return new CActiveDataProvider($this, array(

			//'criteria'=>array(

				//'order'=>'t.id DESC',

				//'with'=>array('u_rating'=>array('select'=>array('AVG(u_rating) AS score'))),

			//)));
	}
	public function userRating1($pin){
		$query="SELECT user_pin,AVG(u_rating) as u_rating FROM tbl_user_rating where user_pin='$pin'";
		 $query=Yii::app()->db->createCommand($query)->queryAll();
		 if(!empty($query[0]['user_pin']))
		return $query[0];
		return null;
		//$criteria = new CDbCriteria();
		
		//$criteria->condition = "user_pin=:user_pin";
		//$criteria->params =array(':user_pin'=>$pin);
		//$criteria->select = "user_pin, AVG(u_rating) as u_rating";
		//$criteria->addCondition('user_pin == "'.$pin.'"');
		//return UserRating::model()->findAll($Criteria);
		
	}
}