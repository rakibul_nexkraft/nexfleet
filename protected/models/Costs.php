<?php

/**
 * This is the model class for table "{{costs}}".
 *
 * The followings are the available columns in table '{{costs}}':
 * @property integer $night_halt
 * @property integer $parking_charge
 * @property integer $toll
 * @property integer $seat_Rent
 * @property string $police_case
 * @property string $others
 */
class Costs extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Costs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{costs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no ','required'),
			array('toll_quantity, driver_pin', 'numerical', 'integerOnly'=>true),
			array('night_halt, parking_charge, toll, seat_Rent, breakfast, lunchbreak, police_case, others, total_cost', 'type', 'type'=>'float'),
			array('police_case, others,	toll_description, created_by', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bill_date, parking_charge_date, toll_date, seat_rent_date, breakfast_date, lunchbreak_date, police_case_date, others_date, created_by, created_time', 'safe'),
			array('night_halt, parking_charge, toll, seat_Rent, police_case, others, breakfast, lunchbreak, bill_date, parking_charge_date, toll_date, 
				seat_rent_date, breakfast_date, lunchbreak_date, police_case_date, others_date, driver_pin, total_cost, created_by, created_time', 'safe', 'on'=>'search'),
			array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vehicle_reg_no' => 'Vehicle Reg No',
			'night_halt' => 'Night Halt',
			'parking_charge' => 'Parking Charge',
			'toll' => 'Toll Amount',
			'toll_description' => 'Toll Description',
			'toll_quantity' => 'Toll Qantity',
			'seat_Rent' => 'Seat Rent',
			'police_case' => 'Police Case',
			'others' => 'Others Cost',
			'breakfast' => 'Breakfast Amount',
			'lunchbreak' => 'Lunchbreak Amount',
			'bill_date' => 'Billing Date',
			'total_cost' => 'Total Cost',
			'driver_pin' => 'Driver Pin',
			'created_time' => 'Created Time',
			'created_by' => 'Created By',
			
			'parking_charge_date' => 'Date of Parking Charge',
			'toll_date' => 'Date of Toll',
			'seat_rent_date' => 'Date of Seat Rent',
			'police_case_date' => 'Date of Police Case',
			'others_date' => 'Date of Others Cost',
			'breakfast_date' => 'Date of Breakfast Amount',
			'lunchbreak_date' => 'Date of Lunchbreak Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('night_halt',$this->night_halt);
		$criteria->compare('parking_charge',$this->parking_charge);
		$criteria->compare('toll',$this->toll);
		$criteria->compare('seat_Rent',$this->seat_Rent);
		$criteria->compare('police_case',$this->police_case,true);
		$criteria->compare('others',$this->others,true);
		$criteria->compare('bill_date',$this->bill_date);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('total_cost',$this->total_cost);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
		));
	}
}