<?php

/**
 * This is the model class for table "{{userdigital_log_book_history}}".
 *
 * The followings are the available columns in table '{{userdigital_log_book_history}}':
 * @property integer $id
 * @property string $login_id
 * @property string $trip_id
 * @property integer $requisition_id
 * @property string $login_end
 * @property string $trip_id_end
 * @property string $duty_day
 * @property integer $driver_pin
 * @property string $driver_name
 * @property string $user_pin
 * @property string $user_name
 * @property string $department
 * @property string $vehicle_reg_no
 * @property integer $vehicle_type_id
 * @property integer $dutytype_id
 * @property string $user_start_time_mobile
 * @property string $user_end_time_mobile
 * @property integer $meter_number
 * @property string $location_mobile
 * @property string $created_time_mobile
 * @property string $created_by
 * @property string $updated_time_mobile
 * @property string $updated_by
 */
class UserdigitalLogBookHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserdigitalLogBookHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{userdigital_log_book_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login_id, duty_day, user_name, department, created_by', 'required'),
			array('requisition_id, driver_pin, vehicle_type_id, dutytype_id, meter_number', 'numerical', 'integerOnly'=>true),
			array('login_id, trip_id, login_end, trip_id_end, duty_day, driver_name, user_pin, user_name, department, vehicle_reg_no, location_mobile, created_by, updated_by', 'length', 'max'=>128),
			array('user_start_time_mobile, user_end_time_mobile, created_time_mobile, updated_time_mobile', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, login_id, trip_id, requisition_id, login_end, trip_id_end, duty_day, driver_pin, driver_name, user_pin, user_name, department, vehicle_reg_no, vehicle_type_id, dutytype_id, user_start_time_mobile, user_end_time_mobile, meter_number, location_mobile, created_time_mobile, created_by, updated_time_mobile, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login_id' => 'Login',
			'trip_id' => 'Trip',
			'requisition_id' => 'Requisition',
			'login_end' => 'Login End',
			'trip_id_end' => 'Trip Id End',
			'duty_day' => 'Duty Day',
			'driver_pin' => 'Driver Pin',
			'driver_name' => 'Driver Name',
			'user_pin' => 'User Pin',
			'user_name' => 'User Name',
			'department' => 'Department',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicle_type_id' => 'Vehicle Type',
			'dutytype_id' => 'Dutytype',
			'user_start_time_mobile' => 'User Start Time Mobile',
			'user_end_time_mobile' => 'User End Time Mobile',
			'meter_number' => 'Meter Number',
			'location_mobile' => 'Location Mobile',
			'created_time_mobile' => 'Created Time Mobile',
			'created_by' => 'Created By',
			'updated_time_mobile' => 'Updated Time Mobile',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login_id',$this->login_id,true);
		$criteria->compare('trip_id',$this->trip_id,true);
		$criteria->compare('requisition_id',$this->requisition_id);
		$criteria->compare('login_end',$this->login_end,true);
		$criteria->compare('trip_id_end',$this->trip_id_end,true);
		$criteria->compare('duty_day',$this->duty_day,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicle_type_id',$this->vehicle_type_id);
		$criteria->compare('dutytype_id',$this->dutytype_id);
		$criteria->compare('user_start_time_mobile',$this->user_start_time_mobile,true);
		$criteria->compare('user_end_time_mobile',$this->user_end_time_mobile,true);
		$criteria->compare('meter_number',$this->meter_number);
		$criteria->compare('location_mobile',$this->location_mobile,true);
		$criteria->compare('created_time_mobile',$this->created_time_mobile,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_time_mobile',$this->updated_time_mobile,true);
		$criteria->compare('updated_by',$this->updated_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}