<?php

/**
 * This is the model class for table "{{accidents}}".
 *
 * The followings are the available columns in table '{{accidents}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $place
 * @property string $accident_date
 * @property string $accident_time
 * @property string $reason
 * @property string $demagedetail
 * @property integer $driver_id
 * @property string $created_time
 * @property string $created_by
 * @property integer $active
 */
class Accidents extends CActiveRecord
{
    public $from_date;
    public $to_date;
    public $driver_name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Accidents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{accidents}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, place, accident_date, driver_id', 'required'),
			array('driver_id, active,amount', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, place, accident_time, action_taken, created_by, driver_name', 'length', 'max'=>127),
            array('reason, demagedetail','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, place, accident_date, accident_time, reason, demagedetail, driver_id, created_time, created_by, active, from_date, to_date, amount, driver_name', 'safe', 'on'=>'search'),
			array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'vehicles'=>array(self::BELONGS_TO, 'Vehicles', 'reg_no'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'place' => 'Place',
			'accident_date' => 'Accident Date',
			'accident_time' => 'Accident Time',
			'reason' => 'Reason',
			'demagedetail' => 'Damage Detail',
			'driver_id' => 'Driver PIN',
			'created_time' => 'Creation Time',
			'created_by' => 'Created By',
			'active' => 'Active',
            'from_date' => 'Accident Date (From)',
            'to_date' => 'Accident Date (To)',
            'amount'=>'Amount',
            'driver_name'=>'Driver Name'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('t.place',$this->place,true);
		$criteria->compare('t.accident_date',$this->accident_date,true);
		$criteria->compare('t.accident_time',$this->accident_time,true);
		$criteria->compare('t.reason',$this->reason,true);
		$criteria->compare('t.demagedetail',$this->demagedetail,true);
		$criteria->compare('t.driver_id',$this->driver_id);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.active',$this->active);
		$criteria->compare('d.name',$this->driver_name,true);
		$criteria->join="LEFT JOIN tbl_drivers as d ON t.driver_id=d.pin ";
		$criteria->addCondition('t.driver_id<>0');
        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('t.accident_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('t.accident_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('t.accident_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
			),
			 'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
	
	public function grandTotal($criteria_get){

		$criteria=new CDbCriteria;

		$criteria->compare('id',$criteria_get['id']);
		$criteria->compare('vehicle_reg_no',$criteria_get['vehicle_reg_no'],true);
		$criteria->compare('place',$criteria_get['place'],true);
		$criteria->compare('accident_date',$criteria_get['accident_date'],true);
		$criteria->compare('accident_time',$criteria_get['accident_time'],true);
		$criteria->compare('reason',$criteria_get['reason'],true);
		$criteria->compare('demagedetail',$criteria_get['demagedetail'],true);
		$criteria->compare('driver_id',$criteria_get['driver_id']);
		$criteria->compare('created_time',$criteria_get['created_time'],true);
		$criteria->compare('created_by',$criteria_get['created_by'],true);
		$criteria->compare('active',$criteria_get['active']);
		$criteria->compare('amount',$criteria_get['amount']);

        if(!empty($criteria_get['from_date']) && empty($criteria_get['to_date']))
        {
            $criteria->addCondition('accident_date = "'.$criteria_get['from_date'].'"');
        }
        elseif(!empty($criteria_get['to_date']) && empty($criteria_get['from_date']))
        {
            $criteria->addCondition('accident_date <= "'.$criteria_get['to_date'].'"');
        }
        elseif(!empty($criteria_get['to_date']) && !empty($criteria_get['from_date']))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('accident_date BETWEEN "'.$criteria_get['from_date'].'" AND "'.$criteria_get['to_date'].'"');
        }
         $criteria->select="SUM(amount) as amount";
         $total_amount=Accidents::model()->find($criteria);
       
		return $total_amount['amount'];
	}
	public function driverName($pin){
        	$driver=Drivers::model()->findByAttributes(array('pin'=>$pin));
        	
        	return $driver['name'];

        }
}