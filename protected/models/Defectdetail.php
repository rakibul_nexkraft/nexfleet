<?php

/**
 * This is the model class for table "{{defectdetail}}".
 *
 * The followings are the available columns in table '{{defectdetail}}':
 * @property integer $id
 * @property integer $defect_id
 * @property string $job_name
 * @property string $action_name
 * @property integer $mechanic_id
 * @property string $mechanic_name
 */
class Defectdetail extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Accidents the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{defectdetail}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('defect_id, job_name, mechanic_id, mechanic_name', 'required'),
            array('defect_id, mechanic_id', 'numerical', 'integerOnly'=>true),
            array('job_name, mechanic_name', 'length', 'max'=>255),
            array('action_name', 'length', 'max'=>512),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, defect_id, job_name, action_name, mechanic_id, mechanic_name', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'defects'=>array(self::BELONGS_TO, 'Defects', 'defect_id'),
//            'vehicles'=>array(self::BELONGS_TO, 'Vehicles', 'reg_no'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'defect_id' => 'Defect ID',
            'job_name' => 'Job Name',
            'action_name' => 'Action Name',
            'mechanic_id' => 'Mechanic ID',
            'mechanic_name' => 'Mechanic Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('defect_id',$this->defect_id,true);
        $criteria->compare('job_name',$this->job_name,true);
        $criteria->compare('action_name',$this->action_name,true);
        $criteria->compare('accident_time',$this->accident_time,true);
        $criteria->compare('mechanic_id',$this->mechanic_id,true);
        $criteria->compare('mechanic_name',$this->mechanic_name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
//            'pagination'=>array(
//                'pageSize'=>40,
//            ),
        ));
    }
     public function workListDtail(){
        
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('defect_id',$this->defect_id,true);
        $criteria->compare('job_name',$this->job_name,true);
        $criteria->compare('action_name',$this->action_name,true);
        //$criteria->compare('accident_time',$this->accident_time,true);
        $criteria->compare('mechanic_id',$this->mechanic_id);
        $criteria->compare('mechanic_name',$this->mechanic_name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
           'pagination'=>array(
                'pageSize'=>40,
            ),
        ));
        
    }
}