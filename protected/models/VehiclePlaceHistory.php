<?php

/**
 * This is the model class for table "{{vehicle_place_history}}".
 *
 * The followings are the available columns in table '{{vehicle_place_history}}':
 * @property integer $id
 * @property string $log_id
 * @property string $trip_id
 * @property string $place
 * @property integer $meter
 * @property string $created_by
 * @property string $created_time_mobile
 * @property string $created_time_vts
 * @property string $updated_by
 * @property string $update_time
 */
class VehiclePlaceHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VehiclePlaceHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vehicle_place_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trip_id, place, meter', 'required'),
			array('meter', 'numerical', 'integerOnly'=>true),
			array('log_id', 'length', 'max'=>255),
			array('trip_id', 'length', 'max'=>225),
			array('place', 'length', 'max'=>525),
			array('created_by, updated_by', 'length', 'max'=>128),
			array('created_time_mobile, created_time_vts, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, log_id, trip_id, place, meter, created_by, created_time_mobile, created_time_vts, updated_by, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'log_id' => 'Log',
			'trip_id' => 'Trip',
			'place' => 'Place',
			'meter' => 'Meter',
			'created_by' => 'Created By',
			'created_time_mobile' => 'Created Time Mobile',
			'created_time_vts' => 'Created Time Vts',
			'updated_by' => 'Updated By',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('log_id',$this->log_id,true);
		$criteria->compare('trip_id',$this->trip_id,true);
		$criteria->compare('place',$this->place,true);
		$criteria->compare('meter',$this->meter);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time_mobile',$this->created_time_mobile,true);
		$criteria->compare('created_time_vts',$this->created_time_vts,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}