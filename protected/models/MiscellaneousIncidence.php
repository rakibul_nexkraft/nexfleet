<?php

/**
 * This is the model class for table "{{miscellaneous_accident}}".
 *
 * The followings are the available columns in table '{{miscellaneous_accident}}':
 * @property integer $id
 * @property integer $driver_pin
 * @property string $vehicle_reg_no
 * @property string $detail
 * @property string $date
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class MiscellaneousIncidence extends CActiveRecord
{
	public $name;
	public $phone;
	public $date_to;
	public $date_from;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MiscellaneousAccident the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{miscellaneous_incidence}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('driver_pin,  detail, date, created_by', 'required'),
			array('driver_pin,phone', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, created_by, updated_by,name,date_from,date_to', 'length', 'max'=>128),
			array('created_time, updated_time,date_from,date_to', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, driver_pin, vehicle_reg_no, detail, date, created_by, created_time, updated_by, updated_time,name,phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'driver_pin' => 'Driver Pin',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'detail' => 'Detail',
			'date' => 'Date',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'name'=>'Name',
			'phone'=>'Phone',
			'date_from'=>'From Date',
			'date_to'=>'To Date ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
	
		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.driver_pin',$this->driver_pin);
		$criteria->compare('t.vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('t.detail',$this->detail,true);
		$criteria->compare('t.date',$this->date,true);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.updated_by',$this->updated_by,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('r.name',$this->name,true);
		$criteria->compare('r.phone',$this->phone,true);

		$criteria->join="INNER JOIN tbl_drivers as r ON t.driver_pin=r.pin";
		if(!empty($this->date_from) && empty($this->date_to))
		{
			$criteria->addCondition('t.date = "'.$this->date_from.'"');
		}
		elseif(!empty($this->date_to) && empty($this->date_from))
		{
			$criteria->addCondition('t.date <= "'.$this->date_to.'"');
		}
		elseif(!empty($this->date_to) && !empty($this->date_to))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('t.date BETWEEN "'.$this->date_from.'" AND "'.$this->date_to.'"');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}