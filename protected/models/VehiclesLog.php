<?php

/**
 * This is the model class for table "{{vehicles_log}}".
 *
 * The followings are the available columns in table '{{vehicles_log}}':
 * @property string $id
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property string $engine_no
 * @property string $cc
 * @property string $vmodel
 * @property string $chassis_no
 * @property string $country
 * @property string $purchase_date
 * @property integer $purchase_price
 * @property string $supplier
 * @property integer $seat_capacity
 * @property string $load_capacity
 * @property string $ac
 * @property integer $driver_pin
 * @property string $created_time
 * @property string $created_by
 * @property integer $active
 */
class VehiclesLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VehiclesLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vehicles_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, vehicletype_id, engine_no, cc, vmodel, chassis_no, purchase_date, purchase_price, supplier, driver_pin', 'required'),
			array('vehicletype_id, purchase_price, seat_capacity, driver_pin, active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, model_code, engine_no, chassis_no, country, supplier, location, created_by, updated_by', 'length', 'max'=>127),
			array('cc, vmodel, load_capacity', 'length', 'max'=>20),
			array('ac', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, vehicletype_id, engine_no, cc, vmodel, chassis_no, country, purchase_date, purchase_price, supplier, seat_capacity, load_capacity, ac, driver_pin, created_time, created_by, active, updated_time, updated_by', 'safe', 'on'=>'search'),
            array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'vehicles'=>array(self::BELONGS_TO, 'Vehicles', 'vehicle_reg_no'),
			'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicle Type',
			'engine_no' => 'Engine No',
			'cc' => 'CC',
			'vmodel' => 'Yr of MFG',
            'model_code' => 'Model Code',
			'chassis_no' => 'Chassis No',
			'country' => 'Country',
			'purchase_date' => 'Purchase Date',
			'purchase_price' => 'Purchase Price',
			'supplier' => 'Supplier',
			'seat_capacity' => 'Seat Capacity',
			'load_capacity' => 'Load Capacity',
			'ac' => 'Ac',
			'driver_pin' => 'Driver Pin',
			'location' => 'Location',
			'created_time' => 'Creation Time',
			'created_by' => 'Created By',
			'active' => 'Active',
			'updated_time' => 'Updated Time',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('engine_no',$this->engine_no,true);
		$criteria->compare('cc',$this->cc,true);
		$criteria->compare('vmodel',$this->vmodel,true);
        $criteria->compare('model_code',$this->model_code,true);
		$criteria->compare('chassis_no',$this->chassis_no,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('purchase_price',$this->purchase_price);
		$criteria->compare('supplier',$this->supplier,true);
		$criteria->compare('seat_capacity',$this->seat_capacity);
		$criteria->compare('load_capacity',$this->load_capacity,true);
		$criteria->compare('ac',$this->ac,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('location',$this->location);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
	}
        
        public function status($ac)
        {
            if($ac == 1)
            return 'Active';
        elseif($ac == 2)
            return 'For Auction';
        elseif($ac == 3)
            return 'Sold';
        }
}