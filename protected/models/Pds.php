<?php

/**
 * This is the model class for table "{{pds}}".
 *
 * The followings are the available columns in table '{{pds}}':
 * @property integer $id
 * @property string $service_date
 * @property string $route
 * @property string $provided
 * @property string $reasons
 * @property string $remarks
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 * @property integer $active
 */
class Pds extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pds the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pds}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('service_date, route, provided, reasons', 'required'),
			array('created_by, updated_by, active', 'numerical', 'integerOnly'=>true),
			array('route, provided, reasons, remarks', 'length', 'max'=>127),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, service_date, route, provided, reasons, remarks, created_by, created_date, updated_by, updated_date, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'service_date' => 'Service Date',
			'route' => 'Route',
			'provided' => 'Provided',
			'reasons' => 'Reasons',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('service_date',$this->service_date,true);
		$criteria->compare('route',$this->route,true);
		$criteria->compare('provided',$this->provided,true);
		$criteria->compare('reasons',$this->reasons,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}