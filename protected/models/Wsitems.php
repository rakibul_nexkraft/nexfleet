<?php

/**
 * This is the model class for table "{{wsitems}}".
 *
 * The followings are the available columns in table '{{wsitems}}':
 * @property integer $id
 * @property string $item_name
 * @property string $purchase_type
 * @property string $purchase_date
 * @property string $item_size
 * @property integer $wssupplier_id
 * @property string $wssupplier_name
 * @property string $vehicle_reg_no
 * @property string $item_slno
 * @property double $bill_amount
 * @property string $bill_date
 * @property string $quantity
 * @property string $warranty
 * @property integer $unit_price
 * @property integer $total_price
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsitems extends CActiveRecord
{
    public $from_date;
    public $to_date;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsitems the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsitems}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_name, quantity, unit_price, total_price', 'required'),
			array('wsitemname_id,wssupplier_id,  wssupplier_id', 'numerical', 'integerOnly'=>true),
			array('unit_price, total_price, bill_amount', 'numerical'),
			array('item_name, vehicle_reg_no, wsrequisition_requisition_no, wssupplier_name, vehicle_model, parts_no, vehicle_code, challan_no, bill_no, quantity, item_size, purchase_type, remarks, bin, part, created_by, updated_by', 'length', 'max'=>127),
			array('purchase_type, item_size', 'length', 'max'=>20),
			array('warranty', 'length', 'max'=>60),
			array('purchase_date, bill_date, requisition_date, challan_date ','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_name, purchase_type, purchase_date, item_size, wssupplier_id, wssupplier_name, item_slno, bill_amount, bill_date, quantity, warranty, unit_price, total_price, created_by, created_time, updated_by, updated_time, from_date,to_date', 'safe', 'on'=>'search'),
			array('created_time','default','value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
			array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'wsstocks'=>array(self::HAS_MANY, 'Wsstock', 'wsitemname_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_name' => 'Item Name',
			'wsitemname_id' => 'Item Name ID',
            'wsrequisition_requisition_no' => 'Requisition No',
            'requisition_date' => 'Requisition Date',
			'purchase_type' => 'Purchase Type',
			'purchase_date' => 'Purchase Date',
			'item_size' => 'Item Size',
            'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicle_model' => 'Model Code',
            'parts_no' => 'Parts No',
			'vehicle_code' => 'Vehicle Code',
			'wssupplier_id' => 'supplier ID',
			'wssupplier_name' => 'Supplier Name',
			'challan_no' => 'Challan No',
            'challan_date' => 'Challan Date',
            'bill_no' => 'Bill No',
			'bill_amount' => 'Bill Amount',
			'bill_date' => 'Bill Date',
			'quantity' => 'Quantity',
			'warranty' => 'Warranty',
			'unit_price' => 'Unit Price',
			'total_price' => 'Total Price',
			'bin'=>'Bin No',
            'part'=>'Part',
            'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
            'from_date'=>'Bill Date(From)',
            'to_date'=>'Bill Date(To)',
		);
	}

    public function actionGetVehicleModel() {
        if (!empty($_GET['term']))
        {
             $qterm = '%'.$_GET['term'].'%';
            //$sql = "SELECT v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.vehicle_model  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";
            $sql = "SELECT distinct v.vehicle_model as value FROM tbl_wsitemnames as v  WHERE v.vehicle_model LIKE '$qterm' ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }

    public function actionGetPartNo() {
        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';
            //$sql = "SELECT v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.vehicle_model  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";
            $sql = "SELECT v.parts_no as value FROM tbl_wsitemnames as v  WHERE v.parts_no LIKE '$qterm' ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }

    public function actionGetItemName() {

        echo "test";
        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';

          echo   $vehicle_model = $_GET['vehicle_model'];
           echo  $parts_no  = $_GET['parts_no'];

            //$sql = "SELECT v.name as value,v.id as wsitemname_id, v.vehicle_model, v.vehicle_type  FROM tbl_wsitemnames as v WHERE name LIKE '$qterm'";
            $sql = "SELECT v.name as value,v.id as wsitemname_id, v.vehicle_model  FROM tbl_wsitemnames as v WHERE name LIKE '$qterm'
             and v.vehicle_model = '$vehicle_model' and v.parts_no= '$parts_no'";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item_name',$this->item_name,true);
        $criteria->compare('wsrequisition_requisition_no',$this->wsrequisition_requisition_no,true);
        $criteria->compare('requisition_date',$this->requisition_date,true);
		$criteria->compare('purchase_type',$this->purchase_type,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('item_size',$this->item_size,true);
        $criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicle_code',$this->vehicle_code,true);
		$criteria->compare('vehicle_model',$this->vehicle_model,true);
        $criteria->compare('parts_no',$this->parts_no,true);
		$criteria->compare('wssupplier_id',$this->wssupplier_id);
		$criteria->compare('wssupplier_name',$this->wssupplier_name,true);
		$criteria->compare('challan_no',$this->challan_no,true);
        $criteria->compare('challan_date',$this->challan_date,true);
        $criteria->compare('bill_no',$this->bill_no,true);
		$criteria->compare('bill_amount',$this->bill_amount);
		$criteria->compare('bill_date',$this->bill_date,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('warranty',$this->warranty,true);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('total_price',$this->total_price);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('bill_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('bill_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('bill_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }



        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}