<?php

/**
 * This is the model class for table "{{cardistributions}}".
 *
 * The followings are the available columns in table '{{cardistributions}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $vehicle_type
 * @property string $user_pin
 * @property string $user_name
 * @property string $recidence
 * @property string $driver_pin
 * @property string $driver_name
 * @property string $created_by
 * @property string $created_time
 * @property integer $active
 */
class Cardistributions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cardistributions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cardistributions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no', 'required'),
			array('id, active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, vehicle_type, user_name, user_desig, driver_name, created_by, updated_by', 'length', 'max'=>127),
			array('user_pin, driver_pin', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
                        array('recidence, created_time, updated_time','safe'),
			array('id, vehicle_reg_no, vehicle_type, user_pin, user_name, recidence, driver_pin, driver_name, created_by, created_time, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicle_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicle_type' => 'Vehicle Type',
			'user_pin' => 'User PIN',
			'user_name' => 'User Name',
            'user_desig' => 'User Designation',
			'recidence' => 'Residence',
			'driver_pin' => 'Driver PIN',
			'driver_name' => 'Driver Name',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'active' => 'Active',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicle_type',$this->vehicle_type,true);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('user_name',$this->user_name,true);
        $criteria->compare('user_desig',$this->user_desig,true);
		$criteria->compare('recidence',$this->recidence,true);
		$criteria->compare('driver_pin',$this->driver_pin,true);
		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
			),
		));
	}
}