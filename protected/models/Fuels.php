<?php

/**
 * This is the model class for table "{{fuels}}".
 *
 * The followings are the available columns in table '{{fuels}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $fuel_type
 * @property double $quantity
 * @property double $value
 * @property double $unit_price
 * @property string $purchase_date
 * @property integer $fuel_station_id
 * @property integer $driver_pin
 * @property string $created_by
 * @property string $created_time
 * @property integer $active
 */
class Fuels extends CActiveRecord
{

    public $from_date;
    public $to_date;
    public $driver_name;
  

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fuels the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuels}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quantity, value, fuel_station_id, driver_pin,vehicle_reg_no,fuel_type,purchase_date', 'required'),
			array('id, fuel_station_id, driver_pin, active', 'numerical', 'integerOnly'=>true),
			array('quantity, unit_price, value', 'numerical'),
			array('vehicle_reg_no, meter_reading,driver_name', 'length', 'max'=>127),
			array('fuel_type, created_by, active', 'length', 'max'=>30),
			array('purchase_date, created_time,driver_name', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, fuel_type, quantity, value, purchase_date, fuel_station_id, driver_pin, requisition_id, receipt_no, created_by, created_time, active,from_date,to_date,driver_name', 'safe', 'on'=>'search'),
			array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'fuelstations'=>array(self::BELONGS_TO, 'Fuelstations', 'fuel_station_id'),
            'fuels_log'=>array(self::HAS_MANY, 'FuelsLog', 'fuel_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'fuel_type' => 'Fuel Type',
			'meter_reading' => 'Meter Reading',
			'quantity' => 'Quantity',
			'unit_price'=>'Unit Price',
			'value' => 'Cost',
			'purchase_date' => 'Purchase Date',
			'fuel_station_id' => 'Fuel Station',
			'driver_pin' => 'Driver PIN',
			'requisition_id'=>'Requisition Id',
			'receipt_no'=>'Receipt No',
			'created_by' => 'Created By',
			'created_time' => 'Creation Time',
			'active' => 'Active',
            'from_date' => 'Purchase Date (From)',
            'to_date' => 'Purchase Date (To)',
            'driver_name' => 'Driver Name',
           
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('fuel_type',$this->fuel_type,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('value',$this->value);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('fuel_station_id',$this->fuel_station_id);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('active',$this->active);
		
		//$criteria->order = 'id DESC';
        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('purchase_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('purchase_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('purchase_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
			),
			'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}