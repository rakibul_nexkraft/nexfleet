<?php

/**
 * This is the model class for table "{{daily_vehicle_check}}".
 *
 * The followings are the available columns in table '{{daily_vehicle_check}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property integer $mobil
 * @property integer $battery
 * @property integer $extra_wheel
 * @property integer $break
 * @property integer $tools
 * @property integer $radiator_water
 * @property integer $indicator_light
 * @property integer $fornt_back_light
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 * @property string $check_date
 */
class DailyVehicleCheck extends CActiveRecord
{
	public $from_date;
    public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DailyVehicleCheck the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{daily_vehicle_check}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no', 'required'),
			array('mobil, battery, extra_wheel, break, tools, radiator_water, indicator_light, fornt_back_light', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, created_by, updated_by', 'length', 'max'=>128),
			array('created_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, mobil, battery, extra_wheel, break, tools, radiator_water, indicator_light, fornt_back_light, created_by, created_time, updated_by, updated_time, check_date, from_date, to_date,', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'mobil' => 'Mobil',
			'battery' => 'Battery',
			'extra_wheel' => 'Extra Wheel',
			'break' => 'Break',
			'tools' => 'Tools',
			'radiator_water' => 'Radiator Water',
			'indicator_light' => 'Indicator Light',
			'fornt_back_light' => 'Fornt Back Light',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'check_date' => 'Check Date',
			'from_date' =>'Date From',
			'to_date' =>'To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('mobil',$this->mobil);
		$criteria->compare('battery',$this->battery);
		$criteria->compare('extra_wheel',$this->extra_wheel);
		$criteria->compare('break',$this->break);
		$criteria->compare('tools',$this->tools);
		$criteria->compare('radiator_water',$this->radiator_water);
		$criteria->compare('indicator_light',$this->indicator_light);
		$criteria->compare('fornt_back_light',$this->fornt_back_light);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		//$criteria->compare('check_date',$this->check_date,true);
		if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('check_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('check_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('check_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
			),
		  'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
	public function check($a)
    {
        if($a==1)
            return "Yes";
       
		else
			return "No";
    }
}