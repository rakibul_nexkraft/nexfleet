<?php

/**
 * This is the model class for table "{{tasks}}".
 *
 * The followings are the available columns in table '{{tasks}}':
 * @property integer $id
 * @property integer $defect_id
 * @property string $task_desc
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Tasks extends CActiveRecord
{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tasks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tasks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('defect_id, service_name, work_type', 'required'),
			array('id, defect_id,wsservice_id', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, remarks, created_by, work_type, service_provider, bill_no, bill_date, out_bill_amount, quantity, updated_by,service_name', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, service_provider, work_type, service_name, defect_id,  created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
            array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
            array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'defects'=>array(self::BELONGS_TO, 'Defects', 'defect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'defect_id' => 'Defect',
            'vehicle_reg_no' => 'Vehicle Reg No',
            'service_name' => 'Service Name',
            'service_provider' => 'Service Provider',
            'bill_no' => 'Bill No',
            'bill_date' => 'Bill Date',
            'quantity' => 'Quantity',
            'out_bill_amount' => 'Bill Amount(Unit)',
            'remarks' => 'Remarks',
            'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('defect_id',$this->defect_id);		
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('service_name',$this->service_name,true);
		$criteria->compare('service_provider',$this->service_provider,true);
		$criteria->compare('work_type',$this->work_type,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
		));
	}
}