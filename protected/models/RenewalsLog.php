<?php

/**
 * This is the model class for table "{{renewals_log}}".
 *
 * The followings are the available columns in table '{{renewals_log}}':
 * @property integer $id
 * @property integer $renewal_id
 * @property string $vehicle_reg_no
 * @property string $present_fitness_date
 * @property string $next_fitness_date
 * @property string $present_tax_date
 * @property string $next_tax_date
 * @property string $present_insurance_date
 * @property string $next_insurance_date
 * @property string $present_routpermit_date
 * @property string $next_routpermit_date
 * @property string $updated_by
 * @property string $updated_time
 * @property integer $active
 */
class RenewalsLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RenewalsLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{renewals_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	/*public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('renewal_id, vehicle_reg_no, present_fitness_date, next_fitness_date, present_tax_date, next_tax_date, present_insurance_date, next_insurance_date, present_routpermit_date, next_routpermit_date, created_time, updated_by, updated_time, active', 'required'),
			array('id, renewal_id, active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, created_by, updated_by', 'length', 'max'=>127),
           // array('created_time', 'numerical', 'integerOnly'=>true),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, renewal_id, vehicle_reg_no, present_fitness_date, next_fitness_date, present_tax_date, next_tax_date, present_insurance_date, next_insurance_date, present_routpermit_date, next_routpermit_date,  updated_by, updated_time, active', 'safe', 'on'=>'search'),
            array('updated_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
		);
	}*/

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('renewal_id,vehicle_reg_no, present_fitness_date, next_fitness_date, present_tax_date, next_tax_date, present_insurance_date, next_insurance_date, present_routpermit_date, next_routpermit_date', 'required'),
            array('active, registration_amount, insurance_amount, routpermit_amount, tax_token, advanced_tax_amount, number_plate_cost, miscellaneous_fee', 'numerical', 'integerOnly'=>true),
            array('created_by, vehicle_reg_no, updated_by', 'length', 'max'=>127),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
			array('registration_date, registration_amount, insurance_amount, routpermit_amount, tax_token, advanced_tax_amount, number_plate_cost, miscellaneous_fee','safe'),
            array('id, renewal_id, vehicle_reg_no, present_fitness_date, next_fitness_date, present_tax_date, next_tax_date, present_insurance_date, next_insurance_date, present_routpermit_date, next_routpermit_date, created_time, created_by, updated_time,updated_by, active', 'safe', 'on'=>'search'),
            //array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
            array('updated_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'renewals'=>array(self::BELONGS_TO, 'Renewals', 'renewal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'renewal_id' => 'Renewal ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'present_fitness_date' => 'Present Fitness Date',
			'next_fitness_date' => 'Next Fitness Date',
			'present_tax_date' => 'Present Tax Date',
			'next_tax_date' => 'Next Tax Date',
			'present_insurance_date' => 'Present Insurance Date',
			'next_insurance_date' => 'Next Insurance Date',
			'present_routpermit_date' => 'Present Routpermit Date',
			'next_routpermit_date' => 'Next Routpermit Date',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'created_by' => 'Created By',
			'created_time' => 'Creation Time',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('renewal_id',$this->renewal_id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('present_fitness_date',$this->present_fitness_date,true);
		$criteria->compare('next_fitness_date',$this->next_fitness_date,true);
		$criteria->compare('present_tax_date',$this->present_tax_date,true);
		$criteria->compare('next_tax_date',$this->next_tax_date,true);
		$criteria->compare('present_insurance_date',$this->present_insurance_date,true);
		$criteria->compare('next_insurance_date',$this->next_insurance_date,true);
		$criteria->compare('present_routpermit_date',$this->present_routpermit_date,true);
		$criteria->compare('next_routpermit_date',$this->next_routpermit_date,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
	}
}