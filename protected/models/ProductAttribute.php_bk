<?php

/**
 * This is the model class for table "{{product_attribute}}".
 *
 * The followings are the available columns in table '{{product_attribute}}':
 * @property integer $id
 * @property string $category
 * @property string $label
 * @property string $value
 * @property integer $product_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 *
 * The followings are the available model relations:
 * @property Product $product
 */
class ProductAttribute extends CActiveRecord
{
	public $product_attributes_array= array(
		"model" => "Model",
		"vehicle_type" => "Vehicle Type",
		"unit" => "Unit",
		"size" => "Size",
		"parts_no" => "Parts No",
	);
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductAttribute the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{product_attribute}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('label, value, product_id', 'required'),
			array('product_id', 'numerical', 'integerOnly'=>true),
			array('category, label, created_by, updated_by', 'length', 'max'=>150),
			array('value', 'length', 'max'=>300),
			array('updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category, label, value, product_id, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'label' => 'Label',
			'value' => 'Value',
			'product_id' => 'Product',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$this->product_id=$_GET['id'];
		
		$criteria->compare('id',$this->id);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('product_id',$this->product_id);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}