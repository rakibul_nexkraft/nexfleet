<?php

/**
 * This is the model class for table "{{wsitemgns}}".
 *
 * The followings are the available columns in table '{{wsitemgns}}':
 * @property integer $id
 * @property string $item_name
 * @property integer $wsitemname_id
 * @property string $wsrequisition_requisition_no
 * @property string $purchase_type
 * @property string $purchase_date
 * @property string $item_size
 * @property string $vehicle_model
 * @property string $vehicle_code
 * @property integer $wssupplier_id
 * @property string $wssupplier_name
 * @property string $challan_no
 * @property string $challan_date
 * @property double $bill_amount
 * @property string $bill_date
 * @property string $quantity
 * @property string $warranty
 * @property double $unit_price
 * @property double $total_price
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsitemgns extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsitemgns the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsitemgns}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_name, vehicle_reg_no, vehicle_model, quantity, unit_price, total_price', 'required'),
			array('wsitemname_id, wssupplier_id', 'numerical', 'integerOnly'=>true),
			array('bill_amount, unit_price, total_price', 'numerical'),
			array('item_name, wsrequisition_requisition_no, vehicle_model, vehicle_code, wssupplier_name, challan_no, quantity, created_by, updated_by', 'length', 'max'=>127),
			array('purchase_type, item_size', 'length', 'max'=>20),
			array('warranty', 'length', 'max'=>60),
            array('purchase_date, challan_date, bill_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_name, wsitemname_id, wsrequisition_requisition_no, purchase_type, purchase_date, item_size, vehicle_model, vehicle_code, wssupplier_id, wssupplier_name, challan_no, challan_date, bill_amount, bill_date, quantity, warranty, unit_price, total_price, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
				array('created_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
			array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'wsstockgns'=>array(self::HAS_MANY, 'Wsstockgn', 'wsitemgn_id'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_name' => 'Item Name',
			'wsitemname_id' => 'Item Name ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'wsrequisition_requisition_no' => 'Requisition No',
			'purchase_type' => 'Purchase Type',
			'purchase_date' => 'Purchase Date',
			'item_size' => 'Item Size',
			'vehicle_model' => 'Model Code',
			'vehicle_code' => 'Vehicle Code',
			'wssupplier_id' => 'Supplier ID',
			'wssupplier_name' => 'Supplier Name',
			'challan_no' => 'Challan No',
			'challan_date' => 'Challan Date',
			'bill_amount' => 'Bill Amount',
			'bill_date' => 'Bill Date',
			'quantity' => 'Quantity',
			'warranty' => 'Warranty',
			'unit_price' => 'Unit Price',
			'total_price' => 'Total Price',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('wsitemname_id',$this->wsitemname_id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);		
		$criteria->compare('wsrequisition_requisition_no',$this->wsrequisition_requisition_no,true);
		$criteria->compare('purchase_type',$this->purchase_type,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('item_size',$this->item_size,true);
		$criteria->compare('vehicle_model',$this->vehicle_model,true);
		$criteria->compare('vehicle_code',$this->vehicle_code,true);
		$criteria->compare('wssupplier_id',$this->wssupplier_id);
		$criteria->compare('wssupplier_name',$this->wssupplier_name,true);
		$criteria->compare('challan_no',$this->challan_no,true);
		$criteria->compare('challan_date',$this->challan_date,true);
		$criteria->compare('bill_amount',$this->bill_amount);
		$criteria->compare('bill_date',$this->bill_date,true);
		$criteria->compare('quantity',$this->quantity,true);
		$criteria->compare('warranty',$this->warranty,true);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('total_price',$this->total_price);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}