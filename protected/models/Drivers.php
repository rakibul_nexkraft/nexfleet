<?php

/**
 * This is the model class for table "{{drivers}}".
 *
 * The followings are the available columns in table '{{drivers}}':
 * @property integer $id
 * @property string $name
 * @property integer $pin
 * @property integer $phone
 * @property integer $active
 */
class Drivers extends CActiveRecord
{
	public $from_date;
	public $to_date;
	public $training;
	public $training_date_from;
	public $training_date_to;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Drivers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{drivers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, pin, phone, active', 'required'),
			array('pin, phone,defence_traning,gen_awarness_traning,	google_map_training, training', 'numerical', 'integerOnly'=>true),
			array('name, dvr_license_no, dvr_license_type, issued_from, blood_group,driver_rating', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
            array('issued_date, valid_to, defence_traning_date, gen_awarness_traning_date, google_map_training_date,training_date_from,training_date_to','safe'),
			array('id, name, pin, phone, active, defence_traning, gen_awarness_traning, google_map_training, training', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'pin' => 'PIN',
			'phone' => 'Phone',
			'active' => 'Active',
            'dvr_license_no' => 'Driving License No',
            'dvr_license_type' => 'Driving License Type',
            'issued_from' => 'Issued From',
            'issued_date' => 'Date of Issue',
            'blood_group' => 'Blood Group',
            'valid_to' => 'Valid Till',
            'driver_rating'=>'Driver Rating',
           'defence_traning'=>'Defensive Driving Training',
            'gen_awarness_traning'=>'Gender Awareness Training',
            'defence_traning_date'=>'Defensive Driving Training Date',
            'gen_awarness_traning_date'=>'Gender Awareness Training Date',
            'google_map_training'=>'Google Map Training',
            'google_map_training_date'=>'Google Map Training Date',
            'training'=>'Training',
            'training_date_to'=>' To Training Date',
            'training_date_from'=>' From Training Date'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		if(isset($this->training)&&!empty($this->training)){
			if($this->training=='4'){
			   $this->defence_traning=1;
			   $this->gen_awarness_traning=1;
			   $this->google_map_training=1;
			  
			   if((isset($this->training_date_from) && !empty($this->training_date_from))&&(isset($this->training_date_to) && !empty($this->training_date_to))){

			   	   $criteria->addCondition('(defence_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'") AND (gen_awarness_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'") AND(google_map_training_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'")');

			   }
			   elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)){

			   	   $criteria->addCondition('defence_traning_date >= "'.$this->training_date_from.'" AND gen_awarness_traning_date >= "'.$this->training_date_from.'"  AND google_map_training_date >= "'.$this->training_date_from.'"');

			   }
			   elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))){

					$criteria->addCondition('defence_traning_date <= "'.$this->training_date_to.'" AND gen_awarness_traning_date <= "'.$this->training_date_to.'"  AND google_map_training_date <= "'.$this->training_date_to.'"');
				}
				else{}

			}
			elseif($this->training=='3'){				
				$this->google_map_training=1;
				 if((isset($this->training_date_from) && !empty($this->training_date_from))&&(isset($this->training_date_to) && !empty($this->training_date_to))){

			   	   $criteria->addCondition('google_map_training_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'"');

			   }
			   elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)){

			   	   $criteria->addCondition('google_map_training_date >= "'.$this->training_date_from.'"');

			   }
			   elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))){

					$criteria->addCondition('google_map_training_date <= "'.$this->training_date_to.'"');
				}
				else{}
			}
			elseif($this->training=='2'){				
				$this->gen_awarness_traning=1;
				 if((isset($this->training_date_from) && !empty($this->training_date_from))&&(isset($this->training_date_to) && !empty($this->training_date_to))){

			   	   $criteria->addCondition('gen_awarness_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'"');

			   }
			   elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)){

			   	   $criteria->addCondition('gen_awarness_traning_date >= "'.$this->training_date_from.'"');

			   }
			   elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))){

					$criteria->addCondition('gen_awarness_traning_date <= "'.$this->training_date_to.'"');
				}
				else{}
			}
			elseif($this->training=='1'){				
				$this->defence_traning=1;
				if((isset($this->training_date_from) && !empty($this->training_date_from))&&(isset($this->training_date_to) && !empty($this->training_date_to))){

			   	   $criteria->addCondition('defence_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'"');

			   }
			   elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)){

			   	   $criteria->addCondition('defence_traning_date>= "'.$this->training_date_from.'"');

			   }
			   elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))){

					$criteria->addCondition('defence_traning_date <= "'.$this->training_date_to.'"');
				}
				else{}
			}
			else{}
		}
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('pin',$this->pin);
		$criteria->compare('phone',$this->phone);
		$criteria->compare('active',$this->active);
		$criteria->compare('dvr_license_no',$this->dvr_license_no,true);
		$criteria->compare('dvr_license_type',$this->dvr_license_type);
		$criteria->compare('issued_from',$this->issued_from,true);
		$criteria->compare('issued_date',$this->issued_date);
		$criteria->compare('valid_to',$this->valid_to);
		$criteria->compare('blood_group',$this->blood_group,true);
		$criteria->compare('driver_rating',$this->driver_rating);
		$criteria->compare('defence_traning',$this->defence_traning);
		$criteria->compare('gen_awarness_traning',$this->gen_awarness_traning);
		//$criteria->compare('defence_traning_date',$this->defence_traning_date);
		//$criteria->compare('gen_awarness_traning_date',$this->gen_awarness_traning_date);
		$criteria->compare('google_map_training',$this->google_map_training);
		//$criteria->compare('google_map_training_date',$this->google_map_training_date);

		//$criteria->order="id desc";

        $criteria->addCondition("active ='Yes'");

       	return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
public function search1()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		

		$criteria=new CDbCriteria;
        $criteria->select = array('avg(f.driver_rating) as driver_rating','t.pin','t.name','t.phone','t.id');
		
	
		

		if(!empty($_GET["Drivers"]["pin"])){
			$this->pin=$_GET["Drivers"]["pin"];
			$criteria->compare('t.pin',$this->pin);
		}
		
	if(!empty($_GET["Drivers"]["from_date"]) && !empty($_GET["Drivers"]["to_date"])){

			$criteria->addCondition('DATE(f.created_time)>="'.$_GET["Drivers"]["from_date"].'" AND DATE(f.created_time)<="'.$_GET["Drivers"]["to_date"].'"');
		}
		elseif(!empty($_GET["Drivers"]["from_date"]) && empty($_GET["Drivers"]["to_date"])){
			$criteria->addCondition('DATE(f.created_time)>="'.$_GET["Drivers"]["from_date"].'"');
		}
		elseif(empty($_GET["Drivers"]["from_date"]) && !empty($_GET["Drivers"]["to_date"])){
			$criteria->addCondition('"DATE(f.created_time)<="'.empty($_GET["Drivers"]["to_date"]).'"');
		}
		else{}
		//$criteria->with='id0';
		$criteria->order="t.id desc";
			$criteria->join=" Left join tbl_requisitions as r on t.pin=r.driver_pin Left join tbl_feedback_rating as f on r.id=f.id";
		$criteria->group = 't.pin';
		if(!empty($_GET["Drivers"]["driver_rating"])){
			
		
		$criteria->having='avg(f.driver_rating)='.$_GET["Drivers"]["driver_rating"];
		
		}
		//else{
		//$criteria->join=" Left join tbl_requisitions as r on t.pin=r.driver_pin Left join tbl_feedback_rating as f on r.id=f.id";
		//$criteria->group = 't.pin';
    	//}
   $criteria->addCondition('t.active="Yes"');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
            ),
		)
			
	);
	}



 public function driverslog(){
        

    
    

         $sql = " SELECT a.pin,a.name,b.vehicle_reg_no,b.vehicletype_id, c.type FROM tbl_drivers AS a
INNER JOIN tbl_vehicles_log AS b
ON a.pin = b.driver_pin
INNER JOIN tbl_vehicletypes AS c
ON c.id = b.vehicletype_id

WHERE a.pin = '".$this->pin."'";
        


    
    
    
    
    
    

        return new CSqlDataProvider($sql, array(
       //  'criteria'=>$criteria,
      
             'pagination'=>array(
             'pageSize'=>30,
            ),
        ));

    }
 public function driversTraining(){
         $sql = " SELECT a.name,a.phone,t.driver_pin,t.defence_traning, t.gen_awarness_traning,t.google_map_training,t.google_map_training_date, t.gen_awarness_traning_date,t.defence_traning_date FROM tbl_drivers AS a
INNER JOIN tbl_driver_training AS t ON a.pin = t.driver_pin WHERE a.pin = '".$this->pin."'";
    
        return new CSqlDataProvider($sql, array(      //  'criteria'=>$criteria,
            'pagination'=>array(
             'pageSize'=>30,
            ),
        ));

    }
public function driversCrashReport(){
       $sql = " SELECT a.name,a.pin,t.vehicle_reg_no,t.place,t.accident_date,t.accident_time,t.reason,t.demagedetail,t.amount FROM tbl_drivers AS a
INNER JOIN tbl_accidents AS t ON a.pin = t.driver_id WHERE a.pin = '".$this->pin."'";

        return new CSqlDataProvider($sql, array(
       //  'criteria'=>$criteria,
      
             'pagination'=>array(
             'pageSize'=>30,
            ),
        ));

    }
    public function getColor(){
    	$valid_to=$this->valid_to;
    	$sub_date=strtotime('-1 month',strtotime($valid_to));

    	//$time1 = DATEADD(m, -1, $this->valid_to);
    	
    	if(date("Y-m-d",$sub_date) < date("Y-m-d")){
    		return "fontColorRed";
    	}
    }
    public function getAllDriverPin() {
    	$data         = array();
        $packageModel = Drivers::model()->findAll("active='Yes'");
        foreach($packageModel as $get){
            $data[$get->pin] = $get->pin;
        }
        return $data;
    }
    public function getAllDriverName() {
    	$data         = array();
        $packageModel = Drivers::model()->findAll("active='Yes'");
        foreach($packageModel as $get){
            $data[$get->name] = $get->name;
        }
        return $data;
    }
    
    
}
