<?php

/**
 * This is the model class for table "{{invoice_line}}".
 *
 * The followings are the available columns in table '{{invoice_line}}':
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $product_classification_id
 * @property integer $product_attributes_id
 * @property double $quantity
 * @property double $unit_price
 * @property string $unit
 * @property double $total_price
 * @property integer $manufacture_id
 * @property integer $supplier_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 *
 * The followings are the available model relations:
 * @property Invoice $invoice
 */
class InvoiceLine extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InvoiceLine the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{invoice_line}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('invoice_id, product_classification_id, product_attributes_id, quantity, unit, created_by', 'required'),
			array('invoice_id, product_classification_id, product_attributes_id, manufacture_id, supplier_id', 'numerical', 'integerOnly'=>true),
			array('quantity, unit_price, total_price', 'numerical'),
			array('unit, created_by, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invoice_id, product_classification_id, product_attributes_id, quantity, unit_price, unit, total_price, manufacture_id, supplier_id, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'invoice' => array(self::BELONGS_TO, 'Invoice', 'invoice_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_id' => 'Invoice',
			'product_classification_id' => 'Product Classification',
			'product_attributes_id' => 'Product Attributes',
			'quantity' => 'Quantity',
			'unit_price' => 'Unit Price',
			'unit' => 'Unit',
			'total_price' => 'Total Price',
			'manufacture_id' => 'Manufacture',
			'supplier_id' => 'Supplier',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invoice_id',$this->invoice_id);
		$criteria->compare('product_classification_id',$this->product_classification_id);
		$criteria->compare('product_attributes_id',$this->product_attributes_id);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('total_price',$this->total_price);
		$criteria->compare('manufacture_id',$this->manufacture_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	// <!-- Task by Rakib -->
	public function getProductClassification($id){

        $sql = "SELECT name FROM tbl_product_classification where id=$id";
				$command = Yii::app()->db->createCommand($sql);
				$all_queried_value = $command->queryRow();
				return $all_queried_value['name'];
				// print_r($all_queried_value);


    }

    public function getProductAttributes($id){

        $sql = "SELECT label FROM  tbl_product_attributes where id=$id";
				$command = Yii::app()->db->createCommand($sql);
				$all_queried_value = $command->queryRow();
				return $all_queried_value['label'];
				// print_r($all_queried_value);


    }

    public function getProductManufacturer($id){

        $sql = "SELECT name FROM  tbl_manufacturer where id=$id";
				$command = Yii::app()->db->createCommand($sql);
				$all_queried_value = $command->queryRow();
				return $all_queried_value['name'];
				// print_r($all_queried_value);


    }

    public function getProductSupplier($id){

        $sql = "SELECT supplier_name FROM  tbl_supplier where id=$id";
				$command = Yii::app()->db->createCommand($sql);
				$all_queried_value = $command->queryRow();
				return $all_queried_value['supplier_name'];
				// print_r($all_queried_value);


    }
    // <!-- Task by Rakib -->
}