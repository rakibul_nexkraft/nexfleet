<?php

/**
 * This is the model class for table "{{auctions}}".
 *
 * The followings are the available columns in table '{{auctions}}':
 * @property integer $id
 * @property integer $vehicletype_id
 * @property string $vehicle_reg_no
 * @property string $sold_to
 * @property integer $selling_price
 * @property string $date_of_work_order
 * @property string $upload_scan
 */
 
 
 
 



class Auctions extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Auctions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{auctions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicletype_id, vehicle_reg_no, sold_to, selling_price, date_of_work_order', 'required'),
                        array('vehicle_reg_no', 'unique', 'message'=>'This vehicle already exists.'),
			array('vehicletype_id, selling_price,active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, sold_to, created_by,updated_by,updated_time', 'length', 'max'=>127),
			array('upload_scan', 'file', 'types'=>'jpg, jpeg, gif, png, doc, docx, xls, xlsx, ppt, pptx, pdf, txt, zip, rar', 'allowEmpty'=>true, 'on'=>'insert,update'),
			array('upload_scan', 'length', 'max'=>300, 'on'=>'insert,update'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicletype_id, vehicle_reg_no, sold_to, selling_price, date_of_work_order, upload_scan, active', 'safe', 'on'=>'search'),
                        array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
          
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
        
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicletype_id' => 'Vehicletype',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'sold_to' => 'Sold To',
			'selling_price' => 'Selling Price',
			'date_of_work_order' => 'Date Of Work Order',
			'upload_scan' => 'Upload Scan',
            'active' => 'Status',
            
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('sold_to',$this->sold_to,true);
		$criteria->compare('selling_price',$this->selling_price);
		$criteria->compare('date_of_work_order',$this->date_of_work_order,true);
		$criteria->compare('upload_scan',$this->upload_scan,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    
        public function status($st)
        {
            if($st == 1)
                return '<span style="color:green;">For Sale</span>';
            elseif ($st == 2)
                return '<span style="color:red;">Sold</span>';
        }
        

}