<?php

/**
 * This is the model class for table "{{wsious}}".
 *
 * The followings are the available columns in table '{{wsious}}':
 * @property integer $id
 * @property string $iou_date
 * @property string $user_name
 * @property string $user_pin
 * @property string $designation
 * @property double $iou_amount
 * @property string $receive_date
 * @property string $due_date_adjust
 * @property string $adjust_date
 * @property string $remarks
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsious extends CActiveRecord
{
    public $from_date;
    public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsious the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsious}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('iou_date, user_pin, iou_amount, location ', 'required'),
			array('iou_amount', 'numerical'),
			array('user_name, user_pin, designation, remarks, created_by, updated_by', 'length', 'max'=>127),
            array('receive_date,due_date_adjust,adjust_date','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, iou_date, user_name, user_pin, designation, iou_amount, receive_date, due_date_adjust, adjust_date, remarks, created_by, created_time, updated_by, updated_time,from_date,to_date', 'safe', 'on'=>'search'),
                        array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
                        array('updated_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'iou_date' => 'IOU Date',
			'user_name' => 'User Name',
			'user_pin' => 'User PIN',
			'designation' => 'Designation',
			'iou_amount' => 'IOU Amount',
			'receive_date' => 'Receiving Date',
			'due_date_adjust' => 'Due Date Adjustment',
			'adjust_date' => 'Adjusted Date',
			'remarks' => 'Remarks',
			'location' => 'Location',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
            'from_date'=>'Receive Date (From)',
            'to_date'=>'Receive Date (To)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('iou_date',$this->iou_date,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('designation',$this->designation,true);
		$criteria->compare('iou_amount',$this->iou_amount);
		$criteria->compare('receive_date',$this->receive_date,true);
		$criteria->compare('due_date_adjust',$this->due_date_adjust,true);
		$criteria->compare('adjust_date',$this->adjust_date,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('location',$this->location);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('receive_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('receive_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('receive_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}