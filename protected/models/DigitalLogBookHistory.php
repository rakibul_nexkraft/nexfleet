<?php

/**
 * This is the model class for table "{{digital_log_book_history}}".
 *
 * The followings are the available columns in table '{{digital_log_book_history}}':
 * @property integer $id
 * @property string $login_id
 * @property string $trip_id
 * @property integer $requisition_id
 * @property string $login_end
 * @property string $trip_id_end
 * @property string $duty_day
 * @property integer $driver_pin
 * @property string $user_pin
 * @property string $user_name
 * @property string $department
 * @property string $vehicle_reg_no
 * @property integer $dutytype_id
 * @property string $driver_start_time_vts
 * @property string $driver_start_time_mobile
 * @property string $driver_end_time_vts
 * @property string $driver_end_time_mobile
 * @property double $meter_number
 * @property string $location_vts
 * @property string $location_mobile
 * @property string $fuel_recipt_no
 * @property double $fuel_total_bill
 * @property double $night_halt_bill
 * @property double $morning_ta
 * @property double $lunch_ta
 * @property double $night_ta
 * @property double $running_per_km_rate
 * @property double $running_km
 * @property double $running_km_bill
 * @property double $over_time_hour_rate
 * @property double $over_time
 * @property double $over_time_bill
 * @property double $total_bill
 * @property string $created_time_vts
 * @property string $created_time_mobile
 * @property string $created_by
 * @property string $updated_time_vts
 * @property string $updated_time_mobile
 * @property string $updated_by
 */
class DigitalLogBookHistory extends CActiveRecord
{
	 public $from_date;
	 public $to_date;
	 public $adjust_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DigitalLogBookHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{digital_log_book_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('duty_day, driver_pin, miei', 'required'),
			array('id, requisition_id, driver_pin, dutytype_id,  vehicle_type_id, total_on_count', 'numerical', 'integerOnly'=>true),
			array('night_halt_bill, morning_ta, lunch_ta, night_ta, running_per_km_rate, running_km, running_km_bill, over_time_hour_rate,  over_time_bill, total_bill, fuel_total_bill, fuel_total_bill, fuel_quantity, speed, vts_longitude, vts_latitude, others_bill,meter_number,missing_KM, new_meter_number, new_km', 'numerical'),
			array('login_id, trip_id, miei, login_end, trip_id_end, duty_day, user_pin, user_name, department, 	vehicle_reg_no, location_vts, location_mobile, fuel_recipt_no, api_status, vts_message, created_by, updated_by, others_cost, others_description, day_end, vehicle_mode, driver_name, bundle', 'length', 'max'=>128),
			array('driver_start_time_vts, driver_start_time_mobile, driver_end_time_vts, driver_end_time_mobile, updated_time_vts, updated_time_mobile, first_on_time, last_off_time, over_time, vehicle_mode, objection,repair_start_time,repair_end_time,create_time_server, message_log,bundle,adjust_date,new_meter_number, new_km', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, login_id, trip_id, ignition_status, requisition_id, login_end, trip_id_end, duty_day, driver_pin, user_pin, user_name, department,vehicle_reg_no, dutytype_id, driver_start_time_vts, driver_start_time_mobile, driver_end_time_vts, driver_end_time_mobile, meter_number, location_vts, location_mobile, fuel_recipt_no, fuel_total_bill, night_halt_bill, morning_ta, lunch_ta, night_ta, running_per_km_rate, running_km, running_km_bill, over_time_hour_rate, over_time, over_time_bill, total_bill, vehicle_type_id, total_on_count,total_on_time, remark, created_time_vts, created_time_mobile, created_by, updated_time_vts, updated_time_mobile,objection, updated_by, over_time,repair_start_time,repair_end_time,vehicle_mode,from_date,to_date,create_time_server, message_log, driver_name, bundle,adjust_date,new_meter_number, new_km', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login_id' => 'Login',
			'trip_id' => 'Trip',
			'miei'=>'MIEI',
			'ignition_status'=>'Ignition Status',
			'speed'=>'Speed',
			'requisition_id' => 'Requisition',
			'login_end' => 'Login End',
			'trip_id_end' => 'Trip Id End',
			'duty_day' => 'Duty Day',
			'driver_pin' => 'Driver Pin',
			'user_pin' => 'User Pin',
			'user_name' => 'User Name',
			'department' => 'Department',
			'vehicle_reg_no' => 'vehicle reg no',
			'dutytype_id' => 'Dutytype id',
			'driver_start_time_vts' => 'Driver Start Time Vts',
			'driver_start_time_mobile' => 'Driver Start Time Mobile',
			'driver_end_time_vts' => 'Driver End Time Vts',
			'driver_end_time_mobile' => 'Driver End Time Mobile',
			'meter_number' => 'Meter Number',
			'vts_longitude'=>'VTS Longitude',
			'vts_latitude'=>'VTS Latitude',
			'location_vts' => 'Location Vts',
			'location_mobile' => 'Location Mobile',
			'fuel_recipt_no' => 'Fuel Recipt No',
			'fuel_total_bill' => 'Fuel Total Bill',
			'night_halt_bill' => 'Night Halt Bill',
			'morning_ta' => 'Morning Ta',
			'lunch_ta' => 'Lunch Ta',
			'night_ta' => 'Night Ta',
			'running_per_km_rate' => 'Running Per Km Rate',
			'running_km' => 'Running Km',
			'running_km_bill' => 'Running Km Bill',
			'over_time_hour_rate' => 'Over Time Hour Rate',
			'over_time' => 'Over Time',
			'over_time_bill' => 'Over Time Bill',
			'total_bill' => 'Total Bill',
			'first_on_time'=>'First On Time',
			'last_off_time'=>'Last Off Time',
			'total_on_time'=>'Total On Time',
			'total_on_count'=>'Total On Count',
			'api_status'=>'Api Status',
			'vts_message'=>'VTS Message',
			'remark'=>'remark',
			'created_time_vts' => 'Created Time Vts',
			'created_time_mobile' => 'Created Time Mobile',
			'created_by' => 'Created By',
			'updated_time_vts' => 'Updated Time Vts',
			'updated_time_mobile' => 'Updated Time Mobile',
			'updated_by' => 'Updated By',
			'vehicle_type_id'=>'Vehicle Type Id',
			'driver_name'=>'driver name',
			'day_end'=>'Day End',
			'vehicle_mode'=>'Vehicle Mode',
			'fuel_quantity'=>'Fuel Quantity',
			'fuel_unit_price'=>'fuel_unit_price',
			'objection'=>'Driver Objection',
			'repair_start_time'=>'Repair Start Time',
			'repair_end_time'=>'Repair End Time',
			'from_date'=>'From Date',
			'to_date'=>'To Date',
			'create_time_server'=>'Create Time Server',
			'message_log'=>'Message Log',
			'bundle'=>"Offline Bundle",
			'missing_KM'=>'Missing KM',
			'adjust_date' => 'Adjust Date From',
			'new_meter_number' =>' Adjust Meter Number',
			'new_km' => 'New KM Found'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('login_id',$this->login_id,true);
		$criteria->compare('trip_id',$this->trip_id,true);
		$criteria->compare('miei',$this->miei,true);
		$criteria->compare('ignition_status',$this->ignition_status,true);
		$criteria->compare('speed',$this->speed,true);
		$criteria->compare('requisition_id',$this->requisition_id);
		$criteria->compare('login_end',$this->login_end,true);
		$criteria->compare('trip_id_end',$this->trip_id_end,true);
		$criteria->compare('duty_day',$this->duty_day,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('department',$this->department,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);		
		$criteria->compare('vehicle_type_id',$this->vehicle_type_id,true);
		$criteria->compare('dutytype_id',$this->dutytype_id);
		$criteria->compare('driver_start_time_vts',$this->driver_start_time_vts,true);
		$criteria->compare('driver_start_time_mobile',$this->driver_start_time_mobile,true);
		$criteria->compare('driver_end_time_vts',$this->driver_end_time_vts,true);
		$criteria->compare('driver_end_time_mobile',$this->driver_end_time_mobile,true);
		$criteria->compare('meter_number',$this->meter_number);
		$criteria->compare('new_meter_number',$this->new_meter_number);		
		$criteria->compare('vts_longitude',$this->vts_longitude);
		$criteria->compare('vts_latitude',$this->vts_latitude);
		$criteria->compare('location_vts',$this->location_vts,true);
		$criteria->compare('location_mobile',$this->location_mobile,true);
		$criteria->compare('fuel_recipt_no',$this->fuel_recipt_no,true);
		$criteria->compare('fuel_total_bill',$this->fuel_total_bill,true);
		$criteria->compare('night_halt_bill',$this->night_halt_bill);
		$criteria->compare('morning_ta',$this->morning_ta);
		$criteria->compare('lunch_ta',$this->lunch_ta);
		$criteria->compare('night_ta',$this->night_ta);
		$criteria->compare('running_per_km_rate',$this->running_per_km_rate);
		$criteria->compare('running_km',$this->running_km);
		$criteria->compare('new_km',$this->new_km);		
		$criteria->compare('running_km_bill',$this->running_km_bill);
		$criteria->compare('over_time_hour_rate',$this->over_time_hour_rate);
		$criteria->compare('over_time',$this->over_time);
		$criteria->compare('over_time_bill',$this->over_time_bill);
		$criteria->compare('total_bill',$this->total_bill);
		$criteria->compare('first_on_time',$this->first_on_time);
		$criteria->compare('last_off_time',$this->last_off_time);
		$criteria->compare('total_on_time',$this->total_on_time);
		$criteria->compare('total_on_count',$this->total_on_count);
		$criteria->compare('api_statust',$this->api_status);
		$criteria->compare('vts_message',$this->vts_message);
		$criteria->compare('remark',$this->remark);
		$criteria->compare('day_end',$this->day_end);
		$criteria->compare('repair_start_time',$this->repair_start_time,true);
		$criteria->compare('repair_end_time',$this->repair_end_time,true);
		$criteria->compare('created_time_vts',$this->created_time_vts,true);
		$criteria->compare('created_time_mobile',$this->created_time_mobile,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_time_vts',$this->updated_time_vts,true);
		$criteria->compare('updated_time_mobile',$this->updated_time_mobile,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('message_log',$this->message_log,true);
		
		if(!empty($this->from_date) && !empty($this->to_date)){
			$criteria->addCondition('DATE(t.created_time_mobile)>="'.$this->from_date.'" AND DATE(t.created_time_mobile)<="'.$this->to_date.'"');
		}
		elseif(!empty($this->from_date) && empty($this->to_date)){
			$criteria->addCondition('DATE(t.created_time_mobile)>="'.$this->from_date.'"');
		}
		elseif(empty($this->from_date) && !empty($this->to_date)){
			$criteria->addCondition('"DATE(t.created_time_mobile)<="'.$this->to_date.'"');
		}
		else{}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
				
			),
			 'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}