<?php

/**
 * This is the model class for table "{{driver_training}}".
 *
 * The followings are the available columns in table '{{driver_training}}':
 * @property integer $id
 * @property integer $driver_pin
 * @property integer $defence_traning
 * @property integer $gen_awarness_traning
 * @property integer $google_map_training
 * @property string $defence_traning_date
 * @property string $gen_awarness_traning_date
 * @property string $google_map_training_date
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class DriverTraining extends CActiveRecord
{
	public $name;
	public $phone;
	public $training;
	public $training_date_from;
	public $training_date_to;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DriverTraining the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{driver_training}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('driver_pin, created_by', 'required'),
			array('driver_pin, defence_traning, gen_awarness_traning, google_map_training, training,phone', 'numerical', 'integerOnly'=>true),
			array('created_by, updated_by,name', 'length', 'max'=>128),
			array('defence_traning_date, gen_awarness_traning_date, google_map_training_date, created_time, updated_time,training_date_from,training_date_to', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, driver_pin, defence_traning, gen_awarness_traning, google_map_training, defence_traning_date, gen_awarness_traning_date, google_map_training_date, created_by, created_time, updated_by, updated_time,name,phone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'driver_pin' => 'Driver Pin',
			'defence_traning' => 'Defence Traning',
			'gen_awarness_traning' => 'Gen Awarness Traning',
			'google_map_training' => 'Google Map Training',
			'defence_traning_date' => 'Defence Traning Date',
			'gen_awarness_traning_date' => 'Gen Awarness Traning Date',
			'google_map_training_date' => 'Google Map Training Date',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'name'=>'Name',
			'phone'=>'Phone',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria = new CDbCriteria;
		$current_year = date("Y");
		if(isset($this->training) && !empty($this->training)) {
			if($this->training == '4') {
			    $this->defence_traning = 1;
			    $this->gen_awarness_traning = 1;
			    $this->google_map_training = 1;
			    if((isset($this->training_date_from) && !empty($this->training_date_from)) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
			   	   $criteria->addCondition('(t.defence_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'") AND (t.gen_awarness_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'") AND(t.google_map_training_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'")');
			    }
			    elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)) {
			    	$criteria->addCondition('defence_traning_date >= "'.$this->training_date_from.'" AND t.gen_awarness_traning_date >= "'.$this->training_date_from.'"  AND t.google_map_training_date >= "'.$this->training_date_from.'"');
			   	   }
			   elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
					$criteria->addCondition('defence_traning_date <= "'.$this->training_date_to.'" AND t.gen_awarness_traning_date <= "'.$this->training_date_to.'"  AND t.google_map_training_date <= "'.$this->training_date_to.'"');
					}
				else {}

			}
			elseif($this->training == '3') {				
				$this->google_map_training = 1;
				if((isset($this->training_date_from) && !empty($this->training_date_from)) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
			   	   $criteria->addCondition('t.google_map_training_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'"');
			    }
			    elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)){
			   	   $criteria->addCondition('t.google_map_training_date >= "'.$this->training_date_from.'"');
			    }
			    elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
					$criteria->addCondition('t.google_map_training_date <= "'.$this->training_date_to.'"');
				}
				else{}
			}
			elseif($this->training == '2') {				
				$this->gen_awarness_traning = 1;
				if((isset($this->training_date_from) && !empty($this->training_date_from)) && (isset($this->training_date_to) && !empty($this->training_date_to))){
			   	   $criteria->addCondition('t.gen_awarness_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'"');
			    }
			    elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)) {
			   	   $criteria->addCondition('t.gen_awarness_traning_date >= "'.$this->training_date_from.'"');
			    }
			   elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
				  $criteria->addCondition('t.gen_awarness_traning_date <= "'.$this->training_date_to.'"');
				}
				else{}
			}
			elseif($this->training == '1'){				
				$this->defence_traning = 1;
				if((isset($this->training_date_from) && !empty($this->training_date_from)) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
			   	   $criteria->addCondition('t.defence_traning_date BETWEEN "'.$this->training_date_from.'" AND "'.$this->training_date_to.'"');
			    }
			    elseif((isset($this->training_date_from) && !empty($this->training_date_from))&& empty($this->training_date_to)) {
			   	   $criteria->addCondition('t.defence_traning_date>= "'.$this->training_date_from.'"');

			    }
			    elseif(empty($this->training_date_from) && (isset($this->training_date_to) && !empty($this->training_date_to))) {
				   $criteria->addCondition('t.defence_traning_date <= "'.$this->training_date_to.'"');
				}
				else{}
			}
			else{}
		}
		elseif(isset($this->training) && empty($this->training)) {}
		else {
			$criteria->addCondition('YEAR(t.google_map_training_date) = "'.$current_year.'" OR YEAR(t.gen_awarness_traning_date) = "'.$current_year.'" OR YEAR(t.defence_traning_date) = "'.$current_year.'"');
		}
		$criteria->compare('t.id',$this->id);
		$criteria->compare('r.pin',$this->driver_pin);
		$criteria->compare('t.defence_traning',$this->defence_traning);
		$criteria->compare('t.gen_awarness_traning',$this->gen_awarness_traning);
		$criteria->compare('t.google_map_training',$this->google_map_training);
		$criteria->compare('t.defence_traning_date',$this->defence_traning_date,true);
		$criteria->compare('t.gen_awarness_traning_date',$this->gen_awarness_traning_date,true);
		$criteria->compare('t.google_map_training_date',$this->google_map_training_date,true);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.updated_by',$this->updated_by,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('r.name',$this->name,true);
		$criteria->compare('r.phone',$this->phone,true);
		$criteria->addCondition("r.active ='Yes'");
		$criteria->join = "RIGHT JOIN tbl_drivers as r ON t.driver_pin=r.pin";
		$criteria->select = array('t.id,r.pin as `driver_pin`,r.name,r.phone,t.defence_traning,t.gen_awarness_traning,t.google_map_training,t.defence_traning_date,t.gen_awarness_traning_date,t.google_map_training_date' );
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'driver_pin DESC')
		));
	}

	public function driverName($pin){
		$sql="SELECT * FROM tbl_drivers WHERE pin='$pin'";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['name'];
	}
	public function driverPhone($pin){
		$sql="SELECT * FROM tbl_drivers WHERE pin='$pin'";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs['phone'];
	}
}