<?php

/**
 * This is the model class for table "{{wsrequisitions}}".
 *
 * The followings are the available columns in table '{{wsrequisitions}}':
 * @property integer $id
 * @property string $requisition_date
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property string $vmodel
 * @property string $engine_no
 * @property string $vin
 * @property string $create_time
 * @property string $created_by
 * @property string $updated_time
 * @property string $updated_by
 * @property integer $active
 */
class Wsrequisitions extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsrequisitions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsrequisitions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('requisition_date, requisition_no, vehicle_reg_no', 'required'),
			array('id, vehicletype_id, active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, engine_no, vin, created_by, updated_by', 'length', 'max'=>127),
			array('vmodel', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, requisition_date, vehicle_reg_no, vehicletype_id, vmodel, engine_no, vin, create_time, created_by, updated_time, updated_by, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'requisition_no' => 'Requisition No',
			'requisition_date' => 'Requisition Date',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicletype',
			'vmodel' => 'Vmodel',
			'engine_no' => 'Engine No',
			'vin' => 'Vin',
			'create_time' => 'Create Time',
			'created_by' => 'Created By',
			'updated_time' => 'Updated Time',
			'updated_by' => 'Updated By',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('requisition_no',$this->requisition_no,true);
		$criteria->compare('requisition_date',$this->requisition_date,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('vmodel',$this->vmodel,true);
		$criteria->compare('engine_no',$this->engine_no,true);
		$criteria->compare('vin',$this->vin,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
		));
	}
}