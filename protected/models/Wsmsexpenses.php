<?php

/**
 * This is the model class for table "{{wsmsexpenses}}".
 *
 * The followings are the available columns in table '{{wsmsexpenses}}':
 * @property integer $id
 * @property string $item_name
 * @property integer $wsitemname_id
 * @property integer $wssupplier_id
 * @property string $wssupplier_name
 * @property string $bill_date
 * @property double $unit_price
 * @property string $item_size
 * @property string $quantity
 * @property double $total_price
 * @property string $remarks
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wsmsexpenses extends CActiveRecord
{
	public $from_date;
    public $to_date;
	/**
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wsmsexpenses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wsmsexpenses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item_name, unit_price', 'required'),
			array('wsitemname_id, wssupplier_id', 'numerical', 'integerOnly'=>true),
			array('unit_price, total_price', 'numerical'),
			array('item_name, wssupplier_name, vehicle_reg_no, requisition_no,challan_no, bill_no, quantity, remarks,status, created_by, updated_by', 'length', 'max'=>127),
			array('item_size', 'length', 'max'=>20),
            array('requisition_date,challan_date, bill_date','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, item_name, wsitemname_id, wssupplier_id, wssupplier_name, requisition_no, requisition_date, bill_date, unit_price, item_size, quantity, total_price, remarks, created_by, created_time,status,from_date,to_date, updated_by, updated_time', 'safe', 'on'=>'search'),
            array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
            array('updated_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'update'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item_name' => 'Item Name',
			'wsitemname_id' => 'Itemname ID',
            'vehicle_reg_no' => 'Vehicle Reg No',
			'wssupplier_id' => 'Supplier ID',
			'wssupplier_name' => 'Supplier Name',
            'requisition_no' => 'Requisition No',
            'requisition_date' => 'Requisition Date',
            'challan_no' => 'Challan No',
            'challan_date' => 'Challan Date',
            'bill_no' => 'Bill No',
			'bill_date' => 'Bill Date',
			'unit_price' => 'Unit Price',
			'item_size' => 'Item Size',
			'quantity' => 'Quantity Purchased',
			'total_price' => 'Total Price',
			'remarks' => 'Remarks',
			'status' => 'Status',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'from_date'=>'Bill Date (From)',
            'to_date'=>'Bill Date (To)',
		);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('wsitemname_id',$this->wsitemname_id);
		$criteria->compare('wssupplier_id',$this->wssupplier_id);
        $criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('wssupplier_name',$this->wssupplier_name,true);
        $criteria->compare('requisition_no',$this->requisition_no,true);
        $criteria->compare('requisition_date',$this->requisition_date);
        $criteria->compare('challan_no',$this->challan_no,true);
        $criteria->compare('challan_date',$this->challan_date);
        $criteria->compare('bill_no',$this->bill_no,true);
		$criteria->compare('bill_date',$this->bill_date);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('item_size',$this->item_size,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('total_price',$this->total_price);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		  if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('bill_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('bill_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('bill_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}