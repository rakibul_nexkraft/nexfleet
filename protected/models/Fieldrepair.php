<?php

/**
 * This is the model class for table "{{fieldrepair}}".
 *
 * The followings are the available columns in table '{{fieldrepair}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property string $driver_name
 * @property integer $driver_pin
 * @property string $last_meter
 * @property string $meter_reading
 * @property string $apply_date
 * @property string $fieldrepair_description
 * @property string $onsite_name
 * @property string $approve_by
 * @property string $program_name
 * @property double $spare_amount
 * @property string $spare_description
 * @property double $labor_amount
 * @property string $labor_description
 * @property double $machine_shop_amount
 * @property string $machine_shop_description
 * @property double $vehicle_towchain_amount
 * @property string $vehicle_towchain_description
 * @property string $remarks
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Fieldrepair extends CActiveRecord
{

    public $from_date;
    public $to_date;
    public $from_p_date;
    public $to_p_date;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fieldrepair the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fieldrepair}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, vehicletype_id', 'required'),
			array('id, vehicletype_id, driver_pin', 'numerical', 'integerOnly'=>true),
			array('spare_amount, labor_amount, machine_shop_amount, vehicle_towchain_amount', 'numerical'),
			array('vehicle_reg_no, driver_name, last_meter, meter_reading, approve_by, remarks, created_by, updated_by', 'length', 'max'=>127),
			array('fieldrepair_description, program_name, spare_description, labor_description, machine_shop_description, vehicle_towchain_description', 'length', 'max'=>512),
			array('onsite_name', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, vehicletype_id, driver_pin, meter_reading, fieldrepair_description, program_name, created_by, created_time, from_date, to_date', 'safe', 'on'=>'search'),
            array('from_p_date,to_p_date', 'safe'),
            array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
            array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicle Type',
			'driver_name' => 'Driver Name',
			'driver_pin' => 'Driver Pin',
			'last_meter' => 'Last Meter',
			'meter_reading' => 'Meter Reading',
			'apply_date' => 'Apply Date',
			'fieldrepair_description' => 'Field Repair Description',
			'onsite_name' => 'On-site Name',
			'approve_by' => 'Approve By',
			'program_name' => 'Program Name',
			'spare_amount' => 'Spare Amount',
			'spare_description' => 'Spare Description',
			'labor_amount' => 'Labor Amount',
			'labor_description' => 'Labor Description',
			'machine_shop_amount' => 'Machine Shop Amount',
			'machine_shop_description' => 'Machine Shop Description',
			'vehicle_towchain_amount' => 'Vehicle Tow-chain Amount',
			'vehicle_towchain_description' => 'Vehicle Tow-chain Description',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
            'from_date'=>'Apply Date(From)',
            'to_date'=>'Apply Date(To)',
            'from_p_date'=>'Date(From)',
            'to_p_date'=>'Date(To)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
//		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('driver_pin',$this->driver_pin);
//		$criteria->compare('last_meter',$this->last_meter,true);
		$criteria->compare('meter_reading',$this->meter_reading,true);
//		$criteria->compare('apply_date',$this->apply_date,true);
		$criteria->compare('fieldrepair_description',$this->fieldrepair_description,true);
//		$criteria->compare('onsite_name',$this->onsite_name,true);
//		$criteria->compare('approve_by',$this->approve_by,true);
		$criteria->compare('program_name',$this->program_name,true);
//		$criteria->compare('spare_amount',$this->spare_amount);
//		$criteria->compare('spare_description',$this->spare_description,true);
//		$criteria->compare('labor_amount',$this->labor_amount);
//		$criteria->compare('labor_description',$this->labor_description,true);
//		$criteria->compare('machine_shop_amount',$this->machine_shop_amount);
//		$criteria->compare('machine_shop_description',$this->machine_shop_description,true);
//		$criteria->compare('vehicle_towchain_amount',$this->vehicle_towchain_amount);
//		$criteria->compare('vehicle_towchain_description',$this->vehicle_towchain_description,true);
//		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
//		$criteria->compare('updated_by',$this->updated_by,true);
//		$criteria->compare('updated_time',$this->updated_time,true);

        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('apply_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('apply_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('apply_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
        ));
	}
}