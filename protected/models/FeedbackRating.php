<?php

/**
 * This is the model class for table "{{feedback_rating}}".
 *
 * The followings are the available columns in table '{{feedback_rating}}':
 * @property integer $id
 * @property double $car_rating
 * @property string $car_feedback
 * @property double $driver_rating
 * @property string $driver_feedback
 * @property double $ifleet_rating
 * @property string $ifleet_feedback
 * @property string $created_by
 * @property string $created_time
 *
 * The followings are the available model relations:
 * @property Requisitions $id0
 */
class FeedbackRating extends CActiveRecord
{
	public $driver_pin;
	public $from_date;
	public $to_date;
	public $vehicle_reg_no;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FeedbackRating the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{feedback_rating}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, car_rating, driver_rating, ifleet_rating,  created_by, created_time', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('car_rating, driver_rating, ifleet_rating', 'numerical'),
			array('car_feedback, driver_feedback, ifleet_feedback, created_by', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, car_rating, car_feedback, driver_rating, driver_feedback, ifleet_rating, ifleet_feedback, created_by, created_time,driver_pin,from_date,to_date,vehicle_reg_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'id0' => array(self::BELONGS_TO, 'Requisitions', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Requisition ID',
			'car_rating' => 'Vehicle Rating',
			'car_feedback' => 'Vehicle Feedback',
			'driver_rating' => 'Driver Rating',
			'driver_feedback' => 'Driver Feedback',
			'ifleet_rating' => 'Ifleet Rating',
			'ifleet_feedback' => 'Ifleet Feedback',
			'created_by' => 'User Pin',
			'created_time' => 'Created Time',
			'from_date'=>'Rating Create Form Date',
			'to_date'=>'Rating Create To Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	/*public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.car_rating',$this->car_rating);
		$criteria->compare('t.car_feedback',$this->car_feedback,true);
		$criteria->compare('t.driver_rating',$this->driver_rating);
		$criteria->compare('t.driver_feedback',$this->driver_feedback,true);
		$criteria->compare('t.ifleet_rating',$this->ifleet_rating);
		$criteria->compare('t.ifleet_feedback',$this->ifleet_feedback,true);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);

		if(!empty($this->driver_pin)){
			$criteria->compare('id0.driver_pin',$this->driver_pin);
		}
		if(!empty($this->vehicle_reg_no)){
			$criteria->compare('id0.vehicle_reg_no',$this->vehicle_reg_no,true);
		}
		if(!empty($this->from_date) && !empty($this->to_date)){
			$criteria->addCondition('DATE(t.created_time)>="'.$this->from_date.'" AND DATE(t.created_time)<="'.$this->to_date.'"');
		}
		elseif(!empty($this->from_date) && empty($this->to_date)){
			$criteria->addCondition('DATE(t.created_time)>="'.$this->from_date.'"');
		}
		elseif(empty($this->from_date) && !empty($this->to_date)){
			$criteria->addCondition('"DATE(t.created_time)<="'.$this->to_date.'"');
		}
		else{}
		$criteria->with='id0';
		$criteria->order="t.id desc";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		)
			
	);
	}*/
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		//$criteria->compare('t.car_rating',$this->car_rating);
		$criteria->compare('t.car_feedback',$this->car_feedback,true);
		//$criteria->compare('t.driver_rating',$this->driver_rating);
		$criteria->compare('t.driver_feedback',$this->driver_feedback,true);
		//$criteria->compare('t.ifleet_rating',$this->ifleet_rating);
		$criteria->compare('t.ifleet_feedback',$this->ifleet_feedback,true);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);

		if(!empty($this->driver_pin)){
			$criteria->compare('id0.driver_pin',$this->driver_pin);
		}
		if(!empty($this->vehicle_reg_no)){
			$criteria->compare('id0.vehicle_reg_no',$this->vehicle_reg_no,true);
		}
        if(!empty($this->driver_rating)){
            $driver_rating_search_param = explode('-',$this->driver_rating);
            if(sizeof($driver_rating_search_param) == 1){
                $criteria->compare('t.driver_rating',$this->driver_rating);
            }
            if (sizeof($driver_rating_search_param) == 2){
                $criteria->addCondition('t.driver_rating >= "'.$driver_rating_search_param[0].'" AND t.driver_rating <= "'.$driver_rating_search_param[1].'"');
            }
        }
        if(!empty($this->car_rating)){
            $car_rating_search_param = explode('-',$this->car_rating);
            if(sizeof($car_rating_search_param) == 1){
                $criteria->compare('t.car_rating',$this->car_rating);
            }
            if (sizeof($car_rating_search_param) == 2){
                $criteria->addCondition('t.car_rating >= "'.$car_rating_search_param[0].'" AND t.car_rating <= "'.$car_rating_search_param[1].'"');
            }
        }
        if(!empty($this->ifleet_rating)){
            $ifleet_rating_search_param = explode('-',$this->ifleet_rating);
            if(sizeof($ifleet_rating_search_param) == 1){
                $criteria->compare('t.ifleet_rating',$this->ifleet_rating);
            }
            if (sizeof($ifleet_rating_search_param) == 2){
                $criteria->addCondition('t.ifleet_rating >= "'.$ifleet_rating_search_param[0].'" AND t.ifleet_rating <= "'.$ifleet_rating_search_param[1].'"');
            }
        }
		if(!empty($this->from_date) && !empty($this->to_date)){
			$criteria->addCondition('DATE(t.created_time)>="'.$this->from_date.'" AND DATE(t.created_time)<="'.$this->to_date.'"');
		}
		elseif(!empty($this->from_date) && empty($this->to_date)){
			$criteria->addCondition('DATE(t.created_time)>="'.$this->from_date.'"');
		}
		elseif(empty($this->from_date) && !empty($this->to_date)){
			$criteria->addCondition('"DATE(t.created_time)<="'.$this->to_date.'"');
		}
		else{}
		$criteria->with='id0';
		$criteria->order="t.id desc";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		)
			
	);
	}
	public function driverPin($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["driver_pin"];
	}
	public function driverName($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["driver_name"];
	}
	public function driverContact($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["driver_phone"];
	}
	public function vehicleReg($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["vehicle_reg_no"];
	}
	public function userName($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["user_name"];
	}
	public function userContact($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["user_cell"];
	}
	public function userDepartment($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["dept_name"];
	}
	public function vehicleEndPoint($req){
		$sql="SELECT * FROM tbl_requisitions WHERE id=$req";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
		return $sql_rs["end_point"];
	}
	
}