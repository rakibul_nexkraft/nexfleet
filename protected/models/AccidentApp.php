<?php

/**
 * This is the model class for table "{{accident_app}}".
 *
 * The followings are the available columns in table '{{accident_app}}':
 * @property integer $id
 * @property integer $own_car_damage_left
 * @property integer $own_car_damage_right
 * @property integer $own_car_damage_back
 * @property integer $own_car_damage_front
 * @property integer $other_car_damage_left
 * @property integer $other_car_damage_right
 * @property integer $other_car_damage_back
 * @property integer $other_car_damage_front
 * @property integer $settlement_type
 * @property string $remark
 * @property string $accident_time
 * @property string $accident_place
 * @property string $driver_pin
 * @property string $vehicle_reg_no
 * @property integer $created_by
 * @property string $created_date
 * @property integer $updated_by
 * @property string $updated_date
 */
class AccidentApp extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AccidentApp the static model class
	 */
	public $accident_date;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{accident_app}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('own_car_damage_left, own_car_damage_right, own_car_damage_back, own_car_damage_front, other_car_damage_left, other_car_damage_right, other_car_damage_back, other_car_damage_front, settlement_type', 'numerical', 'integerOnly'=>true),
			array('remark', 'length', 'max'=>2000),
			array('accident_place,own_car_damage_image,other_car_damage_image', 'length', 'max'=>180),
			array('driver_pin, vehicle_reg_no', 'length', 'max'=>45),
			array('accident_time, created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, own_car_damage_left, own_car_damage_right, own_car_damage_back, own_car_damage_front, other_car_damage_left, other_car_damage_right, other_car_damage_back, other_car_damage_front, settlement_type, remark, accident_time, accident_date, accident_place, driver_pin, vehicle_reg_no, created_by, created_date, updated_by, updated_date, own_car_damage_image, other_car_damage_image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'own_car_damage_left' => 'Own Car Damage Left',
			'own_car_damage_right' => 'Own Car Damage Right',
			'own_car_damage_back' => 'Own Car Damage Back',
			'own_car_damage_front' => 'Own Car Damage Front',
			'other_car_damage_left' => 'Other Car Damage Left',
			'other_car_damage_right' => 'Other Car Damage Right',
			'other_car_damage_back' => 'Other Car Damage Back',
			'other_car_damage_front' => 'Other Car Damage Front',
			'settlement_type' => 'Settlement Type',
			'remark' => 'Remark',
			'accident_time' => 'Accident Time',
			'accident_date' => 'Accident Date',
			'accident_place' => 'Accident Place',
			'driver_pin' => 'Driver Pin',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'created_by' => 'Created By',
			'created_date' => 'Created Date',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
			'own_car_damage_image'=>'Own Car Damage Image',
			'other_car_damage_image'=>'Other Car Damage Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('own_car_damage_left',$this->own_car_damage_left);
		$criteria->compare('own_car_damage_right',$this->own_car_damage_right);
		$criteria->compare('own_car_damage_back',$this->own_car_damage_back);
		$criteria->compare('own_car_damage_front',$this->own_car_damage_front);
		$criteria->compare('other_car_damage_left',$this->other_car_damage_left);
		$criteria->compare('other_car_damage_right',$this->other_car_damage_right);
		$criteria->compare('other_car_damage_back',$this->other_car_damage_back);
		$criteria->compare('other_car_damage_front',$this->other_car_damage_front);
		$criteria->compare('settlement_type',$this->settlement_type);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('accident_time',$this->accident_time,true);
		$criteria->compare('accident_place',$this->accident_place,true);
		$criteria->compare('driver_pin',$this->driver_pin,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}