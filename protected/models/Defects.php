<?php

/**
 * This is the model class for table "{{defects}}".
 *
 * The followings are the available columns in table '{{defects}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property integer $driver_pin
 * @property string $meter_reading
 * @property string $apply_date
 * @property string $mechanic_name
 * @property integer $mechanic_pin
 * @property integer $task_id
 * @property string $created_by
 * @property string $created_time
 */
class Defects extends CActiveRecord
{
    public $from_date;
    public $to_date;
    public $from_p_date;
    public $to_p_date;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Defects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{defects}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, vehicletype_id', 'required'),
			array('id, vehicletype_id, driver_pin, mechanic_pin, task_id', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, driver_name, last_meter, meter_reading, mechanic_name, remarks, created_by', 'length', 'max'=>127),
			array('defect_description, apply_date, edt','safe'),
			array('total_amount', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, vehicletype_id, driver_pin, meter_reading, apply_date, mechanic_name, mechanic_pin, created_by, created_time,from_date,to_date', 'safe', 'on'=>'search'),
            array('from_p_date,to_p_date', 'safe'),
            array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
            array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'defectdetail'=>array(self::HAS_MANY, 'Defectdetail', 'defect_id'),
		'tasks'=>array(self::HAS_MANY, 'Tasks', 'defect_id'),
        'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),

        'wsitemdists'=>array(self::HAS_MANY, 'Wsitemdists', 'defect_id'),
           'deliveryvehicles'=>array(self::BELONGS_TO, 'Deliveryvehicles', 'id'),
           'wsbatteries'=>array(self::HAS_MANY, 'Wsbatteries', 'defect_id'),
           'wstyres'=>array(self::HAS_MANY, 'Wstyres', 'defect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Defects Vehicle Reg No',
			'vehicletype_id' => 'Vehicle Type',
            'driver_name' => 'Driver Name',
			'driver_pin' => 'Driver Pin',
            'last_meter' => 'Last Meter',
			'meter_reading' => 'Meter Reading',
			'apply_date' => 'Apply Date',
			'defect_description' => 'Defect Description',
			'mechanic_name' => 'Job Done By (PIN/Name)',
			'mechanic_pin' => 'Job Done By (PIN)',
			'total_amount' => 'Total Amount',
            'edt' => 'EDD',
            'remarks' => 'Remarks',
			//'task_id' => 'Task',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
            'from_date'=>'Apply Date(From)',
            'to_date'=>'Apply Date(To)',
            'from_p_date'=>'Date(From)',
            'to_p_date'=>'Date(To)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
        $criteria->compare('driver_name',$this->driver_name);
		$criteria->compare('driver_pin',$this->driver_pin);
        $criteria->compare('last_meter',$this->last_meter,true);
		$criteria->compare('meter_reading',$this->meter_reading,true);
		$criteria->compare('apply_date',$this->apply_date,true);
		$criteria->compare('defect_description',$this->defect_description,true);
		$criteria->compare('mechanic_name',$this->mechanic_name,true);
		$criteria->compare('mechanic_pin',$this->mechanic_pin);
		$criteria->compare('task_id',$this->task_id);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);

        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('apply_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('apply_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('apply_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }


        return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}

    public function searchpending()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->select =array('t.id,t.vehicle_reg_no,t.apply_date,t.driver_name,t.defect_description,t.mechanic_name,t.remarks,a.delivery_date');
        $criteria->join ='LEFT JOIN tbl_deliveryvehicles as a ON a.defect_id = t.id ';


        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.vehicle_reg_no',$this->vehicle_reg_no,true);


        $criteria->compare('t.apply_date',$this->apply_date,true);


        $criteria->order = 't.apply_date desc';

        if(!empty($this->from_p_date) && empty($this->to_p_date))
        {
            $criteria->addCondition('t.apply_date = "'.$this->from_p_date.'"');
        }
        elseif(!empty($this->to_p_date) && empty($this->from_p_date))
        {
            $criteria->addCondition('t.apply_date <= "'.$this->to_p_date.'"');
        }
        elseif(!empty($this->to_p_date) && !empty($this->from_p_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('t.apply_date BETWEEN "'.$this->from_p_date.'" AND "'.$this->to_p_date.'"');
            $criteria->addCondition('a.delivery_date IS NULL');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
        ));
    }

    public function getAllDefectsByDefectId($defect_id){
        $result_data = "";
        $sql = "SELECT * FROM tbl_defectdetail WHERE defect_id=$defect_id";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results as $result){
            $result_data .= $result['job_name'].", ";
        }

        return $result_data;
    }
    public function getAllMechanicsByDefectId($defect_id){
        $result_data = "";
        $sql = "SELECT * FROM tbl_defectdetail WHERE defect_id=$defect_id";
        $results = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($results as $result){
            $result_data .= $result['mechanic_name'].", ";
        }

        return $result_data;
    }
}