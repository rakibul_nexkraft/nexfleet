<?php

/**
 * This is the model class for table "{{movements}}".
 *
 * The followings are the available columns in table '{{movements}}':
 * @property integer $id
 * @property integer $requisition_id
 * @property integer $user_pin
 * @property string $user_name
 * @property integer $user_level
 * @property string $user_dept
 * @property string $vehicle_reg_no
 * @property integer $driver_pin
 * @property string $driver_name
 * @property integer $dutytype_id
 * @property string $dutyday
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string $start_point
 * @property string $end_point
 * @property integer $start_meter
 * @property integer $end_meter
 * @property integer $night_halt
 * @property integer $bill_amount
 * @property string $created_by
 * @property string $created_time
 * @property integer $active
 */
class Movements extends CActiveRecord
{
	public $from_date;
	public $to_date;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Movements the static model class
     *
	 */

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{movements}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name, user_dept, vehicle_reg_no, driver_pin, driver_name, dutytype_id, dutyday, start_date, end_date, start_time, end_time, start_point, end_point, start_meter, end_meter, night_halt', 'required'),
			array('id, requisition_id, user_level, driver_pin, dutytype_id, vehicletype_id, start_meter, end_meter,  	total_run, rph, night_halt, nh_amount,  active,morning_ta,lunch_ta,night_ta, used_mileage,user_cell,sup_mile_amount,helper_pin,digi_id', 'numerical', 'integerOnly'=>true),
			array('total_ot_hour,total_ot_amount,helper_ot', 'type', 'type'=>'float'),
			array('user_name, user_pin', 'length', 'max'=>40),
			array('email', 'email'),
			array('vehicle_reg_no, driver_name, dutyday, vehicle_location, start_point, end_point, total_time, created_by, bill_amount,user_address,user_dept,helper_name', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, requisition_id, user_pin, user_name, user_level, user_dept, vehicle_reg_no, driver_pin, driver_name, dutytype_id, dutyday, start_date, end_date, start_time, end_time, start_point, end_point, start_meter, end_meter, night_halt, bill_amount, created_by, created_time, updated_by, updated_time, active, used_mileage, from_date, to_date', 'safe', 'on'=>'search'),
            array('created_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
			array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
       
       array('expire_date_error', 'check'),
       
		);
	}
	
	 public function check()
     {
         if($this->start_meter > $this->end_meter) {
             $this->addError('expire_date_error', 'End Meter should not be less than Start Meter');
             return false;
         }

         return true;
     }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'dutytypes'=>array(self::BELONGS_TO, 'Dutytypes', 'dutytype_id'),
            'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'requisition_id' => 'Requisition',
			'user_pin' => 'User PIN',
			'user_name' => 'User Name',
			'user_level' => 'User Level',
			'user_dept' => 'Department',
			'vehicle_reg_no' => 'Vehicle Reg No',
            'vehicletype_id' => 'Vehicle Type',
            'vehicle_location' => 'Vehicle Location',
			'driver_pin' => 'Driver PIN',
			'driver_name' => 'Driver Name',
			'helper_pin' => 'Helper PIN',
			'helper_name' => 'Helper Name',			
			'helper_ot' => 'Helper OT',
			'dutytype_id' => 'Duty Type',
			'dutyday' => 'Duty Day',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'start_point' => 'Start Point',
			'end_point' => 'End Point',
			'start_meter' => 'Start Meter',
			'end_meter' => 'End Meter',
			'rph' => 'Rate per Hour',
			'night_halt' => 'Night Halt',
			'nh_amount' => 'Night Halt Amount',
			'bill_amount' => 'Bill Amount',
			'created_by' => 'Created By',
			'created_time' => 'Creation Time',
			'active' => 'Active',
			'morning_ta' => 'Morning TA',
			'lunch_ta' => 'Lunch TA',
			'night_ta' => 'Night TA',
			'used_mileage' => 'Total Mileage Used',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
			'digi_id'=>"Digi ID",
		);
	}
	
	public function report()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

	//	$criteria->condition="id BETWEEN UNIX_TIMESTAMP(:time_up_from) AND UNIX_TIMESTAMP(:time_up_to)";
  //$criteria->params[':time_up_from']=$this->start_date;
 // $criteria->params[':time_up_to']=$this->end_date;
		
		
		
		$criteria->condition="start_date>= '$this->start_date' AND end_date<='$this->end_date'";
		$criteria->compare('user_pin',$this->user_pin);
		
		/*$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		*/
		
    $criteria->order = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>45,
            )
		));
	}
	
	 

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('requisition_id',$this->requisition_id);
		$criteria->compare('user_pin',$this->user_pin);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_level',$this->user_level);
		$criteria->compare('user_dept',$this->user_dept,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('dutytype_id',$this->dutytype_id);
		$criteria->compare('dutyday',$this->dutyday,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('start_point',$this->start_point,true);
		$criteria->compare('end_point',$this->end_point,true);
		$criteria->compare('start_meter',$this->start_meter);
		$criteria->compare('end_meter',$this->end_meter);
		$criteria->compare('night_halt',$this->night_halt);
		$criteria->compare('bill_amount',$this->bill_amount);
		$criteria->compare('digi_id',$this->digi_id);
		
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('active',$this->active);

        $criteria->order = 'id DESC';
		
		if(!empty($this->from_date) && empty($this->to_date))
		{
			$criteria->addCondition('start_date = "'.$this->from_date.'"');
		}
		elseif(!empty($this->to_date) && empty($this->from_date))
		{
			$criteria->addCondition('start_date <= "'.$this->to_date.'"');
		}
		elseif(!empty($this->to_date) && !empty($this->from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('start_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>45,
            )
		));
	}
	public function searchExcel($offset_data,$data)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$offset_data=$offset_data*2000;
		$criteria=new CDbCriteria;		
		
		$criteria->compare('requisition_id',$data->requisition_id);
		$criteria->compare('user_pin',$data->user_pin);
		$criteria->compare('user_name',$data->user_name,true);
		$criteria->compare('user_level',$data->user_level);
		$criteria->compare('user_dept',$data->user_dept,true);
		$criteria->compare('vehicle_reg_no',$data->vehicle_reg_no,true);
		$criteria->compare('driver_pin',$data->driver_pin);
		
		$criteria->compare('dutytype_id',$data->dutytype_id);        
		
		if(!empty($data->from_date) && empty($data->to_date))
		{
			$criteria->addCondition('start_date = "'.$data->from_date.'"');
		}
		elseif(!empty($data->to_date) && empty($data->from_date))
		{
			$criteria->addCondition('start_date <= "'.$data->to_date.'"');
		}
		elseif(!empty($data->to_date) && !empty($data->from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('start_date BETWEEN "'.$data->from_date.'" AND "'.$data->to_date.'"');
		}
		$criteria->order = 'id DESC';
		$criteria->limit = 2000;
		$criteria->offset = $offset_data;
		$search_movements_table=Movements::model()->findAll($criteria);
		return $search_movements_table;
		
	}
}