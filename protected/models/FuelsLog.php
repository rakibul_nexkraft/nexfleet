<?php

/**
 * This is the model class for table "{{fuels_log}}".
 *
 * The followings are the available columns in table '{{fuels_log}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $fuel_type
 * @property integer $quantity
 * @property integer $value
 * @property string $purchase_date
 * @property integer $fuel_station_id
 * @property integer $driver_pin
 * @property string $update_time
 * @property string $updated_by
 * @property integer $active
 */
class FuelsLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FuelsLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{fuels_log}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, fuel_id', 'required'),
			array('fuel_station_id, driver_pin, active', 'numerical', 'integerOnly'=>true),
			array('quantity, value', 'type', 'type'=>'float'),
			array('vehicle_reg_no, updated_by', 'length', 'max'=>127),
			array('fuel_type, created_by', 'length', 'max'=>30),
			array('purchase_date, updated_time, created_time, quantity, value', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, fuel_type, quantity, value, purchase_date, fuel_station_id, driver_pin, updated_time, updated_by, created_time, created_by, active', 'safe', 'on'=>'search'),
            array('updated_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'fuels'=>array(self::BELONGS_TO, 'Fuels', 'fuel_id'),
            'fuelstations'=>array(self::BELONGS_TO, 'Fuelstations', 'fuel_station_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'fuel_id' => 'Fuel ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'fuel_type' => 'Fuel Type',
			'quantity' => 'Quantity',
			'value' => 'Value',
			'purchase_date' => 'Purchase Date',
			'fuel_station_id' => 'Fuel Station',
			'driver_pin' => 'Driver Pin',
            'created_time' => 'Creation Time',
            'created_by' => 'Created By',
			'update_time' => 'Update Time',
			'updated_by' => 'Updated By',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('fuel_type',$this->fuel_type,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('value',$this->value);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('fuel_station_id',$this->fuel_station_id);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>10,
			),
		));
	}
}