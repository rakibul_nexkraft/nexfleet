<?php

/**
 * This is the model class for table "{{deliveryvehicles}}".
 *
 * The followings are the available columns in table '{{deliveryvehicles}}':
 * @property integer $id
 * @property integer $defects_id
 * @property string $vehicle_reg_no
 * @property string $delivery_date
 * @property integer $KM_reading
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Deliveryvehicles extends CActiveRecord
{
    public $from_date;
    public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Deliveryvehicles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{deliveryvehicles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('defect_id, vehicle_reg_no, delivery_date, KM_reading, action_taken', 'required'),
			array('defect_id, KM_reading, driver_pin', 'numerical', 'integerOnly'=>true),
            array('delivery_date', 'safe'),
			array('vehicle_reg_no, driver_name, created_by, updated_by', 'length', 'max'=>127),
            array('action_taken', 'length', 'max'=>512),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, defect_id, vehicle_reg_no, delivery_date, KM_reading, created_by, created_time, updated_by, updated_time,from_date,to_date', 'safe', 'on'=>'search'),
            array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
            array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'defect_id' => 'Defect',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'delivery_date' => 'Delivery Date',
			'KM_reading' => 'Km Reading',
            'driver_pin' => 'Delivery to (PIN)',
            'driver_name' => 'Delivery to (Name)',
			'action_taken' => 'Action Taken',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
            'from_date'=>'Delivery Date (From)',
            'to_date'=>'Delivery Date (To)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('defect_id',$this->defect_id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('delivery_date',$this->delivery_date,true);
		$criteria->compare('KM_reading',$this->KM_reading);
        $criteria->compare('driver_pin',$this->driver_pin);
        $criteria->compare('driver_name',$this->driver_name);
        $criteria->compare('action_taken',$this->action_taken);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('delivery_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('delivery_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('delivery_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
        ));
	}
}