<?php

/**
 * This is the model class for table "{{requisitions}}".
 *
 * The followings are the available columns in table '{{requisitions}}':
 * @property integer $id
 * @property string $bpmt_ref_no
 * @property string $vehicle_reg_no
 * @property integer $driver_pin
 * @property string $driver_name
 * @property integer $user_pin
 * @property string $user_name
 * @property integer $user_level
 * @property string $dept_name
 * @property string $start_point
 * @property string $end_point
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property integer $passenger
 * @property string $created_by
 * @property string $created_time
 * @property string $remarks
 * @property integer $dutytype_id
 * @property integer $active
 */
class Requisitions extends CActiveRecord
{

	public $from_date;
	public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Requisitions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{requisitions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('dept_name, start_point, end_point, start_date, end_date, start_time, end_time, dutytype_id', 'required'),
			array('id, driver_pin, driver_phone, user_level, supervisor_level, user_cell, passenger, vehicletype_id, dutytype_id, active,', 'numerical', 'integerOnly'=>true),
			array('bpmt_ref_no, vehicle_reg_no, vehicle_location, driver_name, user_name, dept_name, user_address, start_point, end_point, created_by, start_time, end_time, supervisor_name, approve_status, supervisor_pin, updated_by', 'length', 'max'=>127),
			array('user_pin,billing_code', 'length', 'max'=>255),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('updated_by, updated_time, remarks', 'safe'),
			array('email', 'email'),
			array('id, bpmt_ref_no, vehicle_reg_no, vehicle_location, driver_pin, driver_name, user_pin, user_name, user_level, dept_name, start_point, end_point, start_date,
				end_date, start_time, end_time, passenger, created_by, created_time, remarks, dutytype_id, active, supervisor_name, supervisor_level, 
				approve_status, supervisor_pin, email, updated_by, updated_time, from_date, to_date', 'safe', 'on'=>'search'),
      array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'dutytypes'=>array(self::BELONGS_TO, 'Dutytypes', 'dutytype_id'),
			'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Requisition No.',
			'bpmt_ref_no' => 'BPMT Ref No',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicle Type',
            'vehicle_location' => 'Vehicle Location',
      'billing_code' => 'Billing Code',      
			'driver_pin' => 'Driver PIN',
			'driver_name' => 'Driver Name',
			'user_pin' => 'User PIN',
			'user_name' => 'User Name',
			'driver_phone' => 'Driver\'s Contact No',
			'user_level' => 'User Level',
			'dept_name' => 'Department',
			'email' => 'Email ID',
			'start_point' => 'Start Point',
			'end_point' => 'End Point',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'passenger' => 'Passenger',
			'supervisor_name' => 'Supervisor Name',
			'supervisor_pin' => 'Supervisor PIN',
			'supervisor_level' => 'Supervisor Level',
			'approve_status' => 'Supervisor Approval Status',
			'created_by' => 'Created By',
			'created_time' => 'Creation Time',
			'updated_by' => 'Status Changed By',
			'updated_time' => 'Status Change Time',
			'remarks' => 'Remarks',
			'dutytype_id' => 'Duty Type',
			'active' => 'Action Taken by Fleet',
			'from_date' => 'Start Date',
			'to_date' => 'To Start Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bpmt_ref_no',$this->bpmt_ref_no);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('user_pin',$this->user_pin);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_level',$this->user_level);
		$criteria->compare('dept_name',$this->dept_name,true);
		$criteria->compare('start_point',$this->start_point,true);
		$criteria->compare('end_point',$this->end_point,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('passenger',$this->passenger);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('dutytype_id',$this->dutytype_id);
		$criteria->compare('active',$this->active);
		
		//$criteria->order = 'id DESC';
		
		if(!empty($this->from_date) && empty($this->to_date))
		{
			$criteria->addCondition('start_date = "'.$this->from_date.'"');
		}
		elseif(!empty($this->to_date) && empty($this->from_date))
		{
			$criteria->addCondition('start_date <= "'.$this->to_date.'"');
		}
		elseif(!empty($this->to_date) && !empty($this->from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('start_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>40,
			),
		  'sort'=>array('defaultOrder'=>'id DESC')
       ));
	}
	
	 public function statusSms($user_cell,$updated_time,$id)
    {
        $host = Yii::app()->params['mydeskdb']['host'];
        $port = Yii::app()->params['mydeskdb']['port'];
        $user = Yii::app()->params['mydeskdb']['user'];
        $pass = Yii::app()->params['mydeskdb']['pass'];
        $dbname = Yii::app()->params['mydeskdb']['dbname'];

        $connectionString = 'mysql:host=' . $host . ';port=' . $port . ';dbname=' . $dbname . '';

        $feelConn = new CDbConnection($connectionString, $user, $pass);

        If(strlen($user_cell)==10)
            $user_cell ="0".$user_cell;
		
		$updated_time_from = date('Y-m-d', strtotime($updated_time))." 00:00:00";	
		$updated_time_to = date('Y-m-d', strtotime($updated_time . ' +1 day'))." 23:59:59";			
		

        $sql = "select responce_code from sms_out where app_id=1 AND to_number = '$user_cell' AND create_time BETWEEN '$updated_time_from' AND '$updated_time_to' AND message LIKE '%requisition $id%'"; 
        //$parameters = array(':user_cell' => "0".$user_cell);//, ':updated_time' => $updated_time);
        $result = $feelConn->createCommand($sql);//->execute($parameters);

        $result = $result->queryAll();


        $split = explode(",", $result[0]['responce_code']);

        if($split[0]=='200')
            return "Success";
        else
            return $split[1];


    }
	
	public function statusSmsDriver($driver_cell,$updated_time,$id)
    {
        $host = Yii::app()->params['mydeskdb']['host'];
        $port = Yii::app()->params['mydeskdb']['port'];
        $user = Yii::app()->params['mydeskdb']['user'];
        $pass = Yii::app()->params['mydeskdb']['pass'];
        $dbname = Yii::app()->params['mydeskdb']['dbname'];

        $connectionString = 'mysql:host=' . $host . ';port=' . $port . ';dbname=' . $dbname . '';

        $feelConn = new CDbConnection($connectionString, $user, $pass);

        If(strlen($driver_cell)==10)
            $driver_cell ="0".$driver_cell;
		
		$updated_time_from = date('Y-m-d', strtotime($updated_time))." 00:00:00";	
		$updated_time_to = date('Y-m-d', strtotime($updated_time . ' +1 day'))." 23:59:59";

        $sql = "select responce_code from sms_out where app_id=1 AND to_number = '$driver_cell' AND create_time BETWEEN '$updated_time_from' AND '$updated_time_to' AND message LIKE '%Req # $id%'";
        $result = $feelConn->createCommand($sql);

        $result = $result->queryAll();


        $split = explode(",", $result[0]['responce_code']);

        if($split[0]=='200')
            return "Success";
        else
            return $split[0];


    }
	
	
    public function statusActive($a)
    {
        if($a==1)
            return "Not Approved";
        else if($a==2)
            return "Approved";
        else if($a==0)
            return "Pending";
		else
			return "";
    }
}