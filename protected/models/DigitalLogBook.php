<?php

/**
 * This is the model class for table "{{digital_log_book}}".
 *
 * The followings are the available columns in table '{{digital_log_book}}':
 * @property integer $id
 * @property integer $requisition_id
 * @property string $user_pin
 * @property string $user_name
 * @property integer $user_level
 * @property string $user_dept
 * @property string $email
 * @property integer $user_cell
 * @property string $user_address
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property string $vehicle_location
 * @property integer $driver_pin
 * @property string $driver_name
 * @property integer $helper_pin
 * @property string $helper_name
 * @property double $helper_ot
 * @property integer $dutytype_id
 * @property string $dutyday
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string $total_time
 * @property string $start_point
 * @property string $end_point
 * @property integer $start_meter
 * @property integer $end_meter
 * @property integer $total_run
 * @property integer $rph
 * @property integer $night_halt
 * @property integer $nh_amount
 * @property integer $morning_ta
 * @property integer $lunch_ta
 * @property integer $night_ta
 * @property integer $sup_mile_amount
 * @property double $bill_amount
 * @property double $total_ot_hour
 * @property double $total_ot_amount
 * @property integer $used_mileage
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 * @property integer $active
 * @property string $trip_id
 */
class DigitalLogBook extends CActiveRecord
{
	public $from_date;
	public $to_date;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DigitalLogBook the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{digital_log_book}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name, user_dept, vehicle_reg_no, driver_pin, driver_name, dutytype_id, dutyday, start_date, end_date, start_time, end_time, start_point, end_point, start_meter, end_meter, night_halt', 'required'),
			array('id, requisition_id, user_level, driver_pin, dutytype_id, vehicletype_id, start_meter, end_meter,  	total_run, rph, night_halt, nh_amount,  active,morning_ta,lunch_ta,night_ta, used_mileage,user_cell,sup_mile_amount,helper_pin,submit,remove', 'numerical', 'integerOnly'=>true),
			array('total_ot_hour,total_ot_amount,helper_ot', 'type', 'type'=>'float'),
			array('user_name, user_pin', 'length', 'max'=>40),
			array('email', 'email'),
			array('vehicle_reg_no, driver_name, dutyday, vehicle_location, start_point, end_point, total_time, created_by, bill_amount,user_address,user_dept,helper_name', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, requisition_id, user_pin, user_name, user_level, user_dept, vehicle_reg_no, driver_pin, driver_name, dutytype_id, dutyday, start_date, end_date, start_time, end_time, start_point, end_point, start_meter, end_meter, night_halt, bill_amount, created_by, created_time, updated_by, updated_time, active, used_mileage, from_date, to_date,submit,remove,remark', 'safe', 'on'=>'search'),
            array('created_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
			array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
       
       array('expire_date_error', 'check'),
       
		);
	}
	 public function check()
     {
         if($this->start_meter > $this->end_meter) {
             $this->addError('expire_date_error', 'End Meter should not be less than Start Meter');
             return false;
         }

         return true;
     }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'dutytypes'=>array(self::BELONGS_TO, 'Dutytypes', 'dutytype_id'),
            'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'requisition_id' => 'Requisition',
			'user_pin' => 'User Pin',
			'user_name' => 'User Name',
			'user_level' => 'User Level',
			'user_dept' => 'User Dept',
			'email' => 'Email',
			'user_cell' => 'User Cell',
			'user_address' => 'User Address',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicletype',
			'vehicle_location' => 'Vehicle Location',
			'driver_pin' => 'Driver Pin',
			'driver_name' => 'Driver Name',
			'helper_pin' => 'Helper Pin',
			'helper_name' => 'Helper Name',
			'helper_ot' => 'Helper Ot',
			'dutytype_id' => 'Dutytype',
			'dutyday' => 'Dutyday',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'total_time' => 'Total Time',
			'start_point' => 'Start Point',
			'end_point' => 'End Point',
			'start_meter' => 'Start Meter',
			'end_meter' => 'End Meter',
			'total_run' => 'Total Run',
			'rph' => 'Rph',
			'night_halt' => 'Night Halt',
			'nh_amount' => 'Nh Amount',
			'morning_ta' => 'Morning Ta',
			'lunch_ta' => 'Lunch Ta',
			'night_ta' => 'Night Ta',
			'sup_mile_amount' => 'Sup Mile Amount',
			'bill_amount' => 'Bill Amount',
			'total_ot_hour' => 'Total Ot Hour',
			'total_ot_amount' => 'Total Ot Amount',
			'used_mileage' => 'Used Mileage',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'active' => 'Active',
			'trip_id' => 'Trip',
			'submit'=>'Submit',
			'remove'=>"Remove",
			'remark'=>'Remark',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($remove=null)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		if($remove==1){
			//$criteria->condition="submit=1 OR remove=1 AND";
			$criteria->addCondition('submit=1 OR remove=1');
		}
		else{
			$criteria->compare('submit',0,true);
			$criteria->compare('remove',0,true);
		}

		$criteria->compare('id',$this->id);
		$criteria->compare('requisition_id',$this->requisition_id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('user_level',$this->user_level);
		$criteria->compare('user_dept',$this->user_dept,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('user_cell',$this->user_cell);
		$criteria->compare('user_address',$this->user_address,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('vehicle_location',$this->vehicle_location,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('helper_pin',$this->helper_pin);
		$criteria->compare('helper_name',$this->helper_name,true);
		$criteria->compare('helper_ot',$this->helper_ot);
		$criteria->compare('dutytype_id',$this->dutytype_id);
		$criteria->compare('dutyday',$this->dutyday,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('total_time',$this->total_time,true);
		$criteria->compare('start_point',$this->start_point,true);
		$criteria->compare('end_point',$this->end_point,true);
		$criteria->compare('start_meter',$this->start_meter);
		$criteria->compare('end_meter',$this->end_meter);
		$criteria->compare('total_run',$this->total_run);
		$criteria->compare('rph',$this->rph);
		$criteria->compare('night_halt',$this->night_halt);
		$criteria->compare('nh_amount',$this->nh_amount);
		$criteria->compare('morning_ta',$this->morning_ta);
		$criteria->compare('lunch_ta',$this->lunch_ta);
		$criteria->compare('night_ta',$this->night_ta);
		$criteria->compare('sup_mile_amount',$this->sup_mile_amount);
		$criteria->compare('bill_amount',$this->bill_amount);
		$criteria->compare('total_ot_hour',$this->total_ot_hour);
		$criteria->compare('total_ot_amount',$this->total_ot_amount);
		$criteria->compare('used_mileage',$this->used_mileage);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('trip_id',$this->trip_id,true);
		

		//$criteria->order = 'vehicle_reg_no,id DESC';
		
		if(!empty($this->from_date) && empty($this->to_date))
		{
			$criteria->addCondition('start_date = "'.$this->from_date.'"');
		}
		elseif(!empty($this->to_date) && empty($this->from_date))
		{
			$criteria->addCondition('start_date <= "'.$this->to_date.'"');
		}
		elseif(!empty($this->to_date) && !empty($this->from_date))
		{
			//$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
			$criteria->addCondition('start_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
		}
		
		//$user_vehicle=Yii::app()->db->createCommand("SELECT vehicle_re_no FROM tbl_manage_vehicle_user WHERE user_name='".Yii::app()->user->username."'")->queryAll();
		 $user_vehicle=ManageVehicleUser::model()->findAll('user_name=:user_name',array(':user_name'=>Yii::app()->user->username));
		 if(count($user_vehicle)>0){
		function vehicles($v){
			return $v['vehicle_re_no'];
		}
		$users_vehicles=array_map('vehicles', $user_vehicle);
		
		$criteria->addInCondition('vehicle_reg_no',$users_vehicles);
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>45,
            ),
            'sort'=>array('defaultOrder'=>'vehicle_reg_no,id ')
		));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getColor()
    {
        $sql = "SELECT * FROM tbl_digital_log_book_history WHERE trip_id='" . $this->trip_id . "' AND (objection<>'' OR objection IS NOT NULL) AND objection_solve=0";
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($result)) {
            return "fontColorRed";
        }       

    }
}