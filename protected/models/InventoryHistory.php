<?php

/**
 * This is the model class for table "{{inventory_history}}".
 *
 * The followings are the available columns in table '{{inventory_history}}':
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $inventory_id
 * @property integer $product_attributes_id
 * @property integer $product_classification_id
 * @property string $unit
 * @property double $base_quantity
 * @property double $quantiy
 * @property double $unit_price
 * @property double $total_amount
 * @property integer $manufacture_id
 * @property integer $supplier_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class InventoryHistory extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InventoryHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{inventory_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('inventory_id, product_attributes_id, product_classification_id, unit, base_quantity, quantiy, unit_price, total_amount, created_by', 'required'),
			array('invoice_id, inventory_id, product_attributes_id, product_classification_id, manufacture_id, supplier_id', 'numerical', 'integerOnly'=>true),
			array('base_quantity, quantiy, unit_price, total_amount', 'numerical'),
			array('unit, created_by, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invoice_id, inventory_id, product_attributes_id, product_classification_id, unit, base_quantity, quantiy, unit_price, total_amount, manufacture_id, supplier_id, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_id' => 'Invoice',
			'inventory_id' => 'Inventory',
			'product_attributes_id' => 'Product Attributes',
			'product_classification_id' => 'Product Classification',
			'unit' => 'Unit',
			'base_quantity' => 'Base Quantity',
			'quantiy' => 'Quantiy',
			'unit_price' => 'Unit Price',
			'total_amount' => 'Total Amount',
			'manufacture_id' => 'Manufacture',
			'supplier_id' => 'Supplier',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invoice_id',$this->invoice_id);
		$criteria->compare('inventory_id',$this->inventory_id);
		$criteria->compare('product_attributes_id',$this->product_attributes_id);
		$criteria->compare('product_classification_id',$this->product_classification_id);
		$criteria->compare('unit',$this->unit,true);
		$criteria->compare('base_quantity',$this->base_quantity);
		$criteria->compare('quantiy',$this->quantiy);
		$criteria->compare('unit_price',$this->unit_price);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('manufacture_id',$this->manufacture_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}