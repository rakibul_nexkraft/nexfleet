<?php

/**
 * This is the model class for table "{{feedback_settings}}".
 *
 * The followings are the available columns in table '{{feedback_settings}}':
 * @property integer $id
 * @property string $heading
 * @property string $user_title
 * @property string $user_rating_title
 * @property string $driver_title
 * @property string $driver_rating_title
 * @property string $ifleet_title
 * @property string $ifleet_rating_title
 * @property string $created_time
 * @property string $created_by
 * @property string $updated_time
 * @property string $updated_by
 */
class FeedbackSettings extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FeedbackSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{feedback_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('heading, user_title, user_rating_title, driver_title, driver_rating_title, ifleet_title, ifleet_rating_title,rating_popup,created_time, created_by', 'required'),
			array('heading, user_title, user_rating_title, driver_title, driver_rating_title, ifleet_title, ifleet_rating_title, created_by, updated_by', 'length', 'max'=>128),
			array('updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, heading, user_title, user_rating_title, driver_title, driver_rating_title, ifleet_title, ifleet_rating_title, created_time, created_by, updated_time, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'heading' => 'Heading',
			'user_title' => 'User Title',
			'user_rating_title' => 'User Rating Title',
			'driver_title' => 'Driver Title',
			'driver_rating_title' => 'Driver Rating Title',
			'ifleet_title' => 'Ifleet Title',
			'ifleet_rating_title' => 'Ifleet Rating Title',
			'created_time' => 'Created Time',
			'created_by' => 'Created By',
			'updated_time' => 'Updated Time',
			'updated_by' => 'Updated By',
			'rating_popup'=>'5 Or Below 1.5 Rating Popup Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('heading',$this->heading,true);
		$criteria->compare('user_title',$this->user_title,true);
		$criteria->compare('user_rating_title',$this->user_rating_title,true);
		$criteria->compare('driver_title',$this->driver_title,true);
		$criteria->compare('driver_rating_title',$this->driver_rating_title,true);
		$criteria->compare('ifleet_title',$this->ifleet_title,true);
		$criteria->compare('ifleet_rating_title',$this->ifleet_rating_title,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}