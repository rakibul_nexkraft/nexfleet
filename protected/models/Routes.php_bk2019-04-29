<?php

/**
 * This is the model class for table "{{routes}}".
 *
 * The followings are the available columns in table '{{routes}}':
 * @property integer $id
 * @property string $route_no
 * @property string $route_detail
 * @property integer $seat_capacity
 * @property integer $available_seat
 * @property string $remarks
 * @property string $created_by
 * @property string $created_time
 * @property integer $active
 */
class Routes extends CActiveRecord
{
	public $on_mat_leave;
	public $seat_capacity;
	public $available_seat;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Routes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{routes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('route_no, route_detail, actual_seat', 'required'),
			array('seat_capacity, available_seat, active, actual_seat', 'numerical', 'integerOnly'=>true),
			array('route_no', 'length', 'max'=>20),
			array('route_detail, created_by', 'length', 'max'=>127),
			array('remarks, on_mat_leave', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, route_no, route_detail, seat_capacity, available_seat, remarks, created_by, created_time, active', 'safe', 'on'=>'search'),
            array('created_time','default',
                'value'=>new CDbExpression('NOW()'),
                'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'route_no' => 'Route No',
			'route_detail' => 'Route Details',
			'actual_seat' => 'Seat Capacity',
			'seat_capacity' => 'Border Pass Issued',
			'available_seat' => 'Available Seat',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'active' => 'Active',
			'on_mat_leave'=>'On Maternity Leave',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('route_no',$this->route_no,true);
		$criteria->compare('route_detail',$this->route_detail,true);
		$criteria->compare('seat_capacity',$this->seat_capacity);
		$criteria->compare('available_seat',$this->available_seat);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
			),
		));
	}
	
	public function onMatLeave($route)
	{
		$sql = 'SELECT COUNT(id) FROM `tbl_commonfleets` WHERE `application_type`="Maternity Leave" AND `approve_status`="Approve" AND `present_route`="'.$route.'"';
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		return $result[0]["COUNT(id)"];
	}
	
	public function borderPass($route_no)
	{
		$sql = "SELECT COUNT(id) FROM tbl_commonfleets WHERE present_route = '$route_no'";
		$result = Yii::app()->db->createCommand($sql)->queryAll();
		$pass_no = $result[0]['COUNT(id)'];
		return $pass_no;
	}
	
	public function availableSeat($actual_seat, $seat_capacity, $route_no)
	{
		$av_seat = $actual_seat - $seat_capacity;
		if($route_no == "PH" || $route_no == "PC")
			return 0;
		elseif($av_seat < 0)
			return '<span style="color:red;">'.$av_seat.'</span>';
		else
			return '<span style="color:green;">'.$av_seat.'</span>';
	}
}