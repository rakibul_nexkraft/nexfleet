<?php

/**
 * This is the model class for table "{{invoice}}".
 *
 * The followings are the available columns in table '{{invoice}}':
 * @property integer $id
 * @property string $invoice_number
 * @property double $total_quantity
 * @property double $total_amount
 * @property string $challan_no
 * @property string $challan_date
 * @property string $invoice_date
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Invoice extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Invoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{invoice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('challan_no, challan_date, created_by', 'required'),
			array('total_quantity, total_amount', 'numerical'),
			array('invoice_number, challan_no, created_by, updated_by', 'length', 'max'=>128),
			array('invoice_date, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invoice_number, total_quantity, total_amount, challan_no, challan_date, invoice_date, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice_number' => 'Invoice Number',
			'total_quantity' => 'Total Quantity',
			'total_amount' => 'Total Amount',
			'challan_no' => 'Challan No',
			'challan_date' => 'Challan Date',
			'invoice_date' => 'Invoice Date',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invoice_number',$this->invoice_number,true);
		$criteria->compare('total_quantity',$this->total_quantity);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('challan_no',$this->challan_no,true);
		$criteria->compare('challan_date',$this->challan_date,true);
		$criteria->compare('invoice_date',$this->invoice_date,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}