<?php

/**
 * This is the model class for table "{{renewals}}".
 *
 * The followings are the available columns in table '{{renewals}}':
 * @property integer $id
 * @property integer $vehicle_reg_no
 * @property string $present_fitness_date
 * @property string $next_fitness_date
 * @property string $present_tax_date
 * @property string $next_tax_date
 * @property string $present_insurance_date
 * @property string $next_insurance_date
 * @property string $present_routpermit_date
 * @property string $next_routpermit_date
 * @property string $created_time
 * @property string $created_by
 * @property integer $active
 */
class Renewals extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Renewals the static model class
	 */
    
        public $search_field;
        public $from;
        public $to;
        public $fields = array(
            'present_fitness_date' => 'Present Fitness Date',
            'next_fitness_date' => 'Next Fitness Date',
            'present_tax_date' => 'Present Tax Token Date',
            'next_tax_date' => 'Next Tax Token Date',
            'present_insurance_date' => 'Present Insurance Date',
            'next_insurance_date' => 'Next Insurance Date',
            'present_routpermit_date' => 'Present Rout Permit Date',
            'next_routpermit_date' => 'Next Rout Permit Date',
            'present_ait_date' => 'Present AIT Date',
            'next_ait_date' => 'Next AIT Date',
			
        );


        public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{renewals}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                    array('vehicle_reg_no', 'required'),
                    array('active, number_plate_cost, vehicletype_id', 'numerical', 'integerOnly'=>true),
                    array('created_by, vehicle_reg_no, registration_amount,insurance_amount,tax_token,fitness_amount,routpermit_amount,advanced_tax_amount,miscellaneous_fee, number_plate_cost, next_routpermit_date', 'length', 'max'=>127),
                    // The following rule is used by search().
                    // Please remove those attributes that should not be searched.
                    array('id, vehicle_reg_no, present_fitness_date, next_fitness_date, present_tax_date, next_tax_date, present_insurance_date, next_insurance_date, present_routpermit_date, next_routpermit_date, created_time, created_by, active, search_field, from, to', 'safe', 'on'=>'search'),
                    
                    array('registration_amount, insurance_amount, tax_token, routpermit_amount, advanced_tax_amount, miscellaneous_fee, next_routpermit_date, 
                        registration_date, present_fitness_date, next_fitness_date, present_tax_date, next_tax_date, present_insurance_date, present_ait_date,next_ait_date, 
                        next_insurance_date, present_routpermit_date, search_field, from, to','safe'),
                    array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'renewals_log'=>array(self::HAS_MANY, 'RenewalsLog', 'renewal_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
            'vehicletype_id' => 'Vehicle Type',
			'next_fitness_date' => 'Next Fitness Date',
			'present_tax_date' => 'Present Tax Token Date',
			'next_tax_date' => 'Next Tax Token Date',
			'present_insurance_date' => 'Present Insurance Date',
			'next_insurance_date' => 'Next Insurance Date',
			'present_routpermit_date' => 'Present Rout Permit Date',
			'next_routpermit_date' => 'Next Rout Permit Date',
            'present_ait_date' => 'Present AIT Date',
            'next_ait_date' => 'Next AIT Date',
			'created_time' => 'Creation Time',
			'created_by' => 'Created By',
			'active' => 'Active',
			'registration_date' => 'Registration Date',
			'registration_amount' => 'Registration Cost',
			'insurance_amount' => 'Insurance Cost',
			'tax_token' => 'Tax Token Amount',
			'routpermit_amount' => 'Route Permit Cost',
			'advanced_tax_amount' => 'Advanced Tax Amount',
			'number_plate_cost' => 'Number Plate Cost',
			'miscellaneous_fee' => 'Miscellaneous Cost',
			'fitness_amount' => 'Fitness Amount',
                        'search_field' => 'Field To Search',
                        'from' => 'From Date',
                        'to' => 'To Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
        $criteria->compare('vehicletype_id',$this->vehicletype_id);
        $criteria->compare('present_fitness_date',$this->present_fitness_date,true);
		$criteria->compare('present_fitness_date',$this->present_fitness_date,true);
		$criteria->compare('next_fitness_date',$this->next_fitness_date,true);
		$criteria->compare('present_tax_date',$this->present_tax_date,true);
		$criteria->compare('next_tax_date',$this->next_tax_date,true);
		$criteria->compare('present_insurance_date',$this->present_insurance_date,true);
		$criteria->compare('next_insurance_date',$this->next_insurance_date,true);
		$criteria->compare('present_routpermit_date',$this->present_routpermit_date,true);
		$criteria->compare('next_routpermit_date',$this->next_routpermit_date,true);
        $criteria->compare('present_ait_date',$this->present_ait_date,true);
        $criteria->compare('next_ait_date',$this->next_ait_date,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('active',$this->active);
                
                if(empty($this->search_field) && (!empty($this->from) || !empty($this->to)))
                    $this->addError ('search_field', "Can not be empty!");
                elseif(!empty($this->to) && $this->to < $this->from)
                    $this->addError ('to', "'To Date' Must Follow 'From Date'.");
                elseif(!empty($this->from) && empty($this->to))
                {
                    $criteria->addCondition('DATE('.$this->search_field.') = "'.$this->from.'"');
                }
                elseif(!empty($this->to) && empty($this->from))
                {
                    $criteria->addCondition('DATE('.$this->search_field.') <= "'.$this->to.'"');
                }
                elseif(!empty($this->to) && !empty($this->from))
                {
                    //$criteria->condition = "updated_time  >= '$this->from_date' and updated_time <= '$this->to_date'";
                    $criteria->addCondition('DATE('.$this->search_field.') BETWEEN "'.$this->from.'" AND "'.$this->to.'"');
                }
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
			),
		));
	}
}
?>