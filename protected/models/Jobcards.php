<?php

/**
 * This is the model class for table "{{jobcards}}".
 *
 * The followings are the available columns in table '{{jobcards}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property integer $vehicletype_id
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string $milage
 * @property integer $wsrequisition_id
 * @property integer $defect_id
 * @property string $job_description
 * @property string $outside_work
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 * @property integer $active
 */
class Jobcards extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Jobcards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{jobcards}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array(' vehicle_reg_no, vehicletype_id, start_date, end_date, start_time, end_time, wsrequisition_id, defect_id, job_description, active', 'required'),
			array('id, vehicletype_id, wsrequisition_id, defect_id, active', 'numerical', 'integerOnly'=>true),
			array('vehicle_reg_no, start_time, end_time, milage, created_by, updated_by', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, vehicle_reg_no, vehicletype_id, start_date, end_date, start_time, end_time, milage, wsrequisition_id, defect_id, job_description, outside_work, created_by, created_time, updated_by, updated_time, active', 'safe', 'on'=>'search'),
        array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
        array('updated_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'tasks'=>array(self::HAS_MANY, 'Tasks', 'defect_id'),
		'wsinsides'=>array(self::HAS_MANY, 'Wsinsides', 'jobcard_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicletype_id' => 'Vehicletype',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'milage' => 'Milage',
			'wsrequisition_id' => 'Requisition',
			'defect_id' => 'Defect',
			'job_description' => 'Job Description',
			'outside_work' => 'Outside Work',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('milage',$this->milage,true);
		$criteria->compare('wsrequisition_id',$this->wsrequisition_id);
		$criteria->compare('defect_id',$this->defect_id);
		$criteria->compare('job_description',$this->job_description,true);
		$criteria->compare('outside_work',$this->outside_work,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	 public function tasklog(){
        

        /* $sql = " SELECT a.pin,a.name,b.vehicle_reg_no,b.vehicletype_id, c.type FROM tbl_drivers AS a
INNER JOIN tbl_vehicles_log AS b
ON a.pin = b.driver_pin
INNER JOIN tbl_vehicletypes AS c
ON c.id = b.vehicletype_id
WHERE a.pin = '".$this->pin."'";*/
$sql  =  "select id, task_desc from tbl_tasks WHERE defect_id = '".$this->defect_id."'";
     
        return new CSqlDataProvider($sql, array(
       //  'criteria'=>$criteria,
      
             'pagination'=>array(
             'pageSize'=>30,
            ),
        ));

    }
    
     public function wsrequisitionlog(){
        
        $sql  =  "select id, parts, quantity from tbl_wsrequisitions WHERE id = '".$this->wsrequisition_id."'";
     
        return new CSqlDataProvider($sql, array(
       //  'criteria'=>$criteria,
      
             'pagination'=>array(
             'pageSize'=>30,
            ),
        ));

    }
	
	
	
	
	
	
}