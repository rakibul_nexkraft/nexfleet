<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/14/2019
 * Time: 3:53 PM
 */
class VehicleSearchModel extends CFormModel {

    public $vehicle_reg_no;
    public $driver_pin;
    public $search_error;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('vehicle_reg_no', 'validateForm'),
//            array('driver_pin', 'validateForm'),
        );
    }

    public function validateForm() {
        if ($this->vehicle_reg_no == "" && $this->driver_pin == "") {
            $this->addError("search_error", 'Enter Vehicle Reg No or Driver Pin.');
        } else {
            return true;
        }
    }

}