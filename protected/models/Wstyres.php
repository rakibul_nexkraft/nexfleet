<?php

/**
 * This is the model class for table "{{wstyres}}".
 *
 * The followings are the available columns in table '{{wstyres}}':
 * @property integer $id
 * @property string $vehicle_reg_no
 * @property string $vehicle_type
 * @property string $purchase_date
 * @property string $DO_no_date
 * @property integer $km_reading
 * @property string $supplier
 * @property string $bill_number
 * @property double $bill_amount
 * @property integer $warrenty_km
 * @property integer $warrenty_valid_km_upto
 * @property integer $actual_use_km
 * @property string $remarks
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Wstyres extends CActiveRecord
{
    public $from_date;
    public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wstyres the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wstyres}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_reg_no, vehicle_type, purchase_date, km_reading, supplier, challan_no, amount, warrenty_km, warrenty_valid_km_upto, actual_use_km', 'required'),
			array('km_reading, defect_id, warrenty_km, warrenty_valid_km_upto, actual_use_km', 'numerical', 'integerOnly'=>true),
			array('amount,total_price,unit_price', 'numerical'),
            array('purchase_date, requisition_date, challan_date,bill_date', 'safe'),
			array('vehicle_reg_no, vehicle_type, requisition_no, tyre_size, particulars, quantity, supplier, challan_no, status, remarks, created_by, updated_by,bill_no,tyre_no', 'length', 'max'=>127),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, defect_id, vehicle_reg_no, vehicle_type, purchase_date, , km_reading, supplier, challan_no, amount, warrenty_km, warrenty_valid_km_upto, actual_use_km, status, created_by, created_time, updated_by, updated_time,from_date,to_date', 'safe', 'on'=>'search'),
            array('created_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
            array('updated_time','default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicle_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'defect_id' => 'Defect ID',
			'vehicle_reg_no' => 'Vehicle Reg No',
			'vehicle_type' => 'Vehicle Type',
			'purchase_date' => 'Purchase Date',
			'requisition_no' => 'Requisition No',
            'requisition_date' => 'Requisition Date',
			'km_reading' => 'Km Reading',
            'particulars' => 'Particulars',
			'supplier' => 'Supplier',
            'tyre_size' => 'Size',
            'quantity' => 'Quantity',
			'challan_no' => 'Challan No',
            'challan_date' => 'Challan Date',
			'amount' => 'Amount',
			'warrenty_km' => 'Warranty KM',
			'warrenty_valid_km_upto' => 'Warrenty Valid KM Upto',
			'actual_use_km' => 'Actual Used KM',
            'status' => 'Status',
			'remarks' => 'Remarks',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
            'from_date'=>'Purchase Date (From)',
            'to_date'=>'Purchase Date (To)',
            'bill_no'=>"Bill No",
            'bill_date'=>"Bill Date",
            'unit_price'=>"Unit Price",
            'total_price'=>"Total Price",
            'tyre_no'=>'Tyre No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('defect_id',$this->defect_id,true);
		$criteria->compare('vehicle_reg_no',$this->vehicle_reg_no,true);
		$criteria->compare('vehicle_type',$this->vehicle_type,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('requisition_no',$this->requisition_no);
		$criteria->compare('km_reading',$this->km_reading);
		$criteria->compare('supplier',$this->supplier,true);
		$criteria->compare('challan_no',$this->challan_no,true);
        $criteria->compare('challan_date',$this->challan_date,true);
        $criteria->compare('bill_date',$this->challan_date);
        $criteria->compare('bill_no',$this->bill_no,true);
        $criteria->compare('unit_price',$this->unit_price);
        $criteria->compare('total_price',$this->total_price);
        $criteria->compare('quantity',$this->quantity);
        $criteria->compare('tyre_no',$this->tyre_no,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('warrenty_km',$this->warrenty_km);
		$criteria->compare('warrenty_valid_km_upto',$this->warrenty_valid_km_upto);
		$criteria->compare('actual_use_km',$this->actual_use_km);
        $criteria->compare('status',$this->status,true);        
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('quantity',$this->quantity,true);
		

        if(!empty($this->from_date) && empty($this->to_date))
        {
            $criteria->addCondition('purchase_date = "'.$this->from_date.'"');
        }
        elseif(!empty($this->to_date) && empty($this->from_date))
        {
            $criteria->addCondition('purchase_date <= "'.$this->to_date.'"');
        }
        elseif(!empty($this->to_date) && !empty($this->from_date))
        {
            //$criteria->condition = "start_date  >= '$this->from_date' and start_date <= '$this->to_date'";
            $criteria->addCondition('purchase_date BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>40,
            ),
            'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}