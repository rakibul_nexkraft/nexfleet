<?php

/**
 * This is the model class for table "{{workflow}}".
 *
 * The followings are the available columns in table '{{workflow}}':
 * @property integer $id
 * @property string $workflow
 * @property string $duration
 * @property integer $is_parent
 * @property integer $parent_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 *
 * The followings are the available model relations:
 * @property JobCard[] $jobCards
 * @property JobCardHistory[] $jobCardHistories
 */
class Workflow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Workflow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{workflow}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('', 'required'),
			array('is_parent, parent_id', 'numerical', 'integerOnly'=>true),
			array('workflow, created_by, updated_by', 'length', 'max'=>150),
			array('duration', 'length', 'max'=>45),
			array('updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, workflow, duration, is_parent, parent_id, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'jobCards' => array(self::HAS_MANY, 'JobCard', 'workflow_id'),
			'jobCardHistories' => array(self::HAS_MANY, 'JobCardHistory', 'workflow_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'workflow' => 'Workflow',
			'duration' => 'Duration',
			'is_parent' => 'Is Parent',
			'parent_id' => 'Parent',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('workflow',$this->workflow,true);
		$criteria->compare('duration',$this->duration,true);
		$criteria->compare('is_parent',$this->is_parent);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function statusType($type){
    if($type==0)return "No";
    if($type==1)return "Yes";
	}
}