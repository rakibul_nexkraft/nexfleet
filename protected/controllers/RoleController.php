<?php

class RoleController extends CustomController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','userView','roleDelete'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionUserView($id)
	{
		if ($_GET['type'] == 3) {
			$model = $this->loadModel($id);
		}
		if ($_GET['type'] == 2) {
			$model = $this->actionUpdate($id);
		}
		if ($_GET['type'] == 1) {
			$model = $this->loadModel($id);
		}
		$this->renderpartial('modal_view_update_delete', array(
			'model' => $model,
			'type' => $_GET['type'],
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Role;
		
		if(isset($_POST['Role']))
		{
			$model->attributes=$_POST['Role'];
			$model->created_by = Yii::app()->user->username;
			$model->created_time=new CDbExpression('NOW()');
			if($model->save())
				if($_POST['Role']['rights']){        		
					foreach ($_POST['Role']['rights'] as $right_code => $right_value) { 
						if($right_value =='1') { 

							$model_role_rights=new RoleRights;	
							$model_role_rights->role_id=$model->id;	    
							$model_role_rights->created_time=date('Y-m-d h:i:s');
							$model_role_rights->created_by=yii::app()->user->id;
							$model_role_rights->rights_id=$right_code;
							$model_role_rights->save();
						}
					}
				}
				$this->redirect(array('view','id'=>$model->id));
			}

			$this->renderpartial('_form',array(
				'model'=>$model,
			));
		}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(isset($_POST['Role']))
		{
			$model->attributes=$_POST['Role'];
			$model->updated_by = Yii::app()->user->username;
			$model->updated_time=new CDbExpression('NOW()');
			if($model->save()){
				if($_POST['Role']['rights']){
					$user_rights=RoleRights::model()->findALL('role_id=:roleid',array(':roleid'=>$model->id));	
					$user_rights_code=array_map(function($element) {
						return $element['rights_id'];
					}, $user_rights);	
					
					$user_rihgts_update=array_keys($_POST['Role']['rights'], "1");  
					$user_rights_need_insert=array_diff($user_rihgts_update,$user_rights_code);		
					$user_rights_need_delete=array_diff($user_rights_code,$user_rihgts_update);
					
					$criteria=new CDbCriteria;
					$criteria->condition="role_id=:roleid";
					$criteria->params=array(':roleid'=>$model->id);
					$criteria->addInCondition('rights_id',$user_rights_need_delete,'AND');		
					RoleRights::model()->deleteAll($criteria);        		        		
					foreach ($user_rights_need_insert as $key => $right_code) {
						$model_role_rights=new RoleRights; 
						$model_role_rights->role_id=$model->id; 
						$model_role_rights->created_time=date('Y-m-d h:i:s');
						$model_role_rights->created_by=yii::app()->user->username;
						$model_role_rights->updated_time=date('Y-m-d h:i:s');
						$model_role_rights->updated_by=yii::app()->user->username;	
						$model_role_rights->rights_id=$right_code;
						$model_role_rights->save();
					}
				}
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		return $model;
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionRoleDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Role');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Role('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Role']))
			$model->attributes=$_GET['Role'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Role::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='role-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
