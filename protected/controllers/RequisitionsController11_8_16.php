<?php

class RequisitionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','activeReq','callHrUser','getName','smssend'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','driversCopy','excel','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
	
		if (Yii::app()->request->isAjaxRequest)
		{
			//$this->layout = 'column_pop';
			//outputProcessing = true because including css-files ...
			$this->renderPartial('view', 
				array(
				'model'=>$this->loadModel($id),
				),false,true);
			Yii::app()->end();
		}
	
		else 
		{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
		
	}
	
	public function actionDriversCopy($id)
	{
		$model = $this->loadModel($id);
		if ($model->active == 2){
			$this->render('_drivers_copy',array(
				'model'=>$model,
			));
		}
	}
	
	
public function actionSmssend($requisition_id,$user_cell,$vehicle_reg_no,$driver,$driver_cell,$start_date,$start_time,$destination)
{    
    //$r = file_get_contents("http://mydesk.brac.net/smsc/create?token=286cf3ae67c80ee7d034bb68167ab8455ea443a2&message=test&to_number=8801919197617");
    
            $client = new EHttpClient(
                'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
                    )
            );
            
               if(strlen($user_cell)==10)
            	$user_cell = "0".$user_cell;
      	    else $user_cell = $user_cell;
      	    
      	    
			
            if(strlen($driver_cell)==10)
            	$driver_cell = "0".$driver_cell;
      	    else $driver_cell = $driver_cell;
      
           if(!$vehicle_reg_no)
      	    $message = "It is not possible to provide vehicle against your transport requisition# $requisition_id due to non availability of vehicles. Sorry for inconvenience.";
      	   else
            $message= "Your transport requisition $requisition_id for ($start_date $start_time) has been approved. Veh: $vehicle_reg_no, Driver: $driver  Cell: $driver_cell.";            
            
            
            //$driver_message= "Please collect Requisition from Transport Dept. Duty Start Date: $start_date Start Time: $start_time Destination: $destination.";
            $driver_message= "Req # $requisition_id Duty: $start_date St Time: $start_time Destination: $destination User's Cell: $user_cell";             
        
            
            $client->setParameterPost(array(
                'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
                'message' => $message,
                'to_number' => $user_cell, 
                'app_url' => 'http://ifleet.brac.net/', 
                
            ));                                            
            
           // $client->set
            
            $response = $client->request('POST');                 
             $client->setParameterPost(array(
                'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
                'message' => $driver_message,
                'to_number' => $driver_cell, 
                'app_url' => 'http://ifleet.brac.net/', 
                
            ));                                            
            
             $response = $client->request('POST');     

//            
//			try{
//      	   //$soapClient = new SoapClient("http://192.168.2.39:8080/isoap.comm.smsc/SMSCService?wsdl"
//      	   //$soapClient = new SoapClient("http://192.168.20.109:8080/isoap.comm.smsc/SMSCService?wsdl"
//      	   //$soapClient = new SoapClient("http://172.25.100.41:8080/isoap.comm.smsc/SMSCService?wsdl"
//      	   $soapClient = new SoapClient("http://172.26.4.79:8080/smsc/SMSCService?WSDL"
//      	   
//            //array('proxy_host'=> "",
//            //'proxy_port'    => ,
//            //'proxy_login'    => "",
//            //'proxy_password' => "")
//            );
//            
//            
//            if(strlen($user_cell)==10)
//            	$user_cell = "+880".$user_cell;
//      	    else $user_cell = $user_cell;
//      	    
//      	    
//			
//			if(strlen($driver_cell)==10)
//            	$driver_cell = "+880".$driver_cell;
//      	    else $driver_cell = $driver_cell;
//			
//      	    
//      	    if(!$vehicle_reg_no)
//      	    $message = "It is not possible to provide vehicle against your transport requisition# $requisition_id due to non availability of vehicles. Sorry for inconvenience.";
//      	   
//            $message= "Your transport requisition $requisition_id has been approved. Veh: $vehicle_reg_no, Driver: $driver  Cell: $driver_cell.";            
//            //$driver_message= "Please collect Requisition from Transport Dept. Duty Start Date: $start_date Start Time: $start_time Destination: $destination.";
//            $driver_message= "Req # $requisition_id Duty: $start_date St Time: $start_time Destination: $destination User's Cell: $user_cell";             
//        
//
//      		//$username = array('userName'=>"",'password'=>"",'message'=>array('message'=>$message,'numberList'=>$user_cell,'gateway'=>'MIT,'mask'=>'BRAC','client'=>'ifleet','fromnumber'=>'+8801711057908'));
//      		$username = array('userName'=>"",'password'=>"",'message'=>array('message'=>$message,'numberList'=>$user_cell,'gateway'=>'iFleet','fromnumber'=>'+8801711057908'));
//      	    
//			  	$send_sms=$soapClient->__call('sendSms',array($username));			    			    			  
//			  	
//			  	$username = array('userName'=>"",'password'=>"",'message'=>array('message'=>$driver_message,'numberList'=>$driver_cell,'gateway'=>'iFleet'));
//			  	$send_sms=$soapClient->__call('sendSms',array($username));			    			    			  			  			
//			  			  			  				    
//			}
//			catch (SoapFault $fault) {
//				    $error = 1;
//				    print($fault->faultcode."-".$fault->faultstring);
//			}
		
   		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Requisitions;
		$model->created_by = Yii::app()->user->username;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Requisitions']))
		{
			$model->attributes=$_POST['Requisitions'];
			
			
			
			$model->updated_by = Yii::app()->user->username;
			$model->updated_time = new CDbExpression('NOW()');
			   					
			if($model->save())
			{
				if ($model->active==2) {
                                    
				$this->actionSmssend($model['id'],$model['user_cell'],$model['vehicle_reg_no'],$model['driver_name'],$model['driver_phone'],$model['start_date'],$model['start_time'],$model['end_point']);                                
			//	else
			//	{
			//			$this->actionSmssend($model['id']);
			//	}
			}
				if(!empty($model->email)){
					if ($model->active==2) {
						// && !empty($model->vehicle_reg_no)
						//$message = 'Test Mail';
						//Yii::app()->mailer->Host = '192.168.2.2';
                        Yii::app()->mailer->Host = '172.25.100.113';
						Yii::app()->mailer->IsSMTP();
						Yii::app()->mailer->From = 'ifleet@brac.net';
						Yii::app()->mailer->FromName = 'iFleet';
						//Yii::app()->mailer->AddReplyTo('brac.ictweb@gmail.com');
						//Yii::app()->mailer->AddAddress('shouman.das@gmail.com');
						Yii::app()->mailer->AddAddress($model->email);
						Yii::app()->mailer->Subject = 'Transport Request Status Notification';
						Yii::app()->mailer->Body = $this->renderPartial('_email', array('model'=>$model), true);
						Yii::app()->mailer->Send();
					}
					else {
						// && !empty($model->vehicle_reg_no)
						//$message = 'Test Mail';
						//Yii::app()->mailer->Host = '192.168.2.2';
                        Yii::app()->mailer->Host = '172.25.100.113';
						Yii::app()->mailer->IsSMTP();
						Yii::app()->mailer->From = 'ifleet@brac.net';
						Yii::app()->mailer->FromName = 'iFleet';
						//Yii::app()->mailer->AddReplyTo('brac.ictweb@gmail.com');
						//Yii::app()->mailer->AddAddress('shouman.das@gmail.com');
						Yii::app()->mailer->AddAddress($model->email);
						Yii::app()->mailer->Subject = 'Transport Request Status Notification';
						Yii::app()->mailer->Body = $this->renderPartial('_email_declined', array('model'=>$model), true);
						Yii::app()->mailer->Send();
					}
				}
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		
			
		$model=$this->loadModel($id);
		$prev_status = $model->active;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['Requisitions']))
		{
			
			
			$model->attributes=$_POST['Requisitions'];
			
			
	
   		
   		
			$model->updated_by = Yii::app()->user->username;
			$model->updated_time = new CDbExpression('NOW()');
			
			if($model->save())
			{
						if ($model->active==2) {
							$this->actionSmssend($model['id'],$model['user_cell'],$model['vehicle_reg_no'],$model['driver_name'],$model['driver_phone'],$model['start_date'],$model['start_time'],$model['end_point']);
						}
	//					else
	//			{
	//					$this->actionSmssend($model['id']);
	//			}
			
				if(!empty($model->email) && $prev_status != $model->active){
					if($model->active==2) {
						// && !empty($model->vehicle_reg_no)
						//$message = 'Test Mail';
						//Yii::app()->mailer->Host = '192.168.2.2';
                        Yii::app()->mailer->Host = '172.25.100.113';
						Yii::app()->mailer->IsSMTP();
						Yii::app()->mailer->From = 'ifleet@brac.net';
						Yii::app()->mailer->FromName = 'iFleet';
						//Yii::app()->mailer->AddReplyTo('brac.ictweb@gmail.com');
						//Yii::app()->mailer->AddAddress('shouman.das@gmail.com');
						Yii::app()->mailer->AddAddress($model->email);
						Yii::app()->mailer->Subject = 'Transport Request Status Notification';
						Yii::app()->mailer->Body = $this->renderPartial('_email', array('model'=>$model), true);
						Yii::app()->mailer->Send();
					}
					elseif($model->active==1) {
						// && !empty($model->vehicle_reg_no)
						//$message = 'Test Mail';
						//Yii::app()->mailer->Host = '192.168.2.2';
                        Yii::app()->mailer->Host = '172.25.100.113';
						Yii::app()->mailer->IsSMTP();
						Yii::app()->mailer->From = 'ifleet@brac.net';
						Yii::app()->mailer->FromName = 'iFleet';
						//Yii::app()->mailer->AddReplyTo('brac.ictweb@gmail.com');
						//Yii::app()->mailer->AddAddress('shouman.das@gmail.com');
						Yii::app()->mailer->AddAddress($model->email);
						Yii::app()->mailer->Subject = 'Transport Request Status Notification';
						Yii::app()->mailer->Body = $this->renderPartial('_email_declined', array('model'=>$model), true);
						Yii::app()->mailer->Send();
					}
				}
				$this->redirect(array('view','id'=>$model->id));
			}
        }
		
		if(Yii::app()->request->isAjaxRequest)
		{
			//$this->layout = 'column_pop';
			$this->renderPartial('update',array(
				'model'=>$model,
			),false,true);
			Yii::app()->end();
		}
		else
		{
			$this->render('update',array(
				'model'=>$model,
			));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        throw new CHttpException(400,'Delete operation is stopped.');
        return;

        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Requisitions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		
        $model=new Requisitions('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Requisitions']))
			$model->attributes=$_GET['Requisitions'];
			
		$this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$model->search(),
			));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Requisitions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Requisitions']))
			$model->attributes=$_GET['Requisitions'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Requisitions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionActiveReq() {
		
		$requisition_id = $_REQUEST['Movements']['requisition_id'];
		
		
	  if (!empty($requisition_id)) {
	  	
	  
	  	
	
	  	
	 	$sql = "SELECT vehicle_reg_no,driver_pin,driver_name,user_pin,user_name,user_level,dept_name,dutytype_id,vehicletype_id	FROM tbl_requisitions  WHERE id =  '$requisition_id'";		
	 	
		
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();
		
		echo CJSON::encode($result[0]); exit;
	  } else {
	  	
	  	
		return false;
	  }
	}
	

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='requisitions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionGetName() 
	{		
		if (!empty($_GET['Requisitions']['driver_pin'])) {
	  	$qterm = $_GET['Requisitions']['driver_pin'];
	  	$start_date = $_GET['Requisitions']['start_date'];
	  	$end_date = $_GET['Requisitions']['end_date'];
	  	$start_time = $_GET['Requisitions']['start_time'];
	  	$end_time = $_GET['Requisitions']['end_time'];
	  	
	  	
	  	$allocated_sql = "SELECT driver_pin FROM tbl_requisitions where ((start_time  between '$start_time' AND '$end_time') OR (end_time  between '$start_time' AND '$end_time')) AND ((start_date  between '$start_date' AND '$end_date') AND (end_date  between '$start_date' AND '$end_date')) AND driver_pin =  '$qterm'";
	  	$command = Yii::app()->db->createCommand($allocated_sql);				
			$allocated = $command->queryAll();
	  	if(empty($allocated))
	  	{
	 		$sql = "SELECT name,phone	 FROM tbl_drivers  WHERE pin =  '$qterm'";
	
			$command = Yii::app()->db->createCommand($sql);				
			$result = $command->queryAll();
		
	  	echo CJSON::encode($result[0]); exit;
	  	}
	  	else 
	  		echo CJSON::encode("allocated"); exit;
		
	  	} 
	  	else 
	  	{
				return false;
	  	}
	}
	
	
	
	public function actionCallHrUser()
	{
		try{
	  	$soapClient = new SoapClient("http://172.25.100.86:800/StaffInfo.asmx?wsdl",
			array(
				'proxy_login'    => "",
				'proxy_password' => "")
			);
				
	  	$user_pin = $_REQUEST['Requisitions']['user_pin'];
			
			if(!empty($_REQUEST['Requisitions']['supervisor_pin']))
			{
				$user_pin = $_REQUEST['Requisitions']['supervisor_pin'];
			}
	
    	$StaffPIN=array('strStaffPIN'=>$user_pin);    	
    	$strStaffINFO=$soapClient->__call('StaffInfoByPIN', array($StaffPIN));    	
     	
     
    	$varb = CJSON::decode($strStaffINFO->StaffInfoByPINResult);        
      
    	echo CJSON::encode($varb[0]); exit;
  	}
		catch (SoapFault $fault) {
    	$error = 1;
    	print($fault->faultcode."-".$fault->faultstring);
		}	
	}

	public function actionExcel()
	{
		$model=new Requisitions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['criteria']))
			$model->attributes=$_GET['criteria'];
		
		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Requisitions',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Requisitions',
			//'stream'=>false,
			'columns'=>array(
				array(
					'name' => 'id',
					'type'=>'raw',
					'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
				),
				array(
					'name' => 'bpmt_ref_no',
					'type'=>'raw',
					'value' => 'CHtml::link(CHtml::encode($data->bpmt_ref_no),array("view","id"=>$data->id))',
				),
				'vehicle_reg_no',
				'driver_pin',
				'driver_name',
				'user_pin',
				'user_name',
				'user_level',
				'dept_name',
				'start_date',
				'end_date',
                'end_point',
                'updated_by',
				array(
					'name' => 'dutytype_id',
					'type' => 'raw',
					'value' => '$data->dutytypes->type_name',
				),
				array(
					'name' => 'active',
					'type' => 'raw',
					'value' => 'Requisitions::model()->statusActive($data->active)',
				)
			),
		));
		Yii::app()->end();
		$this->endWidget();
	}
	
		
	public function sendMailIcress($model)
	{		
		
		try{
  	   		//$soapClient = new SoapClient("http://192.168.20.114:8282/isoap.comm.imail/EmailWS?wsdl"
  	   		$soapClient = new SoapClient("http://172.25.100.41:8080/isoap.comm.imail/EmailWS?wsdl"
  	   		
        );
            		
				$job = new jobs;
				
				$job->subject='Transport Seat Allocation Status Notification';												
				$job->jobContentType='text/html';
				$job->fromAddress='ifleet@brac.net';
				$job->udValue1='iFleet';												
				
				$job->jobRecipients[0]=new jobRecipients;
				$job->jobRecipients[0]->recipientEmail="riad.ak@brac.net";						
				$job->jobRecipients[1]=new jobRecipients;
				$job->jobRecipients[1]->recipientEmail=$model->user_email;						
																										
				if($model->approve_status ==  'Approve')
				{
					if($model->application_type == "Pick and Drop")
						$job->body = $this->renderPartial('_email_pickndrop', array('model'=>$model), true);
					else if($model->application_type == "Change Route")
						$job->body =  $this->renderPartial('_email_routechange', array('model'=>$model), true);
					else if($model->application_type == "Cancel Seat")
						$job->body =  $this->renderPartial('_email_cancelseat', array('model'=>$model), true);
					else if($model->application_type == "Maternity Leave")
						$job->body =  $this->renderPartial('_email_matleave', array('model'=>$model), true);
				}
				if($model->approve_status == 'Pending')
				{
						if($model->application_type == "Pick and Drop")
								$job->body = $this->renderPartial('_email_pickndrop_pending', array('model'=>$model), true);
							else if($model->application_type == "Change Route")
								$job->body = $this->renderPartial('_email_routechange_pending', array('model'=>$model), true);						
				}				
															  												
				$jobs = array('jobs'=>$job);		      	
			  $send_email =$soapClient->__call('sendEmail',array($jobs));
                          
			  
			  }
					catch (SoapFault $fault) {
						    $error = 1;
						    print($fault->faultcode."-".$fault->faultstring);
					}																		
			}
			
}

class jobs {
  public $appUserId; // string
  public $attachments; // attachment
  public $bcc; // string
  public $body; // string
  public $caption; // string
  public $cc; // string
  public $complete; // boolean
  public $feedbackDate; // dateTime
  public $feedbackEmail; // string
  public $feedbackName; // string
  public $feedbackSent; // boolean
  public $fromAddress; // string
  public $fromText; // string
  public $gateway; // string
  public $jobContentType; // string
  public $jobId; // long
  public $jobRecipients; // jobRecipients
  public $mode; // string
  public $numberOfItem; // int
  public $numberOfItemFailed; // int
  public $numberOfItemSent; // int
  public $priority; // string
  public $requester; // string
  public $status; // string
  public $subject; // string
  public $toAddress; // string
  public $toText; // string
  public $udValue1; // string
  public $udValue2; // string
  public $udValue3; // string
  public $udValue4; // string
  public $udValue5; // string
  public $udValue6; // string
  public $udValue7; // string
  public $vtemplate; // string
}

class jobRecipients {
  public $failCount; // int
  public $image; // base64Binary
  public $job; // jobs
  public $jobDetailId; // long
  public $recipientEmail; // string
  public $sent; // boolean
  public $sentDate; // dateTime
  public $toText; // string
}
?>