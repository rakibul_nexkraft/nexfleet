<?php

class FieldrepairController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','getName'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','excel','update','pending'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'expression'=>'Yii::app()->user->isAdmin()',
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionView($id)
    {
        $test1 = Fieldrepair::model()->findByPk($id);
        $post=User::model()->find('username=:postID', array(':postID'=>$test1->created_by));
        $user_profile = User::model()->find('id=:postID1', array(':postID1'=>$post->id))->profile;
        //$user_profile = User::model()->findByPk(Yii::app()->user->id)->profile;

        $this->render('view',array(
            'model'=>$this->loadModel($id),
            'user_profile' => $user_profile
            //'test'=>'$this->test'
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
    public function actionCreate()
    {
        $model=new Fieldrepair;
        $model->created_by = Yii::app()->user->username;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Fieldrepair']))
        {
            $model->attributes=$_POST['Fieldrepair'];
            $model->apply_date = $_POST['Fieldrepair']['apply_date'];

            if($model->save())
				$this->redirect(array('view','id'=>$model->id));
        }

        if(Yii::app()->request->isAjaxRequest)
            $this->renderPartial('create',array(
                'model'=>$model,
            ), false, true);
        else
            $this->render('create',array(
                'model'=>$model,
            ));
    }
//	public function actionCreate()
//	{
//		$model=new Fieldrepair;
//
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//
//		if(isset($_POST['Fieldrepair']))
//		{
//			$model->attributes=$_POST['Fieldrepair'];
//			if($model->save())
//				$this->redirect(array('view','id'=>$model->id));
//		}
//
//		$this->render('create',array(
//			'model'=>$model,
//		));
//	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->updated_by = Yii::app()->user->username;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fieldrepair']))
		{
			$model->attributes=$_POST['Fieldrepair'];
            $model->apply_date = $_POST['Fieldrepair']['apply_date'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
    public function actionDelete($id)
    {
//        throw new CHttpException(404,'You are not authorized to delete.');

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Fieldrepair('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Fieldrepair']))
            $model->attributes=$_GET['Fieldrepair'];

        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$model->search(),

        ));
	}

//    public function actionPending()
//    {
//
//        $model=new Defects('searchpending');
//        $model->unsetAttributes();  // clear any default values
//        if(isset($_GET['Defects']))
//            $model->attributes=$_GET['Defects'];
//
//        $this->render('pending',array(
//            'model'=>$model,
//            'dataProvider'=>$model->searchpending(),
//
//        ));
//    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Fieldrepair('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fieldrepair']))
			$model->attributes=$_GET['Fieldrepair'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionGetName() {

        if (!empty($_GET['Fieldrepair']['driver_pin'])) {
            $qterm = $_GET['Fieldrepair']['driver_pin'];
            $sql = "SELECT name	 FROM tbl_drivers  WHERE pin =  '$qterm'";


            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            echo CJSON::encode($result[0]); exit;

        } else {
            return false;
        }
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Fieldrepair::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fieldrepair-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionExcel()
    {
        $model=new Fieldrepair('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'iFleet_Fieldrepair',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Fieldrepair',

            'columns'=>array(
                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),
                'vehicle_reg_no',
                //    'vehicletype_id',
                array(
                    'name' => 'vehicletype_id',
                    'type'=>'raw',
                    'value' => '$data->vehicletypes->type',
                ),

                //'vehicle_reg_no',
                'driver_name',
                'driver_pin',
                'last_meter',
                'meter_reading',
                'apply_date',
                'approve_by',


                /*
                'created_time',
                'created_by',
                'active',
                */
            ),

        ));
        Yii::app()->end();
    }
}
