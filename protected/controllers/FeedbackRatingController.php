<?php

class FeedbackRatingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','excel'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','chart','excelPieChart'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FeedbackRating;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FeedbackRating']))
		{
			$model->attributes=$_POST['FeedbackRating'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FeedbackRating']))
		{
			$model->attributes=$_POST['FeedbackRating'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		$model=new FeedbackRating('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FeedbackRating']))
			$model->attributes=$_GET['FeedbackRating'];

		$this->render('index',array(
			'model'=>$model,
		));
		/*$dataProvider=new CActiveDataProvider('FeedbackRating',array(
			'pagination'=>array(
				'pageSize'=>40,
			),
		  'sort'=>array('defaultOrder'=>'id DESC')
       ));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FeedbackRating('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FeedbackRating']))
			$model->attributes=$_GET['FeedbackRating'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=FeedbackRating::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='feedback-rating-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionExcel()
	{
		$model = new FeedbackRating ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Feedback And Rating',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Feedback_Rating',
            //'stream'=>false,
           'columns'=>array(
		'id',
		'ifleet_rating',
		'ifleet_feedback',
		
		'driver_rating',
		'driver_feedback',
		array(          
            'name'=>'Driver Pin',
            'value'=>'FeedbackRating::driverPin($data->id)',
        ),
        array(          
            'name'=>'Driver Name',
            'value'=>'FeedbackRating::driverName($data->id)',
        ),
         /*array(          
            'name'=>'Driver Phone',
            'value'=>'FeedbackRating::driverContact($data->id)',
        ),*/
		'car_rating',
		'car_feedback',
		 array(          
            'name'=>'Vehicle Reg No',
            'value'=>'FeedbackRating::vehicleReg($data->id)',
        ),
		'created_by',
		array(          
            'name'=>'User Name',
            'value'=>'FeedbackRating::userName($data->id)',
        ),
         array(          
            'name'=>'User Department',
            'value'=>'FeedbackRating::userDepartment($data->id)',
        ),
          array(          
            'name'=>'End Point',
            'value'=>'FeedbackRating::vehicleEndPoint($data->id)',
        ),
         /*array(          
            'name'=>'User Phone',
            'value'=>'FeedbackRating::userContact($data->id)',
        ),*/
         'created_time',
		/*
		'ifleet_feedback',
		'created_by',
		'created_time',
		*/
		//array(
			//'class'=>'CButtonColumn',
		//),
	),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
	public function actionChart()
	{
		$start_date=$_POST['start_date'];
		$to_date=$_POST['to_date'];
		
		$sql="SELECT count(ifleet_rating) as ifleet_rating_0 FROM tbl_feedback_rating WHERE ifleet_rating=0 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$ifleet_rating_0= Yii::app()->db->createCommand($sql)->queryRow();
		
		$sql="SELECT count(ifleet_rating) as ifleet_rating_1 FROM tbl_feedback_rating WHERE (ifleet_rating<=1 AND ifleet_rating>0 ) AND (date(created_time)>='$start_date' AND date(created_time)<='$to_date')";
		$ifleet_rating_1= Yii::app()->db->createCommand($sql)->queryRow();
		
		$sql="SELECT count(ifleet_rating) as ifleet_rating_2 FROM tbl_feedback_rating WHERE ifleet_rating<=2 AND ifleet_rating>1 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$ifleet_rating_2 = Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(ifleet_rating) as ifleet_rating_3 FROM tbl_feedback_rating WHERE ifleet_rating<=3 AND ifleet_rating>2 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$ifleet_rating_3= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(ifleet_rating) as ifleet_rating_4 FROM tbl_feedback_rating WHERE ifleet_rating<=4 AND  ifleet_rating>3 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$ifleet_rating_4= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(ifleet_rating) as ifleet_rating_5 FROM tbl_feedback_rating WHERE ifleet_rating<=5 AND ifleet_rating>4 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$ifleet_rating_5= Yii::app()->db->createCommand($sql)->queryRow();
            
		
		$sql="SELECT count(driver_rating) as driver_rating_0 FROM tbl_feedback_rating WHERE driver_rating=0 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$driver_rating_0= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(driver_rating) as driver_rating_1 FROM tbl_feedback_rating WHERE driver_rating<=1 AND driver_rating>0 AND  date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$driver_rating_1= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(driver_rating) as driver_rating_2 FROM tbl_feedback_rating WHERE driver_rating<=2 AND driver_rating>1 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$driver_rating_2= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(driver_rating) as driver_rating_3 FROM tbl_feedback_rating WHERE driver_rating<=3 AND driver_rating>2 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$driver_rating_3= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(driver_rating) as driver_rating_4 FROM tbl_feedback_rating WHERE driver_rating<=4 AND driver_rating>3 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$driver_rating_4= Yii::app()->db->createCommand($sql)->queryRow();

		$sql="SELECT count(driver_rating) as driver_rating_5 FROM tbl_feedback_rating WHERE driver_rating<=5 AND driver_rating>4 AND date(created_time)>='$start_date' AND date(created_time)<='$to_date'";
		$driver_rating_5= Yii::app()->db->createCommand($sql)->queryRow();

		//start excel report
		Yii::import('application.extensions.PHPExcel1.Classes.PHPExcel');

		$objPHPExcel = new PHPExcel();
		$objWorksheet = $objPHPExcel->getActiveSheet();
		$objWorksheet->fromArray(
			array(
				array('',	      'iFleet',	        'Driver'),
				array('5 ~ 4',   $ifleet_rating_5['ifleet_rating_5'],   $driver_rating_5['driver_rating_5'],		),
				array('4 ~ 3',   $ifleet_rating_4['ifleet_rating_4'],   $driver_rating_4['driver_rating_4'],		),
				array('3 ~ 2',   $ifleet_rating_3['ifleet_rating_3'],   $driver_rating_3['driver_rating_3'],		),
				array('2 ~ 1',   $ifleet_rating_2['ifleet_rating_2'],   $driver_rating_2['driver_rating_2'],		),
				array('1 ~ 0',   $ifleet_rating_1['ifleet_rating_1'],   $driver_rating_1['driver_rating_1'],		),
				array('0 ~ 0',   $ifleet_rating_0['ifleet_rating_0'],   $driver_rating_0['driver_rating_0'],		),
			)
		);


		//	Set the Labels for each data series we want to plot
		//		Datatype
		//		Cell reference for data
		//		Format Code
		//		Number of datapoints in series
		//		Data values
		//		Data Marker
		$dataSeriesLabels1 = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$B$1', NULL, 1),	//	2011
		);
		//	Set the X-Axis Labels
		//		Datatype
		//		Cell reference for data
		//		Format Code
		//		Number of datapoints in series
		//		Data values
		//		Data Marker
		$xAxisTickValues1 = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$7', NULL, 6),	//	Q1 to Q4
		);
		//	Set the Data values for each data series we want to plot
		//		Datatype
		//		Cell reference for data
		//		Format Code
		//		Number of datapoints in series
		//		Data values
		//		Data Marker
		$dataSeriesValues1 = array(
			new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$B$2:$B$7', NULL, 6),
		);

		//	Build the dataseries
		$series1 = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
			NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
			range(0, count($dataSeriesValues1)-1),					// plotOrder
			$dataSeriesLabels1,										// plotLabel
			$xAxisTickValues1,										// plotCategory
			$dataSeriesValues1										// plotValues
		);

		//	Set up a layout object for the Pie chart
		$layout1 = new PHPExcel_Chart_Layout();
		$layout1->setShowVal(TRUE);
		$layout1->setShowPercent(TRUE);

		//	Set the series in the plot area
		$plotArea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
		//	Set the chart legend
		$legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

		$title1 = new PHPExcel_Chart_Title('iFleet Rating Pie Chart');


		//	Create the chart
		$chart1 = new PHPExcel_Chart(
			'chart1',		// name
			$title1,		// title
			$legend1,		// legend
			$plotArea1,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
		);

		//	Set the position where the chart should appear in the worksheet
		$chart1->setTopLeftPosition('A7');
		$chart1->setBottomRightPosition('H20');

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart1);


		
		//	Set the Labels for each data series we want to plot
		//		Datatype
		//		Cell reference for data
		//		Format Code
		//		Number of datapoints in series
		//		Data values
		//		Data Marker
		$dataSeriesLabels2 = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$CS$1', NULL, 1),	//	2011
		);
		//	Set the X-Axis Labels
		//		Datatype
		//		Cell reference for data
		//		Format Code
		//		Number of datapoints in series
		//		Data values
		//		Data Marker
		$xAxisTickValues2 = array(
			new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$7', NULL, 6),	//	Q1 to Q4
		);
		//	Set the Data values for each data series we want to plot
		//		Datatype
		//		Cell reference for data
		//		Format Code
		//		Number of datapoints in series
		//		Data values
		//		Data Marker
		$dataSeriesValues2 = array(
			new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$2:$C$7', NULL, 6),
		);

		//	Build the dataseries
		$series2 = new PHPExcel_Chart_DataSeries(
			PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
			NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
			range(0, count($dataSeriesValues2)-1),					// plotOrder
			$dataSeriesLabels2,										// plotLabel
			$xAxisTickValues2,										// plotCategory
			$dataSeriesValues2										// plotValues
		);

		//	Set up a layout object for the Pie chart
		$layout2 = new PHPExcel_Chart_Layout();
		$layout2->setShowVal(TRUE);
		$layout2->setShowPercent(TRUE);

		//	Set the series in the plot area
		$plotArea2 = new PHPExcel_Chart_PlotArea($layout2, array($series2));
		//	Set the chart legend
		$legend2 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

		$title2 = new PHPExcel_Chart_Title('Driver Rating Pie Chart');


		//	Create the chart
		$chart2 = new PHPExcel_Chart(
			'chart2',		// name
			$title2,		// title
			$legend2,		// legend
			$plotArea2,		// plotArea
			true,			// plotVisibleOnly
			0,				// displayBlanksAs
			NULL,			// xAxisLabel
			NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
		);

		//	Set the position where the chart should appear in the worksheet
		$chart2->setTopLeftPosition('I7');
		$chart2->setBottomRightPosition('P20');

		//	Add the chart to the worksheet
		$objWorksheet->addChart($chart2);

	


		// Save Excel 2007 file
		//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->setIncludeCharts(TRUE);

		$objWriter->save(str_replace('FeedbackRatingController.php','rating.xlsx',__FILE__));

		//echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;


		// Echo memory peak usage
		//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

		// Echo done
		//echo date('H:i:s') , " Done writing file" , EOL;
		//echo 'File has been created in ' , getcwd() , EOL;



		// end excel report



		echo json_encode(array(
							  'ifleet0'=>$ifleet_rating_0['ifleet_rating_0'],
							  'ifleet1'=>$ifleet_rating_1['ifleet_rating_1'],
							  'ifleet2'=>$ifleet_rating_2['ifleet_rating_2'],
							  'ifleet3'=>$ifleet_rating_3['ifleet_rating_3'],
							  'ifleet4'=>$ifleet_rating_4['ifleet_rating_4'],
							  'ifleet5'=>$ifleet_rating_5['ifleet_rating_5'],
							  'driver0'=>$driver_rating_0['driver_rating_0'],
							  'driver1'=>$driver_rating_1['driver_rating_1'],
							  'driver2'=>$driver_rating_2['driver_rating_2'],
							  'driver3'=>$driver_rating_3['driver_rating_3'],
							  'driver4'=>$driver_rating_4['driver_rating_4'],
							  'driver5'=>$driver_rating_5['driver_rating_5']
						));
	}

	public function actionExcelPieChart(){
			Yii::import('application.extensions.PHPExcel1.Classes.PHPExcel');

			$objPHPExcel = new PHPExcel();
$objWorksheet = $objPHPExcel->getActiveSheet();
$objWorksheet->fromArray(
	array(
		array('',	2010,	2011,	2012),
		array('Q1',   12,   15,		21),
		array('Q2',   56,   73,		86),
		array('Q3',   52,   61,		69),
		array('Q4',   30,   32,		0),
	)
);


//	Set the Labels for each data series we want to plot
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$dataSeriesLabels1 = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1),	//	2011
);
//	Set the X-Axis Labels
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$xAxisTickValues1 = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$5', NULL, 4),	//	Q1 to Q4
);
//	Set the Data values for each data series we want to plot
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$dataSeriesValues1 = array(
	new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$2:$C$5', NULL, 4),
);

//	Build the dataseries
$series1 = new PHPExcel_Chart_DataSeries(
	PHPExcel_Chart_DataSeries::TYPE_PIECHART,				// plotType
	NULL,			                                        // plotGrouping (Pie charts don't have any grouping)
	range(0, count($dataSeriesValues1)-1),					// plotOrder
	$dataSeriesLabels1,										// plotLabel
	$xAxisTickValues1,										// plotCategory
	$dataSeriesValues1										// plotValues
);

//	Set up a layout object for the Pie chart
$layout1 = new PHPExcel_Chart_Layout();
$layout1->setShowVal(TRUE);
$layout1->setShowPercent(TRUE);

//	Set the series in the plot area
$plotArea1 = new PHPExcel_Chart_PlotArea($layout1, array($series1));
//	Set the chart legend
$legend1 = new PHPExcel_Chart_Legend(PHPExcel_Chart_Legend::POSITION_RIGHT, NULL, false);

$title1 = new PHPExcel_Chart_Title('Test Pie Chart');


//	Create the chart
$chart1 = new PHPExcel_Chart(
	'chart1',		// name
	$title1,		// title
	$legend1,		// legend
	$plotArea1,		// plotArea
	true,			// plotVisibleOnly
	0,				// displayBlanksAs
	NULL,			// xAxisLabel
	NULL			// yAxisLabel		- Pie charts don't have a Y-Axis
);

//	Set the position where the chart should appear in the worksheet
$chart1->setTopLeftPosition('A7');
$chart1->setBottomRightPosition('H20');

//	Add the chart to the worksheet
$objWorksheet->addChart($chart1);


//	Set the Labels for each data series we want to plot
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$dataSeriesLabels2 = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$C$1', NULL, 1),	//	2011
);
//	Set the X-Axis Labels
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$xAxisTickValues2 = array(
	new PHPExcel_Chart_DataSeriesValues('String', 'Worksheet!$A$2:$A$5', NULL, 4),	//	Q1 to Q4
);
//	Set the Data values for each data series we want to plot
//		Datatype
//		Cell reference for data
//		Format Code
//		Number of datapoints in series
//		Data values
//		Data Marker
$dataSeriesValues2 = array(
	new PHPExcel_Chart_DataSeriesValues('Number', 'Worksheet!$C$2:$C$5', NULL, 4),
);

//	Build the dataseries
$series2 = new PHPExcel_Chart_DataSeries(
	PHPExcel_Chart_DataSeries::TYPE_DONUTCHART,		// plotType
	NULL,			                                // plotGrouping (Donut charts don't have any grouping)
	range(0, count($dataSeriesValues2)-1),			// plotOrder
	$dataSeriesLabels2,								// plotLabel
	$xAxisTickValues2,								// plotCategory
	$dataSeriesValues2								// plotValues
);

//	Set up a layout object for the Pie chart
$layout2 = new PHPExcel_Chart_Layout();
$layout2->setShowVal(TRUE);
$layout2->setShowCatName(TRUE);

//	Set the series in the plot area
$plotArea2 = new PHPExcel_Chart_PlotArea($layout2, array($series2));

$title2 = new PHPExcel_Chart_Title('Test Donut Chart');


//	Create the chart
$chart2 = new PHPExcel_Chart(
	'chart2',		// name
	$title2,		// title
	NULL,			// legend
	$plotArea2,		// plotArea
	true,			// plotVisibleOnly
	0,				// displayBlanksAs
	NULL,			// xAxisLabel
	NULL			// yAxisLabel		- Like Pie charts, Donut charts don't have a Y-Axis
);

//	Set the position where the chart should appear in the worksheet
$chart2->setTopLeftPosition('I7');
$chart2->setBottomRightPosition('P20');

//	Add the chart to the worksheet
$objWorksheet->addChart($chart2);


// Save Excel 2007 file
//echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->setIncludeCharts(TRUE);

$objWriter->save(str_replace('\protected\controllers\FeedbackRatingController.php','/Excel/rating.xlsx',__FILE__));

//echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;


// Echo memory peak usage
//echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
//echo date('H:i:s') , " Done writing file" , EOL;
//echo 'File has been created in ' , getcwd() , EOL;

	}
	
	
}
