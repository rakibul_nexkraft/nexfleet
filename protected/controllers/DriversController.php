<?php

class DriversController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    public $active_status = array(
        'Yes' =>'Yes',
        'No'=>'No'
    );

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','getName','driverRating','excel','training','excelTraining'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Drivers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Drivers']))
		{
			$model->attributes=$_POST['Drivers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
            'active_status'=>$this->active_status
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Drivers']))
		{
			$model->attributes=$_POST['Drivers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
            'active_status'=>$this->active_status,
		));
	}
	public function actionDriverRating(){

	$model=new Drivers('search1');
		$model->unsetAttributes();  // clear any default values
		//if(isset($_GET['FeedbackRating']))
			//$model->attributes=$_GET['FeedbackRating'];

		$this->render('driverRating',array(
			'model'=>$model,
		));
	
}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        throw new CHttpException(400,'Delete operation is stopped.');
        return;

        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*	$dataProvider=new CActiveDataProvider('Drivers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));	*/
		
		$model=new Drivers('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Drivers']))
			$model->attributes=$_GET['Drivers'];

		$this->render('index',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
		));
	}

	public function actionTraining()
	{
		/*	$dataProvider=new CActiveDataProvider('Drivers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));	*/
		
		$model=new Drivers('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Drivers']))
			$model->attributes=$_GET['Drivers'];

		$this->render('training',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
		));
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Drivers('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Drivers']))
			$model->attributes=$_GET['Drivers'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Drivers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function actionGetName() {
	 		
	  if (!empty($_GET['Vehicles']['driver_pin']) || !empty($_GET['Cardistributions']['driver_pin'])) {
	  	
		if(!empty($_GET['Vehicles']['driver_pin']))
			$qterm = $_GET['Vehicles']['driver_pin'];
		elseif(($_GET['Cardistributions']['driver_pin']))
			$qterm = $_GET['Cardistributions']['driver_pin'];
	 	$sql = "SELECT name	 FROM tbl_drivers  WHERE pin =  '$qterm'";
	
		
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();
		
	  echo CJSON::encode($result[0]); exit;
		
	  } else {
		return false;
	  }
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='drivers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	public function actionExcel()
	{
		$model = new Drivers ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Drivers Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Drivers_Information',
            //'stream'=>false,
           'columns'=>array(
				'id',
				array(
		            'name' => 'name',
		            'type'=>'raw',
		            'value' => 'CHtml::link(CHtml::encode($data->name),array("view","id"=>$data->id))',
		        ),
				'pin',
				'phone',
				'dvr_license_no',
				'dvr_license_type',
				'issued_from',
				'issued_date',
				'valid_to',
				'driver_rating',
		        'blood_group',
				'active',
			),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
 public function actionExcelTraining(){
 	$model = new Drivers ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Drivers Training Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Drivers_training_Information',
            //'stream'=>false,
           'columns'=>array(
		'id',
		array(
            'name' => 'name',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->name),array("view","id"=>$data->id))',
        ),
		'pin',
		'phone',
		array(
            'name' => 'defence_traning',
            'type'=>'raw',
            'value' => '($data->defence_traning==1)?"Yes":"NO"',
        ),
         'defence_traning_date',
		array(
            'name' => 'gen_awarness_traning',
            'type'=>'raw',
            'value' => '($data->gen_awarness_traning==1)?"Yes":"NO"',
        ),      
        'gen_awarness_traning_date',
        array(
            'name' => 'google_map_training',
            'type'=>'raw',
            'value' => '($data->google_map_training==1)?"Yes":"NO"',
        ),      
        'google_map_training_date',
	), ));
        Yii::app()->end();
        $this->endWidget();

	}
}
