<?php
date_default_timezone_set("Asia/Dhaka");
class DigitalLogBookHistoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','excel'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete','meterAdjust'),
				'users'=>array('@'),
			),
			//array('allow', // allow admin user to perform 'admin' and 'delete' actions
			//actions'=>array('admin','delete'),
				//'users'=>array('admin'),
			//),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DigitalLogBookHistory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DigitalLogBookHistory']))
		{
			$model->attributes=$_POST['DigitalLogBookHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DigitalLogBookHistory']))
		{
			$model->attributes=$_POST['DigitalLogBookHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DigitalLogBookHistory',array(
			'sort'=>array('defaultOrder'=>'id DESC'),
			
		));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DigitalLogBookHistory('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DigitalLogBookHistory']))
			$model->attributes=$_GET['DigitalLogBookHistory'];

		$this->render('admin',array(
			'model'=>$model,
		));
		 /*public MessageModel sendMessage( MessageModel smsModel ) {
       try {
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            String url = "http://27.147.137.181/M2BWS_SMS.asmx";
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration( "xsi", "http://www.w3.org/2001/XMLSchema-instance" );
            envelope.addNamespaceDeclaration( "xsd", "http://www.w3.org/2001/XMLSchema" );
            SOAPBody soapBody = envelope.getBody();
            SOAPElement SmsTextFs = soapBody.addChildElement( "SMS_TXTFS", "", "http://www.bdmitech.com/m2b" );
            SmsTextFs.addChildElement( "Sender" ).addTextNode( "nexParc" );
            SmsTextFs.addChildElement( "Recipient_Mobile_No" ).addTextNode( smsModel.getToNumber() );
            SmsTextFs.addChildElement( "Message" ).addTextNode( smsModel.getMessage() );
            SmsTextFs.addChildElement( "IS_Sender_Masked" ).addTextNode( "Y" );
            SmsTextFs.addChildElement( "IS_SMS_Single" ).addTextNode( "Y" );
            SmsTextFs.addChildElement( "Account_ID" ).addTextNode( "16081800000001" );
            SmsTextFs.addChildElement( "User_Name" ).addTextNode( "nexkode" );
            SmsTextFs.addChildElement( "Password" ).addTextNode( "nexkode123" );
            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader( "SOAPAction", "http://www.bdmitech.com/m2b/SMS_TXTFS" );
            soapMessage.saveChanges();
            soapMessage.writeTo( System.out );
            SOAPMessage soapResponseTextFs = soapConnection.call( soapMessage, url );
            printSOAPResponse( soapResponseTextFs );
            soapConnection.close();
        }*/
      /*  $client = new SoapClient(null, array('location' =>"http://www.bdmitech.com/m2b",
                                     'uri'      => "http://www.w3.org/2001/XMLSchema-instance",
                                     
                                     'login'    => "nexkode",
                                     'password' => "nexkode123",


                                 ));
        $result = $client->SomeFunction();*/
       //$client->__call('sendEmail', "http://27.147.137.181/M2BWS_SMS.asmx");
        //var_dump($this->route);
        //$client = new SoapClient("D:/project/ifleet/protected/controllers/mass.wsdl");
        //var_dump($client);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DigitalLogBookHistory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='digital-log-book-history-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionExcel()
    {
        $model = new DigitalLogBookHistory('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'Digital Log BookHistory',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'DigitalLogBookHistory',
            //'stream'=>false,
            'columns'=>array(
		'id',
		'login_id',
		'trip_id',
		'requisition_id',
		'login_end',
		'trip_id_end',
		'vehicle_reg_no',
		'duty_day',
		'driver_pin',
		'user_pin',
		'user_name',
		'department',
		
		'dutytype_id',
		'driver_start_time_vts',
		'driver_start_time_mobile',
		'driver_end_time_vts',
		'driver_end_time_mobile',
		'meter_number',
		'vehicle_mode',
		'location_vts',
		'location_mobile',
		'fuel_recipt_no',
		'fuel_unit_price',
		'fuel_quantity',
		'fuel_total_bill',
		'night_halt_bill',
		'morning_ta',
		'lunch_ta',
		'night_ta',
		'running_per_km_rate',
		'running_km',
		'running_km_bill',
		'over_time_hour_rate',
		'over_time',
		'over_time_bill',
		'total_bill',
		'objection',
		'repair_start_time',
		'repair_end_time',
		'created_time_vts',
		'created_time_mobile',
		'created_by',
		//'updated_time_vts',
		//'updated_time_mobile',
		//'updated_by',
		
		
	),
        ));
        Yii::app()->end();
        $this->endWidget();
    }
    public function actionMeterAdjust() {
    	$vehicle_info = Yii::app()->db->createCommand("SELECT * FROM tbl_vehicles WHERE reg_no='".$_POST["vehicle_no"]."'")->queryRow();    
    	if(!empty($vehicle_info) && !empty($vehicle_info['asset_id'])) { 
    		$asset_id = $vehicle_info['asset_id']; 			
    	}	
    	else {
    		echo "Vehicle or Asset Id Not Found";
    		return;
    	}
    	$maxMeter = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE vehicle_reg_no ='".$_POST['vehicle_no']."' AND date(driver_start_time_mobile)<'".$_POST['adjust_date']."' ORDER BY created_time_mobile DESC")->queryRow();
    	if(empty($maxMeter)) { 
    		echo "Max Meter Empty!";
    		return;
    	} 
    	$maxMeter = $maxMeter['meter_number'];  
    	$miss_KM = $maxMeter['missing_KM']; 	
    	$data = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE vehicle_reg_no='".$_POST['vehicle_no']."' AND  date(driver_start_time_mobile)>='".$_POST['adjust_date']."' ORDER BY created_time_mobile ASC")->queryAll();         	
    	foreach ($data as $key => $value) {
    		$st = explode(' ',date("d/m/Y H:i",strtotime($value['driver_start_time_mobile'])));
        	$et = explode(' ',date("d/m/Y H:i",strtotime($value['created_time_mobile'])));         	  		
    	 	$km = $this->pathFinderKM($asset_id,$st,$et);
    	 	$up = Yii::app()->db->createCommand("UPDATE tbl_digital_log_book_history SET new_meter_number='".($km + $maxMeter + $miss_KM)."', new_km='".$km."' WHERE id='".$value['id']."'")->execute(); 
    	 	if($value['trip_id_end']=='1' || $value['day_end']=="Yes") {
    	 		 $maxMeter = $km + $maxMeter;
    	 	}    	 	
    	 	$miss_KM = $value['missing_KM'];    	 	
    	 } 
    	 echo "Adjust Complete !";    
    	return;
    }   
    private function pathFinderKM($asset_id,$st,$et) {     
      try {
        $KM = file_get_contents('https://api.finder-lbs.com/api/54f2cb4b421aa9493557fc09/getdistancebyasset?email=bracblc123@gmail.com&asset_id='.$asset_id.'&tf='.$st['0'].'%20'.$st['1'].'&tt='.$et['0'].'%20'.$et['1']);
        $KM = json_decode($KM);

        return $km = $KM->success ? $KM->output[0]->distance_in_kms : 0;
      } catch(Exception $e) {
      } 
      return -1;     
    }
}
