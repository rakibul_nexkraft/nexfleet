<?php

class AuctionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('download'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','admin','update','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Auctions;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Auctions']))
		{
            
            $getname = Yii::app()->user->username;
            $_POST['Auctions']['created_by'] = $getname;
			$model->attributes=$_POST['Auctions'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $status = $model->active;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $old_file = $model->upload_scan;
                $file_dir = Yii::getPathOfAlias('webroot')."/images/scandoc/$model->vehicle_reg_no/";
                
		if(isset($_POST['Auctions']))
		{
                    $model->attributes=$_POST['Auctions'];
                    $model->updated_by = Yii::app()->user->username;
                    $model->updated_time = new CDbExpression('NOW()');
                    if(isset($_POST['Auctions']['upload_scan']))
                    {
                        if($old_file) unlink($file_dir.$old_file);
                        $file = CUploadedFile::getInstance($model,'upload_scan');
                        $model->upload_scan = $file;
                    }
                    
                    if($model->save())
                    {
                        if(!empty($file))
                        {
                            if(!(is_dir($file_dir)))
                            {
                                mkdir (Yii::getPathOfAlias('webroot')."/images/scandoc/$model->vehicle_reg_no", 0777, true);
                                $file_dir = $file_dir;
                            }
                            $file->saveAs($file_dir.$file->name);
                        }
                        if($model->active == 2)
                        {
                            $mdl_vehicle = Vehicles::model()->find('reg_no=:vehicle_reg_no', array(':vehicle_reg_no'=>$model->vehicle_reg_no));
                            $mdl_vehicle->active = 3;
                            $mdl_vehicle->update();
                            
                            $mdl_vehicle_log = new VehiclesLog;
                            $mdl_vehicle_log->attributes = $mdl_vehicle->attributes;
                            $mdl_vehicle_log->vehicle_reg_no = $model->vehicle_reg_no;
                            $mdl_vehicle_log->updated_by = Yii::app()->user->username;
                            $mdl_vehicle_log->updated_time = new CDbExpression('NOW()');
                            $mdl_vehicle_log->insert();
                        }
                        $this->redirect(array('view','id'=>$model->id));
                    }
		}
                
                $this->render('update',array(
                    'model'=>$model,
                    'status'=>$status,
                ));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('Auctions');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
        //));
        
       $model=new Auctions('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Auctions']))
            $model->attributes=$_GET['Auctions'];

        $this->render('index',array(
            'model'=>$model,
        )); 
        
        
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Auctions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Auctions']))
			$model->attributes=$_GET['Auctions'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Auctions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='auctions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionDownload($ufile,$vehicle)
        {
            $file = Yii::getPathOfAlias('webroot').'/images/scandoc/'.$vehicle.'/'.$ufile;
            if(file_exists($file))
            {
                Yii::app()->request->sendFile(
                        $ufile,
                        file_get_contents($file),
                        $ufile
                );
            }
        }
}
