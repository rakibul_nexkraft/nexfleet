<?php
date_default_timezone_set("Asia/Dhaka");
class DigitalLogBookController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','ditalLogBookRemove','getRequisitionData','callHrUser','getName','callHrUserForHelper','others','allBills','allBill','remarks','driverObjection','objectionSubmit'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','logBook','otherCost','remove'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DigitalLogBook;
		$model->created_by=Yii::app()->user->username;
		$model->created_time=date("Y-m-d H:i:s");

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DigitalLogBook']))
		{
			$model->attributes=$_POST['DigitalLogBook'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		/*if(isset($_POST['DigitalLogBook']))
		{
			$model->attributes=$_POST['DigitalLogBook'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));*/
	$rate = 0;
        $ot_amount = 0;
        $new_used_mileage = 0;



        $model = $this->loadModel($id);
        $model->updated_by = Yii::app()->user->username;
        $model->updated_time=date("Y-m-d H:i:s");

        if ($model->morning_ta == '80')
            $model->morning_ta = 1;
        if ($model->lunch_ta == '80')
            $model->lunch_ta = 1;
        if ($model->night_ta == '80')
            $model->night_ta = 1;

        $model->bill_amount = 0;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);


        if (isset($_POST['DigitalLogBook'])) {

            $vehicle_type = $_POST['DigitalLogBook']['vehicletype_id'];

            $sql = "SELECT * FROM tbl_vehicletypes WHERE id =  '$vehicle_type'";

            $command = Yii::app()->db->createCommand($sql);
            $vehicle_types = $command->queryAll();
            $vehicle_rate_sup = $vehicle_types[0]['rate_per_km'];

            $vehicle_type_rate = '';
            if ($_POST['DigitalLogBook']['dutytype_id'] == '1')
                $vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
            else if ($_POST['DigitalLogBook']['dutytype_id'] == '2')
                $vehicle_type_rate = $vehicle_types[0]['rate_per_km_personal'];
            else if ($_POST['DigitalLogBook']['dutytype_id'] == '3') {
                $vehicle_type_rate = $vehicle_types[0]['rate_per_km_external'];
                $vehicle_hour_rate = $vehicle_types[0]['rph_visitor'];
            } else if ($_POST['DigitalLogBook']['dutytype_id'] == '4' || $_POST['DigitalLogBook']['dutytype_id'] == '5')
                $vehicle_type_rate = $vehicle_types[0]['rate_per_km'];

            $start_meter = $_POST['DigitalLogBook']['start_meter'];
            $end_meter = $_POST['DigitalLogBook']['end_meter'];

            $total_run = $end_meter - $start_meter;
            /* 	echo "<pre>";
              echo "$end_meter";
              echo "<br />";
              echo "$start_meter";
              echo "<br />";
              echo "$total_run";
              echo "</pre>";
              die; */

            $start_time = $_POST['DigitalLogBook']['start_time'];
            $end_time = $_POST['DigitalLogBook']['end_time'];

            //$total_time_temp = $end_time - $start_time;

            $start_time_mod = explode(".", $start_time);

            $end_time_mod = explode(".", $end_time);

            if (strtotime($_POST['DigitalLogBook']['end_date']) > strtotime($_POST['DigitalLogBook']['start_date'])) {
                $end_time = 24 + $_POST['DigitalLogBook']['end_time'];
            }

            $end_time_temp = $end_time;
            $flag = 0;

            if ($end_time_mod[1] < $start_time_mod[1]) {
                $end_time = ($end_time - .40);
                $flag = 1;

                //echo (.40+".".$end_time_mod[1]);
                //echo (.60+$end_time_mod[1]/100);
                //echo $end_time = $end_time - mod[1]/100);
            }
            //else if($_POST['DigitalLogBook']['user_level']>=17 && $_POST['DigitalLogBook']['dutyday']=='Holiday')
            //	$end_time = ($end_time - .40);

            $dri_work_start_time = "07.45";
            $ot_time_start  = "07.45";

            /*Code for Dynamic Time Settings*/
            $end_date = $_POST['DigitalLogBook']['end_date'];

            $sql = "SELECT * FROM tbl_timesettings WHERE  start_date<= '$end_date'  AND end_date>= '$end_date'";
            $command = Yii::app()->db->createCommand($sql);
            $timesettings = $command->queryAll();

            $timesettings_start_date = $timesettings[0]['start_date'];

            $driver_office_start_time =  $timesettings[0]['driver_office_start_time'];
            $driver_office_start_time_split = explode(".",$driver_office_start_time);

            $driver_office_end_time =  $timesettings[0]['driver_office_end_time'];
            $driver_office_end_time_split = explode(".",$driver_office_end_time);
            /* ---------------------------------- */

            if($driver_office_start_time) {

                if ($driver_office_start_time_split[1] < $start_time_mod[1]) {
                    $ot_time_start = ($driver_office_start_time - .40);
                } else
                    $ot_time_start = $driver_office_start_time;
            }
else
{
 if (45 < $start_time_mod[1]) {
                $ot_time_start = (7.45 - .40);
            } else
                $ot_time_start = 7.45;


}

            if($driver_office_end_time)
            {
                if ($end_time_mod[1] < $driver_office_end_time_split[1]) {
                    $ot_time_end = ($end_time_temp - .40);
                } else
                    $ot_time_end = $end_time_temp;
            }

            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-01-19' AND $_POST['DigitalLogBook']['end_date'] <= '2015-01-28') {

                if ($end_time_mod[1] < 20) {
                    $ot_time_end = ($end_time_temp - .40);
                } else
                    $ot_time_end = $end_time_temp;
            }
            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-03-19' AND $_POST['DigitalLogBook']['end_date'] <= '2015-03-19') {

                if ($end_time_mod[1] < 50) {
                    $ot_time_end = ($end_time_temp - .40);
                } else
                    $ot_time_end = $end_time_temp;
            }
            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-02-15' AND $_POST['DigitalLogBook']['end_date'] <= '2015-03-24') {

                if ($end_time_mod[1] < 20) {
                    $ot_time_end = ($end_time_temp - .40);
                } else
                    $ot_time_end = $end_time_temp;
            }
            else {


                if ($end_time_mod[1] < 50) {
                    $ot_time_end = ($end_time_temp - .40);
                } else
                    $ot_time_end = $end_time_temp;
            }

            //echo $ot_time_start-$start_time;
            //echo $ot_time_end;
			
			 if($driver_office_end_time) {
                if($_POST['DigitalLogBook']['end_date'] == $timesettings_start_date &&  $_POST['DigitalLogBook']['end_time']<=$driver_office_start_time)
                    $dri_work_end_time = "17.50";
                else
				{
                    $dri_work_end_time = $driver_office_end_time;
					$dri_work_start_time = $driver_office_start_time;
				}
			 }
            else if ($_POST['DigitalLogBook']['end_date'] >= '2014-06-30' AND $_POST['DigitalLogBook']['end_date'] <= '2014-07-29')
                $dri_work_end_time = "15.50";


            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-01-19' AND $_POST['DigitalLogBook']['end_date'] <= '2015-01-28')
                $dri_work_end_time = "17.20";

            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-03-19' AND $_POST['DigitalLogBook']['end_date'] <= '2015-03-19')
                $dri_work_end_time = "13.50";

            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-02-15' AND $_POST['DigitalLogBook']['end_date'] <= '2015-03-24')
                $dri_work_end_time = "17.20";

            else if ($_POST['DigitalLogBook']['end_date'] >= '2015-06-21' AND $_POST['DigitalLogBook']['end_date'] <= '2015-07-18')
                $dri_work_end_time = "15.50";
                else if ($_POST['DigitalLogBook']['end_date'] >= '2016-06-07' AND $_POST['DigitalLogBook']['end_date'] <= '2016-07-06')
                {
                
                
                	if($_POST['DigitalLogBook']['end_date'] == '2016-06-07' &&  $_POST['DigitalLogBook']['end_time']<=$dri_work_start_time)
                	 $dri_work_end_time = "17.50";
                	else
                	$dri_work_end_time = "15.50";
                
                
              }
                
            else
                $dri_work_end_time = "17.50";
                



            if ($_POST['DigitalLogBook']['dutyday'] == 'Holiday')
                $ot_time = $end_time - $start_time;
            else if ($start_time < 7.45 && $end_time > $dri_work_end_time) {
                //$ot_time = number_format($ot_time,2);
                $time = number_format($ot_time_start - $start_time, 2);
                $time2 = number_format($ot_time_end - $dri_work_end_time, 2);

                $secs = strtotime($time2) - strtotime("00.00");
                $ot_time = date("H.i", strtotime($time) + $secs);
            } else if ($start_time < 7.45 && $end_time < 7.45) {
                $ot_time = ($end_time - $start_time);
            } else if ($start_time > $dri_work_end_time) {
                $ot_time = $end_time - $start_time;
            } else if ($start_time < 7.45) {
                $ot_time = ($ot_time_start - $start_time);
            } else if ($end_time > $dri_work_end_time)
                $ot_time = ($ot_time_end - $dri_work_end_time);
            else
                $ot_time = 0.00;

            $ot_time_temp = $ot_time;


            $ot_time = number_format($ot_time, 2);


            $ot_time_mod = explode(".", $ot_time);



            $ot_time = $ot_time_mod[0];



            if (strlen($ot_time_mod[1]) == '1')
                $ot_time_min = $ot_time_mod[1] * 10;
            else
                $ot_time_min = $ot_time_mod[1];

            if ($start_time <= 22 && $_POST['DigitalLogBook']['user_level'] >= 17) {
                //$end_time = $end_time_temp;

                $total_time_sup = $end_time_temp - 22.00;
                $total_time_sup = number_format($total_time_sup, 2);
            } else
                $total_time_sup = $end_time - $start_time;

            $total_time = $end_time - $start_time;

            $total_time_temp = $total_time;


            $total_time_mod = explode(".", $total_time);
            $total_time_sup_mod = explode(".", $total_time_sup);

            $total_time = $total_time_mod[0];
            $total_time_sup = $total_time_sup_mod[0];



            if (strlen($total_time_mod[1]) == '1')
                $total_time_min = $total_time_mod[1] * 10;
            else
                $total_time_min = $total_time_mod[1];

            if (strlen($total_time_sup_mod[1]) == '1')
                $total_time_sup_min = $total_time_sup_mod[1] * 10;
            else
                $total_time_sup_min = $total_time_sup_mod[1];

            $end_time = $end_time_temp;


            $total_time = number_format($total_time, 2);
            $total_time_min = number_format($total_time_min, 2);
            $total_time_sup = number_format($total_time_sup, 2);
            $total_time_sup_min = number_format($total_time_sup_min, 2);



            $sql = "SELECT * FROM tbl_billrates";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            $sql = "SELECT * FROM tbl_dutytypes";
            $command = Yii::app()->db->createCommand($sql);
            $duty_types = $command->queryAll();

            $user_pin = $_POST['DigitalLogBook']['user_pin'];
            $sql = "SELECT sum(total_run) FROM tbl_digital_log_book WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2'";
            $command = Yii::app()->db->createCommand($sql);
            $total_run_info = $command->queryAll();

            if ($_POST['DigitalLogBook']['dutytype_id'] != 1) {
                $sql = "SELECT * FROM tbl_digital_log_book WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";

                $command = Yii::app()->db->createCommand($sql);
                $result_um = $command->queryAll();


                if (empty($result_um[1]['used_mileage'])) {
                    $used_mileage = 0;
                } else {
                    $used_mileage = $result_um[1]['used_mileage'];
                }

                if ($_POST['DigitalLogBook']['dutytype_id'] != $model->dutytype_id)
                    $used_mileage = $result_um[0]['used_mileage'];

                $new_used_mileage = $used_mileage + $total_run;

                /* echo "<pre>";
                  echo "$used_mileage";
                  echo "<br/>";
                  echo "$id";
                  echo "<br/>";
                  echo "$total_run";
                  echo "<br/>";
                  echo "$new_used_mileage";

                  echo "</pre>";
                  die; */


                $sql = "UPDATE tbl_digital_log_book SET used_mileage='$new_used_mileage' WHERE id='$id'";
                $command = Yii::app()->db->createCommand($sql);
                $execute1 = $command->execute();
            }
            /* 	echo "<pre>";
              print_r($total_run_info);
              echo "</pre>";
              die;

              echo $duty_types[2]['service_charge'];
              echo $total_run;
              echo $vehicle_type_rate;
              echo $total_time;
             */


            if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && ($_POST['DigitalLogBook']['user_level'] < 17 || empty($_POST['DigitalLogBook']['user_level'])) && ($_POST['DigitalLogBook']['dutytype_id'] == 1 || $_POST['DigitalLogBook']['dutytype_id'] == 2)) {
                $rate = $total_run * $vehicle_type_rate + $total_time * $result[2]['rate_amount'] + ($total_time_min * $result[2]['rate_amount']) / 60;
                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
            } else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && $_POST['DigitalLogBook']['user_level'] < 17 && ($_POST['DigitalLogBook']['dutytype_id'] == 1 || $_POST['DigitalLogBook']['dutytype_id'] == 2)) {
                $rate = $total_run * $vehicle_type_rate + $total_time * $result[3]['rate_amount'] + ($total_time_min * $result[3]['rate_amount']) / 60;
                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
            } else if ($_POST['DigitalLogBook']['night_halt'] == 1 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && $_POST['DigitalLogBook']['user_level'] < 17 && ($_POST['DigitalLogBook']['dutytype_id'] == 1 || $_POST['DigitalLogBook']['dutytype_id'] == 2)) {
                $rate = $total_run * $vehicle_type_rate + $result[0]['rate_amount'];
                //$ot_amount = 	$result[0]['rate_amount'];
            } else if ($_POST['DigitalLogBook']['night_halt'] == 1 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && $_POST['DigitalLogBook']['user_level'] < 17 && ($_POST['DigitalLogBook']['dutytype_id'] == 1 || $_POST['DigitalLogBook']['dutytype_id'] == 2)) {
                $rate = $total_run * $vehicle_type_rate + $result[1]['rate_amount'];
                //$ot_amount = $result[1]['rate_amount'];
                $ot_amount = 0.00;
            }

            // Supervisor Official Bill
            else if ($_POST['DigitalLogBook']['dutyday'] == 'Office Day' && $_POST['DigitalLogBook']['user_level'] >= 17 && ($_POST['DigitalLogBook']['dutytype_id'] == 1)) {
                if ($_POST['DigitalLogBook']['night_halt'] == 0) {
                    $rate = $total_run * $vehicle_type_rate + $total_time * $result[2]['rate_amount'] + ($total_time_min * $result[2]['rate_amount']) / 60;
                    $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                } else {
                    $rate = $total_run * $vehicle_type_rate;
                }
            } else if ($_POST['DigitalLogBook']['dutyday'] == 'Holiday' && $_POST['DigitalLogBook']['user_level'] >= 17 && ($_POST['DigitalLogBook']['dutytype_id'] == 1)) {
                if ($_POST['DigitalLogBook']['night_halt'] == 0) {
                    $rate = $total_run * $vehicle_type_rate + $total_time * $result[3]['rate_amount'] + ($total_time_min * $result[3]['rate_amount']) / 60;
                    $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                } else {
                    $rate = $total_run * $vehicle_type_rate;
                }
            }
            // END Supervisor Official Bill
            else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && ($_POST['DigitalLogBook']['dutytype_id'] == 4 || $_POST['DigitalLogBook']['dutytype_id'] == 5)) {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[3]['service_charge'])) + $total_time * $result[2]['rate_amount'] + ($total_time_min * $result[2]['rate_amount']) / 60;
                $rate += $rate * $duty_types[3]['service_charge'];

                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
            } else if ($_POST['DigitalLogBook']['night_halt'] == 1 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && ($_POST['DigitalLogBook']['dutytype_id'] == 4 || $_POST['DigitalLogBook']['dutytype_id'] == 5)) {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[3]['service_charge'])) + $result[0]['rate_amount'];
                $rate += $rate * $duty_types[3]['service_charge'];
                //$ot_amount = $result[0]['rate_amount'];
            } else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && ($_POST['DigitalLogBook']['dutytype_id'] == 4 || $_POST['DigitalLogBook']['dutytype_id'] == 5)) {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[3]['service_charge'])) + $total_time * $result[3]['rate_amount'] + ($total_time_min * $result[3]['rate_amount']) / 60;
                $rate += $rate * $duty_types[3]['service_charge'];
                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
            } else if ($_POST['DigitalLogBook']['night_halt'] == 1 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && ($_POST['DigitalLogBook']['dutytype_id'] == 4 || $_POST['DigitalLogBook']['dutytype_id'] == 5)) {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[3]['service_charge'])) + $result[1]['rate_amount'];
                $rate += $rate * $duty_types[3]['service_charge'];
                //$ot_amount = $result[0]['rate_amount'];
            } else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && $_POST['DigitalLogBook']['dutytype_id'] == 3) {
                $rate1 = ($total_run * ($vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[2]['service_charge'])) + $total_time * $vehicle_hour_rate + ($total_time_min * $vehicle_hour_rate) / 60);
                $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;

                if ($rate < $vehicle_types[0]['minimum_rate'])
                    $rate = $vehicle_types[0]['minimum_rate'];
            }


            else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && $_POST['DigitalLogBook']['dutytype_id'] == 3) {
                $rate1 = ($total_run * ($vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[2]['service_charge'])) + $total_time * $vehicle_hour_rate + ($total_time_min * $vehicle_hour_rate) / 60);
                $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;

                if ($rate < $vehicle_types[0]['minimum_rate'])
                    $rate = $vehicle_types[0]['minimum_rate'];
            }
            else if ($_POST['DigitalLogBook']['night_halt'] == 1 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && $_POST['DigitalLogBook']['dutytype_id'] == 3) {
                $rate1 = ($total_run * ($vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[2]['service_charge'])) + $result[0]['rate_amount']); //$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
                $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
                //$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			

                if ($rate < $vehicle_types[0]['minimum_rate'])
                    $rate = $vehicle_types[0]['minimum_rate'];
            }
            else if ($_POST['DigitalLogBook']['night_halt'] == 1 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && $_POST['DigitalLogBook']['dutytype_id'] == 3) {
                $rate1 = ($total_run * ($vehicle_type_rate + ceil($vehicle_type_rate * $duty_types[2]['service_charge'])) + $result[0]['rate_amount']); //$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
                $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;

                if ($rate < $vehicle_types[0]['minimum_rate'])
                    $rate = $vehicle_types[0]['minimum_rate'];
            }
            else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['user_level'] >= 17 && $_POST['DigitalLogBook']['user_level'] <= 19 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day') {
                $sql = "SELECT * FROM tbl_DigitalLogBook WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
                $command = Yii::app()->db->createCommand($sql);
                $used_mileage = $command->queryAll();
                $calculate_run = $used_mileage[0]['used_mileage'];


                if ($_POST['DigitalLogBook']['user_level'] == 17 && $calculate_run <= 3000.00) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;


                    if ($end_time > 22.00) {

                        // 	                	echo $total_time_sup;
                        //              	echo $total_time_sup_min;
                        //            	die;

                        $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                    } else {
                        $rate = 0.00;
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                    }
                } else if ($_POST['DigitalLogBook']['user_level'] == 17 && $calculate_run > 3000.00) {

                    $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
                    $test_run = $used_mileage[0]['used_mileage'] - $total_run;


                    if ($test_run < 3000.00) {

                        //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                        if ($end_time > 22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            } else {
                                $rate = $run1 * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            }
                        }
                    } else {
                        if ($end_time > 22.30) {

                            $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            } else {
                                $rate = $total_run * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            }
                        }
                    }
                    //  $new_run = 3000 - $run1;
                    /* if($total_run_info[0]['sum(total_run)'] > 3000) {
                      /*if($end_time>22.30) {
                      echo "<pre>";
                      echo $total_run_info[0]['sum(total_run)'] ;
                      echo "<br />";
                      echo  $total_run;
                      echo "<br />";
                      echo $run1;
                      echo "<br />";
                      echo ($new_run);
                      echo "</pre>";
                      die;
                      $rate = $run1 * $vehicle_type_rate + ($end_time-22)*$result[2]['rate_amount'] + $result[4]['rate_amount'];
                      }
                      else{
                      $rate = $total_run * $vehicle_type_rate  + ($end_time-22)*$result[2]['rate_amount'];
                      }
                      } */
                }

                if ($_POST['DigitalLogBook']['user_level'] == 18 && $calculate_run <= 4000.00) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;
                    if ($end_time > 22.30) {
                        //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                        $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                    } else {
                        if ($end_time <= 22.30 && $end_time >= 22.00) {
                            $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                        } else {
                            $rate = 0.00;
                        }
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                    }
                } else if ($_POST['DigitalLogBook']['user_level'] == 18 && $calculate_run > 4000.00) {
                    $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
                    $test_run = $used_mileage[0]['used_mileage'] - $total_run;

                    if ($test_run < 4000.00) {
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;

                        if ($end_time > 22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            } else {
                                $rate = $run1 * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            }
                        }
                    } else {
                        if ($end_time > 22.30) {

                            $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'];
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            } else {
                                $rate = $total_run * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            }
                        }
                    }
                }


                if ($_POST['DigitalLogBook']['user_level'] == 19 && $calculate_run <= 6000.00) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;
                    if ($end_time > 22.30) {
                        //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                        $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                    } else {
                        if ($end_time <= 22.30 && $end_time >= 22.00) {
                            //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            $rate = 0.00;
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        }
                    }
                } else if ($_POST['DigitalLogBook']['user_level'] == 19 && $calculate_run > 6000.00) {
                    $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
                    $test_run = $used_mileage[0]['used_mileage'] - $total_run;
                    if ($test_run < 6000.00) {
                        if ($end_time > 22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            } else {
                                $rate = $run1 * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            }
                        }
                    } else {
                        if ($end_time > 22.30) {

                            $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            } else {
                                $rate = $total_run * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                            }
                        }
                    }
                }
            } else if ($_POST['DigitalLogBook']['night_halt'] == 0 && $_POST['DigitalLogBook']['user_level'] >= 17 && $_POST['DigitalLogBook']['user_level'] <= 19 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday') {
                $sql = "SELECT * FROM tbl_DigitalLogBook WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
                $command = Yii::app()->db->createCommand($sql);
                $used_mileage = $command->queryAll();
                $calculate_run = $used_mileage[0]['used_mileage'];

                if ($_POST['DigitalLogBook']['user_level'] == 17 && $calculate_run <= 3000.00) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;

                    if ($end_time > 22.00) {
                        $rate = $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;

                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    } else {
                        $rate = 0.00;
                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    }
                } else if ($_POST['DigitalLogBook']['user_level'] == 17 && $calculate_run > 3000.00) {

                    $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
                    $test_run = $used_mileage[0]['used_mileage'] - $total_run;


                    if ($test_run < 3000.00) {

                        //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                        if ($end_time > 22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            } else {
                                $rate = $run1 * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            }
                        }
                    } else {
                        if ($end_time > 22.30) {

                            $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            } else {
                                $rate = $total_run * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            }
                        }
                    }
                }

                if ($_POST['DigitalLogBook']['user_level'] == 18 && $calculate_run <= 4000.00) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;

                    if ($end_time > 22.00) {
                        $rate = $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    } else {
                        $rate = 0.00;
                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    }
                } else if ($_POST['DigitalLogBook']['user_level'] == 18 && $calculate_run > 4000.00) {

                    $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
                    $test_run = $used_mileage[0]['used_mileage'] - $total_run;


                    if ($test_run < 4000.00) {
                        if ($end_time > 22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            } else {
                                $rate = $run1 * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            }
                        }
                    } else {
                        if ($end_time > 22.30) {

                            $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            } else {
                                $rate = $total_run * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            }
                        }
                    }
                }

                if ($_POST['DigitalLogBook']['user_level'] == 19 && $calculate_run <= 6000.00) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;

                    if ($end_time > 22.00) {
                        $rate = $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    } else {
                        $rate = 0.00;
                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    }
                } else if ($_POST['DigitalLogBook']['user_level'] == 19 && $calculate_run > 6000.00) {

                    $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
                    $test_run = $used_mileage[0]['used_mileage'] - $total_run;


                    if ($test_run < 6000.00) {
                        if ($end_time > 22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $run1 * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            } else {
                                $rate = $run1 * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            }
                        }
                    } else {
                        if ($end_time > 22.30) {

                            $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60; // + $result[4]['rate_amount'];
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        } else {
                            if ($end_time <= 22.30 && $end_time >= 22.00) {
                                $rate = $total_run * $vehicle_type_rate + $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            } else {
                                $rate = $total_run * $vehicle_type_rate;
                                $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                            }
                        }
                    }
                }
            } else if ($_POST['DigitalLogBook']['user_level'] >= 20 && $_POST['DigitalLogBook']['dutyday'] == 'Office Day' && $_POST['DigitalLogBook']['dutytype_id'] == 2) {

                if ($_POST['DigitalLogBook']['user_level'] >= 20) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;

                    if ($end_time > 22.00) {
                        if ($_POST['DigitalLogBook']['night_halt'] == '0') {
                            $rate = $total_time_sup * $result[2]['rate_amount'] + ($total_time_sup_min * $result[2]['rate_amount']) / 60;
                            $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                        }
                    } else {
                        $rate = 0.00;
                        $ot_amount = $ot_time * $result[2]['rate_amount'] + ($ot_time_min * $result[2]['rate_amount']) / 60;
                    }
                }
            } else if ($_POST['DigitalLogBook']['user_level'] >= 20 && $_POST['DigitalLogBook']['dutyday'] == 'Holiday' && $_POST['DigitalLogBook']['dutytype_id'] == 2) {

                if ($_POST['DigitalLogBook']['user_level'] >= 20) {
                    $sup_mile_amount = $total_run * $vehicle_rate_sup;
                    if ($end_time > 22.00) {
                        if ($_POST['DigitalLogBook']['night_halt'] == '0') {

                            $rate = $total_time_sup * $result[3]['rate_amount'] + ($total_time_sup_min * $result[3]['rate_amount']) / 60;
                            $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                        }
                    } else {
                        $rate = 0.00;
                        $ot_amount = $ot_time * $result[3]['rate_amount'] + ($ot_time_min * $result[3]['rate_amount']) / 60;
                    }
                }
            }


            if ($_POST['DigitalLogBook']['night_halt'] == '1' && $_POST['DigitalLogBook']['dutyday'] == 'Office Day')
                $_POST['DigitalLogBook']['nh_amount'] = $result[0]['rate_amount'];

            else if ($_POST['DigitalLogBook']['night_halt'] == '1' && $_POST['DigitalLogBook']['dutyday'] == 'Holiday')
                $_POST['DigitalLogBook']['nh_amount'] = $result[1]['rate_amount'];
            else
                $_POST['DigitalLogBook']['nh_amount'] = 0;


            if ($_POST['DigitalLogBook']['night_halt'] == '1' && $_POST['DigitalLogBook']['user_level'] >= 17)// && 	$_POST['DigitalLogBook']['dutytype_id']==2)
                $rate+=$_POST['DigitalLogBook']['nh_amount'];

            if ($_POST['DigitalLogBook']['dutyday'] == 'Office Day')
                $_POST['DigitalLogBook']['rph'] = $result[2]['rate_amount'];
            if ($_POST['DigitalLogBook']['dutyday'] == 'Holiday')
                $_POST['DigitalLogBook']['rph'] = $result[3]['rate_amount'];



            if ($_POST['DigitalLogBook']['morning_ta']) {
                $_POST['DigitalLogBook']['morning_ta'] = $result[4]['rate_amount'];
                //if($_POST['DigitalLogBook']['user_level']>=17 &&  $_POST['DigitalLogBook']['dutytype_id']==2)
                //		$rate+= $result[4]['rate_amount'];	 
            } else {
                $_POST['DigitalLogBook']['morning_ta'] = 0;
            }
            if (!empty($_POST['DigitalLogBook']['lunch_ta'])) {
                $_POST['DigitalLogBook']['lunch_ta'] = $result[4]['rate_amount'];
                if ($_POST['DigitalLogBook']['user_level'] >= 17 && $_POST['DigitalLogBook']['dutytype_id'] == 2)
                    $rate+= $result[4]['rate_amount'];
            }
            else {
                $_POST['DigitalLogBook']['lunch_ta'] = 0;
            }
            if ($_POST['DigitalLogBook']['night_ta']) {
                $_POST['DigitalLogBook']['night_ta'] = $result[4]['rate_amount'];
                if ($_POST['DigitalLogBook']['user_level'] >= 17 && $_POST['DigitalLogBook']['dutytype_id'] == 2)
                    $rate+= $result[4]['rate_amount'];
            }
            else {
                $_POST['DigitalLogBook']['night_ta'] = 0;
            }


            if ($_POST['DigitalLogBook']['helper_pin']) {
                if ($model->dutytype_id == 4 || $model->dutytype_id == 5)
                    $s_charge = 0.10;
                else
                    $s_charge = 0;

                $rate += ($total_time * $result[5]['rate_amount'] + ($total_time_min * $result[5]['rate_amount']) / 60) + ($total_time * $result[5]['rate_amount'] + ($total_time_min * $result[5]['rate_amount']) / 60) * $s_charge;
                $ot_amount_helper = $ot_time * $result[5]['rate_amount'] + ($ot_time_min * $result[5]['rate_amount']) / 60;
            }
            //else $ot_amount_helper=0;

            $_POST['DigitalLogBook']['helper_ot'] = $ot_amount_helper;


            if ($_POST['DigitalLogBook']['night_halt'] == 1) {
                $ot_time_temp = 0;
                $ot_amount = 0;
            }

            $_POST['DigitalLogBook']['total_time'] = $total_time_temp;

            $_POST['DigitalLogBook']['total_run'] = $total_run;

            $_POST['DigitalLogBook']['total_ot_hour'] = number_format($ot_time_temp, 2);


            $_POST['DigitalLogBook']['total_ot_amount'] = $ot_amount;

            $_POST['DigitalLogBook']['sup_mile_amount'] = $sup_mile_amount;

            $_POST['DigitalLogBook']['bill_amount'] = $rate;



            if ($_POST['DigitalLogBook']['user_level'] >= 17 && $_POST['DigitalLogBook']['dutytype_id'] == 2) {
                $_POST['DigitalLogBook']['used_mileage'] = $new_used_mileage;
            } else {
                $_POST['DigitalLogBook']['used_mileage'] = 0;
            }


            $model->attributes = $_POST['DigitalLogBook'];

            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DigitalLogBook');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DigitalLogBook('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DigitalLogBook']))
			$model->attributes=$_GET['DigitalLogBook'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    public function actionDriverObjection(){
        $model = DigitalLogBook::model()->findByPk($_GET["id"]);
        $this->renderPartial('objection', array(
            'model' => $model,
        ));        
    }
    public function actionObjectionSubmit(){
       $row = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE id='".$_POST["id"]."'")->queryRow(); 
       $v = ($row['objection_solve']==0) ? 1 : 0;      
       $update_row = Yii::app()->db->createCommand("UPDATE tbl_digital_log_book_history SET objection_solve ='".$v."' WHERE id='".$_POST["id"]."'")->execute();
       if($update_row) {
        echo "Save Successfully.";
       }
       else{
        echo "Sorry, Try Agin.";
       }     
    }

    public function actionRemove()
    {
        $model=new DigitalLogBook('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['DigitalLogBook']))
            $model->attributes=$_GET['DigitalLogBook'];

        $this->render('remove',array(
            'model'=>$model,
        ));
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DigitalLogBook::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='digital-log-book-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionLogBook(){
         
		$id=$_POST['id'];
        $sql="SELECT * FROM tbl_movements WHERE digi_id='$id'";
        $sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
        if(empty($sql_rs)) {
    		$model = $this->loadModel($id);
    	
    		//$model1 = new Movements;
    		//$model1->attributes=$model->attributes;

            $user_name=Yii::app()->user->username;
           
    		$created_time=date("Y-m-d H:i:s");
            $sql_rs = Yii::app()
                ->db
                ->createCommand()
                ->insert('tbl_movements',
                    array(
                    'id'=>'',
                    'requisition_id' => $model->requisition_id,
                    'user_pin' => $model->user_pin,
                    'user_name' => $model->user_name,
                    'user_level' => $model->user_level,
                    'user_dept' => $model->user_dept,
                    'email' => $model->email,
                    'user_cell' => $model->user_cell,
                    'user_address' => $model->user_address,
                    'vehicle_reg_no' => $model->vehicle_reg_no,
                    'vehicletype_id' => $model->vehicletype_id,
                    'vehicle_location' => $model->start_point,
                    'driver_pin' => $model->driver_pin,
                    'driver_name' => $model->driver_name,
                    'helper_pin' => $model->helper_pin,
                    'helper_name' => $model->helper_name,
                    'helper_ot' => $model->helper_ot,
                    'dutytype_id' => $model->dutytype_id,
                    'dutyday' => $model->dutyday,
                    'start_date' => $model->start_date,
                    'end_date' => $model->end_date,
                    'start_time' => $model->start_time,
                    'end_time' => $model->end_time,
                    'total_time' => $model->total_time,
                    'start_point' => $model->start_point,
                    'end_point' => $model->end_point,
                    'start_meter' => $model->start_meter,
                    'end_meter' => $model->end_meter,
                    'total_run' => $model->total_run,
                    'rph' => $model->rph,
                    'night_halt' => $model->night_halt,
                    'nh_amount' => $model->nh_amount,
                    'morning_ta' => $model->morning_ta,
                    'lunch_ta' => $model->lunch_ta,
                    'night_ta' => $model->night_ta,
                    'sup_mile_amount' => $model->sup_mile_amount,
                    'bill_amount' => $model->bill_amount,
                    'total_ot_hour' => $model->total_ot_hour,
                    'total_ot_amount' => $model->total_ot_amount,
                    'used_mileage' => $model->used_mileage,
                    'created_by' => $model->created_by,
                    'created_time' => $model->created_time,
                    'trip_id' => $model->trip_id,
                    'digi_id' => $model->id,
                    ));
            
    		//if($sql_rs){
                    
                $r= Yii::app()->db->createCommand("UPDATE tbl_digital_log_book SET submit='1', updated_by='".Yii::app()->user->username."', updated_time='".date("Y-m-d H:i:s")."' WHERE id='".$id."'")->execute();
              
              /*$r = Yii::app()->db->createCommand()->update(
                            'tbl_digital_log_book', 
                            array(
                                'submit'=>1,
                                'updated_by'=>Yii::app()->user->username,
                                'updated_time'=>date("Y-m-d H:i:s"),
                            ),
                            'user_id=:id', 
                            array(':id'=>$id)                        
                        );*/
                //$model->submit='1';
                //$model->updated_by = Yii::app()->user->username;
                //$model->updated_time = date("Y-m-d H:i:s");
                //$model->save();
               // var_dump($model->getErrors());
    			echo "Data Save to Log Book successfully.";
    		/*}
    		else{
    			
    			echo "Sorry! Try Again.";
    		}*/
        }
        else{
            echo "You already submit this log!.";
        }
	}
    public function actionOtherCost(){
        $ids=$_POST['str'];
       
            $sql="SELECT * FROM tbl_digital_log_book_history WHERE id IN($ids)"; 
            $sql_rs=Yii::app()->db->createCommand($sql)->queryALL();

            $parking_charge=0;
            $toll=0;
            $toll_quantity=0;
            $toll_description="";
            $seat_Rent=0;
            $police_case=0;
            $breakfast=0;
            $lunchbreak=0;
            $repaircost=0;
            $others=0;
            $vehicle_reg_no=$sql_rs[0]['vehicle_reg_no'];
            $bill_date=date("Y-m-d");
            $driver_pin=$sql_rs[0]['driver_pin'];
            $total_cost=0;
            $created_by=Yii::app()->user->username;
            $created_time=date("Y-m-d H:i:s");
            $id_count=array();

            foreach ($sql_rs as $key => $value) {
               
               
             if(trim($value['others_cost'])=="Parking Charge"){  
                $parking_charge=$parking_charge + $value['others_bill']; 
                $id_count[]= $value['id'];                        
             } 
             if($value['others_cost']=="Toll Amount"){ 
                $toll=$toll+ $value['others_bill'];
                $toll_quantity++;  
                 $toll_description=empty($toll_description)?$toll_description:$toll_description+", "+$value['others_description']; 
                $id_count[]= $value['id'];                 
             }  
             if($value['others_cost']=="Toll Description"){ 
                $toll=$toll+ $value['others_bill'];
                $toll_quantity++;  
                $toll_description=empty($toll_description)?$toll_description:$toll_description+", "+$value['others_description']; 
                $id_count[]= $value['id'];                 
             }  
             if($value['others_cost']=="Toll Quantity"){ 
                $toll=$toll+ $value['others_bill'];
                $toll_quantity++;  
                 $toll_description=empty($toll_description)?$toll_description:$toll_description+", "+$value['others_description'];  
                $id_count[]= $value['id'];                
             }  
              if($value['others_cost']=="Seat Rent"){ 
               $seat_Rent=$seat_Rent+$value['others_bill'];
               $id_count[]= $value['id'];              
             }  
             if($value['others_cost']=="Police Case"){ 
                $police_case=$police_case+$value['others_bill']; 
                $id_count[]= $value['id'];                  
             } 
             if($value['others_cost']=="Repair Cost"){   
                $repaircost=$repaircost+$value['others_bill'];
             } 
             if($value['others_cost']=="Breakfast Amount"){  
                $breakfast=$breakfast+$value['others_bill']; 
                $id_count[]= $value['id'];             
             } 
             if($value['others_cost']=="Lunchbreak Amount"){  
                $lunchbreak=$lunchbreak+$value['others_bill'];   
                $id_count[]= $value['id'];           
             } 
             if($value['others_cost']=="Others"){  
                $others=$others+$value['others_bill'];  
                $id_count[]= $value['id'];            
             }  

              
        }

        $total_cost=$parking_charge+$toll+$seat_Rent+$police_case+$breakfast+$lunchbreak+$others;
          
       $sql="INSERT INTO tbl_costs(
            id,
            night_halt,
            parking_charge,
            toll,
            toll_description,
            toll_quantity,
            seat_Rent,
            police_case,
            breakfast,
            lunchbreak,
            others,
            vehicle_reg_no,
            bill_date,
            parking_charge_date,
            toll_date,
            seat_rent_date,
            police_case_date,
            breakfast_date,
            lunchbreak_date,
            others_date,
            driver_pin,
            total_cost,
            created_by,
            created_time
            )
             VALUES(
             null,
             '0',
             '".$parking_charge."',
             '".$toll."',
             '".$toll_description."',
             '".$toll_quantity."',
             '".$seat_Rent."',
             '".$police_case."',
             '".$breakfast."',
             '".$lunchbreak."',
             '".$others."',
             '".$vehicle_reg_no."',
             '".$bill_date."',
             '0000-00-00',
             '0000-00-00',
             '0000-00-00',
             '0000-00-00',
             '0000-00-00',
             '0000-00-00',
             '0000-00-00',
             '".$driver_pin."',
             '".$total_cost."',
             '".$created_by."',
             '".$created_time."'           
         )";
        
         $sql_rs=Yii::app()->db->createCommand($sql)->execute();
          
         $sql="SELECT * FROM tbl_costs WHERE vehicle_reg_no='$vehicle_reg_no' AND created_by='$created_by' AND created_time='$created_time'";
         $sql_rs=Yii::app()->db->createCommand($sql)->queryRow();

         $cosr_id=$sql_rs['id'];

         foreach ($id_count as $key => $value) {

             $sql="UPDATE tbl_digital_log_book_history SET cost_id='$cosr_id' WHERE id='$value'";
             $sql_rs_digi=Yii::app()->db->createCommand($sql)->execute();
         }

        if($sql_rs){

            echo "Data Save to Log Book successfully.";
        }
        else{
            
            echo "Sorry! Try Again.";
        }
        
        
    }
    public function actionDitalLogBookRemove(){
        $id=$_POST['id'];
        $remove=Yii::app()->db->createCommand("UPDATE tbl_digital_log_book SET remove=1 WHERE id='".$id."'")->execute();
        if($remove){
            echo "Data Remove successfully.";
        }
        else{
              echo "Sorry! Try Again.";
        }

    }
    public function actionOthers($id)
    {
        $this->renderPartial('others',array(
            'model'=>$this->loadModel($id),
        ));
    }
    public function actionAllBill($id)
    {        
        $this->render('allBill',array(
            'model'=>$this->loadModel($id),
        ));
    }
    public function actionAllBills($id)
    {
        $this->renderPartial('allBills',array(
            'model'=>$this->loadModel($id),
        ));

    }
     public function actionRemarks()
    {
        if(!empty($_POST['id'])) {            
            $sql_rs = Yii::app()->db->createCommand("UPDATE tbl_digital_log_book SET remark = '".$_POST["body"]."' WHERE id = '".$_POST["id"]."'")->execute();
            if(!empty($sql_rs)) {
                echo "Remarks Save Successfully.";
                return;
                }
            else {
                echo "Sorry, Try Again.";
                return;
            }  
        }
        else{
           echo "Invalid ID";
                return; 
        }

    }
    public function actionGetRequisitionData() {
        $requisition_id = $_REQUEST['DigitalLogBook']['requisition_id'];
        
       

        if (!empty($requisition_id)) {





            $sql = "SELECT vehicle_reg_no,vehicle_location,driver_pin,driver_name,user_pin,user_name,user_level,dept_name,user_cell,email,user_address,dutytype_id,vehicletype_id,billing_code  FROM tbl_requisitions  WHERE id =  '$requisition_id' AND active=2";


            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            if (!empty($result[0]['vehicle_reg_no'])) {
                $vehicle_reg_no = $result[0]['vehicle_reg_no'];


               //$sql = "SELECT max(end_meter) as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no'";
               $sql = "SELECT end_meter as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no' ORDER BY created_time DESC";
                $command = Yii::app()->db->createCommand($sql);
                $result1 = $command->queryAll();

                if ($result1[0]['max_meter']){
                    $result[0]['start_meter'] = $result1[0]['max_meter'];
                   
                    

                }
                else{
                    $result[0]['start_meter'] = 0;
                    
                }

                echo CJSON::encode($result[0]);
                exit;
            }
        } else {


            return false;
        }
    }
    public function actionCallHrUser() {
        if ($_REQUEST['DigitalLogBook']['requisition_id'])
            return 0;

        $user_pin = $_REQUEST['DigitalLogBook']['user_pin'];

        $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";

        $staffInfo = CJSON::decode(file_get_contents($uri));

        $project_id = $staffInfo[0]['ProjectID'];

        $uri2 = "http://api.brac.net/v1/projects/".$project_id."/billingcode/Transport%20running%20Cost";
        $transportRunningCost = CJSON::decode(file_get_contents($uri2));

        $staffInfo[0]['AccountsHeadShortCode'] = $transportRunningCost[0]['AccountsHeadShortCode'];

        echo CJSON::encode($staffInfo[0]);die;
        // try {
        //     /* $soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl"); */

        //     /*
        //       $soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl",
        //       array('proxy_host'=> "192.168.2.1",
        //       'proxy_port'    => 8080,
        //       'proxy_login'    => "",
        //       'proxy_password' => ""));
        //      */

        //     $soapClient = new SoapClient("http://172.25.100.86:800/StaffInfo.asmx?wsdl", array(
        //         'proxy_host' => "192.168.2.1",
        //         //'proxy_port'    => 8080,
        //         'proxy_login' => "",
        //         'proxy_password' => "")
        //     );


        //     if ($_REQUEST['Movements']['requisition_id'])
        //         return 0;

        //     $user_pin = $_REQUEST['Movements']['user_pin'];
        //     $StaffPIN = array('strStaffPIN' => $user_pin);

        //     $strStaffINFO = $soapClient->__call('StaffInfoByPIN', array($StaffPIN));


        //     $varb = CJSON::decode($strStaffINFO->StaffInfoByPINResult);

        //     echo CJSON::encode($varb[0]);
        //     exit;
        // } catch (SoapFault $fault) {
        //     $error = 1;
        //     print($fault->faultcode . "-" . $fault->faultstring);
        // }
    }
    public function actionGetName() {

        if (!empty($_GET['DigitalLogBook']['driver_pin'])) {
            $qterm = $_GET['DigitalLogBook']['driver_pin'];
            $sql = "SELECT name  FROM tbl_drivers  WHERE pin =  '$qterm'";


            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            echo CJSON::encode($result[0]);
            exit;
        } else {
            return false;
        }
    }
     public function actionCallHrUserForHelper() {
        $user_pin = $_REQUEST['DigitalLogBook']['helper_pin'];

        $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";

        $staffInfo = CJSON::decode(file_get_contents($uri));

        $project_id = $staffInfo[0]['ProjectID'];

        $uri2 = "http://api.brac.net/v1/projects/".$project_id."/billingcode/Transport%20running%20Cost";
        $transportRunningCost = CJSON::decode(file_get_contents($uri2));

        $staffInfo[0]['AccountsHeadShortCode'] = $transportRunningCost[0]['AccountsHeadShortCode'];
        
        echo CJSON::encode($staffInfo[0]);die;
        // try {
        //     /*
        //       $soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl");
        //      */

        //     /*
        //       $soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl",
        //       array('proxy_host'=> "192.168.2.1",
        //       'proxy_port'    => 8080,
        //       'proxy_login'    => "",
        //       'proxy_password' => "")
        //       );
        //      */

        //     $soapClient = new SoapClient("http://172.25.100.86:800/StaffInfo.asmx?wsdl", array('proxy_host' => "192.168.2.1",
        //         //'proxy_port'    => 8080,
        //         'proxy_login' => "",
        //         'proxy_password' => ""));

        //     $user_pin = $_REQUEST['Movements']['helper_pin'];



        //     $StaffPIN = array('strStaffPIN' => $user_pin);
        //     //$strStaffINFO=$soapClient->__call('getStaffInfo', $StaffPIN);
        //     $strStaffINFO = $soapClient->__call('StaffInfoByPIN', array($StaffPIN));
        //     //$strStaffINFO=$soapClient->getStaffInfo('00080364');
        //     //echo $strStaffINFO->getStaffInfoResult;

        //     $varb = CJSON::decode($strStaffINFO->StaffInfoByPINResult);

        //     echo CJSON::encode($varb[0]);
        //     exit;
        // } catch (SoapFault $fault) {
        //     $error = 1;
        //     print($fault->faultcode . "-" . $fault->faultstring);
        // }
    }
    private function vehicle($reg) {
      $sql = Yii::app()->db->createCommand("SELECT * FROM tbl_vehicles WHERE reg_no='".$reg."'")->queryRow();
      return $sql;
    }
    private function pathFinderKM($asset_id,$st,$et) {     
      $KM = file_get_contents('https://api.finder-lbs.com/api/54f2cb4b421aa9493557fc09/getdistancebyasset?email=bracblc123@gmail.com&asset_id='.$asset_id.'='.$st['0'].'%20'.$st['1'].'&tt='.$et['0'].'%20'.$et['1']);
      $KM = json_decode($KM);       
      return $km = $KM->success ? $KM->output[0]->distance_in_kms : 0;
    }

}
/*
        // fetch vehicle info
        $reg = $this->vehicle($model->reg_no);
        // Path finder KM
        $st = explode(' ',date("d/m/Y H:i",strtotime($model->driver_start_time_mobile)));
        $et = explode(' ',date("d/m/Y H:i",strtotime($model->driver_end_time_mobile)));        
        $km = $this->pathFinderKM($reg['asset_id'],$st,$et);
        $first_meter = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE driver_start_time_mobile='".$model->driver_start_time_mobile."' AND trip_id='".$model->trip_id."' order by id ASC")->queryRow();
        $model->meter_number = $first_meter['meter_number'] + $km;

*/
