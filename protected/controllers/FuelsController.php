<?php

class FuelsController extends Controller
{

	public $typefuel = Array(
			'CNG'=>'CNG',
			'Octane'=>'Octane',
			'Diesel'=>'Diesel',
			'petrol '=>'petrol ',
			'LPG'=>'LPG',
			'Others'=>'Others',			
	);
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','calculationForm','fuelCalculationForm','driverInfo'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','excel','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCalculationForm()
	{
		$model = new Fuels;
		$data ='';
			if(isset($_GET['Fuels'])) {
				$model->attributes = $_GET['Fuels'];				
				if(!empty($model->vehicle_reg_no) && !empty($model->purchase_date)){
					$data = $this->driverFuelConsume($model);
					$model->driver_pin = $data['pin'];				
					$model->driver_name = $data['name'];				
				}				
			}			
		$this->render('calculationFormOne',array(
			'model' => $model,
			'data' => $data,
		));
	}

	public function actionFuelCalculationForm()
	{
		$model = new Fuels;
		$this->render('calculationFormTwo',array(
			'model'=>$model,
		));
	}
	private function driverFuelConsume($data) {
		$month_year = explode("-", date("Y-m",strtotime($data->purchase_date)));
		/*$fuel_list = Yii::app()->db->createCommand("SELECT * FROM tbl_fuels WHERE driver_pin='".$data->driver_pin."' AND YEAR(purchase_date)='".$month_year[0]."' AND MONTH(purchase_date)='".$month_year[1]."' ORDER BY purchase_date ASC")->queryAll();*/
		$fuel_list = Yii::app()->db->createCommand("SELECT * FROM tbl_fuels WHERE vehicle_reg_no='".$data->vehicle_reg_no."' AND YEAR(purchase_date)='".$month_year[0]."' AND MONTH(purchase_date)='".$month_year[1]."' ORDER BY purchase_date ASC")->queryAll();
		$driver_info = Yii::app()->db->createCommand("SELECT * FROM tbl_vehicles as v INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin WHERE v.reg_no='".$data->vehicle_reg_no."'")->queryRow();
		$cng=$diesel=0;
		$fuel_chart = array('cng'=>$cng,'diesel'=>$diesel,'pin'=>$driver_info['pin'],'name'=>$driver_info['name']);
		foreach ($fuel_list as $key => $value) {
			if($value['fuel_type']=='CNG' || $value['fuel_type']=='LPG') {
				$size = count($fuel_chart[$value['purchase_date']]['cng'])+1;
				if($size>$cng) $cng = $size;
				$fuel_chart[$value['purchase_date']]['cng'][$size] = $value['quantity'];
			}
			elseif($value['fuel_type']=='Diesel' || $value['fuel_type']=='Octane' || $value['fuel_type']=='petrol') {
				$size = count($fuel_chart[$value['purchase_date']]['diesel'])+1;
				if($size>$diesel) $diesel = $size;
				$fuel_chart[$value['purchase_date']]['diesel'][$size] = $value['quantity'];
			}
			else{}
			
		}
		$fuel_chart['cng'] = $cng;
		$fuel_chart['diesel'] = $diesel;
		return $fuel_chart;

		//return Yii::app()->db->createCommand("SELECT fuel_type,purchase_date, count(*) FROM tbl_fuels WHERE driver_pin='".$data->driver_pin."' AND YEAR(purchase_date)='".$month_year[0]."' AND MONTH(purchase_date)='".$month_year[1]."' GROUP BY purchase_date,fuel_type ORDER BY count(purchase_date) DESC")->queryAll();

	}
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
		$model=new Fuels;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuels']))
		{
			$getname = Yii::app()->user->username;
            $_POST['Fuels']['created_by'] = $getname;
			
			$model->attributes=$_POST['Fuels'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'typefuel'=>$this->typefuel,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Fuels']))
		{
			$model->attributes=$_POST['Fuels'];
            
            $b=new FuelsLog;
            $b->attributes = $model->attributes;
            $model->save();
            $b->fuel_id = $model->id;
            $b->created_time = $model->created_time;
            $b->updated_by = Yii::app()->user->username;
            
            if($b->save())
                    $this->redirect(array('view','id'=>$model->id));
            
			/*if($model->save())
				$this->redirect(array('view','id'=>$model->id));*/
		}

		$this->render('update',array(
			'model'=>$model,
            'typefuel'=>$this->typefuel,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        throw new CHttpException(400,'Delete operation is stopped.');
        return;

        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Fuels');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		
		$model=new Fuels('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuels']))
			$model->attributes=$_GET['Fuels'];

		$this->render('index',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
			'typefuel'=>$this->typefuel,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Fuels('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Fuels']))
			$model->attributes=$_GET['Fuels'];

		$this->render('admin',array(
			'model'=>$model,
			'typefuel'=>$this->typefuel,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Fuels::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='fuels-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
    
    
   
   
       public function actionExcel()
    {
        $model=new Fuels('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];
        
        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'iFleet_Fuels',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Fuels',
    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        array(
            'name' => 'vehicle_reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg_no),array("view","id"=>$data->id))',
        ),
        //'vehicle_reg_no',
        'fuel_type',
        'quantity',
        'value',
        'purchase_date',
        array(
            'name' => 'fuel_station_id',
            'type'=>'raw',
            'value' => '$data->fuelstations->name',
        ),
        'receipt_no',
        //'fuel_station_id',
        /*
        'driver_pin',
        'created_by',
        'created_time',
        'active',
        */
    ),
        ));
        Yii::app()->end();
    } 
    public function actionDriverInfo()
    {
    	$sql = "SELECT name,pin FROM tbl_drivers WHERE active='Yes' ";
    	if($_POST['f']==1){
    		$driver_info = Yii::app()->db->createCommand($sql." AND name='".$_POST['v']."'")->queryRow();    		
    		 echo $driver_info['pin'];return;
    	}
    	elseif($_POST['f']==2){
    		$driver_info = Yii::app()->db->createCommand($sql." AND pin='".$_POST['v']."'")->queryRow();
    		echo $driver_info['name'];return;
    	}
    	else{

    	}
    }
    
  	
    

}
