<?php
class MovementsController extends Controller
{
	
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getRequisitionData','report','callHrUser','scriptUpdate','scriptInit','getName','callHrUserForHelper'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','excel','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


public function actionReport()
{
	        
        $model=new Movements('report');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Movements']))
            $model->attributes=$_GET['Movements'];

        $this->render('report',array(
            'model'=>$model,
            'dataProvider'=>$model->report(),
        ));
	
}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		if (Yii::app()->request->isAjaxRequest) 
		{
			$this->renderPartial('view', array(
								'model'=>$this->loadModel($id),
								'asDialog'=>!empty($_GET['asDialog']),
			),
			false,true);
			Yii::app()->end();
		}
	
		else 
		{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$rate=0;
		$ot_amount = 0;
		$new_used_mileage=0;
		
		$model=new Movements;
    $model->created_by = Yii::app()->user->username;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

    if(isset($_POST['Movements']))
    {
      $vehicle_type = $_POST['Movements']['vehicletype_id'];

      $sql = "SELECT * FROM tbl_vehicletypes WHERE id =  '$vehicle_type'";


      $command = Yii::app()->db->createCommand($sql);
      $vehicle_types = $command->queryAll();

      $vehicle_type_rate ='';
      if($_POST['Movements']['dutytype_id'] == '1' )
          $vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
      else if($_POST['Movements']['dutytype_id'] == '2')
          $vehicle_type_rate = $vehicle_types[0]['rate_per_km_personal'];
      else if($_POST['Movements']['dutytype_id'] == '3')
      {
          $vehicle_type_rate = $vehicle_types[0]['rate_per_km_external'];
          $vehicle_hour_rate = $vehicle_types[0]['rph_visitor'];
      }
      else if($_POST['Movements']['dutytype_id'] == '4' || $_POST['Movements']['dutytype_id'] == '5')
          $vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
          
      $vehicle_rate_sup = $vehicle_types[0]['rate_per_km'];

      $start_meter = $_POST['Movements']['start_meter'];
      $end_meter = $_POST['Movements']['end_meter'];
      
      

      $total_run = $end_meter - $start_meter;

      $start_time = $_POST['Movements']['start_time'];
      $end_time = $_POST['Movements']['end_time'];
      //$total_time_temp = $end_time - $start_time;

      $start_time_mod = explode(".",$start_time); 
      $end_time_mod = explode(".",$end_time);           	 
			if(strtotime($_POST['Movements']['end_date']) > strtotime($_POST['Movements']['start_date']))
			{
			   $end_time = 24 + $_POST['Movements']['end_time'];		   
			}
			
			$end_time_temp = $end_time;
			$flag=0;
			
			if($end_time_mod[1] < $start_time_mod[1])//&& $_POST['Movements']['user_level']<17)
			{			
				 $end_time = ($end_time - .40);
				 $flag=1;
	
			//echo (.40+".".$end_time_mod[1]);
			//echo (.60+$end_time_mod[1]/100);
				//echo $end_time = $end_time - mod[1]/100);
			}		
			//else if($_POST['Movements']['user_level']>=17 && $_POST['Movements']['dutyday']=='Holiday')
			//{
			//		$end_time = ($end_time - .40);
			//		$flag=1;
			//}
				if(45 < $start_time_mod[1])
					{			
							 $ot_time_start = (7.45 - .40);							 				
					}
					else 
					$ot_time_start = 7.45;
					
					if($end_time_mod[1]<50)
					{									
							 $ot_time_end = ($end_time_temp - .40);							 				
							 
					}
					else $ot_time_end = $end_time_temp;
					
										
					//echo $ot_time_start-$start_time;
					//echo $ot_time_end;
						
						
										
					if($_POST['Movements']['dutyday']=='Holiday')
					    $ot_time = $end_time- $start_time;
          else if($start_time< 7.45 && $end_time>17.50)
          {          	          	 
          	//$ot_time = number_format($ot_time,2);
          	  $time = number_format($ot_time_start  - $start_time,2);
							$time2 = number_format($ot_time_end - 17.50,2);
							
						  $secs = strtotime($time2)-strtotime("00.00");
							$ot_time = date("H.i",strtotime($time)+$secs);
							            }
					 else if($start_time< 7.45 && $end_time< 7.45)
           {
             $ot_time = ($end_time  - $start_time);             
           }
            else if($start_time > 17.50)
           {
          	    $ot_time = $end_time- $start_time;
           }
          else if($start_time< 7.45)
          {
             $ot_time = ($ot_time_start  - $start_time);             
           }
          else if($end_time>17.50)
              $ot_time = ($ot_time_end - 17.50);
          else
              $ot_time = 0.00;
			
		$ot_time_temp = $ot_time;
		
		
		$ot_time = number_format($ot_time,2);
	 
			
		$ot_time_mod = explode(".",$ot_time);  
		
		
		
		 $ot_time = $ot_time_mod[0];		
		
		
		
		if(strlen($ot_time_mod[1])=='1')	    
		 	$ot_time_min = $ot_time_mod[1]*10;		
		else 
		 	$ot_time_min = $ot_time_mod[1];				
		
            if($start_time<=22 && $_POST['Movements']['user_level']>=17)
            {
               // $end_time = $end_time_temp;
            
                $total_time_sup = $end_time_temp-22.00;
            }
            else 
                $total_time_sup = $end_time-$start_time;
						
										
		$total_time = $end_time - $start_time; 
		$total_time_temp = $total_time;
								
		 
		$total_time_mod = explode(".",$total_time); 
		$total_time_sup_mod = explode(".",$total_time_sup); 
		 
		$total_time = $total_time_mod[0];			
		$total_time_sup = $total_time_sup_mod[0];			
		
		if(strlen($total_time_mod[1])=='1')	    
		 	$total_time_min = $total_time_mod[1]*10;		
		else 
		 	$total_time_min = $total_time_mod[1];		
		 
		 
		 if(strlen($total_time_sup_mod[1])=='1')	    
		 	$total_time_sup_min = $total_time_sup_mod[1]*10;		
		else 
		 	$total_time_sup_min = $total_time_sup_mod[1];				
							
				$end_time = $end_time_temp;	
				
        $sql = "SELECT * FROM tbl_billrates";
        $command = Yii::app()->db->createCommand($sql);
        $result = $command->queryAll();

        $sql = "SELECT * FROM tbl_dutytypes";
        $command = Yii::app()->db->createCommand($sql);
        $duty_types = $command->queryAll();

        $user_pin = $_POST['Movements']['user_pin'];
        $sql ="SELECT sum(total_run) FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
            AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ";
        $command = Yii::app()->db->createCommand($sql);
        $total_run_info = $command->queryAll();


        /*	echo "<pre>";
            print_r($total_run_info);
            echo "</pre>";
            die;

            echo $duty_types[2]['service_charge'];
            echo $total_run;
            echo $vehicle_type_rate;
            echo $total_time;
        */

            if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
            {
                $rate = $total_run * $vehicle_type_rate + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
            }
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
            {
                $rate = $total_run * $vehicle_type_rate+$total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
              //  $ot_amount = 	$ot_time*$result[3]['rate_amount'];
                $ot_amount = 	$ot_time*$result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
            }

            else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
            {
                $rate = $total_run * $vehicle_type_rate+$result[0]['rate_amount'];
                //$ot_amount = 	$result[0]['rate_amount'];
            }
            else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
            {
                $rate = $total_run * $vehicle_type_rate+$result[1]['rate_amount'];
                //$ot_amount = $result[1]['rate_amount'];
            }
            
            // Supervisor Official Bill
            else if($_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['user_level']>=17 && ($_POST['Movements']['dutytype_id']==1))
            {
            	if ($_POST['Movements']['night_halt']==0){
                $rate = $total_run * $vehicle_type_rate + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
              } else { $rate = $total_run * $vehicle_type_rate; }
            }
            else if($_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['user_level']>=17 && ($_POST['Movements']['dutytype_id']==1))
            {
            	if ($_POST['Movements']['night_halt']==0){
              $rate = $total_run * $vehicle_type_rate+$total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
              $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
              } 
              else { $rate = $total_run * $vehicle_type_rate;
              	}
            }
            // END Supervisor Official Bill
            
            
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Office Day' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
            {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
                $rate += $rate*$duty_types[3]['service_charge'];
                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
            }
            else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
            {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $result[0]['rate_amount'];
                $rate += $rate*$duty_types[3]['service_charge'];
                //$ot_amount = $result[0]['rate_amount'];
            }
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Holiday' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
            {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
                $rate += $rate*$duty_types[3]['service_charge'];
                $ot_amount = 	$ot_time*$result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;		
            }
            else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
            {
                $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge']))+$result[1]['rate_amount'];
                $rate += $rate*$duty_types[3]['service_charge'];
                //$ot_amount = $result[0]['rate_amount'];
            }
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['dutytype_id']==3)
            {
                $rate1 = ($total_run * ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
                echo $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			

                if($rate < $vehicle_types[0]['minimum_rate'])
                    $rate = $vehicle_types[0]['minimum_rate'];
            }
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['dutytype_id']==3)
            {
                $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
                $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
                $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			

                if($rate < $vehicle_types[0]['minimum_rate'])
                    $rate = $vehicle_types[0]['minimum_rate'];
            }
            else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['dutytype_id']==3)
	 {
              $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $result[0]['rate_amount']);//$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
              $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
              //$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
			  			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['dutytype_id']==3)
	 {
             $rate1 = ($total_run * ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $result[0]['rate_amount']);//$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
             $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
             $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['user_level']>=17 && $_POST['Movements']['user_level']<=19 && $_POST['Movements']['dutyday']=='Office Day')
            {
                 $ot_amount = 0.00;
								 $calculate_run = $total_run_info[0]['sum(total_run)'] + $total_run;
			
                if($_POST['Movements']['user_level']==17 && $calculate_run<=3000.00)
                {
                    $sup_mile_amount =  $total_run * $vehicle_rate_sup;
                    if($end_time>22.30)
                    {
                        $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                        $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                    }
                    else{
                        if ($end_time<=22.30 && $end_time>=22.00){
                            $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                            
                            } else { $rate = 0.00;}
                    }
                } else  if($_POST['Movements']['user_level']==17 && $calculate_run>3000.00)
                {
                    $run1 = (($total_run_info[0]['sum(total_run)']+$total_run) - 3000);
                    if ($total_run_info[0]['sum(total_run)'] < 3000.00){
                        if($end_time>22.30) {
                        	$rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 					$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                          
                        }
                        else{
                            if ($end_time<=22.30 && $end_time>=22.00){
                            $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                            }
                            else{$rate = $run1 * $vehicle_type_rate; 
                            	$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                            	}
                        }

                    } else{
                        if($end_time>22.30) {
                            $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                        }
                        else{
                            if ($end_time<=22.30 && $end_time>=22.00){
                            $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                            }
                            else{$rate = $total_run * $vehicle_type_rate; 
                            	
                            	$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                            	}
                        }
                    }
                }

                if($_POST['Movements']['user_level']==18 && $calculate_run<=4000.00)
                {
                    $sup_mile_amount =  $total_run * $vehicle_rate_sup;
                    if($end_time>22.30)
                    {
                        $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                        $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                    }
                    else{
                        if ($end_time<=22.30 && $end_time>=22.00){                            
                            $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;                            
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                            } else { 
                            	$rate = 0.00;
                            	$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                    }
                } else  if($_POST['Movements']['user_level']==18 && $calculate_run>4000.00)
                {
                    $run1 = (($total_run_info[0]['sum(total_run)']+$total_run) - 4000.00);
                    if ($total_run_info[0]['sum(total_run)'] < 4000.00){
                        if($end_time>22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                        }
                        else{
                            if ($end_time<=22.30 && $end_time>=22.00){
                                $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                            else{$rate = $run1 * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                        }

                    } else{
                        if($end_time>22.30) {
                            $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                        }
                        else{
                            if ($end_time<=22.30 && $end_time>=22.00){
                                $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                            else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                        }
                    }
                }


                if($_POST['Movements']['user_level']==19 && $calculate_run<=6000.00)
                {
                    $sup_mile_amount =  $total_run * $vehicle_rate_sup;
                    if($end_time>22.30)
                    {
                        $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                        
                        $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                    }
                    else{
                        if ($end_time<=22.30 && $end_time>=22.00){
                            $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else { $rate = 0.00;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                    }
                } else  if($_POST['Movements']['user_level']==19 && $calculate_run>6000.00)
                {
                    $run1 = (($total_run_info[0]['sum(total_run)']+$total_run) - 6000.00);
                    if ($total_run_info[0]['sum(total_run)'] < 6000.00){
                        if($end_time>22.30) {
                            $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                        }
                        else{
                            if ($end_time<=22.30 && $end_time>=22.00){
                                $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                            else{$rate = $run1 * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                        }

                    } else{
                        if($end_time>22.30) {
                            $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                            $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                        }
                        else{
                            if ($end_time<=22.30 && $end_time>=22.00){
                                $rate = $total_run * $vehicle_type_rate  +  $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                            else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                        }
                    }
                }


            }
            
            else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['user_level']>=17 && $_POST['Movements']['user_level']<=19 && $_POST['Movements']['dutyday']=='Holiday')
	 {
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		 $command = Yii::app()->db->createCommand($sql);
		 $used_mileage = $command->queryAll();
       $calculate_run = $used_mileage[0]['used_mileage'];
        
         if($_POST['Movements']['user_level']==17 && $calculate_run<=3000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
        
         if($end_time>22.00)
	 		   {
	 		   $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
	 		   
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
         }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
				} 
         else  if($_POST['Movements']['user_level']==17 && $calculate_run>3000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 3000.00){
				
             //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                 if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
          
         }

         if($_POST['Movements']['user_level']==18 && $calculate_run<=4000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
        
         if($end_time>22.00)
	 		   {
	 		   		$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
         }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
				}
				
        else  if($_POST['Movements']['user_level']==18 && $calculate_run>4000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 4000.00){
				         if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
       }

       if($_POST['Movements']['user_level']==19 && $calculate_run<=6000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
        
         if($end_time>22.00)
	 		   {
	 		   		$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
         }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
				}
         
    else  if($_POST['Movements']['user_level']==19 && $calculate_run>6000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 6000.00){
				         if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
       }


     }
            else if( $_POST['Movements']['user_level']>=20 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['dutytype_id']==2){

                if($_POST['Movements']['user_level']>=20)
                {
                    $sup_mile_amount =  $total_run * $vehicle_rate_sup;

                    if($end_time>22.00)
                    {
                    	if($_POST['Movements']['night_halt']=='0')
	 		   		{
                        $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                        $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
                      }
                    }
                    else
                    {
                        $rate = 0.00; $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
                    }
                }
            }

            else if( $_POST['Movements']['user_level']>=20 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['dutytype_id']==2){

                if($_POST['Movements']['user_level']>=20)
                {
                    $sup_mile_amount =  $total_run * $vehicle_rate_sup;
                    if($end_time>22.00)
                    {
                    	if($_POST['Movements']['night_halt']=='0')
	 		   		{
                        $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                        $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
                      }
                    }
                    else
                    {
                        $rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
                    }
                }
            }
            
            
           
			if ($_POST['Movements']['dutytype_id']!=1){
			$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' and dutytype_id='2' ORDER BY id DESC";
			$command = Yii::app()->db->createCommand($sql);
			$result_um = $command->queryAll();
			if(empty ($result_um))
			{
				$used_mileage = 0;
			}
			else
			{
				$used_mileage = $result_um[0]['used_mileage'];
			}
			$new_used_mileage = $used_mileage + $total_run;
			
		}	
		
			if($_POST['Movements']['morning_ta'])
			 {
			 			$_POST['Movements']['morning_ta'] = $result[4]['rate_amount'];
			 		//	if($_POST['Movements']['user_level']>=17  &&  $_POST['Movements']['dutytype_id']==2)
	 				//	$rate+= $result[4]['rate_amount'];	 
			 			
			 }
			 if($_POST['Movements']['lunch_ta'])
			 {
			 		$_POST['Movements']['lunch_ta'] = $result[4]['rate_amount'];
			 		if($_POST['Movements']['user_level']>=17   &&  $_POST['Movements']['dutytype_id']==2)
	 					$rate+= $result[4]['rate_amount'];	 
			 }
			 if($_POST['Movements']['night_ta'])
			 {
			 		$_POST['Movements']['night_ta'] = $result[4]['rate_amount'];
			 		if($_POST['Movements']['user_level']>=17   &&  $_POST['Movements']['dutytype_id']==2)
	 					$rate+= $result[4]['rate_amount'];	 
			 }			
			 
			 if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' )
			 		$_POST['Movements']['nh_amount'] = $result[0]['rate_amount'];
			 if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' )
			 		$_POST['Movements']['nh_amount'] = $result[1]['rate_amount'];
			 	
			 if(	$_POST['Movements']['night_halt']=='1' &&  	$_POST['Movements']['user_level']>=17) //&& 	$_POST['Movements']['dutytype_id']==2)
   				$rate+=$_POST['Movements']['nh_amount'];
			 	
			 if($_POST['Movements']['dutyday']=='Office Day' )
   		 		$_POST['Movements']['rph'] = $result[2]['rate_amount'];
   			if($_POST['Movements']['dutyday']=='Holiday' )
   		 		$_POST['Movements']['rph'] = $result[3]['rate_amount'];
			 
	 			
			$_POST['Movements']['used_mileage'] = $new_used_mileage;
			
		if($_POST['Movements']['night_halt']==1)
	 {
	 		$ot_time_temp=0;
	 		$ot_amount=0;
	 }
	 		
	 		
	 		if($_POST['Movements']['helper_pin'])
			 {
			 	$rate += $total_time*$result[5]['rate_amount']+($total_time_min*$result[5]['rate_amount'])/60;	 	
			 	$ot_amount_helper = 	$ot_time*$result[5]['rate_amount']+($ot_time_min*$result[5]['rate_amount'])/60;
			 	
			 }
			 $_POST['Movements']['helper_ot']=$ot_amount_helper;
	 

            $_POST['Movements']['total_time']=$total_time_temp;
            $_POST['Movements']['total_run']=$total_run;
            $_POST['Movements']['total_ot_hour']=number_format($ot_time_temp,2);
            $_POST['Movements']['total_ot_amount']=$ot_amount;
			//$_POST['Movements']['total_ot_amount']=0;
            $_POST['Movements']['bill_amount'] = $rate;
            
            $_POST['Movements']['sup_mile_amount'] = $sup_mile_amount;

            $model->attributes=$_POST['Movements'];

            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$rate=0;
		$ot_amount = 0;
		$new_used_mileage=0;
		
	
		
		$model=$this->loadModel($id);
		$model->updated_by = Yii::app()->user->username;
		
		if($model->morning_ta=='70')
			$model->morning_ta = 1;
		if($model->lunch_ta=='70')
			$model->lunch_ta = 1;
		if($model->night_ta=='70')
			$model->night_ta = 1;

        $model->bill_amount=0;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

	
		if(isset($_POST['Movements']))
		{

			$vehicle_type = $_POST['Movements']['vehicletype_id'];
					
	 	  $sql = "SELECT * FROM tbl_vehicletypes WHERE id =  '$vehicle_type'";			 	
		
		$command = Yii::app()->db->createCommand($sql);				
		$vehicle_types = $command->queryAll();		
		$vehicle_rate_sup = $vehicle_types[0]['rate_per_km'];

		$vehicle_type_rate ='';
		if($_POST['Movements']['dutytype_id'] == '1' )
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
		else if($_POST['Movements']['dutytype_id'] == '2')
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km_personal'];		
		else if($_POST['Movements']['dutytype_id'] == '3')
			{
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km_external'];		
			$vehicle_hour_rate = $vehicle_types[0]['rph_visitor'];
			}
		else if($_POST['Movements']['dutytype_id'] == '4' || $_POST['Movements']['dutytype_id'] == '5')
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
		
		$start_meter = $_POST['Movements']['start_meter'];
		$end_meter = $_POST['Movements']['end_meter'];
		
		$total_run = $end_meter - $start_meter;
		/*	echo "<pre>";
		echo "$end_meter";
		echo "<br />";
		echo "$start_meter";
		echo "<br />";
		echo "$total_run";
		echo "</pre>";
		die;*/
		
		$start_time = $_POST['Movements']['start_time'];
		$end_time = $_POST['Movements']['end_time'];
		
		//$total_time_temp = $end_time - $start_time;
		
		$start_time_mod = explode(".",$start_time); 
		        
		$end_time_mod = explode(".",$end_time);
		           	 
		if(strtotime($_POST['Movements']['end_date']) > strtotime($_POST['Movements']['start_date']))
		{
		   $end_time = 24 + $_POST['Movements']['end_time'];		   
		}
		
		$end_time_temp = $end_time;
		$flag=0;
		
		if($end_time_mod[1] < $start_time_mod[1])
		{						
			echo $end_time = ($end_time - .40);
			$flag=1;

			//echo (.40+".".$end_time_mod[1]);
			//echo (.60+$end_time_mod[1]/100);
			//echo $end_time = $end_time - mod[1]/100);
		}		
		//else if($_POST['Movements']['user_level']>=17 && $_POST['Movements']['dutyday']=='Holiday')
		//	$end_time = ($end_time - .40);
		
			if(45 < $start_time_mod[1])
					{			
							 $ot_time_start = (7.45 - .40);							 				
					}
					else 
					$ot_time_start = 7.45;
					
					if($end_time_mod[1]<50)
					{									
							 $ot_time_end = ($end_time_temp - .40);							 				
							 
					}
					else $ot_time_end = $end_time_temp;
										
					//echo $ot_time_start-$start_time;
					//echo $ot_time_end;
						
						
										
					if($_POST['Movements']['dutyday']=='Holiday')
					    $ot_time = $end_time- $start_time;
          else if($start_time< 7.45 && $end_time>17.50)
          {          	          	 
          	//$ot_time = number_format($ot_time,2);
          	 $time = number_format($ot_time_start  - $start_time,2);          	           	 
						$time2 = number_format($ot_time_end - 17.50,2);
							
						  $secs = strtotime($time2)-strtotime("00.00");
							$ot_time = date("H.i",strtotime($time)+$secs);
							
							
            }
            else if($start_time< 7.45 && $end_time< 7.45)
           {
             $ot_time = ($end_time  - $start_time);             
           }
            
            else if($start_time > 17.50)
           {
          	    $ot_time = $end_time- $start_time;
           }
          else if($start_time< 7.45)
          {
             $ot_time = ($ot_time_start  - $start_time);             
           }
          else if($end_time>17.50)
              $ot_time = ($ot_time_end - 17.50);
          else
              $ot_time = 0.00;
			
		$ot_time_temp = $ot_time;
		
		
		$ot_time = number_format($ot_time,2);
	 
			
		$ot_time_mod = explode(".",$ot_time);  
		
		
		
		$ot_time = $ot_time_mod[0];		
		
		
		
		if(strlen($ot_time_mod[1])=='1')	    
		 	$ot_time_min = $ot_time_mod[1]*10;		
		else 
		 	$ot_time_min = $ot_time_mod[1];				
		
		if($start_time<=22 && $_POST['Movements']['user_level']>=17)
		{
			//$end_time = $end_time_temp;
			
		   $total_time_sup = $end_time_temp-22.00;
		 }
		else 
			$total_time_sup = $end_time-$start_time;
			
		$total_time = $end_time - $start_time; 
	
		$total_time_temp = $total_time;
		
		
		$total_time_mod = explode(".",$total_time);  
		$total_time_sup_mod = explode(".",$total_time_sup);
		 
		$total_time = $total_time_mod[0];
		$total_time_sup = $total_time_sup_mod[0];
		
			
		if(strlen($total_time_mod[1])=='1')
		 	$total_time_min = $total_time_mod[1]*10;
		else
		 	$total_time_min = $total_time_mod[1];
		 	
		if(strlen($total_time_sup_mod[1])=='1')	    
		 	$total_time_sup_min = $total_time_sup_mod[1]*10;		
		else 
		 	$total_time_sup_min = $total_time_sup_mod[1];						 	 			 			 		 		 					 		 
				
		$end_time = $end_time_temp;	
		
	  
	  $sql = "SELECT * FROM tbl_billrates";
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();		
				
	 	$sql = "SELECT * FROM tbl_dutytypes";			 	
		$command = Yii::app()->db->createCommand($sql);
		$duty_types = $command->queryAll();		

        $user_pin = $_POST['Movements']['user_pin'];
		$sql ="SELECT sum(total_run) FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2'";
        $command = Yii::app()->db->createCommand($sql);
        $total_run_info = $command->queryAll();

    if ($_POST['Movements']['dutytype_id']!=1){
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		$command = Yii::app()->db->createCommand($sql);
		$result_um = $command->queryAll();
		
		
		if(empty($result_um[1]['used_mileage'])) {
			$used_mileage = 0;
		}
		else {
			$used_mileage = $result_um[1]['used_mileage'];
		}
	
		if ($_POST['Movements']['dutytype_id']!=$model->dutytype_id)
				$used_mileage = $result_um[0]['used_mileage']; 
		
	$new_used_mileage = $used_mileage + $total_run;
		
		/*echo "<pre>";
		echo "$used_mileage";
		echo "<br/>";
		echo "$id";
		echo "<br/>";
		echo "$total_run";
		echo "<br/>";
		echo "$new_used_mileage";
	
		echo "</pre>";
		die;	*/	
		
		
		$sql = "UPDATE tbl_movements SET used_mileage='$new_used_mileage' WHERE id='$id'";
		$command = Yii::app()->db->createCommand($sql);
		$execute1 = $command->execute();
            }
    /*	echo "<pre>";
		print_r($total_run_info);
		echo "</pre>";
		die;

		echo $duty_types[2]['service_charge'];
		echo $total_run;
		echo $vehicle_type_rate;
		echo $total_time;
    */
		
		
	 if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Office Day' && ($_POST['Movements']['user_level']<17||empty($_POST['Movements']['user_level'])) && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
	 {
				$rate = $total_run * $vehicle_type_rate + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;			
				$ot_amount = 	$ot_time*$result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
				
	 }
	 else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
	 {
				$rate = $total_run * $vehicle_type_rate+$total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
				$ot_amount = 	$ot_time*$result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
				
	 }			
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
	 {
	 			$rate = $total_run * $vehicle_type_rate+$result[0]['rate_amount'];
	 			//$ot_amount = 	$result[0]['rate_amount'];
	 }
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['user_level']<17 && ($_POST['Movements']['dutytype_id']==1 ||  $_POST['Movements']['dutytype_id']==2))
	 {
	 			$rate = $total_run * $vehicle_type_rate+$result[1]['rate_amount'];	 			
	 			//$ot_amount = $result[1]['rate_amount'];
	 	 $ot_amount = 0.00;
		
	 }
	 
	  // Supervisor Official Bill
            else if($_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['user_level']>=17 && ($_POST['Movements']['dutytype_id']==1))
            {
            	if ($_POST['Movements']['night_halt']==0){
                $rate = $total_run * $vehicle_type_rate + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
              } 
              else { $rate = $total_run * $vehicle_type_rate; }
            }
            else if($_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['user_level']>=17 && ($_POST['Movements']['dutytype_id']==1))
            {
            	if ($_POST['Movements']['night_halt']==0){
              $rate = $total_run * $vehicle_type_rate+$total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
              $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
              
               } 
              else { $rate = $total_run * $vehicle_type_rate;


              	}
            }
            // END Supervisor Official Bill
	 
	 else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Office Day' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
	 {
		        $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
		        $rate += $rate*$duty_types[3]['service_charge'];
            	$ot_amount = 	$ot_time*$result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
	 }			
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
	 {
				$rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $result[0]['rate_amount'];
				$rate += $rate*$duty_types[3]['service_charge'];
           		//$ot_amount = $result[0]['rate_amount'];
	 } 			
	 else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Holiday' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
	 {
				$rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
				$rate += $rate*$duty_types[3]['service_charge'];
        $ot_amount = 	$ot_time*$result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
	 }		
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' && ($_POST['Movements']['dutytype_id']==4 || $_POST['Movements']['dutytype_id']==5))
	 {
				$rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge']))+$result[1]['rate_amount'];
				$rate += $rate*$duty_types[3]['service_charge'];
        //$ot_amount = $result[0]['rate_amount'];
	 }		
	 else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['dutytype_id']==3)
	 {
              $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
              $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
              $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
			  			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }

	 
	 else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['dutytype_id']==3)
	 {
             $rate1 = ($total_run * ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
             $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
             $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['dutytype_id']==3)
	 {
              $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $result[0]['rate_amount']);//$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
              $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
              //$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
			  			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($_POST['Movements']['night_halt']==1 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['dutytype_id']==3)
	 {
             $rate1 = ($total_run * ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $result[0]['rate_amount']);//$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
             $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
             $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['user_level']>=17 && $_POST['Movements']['user_level']<=19 && $_POST['Movements']['dutyday']=='Office Day')
	 {
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		 $command = Yii::app()->db->createCommand($sql);
		 $used_mileage = $command->queryAll();
       	$calculate_run = $used_mileage[0]['used_mileage'];
      

         if($_POST['Movements']['user_level']==17 && $calculate_run<=3000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
         	
         	
         if($end_time>22.00)
	 		   {
	 		   	
	 		  // 	                	echo $total_time_sup;
          //              	echo $total_time_sup_min;
            //            	die;
        
	 		   		//$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
	 		   		$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
          	
        }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
						 
						
				}
	 		  } 
	 		  else  if($_POST['Movements']['user_level']==17 && $calculate_run>3000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 3000.00){
				
             //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                 if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }
             }
           //  $new_run = 3000 - $run1;
             /*if($total_run_info[0]['sum(total_run)'] > 3000) {
             /*if($end_time>22.30) {
                 echo "<pre>";
                 echo $total_run_info[0]['sum(total_run)'] ;
                 echo "<br />";
                 echo  $total_run;
                 echo "<br />";
                 echo $run1;
                 echo "<br />";
                 echo ($new_run);
                 echo "</pre>";
                 die;
                 $rate = $run1 * $vehicle_type_rate + ($end_time-22)*$result[2]['rate_amount'] + $result[4]['rate_amount'];
             }
             else{
                $rate = $total_run * $vehicle_type_rate  + ($end_time-22)*$result[2]['rate_amount'];
             }
         }*/
         }

         if($_POST['Movements']['user_level']==18 && $calculate_run<=4000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
             if($end_time>22.30)
             {
                 //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
             }
             else{
                 if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                     $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                     }
                 else { $rate = 0.00;}
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
             }
         } else  if($_POST['Movements']['user_level']==18 && $calculate_run>4000.00)
         {
             $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
             
             if ($test_run < 4000.00){
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			

                 if($end_time>22.30) {
                     $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
             				 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else {
                         $rate = $run1 * $vehicle_type_rate;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {

                     $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount'];
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }
             }
         }


         if($_POST['Movements']['user_level']==19 && $calculate_run<=6000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
             if($end_time>22.30)
             {
                 //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
             }
             else{
                 if ($end_time<=22.30 && $end_time>=22.00){
                     //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     
                 else { $rate = 0.00;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 
             }
         } else  if($_POST['Movements']['user_level']==19 && $calculate_run>6000.00)
         {
             $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
             if ($test_run < 6000.00){
                 if($end_time>22.30) {
                     $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
             				 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else {
                         $rate = $run1 * $vehicle_type_rate;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {

                     $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }
             }
         }


     }
     
else if($_POST['Movements']['night_halt']==0 && $_POST['Movements']['user_level']>=17 && $_POST['Movements']['user_level']<=19 && $_POST['Movements']['dutyday']=='Holiday')
	 {
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		 $command = Yii::app()->db->createCommand($sql);
		 $used_mileage = $command->queryAll();
       $calculate_run = $used_mileage[0]['used_mileage'];
        
         if($_POST['Movements']['user_level']==17 && $calculate_run<=3000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
        
         if($end_time>22.00)
	 		   {	 		   		 		   		 		   	
	 		   	$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		
	 		   	
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
          
         }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
				} 
         else  if($_POST['Movements']['user_level']==17 && $calculate_run>3000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 3000.00){
				
             //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                 if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
          
         }

         if($_POST['Movements']['user_level']==18 && $calculate_run<=4000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
        
         if($end_time>22.00)
	 		   {
	 		   		$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
         }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
				}
				
        else  if($_POST['Movements']['user_level']==18 && $calculate_run>4000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 4000.00){
				         if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
       }

       if($_POST['Movements']['user_level']==19 && $calculate_run<=6000.00)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
        
         if($end_time>22.00)
	 		   {
	 		   		$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
         }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
				}
         
    else  if($_POST['Movements']['user_level']==19 && $calculate_run>6000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 6000.00){
				         if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
       }


     }
     
     else if( $_POST['Movements']['user_level']>=20 && $_POST['Movements']['dutyday']=='Office Day' && $_POST['Movements']['dutytype_id']==2){
     	
     	 if($_POST['Movements']['user_level']>=20)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
         	         	
         if($end_time>22.00)
	 		   {
	 		   	 		if($_POST['Movements']['night_halt']=='0')
	 		   		{
	 		   		$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
          	}
        }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
				}
	 		  }
    }
    
    else if( $_POST['Movements']['user_level']>=20 && $_POST['Movements']['dutyday']=='Holiday' && $_POST['Movements']['dutytype_id']==2){
     	
     	 if($_POST['Movements']['user_level']>=20)
         {
         	$sup_mile_amount =  $total_run * $vehicle_rate_sup;
         if($end_time>22.00)
	 		   {
	 		   		if($_POST['Movements']['night_halt']=='0')
	 		   		{
	 		   		
	 		   		$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
          }
        }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
				}
	 		  }
    }

	 
	if($_POST['Movements']['night_halt']=='1' && $_POST['Movements']['dutyday']=='Office Day' )
			 	$_POST['Movements']['nh_amount'] = $result[0]['rate_amount'];
		 		
	else if($_POST['Movements']['night_halt']=='1' && $_POST['Movements']['dutyday']=='Holiday' )
			 		$_POST['Movements']['nh_amount'] = $result[1]['rate_amount'];
	else
			 		$_POST['Movements']['nh_amount'] = 0;
  
  
  if(	$_POST['Movements']['night_halt']=='1' &&  	$_POST['Movements']['user_level']>=17)// && 	$_POST['Movements']['dutytype_id']==2)
   $rate+=$_POST['Movements']['nh_amount'];

 if($_POST['Movements']['dutyday']=='Office Day' )
 		 		$_POST['Movements']['rph'] = $result[2]['rate_amount'];
 if($_POST['Movements']['dutyday']=='Holiday' )
   		 		$_POST['Movements']['rph'] = $result[3]['rate_amount'];



	 if($_POST['Movements']['morning_ta'])
	 {
	 			$_POST['Movements']['morning_ta'] = $result[4]['rate_amount'];
	 			//if($_POST['Movements']['user_level']>=17 &&  $_POST['Movements']['dutytype_id']==2)
	 			//		$rate+= $result[4]['rate_amount'];	 
	 }
	 else
	 {	
	 		$_POST['Movements']['morning_ta'] = 0;	 			 
	 }
	 if(!empty($_POST['Movements']['lunch_ta']))
	 {
	 		$_POST['Movements']['lunch_ta'] = $result[4]['rate_amount'];
	 		if($_POST['Movements']['user_level']>=17 &&  $_POST['Movements']['dutytype_id']==2) 
	 		$rate+= $result[4]['rate_amount'];
	 }
	 else
	 {	
	 		$_POST['Movements']['lunch_ta'] = 0;	 			 
	 }
	 if($_POST['Movements']['night_ta'])
	 {
	 		$_POST['Movements']['night_ta'] = $result[4]['rate_amount'];
	 		if($_POST['Movements']['user_level']>=17 &&  $_POST['Movements']['dutytype_id']==2)
	 		$rate+= $result[4]['rate_amount'];
	 }
	 else
	 {	
	 		$_POST['Movements']['night_ta'] = 0;	 			 
	 }
	 
	 
	 if($_POST['Movements']['helper_pin'])
	 {
	 	$rate += $total_time*$result[5]['rate_amount']+($total_time_min*$result[5]['rate_amount'])/60;	 	
	 	$ot_amount_helper = 	$ot_time*$result[5]['rate_amount']+($ot_time_min*$result[5]['rate_amount'])/60;
	 	
	 }
	 //else $ot_amount_helper=0;
	 
	 $_POST['Movements']['helper_ot']=$ot_amount_helper;
	 
	 
	 if($_POST['Movements']['night_halt']==1)
	 {
	 		$ot_time_temp=0;
	 		$ot_amount=0;
	 }
	 		
	  $_POST['Movements']['total_time']=$total_time_temp;
   
   $_POST['Movements']['total_run']=$total_run;
   
   $_POST['Movements']['total_ot_hour']=number_format($ot_time_temp,2);
   
   
 	 $_POST['Movements']['total_ot_amount']=$ot_amount;		
 	 
 	 $_POST['Movements']['sup_mile_amount'] = $sup_mile_amount;
 	  	 
	 $_POST['Movements']['bill_amount'] = $rate;
	 
	 
		
	 if ( $_POST['Movements']['user_level']>=17 && $_POST['Movements']['dutytype_id']==2 ){
	$_POST['Movements']['used_mileage'] = $new_used_mileage; } else {
		$_POST['Movements']['used_mileage'] = 0;
		
		
	}
	 
		
	 $model->attributes=$_POST['Movements'];
				
   if($model->save())
				$this->redirect(array('view','id'=>$model->id));
   }

		$this->render('update',array(
			'model'=>$model,
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        throw new CHttpException(400,'Delete operation is stopped.');
        return;

        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Movements');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
        
        $model=new Movements('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Movements']))
            $model->attributes=$_GET['Movements'];

        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Movements('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Movements']))
			$model->attributes=$_GET['Movements'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionGetRequisitionData() {
		$requisition_id = $_REQUEST['Movements']['requisition_id'];
		
		
	  if (!empty($requisition_id)) {
	  	
	  
	  	
	
	  	
	 	$sql = "SELECT vehicle_reg_no,driver_pin,driver_name,user_pin,user_name,user_level,dept_name,user_cell,email,user_address,dutytype_id,vehicletype_id	FROM tbl_requisitions  WHERE id =  '$requisition_id' AND active=2";		
	 	
		
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();
		
		if(!empty($result[0]['vehicle_reg_no']))
		{
		$vehicle_reg_no = $result[0]['vehicle_reg_no'];
		

		$sql = "SELECT max(end_meter) as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no'";					 			
		$command = Yii::app()->db->createCommand($sql);				
		$result1 = $command->queryAll();
		
		if($result1[0]['max_meter'])
		$result[0]['start_meter'] = $result1[0]['max_meter'];
		else 
		$result[0]['start_meter'] = 0;
		
		echo CJSON::encode($result[0]); exit;
	}
	  } else {
	  	
	  	
		return false;
	  }
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Movements::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='movements-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	//Script for update all
	
		public function actionScriptUpdate($id)
	{
		$rate=0;
		$ot_amount = 0;
		$new_used_mileage=0;
							
		$model=$this->loadModel($id);
		
		
		if($model->morning_ta=='70')
			$model->morning_ta = 1;
		if($model->lunch_ta=='70')
			$model->lunch_ta = 1;
		if($model->night_ta=='70')
			$model->night_ta = 1;

        $model->bill_amount=0;
        
      
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

	
		if(isset($model))
		{

			$vehicle_type = $model->vehicletype_id;
					
	 	  $sql = "SELECT * FROM tbl_vehicletypes WHERE id =  '$vehicle_type'";			 	
		
		$command = Yii::app()->db->createCommand($sql);				
		$vehicle_types = $command->queryAll();		

		$vehicle_type_rate ='';
		if($model->dutytype_id == '1' )
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
		else if($model->dutytype_id == '2')
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km_personal'];		
		else if($model->dutytype_id == '3')
			{
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km_external'];		
			$vehicle_hour_rate = $vehicle_types[0]['rph_visitor'];
			}
		else if($model->dutytype_id== '4' || $model->dutytype_id== '5')
			$vehicle_type_rate = $vehicle_types[0]['rate_per_km'];
		
		$vehicle_rate_sup = $vehicle_types[0]['rate_per_km'];
		
		$start_meter = $model->start_meter;
		$end_meter = $model->end_meter;
		
		$total_run = $end_meter - $start_meter;

		$start_time = $model->start_time;
		$end_time = $model->end_time;
		
		//$total_time_temp = $end_time - $start_time;
		
		$start_time_mod = explode(".",$start_time); 
		        
		$end_time_mod = explode(".",$end_time);
		           	 
		if(strtotime($model->end_date) > strtotime($model->start_date))
		{
		   $end_time = 24 + $model->end_time;		   
		}
		
		$end_time_temp = $end_time;
		$flag=0;
		
		if($end_time_mod[1] < $start_time_mod[1])//&& $model->user_level<17)
		{			
			$end_time = ($end_time - .40);
			$flag=1;

			//echo (.40+".".$end_time_mod[1]);
			//echo (.60+$end_time_mod[1]/100);
			//echo $end_time = $end_time - mod[1]/100);
		}		
		//else if($model->user_level>=17 && $model->dutyday=='Holiday')
		//{
		//	$end_time = ($end_time - .40);
		//	$flag=1;
		//}
		
			if(45 < $start_time_mod[1])
					{			
							 $ot_time_start = (7.45 - .40);							 				
					}
					else 
					$ot_time_start = 7.45;
					
				if($end_time_mod[1]<50)
					{									
							 $ot_time_end = ($end_time_temp - .40);							 				
							 
					}
					else $ot_time_end = $end_time_temp;
					
										

						
										
					if($model->dutyday=='Holiday')
					    $ot_time = $end_time- $start_time;
          else if($start_time< 7.45 && $end_time>17.50)
          {          	          	 
          	//$ot_time = number_format($ot_time,2);
          	  $time = number_format($ot_time_start  - $start_time,2);
							$time2 = number_format($ot_time_end - 17.50,2);
							
						  $secs = strtotime($time2)-strtotime("00.00");
							$ot_time = date("H.i",strtotime($time)+$secs);
							            }
					 else if($start_time< 7.45 && $end_time< 7.45)
           {
             $ot_time = ($end_time  - $start_time);             
           }
            else if($start_time > 17.50)
           {
          	    $ot_time = $end_time- $start_time;
           }
          else if($start_time< 7.45)
          {
             $ot_time = ($ot_time_start  - $start_time);             
           }
          else if($end_time>17.50)
              $ot_time = ($ot_time_end - 17.50);
          else
              $ot_time = 0.00;
			
		$ot_time_temp = $ot_time;
		
		
		$ot_time = number_format($ot_time,2);
	 
			
		$ot_time_mod = explode(".",$ot_time);  
		
		
		
		echo $ot_time = $ot_time_mod[0];		
		
		
		
		if(strlen($ot_time_mod[1])=='1')	    
		 	$ot_time_min = $ot_time_mod[1]*10;		
		else 
		 	$ot_time_min = $ot_time_mod[1];				
		
        if($start_time<=22 && $model->user_level>=17)
        {                        
           $total_time_sup = $end_time_temp-22.00;
         }
        else 
            $total_time_sup = $end_time-$start_time;
		
		$total_time = $end_time - $start_time; 
		
		$total_time_temp = $total_time;
		
		$total_time_mod = explode(".",$total_time);  
		$total_time_sup_mod = explode(".",$total_time_sup);
		 
		
		$total_time = $total_time_mod[0];	
		$total_time_sup = $total_time_sup_mod[0];
		
			
		if(strlen($total_time_mod[1])=='1')	    
		 	$total_time_min = $total_time_mod[1]*10;		
		else 
		 	$total_time_min = $total_time_mod[1];		
		 	
		 	
		if(strlen($total_time_sup_mod[1])=='1')	    
		 	$total_time_sup_min = $total_time_sup_mod[1]*10;		
		else 
		 	$total_time_sup_min = $total_time_sup_mod[1];						 	 			 			 		 					
				
		$end_time = $end_time_temp;	
		
	  
	  $sql = "SELECT * FROM tbl_billrates";
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();		
				
	 	$sql = "SELECT * FROM tbl_dutytypes";			 	
		$command = Yii::app()->db->createCommand($sql);
		$duty_types = $command->queryAll();		

        $user_pin = $model->user_pin;
		$sql ="SELECT sum(total_run) FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2'";
        $command = Yii::app()->db->createCommand($sql);
        $total_run_info = $command->queryAll();

            if ($model->dutytype_id!=1){
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		$command = Yii::app()->db->createCommand($sql);
		$result_um = $command->queryAll();
		if(empty($result_um[1]['used_mileage'])) {
			$used_mileage = 0;
		}
		else {
			$used_mileage = $result_um[1]['used_mileage'];
		}
		
		
		$new_used_mileage = $used_mileage + $total_run;
		
		$sql = "UPDATE tbl_movements SET used_mileage='$new_used_mileage' WHERE id='$id'";
		$command = Yii::app()->db->createCommand($sql);
		$execute1 = $command->execute();
            }

	 if($model->night_halt==0 && $model->dutyday=='Office Day' && ($model->user_level<17||empty($model->user_level)) && ($model->dutytype_id==1 ||  $model->dutytype_id==2))
	 {
				$rate = $total_run * $vehicle_type_rate + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;			
				$ot_amount = 	$ot_time*$result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
				
	 }
	 else if($model->night_halt==0 && $model->dutyday=='Holiday' && $model->user_level<17 && ($model->dutytype_id==1 ||  $model->dutytype_id==2))
	 {
				$rate = $total_run * $vehicle_type_rate+$total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
				$ot_amount = 	$ot_time*$result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
				
	 }			
	 else if($model->night_halt==1 && $model->dutyday=='Office Day' && $model->user_level<17 && ($model->dutytype_id==1 ||  $model->dutytype_id==2))
	 {
	 			$rate = $total_run * $vehicle_type_rate+$result[0]['rate_amount'];
	 }
	 else if($model->night_halt==1 && $model->dutyday=='Holiday' && $model->user_level<17 && ($model->dutytype_id==1 ||  $model->dutytype_id==2))
	 {
	 			$rate = $total_run * $vehicle_type_rate+$result[1]['rate_amount'];	 			
	 			$ot_amount = 0.00;
		
	 }
	 
	  // Supervisor Official Bill
            else if($model->dutyday=='Office Day' && $model->user_level>=17 && ($model->dutytype_id==1))
            {
            	if ($model->night_halt==0){
                $rate = $total_run * $vehicle_type_rate + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
                $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
              } else { $rate = $total_run * $vehicle_type_rate; }
            }
            else if($model->dutyday=='Holiday' && $model->user_level>=17 && ($model->dutytype_id==1))
            {
            	if ($model->night_halt==0){
              $rate = $total_run * $vehicle_type_rate+$total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
              $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
              
              } 
              else { $rate = $total_run * $vehicle_type_rate;
              	}
            }
            // END Supervisor Official Bill
	 
	 else if($model->night_halt==0 && $model->dutyday=='Office Day' && ($model->dutytype_id==4 || $model->dutytype_id==5))
	 {
		        $rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $total_time*$result[2]['rate_amount']+($total_time_min*$result[2]['rate_amount'])/60;
		        $rate+=$rate*$duty_types[3]['service_charge'];
            	$ot_amount = 	$ot_time*$result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
	 }			
	 else if($model->night_halt==1 && $model->dutyday=='Office Day' && ($model->dutytype_id==4 || $model->dutytype_id==5))
	 {
				$rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $result[0]['rate_amount'];
				$rate+=$rate*$duty_types[3]['service_charge'];
           		//$ot_amount = $result[0]['rate_amount'];
	 } 			
	 else if($model->night_halt==0 && $model->dutyday=='Holiday' && ($model->dutytype_id==4 || $model->dutytype_id==5))
	 {
				$rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge'])) + $total_time*$result[3]['rate_amount']+($total_time_min*$result[3]['rate_amount'])/60;
				$rate+=$rate*$duty_types[3]['service_charge'];
        $ot_amount = 	$ot_time*$result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
	 }		
	 else if($model->night_halt==1 && $model->dutyday=='Holiday' && ($model->dutytype_id==4 || $model->dutytype_id==5))
	 
	 {
				$rate = $total_run * ( $vehicle_type_rate + ceil($vehicle_type_rate*$duty_types[3]['service_charge']))+$result[1]['rate_amount'];
				$rate+=$rate*$duty_types[3]['service_charge'];
        //$ot_amount = $result[0]['rate_amount'];
	 }		
	 else if($model->night_halt==0 && $model->dutyday=='Office Day' && $model->dutytype_id==3)
	 {
              $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
              $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
              $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
			  			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($model->night_halt==0 && $model->dutyday=='Holiday' && $model->dutytype_id==3)
	 {
             $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge']))+ $total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
             $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
             $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($model->night_halt==1 && $model->dutyday=='Office Day' && $model->dutytype_id==3)
	 {
              $rate1 = ($total_run *  ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $result[0]['rate_amount']);//$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
              $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
              //$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
			  			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($model->night_halt==1 && $model->dutyday=='Holiday' && $model->dutytype_id==3)
	 {
             $rate1 = ($total_run * ($vehicle_type_rate+ceil($vehicle_type_rate*$duty_types[2]['service_charge'])) + $result[0]['rate_amount']);//$total_time*$vehicle_hour_rate+($total_time_min*$vehicle_hour_rate)/60);
             $rate = $rate1 + ($rate1 * $duty_types[2]['service_charge']);
             $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
			  
			  if($rate < $vehicle_types[0]['minimum_rate'])
			  	$rate = $vehicle_types[0]['minimum_rate'];
	 }
	 else if($model->night_halt==0 && $model->user_level>=17 && $model->user_level<=19 && $model->dutyday=='Office Day')
	 {
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		 $command = Yii::app()->db->createCommand($sql);
		 $used_mileage = $command->queryAll();
      	$calculate_run = $used_mileage[0]['used_mileage'];

         if($model->user_level==17 && $calculate_run<=3000.00)
         {
         if($end_time>22.00)
	 		   {
	 	    $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
          	$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
        }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
						
				}
	 		  } 
	 		  else  if($model->user_level==17 && $calculate_run>3000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 3000.00){
				
             //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                 if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }
             }
         }

         if($model->user_level==18 && $calculate_run<=4000.00)
         {
             if($end_time>22.30)
             {
                 $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
             }
             else{
                 if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                     $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                     }
                 else { $rate = 0.00;}
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
             }
         } else  if($model->user_level==18 && $calculate_run>4000.00)
         {
             $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
             
             if ($test_run < 4000.00){
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			

                 if($end_time>22.30) {
                     $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
             				 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else {
                         $rate = $run1 * $vehicle_type_rate;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {

                     $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount'];
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }
             }
         }


         if($model->user_level==19 && $calculate_run<=6000.00)
         {
             if($end_time>22.30)
             {
                 //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
             }
             else{
                 if ($end_time<=22.30 && $end_time>=22.00){
                     //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                     $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;	 		   
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     
                 else { $rate = 0.00;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 
             }
         } else  if($model->user_level==19 && $calculate_run>6000.00)
         {
             $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
             if ($test_run < 6000.00){
                 if($end_time>22.30) {
                     $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
             				 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			} else {
                         $rate = $run1 * $vehicle_type_rate;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {

                     $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                     $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;			}
                 }
             }
         }


     }
     
     else if($model->night_halt==0 && $model->user_level>=17 && $model->user_level<=19 && $model->dutyday=='Holiday')
	 {
		$sql = "SELECT * FROM tbl_movements WHERE start_date BETWEEN CONCAT( YEAR( CURDATE( ) ) , '-01-01' )
                AND CONCAT( YEAR( CURDATE( ) ) +1, '-12-31' ) AND user_pin='$user_pin' AND dutytype_id='2' ORDER BY id DESC";
		 $command = Yii::app()->db->createCommand($sql);
		 $used_mileage = $command->queryAll();
      	$calculate_run = $used_mileage[0]['used_mileage'];

         if($model->user_level==17 && $calculate_run<=3000.00)
         {
         if($end_time>22.00)
	 		   {
	 		   	
	 		   	
	 		   		$rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   	 		   		 
          	$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
        }
				else 
				{ 
						$rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
						
				}
	 		  } 
	 		  else  if($model->user_level==17 && $calculate_run>3000.00)
         {
         	
             $run1 = $used_mileage[0]['used_mileage'] - 3000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
    
			 
             if ($test_run < 3000.00){
				
             //    if ($total_run_info[0]['sum(total_run)'] < 3000){
                 if($end_time>22.30) {
                 $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
			         if ($end_time<=22.30 && $end_time>=22.00){
					 $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else { 
					 $rate = $run1 * $vehicle_type_rate;
					 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {
              
                 $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
           }

         if($model->user_level==18 && $calculate_run<=4000.00)
         {
             if($end_time>22.30)
             {
                 //$rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;// + $result[4]['rate_amount'];
                 $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
             }
             else{
                 if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                     $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   
                     }
                 else { $rate = 0.00;}
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
             }
         } else  if($model->user_level==18 && $calculate_run>4000.00)
         {
             $run1 = $used_mileage[0]['used_mileage'] - 4000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
             
             if ($test_run < 4000.00){
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			

                 if($end_time>22.30) {
                     $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
             				 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else {
                         $rate = $run1 * $vehicle_type_rate;
                         $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {

                     $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount'];
                         $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
         }


         if($model->user_level==19 && $calculate_run<=6000.00)
         {
             if($end_time>22.30)
             {
                 $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
             }
             else{
                 if ($end_time<=22.30 && $end_time>=22.00){
                     $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;	 		   
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     
                 else { $rate = 0.00;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 
             }
         } else  if($model->user_level==19 && $calculate_run>6000.00)
         {
             $run1 = $used_mileage[0]['used_mileage'] - 6000.00;
             $test_run = $used_mileage[0]['used_mileage'] - $total_run;
             if ($test_run < 6000.00){
                 if($end_time>22.30) {
                     $rate = $run1 * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
             				 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $run1 * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			} else {
                         $rate = $run1 * $vehicle_type_rate;
                         $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }

             } else{
                 if($end_time>22.30) {

                     $rate = $total_run * $vehicle_type_rate + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;// + $result[4]['rate_amount'];
                     $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			
                 }
                 else{
                     if ($end_time<=22.30 && $end_time>=22.00){
                         $rate = $total_run * $vehicle_type_rate  + $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                         $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                     else{$rate = $total_run * $vehicle_type_rate;$ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;			}
                 }
             }
         }
     }

     else if( $model->user_level>=20 && $model->dutyday=='Office Day' && $model->dutytype_id==2){

         if($model->user_level>=20)
         {
             $sup_mile_amount =  $total_run * $vehicle_rate_sup;

             if($end_time>22.00)
             {
                 $rate = $total_time_sup*$result[2]['rate_amount']+($total_time_sup_min*$result[2]['rate_amount'])/60;
                 $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
             }
             else
             {
                 $rate = 0.00; $ot_amount = $ot_time * $result[2]['rate_amount']+($ot_time_min*$result[2]['rate_amount'])/60;
             }
         }
     }

     else if( $model->user_level>=20 && $model->dutyday=='Holiday' && $model->dutytype_id==2){

         if($model->user_level>=20)
         {
             $sup_mile_amount =  $total_run * $vehicle_rate_sup;
             if($end_time>22.00)
             {
                 $rate = $total_time_sup*$result[3]['rate_amount']+($total_time_sup_min*$result[3]['rate_amount'])/60;
                 $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
             }
             else
             {
                 $rate = 0.00; $ot_amount = $ot_time * $result[3]['rate_amount']+($ot_time_min*$result[3]['rate_amount'])/60;
             }
         }
     }
     
	 
	if($model->night_halt=='1' && $model->dutyday=='Office Day' )
			 	$model->nh_amount= $result[0]['rate_amount'];
		 		
	else if($model->night_halt=='1' && $model->dutyday=='Holiday' )
			 		$model->nh_amount= $result[1]['rate_amount'];
	else
			 		$model->nh_amount= 0;
			 		
  if($model->night_halt=='1' &&  $model->user_level>=17)// && $model->dutytype_id==2)
   $rate+=$model->nh_amount;

 if($model->dutyday=='Office Day' )
 		 		$model->rph= $result[2]['rate_amount'];
 if($model->dutyday=='Holiday' )
   		 		$model->rph= $result[3]['rate_amount'];



	 if($model->morning_ta)
	 {
	 			$model->morning_ta= $result[4]['rate_amount'];
	 			//if($model->user_level>=17   &&  $model->dutytype_id==2)
	 			//		$rate+= $result[4]['rate_amount'];	 
	 }
	 else
	 {	
	 		$model->morning_ta= 0;	 			 
	 }
	 if(!empty($model->lunch_ta))
	 {
	 		$model->lunch_ta= $result[4]['rate_amount'];
	 		if($model->user_level>=17 &&  $model->dutytype_id==2)
	 		$rate+= $result[4]['rate_amount'];
	 }
	 else
	 {	
	 		$model->lunch_ta= 0;	 			 
	 }
	 if($model->night_ta)
	 {
	 		$model->night_ta= $result[4]['rate_amount'];
	 		if($model->user_level>=17 &&  $model->dutytype_id==2)
	 		$rate+= $result[4]['rate_amount'];
	 }
	 else
	 {	
	 		$model->night_ta= 0;	 			 
	 }
	 if($model->night_halt==1)
	 		$ot_time_temp=0;
	 		
	 		
	 if($model->helper_pin)
	 {
	 	$rate += $total_time*$result[5]['rate_amount']+($total_time_min*$result[5]['rate_amount'])/60;	 	
	 	$ot_amount_helper = 	$ot_time*$result[5]['rate_amount']+($ot_time_min*$result[5]['rate_amount'])/60;
	 	
	 }	 
	 $model->helper_ot=$ot_amount_helper;
	 		
	  $model->total_time=$total_time_temp;
   
   $model->total_run=$total_run;
   
   $model->total_ot_hour=number_format($ot_time_temp,2);
   
 	 $model->total_ot_amount=$ot_amount;		
 	  	 
	 $model->bill_amount= $rate;
	 $model->used_mileage= $new_used_mileage;
     $model->sup_mile_amount = $sup_mile_amount;
	 
		
	 //$model->attributes=$_POST['Movements'];
	 
	 $model->save();
	}			
   /*if($model->save())
				$this->redirect(array('view','id'=>$model->id));
   

		$this->render('update',array(
			'model'=>$model,
		));
		*/
	}
	//end script
	
	
	public function actionScriptInit()
	{
	 	$sql = "SELECT id FROM tbl_movements";			 	
		
		$command = Yii::app()->db->createCommand($sql);				
		$mov_id = $command->queryAll();	
		
		foreach($mov_id as $item)
		{		
			$this->actionScriptUpdate($item['id']);
		}
	}
	
	public function actionGetName() {		
	 		
	  if (!empty($_GET['Movements']['driver_pin'])) {
	  	$qterm = $_GET['Movements']['driver_pin'];
	 	$sql = "SELECT name	 FROM tbl_drivers  WHERE pin =  '$qterm'";
	
		
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();
		
	  echo CJSON::encode($result[0]); exit;
		
	  } else {
		return false;
	  }
	}

	
	
		public function actionCallHrUser()
	{
			try{
    /*$soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl");*/
    
/*    $soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl",
            array('proxy_host'=> "192.168.2.1",
            'proxy_port'    => 8080,
            'proxy_login'    => "",
            'proxy_password' => ""));*/

                $soapClient = new SoapClient("http://172.16.1.7:811/HRWebServices.asmx?wsdl",
                    array('proxy_host'=> "192.168.2.1",
                //        'proxy_port'    => 8080,
                        'proxy_login'    => "",
                        'proxy_password' => ""));

    

		
	  $user_pin = $_REQUEST['Movements']['user_pin'];
	  
	  
		
		
		
    $StaffPIN=array('strStaffPIN'=>$user_pin);
    //$strStaffINFO=$soapClient->__call('getStaffInfo', $StaffPIN);
    $strStaffINFO=$soapClient->__call('getStaffInfo', array($StaffPIN));
    //$strStaffINFO=$soapClient->getStaffInfo('00080364');
     //echo $strStaffINFO->getStaffInfoResult;
     
    $varb = CJSON::decode($strStaffINFO->getStaffInfoResult);        
      
    echo CJSON::encode($varb[0]); exit;
    
    
  }
catch (SoapFault $fault) {
    $error = 1;
    print($fault->faultcode."-".$fault->faultstring);
}
			
	}
	
			public function actionCallHrUserForHelper()
	{
			try{
    /*$soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl");*/
    
/*    $soapClient = new SoapClient("http://192.168.3.102/HRService/HRWebServices.asmx?wsdl",
            array('proxy_host'=> "192.168.2.1",
            'proxy_port'    => 8080,
            'proxy_login'    => "",
            'proxy_password' => ""));*/

                $soapClient = new SoapClient("http://172.16.1.7:811/HRWebServices.asmx?wsdl",
                    array('proxy_host'=> "192.168.2.1",
                //        'proxy_port'    => 8080,
                        'proxy_login'    => "",
                        'proxy_password' => ""));

    

		
	  
	  $user_pin = $_REQUEST['Movements']['helper_pin'];
		
		
		
    $StaffPIN=array('strStaffPIN'=>$user_pin);
    //$strStaffINFO=$soapClient->__call('getStaffInfo', $StaffPIN);
    $strStaffINFO=$soapClient->__call('getStaffInfo', array($StaffPIN));
    //$strStaffINFO=$soapClient->getStaffInfo('00080364');
     //echo $strStaffINFO->getStaffInfoResult;
     
    $varb = CJSON::decode($strStaffINFO->getStaffInfoResult);        
      
    echo CJSON::encode($varb[0]); exit;
    
    
  }
catch (SoapFault $fault) {
    $error = 1;
    print($fault->faultcode."-".$fault->faultstring);
}
			
	}
	
	
    public function actionExcel()
    {
        $model=new movements('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];
        
        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'iFleet_Log_Book',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Log_Book',
    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'requisition_id',
        'driver_pin',
        'driver_name',
        'user_pin',
        'user_name',
        'user_level',
        'user_dept',
        'vehicle_reg_no',
        'start_date',
        'end_date',
        'start_meter',
        'end_meter',
        'total_run',
        array(
            'name' => 'dutytype_id',
            'type' => 'raw',
            'value' => 'CHtml::encode($data->dutytypes->type_name)'
        ),
        array(
            'name' => 'bill_amount',
            'type' => 'raw',
            'header' => 'Bill Amount (BDT)',
            'value' => '$data->bill_amount'
        )
        /*


        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'start_point',
        'end_point',
        'start_meter',
        'end_meter',
        'night_halt',
        'bill_amount',
        'created_by',
        'created_time',
        'active',
        */
    ),
        ));
        Yii::app()->end();
    }
	
	
}
