<?php
date_default_timezone_set("Asia/Dhaka");
class EmailSmsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $driver_category = array('Pick and Drop Vehicle'=>'Pick and Drop Vehicle','Pool Vehicle' => 'Pool Vehicle', 'Pickup or Delivery Van' => 'Pickup or Delivery Van',' Dedicated Vehicle' => ' Dedicated Vehicle','All Bus'=>'All Bus','All Field vehicle (39 Vehicle)'=>'All Field vehicle (39 Vehicle)','All Drivers' => 'All Drivers');
	public $WS_category = array('Officer'=>'Officer','Engine Section' => 'Engine Section', 'AC Section' => 'AC Section','Elecrtic Section' => 'Elecrtic Section','Dent Section'=>'Dent Section','Paint Section'=>'Paint Section','Washing Section' => 'Washing Section','Workshop Driver' => 'Workshop Driver','Foreman'=>'Foreman','All Workshop User'=>'All Workshop User');

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','category'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EmailSms;
		$drop_down ="";
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailSms']))
		{
			$model->attributes = $_POST['EmailSms'];			
			$model->created_time = date('Y-m-d H:i:s');
			$model->created_by = Yii::app()->user->name;			
			$model->type = 2;
			$drop_down = ($model->user_type=="Driver") ? $this->driver_category	:(($model->user_type=="Workshop") ?  $this->WS_category :"");			
			if(!empty($model->body) && !empty($model->category) && !empty($model->user_type)) {	
				if($model->user_type=="Driver") {					
					$this->driverGroup($model->body,$model->category);					
				}
				else if($model->user_type=="Workshop") {					
					$this->workshopGroup($model->body,$model->category);
				}
				else {}								
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}		
		$this->render('create',array(
			'model'=>$model,'drop_down'=>$drop_down,
		));
	}
	private function workshopGroup($body,$category){
		$sql = "SELECT * FROM tbl_workshop_staff ";
		if($category!="All Workshop User") {
			$sql .=" WHERE user_designation='".$category."'";
		}
		$workshop = Yii::app()->db->createCommand($sql)->queryALL();		
		$response = $this->actionSmssend("01684888352", $body);			
		foreach ($workshop as $key => $value) {
			//if(!empty($value['phone']))				
			//$this->actionSmssend($value['phone'], $body);
		}	
	}
	private function driverGroup($body,$category){
		$sql = "SELECT d.*,v.* FROM tbl_drivers as d  INNER JOIN tbl_vehicles as v ON v.driver_pin=d.pin WHERE d.active='Yes' AND v.active=1 ";
		if($category=="Pick and Drop Vehicle") {
			$sql.= " AND v.dedicate='Pick and Drop' AND v.active=1";
		}		
		else if($category=="Pool Vehicle") {
			$sql.= " AND v.dedicate='Pool' ";
		}		
		else if($category=="Pickup or Delivery Van") {
			$sql.= " AND v.vehicletype_id='4' ";
		}		
		else if($category=="Dedicated Vehicle") {
			$sql.= " AND v.dedicate='Dedicate' ";
		}		
		else if($category=="All Bus") {
			$sql.= " AND v.vehicletype_id='5' ";
		}		
		else if($category=="All Field vehicle (39 Vehicle)") {
			$sql.= " AND (v.dedicate='BLC' OR v.dedicate='Microfinance') ";
		}		
		else if($category=="All Drivers") {
			$sql = "SELECT d.* FROM tbl_drivers as d WHERE d.active='Yes' ";
		}		
		else {}	
		$sql .=" GROUP BY d.pin";
		$drivers = Yii::app()->db->createCommand($sql)->queryALL();	
		//var_dump($drivers);exit();
		$response = $this->actionSmssend("01684888352", $body);			
		foreach ($drivers as $key => $value) {
			//if(!empty($value['phone']))				
			//$this->actionSmssend($value['phone'], $body);
		}
	}
	public function actionSmssend($driver_cell, $body)
    {
        $client = new EHttpClient(
            'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
            )
        );   
		$driver_cell = substr($driver_cell, -11);
        if (strlen($driver_cell) == 10)
            $driver_cell = "0" . $driver_cell;
        else
        $driver_cell = $driver_cell;         
            
       
        $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $body,
            'to_number' => $driver_cell,
            'app_url' => 'http://ifleet.brac.net/',
            'type' =>3
        ));        
        $response = $client->request('POST');
       
       }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EmailSms']))
		{
			$model->attributes=$_POST['EmailSms'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('EmailSms');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		$model=new EmailSms('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmailSms']))
			$model->attributes=$_GET['EmailSms'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EmailSms('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmailSms']))
			$model->attributes=$_GET['EmailSms'];

		$this->render('admin',array(
			'model'=>$model,
		));
		/*$workshop_user = Yii::app()->db->createCommand("SELECT * FROM tbl_workshop_staff")->queryALL();
		foreach ($workshop_user  as $key => $value) { 
			$uri = "http://api.brac.net/v1/staffs/".$value['user_pin']."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
	        $staffInfo = CJSON::decode(file_get_contents($uri)); 
	        var_dump($staffInfo[0]["EmailID"]);  	    
			$user_update = Yii::app()->db->createCommand("update tbl_workshop_staff SET phone='".$staffInfo[0]["MobileNo"]."', email='".$staffInfo[0]["EmailID"]."' WHERE id='".$value["id"]."'")->execute();

	            $first_name=substr($staffInfo[0]['StaffName'],0,strrpos($staffInfo[0]['StaffName'],' '));
	            $last_name=substr($staffInfo[0]['StaffName'],-(strlen($staffInfo[0]['StaffName'])-(strrpos($staffInfo[0]['StaffName'],' '))));
	            
       	} */    
	}/**/

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=EmailSms::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function actionCategory() {

		if($_POST['v']=='Driver') {
			$drop_down = $this->driver_category;
		}
		else if ($_POST['v']=='Workshop') {
			$drop_down = $this->WS_category;
		}
		else {
			$drop_down = '';
		}
		$this->renderPartial('category',array('drop_down'=>$drop_down));	
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='email-sms-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
