<?php

class WorkshopController extends CustomController
{
	public $layout='//layouts/main_workshop_home';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index', 'dashboard'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				// 'actions'=>array('create','update','excel'),
				// 'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				// 'actions'=>array('admin','delete'),
				////'users'=>array('admin'),
				// 'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				// 'users'=>array('*'),
			),
		);
	}

	/**
	 * Workshop landing page.
	 */
	public function actionIndex() {
		$this->render('index');
	}

	/**
	 * Workshop dashboard.
	 */
	public function actionDashboard() {
		$this->layout='main_workshop';
		$this->render('dashboard');
	}
}
