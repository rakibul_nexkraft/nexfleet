<?php

class InvoiceLineController extends CustomController
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','userView','invoiceLineDelete','getItemsDataModel'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionGetItemsDataModel() {
      $product_attributes_get = $_GET['v'];        
       $sql = "SELECT * FROM  tbl_product_attributes where label= '".$product_attributes_get."'";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            $itemList = array();
        foreach($result as $item1){
            $itemList[$item1['id']] = $item1['value'];            
        }
      
        $this->renderPartial('itemlist_bk', array('itemList'=>$itemList));

     }


	public function actionUserView($id)
	{
		if ($_GET['type'] == 3) {
			$model = $this->loadModel($id);
		}
		if ($_GET['type'] == 2) {
			$model = $this->actionUpdate($id);
		}
		if ($_GET['type'] == 1) {
			$model = $this->loadModel($id);
		}
		$this->renderpartial('modal_view_update_delete', array(
			'model' => $model,
			'type' => $_GET['type'],
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new InvoiceLine;
		$invoice = new Invoice;
		$model->created_by = Yii::app()->user->username;
		$model->created_time = new CDbExpression('NOW()');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['InvoiceLine']))
		{
			$model->attributes=$_POST['InvoiceLine'];
			var_dump($model);die();
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->renderpartial('_form',array(
			'model'=>$model,
			'invoice'=>$invoice,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated_by = Yii::app()->user->username;
		$model->updated_time = new CDbExpression('NOW()');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['InvoiceLine']))
		{
			$model->attributes=$_POST['InvoiceLine'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		return $model;
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actioninvoiceLineDelete($id)
	{
		$this->loadModel($id)->delete();
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('InvoiceLine');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new InvoiceLine('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['InvoiceLine']))
			$model->attributes=$_GET['InvoiceLine'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=InvoiceLine::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invoice-line-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
