<?php

class VehiclestatusController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function accessRules()
    {
        return array(
//            array('allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array('test', 'pickAndDropStats', 'marketplaceStats', 'requisitionStats', 'nextDayRequisitions','autoLogin'),
//                'users'=>array('*'),
//            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'findVehicleByParameters', 'getVehicleDetails', 'getVehicleLocation', 'getRequisitionList', 'requisitionDetailView', 'getMainMap', 'getVehiclesWithAssetId'),
                'users' => array('@'),
            ),
            /*array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array(''),
                'users'=>array('admin'),
            ),*/
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }
    //default action: if no action is called then this function will be called by default
	public function actionIndex()
	{
        $this->layout = "//layouts/main_vehicle_status";
        $cs=Yii::app()->clientScript;
        $cs->scriptMap=array(
            'jquery.js'=>false,
            'bootstrap.css'=>false,
            'bootstrap-responsive.css'=>false,
            'bootstrap-yii.css'=>false,
            'jquery-ui-bootstrap.css'=>false,
            'bootstrap.bootbox.min.js'=>false,
            'bootstrap.js'=>false,
            'jquery.yiiactiveform.js'=>false
//            'jquery-1.12.4.js'=>Yii::app()->theme->baseUrl.'/js/jquery-1.12.4.js',
        );
//        $cs->registerScriptFile('jquery-1.12.4.js',CClientScript::POS_END);

        $model = new VehicleSearchModel();
            $condition = "active=1 AND is_special IS TRUE";
        if (Yii::app()->user->department == 'Microfinance'){
            $condition .= " AND allocated_to = 'Microfinance'";
        }

        $vehicles = Vehicles::model()->findAll(array(
            'select'=>'reg_no, driver_pin',
            'condition'=>$condition
        ));
        $this->render('index', array(
		    'model' => $model,
            'vehicles' => $vehicles
        ));
	}

	public function actionFindVehicleByParameters(){

	    $model = new VehicleSearchModel();
	    $model->attributes = $_POST['VehicleSearchModel'];
        $this->performAjaxValidation($model);
	    if (isset($_POST['VehicleSearchModel'])){
            if (!$model->validate()){
                echo
                    '<script type=\"text/javascript\">
                        toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": true,
                            "progressBar": false,
                            "positionClass": "toast-bottom-right",
                            "preventDuplicates": false,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "0",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error("Enter Vehicle Reg No or Driver Pin", "");
                    </script>';
                die;
            }

            $where = "";
            $params = array();
            if (isset($model->vehicle_reg_no)) {
                $where .= $where . "t.reg_no = :vehicle_reg_no";
                $params[':vehicle_reg_no'] = $model->vehicle_reg_no;
            }
            if (isset($model->driver_pin)) {
                if (!empty($where)) $where .= $where . " and ";
                $where .= $where . "t.driver_pin = :driver_pin";
                $params[':driver_pin'] = $model->driver_pin;
            }
            $criteria = new CDbCriteria;
            $criteria->select = 't.*, vt.type';
            $criteria->join = 'LEFT JOIN tbl_vehicletypes vt ON vt.id=t.vehicletype_id';
            $criteria->condition = $where . ' and t.active = 1';
            $criteria->params = $params;
            //$criteria->order = "t.created_time DESC";
            //$criteria->limit=5;
            $vehicles = Vehicles::model()->findAll($criteria);

            $html = $this->renderPartial('vehiclelist', array(
                'vehicles'=>$vehicles
            ));
            //sleep(2);
            echo $html;
            Yii::app()->end();
        }
    }

    public function actionGetVehicleDetails(){
	    if (isset($_POST['vehicle_reg_no'])){
	        $vehicle = Vehicles::model()->findByPk($_POST['vehicle_reg_no']);

	        /*CHECK IF VEHICLE HAS ASSET ID*/
            /*if (is_null($vehicle->asset_id) || strlen($vehicle->asset_id) < 20) {
                echo "<section id='page-title' class='page-title-center'><div class='clearfix'><h1>Vehicle Asset ID not found</h1><span>Please search for another vehicle</span></div></section>";
                die;
            }*/


            $html = $this->renderPartial('vehicledetails', array(
                'vehicle'=>$vehicle
            ));
            //sleep(2);
            echo $html;
            die;

        }
    }

    public function actionGetVehicleLocation () {
//sleep(200);
        $vehicle_data = explode(",", $_POST['vehicle_data']);
        $vreg_no = $vehicle_data[0];
        $vasset_id = $vehicle_data[1];

        /*GET LOCATIONS OF VEHICLE*/
        $vcord = file_get_contents("https://api.finder-lbs.com/api/54f2cb4b421aa9493557fc09/current_location?email=bracblc123@gmail.com&asset=".$vasset_id);
        $result = json_decode($vcord,true);


        $obj = new stdClass();
        $obj->vehicle_reg_no  = $vreg_no;
        if(!isset($result[ 'response']['last_given_location']['coordinates'])) {
            $obj->success = false;
            $obj->message = '<section id=\'page-title\' class=\'page-title-center\'><div class=\'clearfix\'><!--<h1>"Unable to find vehicle co-ordinate from VTS!"</h1>--><span>Please search for another vehicle</span></div></section>';
            echo json_encode($obj);
            die;
        }
        $lat = $result['response']['last_given_location']['coordinates'][0];
        $long = $result['response']['last_given_location']['coordinates'][1];

        $obj->coordinates = $result[ 'response']['last_given_location']['coordinates'][1].','.$result[ 'response']['last_given_location']['coordinates'][0];
//        $obj->message = '<section id=\'page-title\' class=\'page-title-center\'><div class=\'clearfix\'><h1>Vehicle Asset ID not found</h1><span>Please search for another vehicle</span></div></section>';
        $obj->success = true;
        echo json_encode($obj);
        die;
    }

    public function actionGetRequisitionList () {

        $vehicle_data = explode(",", $_POST['vehicle_data']);
        $vreg_no = $vehicle_data[0];
        $vasset_id = $vehicle_data[1];

        /*GET LOCATIONS OF VEHICLE*/
        /*$criteria = new CDbCriteria;
        $criteria->select = 't.*';
        $criteria->join = 'JOIN tbl_requisitions r ON r.vehicle_reg_no = t.reg_no';
        $criteria->condition = 't.reg_no = ":vehicle_reg_no" AND r.active = 2 AND r.start_date BETWEEN DATE(\'2019-06-10\') AND DATE_ADD(DATE(\'2019-06-10\'), INTERVAL 7 DAY)';
        $criteria->params = array(':vehicle_reg_no' => $vreg_no);

        $requisitions = Vehicles::model()->findAll($criteria);*/

        $sql = "SELECT v.*, r.start_time, r.end_time, r.start_date, r.end_date, r.start_point, r.end_point, r.id as req_id, d.type_name 
                FROM tbl_vehicles v
                JOIN ifleetdb.tbl_requisitions r ON r.vehicle_reg_no = v.reg_no  
                JOIN ifleetdb.tbl_dutytypes d ON d.id = r.dutytype_id 
                WHERE v.reg_no = '".$vreg_no."' AND r.active = 2 AND r.start_date BETWEEN current_date() and DATE_ADD(current_date(), INTERVAL +7 DAY)";
        $rawData = Yii::app()->db->createCommand($sql);
        //$rawData->bindParam(":vehicle_reg_no", $vreg_no, PDO::PARAM_STR);
        $requisitions = $rawData->queryAll();
        //return json_encode(var_dump($requisitions));die;
        $html =
            '<table class="table table-striped">
            <thead>
            <tr>
                <th>Timings</th>
                <th>Destination</th>
                <th>Duty Type</th>
                <th></th>
            </tr>
            </thead>
            <tbody>';
        if (count($requisitions) == 0) {
            $html .=
                '<tr><td colspan="4"><span class="">No requisition found</span></td></tr>';
        }
        foreach ($requisitions as $requisition){
            $requisition_id = $requisition['req_id'];
            $html .=
                '<tr>
                    <td><span class="label label-danger">'.$requisition['start_date'].' '.$requisition['start_time'].'</span></td>
                    <td>'.$requisition['end_point'].'</td>
                    <td>'.$requisition['type_name'].'</td>
                    <td>'.
                        CHtml::link(
                            'View',
                            "javascript:void(0)",
                            array(
                                "onclick"=>"requisitionDetail($requisition_id)",
                                "data-id"=>"$requisition_id",
                                "id"=>"myModalButton",
                                "role" =>"button",
                                "class"=>"btn btn-primary btn-sm btn-outline-info"
                            )
                        ).
                    '</td>
                </tr>';
        }
        $html .= '</tbody></table>';
        echo $html;
        die;
    }

    public function actionRequisitionDetailView(){
        $model = Requisitions::model()->findByPk($_GET["id"]);
        echo $this->renderPartial('requisitiondetails', array(
            'model' => $model,
        ));
        die;
    }

    public function actionGetMainMap() {
        echo $this->renderPartial('mainmap');
        die;
    }

    public function actionGetVehiclesWithAssetId() {
        $sql = "SELECT * FROM tbl_vehicles WHERE CHAR_LENGTH(asset_id) > 20 AND is_special IS TRUE ";
        if (Yii::app()->user->department == 'Microfinance'){
            $sql .= " AND allocated_to = 'Microfinance'";
        }
        $vehicles = Yii::app()->db->createCommand($sql)->queryAll();
        echo json_encode($vehicles);
        die;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='vehicle-search-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}