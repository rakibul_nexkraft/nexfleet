<?php

class SmsservicesController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
		'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('service','testmail'),
		'users'=>array('*'),
		),

		array('allow',  // allow all users to perform 'index' and 'view' actions
		'actions'=>array('create','index','view'),
		'users'=>array('@'),
		),


		array('allow', // allow authenticated user to perform 'create' and 'update' actions
		'actions'=>array('update'),
		'users'=>array('@'),
		),
		array('allow', // allow admin user to perform 'admin' and 'delete' actions
		'actions'=>array('admin','delete'),
		//'users'=>array('admin'),
		'expression'=>'Yii::app()->user->isAdmin()',
		),
		array('deny',  // deny all users
		'users'=>array('*'),
		),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Smsservices;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Smsservices']))
		{
			$model->attributes=$_POST['Smsservices'];
			$route_no = $_POST['Smsservices']['sent_to'];

			$message  = $_POST['Smsservices']['message'].", ICT";

			if($route_no=='securityinfo')
			{
				$message  = $_POST['Smsservices']['message'];
				$user_cell[] = "+8801730329494";
				$user_cell[] = "+8801730781930";
				$user_cell[] = "+8801714078866";
				$user_cell[] = "+8801713042719";
				$user_cell[] = "+8801713000499";
				$user_cell[] = "+8801729070459";
				$user_cell[] = "+8801713041862";
				$user_cell[] = "+8801730781934";
				$user_cell[] = "+8801713366609";
				$user_cell[] = "+8801713000857";
				$user_cell[] = "+8801713031670";
				$user_cell[] = "+8801711543522";
				$user_cell[] = "+8801713037743";
				$user_cell[] = "+8801714091214";
				$user_cell[] = "+8801713082772";
				$user_cell[] = "+8801711404572";
				$user_cell[] = "+8801713333101";
				$user_cell[] = "+8801730781933";
				$user_cell[] = "+8801713046681";
				$user_cell[] = "+8801730781932";
				$user_cell[] = "+8801729071548";
				$user_cell[] = "+8801713082771";
				$user_cell[] = "+8801711837746";				
				$user_cell[] = "+8801714091458";
				$user_cell[] = "+8801711404576";
				$user_cell[] = "+8801713069640";
				$user_cell[] = "+8801730784020";
				$user_cell[] = "+8801729071072";
				$user_cell[] = "+8801783586141";
				$user_cell[] = "+8801730329494";
				$user_cell[] = "+8801713041394";
				$user_cell[] = "+8801742089259";
				$user_cell[] = "+8801729070684";
				$user_cell[] = "+8801711055082";
				$user_cell[] = "+8801729071488";
				$user_cell[] = "+8801730351386";
				$user_cell[] = "+8801730346752";				
				$user_cell[] = "+8801755692045";
				$user_cell[] = "+8801730348989";
				$user_cell[] = "+8801729071725";
				$user_cell[] = "+8801730350930";
				$user_cell[] = "+8801730346693";
				$user_cell[] = "+8801730349924";
				$user_cell[] = "+8801730351212";
				$user_cell[] = "+8801730350175";
				$user_cell[] = "+8801730347083";
				$user_cell[] = "+8801730351042";
				$user_cell[] = "+8801730347763";
				$user_cell[] = "+8801730346197";
				$user_cell[] = "+8801730347036";
				$user_cell[] = "+8801730350520";
				$user_cell[] = "+8801730350293";
				$user_cell[] = "+8801730350700";
				$user_cell[] = "+8801730350979";
				$user_cell[] = "+8801730347767";
				$user_cell[] = "+8801714106817";
				$user_cell[] = "+8801730351291";
				$user_cell[] = "+8801730346517";
				$user_cell[] = "+8801730347956";
				$user_cell[] = "+8801730350524";
				$user_cell[] = "+8801730348186";
				$user_cell[] = "+8801730329569";
				$user_cell[] = "+8801730349986";
				$user_cell[] = "+8801730347747";
				$user_cell[] = "+8801730348843";
				$user_cell[] = "+8801730348665";
				$user_cell[] = "+8801730345872";
				$user_cell[] = "+8801730349897";
				$user_cell[] = "+8801730349937";
				$user_cell[] = "+8801730350542";
				$user_cell[] = "+8801730347440";
				$user_cell[] = "+8801730349935";
				$user_cell[] = "+8801730348211";
				$user_cell[] = "+8801730346986";
				$user_cell[] = "+8801730348162";
				$user_cell[] = "+8801714091364";
				$user_cell[] = "+8801730351076";
				$user_cell[] = "+8801730350054";
				$user_cell[] = "+8801730348176";
				$user_cell[] = "+8801730321719";
				$user_cell[] = "+8801730351039";
				$user_cell[] = "+8801730321875";
				$user_cell[] = "+8801730346312";
				$user_cell[] = "+8801730346079";
				$user_cell[] = "+8801730347304";
				$user_cell[] = "+8801730350687";
				$user_cell[] = "+8801730348422";
				$user_cell[] = "+8801730351055";
				$user_cell[] = "+8801730350557";
				$user_cell[] = "+8801730350670";
				$user_cell[] = "+8801730350587";
				$user_cell[] = "+8801730348776";
				$user_cell[] = "+8801730348092";
				$user_cell[] = "+8801729070020";
				$user_cell[] = "+8801730350824";
				$user_cell[] = "+8801730350325";
				$user_cell[] = "+8801730346761";
				$user_cell[] = "+8801730321521";
				$user_cell[] = "+8801730350077";
				$user_cell[] = "+8801730321561";
				$user_cell[] = "+8801729071100";
				$user_cell[] = "+8801730350421";
				$user_cell[] = "+8801729071175";
				$user_cell[] = "+8801730349704";
                                $user_cell[] = "+8801711863297";
                                
				
				
			}
			else
			{

				$sql = "SELECT user_cell FROM tbl_commonfleets WHERE present_route =  '$route_no' and application_type='Pick and Drop' and approve_status='Approve'";

				$command = Yii::app()->db->createCommand($sql);
				$route_no = $command->queryAll();

				foreach($route_no as $item=>$val)
				{

					if(strlen($val['user_cell'])==10)$user_cell[] = "+880".$val['user_cell'];
					else $user_cell[] = $val['user_cell'];
				}
			}


			try{
				//$soapClient = new SoapClient("http://172.25.100.41:8080/isoap.comm.smsc/SMSCService?wsdl"
				$soapClient = new SoapClient("http://172.26.4.79:8080/smsc/SMSCService?WSDL"
				//array('proxy_host'=> "",
				//'proxy_port'    => ,
				//'proxy_login'    => "",
				//'proxy_password' => "")
				);

				//$username = array('userName'=>"",'password'=>"",'message'=>array('message'=>$message,'numberList'=>$user_cell,'gateway'=>'MIT','mask'=>'BRAC','client'=>'ifleet','fromnumber'=>'+8801711057908'));
				$username = array('userName'=>"",'password'=>"",'message'=>array('message'=>$message,'numberList'=>$user_cell,'gateway'=>'iFleet','mask'=>'BRAC','fromnumber'=>'+8801711057908'));

				$send_sms=$soapClient->__call('sendSms',array($username));

			}
			catch (SoapFault $fault) {
				$error = 1;
				print($fault->faultcode."-".$fault->faultstring);
			}



			$model->sent_by = Yii::app()->user->username;
			$model->sent_time = new CDbExpression('NOW()');

			if($model->save())
			{
				if($route_no=='securityinfo')
				{
						Yii::app()->mailer->Host = '172.25.100.113';
						Yii::app()->mailer->IsSMTP();
						Yii::app()->mailer->From = 'ifleet@brac.net';
						Yii::app()->mailer->FromName = 'iFleet';												
						Yii::app()->mailer->AddAddress("syed.ekramuddin@brac.net");
						Yii::app()->mailer->AddAddress("riad.ak@brac.net");
						Yii::app()->mailer->AddAddress("saiful.raju@brac.net");
						//Yii::app()->mailer->AddAddress("rpronet84@gmail.com");
						
						//Yii::app()->mailer->AddAddress("riad06@gmail.com");
						Yii::app()->mailer->Subject = 'New Security Alert Submitted';
						Yii::app()->mailer->Body = "FYI";
						Yii::app()->mailer->Send();
					}
						
						$this->redirect(array('view','id'=>$model->id));
			}

		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionService()
	{


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);


		$route_no1 = $_REQUEST['param1'];
		$message  = $_REQUEST['param2'].", ICT";


		Yii::log("Error ".$route_no1.$message, 'info', 'application');



		if(strcmp($route_no1,'G1')==0 || strcmp($route_no1,'G2')==0)
		{

			if(strcmp($route_no1,'G1')==0)
			{
				$j=1;$k=31;
			}



			elseif(strcmp($route_no1,'G2')==0)
			{
				$j=31;$k=39;
			}


			for($i=$j;$i<=$k;$i++)


			{
				$sql = "SELECT user_cell FROM tbl_commonfleets WHERE present_route =  $i and application_type='Pick and Drop' and approve_status='Approve'";

				$command = Yii::app()->db->createCommand($sql);
				$route_no1 = $command->queryAll();
				$route_arr[] =$route_no1;
			}

			foreach($route_arr as $item=>$val)
			{
				foreach($val as $item1=>$val1)
				{

					if($val1['user_cell']!=0)
					{
						if(strlen($val1['user_cell'])==10)$user_cell[] = "+880".$val1['user_cell'];
						else $user_cell[] = $val1['user_cell'];
					}
				}
			}
		}






		if(is_numeric($route_no1))
		{
			//$sql = "SELECT user_cell FROM tbl_commonfleets WHERE preferred_route =  '$route_no1'";
			$sql = "SELECT user_cell FROM tbl_commonfleets WHERE present_route =  '$route_no' and application_type='Pick and Drop' and approve_status='Approve'";


			$command = Yii::app()->db->createCommand($sql);
			$route_no = $command->queryAll();

			foreach($route_no as $item=>$val)
			{

				if($val['user_cell']!=0)
				{

					if(strlen($val['user_cell'])==10)$user_cell[] = "+880".$val['user_cell'];
					else $user_cell[] = $val['user_cell'];
				}
			}
		}


		try{
			//$soapClient = new SoapClient("http://192.168.2.39:8080/isoap.comm.smsc/SMSCService?wsdl"

			$soapClient = new SoapClient("http://172.26.4.79:8080/smsc/SMSCService?WSDL"

			//$soapClient = new SoapClient("http://172.25.100.41:8080/isoap.comm.smsc/SMSCService?wsdl"
			//array('proxy_host'=> "",
			//'proxy_port'    => ,
			//'proxy_login'    => "",
			//'proxy_password' => "")
			);



			$username = array('message'=>array('message'=>$message,'numberList'=>$user_cell,'gateway'=>'iFleet','mask'=>'BRAC','fromnumber'=>'+8801711057908'));

			$send_sms=$soapClient->__call('sendSms',array($username));



		}
		catch (SoapFault $fault) {
			$error = 1;
			print($fault->faultcode."-".$fault->faultstring);
		}


		$model=new Smsservices;
		$model->sent_to = $route_no1;
		$model->message = $message;

		$model->sent_by = 'SMS';
		$model->sent_time = new CDbExpression('NOW()');

		$model->save();




	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Smsservices']))
		{
			$model->attributes=$_POST['Smsservices'];
			if($model->save())
			$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
		'model'=>$model,
		));
	}
	
	public function actionTestmail()
	{							
						Yii::app()->mailer->Host = '172.25.100.113';
						Yii::app()->mailer->IsSMTP();
						Yii::app()->mailer->From = 'ifleet@brac.net';
						Yii::app()->mailer->FromName = 'iFleet';												
						Yii::app()->mailer->AddAddress("syed.ekramuddin@brac.net");
						Yii::app()->mailer->AddAddress("riad.ak@brac.net");
						//Yii::app()->mailer->AddAddress("rpronet84@gmail.com");
						//Yii::app()->mailer->AddAddress("saiful.raju@brac.net");
						//Yii::app()->mailer->AddAddress("riad06@gmail.com");
						Yii::app()->mailer->Subject = 'New Security Alert Submitted';
						Yii::app()->mailer->Body = "FYI";
						Yii::app()->mailer->Send();
				
	}


	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Smsservices');
		$this->render('index',array(
		'dataProvider'=>$dataProvider,
		));
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Smsservices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Smsservices']))
		$model->attributes=$_GET['Smsservices'];

		$this->render('admin',array(
		'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer the ID of the model to be loaded
	*/
	public function loadModel($id)
	{
		$model=Smsservices::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param CModel the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='smsservices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
