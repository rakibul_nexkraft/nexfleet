<?php

class VehiclesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','callHrUserForHelper'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('getRegNo','callHrUser','excel','allRegNo1'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','admin','update','delete','deactivate'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $reg_no the Reg No of the model to be displayed
	 */
	public function actionView($reg_no)
	{
  		$this->render('view',array(
			'model'=>$this->loadModel($reg_no),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Vehicles;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Vehicles']))
		{
			//$model->attributes=$_POST['Vehicles'];

            $getname = Yii::app()->user->username;
            $_POST['Vehicles']['created_by'] = $getname;

            $model->attributes=$_POST['Vehicles'];
            $model->save();

			if($model->save())
				$this->redirect(array('view','reg_no'=>$model->reg_no));
        }



        // create another model instance
        /*$b=new VehiclesLog;
        $b->attributes=$model->attributes;
        $b->vehicle_reg_no=$model->reg_no;

        if($model->save()){
            if($b->save())
                $this->redirect(array('view','reg_no'=>$model->reg_no));
        }*/


		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $reg_no the Reg No of the model to be updated
	 */
	public function actionUpdate($reg_no)
	{
		$model=$this->loadModel($reg_no);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Vehicles']))
		{
                    $model->attributes=$_POST['Vehicles'];
                    $b=new VehiclesLog;
                    $b->attributes = $model->attributes;
                    $b->vehicle_reg_no = $model->reg_no;
                    $b->created_time = $model->created_time;
                    $b->updated_by = Yii::app()->user->username;
                    $b->insert();
                    if($model->save())
                        $this->redirect(array('view','reg_no'=>$model->reg_no));
			/*if($model->save())
				$this->redirect(array('view','reg_no'=>$model->reg_no));*/
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $reg_no the Reg No of the model to be deleted
	 */
	public function actionDelete($reg_no)
	{
		throw new CHttpException(400,'Delete operation is stopped.');
        return;

        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
            $this->loadModel($reg_no)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');


	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*
		$dataProvider=new CActiveDataProvider('Vehicles');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		*/

		$model=new Vehicles('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Vehicles']))
			$model->attributes=$_GET['Vehicles'];
		
		$this->render('index',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Vehicles('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Vehicles']))
			$model->attributes=$_GET['Vehicles'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the Reg No of the model to be loaded
	 */
	public function loadModel($reg_no)
	{
		$model=Vehicles::model()->findByPk($reg_no);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='vehicles-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionGetRegNo() {
	  if (!empty($_GET['term'])) {
	  	$qterm = '%'.$_GET['term'].'%';
	 	$sql = "SELECT v.reg_no as value,v.driver_pin,v.driver_name,v.vehicletype_id,v.location,v.vmodel,v.engine_no,b.phone FROM tbl_vehicles as v
                        LEFT JOIN tbl_drivers as b ON b.pin=v.driver_pin   WHERE reg_no LIKE '$qterm' AND v.active=1";
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();

		$count=0;
		foreach($result as $item)		
		{
		
		$vehicle_reg_no = $item['value'];
		
//if ($vehicle_reg_no == 'DM-SA-11-0127')
            /*if ($vehicle_reg_no == 'DM-JHA-14-0020'){
                $sql = "SELECT end_meter as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no' order by id desc limit 1";
                $command = Yii::app()->db->createCommand($sql);
                $result1 = $command->queryAll();
            }
            */
			if ($vehicle_reg_no == 'DM-SA-11-0127'){
                $sql = "SELECT end_meter as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no' order by id desc limit 1";
                $command = Yii::app()->db->createCommand($sql);
                $result1 = $command->queryAll();
            }
else{
		$sql = "SELECT max(end_meter) as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no'";					 			
		$command = Yii::app()->db->createCommand($sql);				
		$result1 = $command->queryAll();
}
		//print_r($result1);
		if($result1[0]['max_meter'])
		$result[$count]['start_meter'] = $result1[0]['max_meter'];
		else
		$result[$count]['start_meter'] = 0;
		$count++;
	}
		
		echo CJSON::encode($result); exit;
	  } else {
		return false;
	  }
	}

    public function actionAllRegNo1() {
        if (!empty($_GET['term'])) {
            $qterm = '%'.$_GET['term'].'%';
            $sql = "
(SELECT p.reg_no as value,p.driver_pin,p.driver_name,p.vehicletype_id,p.location,p.vmodel,p.engine_no FROM tbl_vehiclesisters as p
WHERE p.reg_no LIKE '$qterm'
)
UNION
(SELECT v.reg_no as value,v.driver_pin,v.driver_name,v.vehicletype_id,v.location,v.vmodel,v.engine_no FROM tbl_vehicles as v
                        LEFT JOIN tbl_drivers as b ON b.pin=v.driver_pin   WHERE v.reg_no LIKE '$qterm' AND v.active=1)

";

/*
            $sql = "SELECT v.reg_no as value,v.driver_pin,v.driver_name,v.vehicletype_id,v.location,v.vmodel,v.engine_no,b.phone FROM tbl_vehicles as v
                        LEFT JOIN tbl_drivers as b ON b.pin=v.driver_pin   WHERE reg_no LIKE '$qterm' AND v.active=1";
*/
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            $count=0;
            foreach($result as $item)
            {

                $vehicle_reg_no = $item['value'];


                $sql = "SELECT max(end_meter) as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no'";
                $command = Yii::app()->db->createCommand($sql);
                $result1 = $command->queryAll();

                //print_r($result1);
                if($result1[0]['max_meter'])
                    $result[$count]['start_meter'] = $result1[0]['max_meter'];
                else
                    $result[$count]['start_meter'] = 0;
                $count++;
            }

            echo CJSON::encode($result); exit;
        } else {
            return false;
        }
    }

	
	public function actionCallHrUser()
	{
			try{
          
				$soapClient = new SoapClient("http://172.25.100.86:800/StaffInfo.asmx?wsdl",
																			array(                     
																				'proxy_login'    => "",
																		 		'proxy_password' => ""
																		 		)
																		 );


				if($_REQUEST['Commonfleets'])
					$user_pin = $_REQUEST['Commonfleets']['user_pin'];
					
				elseif($_REQUEST['Cardistributions'])
					$user_pin = $_REQUEST['Cardistributions']['user_pin'];
					
		    elseif($_REQUEST['Wssnacks'])
		    	$user_pin = $_REQUEST['Wssnacks']['user_pin'];
		    	
		    elseif($_REQUEST['Wsious'])
		    	$user_pin = $_REQUEST['Wsious']['user_pin'];
		    	
		    elseif($_REQUEST['Defects'])
		    	$user_pin = $_REQUEST['Defects']['mechanic_pin'];
		    	
		    elseif($_REQUEST['Wsitemdists'])
		    	$user_pin = $_REQUEST['Wsitemdists']['mechanic_pin'];
		    	

				if(!$user_pin)
					$user_pin = $_REQUEST['Requisitions']['user_pin'];
				
					$StaffPIN=array('strStaffPIN'=>$user_pin);    	
    			$strStaffINFO=$soapClient->__call('StaffInfoByPIN', array($StaffPIN));    	
     	     
    			$varb = CJSON::decode($strStaffINFO->StaffInfoByPINResult);             
				  
					echo CJSON::encode($varb[0]); exit;
  	}
    
		catch (SoapFault $fault) 
		{
    	$error = 1;
    	print($fault->faultcode."-".$fault->faultstring);
		}
			
	}
	
	public function actionCallHrUserForHelper()
	{
		try{

			$soapClient = new SoapClient("http://172.25.100.86:800/StaffInfo.asmx?wsdl",
			array(			
			'proxy_login'    => "",
			'proxy_password' => ""));
	  
		  $user_pin = $_REQUEST['Vehicles']['helper_pin'];
		
	    $StaffPIN=array('strStaffPIN'=>$user_pin);	    
	    $strStaffINFO=$soapClient->__call('StaffInfoByPIN', array($StaffPIN));
	     
	    $varb = CJSON::decode($strStaffINFO->StaffInfoByPINResult);        
	      
	    echo CJSON::encode($varb[0]); exit;
	       
  	}
		
		catch (SoapFault $fault) {
		    $error = 1;
		    print($fault->faultcode."-".$fault->faultstring);
		}
			
	}
	
	public function actionExcel()
	{
		$model=new Vehicles('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['criteria']))
			$model->attributes=$_GET['criteria'];
		
		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Vehicles',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Vehicles',
			'columns'=>array(
				array(
					'name' => 'reg_no',
					'type'=>'raw',
					'value' => 'CHtml::link(CHtml::encode($data->reg_no),array("view","reg_no"=>$data->reg_no))',
				),
				array(
					'name' => 'vehicletype_id',
					'type'=>'raw',
					'value' => '$data->vehicletypes->type',
				),
				'cc',
				'vmodel',
				'chassis_no',
				'country',
				'purchase_date',
//				'purchase_price',
				'supplier',
				'seat_capacity',
				'load_capacity',
				'driver_pin',
                'location',
			),
		));
		Yii::app()->end();
	}
	
	public function actionDeactivate($reg_no)
	{
		if(Yii::app()->request->isPostRequest)
                {
                    $model = $this->loadModel($reg_no);
                    if($model->active == 1)
                    {
                        $model->active = 2;
                        $mdl_auction = new Auctions;
                        $mdl_auction->vehicletype_id = $model->vehicletype_id;
                        $mdl_auction->vehicle_reg_no = $reg_no;
                        $mdl_auction->created_time = new CDbExpression('NOW()');
                        $mdl_auction->created_by = Yii::app()->user->username;
                        $mdl_auction->active = 1;
                        $mdl_auction->insert();
                        //$mdl_auction->save();
                    }
                    elseif($model->active == 2)
                    {
                        $model->active = 1;
                        $mdl_auction = Auctions::model()->find('vehicle_reg_no=:reg_no', array(':reg_no'=>$reg_no));
                        if($mdl_auction->upload_scan)
                            unlink(Yii::getPathOfAlias('webroot')."/images/scandoc/$mdl_auction->vehicle_reg_no/".$mdl_auction->upload_scan);
                        $mdl_auction->delete();
                    }
                    if($model->save())
                    {
                        $log=new VehiclesLog;
                        $log->attributes = $model->attributes;
                        $log->vehicle_reg_no = $model->reg_no;
                        $log->updated_time = new CDbExpression('NOW()');
                        $log->updated_by = Yii::app()->user->username;
                        $log->insert();
                    }
                    
                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : Yii::app()->request->urlReferrer);
                }
                else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');


	}
}
