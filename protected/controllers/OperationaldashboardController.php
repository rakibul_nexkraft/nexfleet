<?php

class OperationaldashboardController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionTopDataAvailable(){
        $sql = "SELECT t.`type`, COUNT(*) AS countNo FROM `tbl_vehicles` v JOIN `tbl_vehicletypes` t ON v.`vehicletype_id`=t.`id` WHERE v.reg_no NOT IN (SELECT vehicle_reg_no FROM tbl_requisitions WHERE start_date<= CURRENT_DATE AND end_date>=CURRENT_DATE AND approve_status='YES') AND v.`active`=1 GROUP BY v.`vehicletype_id` ORDER BY v.`vehicletype_id` ASC ";

        $command_top = Yii::app()->db->createCommand($sql);
        $result = $command_top->queryAll();
        $data         = array();
        foreach($result as $key => $value){
            $data[$key] = $value;
        }

        echo json_encode($result);
    }
    public function actionTopDataOnDuty(){
        $sql = "SELECT vt.type, COUNT(*) AS countNo
                FROM tbl_vehicles v
                LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no
                LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id
                WHERE r.`start_date`<= CURRENT_DATE AND r.`end_date`>=CURRENT_DATE AND v.active=1
                GROUP BY v.`vehicletype_id` ORDER BY v.`vehicletype_id` ASC";
        $command_top = Yii::app()->db->createCommand($sql);
        $result = $command_top->queryAll();
        echo json_encode($result);
    }
    public function actionTopDataWorkshop(){
        $sql = "SELECT t.type, COUNT(*) AS countNo FROM `tbl_defects` d JOIN `tbl_deliveryvehicles` dv ON dv.`defect_id`=d.`id` JOIN `tbl_vehicletypes` t ON t.`id`=d.`vehicletype_id` WHERE dv.`delivery_date`>CURRENT_DATE GROUP BY d.`vehicletype_id` ORDER BY d.`vehicletype_id` ASC";
        $command_top = Yii::app()->db->createCommand($sql);
        $result = $command_top->queryAll();
        echo json_encode($result);
    }
    public function actionTopDataStaffPickDrop(){
        $sql = "SELECT t.`type`, COUNT(DISTINCT c.vehicle_reg_no) AS countNo FROM tbl_commonfleets c JOIN `tbl_vehicletypes` t ON c.`vehicletype_id`=t.`id` WHERE c.application_type='Pick and Drop' AND c.approve_status='Approve' GROUP BY c.`vehicletype_id` ORDER BY c.`vehicletype_id` ASC ";
        $command_top = Yii::app()->db->createCommand($sql);
        $result = $command_top->queryAll();
        echo json_encode($result);
    }

    public function actionSearchByCriteria(){
        try{
            $total_time = 24;
            $offset = 0;
            $time_start = 0;
            $time_end = 0;
            $time_selected = 0;

            $search_type = $_GET['searchType'];
            $search_date = $_GET['searchDate'];
            $vehicle_type = $_GET['vehicleType']?$_GET['vehicleType']:"";

            if (!empty($search_type) || !empty($search_date)) {
                if($search_type == "onduty" || $search_type == "available"){
                    $vehiclesql = "
                    SELECT v.reg_no as vehicle_reg_no, v.vehicletype_id, v.active, r.id, r.start_date, r.end_date, r.start_time, r.end_time, vt.type 
                    FROM tbl_vehicles v 
                    LEFT JOIN tbl_requisitions r ON v.reg_no=r.vehicle_reg_no";
                    $leftjoin = " LEFT JOIN tbl_vehicletypes vt ON v.vehicletype_id=vt.id ";


                    $date_criteria = " r.start_date<='$search_date' AND r.end_date>='$search_date' ";
                    $where = " WHERE v.active=1 ";
                    if($search_type == "available") {
                        $date_criteria = " AND " . $date_criteria;
                        $where = $where . " and r.start_date is null ";
                    }else{
                        $date_criteria  = "";
                        $where = $where . " AND r.start_date<='$search_date' AND r.end_date>='$search_date' ";
                    }
                    $vehiclesql = $vehiclesql . $date_criteria . $leftjoin . $where . " ORDER BY v.reg_no ASC";

                    $command_top = Yii::app()->db->createCommand($vehiclesql);
                    $result = $command_top->queryAll();

                    $html = "<table class='table table-bordered dTableHead fixed_header'><thead class='fixed_header_thead'>";
                    $html .=
                        "<tr>".
                        "<th style='width:17%'>Vehicle Reg No</th>".
                        "<th style='width:7%'>09AM</th>".
                        "<th style='width:7%'>10AM</th>".
                        "<th style='width:7%'>11AM</th>".
                        "<th style='width:7%'>12PM</th>".
                        "<th style='width:7%'>01PM</th>".
                        "<th style='width:7%'>02PM</th>".
                        "<th style='width:7%'>03PM</th>".
                        "<th style='width:7%'>04PM</th>".
                        "<th style='width:7%'>05PM</th>".
                        "<th style='width:7%'>06PM</th>".
                        "<th style='width:13%'>Type</th>".
                        "</tr></thead><tbody class='fixed_header_tbody'><tr><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 17%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 13%;'></td></tr>";
                    $oldVId = "";
                    $newVId = "";
                    foreach ($result as $key => $item) {
                        // Initial tr and td here
                        $count = 0;
                        $itemId = $item['id'];
                        $html .= "<tr><td style='text-align: center'>" .
                            "<a class='button' title='Click here for details' onclick='getRequisitionDetail($itemId)' data-id='$itemId' id='myModalButton' data-toggle='modal' data-target='#myModal' style='cursor: hand;' href='#'>" .
                            "" . $item['vehicle_reg_no'] . "</a></td>";

                        $sHour = explode(":", str_replace(".",":", $item['start_time']) )[0];
                        if ($item['start_date'] < $search_date || ($sHour < 9)) $sHour = 9;

                        $eHour = explode(":", str_replace(".",":", $item['end_time']) )[0];
                        $eHour = $eHour + 12 > 24 ? $eHour : $eHour + 12;
                        if ($item['end_date'] > $search_date || $eHour >= 18) $eHour = 18;

                        if($search_type == "onduty") {
                            for ($i = 9; $i < $sHour; $i++) {
                                $html .= "<td style='width: 100px;border: 1px solid #DEDEDE;background-color: #FFFFFF'></td>";
                                $count++;
                                if ($count == 10) break;
                            }

                            for ($i = $sHour; $i <= $eHour; $i++) {
                                $html .= "<td style='width: 100px;border: 1px solid #DEDEDE;background-color: #C3c3c3'></td>";
                                $count++;
                                if ($count == 10) break;
                            }

                            for ($i = $eHour + 1; $i < 19; $i++) {
                                $html .= "<td style='width: 100px;border: 1px solid #DEDEDE;background-color: #FFFFFF'></td>";
                                $count++;
                                if ($count == 10) break;
                            }
                        }else{
                            for ($i = 9; $i < 19; $i++) {
                                $html .= "<td style='width: 100px;border: 1px solid #DEDEDE;background-color: #FFFFFF'></td>";
                            }
                        }
                        $html .= "<td><p style='font-size: 1.1em;text-align: center'>" . $item['type'] . "</p></td></tr>";
                    }
                    $html .= "</tbody></table>";
                    echo $html;die;


                }elseif($_GET["searchType"] == "workshop"){

                    if (!empty($search_date)){
                        $dateType = " (dv.delivery_date>='$search_date') ";
                    }

                    $sql =  "SELECT d.id, d.vehicle_reg_no AS vehicle_reg_no, d.apply_date AS start_date, dv.delivery_date AS end_date, vt.type AS type ".
                        " FROM tbl_defects d ".
                        " JOIN tbl_deliveryvehicles dv ON dv.defect_id = d.id  JOIN tbl_vehicletypes vt ON d.vehicletype_id=vt.id ".
                        " WHERE dv.delivery_date>= '".$search_date."' ORDER BY dv.delivery_date DESC";

                    $command_top = Yii::app()->db->createCommand($sql);
                    $result = $command_top->queryAll();

                    $html = "<table class='table table-bordered dTableHead fixed_header'><thead class='fixed_header_thead'>";
                    $html .=
                        "<tr>".
                        "<th style='width:17%'>Vehicle Reg No</th>".
                        "<th style='width:7%'>09AM</th>".
                        "<th style='width:7%'>10AM</th>".
                        "<th style='width:7%'>11AM</th>".
                        "<th style='width:7%'>12PM</th>".
                        "<th style='width:7%'>01PM</th>".
                        "<th style='width:7%'>02PM</th>".
                        "<th style='width:7%'>03PM</th>".
                        "<th style='width:7%'>04PM</th>".
                        "<th style='width:7%'>05PM</th>".
                        "<th style='width:7%'>06PM</th>".
                        "<th style='width:13%'>Type</th>".
                        "</tr></thead><tbody class='fixed_header_tbody'><tr><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:17%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:13%'></td></tr>";
                    if(count($result)>0){
                        foreach ($result as $key => $item){
                            $itemId = $item['id'];
                            $html .=
                                "<tr><td style='text-align: center;vertical-align: middle'>".
                                "<a class='button' title='Click here for details' onclick='getRequisitionDetail($itemId)' data-id='$itemId' id='myModalButton' data-toggle='modal' data-target='#myModal' style='cursor: hand;' href='#'>".
                                $item['vehicle_reg_no']."</a></td>";

                            for ($i=9;$i<19;$i++){
                                $html .= "<td style=' border: 1px solid #DEDEDE;background-color: #C3C3C3'></td>";
                            }
                            $html .= "<td><p style='font-size: 1.1em;text-align: center'>".$item['type']."</p></td></tr><tr><td style=' border: 0;height: 1px;padding: 0;margin: 0;' colspan='12'></td></tr>";
                        }
                    }else{
                        $html .= "<tr><td colspan='12'>No Data Found</td></tr>";
                    }

                    $html .= "</tbody></table>";
                    echo $html;die;
                }elseif($_GET["searchType"] == "staffpickdrop"){
                    $sql = "SELECT c.id, c.vehicle_reg_no AS vehicle_reg_no, vt.type AS type ".
                        " FROM tbl_commonfleets c ".
                        " JOIN tbl_vehicletypes vt ON c.vehicletype_id = vt.id ".
                        " WHERE c.application_type='Pick and Drop' AND c.approve_status='Approve' AND c.vehicle_reg_no<>'' ";
                    $command_top = Yii::app()->db->createCommand($sql);
                    $result = $command_top->queryAll();

                    $html = "<table class='table table-bordered dTableHead fixed_header'><thead class='fixed_header_thead'>";
                    $html .=
                        "<tr>".
                        "<th style='width:17%'>Vehicle Reg No</th>".
                        "<th style='width:7%'>09AM</th>".
                        "<th style='width:7%'>10AM</th>".
                        "<th style='width:7%'>11AM</th>".
                        "<th style='width:7%'>12PM</th>".
                        "<th style='width:7%'>01PM</th>".
                        "<th style='width:7%'>02PM</th>".
                        "<th style='width:7%'>03PM</th>".
                        "<th style='width:7%'>04PM</th>".
                        "<th style='width:7%'>05PM</th>".
                        "<th style='width:7%'>06PM</th>".
                        "<th style='width:13%'>Type</th>".
                        "</tr></thead><tbody class='fixed_header_tbody'><tr><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:17%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:7%'></td><td style=' border: 0;height: 1px;padding: 0;margin: 0;width:13%'></td></tr>";
                    if(count($result)>0){
                        foreach ($result as $key => $item){
                            $itemId = $item['id'];
                            $html .=
                                "<tr><td style='text-align: center;vertical-align: middle'>".
                                "<a class='button' title='Click here for details' onclick='getRequisitionDetail($itemId)' data-id='$itemId' id='myModalButton' data-toggle='modal' data-target='#myModal' style='cursor: hand;' href='#'>".
                                $item['vehicle_reg_no']."</a></td>";

                            for ($i=9;$i<19;$i++){
                                if($i==9){
                                    $html .= "<td style='border: 1px solid #DEDEDE;background-color: #C3C3C3'></td>";
                                }elseif($i>9 && $i<18){
                                    $html .= "<td style='border: 1px solid #DEDEDE;background-color: #FFFFFF'></td>";
                                }else{
                                    $html .= "<td style='border: 1px solid #DEDEDE;background-color: #C3C3C3'></td>";
                                }
                            }
                            $html .= "<td><p style='font-size: 1.1em;text-align: center'>".$item['type']."</p></td></tr><tr><td style=' border: 0;height: 1px;padding: 0;margin: 0;' colspan='12'></td></tr>";
                        }
                    }else{
                        $html .= "<tr><td colspan='12'>No Data Found</td></tr>";
                    }

                    $html .= "</tbody></table>";
                    echo $html;die;

                }
            }
        }catch(Exception $e){
            echo $e;
        }
    }
    public function actionGetRequisitionDetail(){
        $model = Requisitions::model()->findByPk($_GET["id"]);
        echo json_encode($model->attributes);die();
    }
    public function actionGetDefectDetail(){
        $sql = "SELECT d.id, d.vehicle_reg_no,d.driver_name,d.driver_pin,d.apply_date,dv.delivery_date, dv.action_taken FROM tbl_defects d JOIN tbl_deliveryvehicles dv on dv.defect_id=d.id WHERE d.id = ".$_GET['id'];
        $c = Yii::app()->db->createCommand($sql);
        $result = $c->queryRow();
        echo json_encode($result);die();
    }

}