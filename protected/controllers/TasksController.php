<?php

class TasksController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */

    public $work_type = Array(
        'inside'=>'Inside',
        'outside'=>'Outside',
        'other'=>'Other',

    );
    
   public $v_type = Array(
        '1'=>'micro_jeep',
        '2'=>'car',
        '3'=>'micro_jeep',
        '4'=>'pickup',
        '5'=>'bus_coaster',

    );

	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}



	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getVehicleData'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		
	
		
		$model=new Tasks;    
    $model->created_by = Yii::app()->user->username;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
				
			if(empty($_REQUEST['id']))			
			{
					throw new CHttpException(404,'Pleae pick task from defect.');
			}


		
	  if(isset($_POST['Tasks']))
		{


			  $model->attributes=$_POST['Tasks'];
			  
			  


            $get_type = $model->work_type;

        if ($get_type == 'inside'){
            $sql  =  "select vehicletype_id from tbl_defects  WHERE id = '".$model->defect_id."'";
		             
			  $rows = Yii::app()->db->createCommand($sql)->queryAll();
			  $vehicletype = $this->v_type[$rows[0]['vehicletype_id']];

            if(empty($vehicletype)){
                throw new CHttpException(404,'Required field can not be blank.');
            }

		    //  $sql  =  "select '.$vehicletype.' from tbl_wsservices where service_name ='".$model->service_name."'";
            echo $sql  =  "select $vehicletype  from tbl_wsservices where trim(service_name) ='".$model->service_name."'";
       

				$rows = Yii::app()->db->createCommand($sql)->queryAll();
			

				$model->bill_amount= $rows[0][$vehicletype];

            $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));

            $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            if(empty ($result))
            {
                $temp_total = 0;
            } else{
                $total = $result[0]['totalp'] + $model->bill_amount;
            }


            $d[0]['total_amount'] = $total;

            Defects::model()->updateByPk($model->defect_id, array(
                'total_amount' =>  $d[0]['total_amount']

            ));

            }

            elseif ($get_type == 'outside'){
                $sql  =  "select vehicletype_id from tbl_defects  WHERE id = '".$model->defect_id."'";

                $rows = Yii::app()->db->createCommand($sql)->queryAll();
                $vehicletype = $this->v_type[$rows[0]['vehicletype_id']];

                if(empty($vehicletype)){
                    throw new CHttpException(404,'Required field can not be blank.');
                }

                $sql  =  "select $vehicletype  from tbl_wsoutservices where trim(service_name) ='".$model->service_name."'";
                $rows = Yii::app()->db->createCommand($sql)->queryAll();

                if(!empty($model->quantity)){
                $model->bill_amount= $rows[0][$vehicletype] * $model->quantity;
                } else {
                    $model->bill_amount= $rows[0][$vehicletype];
                }

                $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));
                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";
                $command = Yii::app()->db->createCommand($sql);
                $result = $command->queryAll();

                if(empty ($result))
                {
                    $temp_total = 0;
                } else{
                    $total = $result[0]['totalp'] + $model->bill_amount;
                }


                $d[0]['total_amount'] = $total;

                Defects::model()->updateByPk($model->defect_id, array(
                    'total_amount' =>  $d[0]['total_amount']

                ));
            }
            elseif($get_type == 'other'){
                //$model->bill_amount= $model->out_bill_amount * $model->quantity;


                if(!empty($model->quantity)){
                    $model->bill_amount= $model->out_bill_amount * $model->quantity;
                } else {
                    $model->bill_amount= $model->out_bill_amount;
                }

                $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));
                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";
                $command = Yii::app()->db->createCommand($sql);
                $result = $command->queryAll();

                if(empty ($result))
                {
                    $temp_total = 0;
                } else{
                    $total = $result[0]['totalp'] + $model->bill_amount;
                }
                $d[0]['total_amount'] = $total;
                Defects::model()->updateByPk($model->defect_id, array(
                    'total_amount' =>  $d[0]['total_amount']
                ));

            }

						
            if($model->save())
         		$this->redirect(array('defects/view','id'=>$model->defect_id));
		}

		if(Yii::app()->request->isAjaxRequest)
			$this->renderPartial('create',array(
				'model'=>$model,
				'work_type'=>$this->work_type,
			), false, true);
		else
			$this->render('create',array(
				'model'=>$model,
				'work_type'=>$this->work_type,
			));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        if(isset($_POST['Tasks']))
        {
            $model->attributes=$_POST['Tasks'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
            'work_type'=>$this->work_type
        ));
    }

   /* public function actionUpdate($id)
    {
        // throw new CHttpException(404,'You are not authorized to update.');

        $model=$this->loadModel($id);

        $model->updated_by = Yii::app()->user->username;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        //if($rows[0]['active']=='3')throw new CHttpException(404,'The jobcard associated with this task is closed. So update functionality is dectivated');

        if(isset($_POST['Tasks']))
        {
            $model->attributes=$_POST['Tasks'];
            $get_type = $model->work_type;
            if ($get_type == 'inside'){
                $sql  =  "select vehicletype_id from tbl_defects  WHERE id = '".$model->defect_id."'";

                $rows = Yii::app()->db->createCommand($sql)->queryAll();
                $vehicletype = $this->v_type[$rows[0]['vehicletype_id']];

                if(empty($vehicletype)){
                    throw new CHttpException(404,'Required field can not be blank.');
                }


                $sql = "SELECT  $vehicletype FROM tbl_wsservices WHERE service_name = '$model->service_name'";

                $rows = Yii::app()->db->createCommand($sql)->queryAll();
                $model->bill_amount= $rows[0][$vehicletype];

                $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));

                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";
                $command = Yii::app()->db->createCommand($sql);
                $result = $command->queryAll();

                if(empty ($result))
                {
                    $temp_total = 0;
                } else{
                    $total = $result[0]['totalp'] + $model->bill_amount;
                }


                $d[0]['total_amount'] = $total;

                Defects::model()->updateByPk($model->defect_id, array(
                    'total_amount' =>  $d[0]['total_amount']

                ));
            }

            elseif ($get_type == 'outside'){
                //  die;
                $sql  =  "select vehicletype_id from tbl_defects  WHERE id = '".$model->defect_id."'";

                $rows = Yii::app()->db->createCommand($sql)->queryAll();
                $vehicletype = $this->v_type[$rows[0]['vehicletype_id']];

                if(empty($vehicletype)){
                    throw new CHttpException(404,'Required field can not be blank.');
                }

                $sql  =  "select $vehicletype  from tbl_wsoutservices where service_name ='".$model->service_name."'";
                $rows = Yii::app()->db->createCommand($sql)->queryAll();
                //$model->bill_amount= $rows[0][$vehicletype];
                if(!empty($model->quantity)){
                    $model->bill_amount= $rows[0][$vehicletype] * $model->quantity;
                } else {
                    $model->bill_amount= $rows[0][$vehicletype];
                }

                $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));
                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";

                $command = Yii::app()->db->createCommand($sql);
                $result = $command->queryAll();

                if(empty ($result))
                {
                    $temp_total = 0;
                } else{
                    $total = $result[0]['totalp'] + $model->bill_amount;
                }
                $d[0]['total_amount'] = $total;
                Defects::model()->updateByPk($model->defect_id, array(
                    'total_amount' =>  $d[0]['total_amount']
                ));
            }

            elseif($get_type == 'other'){
                //$model->bill_amount= $model->out_bill_amount;
                if(!empty($model->quantity)){
                    $model->bill_amount= $model->out_bill_amount * $model->quantity;
                } else {
                    $model->bill_amount= $model->out_bill_amount;
                }

                $d = Defects::model()->findAll(array("condition"=>"id =  $model->defect_id","order"=>"id"));
                $sql = "SELECT sum(total_amount) as totalp FROM tbl_defects where id = $model->defect_id ";
                $command = Yii::app()->db->createCommand($sql);
                $result = $command->queryAll();

                if(empty ($result))
                {
                    $temp_total = 0;
                } else{
                    $total = $result[0]['totalp'] + $model->bill_amount;
                }

                $d[0]['total_amount'] = $total;
                Defects::model()->updateByPk($model->defect_id, array(
                    'total_amount' =>  $d[0]['total_amount']
                ));

            }


            if($model->save())
                $this->redirect(array('view','id'=>$model->id));

        }

        $this->render('update',array(
            'model'=>$model,
            'work_type'=>$this->work_type,
        ));

    }*/

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        //throw new CHttpException(404,'You are not authorized to delete.');
		//$this->loadModel($id)->delete();
        $user = Yii::app()->user->username;

        $def_sql="SELECT defect_id FROM tbl_tasks where id= '$id'";
        $command = Yii::app()->db->createCommand($def_sql);
        $result = $command->queryAll();

        $defect_id =  $result[0]['defect_id'];

        $sql =  "UPDATE tbl_defects SET total_amount=(total_amount-(SELECT sum(bill_amount) FROM tbl_tasks
        where id='$id')) WHERE id='$defect_id'";

        $command = Yii::app()->db->createCommand($sql);
        $resultex = $command->execute();

        $command = Yii::app()->db->createCommand();

        $in_sql ='INSERT INTO taskdel_logs(id, defect_id, service_name, work_type, quantity, bill_amount, delete_by) SELECT id, defect_id, service_name, work_type, quantity, bill_amount, :users FROM tbl_tasks
        where id=:id';
        $params = array(
            "id" => $id,
            "users" => $user

        );
        $command->setText($in_sql)->execute($params);

        $sql='DELETE FROM tbl_tasks WHERE id=:id';
        $params = array(
            "id" => $id
        );
        $command->setText($sql)->execute($params);


		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))

			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('defects/view','id'=>$defect_id));
	}

	/**
	 * Lists all models.
	 */

    public function actionIndex()
    {
        $model=new Tasks('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Tasks']))
            $model->attributes=$_GET['Tasks'];

        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Tasks('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tasks']))
			$model->attributes=$_GET['Tasks'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


    public function actionGetVehicleData() {
        $defect_id = $_REQUEST['Tasks']['defect_id'];
        if($_REQUEST['Deliveryvehicles']['defect_id']){
        $defect_id = $_REQUEST['Deliveryvehicles']['defect_id'];
        }
        if($_REQUEST['Wstyres']['defect_id']){
        $defect_id = $_REQUEST['Wstyres']['defect_id'];
        }
        if($_REQUEST['Wsbatteries']['defect_id']){
        $defect_id = $_REQUEST['Wsbatteries']['defect_id'];
        }
        if($_REQUEST['Wsitemdists']['defect_id']){
            $defect_id = $_REQUEST['Wsitemdists']['defect_id'];
        }

        if (!empty($defect_id)) {

            $sql = "SELECT vehicle_reg_no,driver_pin,driver_name,vehicletype_id	FROM tbl_defects  WHERE id =  '$defect_id'";


            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();

            if(!empty($result[0]['vehicle_reg_no']))
            {
                $vehicle_reg_no = $result[0]['vehicle_reg_no'];

/*
                $sql = "SELECT max(end_meter) as max_meter FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_reg_no'";
                $command = Yii::app()->db->createCommand($sql);
                $result1 = $command->queryAll();

                if($result1[0]['max_meter'])
                    $result[0]['start_meter'] = $result1[0]['max_meter'];
                else
                    $result[0]['start_meter'] = 0;*/

                echo CJSON::encode($result[0]); exit;
            }
        } else {


            return false;
        }
    }



    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Tasks::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tasks-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
