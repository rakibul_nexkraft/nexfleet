<?php

class WsitemsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

            array('allow',
                'actions'=>array(
                    'treePath','loadContent','suggestAuPlaces',
                    'suggestAuHierarchy','suggestLastname','fillAuTree','viewUnitPath','viewUnitLabel','initPerson',
                    'suggestPerson','suggestVehiclemodel','suggestpartno','suggestItemname','suggestPersonGroupCountry','listPersonsWithSameFirstname',
                    'addTabularInputs','addTabularInputsAsTable'
                ),
                'users'=>array('*'),
            ),

			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getVehicleModel','getPartNo','getItemName','UpdateAjaxStation','UpdateAjaxStation1','UpdateAjaxStation2','getItemsData','getOneItemData','getItemsDataModel'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Wsitems;
        $model->created_by = Yii::app()->user->username;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsitems']))
		{
			$model->attributes=$_POST['Wsitems'];
			
			if($model->save())
			{
			 	$b=new Wsstocks;
                $b->wsitem_id=$model->id;
				$b->wsitemname_id=$model->wsitemname_id;
                $b->vehicle_reg_no=$model->vehicle_reg_no;
                $b->vehicle_model=$model->vehicle_model;
                $b->parts_no=$model->parts_no;
				$b->stock_in=$model->quantity;
				$b->temp_stock=$model->quantity;
				$b->price=$model->unit_price;
				$b->total_amount=$model->total_price;
				$b->purchase_date=$model->created_time;
				
			//	$sql1 = "SELECT name FROM tbl_wsitemnames where id = $model->wsitemname_id";
			//	$command = Yii::app()->db->createCommand($sql1);				
		//		$result1 = $command->queryAll();
				$b->wsitemname=$model->item_name;
				
				if($model->vehicle_reg_no)
				$sql = "SELECT vehicle_reg_no,available_stock FROM tbl_wsstocks where wsitemname_id = $model->wsitemname_id  and vehicle_reg_no='$model->vehicle_reg_no' order by id desc limit 1";
				
				else 
				 $sql = "SELECT vehicle_reg_no,available_stock FROM tbl_wsstocks where wsitemname_id = $model->wsitemname_id  and vehicle_reg_no=''  order by id desc limit 1";
				$command = Yii::app()->db->createCommand($sql);				
				$result = $command->queryAll();
				

				
				if(!empty($result[0]))
				{
					
				if($model->vehicle_reg_no)
					{
						if($result[0]['vehicle_reg_no'])
						
						$b->available_stock = $model->quantity+$result[0]['available_stock'];
						
				}
				else if(!$model->vehicle_reg_no)
				{
				 		if(!$result[0]['vehicle_reg_no'])
						$b->available_stock = $model->quantity+$result[0]['available_stock'];
				}
                }
				else
					$b->available_stock = $model->quantity;


				
				$b->save();
				
				Yii::app()->user->setFlash('success', '<strong>Success!</strong> Item saved successfully.');
				//$this->redirect(array('view','id'=>$model->id));
				//$this->redirect(array('index'));
                if(!isset($_GET['redirectUrl']))
                    {
                        $this->redirect(array('index','id'=>$model->id));
                    }
			}
		}

        $purchasetype =Array(
            ''=>'',
            'By Procurement'=> 'By Procurement',
            'By Workshop(cash)'=> 'By Workshop(cash)',
            'DO'=> 'DO',
            'Sister Concern'=> 'Sister Concern'
        );

		if(Yii::app()->request->isAjaxRequest)
			$this->renderPartial('create',array(
				'model'=>$model,
                'purchasetype'=>$purchasetype,
			), false, true);
		else{
            $this->render('index',array(
            'model'=>$model,
            'purchasetype'=>$purchasetype,
        ));

            //$this->render('create',array(
                //'model'=>$model,
                //'purchasetype'=>$purchasetype,
            //));
            }
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{

        throw new CHttpException(404,'Update feature is not available Now.');

		$model=$this->loadModel($id);
        $model->updated_by = Yii::app()->user->username;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsitems']))
		{
			$model->attributes=$_POST['Wsitems'];
			//$model->updated_by = Yii::app()->user->username;
			if($model->save())
			{
				
				$b=new Wsstocks;            
				$b->wsitemname_id=$model->wsitemname_id;
                $b->vehicle_model=$model->vehicle_model;
				$b->stock_in=$model->quantity;
				$b->temp_stock=$model->quantity;
				$b->price=$model->unit_price;
				$b->total_amount=$model->total_price;
				$b->purchase_date=$model->created_time;    
				
				$sql = "SELECT available_stock FROM tbl_wsstocks where wsitemname_id = $model->wsitemname_id  order by id desc limit 1";
				$command = Yii::app()->db->createCommand($sql);				
				$result = $command->queryAll();
				
				if(!empty($result[0]))
			  $b->available_stock = $model->quantity+$result[0]['available_stock'];
			  else
			  	$b->available_stock = $model->quantity;
			  		
				$b->save();
    
						
				$this->redirect(array('view','id'=>$model->id));
			}
		}

        $purchasetype =Array(
            ''=>'',
            'By Procurement'=> 'By Procurement',
            'By Workshop(cash)'=> 'By Workshop(cash)',
            'DO'=> 'DO',
            'Sister Concern'=> 'Sister Concern'
        );

		$this->render('update',array(
			'model'=>$model,
            'purchasetype'=>$purchasetype,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
    public function actionDelete($id)
    {


        $def_sql="SELECT defect_id, total_price FROM tbl_wsitemdists where wsitem_id= '$id'";

        $command = Yii::app()->db->createCommand($def_sql);
        $result = $command->queryAll();

      $defect_id =  $result[0]['defect_id'];

       $sql =  "UPDATE tbl_defects SET total_amount=(total_amount-(SELECT sum(total_price) FROM tbl_wsitemdists
    where wsitem_id='$id')) WHERE id='$defect_id'";


        //$sql = "UPDATE tbl_defect set total_amount=".$temp_stock[$i]['quantity']. " where id = '$id' ";
        $command = Yii::app()->db->createCommand($sql);
        $resultex = $command->execute();



        //throw new CHttpException(404,'You are not authorized to delete.');

        //$query = "delete from `tbl_wsstocks` where wsitem_id= $id";
        $command = Yii::app()->db->createCommand();

        $in_sql ='INSERT INTO temp_wsstocks SELECT * FROM tbl_wsstocks where wsitem_id=:id';
        $params = array(

            "id" => $id
        );
        $command->setText($in_sql)->execute($params);

        $sql='DELETE FROM tbl_wsstocks WHERE wsitem_id=:id';
        $params = array(

            "id" => $id
        );
        $command->setText($sql)->execute($params);




        $sql1='DELETE FROM tbl_wsitemdists WHERE wsitem_id=:id';
        $params = array(

            "id" => $id
        );
        $command->setText($sql1)->execute($params);

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Wsitems('search');
		$model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsitems']))
            $model->attributes=$_GET['Wsitems'];
			
		$this->render('index',array(
			'model'=>$model,
			'dataProvider'=>$model->search(),
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Wsitems('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsitems']))
			$model->attributes=$_GET['Wsitems'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    public function actionGetVehicleModel() {
        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';
            //$sql = "SELECT v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.vehicle_model  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";
            $sql = "SELECT distinct v.vehicle_model as value FROM tbl_wsitemnames as v  WHERE v.vehicle_model LIKE '$qterm' ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }

    public function actionGetPartNo() {
        if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';
            //$sql = "SELECT v.wsitemname as value,v.wsitemname_id as wsitemname_id, v.vehicle_model  FROM tbl_wsstocks as v WHERE wsitemname LIKE '$qterm' and v.available_stock !=0 and v.temp_stock!=0 ";
            $sql = "SELECT v.parts_no as value FROM tbl_wsitemnames as v  WHERE v.parts_no LIKE '$qterm' ";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        }
        else {
            return false;
        }
    }


    public function actionGetItemName() {

if (!empty($_GET['term']))
        {
            $qterm = '%'.$_GET['term'].'%';

             $vehicle_model = $_GET['vehicle_model'];
             $parts_no  = $_GET['parts_no'];

             if(!empty($parts_no)){
            //$sql = "SELECT v.name as value,v.id as wsitemname_id, v.vehicle_model, v.vehicle_type  FROM tbl_wsitemnames as v WHERE name LIKE '$qterm'";
            $sql = "SELECT v.name as value,v.id as wsitemname_id, v.vehicle_model  FROM tbl_wsitemnames as v WHERE 
            name LIKE '$qterm' and v.vehicle_model = '$vehicle_model' and v.parts_no= '$parts_no'";
} else{
            $sql = "SELECT v.name as value,v.id as wsitemname_id, v.vehicle_model  FROM tbl_wsitemnames as v WHERE 
            name LIKE '$qterm' and v.vehicle_model = '$vehicle_model'";
}
            
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); 

        }
        else {
            return false;
        }
    }

    public function actionUpdateAjaxStation()
    {
        //$district = mysql_real_escape_string($_REQUEST['Criminalcases']['district']);
        $getmodel = addslashes($_REQUEST['Wsitems']['vehicle_model']);
      //  $getparts = addslashes($_REQUEST['Wsitems']['parts_no']);

        $data = Wsitemnames::model()->findAll(array('condition'=>"vehicle_model='$getmodel' ",'select'=>'parts_no'));


        if (!empty($data)) { echo '<option value="">Select Parts...</option>'; }

        foreach($data as $value=>$name) {

            echo CHtml::tag('option',
              array('value'=>$name->parts_no),CHtml::encode($name->parts_no),true);
        }
    }

    public function actionUpdateAjaxStation1()
    {
        //$district = mysql_real_escape_string($_REQUEST['Criminalcases']['district']);
        $getparts = addslashes($_REQUEST['Wsitems']['parts_no']);

        $data = Wsitemnames::model()->findAll(array('condition'=>"parts_no='$getparts' ",'select'=>'name,id'));


        if (!empty($data)) { echo '<option value="">Select Item Name...</option>'; }

        foreach($data as $value=>$name) {

            echo CHtml::tag('option',
                array('value'=>$name->name),CHtml::encode($name->name),true);

        }
    }

    public function actionUpdateAjaxStation2()
    {
       // echo "1";
         $getparts = addslashes($_REQUEST['Wsitems']['parts_no']);
         $getname = addslashes($_REQUEST['Wsitems']['item_name']);

        //  $getparts = addslashes($_REQUEST['Wsitems']['parts_no']);
        $data = Wsitemnames::model()->findAll(array('condition'=>"name='$getname' and parts_no='$getparts' ",'select'=>'id'));


        if (!empty($data)) { //echo '<option value="">Select ID...</option>';
        }

       // echo CJSON::encode($data); exit;
       foreach($data as $value=>$name) {

            echo CHtml::tag('option',
                array('value'=>$name->id),CHtml::encode($name->id),true);

        }

       /*foreach($data as $value=>$name) {
// echo $name->id;
            echo CHtml::encode($name->id);

            //echo CHTML::encode($name->id);



        }
        */
    }


    public function actions()
    {
        return array(
            'suggestCountry'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'Country',
                'methodName'=>'suggest',
            ),
            'legacySuggestCountry'=>array(
                'class'=>'ext.actions.XLegacySuggestAction',
                'modelName'=>'Country',
                'methodName'=>'legacySuggest',
            ),
            'fillTree'=>array(
                'class'=>'ext.actions.XFillTreeAction',
                'modelName'=>'Menu',
                'showRoot'=>false
            ),
            'treePath'=>array(
                'class'=>'ext.actions.XAjaxEchoAction',
                'modelName'=>'Menu',
                'attributeName'=>'pathText',
            ),
            'uploadFile'=>array(
                'class'=>'ext.actions.XHEditorUpload',
            ),
            'suggestAuPlaces'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'AdminUnit',
                'methodName'=>'suggestPlaces',
                'limit'=>30
            ),
            'suggestAuHierarchy'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'AdminUnit',
                'methodName'=>'suggestHierarchy',
                'limit'=>30
            ),
            'suggestLastname'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'Person',
                'methodName'=>'suggestLastname',
                'limit'=>30
            ),
            'fillAuTree'=>array(
                'class'=>'ext.actions.XFillTreeAction',
                'modelName'=>'AdminUnit',
                'showRoot'=>false,
            ),
            'viewUnitPath'=>array(
                'class'=>'ext.actions.XAjaxEchoAction',
                'modelName'=>'AdminUnit',
                'attributeName'=>'rootlessPath',
            ),
            'viewUnitLabel'=>array(
                'class'=>'ext.actions.XAjaxEchoAction',
                'modelName'=>'AdminUnit',
                'attributeName'=>'label',
            ),
            'initPerson'=>array(
                'class'=>'ext.actions.XSelect2InitAction',
                'modelName'=>'Person',
                'textField'=>'fullname',
            ),
            'suggestPerson'=>array(
                'class'=>'ext.actions.XSelect2SuggestAction',
                'modelName'=>'Person',
                'methodName'=>'suggestPerson',
                'limit'=>30
            ),
            'suggestPersonGroupCountry'=>array(
                'class'=>'ext.actions.XSelect2SuggestAction',
                'modelName'=>'Person',
                'methodName'=>'suggestPersonGroupCountry',
                'limit'=>30
            ),
            'suggestVehiclemodel'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'Wsitems',
                'methodName'=>'actionGetVehicleModel',
                'limit'=>2
            ),
            'suggestPartno'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'Wsitems',
                'methodName'=>'actionGetPartNo',
                'limit'=>2
            ),
            'suggestItemname'=>array(
                'class'=>'ext.actions.XSuggestAction',
                'modelName'=>'Wsitems',
                'methodName'=>'actionGetItemName',
                'limit'=>2
            ),
            'addTabularInputs'=>array(
                'class'=>'ext.actions.XTabularInputAction',
                'modelName'=>'Wsitems',
                'viewName'=>'_tabularInput',
                //'max'=>30
            ),
            'addTabularInputsAsTable'=>array(
                'class'=>'ext.actions.XTabularInputAction',
                'modelName'=>'Person',
                'viewName'=>'/site/extensions/_tabularInputAsTable',
            ),
        );
    }

    public function actionExcel()
    {
        $model=new Wsitems('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'workshop_items',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'workshop_items',

            'columns'=>array(
                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),
                'item_name',
                'wsrequisition_requisition_no',
                'requisition_date',
                'vehicle_reg_no',
                'vehicle_model',
                'parts_no',
                'purchase_type',
                'bill_date',
                //'purchase_date',
                'item_size',
                'quantity',
                'unit_price',
                'total_price',
                'wssupplier_name',


            ),

        ));
        Yii::app()->end();
    }



	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Wsitems::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wsitems-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    public function actionGetItemsData() {
      $vehicle_reg_no = $_REQUEST['vehicle_reg_no'];
        //$model=Wsitems::model()->findAllByAttributes(array('vehicle_reg_no'=>$defect_id));
        //echo CJSON::encode($result[0]);
       //$model=Wsitems::model()->findAll(array(
            //'select'=>'id,item_name',
           // 'condition'=>'vehicle_reg_no=:vehicleregno',
           // 'params'=>array(':vehicleregno'=>$vehicle_reg_no),
            //));
       //var_dump($model[0]);
        //$sql = "SELECT tbl_wsstocks.wsitemname as item_name1,tbl_wsstocks.id FROM tbl_wsitems INNER JOIN tbl_wsstocks ON tbl_wsitems.wsitemname_id=tbl_wsstocks.wsitemname_id WHERE tbl_wsitems.vehicle_reg_no = '$vehicle_reg_no' AND tbl_wsstocks.available_stock <> '0' GROUP BY tbl_wsstocks.wsitemname_id";
      $sql = "SELECT wsitemname as item_name1,id,SUM(temp_stock) as available_stock FROM  tbl_wsstocks  WHERE vehicle_reg_no = '$vehicle_reg_no' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL GROUP BY wsitemname_id ORDER BY wsitemname";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            //var_dump($result);
            if($result){
            //$result_final=array_map(function($v){
                //return [$v['id']=>$v['item_name']];
            //},$result);
           echo CJSON::encode($result);exit;
            }
             else {
                $result[0]['id']='';
                $result[0]['item_name1']='No Data Found';
                echo CJSON::encode($result);exit;
           // return false;
        }      

     }
     public function actionGetItemsDataModel() {
      $vehicle_model = $_REQUEST['vehicle_model'];
        //$model=Wsitems::model()->findAllByAttributes(array('vehicle_reg_no'=>$defect_id));
        //echo CJSON::encode($result[0]);
       //$model=Wsitems::model()->findAll(array(
            //'select'=>'id,item_name',
           // 'condition'=>'vehicle_reg_no=:vehicleregno',
           // 'params'=>array(':vehicleregno'=>$vehicle_reg_no),
            //));
       //var_dump($model[0]);
        //$sql = "SELECT id,item_name FROM tbl_wsitems WHERE vehicle_model = '$vehicle_model '";
       //$sql = "SELECT wsitemname as item_name1,id FROM  tbl_wsstocks  WHERE vehicle_model = '$vehicle_model' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL GROUP BY wsitemname_id ORDER BY id";
       $sql = "SELECT wsitemname as item_name1,id,SUM(temp_stock) as available_stock FROM  tbl_wsstocks  WHERE vehicle_model = '$vehicle_model' AND vehicle_reg_no ='' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL GROUP BY wsitemname_id ORDER BY wsitemname";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            //var_dump($result);
            if($result){
            //$result_final=array_map(function($v){
                //return [$v['id']=>$v['item_name']];
            //},$result);
           echo CJSON::encode($result);exit;
            }
             else {
                $result[0]['id']='';
                $result[0]['item_name1']='No Data Found';
                echo CJSON::encode($result);exit;
            //return false;
        }      

     }
     public function actiongetOneItemData() {
      $id = $_REQUEST['id'];
        //$model=Wsitems::model()->findAllByAttributes(array('vehicle_reg_no'=>$defect_id));
        //echo CJSON::encode($result[0]);
       //$model=Wsitems::model()->findAll(array(
            //'select'=>'id,item_name',
           // 'condition'=>'vehicle_reg_no=:vehicleregno',
           // 'params'=>array(':vehicleregno'=>$vehicle_reg_no),
            //));
       //var_dump($model[0]);
        /*$sql = "SELECT * FROM tbl_wsitems WHERE id = '$id'";
            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            //var_dump($result);
            $vehicleReg=$result[0]['vehicle_reg_no'];
            $wsitemnameId=$result[0]['wsitemname_id'];
            $wsitemname=$result[0]['item_name'];
        if($vehicleReg)
            $sql = "SELECT available_stock FROM tbl_wsstocks where  vehicle_reg_no='$vehicleReg' AND available_stock<>'0' GROUP BY '$wsitemname' order by id desc limit 1";
            else
                $sql = "SELECT available_stock FROM tbl_wsstocks where wsitemname_id = '$wsitemnameId'   and vehicle_reg_no=''  order by id desc limit 1";

            $command = Yii::app()->db->createCommand($sql);             
            $result1 = $command->queryAll();
                
            $available_stock = $result1[0]['available_stock'];
            $result[0]['availableStock']=$available_stock;*/
             $sql = "SELECT * FROM tbl_wsstocks WHERE id = '$id'";
            $command = Yii::app()->db->createCommand($sql);
            $result1 = $command->queryAll();
            if($result1[0]['vehicle_reg_no']){
                $vehicle_reg_no=$result1[0]['vehicle_reg_no'];
                $wsitemname_id=$result1[0]['wsitemname_id'];
                $sql = "SELECT id,SUM(temp_stock) as available_stock1 FROM  tbl_wsstocks  WHERE vehicle_reg_no = '$vehicle_reg_no' AND wsitemname_id='$wsitemname_id' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL";
            $command = Yii::app()->db->createCommand($sql);
            $result3 = $command->queryAll();
            }
            else{
                 $vehicle_model=$result1[0]['vehicle_model'];
                 $wsitemname_id=$result1[0]['wsitemname_id'];
                 $sql = "SELECT wsitemname as item_name1,id,SUM(temp_stock) as available_stock1 FROM  tbl_wsstocks  WHERE vehicle_model = '$vehicle_model' AND vehicle_reg_no ='' AND wsitemname_id='$wsitemname_id' AND temp_stock <> '0' AND wsitem_id <> '0' AND wsitem_id IS NOT NULL";
                $command = Yii::app()->db->createCommand($sql);
                $result3 = $command->queryAll();
            }
            $wsitem_id= $result1[0]['wsitem_id'];
             $sql = "SELECT * FROM tbl_wsitems WHERE id = '$wsitem_id'";
             $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            $result[0]['availableStock']=$result3[0]['available_stock1'];
            if($result){
            //$result_final=array_map(function($v){
                //return [$v['id']=>$v['item_name']];
            //},$result);
           echo CJSON::encode($result);exit;
            }
             else {
            return false;
        }      

     }
    
	
		
}
