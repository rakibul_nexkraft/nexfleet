<?php 
date_default_timezone_set("Asia/Dhaka");
class ApiController extends Controller
{
    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';
    /**
     * @return array action filters
     */
    public function filters()
    {
            return array();
    }

    // Actions
    public function actionList()
    {
                // Get the respective model instance
            switch($_GET['model'])
            {
                case 'posts':
                    $models = Post::model()->findAll();
                    break;
                case 'dglogbook':
                    $models = DigitalLogBookHistory::model()->findAll(array('limit' => 5)); 
                    break;
               
                case 'feedbackSettings':
                    $models = FeedbackSettings::model()->findAll();
                    break;
                case 'feedbackRating':
                    $models = FeedbackRating::model()->findAll();
                    break;
                case 'billRates':
                    $models = Billrates::model()->findAll();
                    break;  
                case 'routeList':
                    $models = Routes::model()->findAll();
                    break; 
                 case 'workshopUserList':
                    $workShpouserList=array(array('name'=>'Partha Protim Bala','pin'=>'175217','designation'=>'MANAGER'),
                    array('name'=>'Md. Mizanur Rahman Milon','pin'=>'175313','designation'=>'ASST. MANAGER'),
                    array('name'=>'Md Jafor Alam','pin'=>'4245','designation'=>'HEAD MECHANIC'),
                    array('name'=>'Md. Mizanur Rahman','pin'=>'53064','designation'=>'TRANSPORT ASSISTANT'),
                  );
                  $this->_sendResponse(200, CJSON::encode($workShpouserList));
                        return;
                break;  
                case 'timesetting':
                 $date=date("Y-m-d");
                 $criteria = new CDbCriteria();
                 $criteria->addCondition('start_date <= "'.$date.'" AND end_date >= "'.$date.'"');
                 $models = Timesettings::model()->findAll($criteria);
                break;
                case 'dutytypeservice':
                    $models =Dutytypes::model()->findAll();   
                break;  
                case 'holiday':
                 $date=date("Y-m-d");
                 $criteria = new CDbCriteria();
                 $criteria->addCondition('start_date <= "'.$date.'" AND end_date >= "'.$date.'"');
                 $models = Holidays::model()->findAll($criteria);
                 if(!$models){
                    echo '0';
                    return;
                 }
                 else{
                    echo '1';
                    return;
                 }
                break;  
                case 'ifleetrating':                
                $query="SELECT id, AVG(ifleet_rating) as rating FROM tbl_feedback_rating";
                $models=Yii::app()->db->createCommand($query)->queryAll();  
                if(!empty($models[0]['id'])){
                        $this->_sendResponse(200, CJSON::encode($models));
                        return;
                        }
                unset($models);          
                break;
                case 'tadatime':
                    $current_time=date('H:i:s');
                    $flag=0;
                    $models =TaDaTime::model()->findALL(); 
                    if($models){
                        foreach ($models as $key => $value) {
                            if($value['start_time']<=$current_time && $value['end_time']>=$current_time && $value['id']!=5){
                                echo $value['id']; 
                                $flag=1; 
                            }                 
                        }
                        if($flag==0)echo '0';
                        return;
                    } 
                    else{
                        echo '0';
                        return;
                    } 
                break;
                case 'mtomvehiclelist':
                      
                       $token=$this->mtoMAuthentication();
                       $postData = array(
                        "product" => "VTS API",
                        "version" => "1.0",
                        "apiName" => "getVehicleList",
                        "apiSecretKey" => "4d324d-415049-565453-4e4558444543414445",
                        "token"=>"$token", "offset"=>0, 
                        "limit"=>100
                      );


                      $curl = curl_init();

                      curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.m2mbd.com/VehicleList",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => json_encode($postData),
                          CURLOPT_HTTPHEADER => array(
                              "Accept: application/json",
                              "Cache-Control: no-cache"
                          ),
                      ));

                      $response = curl_exec($curl);
                      $err = curl_error($curl);

                      curl_close($curl);

                      if ($err) {
                          echo "cURL Error #:" . $err;
                      } else {
                         $response= json_decode($response);
                         $vehicle=$response->response[0]->detailsOfVehicle;
                         $i=0;
                         foreach ($vehicle as $key => $value) {
                           $vehId=trim($value->vehId);
                           $vehName=trim($value->vehName);                          
                           $vehName=substr_replace($vehName, "-", 2, 0);
                           $vehName=substr_replace($vehName, "-", -6,0);
                           $vehName=substr_replace($vehName, "-", -4,0);
                           $vehName=trim($vehName);
                           
                           //$sql="UPDATE tbl_vehicles SET asset_id='$vehId' WHERE reg_no='$vehName'";//for update asset ID
                           //$sql1="Select * FROM tbl_vehicles WHERE reg_no='$vehName'";
                           //$sql_rs=Yii::app()->db->createCommand($sql1)->queryRow();
                         $sql_rs=Yii::app()->db->createCommand($sql)->execute();
                           if($sql_rs){$i++; echo $vehName;echo "</br>";
                              }
                        
                         }
                         echo $i;
                      
                      }

                   
                return;
                break;
                default:
                    // Model not implemented error
                    $this->_sendResponse(501, sprintf(
                        'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
                        $_GET['model']) );
                    Yii::app()->end();
            }
            // Did we get some results?
            if(empty($models)) {
                // No
                $this->_sendResponse(200, 
                        sprintf('No items where found for model <b>%s</b>', $_GET['model']) );
            } else {
                // Prepare response
                $rows = array();
                foreach($models as $model)
                    $rows[] = $model->attributes;
                // Send the response
                $this->_sendResponse(200, CJSON::encode($rows));
            }
    }
    public function actionView()
    { 
        // Check if id was submitted via GET
        if(!isset($_GET['id']))
            $this->_sendResponse(500, 'Error: Parameter <b>id</b> is missing' );

        switch($_GET['model'])
        {
            // Find respective model    
            case 'posts':
                $model = Post::model()->findByPk($_GET['id']);
                break;
            case 'feedbackSettings':
                $model = FeedbackSettings::model()->findByPk($_GET['id']);
                break;
            case 'feedbackRating':
            $model =FeedbackRating::model()->findByPk($_GET['id']);                    
            break;

            case 'requisitions':
            $model =Requisitions::model()->findByPk($_GET['id']);   
            break;
            case 'vehicletyperate':
            $model =Vehicletypes::model()->findByPk($_GET['id']);   
            break;
            case 'dutytypeservice':
            $model =Dutytypes::model()->findByPk($_GET['id']); 
            if($model){
              $models[]=$model; 
              $this->_sendResponse(200, CJSON::encode($models)); 
              return;
            }  
            break;
            case 'tadatime':
            $model =TaDaTime::model()->findByPk($_GET['id']);   
            break;
            case 'dailysummery':
              //$model=DigitalLogBookHistory::model()->findAllByAttributes(array('driver_pin'=> $_GET['id']));
            $start_date=date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
           
            $query="SELECT  * FROM tbl_digital_log_book_history where driver_pin='".$_GET['id']."' AND DATE(created_time_mobile)>='".$start_date."' AND DATE(created_time_mobile)<='".date("Y-m-d")."' AND trip_id <>'' order by  created_time_mobile";
            $models=Yii::app()->db->createCommand($query)->queryAll(); 
            $i=0;
            $trip_content=array();
           // var_dump($models);
            foreach ($models as $key => $value) {

             // if($i==0 && !empty($value['trip_id'])){                                    
               // $trip_content[$value['trip_id']]=$value;
               // $i++;
              //}
               if(!array_key_exists($value['trip_id'], $trip_content) && !empty($value['trip_id'])){
                $trip_content[$value['trip_id']]=$value;

                 $trip_content[$value['trip_id']]['end_meter']=$value['meter_number'];
                 $trip_content[$value['trip_id']]['over_time']=$value['over_time'];
                 if(!empty($value['fuel_recipt_no'])){
               
                  $fuel_info['fuel_recipt_no']=$value['fuel_recipt_no'];
                  $fuel_info['fuel_total_bill']=$value['fuel_total_bill'];
                  $fuel_info['meter_number']=$value['meter_number'];
                  $fuel_info['location_vts']=$value['location_vts'];
                  $fuel_info['location_mobile']=$value['location_mobile'];
                  $fuel_info['created_time_vts']=$value['created_time_vts'];
                  $fuel_info['created_time_mobile']=$value['created_time_mobile'];
                  $fuel_info['created_time_vts']=$value['created_time_vts'];
                  $trip_content[$value['trip_id']]['fuel_take'][]=$fuel_info;

                }
                  if(!empty($value['others_cost'])){
                  $others_cost['others_cost']=$value['others_cost'];
                  $others_cost['others_bill']=$value['others_bill'];
                  $others_cost['others_description']=$value['others_description'];
                  $others_cost['meter_number']=$value['meter_number'];
                  $others_cost['location_vts']=$value['location_vts'];
                  $others_cost['location_mobile']=$value['location_mobile'];
                  $others_cost['created_time_vts']=$value['created_time_vts'];
                  $others_cost['created_time_mobile']=$value['created_time_mobile'];
                  $others_cost['created_time_vts']=$value['created_time_vts'];
                  $trip_content[$value['trip_id']]['others_cost'][]=$others_cost;

                }
               }
              if(array_key_exists($value['trip_id'], $trip_content) && !empty($value['trip_id'])){
                

                if($trip_content[$value['trip_id']]['meter_number']<=$value['meter_number'])
                    $trip_content[$value['trip_id']]['end_meter']=$value['meter_number'];

                if($trip_content[$value['trip_id']]['meter_number']>$value['meter_number'])
                    $trip_content[$value['trip_id']]['meter_number']=$value['meter_number'];

                if($trip_content[$value['trip_id']]['running_km']<=$value['running_km'])
                    $trip_content[$value['trip_id']]['running_km']=$value['running_km'];

              if(empty($trip_content[$value['trip_id']]['driver_end_time_mobile']) || ($trip_content[$value['trip_id']]['driver_end_time_mobile']=="0000-00-00 00:00:00"))
                  $trip_content[$value['trip_id']]['driver_end_time_mobile']=$value['driver_end_time_mobile'];
               
                if(empty($trip_content[$value['trip_id']]['driver_end_time_vts']) || ($trip_content[$value['trip_id']]['driver_end_time_vts']=="0000-00-00 00:00:00"))
                  $trip_content[$value['trip_id']]['driver_end_time_vts']=$value['driver_end_time_vts'];
               
                if($trip_content[$value['trip_id']]['morning_ta']==0)
                  $trip_content[$value['trip_id']]['morning_ta']=$value['morning_ta'];

                if($trip_content[$value['trip_id']]['lunch_ta']==0)
                  $trip_content[$value['trip_id']]['lunch_ta']=$value['lunch_ta'];

                if($trip_content[$value['trip_id']]['night_ta']==0)
                  $trip_content[$value['trip_id']]['night_ta']=$value['night_ta'];

                if($trip_content[$value['trip_id']]['night_halt_bill']==0)                  
                  $trip_content[$value['trip_id']]['night_halt_bill']=$value['night_halt_bill'];

                 if(empty($trip_content[$value['trip_id']]['first_on_time']) || ($trip_content[$value['trip_id']]['first_on_time']>$value['first_on_time']))                  
                  $trip_content[$value['trip_id']]['first_on_time']=$value['first_on_time'];

                if(empty($trip_content[$value['trip_id']]['last_off_time']) || ($trip_content[$value['trip_id']]['last_off_time']<$value['last_off_time']))                 
                  $trip_content[$value['trip_id']]['last_off_time']=$value['last_off_time'];

                if($trip_content[$value['trip_id']]['total_on_count'] < $value['total_on_count'])                  
                  $trip_content[$value['trip_id']]['total_on_count']=$value['total_on_count'];

                if($value['trip_id_end']=='1'){                 
                  $trip_content[$value['trip_id']]['trip_id_end']=$value['trip_id_end'];
                  $trip_content[$value['trip_id']]['end_location_vts']=$value['location_vts'];
                  $trip_content[$value['trip_id']]['end_location_mobile']=$value['location_mobile'];
                  $trip_content[$value['trip_id']]['id']=$value['id'];
                  $trip_content[$value['trip_id']]['over_time']=$value['over_time'];

                }

                if($value['day_end']=='yes')                  
                  $trip_content[$value['trip_id']]['day_end']=$value['day_end'];

                if(!empty($value['fuel_recipt_no'])){
                   
                   $fuel_info['fuel_recipt_no']=$value['fuel_recipt_no'];
                  $fuel_info['fuel_total_bill']=$value['fuel_total_bill'];
                  $fuel_info['meter_number']=$value['meter_number'];
                  $fuel_info['location_vts']=$value['location_vts'];
                  $fuel_info['location_mobile']=$value['location_mobile'];
                  $fuel_info['created_time_vts']=$value['created_time_vts'];
                  $fuel_info['created_time_mobile']=$value['created_time_mobile'];
                  $fuel_info['created_time_vts']=$value['created_time_vts'];
                  $trip_content[$value['trip_id']]['fuel_take'][]=$fuel_info;

                }
                if(!empty($value['others_cost'])){
                  $others_cost['others_cost']=$value['others_cost'];
                  $others_cost['others_bill']=$value['others_bill'];
                  $others_cost['others_description']=$value['others_description'];
                  $others_cost['meter_number']=$value['meter_number'];
                  $others_cost['location_vts']=$value['location_vts'];
                  $others_cost['location_mobile']=$value['location_mobile'];
                  $others_cost['created_time_vts']=$value['created_time_vts'];
                  $others_cost['created_time_mobile']=$value['created_time_mobile'];
                  $others_cost['created_time_vts']=$value['created_time_vts'];
                  $trip_content[$value['trip_id']]['others_cost'][]=$others_cost;

                }



                  //$trip_content[$value['trip_id']]['driver_end_time_vts']=$value['driver_end_time_vts'];
                 //$trip_content[$value['trip_id']]['running_km']=$value['running_km'];
              }

             // var_dump($trip_content);
             // echo $i++;
             // echo "</br>";
            }
          
           foreach ($trip_content as $key => $value) {
             $trip_content1[]=$value;
           }
            $this->_sendResponse(200, CJSON::encode(array_reverse($trip_content1)));
           
            return;
             
            break;
            default:
                $this->_sendResponse(501, sprintf(
                    'Mode <b>view</b> is not implemented for model <b>%s</b>',
                    $_GET['model']) );
                Yii::app()->end();
        }
        // Did we find the requested model? If not, raise an error
        if(is_null($model))
            $this->_sendResponse(404, 'No Item found with id '.$_GET['id']);
        else
            $this->_sendResponse(200, CJSON::encode($model));
    }
    public function actionCreate()
    {
         if(!isset($_POST['_token'])){
            switch($_GET['model'])
        {
        // Get an instance of the respective model
        case 'posts':
            $model = new Post;                    
            break;
        case 'feedbackSettings':
            $model = new FeedbackSettings;                    
            break;
        case 'feedbackRating':
            $model = new FeedbackRating;                    
            break;
        case 'fuels':
            $model = new Fuels;                    
            break;
        case 'fuellist':
          $json_list=$_POST['fuellist'];
          $listFU=json_decode($json_list); 
          $listerror=array(); 
          //var_dump($listFU);
          foreach ($listFU as $listkey => $listvalue) {
            $model = new Fuels; 
            $model->unsetAttributes();
            foreach($listvalue as $var=>$value) {
              // Does the model have this attribute 
              if($var!="id") {
                if($model->hasAttribute($var)) {
                  $model->$var = $value;
                } 
              }
                    
            }
            //$model->$id = null;
            if($model->save()) {
              $listvalue->success=1;
              $listerror[]=$listvalue;
            }
            else {
              $listvalue->success=0;
              $listerror[]=$listvalue;
            }
          }           
          $this->_sendResponse(200, CJSON::encode($listerror)); 
          return;                
          break;
        case 'userRating':
            $model = new UserRating;                    
            break;
        case 'logbook':
            $model = new Movements;                    
            break;
        case 'digitalLogBookHistoryList':        
          $json_list = $_POST['digitalLogBookHistoryList'];
          $listDG = json_decode($json_list); 
          $listerror = array();
          $count = count($listDG);
          $sort_array = array();
          if($count > 0) {
            for($i=0;$i<$count;$i++) {
              $min = $i;
              for($j=$i+1;$j<$count;$j++) {                
                if($listDG[$j]->rowID < $listDG[$min]->rowID) {
                  $min = $j;
                }
              }
              if($min!==$i) {
                $swp = $listDG[$i];
                $listDG[$i] = $listDG[$min];
                $listDG[$min] = $swp;

              }
            }
          }
          else{
            $listerror['error'] = "No data found";
            $this->_sendResponse(200, CJSON::encode($listerror));
          }
              
          $bundle = strtotime(date("Y-m-d H:i:s"));   

          foreach ($listDG as $listkey => $listvalue) {//list strat             
            $model = new DigitalLogBookHistory;  
            $model->unsetAttributes();
            $model->create_time_server = date('Y-m-d H:i:s');
              // Try to assign POST values to attributes
              foreach($listvalue as $var=>$value) {
                // Does the model have this attribute? If not raise an error
                if($model->hasAttribute($var)) {
                  $model->$var = $value;
                }                  
              }
              // Check duplicate trip entry
              $_trip_id = $model->trip_id;
              $row = Yii::app()->db->createCommand("Select * FROM tbl_digital_log_book_history WHERE trip_id_end='1' AND trip_id='$_trip_id'")->queryRow();
              if($row) {
                $update_hit_counter=Yii::app()->db->createCommand("UPDATE tbl_digital_log_book_history set hit_counter='".($row["hit_counter"] + 1)."' WHERE id='".$row["id"]."'")->execute();                
                $listvalue->success = 2;              
                $listvalue->message = "The record already exists but still upated with latest";               
                $listerror[] = $listvalue;
               continue;               
              }
              $model->bundle = $bundle;               
              //find last meter             
              $max_meter=$this->maxMeter($model);
              $model->meter_number =  sprintf('%0.2f',(double)$max_meter + $model->running_km);                  
              // Try to save the model
              if($model->save()) {                  
                $listvalue->success = 1;
                $listvalue->message = '';
                $listerror[] =  $listvalue;                  
                $trip_id = $model->trip_id;
                if(($model->trip_id_end == 1 && $model->day_end == 'No')||($model->trip_id_end == 0 && $model->day_end == 'Yes')) {
                    $request_query = "SELECT * FROM  tbl_digital_log_book_history Where ((trip_id_end=1 AND day_end ='No') OR (day_end='Yes' and trip_id_end<>1)) AND bill=0 AND trip_id='$trip_id' order by created_time_mobile DESC";
                    $request_data = Yii::app()->db->createCommand($request_query)->queryRow();
                    $rvalue = $request_data;   
                    if($rvalue) {
                      $requisition_id = $rvalue['requisition_id'];              
                      $user_pin = $rvalue['user_pin'];                               
                      $user_name = $rvalue['user_name'];
                      $Job_level = '';
                      $user_cell = '0';
                      $user_email = '';
                      $user_address = '';
                      if(!empty($user_pin)){
                        $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
                        $staffInfo = CJSON::decode(file_get_contents($uri));
                        $Job_level = $staffInfo[0]['JobLevel'];
                        $user_cell = $staffInfo[0]['MobileNo'];
                        $user_email = $staffInfo[0]['EmailID'];
                        $user_address = $staffInfo[0]['EmailID'];
                      }           
                      $user_department = $rvalue['department'];
                      $vehicle_reg_no = $rvalue['vehicle_reg_no'];
                      $vehicletype_id = $rvalue['vehicle_type_id'];             
                      $driver_pin = $rvalue['driver_pin'];
                      $driver_name = $rvalue['driver_name'];
                      $dutytype_id = $rvalue['dutytype_id'];
                      $duty_day = $rvalue['duty_day'];
                      $offline = strpos($rvalue['message_log'],"Offline");
                     
                      if(!empty($rvalue['driver_start_time_mobile']) && $rvalue['driver_start_time_mobile']!="0000-00-00 00:00:00") {
                          $rvalue['driver_start_time_mobile'];
                          $statr_DT = explode(" ",$rvalue['driver_start_time_mobile']);  
                          $start_date = $statr_DT[0];
                          if(!empty($statr_DT[1])) {
                             $start_time = $statr_DT[1];
                          }
                      }
                      else {
                          $statr_DT = explode(" ", $rvalue['driver_start_time_vts']);
                            $start_date=$statr_DT[0];
                         if(!empty($statr_DT[1])) {
                            $start_time=$statr_DT[1];
                         }
                      }
                      if(!empty($rvalue['driver_end_time_mobile']) && $rvalue['driver_end_time_mobile']!="0000-00-00 00:00:00") {
                        $end_DT = explode(" ", $rvalue['driver_end_time_mobile']);
                        $end_date = $end_DT[0];
                        if(!empty($end_DT[1])){
                            $end_time = $end_DT[1];
                         }
                      }
                      else {
                        $end_DT = explode(" ", $rvalue['driver_end_time_vts']);
                        $end_date = $end_DT[0];
                        if(!empty($end_DT[1])){
                          $end_time = $end_DT[1];
                        }
                      }
                      $start_time_array=explode(":",  $start_time);
                      if(isset($start_time_array[2])) {
                        $start_time_array[2] = "00";
                        $start_time = implode(":", $start_time_array);
                      }
                      $end_time_array = explode(":",  $end_time);
                      if(isset($end_time_array[2])){
                        $end_time_array[2]="00";
                        $end_time = implode(":", $end_time_array);
                      }
                      $a = new DateTime($start_time);
                      $b = new DateTime($end_time);
                      $interval = $a->diff($b);
                      $total_time = $interval->format("%H:%I");
                      $duty_day = $rvalue['duty_day'];
                      $end_point = empty($rvalue['location_vts'])?$rvalue['location_mobile']:$rvalue['location_vts'];
                      $end_meter = $rvalue['meter_number'];
                      $sql = "SELECT * FROM tbl_digital_log_book_history Where  trip_id ='".$rvalue['trip_id']."' AND bill=0 order by created_time_mobile ASC";           
                      $sql_rs = Yii::app()->db->createCommand($sql)->queryAll();
                      $start_point = empty($sql_rs[0]['location_vts'])?$sql_rs[0]['location_mobile']:$sql_rs[0]['location_vts'];
                      $start_meter = $sql_rs[0]['meter_number'];               
                      $night_halt = 0;
                      $nh_amount = 0;
                      $morning_ta = 0;
                      $lunch_ta = 0;
                      $night_ta = 0;
                      foreach ($sql_rs as $key => $value) {
                        if($value['night_halt_bill']!=0) {
                          $night_halt = 1;
                          $nh_amount = $value['night_halt_bill'];
                        }
                        if($value['morning_ta']!=0) {
                            $morning_ta = $value['morning_ta'];
                        }
                        if($value['lunch_ta']!=0) {
                            $lunch_ta = $value['lunch_ta'];
                        }
                        if($value['night_ta']!=0){
                            $night_ta = $value['night_ta'];
                        }
                        $id_history = $value['id'];
                        //$sql_update_history = "UPDATE tbl_digital_log_book_history SET bill=1  Where  id='$id_history'";        
                        //$sql_update_history_rs = Yii::app()->db->createCommand($sql_update_history)->execute();                
                      }
                      $sup_mile_amount = 0;
                      $total_run = $end_meter-$start_meter; 
                      $rph = $rvalue['running_per_km_rate'];
                      $sql_dutytypes = "SELECT * FROM tbl_dutytypes WHERE id=".$dutytype_id;
                      $sql_dutytypes_rs = Yii::app()->db->createCommand($sql_dutytypes)->queryRow();
                      $sql_vehicletypes = "SELECT * FROM tbl_vehicletypes WHERE id=".$vehicletype_id;
                      $sql_vehicletypes_rs = Yii::app()->db->createCommand($sql_vehicletypes)->queryRow();           
                      if($dutytype_id==1) {
                        $bill_amount = $sql_vehicletypes_rs['rate_per_km']*$total_run;
                        $bill_amount = $bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
                      }
                      else if($dutytype_id==2) {
                        $bill_amount = $sql_vehicletypes_rs['rate_per_km_personal']*$total_run;
                        $bill_amount = $bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
                      }
                      else if($dutytype_id==3) {            
                        $hour = explode(":", $total_time);
                        $hour_bill = $hour[0]*$sql_vehicletypes_rs['rph_visitor'];
                        if($sql_vehicletypes_rs['minimum_rate']>$hour_bill) {
                            $bill_amount = $sql_vehicletypes_rs['rate_per_km_external']*$total_run;
                            $bill_amount = $sql_vehicletypes_rs['minimum_rate']+$bill_amount* $sql_dutytypes_rs['service_charge'];
                        }
                        else{
                            $bill_amount = $sql_vehicletypes_rs['rate_per_km_external']*$total_run;
                            $bill_amount = $hour_bill+$bill_amount* $sql_dutytypes_rs['service_charge'];
                        }
                      }
                      else if($dutytype_id==4){
                        $bill_amount = $sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
                        $bill_amount = $bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
                      }
                      else if($dutytype_id==5){
                        $bill_amount = $sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
                        $bill_amount = $bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
                      }
                      else{
                        $bill_amount = $rph*$total_run;
                      }             
                  $over_hour_minute = explode(":", $rvalue['over_time']);
                  if(isset($over_hour_minute[1])) {
                      $total_ot_hour = $over_hour_minute[0].".".$over_hour_minute[1];
                    }
                    else {
                      $total_ot_hour = $rvalue['over_time'];
                    }
                    $start_time_hour_minute = explode(":", $start_time);
                    if(isset($start_time_hour_minute[1])) {
                      $start_time = $start_time_hour_minute[0].".".$start_time_hour_minute[1];
                    }
                    else {
                      $start_time = $start_time;
                    }
                    $end_time_hour_minute = explode(":", $end_time);
                    if(isset($end_time_hour_minute[1])) {
                      $end_time = $end_time_hour_minute[0].".".$end_time_hour_minute[1];
                    }
                    else{
                      $end_time = $end_time;
                    }
                    $total_ot_amount = $rvalue['over_time_bill'];
                    $created_by = $rvalue['created_by'];
                    $created_time = date("Y-m-d h:i:s");
                    $trip_id = $rvalue['trip_id'];

                    if($duty_day=='Official') {
                      $duty_day = "Office Day";
                    }             

                $sql_rs = Yii::app()
                ->db
                ->createCommand()
                ->insert(
                  'tbl_digital_log_book',
                  array(
                  'id'=>'',
                  'requisition_id'=>$requisition_id,
                  'user_pin'=>$user_pin,
                  'user_name'=>$user_name,
                  'user_level'=>$Job_level,
                  'user_dept'=>$user_department,
                  'email'=>$user_email,
                  'user_cell'=>$user_cell,
                  'user_address'=>'',
                  'vehicle_reg_no'=>$vehicle_reg_no,
                  'vehicletype_id'=>$vehicletype_id,
                  'vehicle_location'=>$start_point,
                  'driver_pin'=>$driver_pin,
                  'driver_name'=>$driver_name,
                  'helper_pin'=>'',
                  'helper_name'=>'',
                  'helper_ot'=>'',
                  'dutytype_id'=>$dutytype_id,
                  'dutyday'=>$duty_day,
                  'start_date'=>$start_date,
                  'end_date'=>$end_date,
                  'start_time'=>$start_time,
                  'end_time'=>$end_time,
                  'total_time'=>$total_time,
                  'start_point'=>$start_point,
                  'end_point'=>$end_point,
                  'start_meter'=>$start_meter,
                  'end_meter'=>$end_meter,
                  'total_run'=>$total_run,
                  'rph'=>$rph,
                  'night_halt'=>$night_halt,
                  'nh_amount'=>$nh_amount,
                  'morning_ta'=>$morning_ta,
                  'lunch_ta'=>$lunch_ta,
                  'night_ta'=>$night_ta,
                  'sup_mile_amount'=>'',
                  'bill_amount'=>$bill_amount,
                  'total_ot_hour'=>$total_ot_hour,
                  'total_ot_amount'=>$total_ot_amount,
                  'created_by'=>$created_by,
                  'created_time'=>$created_time,
                  'trip_id'=>$trip_id
                ));           
              }
              else {
                $sql = "DELETE FROM tbl_digital_log_book_history WHERE trip_id='".$model->trip_id."' order by create_time_server DESC Limit 1";
                $sql_rs = Yii::app()->db->createCommand($sql)->execute();
              }                     
            }
            else {  

            }
          } 
          else {
            $listvalue->success = 0;
            foreach($model->errors as $attribute=>$attr_errors) {
              foreach($attr_errors as $attr_error){
                $listvalue->message .= $attr_error;
              }
            }
              $listerror[]=$listvalue;
          } 
        }
        $this->_sendResponse(200, CJSON::encode($listerror));
        return; 
        break;

        case 'digitalLogBookHistory':
          $model=$this->digitatLogbookInsert($_POST);           
        return;
        break;

        case 'dailyVehicleCheck':   
              if(isset($_POST['dailyVehicleChecklist'])){
                $json_list=$_POST['dailyVehicleChecklist'];
                $listDC=json_decode($json_list); 
                if(count($listDC)>0){
                  foreach ($listDC as $key => $value) {
                   $result[]=$this->dailyVehicleCheck($value,1);
                  }

                }
                else{
                  $result=array('id'=>0,
                                'success'=>0,
                                'message'=> 'No Data Found', 
                      );
                  //$sql="SELECT * FROM tbl_daily_vehicle_check Limit 5";
                  //$result=Yii::app()->db->CreateCommand($sql)->queryAll();
               }

              } 
              else{           
                 $result=$this->dailyVehicleCheck($_POST,1);
              }
             $this->_sendResponse(200, CJSON::encode($result));
            return;               
            break; 
        case 'vehiclePlaceHistory':       
            $model = new VehiclePlaceHistory;                    
            break;
        case 'logError':   
              if(isset($_POST['logError'])){
                $json_list=$_POST['logError'];
                $listDC=json_decode($json_list);
                                
                if(count($listDC)>0){
                  foreach ($listDC as $key => $value) {
                   $result[]=$this->dailyVehicleCheck($value,2);
                  }

                }
                else{
                  $result=array('id'=>0,
                                'success'=>0,
                                'message'=> 'No Data Found', 
                      );
                  //$sql="SELECT * FROM tbl_error_log Limit 5";
                  //$result=Yii::app()->db->CreateCommand($sql)->queryAll();
               }

              } 
              else{           
                 $result=$this->dailyVehicleCheck($_POST,2);
              }
             $this->_sendResponse(200, CJSON::encode($result));
            return;               
            break; 
        case 'tripIdEnd':

        $tripId=$_POST['tripidend'];
        $sql="SELECT* FROM tbl_digital_log_book_history WHERE trip_id='$tripId' order by  created_time_mobile  ASC";
            $sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
           
            $total_fuel_bill=0;
            foreach ($sql_rs as $key => $value) {
              $total_fuel_bill+=$value['fuel_total_bill'];
              $end_location_vts=$value['location_vts'];
              $end_location_mobile=$value['location_mobile'];
              $driver_start_time_vts=$value['driver_start_time_vts'];
              $driver_start_time_mobile=$value['driver_start_time_mobile'];
              $driver_end_time_vts=$value['driver_end_time_vts'];
              $driver_end_time_mobile=$value['driver_end_time_mobile'];
              $running_km=$value['running_km'];
              $vehicle_reg_no=$value['vehicle_reg_no'];
            }
            $body="Dear Concern\n\n";            
            $body.="Your trip details: \n\n";
            
            $req=$sql_rs[0]['requisition_id'];
            $start_palace=empty($sql_rs[0]['location_vts'])?$sql_rs[0]['location_mobile']:$sql_rs[0]['location_vts'];

            $end_palce=empty($end_location_vts)?$end_location_mobile:$end_location_vts;
          
            if(!empty($req)||$req!=0){
              $body.="Requisition: $req \n";
              $sub="Trip summery of requisition: ".$req." on ".date("Y-m-d");
            }
            else{
                $sub="Trip summery of ".$start_palace." to ".$end_palce." route on ".date("Y-m-d");
            }

          $date_start=empty($driver_start_time_mobile)?$driver_start_time_vts:$driver_start_time_mobile;
          $date_end=empty($driver_end_time_mobile)?$driver_end_time_vts:$driver_end_time_mobile;
            $vehicle=$vehicle_reg_no;
            $runn_km=$running_km;
            $body.="Vehicle No: $vehicle \n";
            $body.="Star Date & Time: $date_start \n";
            $body.="End Date & Time: $date_end \n";
            $body.="Start place: $start_palace \n";
            $body.="End Place : $end_palce\n";
            $body.="Total KM: $runn_km \n";
            $body.="Total Fuel Bill: $total_fuel_bill \n\n";
            $body.="Thanks \n\n";
            $body.="Brac Transport Department\n";
            $email='ehsan@nexkraft.com';
           $this->sendMailIcress($email,$body,$sub);
            $cell="01647040520";
            $message="Trip Vehicle No: $vehicle, Star: $start_palace ($date_start), End: $end_palce ($date_end), KM Run: $runn_km and Fuel Bill: $total_fuel_bill";
            $this->sendSMS($cell,$message);
          

           // $email='mehedi.mha@brac.net';
            //$cell="01616505036";
           
            //$this->sendMailIcress($email,$body,$sub);
            //$this->sendSMS($cell,$message);

          //$uri = "http://api.brac.net/v1/staffs/".$model->user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";

           //$staffInfo = CJSON::decode(file_get_contents($uri));

           // $user_cell = $staffInfo[0]['MobileNo'];
           // $user_email = $staffInfo[0]['EmailID'];
            //$this->sendMailIcress($user_email,$body,$sub);
            //$this->sendSMS($user_cell,$message);

        return;
        break;
        case 'sendsms':
        $models = $this->sendSMS($_POST['cell'],$_POST['message']);
        return;
        break;
        case 'holiday':
                $i=0;
                $count_day=0;
                $date_list_orde=array();
                 $date_start=$_POST['date_start'];
                 $date_end=$_POST['date_end'];
                 //$date=date("Y-m-d");
                 $criteria = new CDbCriteria();
                 $criteria->addCondition('end_date >= "'.$date_start.'" AND NOT start_date > "'.$date_end.'"');
                $criteria->order = 'start_date';
                 $models = Holidays::model()->findAll($criteria);
                 
                 function dateCount($d1,$d2){
                        $datetime1 = date_create($d1);
                        $datetime2 = date_create($d2);
                        $interval = date_diff($datetime1, $datetime2);
                        $count_day1= $interval->format('%a')+1;
                        return $count_day1;
                 }
                 function addOneDay($d){
                    $date_add = new DateTime($d);
                    $date_add->add(new DateInterval('P1D'));
                       $date_add->format('Y-m-d');    
                    return $date_add->format('Y-m-d');
                 }
                 
                 if($models){
                    foreach ($models as  $value) {
                        if($date_start<=$date_end){
                         if($date_start<=$value['start_date']  && $date_end<=$value['end_date']){
                            $count_day1=dateCount($value['start_date'],$date_end);
                            $count_day=$count_day1+$count_day;
                           $date_start=addOneDay($date_end);                  
                        }
                        else if($date_start>=$value['start_date']  && $date_end<=$value['end_date']){
                            $count_day1=dateCount($date_start,$date_end);
                            $count_day=$count_day1+$count_day;
                            
                            $date_start=addOneDay($date_end); 
                           
                           
                        }
                        else if($date_start>=$value['start_date']  && $date_end>=$value['end_date'] && $date_start<=$value['end_date']){
                            
                          $count_day1=dateCount($date_start,$value['end_date']);
                              $count_day=$count_day1+$count_day;
                            
                             $date_start=addOneDay($value['end_date']); 
                           
                        }
                        else if($date_start<=$value['start_date']  && $date_end>=$value['end_date']){
                            
                          $count_day1=dateCount($value['start_date'],$value['end_date']);
                             $count_day=$count_day1+$count_day;
                            
                              $date_start=addOneDay($value['end_date']); 
                           
                        }
                        else{

                        }
                    }

                        
                        

                   } 
                 }
               echo $count_day;
                 return;
                break;
              case 'holidaydate':               
                
                 $date_start=$_POST['date_start'];
                  $date_end=$_POST['date_end'];
                 //$date=date("Y-m-d");
                 $sql="SELECT * FROM tbl_holidays WHERE start_date>='".$date_start."' AND end_date <='".$date_end."'";
                $sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
               // echo "<pre>";
                 //var_dump($sql_rs);    
                 // echo "</pre>"; 
                   $this->_sendResponse(200, CJSON::encode($sql_rs));             
                 return;
                break;
              case 'holidayCheck':
                
                
                 $date_start=$_POST['date_start'];
                  $date_end=$_POST['date_end'];
                 //$date=date("Y-m-d");
                 $sql="SELECT * FROM tbl_holidays WHERE start_date>='".$date_start."' AND end_date <='".$date_end."'";
                $sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
               // echo "<pre>";
                 //var_dump($sql_rs);    
                 // echo "</pre>"; 
                   $this->_sendResponse(200, CJSON::encode($sql_rs));             
                 return;
                break;
              case 'holidayRangeStatus':
                 $date_start=$_POST['date_start'];
                 $date_end=$_POST['date_end'];                 
                 $criteria = new CDbCriteria();
                 $criteria->addCondition('end_date >= "'.$date_start.'" AND NOT start_date > "'.$date_end.'"');
                 $criteria->order = 'start_date';                 
                 $models = Holidays::model()->findAll($criteria);
                 $date_array=array();
                for($i=$date_start; $i<=$date_end; $i){                   
                   $date_array[$i]=0;  
                   $date_step=date_create($i);
                   $F_S=date_format($date_step,"D"); 
                   if($F_S=="Fri" ||$F_S=="Sat"){$date_array[$i]=1;}
                   $i = date('Y-m-d', strtotime($i.' +1 day'));
                 }
               if(count($models)>0){
                   foreach ($models as $key => $value) {
                      for($i=$value['start_date']; $i<=$value['end_date']; $i){ 
                          if(isset($date_array[$i])){                  
                            $date_array[$i]=1; 
                          }                                      
                       $i = date('Y-m-d', strtotime($i.' +1 day'));
                      }
                   }
                }
                if(count($date_array)<=0){
                  $date_array=array(''=>null);
                }
                $this->_sendResponse(200, CJSON::encode($date_array));
                return;
              break;
              case 'imei':
                $imei=$_POST['imei_no'];            
                $model = Vehicles::model()->findByAttributes(array('imei_no'=> $imei));
                if($model)
                $this->_sendResponse(200, CJSON::encode($model));
                else echo "0";
                return;
             break;
            case 'vehiclereg':
              $reg=$_POST['vehicle_reg'];            
              $model = Vehicles::model()->model()->findByPk($reg);
              if($model)
              $this->_sendResponse(200, CJSON::encode($model));
              else echo "0";
              return;
            break;
             case 'tripend':
                $log_id=$_POST['logid'];
                $trip_id=$_POST['tripid'];

                //var_dump($_POST);
                /*$query="SELECT running_per_km_rate,meter_number, DATE(created_time_mobile) as create_date, morning_ta, lunch_ta, night_ta, night_halt_bill, fuel_total_bill, requisition_id, user_pin, department, user_name,vehicle_reg_no, vehicle_type_id, location_mobile,driver_pin,dutytype_id,duty_day,DATE(driver_start_time_mobile) as start_date, TIME(driver_start_time_mobile) as start_time,DATE(driver_end_time_mobile) as end_date,TIME(driver_end_time_mobile) as end_time, driver_name FROM tbl_digital_log_book_history where login_id='$log_id' AND trip_id='$trip_id'";
                $models=Yii::app()->db->createCommand($query)->queryAll(); 
                
                $logBook=array();
                $total=0;
                $fuel_total=0;

                $meterer_reading_pre=0;
                $meter_per_km_hour=0;
                $meterer_total=0;
                $meter_total_rate=0; 

                $date_ta_da=array();
                $ta=0;
                $night_halt=0;              
                $i=0;
               if($models) {
                    foreach ($models as $key => $value) {
                        // meter Calculation Start                 

                     if(!empty($value['running_per_km_rate']) && $value['running_per_km_rate']!=0)
                            $meter_per_km_hour=$value['running_per_km_rate'];

                     if($i==0){
                            $meterer_reading_pre= $value['meter_number'];
                            //  start_date Calculation Start   
                            if(!empty($value['start_date']))
                            $logBook['add']['start_date']=$value['start_date'];
                        //      start_date Calculation END

                        //  start_time Calculation Start
                        if(!empty($value['start_time']))
                            $logBook['add']['start_time']=$value['start_time'];
                        //      start_time Calculation END
                        // start_point Calculation Start
                     if(!empty($value['location_mobile']))
                            $logBook['add']['start_point']=$value['location_mobile'];
                    //   start_point Calculation END

                         // start_meter Calculation Start                     
                            $logBook['add']['start_meter']=$meterer_reading_pre;
                    //   start_meter Calculation END               

                        
                     }
                     if($value['meter_number']!=0 && !empty($value['meter_number'])){
                       $meterer_total=$meterer_total+$value['meter_number']-$meterer_reading_pre; 
                       $meterer_reading_pre= $value['meter_number'];
                      }
                    // meter Calculation End
                    // TA-DA Calculation Start
                      //$date_ta_da=$value['create_date'];
                      if(!empty($value['morning_ta']) && $value['morning_ta']!=0){                
                      $date_ta_da['morning']['date']=$value['create_date'];
                      $date_ta_da['morning']['ta']=$value['morning_ta'];
                      $ta=$ta+$value['morning_ta'];
                     
                      }
                      if(!empty($value['lunch_ta']) && $value['lunch_ta']!=0){
                      $date_ta_da['lunch']['date']=$value['create_date'];
                      $date_ta_da['lunch']['ta']=$value['lunch_ta'];
                      $ta=$ta+$value['lunch_ta'];
                      
                      }
                      if(!empty($value['night_ta']) && $value['night_ta']!=0){ 
                      $date_ta_da['night']['date']=$value['create_date'];
                      $date_ta_da['night']['ta']=$value['night_ta'];
                      $ta=$ta+$value['night_ta'];
                     
                      }
                     if(!empty($value['night_halt_bill']) && $value['night_halt_bill']!=0){
                      $date_ta_da['nightHalt']['date']=$value['create_date'];
                      $date_ta_da['nightHalt']['ta']=$value['night_halt_bill'];
                      if(isset($date_ta_da['morning']) && !empty($date_ta_da['morning']) && $date_ta_da['morning']['date']== $date_ta_da['nightHalt']['date']){
                        $ta=$ta-$date_ta_da['morning']['ta'];
                        
                      }
                      if(isset($date_ta_da['lunch']) && !empty($date_ta_da['lunch']) && $date_ta_da['lunch']['date']== $date_ta_da['nightHalt']['date']){
                        $ta=$ta-$date_ta_da['lunch']['ta'];
                        
                      }
                      if(isset($date_ta_da['night']) && !empty($date_ta_da['night']) && $date_ta_da['night']['date']== $date_ta_da['nightHalt']['date']){
                        $ta=$ta-$date_ta_da['night']['ta'];
                        
                      }

                      $ta=$ta+$value['night_halt_bill'];
                      $night_halt++;
                      

                     }                      
                    // TA-DA Calculation End

                     // Fuel Calculation Start
                     if(!empty($value['fuel_total_bill']) && $value['fuel_total_bill']!=0)
                            $fuel_total=$fuel_total+$value['fuel_total_bill'];
                    // Fuel Calculation END
                    // requisition_id Calculation Start
                     if(!empty($value['requisition_id']) && $value['requisition_id']!=0)
                            $logBook['add']['requisition_id']=$value['requisition_id'];
                    // requisition_id Calculation END
                    // user_pin Calculation Start
                     if(!empty($value['user_pin']))
                            $logBook['add']['user_pin']=$value['user_pin'];
                    // user_pin Calculation END
                    // user_name Calculation Start
                     if(!empty($value['user_name']))
                            $logBook['add']['user_name']=$value['user_name'];
                    //  user_name Calculation END
                    // department Calculation Start
                     if(!empty($value['department']))
                            $logBook['add']['user_dept']=$value['department'];
                    //   department Calculation END
                    // department Calculation Start
                     if(!empty($value['vehicle_reg_no']))
                            $logBook['add']['vehicle_reg_no']=$value['vehicle_reg_no'];
                    //   department Calculation END
                    // vehicle_type_id Calculation Start
                     if(!empty($value['vehicle_type_id']) && $value['vehicle_type_id']!=0)
                            $logBook['add']['vehicletype_id']=$value['vehicle_type_id'];
                    //   vehicle_type_id Calculation END
                    // location_mobile Calculation Start
                     if(!empty($value['location_mobile']))
                            $logBook['add']['vehicle_location']=$value['location_mobile'];
                            $logBook['add']['end_point']=$value['location_mobile'];                       
                    //   location_mobile Calculation END
                    // driver_pin Calculation Start
                     if(!empty($value['driver_pin']) && $value['driver_pin']!=0)
                            $logBook['add']['driver_pin']=$value['driver_pin'];
                    //   driver_pin Calculation END
                    // driver_name Calculation Start
                     if(!empty($value['driver_name']))
                            $logBook['add']['driver_name']=$value['driver_name'];
                    //  driver_name Calculation END
                    // dutytype_id Calculation Start
                     if(!empty($value['dutytype_id']) && $value['dutytype_id']!=0)
                            $logBook['add']['dutytype_id']=$value['dutytype_id'];
                    //   dutytype_id Calculation END
                    //  duty_day Calculation Start
                     if(!empty($value['duty_day']))
                            $logBook['add']['dutyday']=$value['duty_day'];
                    //      duty_day Calculation END
                    //  end_date Calculation Start
                    if(!empty($value['end_date']))
                            $logBook['add']['end_date']=$value['end_date'];
                        //      end_date Calculation END

                        //  end_time Calculation Start
                    if(!empty($value['end_time']))
                            $logBook['add']['end_time']=$value['end_time'];
                        //      end_time Calculation END
                        //  end_meter Calculation Start
                            $logBook['add']['end_meter']=$meterer_reading_pre;
                        //      end_meter Calculation END

                        
                       $i++;
                } 
                //total_run  Calculation Start
                    
                $logBook['add']['total_run']=$meterer_total;
            //       total_run  Calculation END
            //rph  Calculation Start
                    
                $logBook['add']['rph']=$meter_per_km_hour;
            //       rph  Calculation END
            //night_halt  Calculation Start
                    
                $logBook['add']['night_halt']=$night_halt;
            //      night_halt Calculation END
                
                  
                $meter_total_rate=$meterer_total*$meter_per_km_hour;                               
                $total=$ta+$fuel_total+$meter_total_rate;
                //night_halt  Calculation Start
                    
                $logBook['add']['bill_amount']=$total;
            //      night_halt Calculation END
            //created_by  Calculation Start
                    
                $logBook['add']['created_by']=$logBook['add']['driver_pin'];
            //      created_by Calculation END

                    //$model1=new Movements;
                    //$model1->attributes=$logBook['add'];
                    //if($model1->save()){
                        echo '1';
                    //}
                    //else {
                        //echo '0';
                    //}
                 
               
            }
            else{
               echo '0';
                         

            }*/
            $request_query="SELECT * FROM  tbl_digital_log_book_history Where ((trip_id_end=1 AND day_end ='No') OR (day_end='Yes' and trip_id_end<>1)) AND bill=0 AND trip_id='$trip_id' order by created_time_mobile DESC";
            $request_data=Yii::app()->db->createCommand($request_query)->queryRow();
            $rvalue=$request_data;            
           
            if($rvalue){
            $requisition_id=$rvalue['requisition_id'];              
              $user_pin=$rvalue['user_pin'];
              $user_name=$rvalue['user_name'];
              $Job_level='';
              $user_cell='0';
              $user_email='';
              $user_address='';
              if(!empty($user_pin)){
               $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
               $staffInfo = CJSON::decode(file_get_contents($uri));
               $Job_level = $staffInfo[0]['JobLevel'];
               $user_cell=$staffInfo[0]['MobileNo'];
              $user_email=$staffInfo[0]['EmailID'];
              $user_address=$staffInfo[0]['EmailID'];
              }           
              $user_department=$rvalue['department'];
              $vehicle_reg_no=$rvalue['vehicle_reg_no'];
              $vehicletype_id=$rvalue['vehicle_type_id'];             
              $driver_pin=$rvalue['driver_pin'];
              $driver_name=$rvalue['driver_name'];
              $dutytype_id=$rvalue['dutytype_id'];
              $duty_day=$rvalue['duty_day'];
              //echo $valuetr['driver_start_time_vts'];
              if(!empty($rvalue['driver_start_time_vts']) && $rvalue['driver_start_time_vts']!="0000-00-00 00:00:00"){
                 $rvalue['driver_start_time_vts'];
                $statr_DT=explode(" ",$rvalue['driver_start_time_vts']);
                
                $start_date=$statr_DT[0];
                if(!empty($statr_DT[1])){
                  $start_time=$statr_DT[1];

                }
              }
              else{
                $statr_DT=explode(" ", $rvalue['driver_start_time_mobile']);
                $start_date=$statr_DT[0];
                if(!empty($statr_DT[1]))
                $start_time=$statr_DT[1];
              }
              if(!empty($rvalue['driver_end_time_vts']) && $rvalue['driver_end_time_vts']!="0000-00-00 00:00:00"){
                $end_DT=explode(" ", $rvalue['driver_end_time_vts']);
                $end_date=$end_DT[0];
                if(!empty($end_DT[1]))
                $end_time=$end_DT[1];
              }
              else{
                $end_DT=explode(" ", $rvalue['driver_end_time_mobile']);
                $end_date=$end_DT[0];
                if(!empty($end_DT[1]))
                $end_time=$end_DT[1];
              }
            $start_time_array=explode(":",  $start_time);
            if(isset($start_time_array[2])){
                    $start_time_array[2]="00";
                    $start_time=implode(":", $start_time_array);
              }
            $end_time_array=explode(":",  $end_time);
            if(isset($end_time_array[2])){
                    $end_time_array[2]="00";
                    $end_time=implode(":", $end_time_array);
                  }


              $a = new DateTime($start_time);
              $b = new DateTime($end_time);
              $interval = $a->diff($b);
              $total_time=$interval->format("%H:%I");
              $duty_day=$rvalue['duty_day'];
              $end_point=empty($rvalue['location_vts'])?$rvalue['location_mobile']:$rvalue['location_vts'];
              $end_meter=$rvalue['meter_number'];




              $sql="SELECT * FROM tbl_digital_log_book_history Where  trip_id ='".$rvalue['trip_id']."' AND bill=0 order by created_time_mobile ASC";
           
              $sql_rs=Yii::app()->db->createCommand($sql)->queryAll();

               $start_point=empty($sql_rs[0]['location_vts'])?$sql_rs[0]['location_mobile']:$sql_rs[0]['location_vts'];
               $start_meter=$sql_rs[0]['meter_number'];
               
               $night_halt=0;
               $nh_amount=0;
               $morning_ta=0;
               $lunch_ta=0;
               $night_ta=0;
              foreach ($sql_rs as $key => $value) {
                if($value['night_halt_bill']!=0){
                  $night_halt=1;
                  $nh_amount=$value['night_halt_bill'];
                }
                if($value['morning_ta']!=0){
                  $morning_ta=$value['morning_ta'];
                }
                if($value['lunch_ta']!=0){
                   $lunch_ta=$value['lunch_ta'];
                }
                if($value['night_ta']!=0){
                  $night_ta=$value['night_ta'];
                }
                $id_history=$value['id'];
             //$sql_update_history="UPDATE tbl_digital_log_book_history SET bill=1  Where  id='$id_history'";        
             //$sql_update_history_rs=Yii::app()->db->createCommand($sql_update_history)->execute(); 
                
              }
            $sup_mile_amount=0;
            $total_run=$end_meter-$start_meter; 
            $rph=$rvalue['running_per_km_rate'];

            $sql_dutytypes="SELECT * FROM tbl_dutytypes WHERE id=".$dutytype_id;
            $sql_dutytypes_rs=Yii::app()->db->createCommand($sql_dutytypes)->queryRow();

            $sql_vehicletypes="SELECT * FROM tbl_vehicletypes WHERE id=".$vehicletype_id;
            $sql_vehicletypes_rs=Yii::app()->db->createCommand($sql_vehicletypes)->queryRow();
             
           if($dutytype_id==1){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
           else if($dutytype_id==2){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_personal']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
           else if($dutytype_id==3){            
            $hour=explode(":", $total_time);
            $hour_bill=$hour[0]*$sql_vehicletypes_rs['rph_visitor'];
            if($sql_vehicletypes_rs['minimum_rate']>$hour_bill){
                $bill_amount=$sql_vehicletypes_rs['rate_per_km_external']*$total_run;
                $bill_amount=$sql_vehicletypes_rs['minimum_rate']+$bill_amount* $sql_dutytypes_rs['service_charge'];
            }
            else{
                 $bill_amount=$sql_vehicletypes_rs['rate_per_km_external']*$total_run;
                $bill_amount=$hour_bill+$bill_amount* $sql_dutytypes_rs['service_charge'];
            }
           }
           else if($dutytype_id==4){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
            else if($dutytype_id==5){
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
            $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
           }
           else{
            $bill_amount=$rph*$total_run;
           }
            //echo $bill_amount;
           $over_hour_minute=explode(":", $rvalue['over_time']);
           if(isset($over_hour_minute[1])){
            $total_ot_hour=$over_hour_minute[0].".".$over_hour_minute[1];
            }
           else{
            $total_ot_hour=$rvalue['over_time'];
           }

           $start_time_hour_minute=explode(":", $start_time);
           if(isset($start_time_hour_minute[1])){
            $start_time=$start_time_hour_minute[0].".".$start_time_hour_minute[1];
            }
           else{
            $start_time=$start_time;
           }

           $end_time_hour_minute=explode(":", $end_time);
           if(isset($end_time_hour_minute[1])){
            $end_time=$end_time_hour_minute[0].".".$end_time_hour_minute[1];
            }
           else{
            $end_time=$end_time;
           }

            $total_ot_amount=$rvalue['over_time_bill'];
            $created_by=$rvalue['created_by'];
            $created_time=date("Y-m-d h:i:s");
            $trip_id=$rvalue['trip_id'];
            if($duty_day=='Official') {
               $duty_day="Office Day";
            }

            $sql="INSERT INTO tbl_digital_log_book(
            id,
            requisition_id,
            user_pin,
            user_name,
            user_level,
            user_dept,
            email,
            user_cell,
            user_address,
            vehicle_reg_no,
            vehicletype_id,
            vehicle_location,
            driver_pin,
            driver_name,
            helper_pin,
            helper_name,
            helper_ot,
            dutytype_id,
            dutyday,
            start_date,
            end_date,
            start_time,
            end_time,
            total_time,
            start_point,
            end_point,
            start_meter,
            end_meter,
            total_run,
            rph,
            night_halt,
            nh_amount,
            morning_ta,
            lunch_ta,
            night_ta,
            sup_mile_amount,
            bill_amount,
            total_ot_hour,
            total_ot_amount,
            created_by,
            created_time,
            trip_id)
             VALUES(
             '".$i."',
             '".$requisition_id."',
             '".$user_pin."',
             '".$user_name."',
             '".$Job_level."',
             '".$user_department."',
             '".$user_email."',
             '".$user_cell."',
             '',
             '".$vehicle_reg_no."',
             '".$vehicletype_id."',
             '".$start_point."',
             '".$driver_pin."',
             '".$driver_name."',
             '',
             '',
             '',
             '".$dutytype_id."',
             '".$duty_day."',
             '".$start_date."',
             '".$end_date."',
             '".$start_time."',
             '".$end_time."',
             '".$total_time."',
             '".$start_point."',
             '".$end_point."',
             '".$start_meter."',
             '".$end_meter."',
             '".$total_run."',
             '".$rph."',
             '".$night_halt."',
             '".$nh_amount."',
             '".$morning_ta."',
             '".$lunch_ta."',
             '".$night_ta."',
             '',
             '".$bill_amount."',
             '".$total_ot_hour."',
             '".$total_ot_amount."',
             '".$created_by."',
             '".$created_time."',
             '".$trip_id."'
         )";
         $sql_rs=Yii::app()->db->createCommand($sql)->execute(); 

         echo 1;    
        }
        else {
          echo 0;
        }
            return; 
            break; 
        case 'driverobjection':       
            $model = new DigitalLogBookHistory;  
            $driver_objection=$_POST['driverobjection'];
            $trip_id=$_POST['tripid'];
            $query="SELECT* FROM tbl_digital_log_book_history WHERE trip_id='".$trip_id."' AND  trip_id_end=1";
            $model=Yii::app()->db->createCommand($query)->queryRow();
          
            if($model){
              $query="UPDATE tbl_digital_log_book_history SET objection='".$driver_objection."' WHERE id=".$model['id'];
              echo $update=Yii::app()->db->createCommand($query)->execute();
              
          }
           else
            echo 0;
          return;          
          break;

       case 'initialapi': 
         $vehicle=$_POST['vehicle'];
        $response=array('bill_rate'=>'','trip_info'=>'');
        $models = Billrates::model()->findAll();
        $response['bill_rate']=$models;
        // trip-end
        $sql="SELECT * FROM tbl_digital_log_book_history WHERE trip_id_end<>1 and vehicle_reg_no='$vehicle' and day_end='Yes' order by created_time_mobile DESC";
        $command = Yii::app()->db->createCommand($sql)->queryRow();       
                
       if($command){
        $sql="SELECT trip_id FROM tbl_digital_log_book_history WHERE trip_id_end=1 and vehicle_reg_no='$vehicle' AND trip_id='".$command['trip_id']."'order by created_time_mobile DESC";
        $command1 = Yii::app()->db->createCommand($sql)->queryRow();  
        if($command1){

        } 
        else{
        //$response['trip_id']=$command['trip_id']; 
        $response['trip_info']=$command; 
        }
       }

            $this->_sendResponse(200, CJSON::encode($response));
        return;          
          break;
       case 'initialapitrip': 
        $tripid=$_POST['trip_id'];
        $vehicle_reg_no=$_POST['vehicle_reg_no'];
        if(isset($_POST['imei_no']) && !empty($_POST['imei_no'])){
           $sql="SELECT * FROM tbl_vehicles WHERE  imei_no='".$_POST['imei_no']."' Limit 1";
           $command = Yii::app()->db->createCommand($sql)->queryRow();
           $vehicle_reg_no= $command['reg_no']; 
        }
        $response=array('bill_rate'=>'','trip_info'=>null,'maxmeter'=>null,'dutyType'=>'','vehicleType'=>null,'holiday'=>'','taDa'=>'','vehicleCheskStatus'=>'','holidayCheck'=>array(),'vehicle_info'=>null,'requisitionCount'=>0);


        $models = Billrates::model()->findAll();
        $response['bill_rate']=$models;
        $models =Dutytypes::model()->findAll();   
        $response['dutyType']=$models;
        $model =TaDaTime::model()->findByPk(5);  
        $response['taDa']=$model;



        $date=date("Y-m-d");
        $criteria = new CDbCriteria();
        $criteria->addCondition('start_date <= "'.$date.'" AND end_date >= "'.$date.'"');
        $models = Holidays::model()->findAll($criteria);
          if(!$models){
              //echo '0';
            $response['holiday']=0;
                    
            }
          else{
            $response['holiday']=1;
              //echo '1';
              
            }

          $criteria=new CDbCriteria;
          $criteria->addCondition('vehicle_reg_no = "'.$vehicle_reg_no.'" AND check_date = "'.$date.'"');
          $models=DailyVehicleCheck::model()->findAll($criteria);
          if(!empty($models[0]['vehicle_reg_no'])){                       
                    //echo 1;
             $response['vehicleCheskStatus']=1;
                        
            }
           else{
                 //echo 0;
             $response['vehicleCheskStatus']=0;                  
            }


        // trip-end
        if(isset($tripid) && !empty($tripid)){
        $sql="SELECT * FROM tbl_digital_log_book_history WHERE trip_id_end<>1 and trip_id='$tripid' and day_end='Yes' AND vehicle_reg_no='$vehicle_reg_no' order by created_time_mobile DESC";

        }
        else if(isset($_POST['imei_no']) && !empty($_POST['imei_no'])){
           $sql="SELECT * FROM tbl_vehicles WHERE  imei_no='".$_POST['imei_no']."' Limit 1";
        }
        else {
             $sql="SELECT * FROM tbl_vehicles WHERE  reg_no='$vehicle_reg_no' Limit 1";
        }
        $command = Yii::app()->db->createCommand($sql)->queryRow(); 
      
       if($command){  

          if(isset($tripid) && !empty($tripid)){      
            $response['trip_info']=$command; // response for previsous day information
            $vehicle_type_id=$command['vehicle_type_id'];
            $vehicle_no=$command['vehicle_reg_no']; 
           }
           else{
             $vehicle_type_id=$command['vehicletype_id'];
             $vehicle_no=$command['reg_no'];
             $response['trip_info']=null;
           }
          
          
          $model =Vehicletypes::model()->findByPk($vehicle_type_id);
          $response['vehicleType']=$model; 

           
          $sql = "SELECT end_meter as max_meter, vehicle_reg_no FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_no' ORDER BY id DESC";                
          $command1 = Yii::app()->db->createCommand($sql);       
          $result1 = $command1->queryRow();
                
          $sql = "SELECT max(meter_number) as max_meter,vehicle_reg_no FROM tbl_digital_log_book_history  WHERE vehicle_reg_no =  '$vehicle_no' ORDER BY created_time_mobile DESC";                
          $command1 = Yii::app()->db->createCommand($sql); 
          $result2 = $command1->queryRow();
          $response['maxmeter']=!empty($result1) ? $result1 : 0;
          if(!empty($result2) && $response['maxmeter'] < $result2) {
            $response['maxmeter']=$result2;
          }
        
       }

       //----Start holiday check-------
       if(isset($_POST['date_start']) && !empty($_POST['date_start']) && isset($_POST['date_end']) && !empty($_POST['date_start']) ){
                 $date_start=$_POST['date_start'];
                 $date_end=$_POST['date_end'];                 
                 $criteria = new CDbCriteria();
                 $criteria->addCondition('end_date >= "'.$date_start.'" AND NOT start_date > "'.$date_end.'"');
                 $criteria->order = 'start_date';                 
                 $models = Holidays::model()->findAll($criteria);
                 $date_array=array();

                for($i=$date_start; $i<=$date_end; $i){                   
                   $date_array[$i]=0;  
                   $date_step=date_create($i);
                   $F_S=date_format($date_step,"D"); 
                   if($F_S=="Fri" ||$F_S=="Sat"){$date_array[$i]=1;}
                   $i = date('Y-m-d', strtotime($i.' +1 day'));
                 }
               if(count($models)>0){
                   foreach ($models as $key => $value) {
                      for($i=$value['start_date']; $i<=$value['end_date']; $i){ 
                          if(isset($date_array[$i])){                  
                            $date_array[$i]=1; 
                          }                                      
                       $i = date('Y-m-d', strtotime($i.' +1 day'));
                      }
                   }
                }
                $i=0;
                if(count($date_array)>0){
                  foreach ($date_array as $key => $value) {
                    $response['holidayCheck'][$i]=array('date'=>$key,'holiday'=>$value);
                    $i++;
                  }
                }
               
                else{
                  $response['holidayCheck']=array();
                  }
                
               
          }
           //---End holiday check----
           //---start vehicle info----
          if(isset($_POST['imei_no'])&&!empty($_POST['imei_no'])){
            $imei=$_POST['imei_no'];            
            $response['vehicle_info'] = Vehicles::model()->findByAttributes(array('imei_no'=> $imei));
        }
         //---End vehicle info----
        //---start vehicle Requisition----
          if((isset($_POST['vehicle_reg_no'])&&!empty($_POST['vehicle_reg_no']))||(isset($_POST['imei_no'])&&!empty($_POST['imei_no']))){
            $vehicle='';
            if(isset($_POST['vehicle_reg_no']) && !empty($_POST['vehicle_reg_no'])){
              //$vehicle=Vehicles::model()->findByPk(array($vehicle_reg_no));
              $vehicle=$_POST['vehicle_reg_no'];
            }
            if(isset($_POST['imei_no']) && !empty($_POST['imei_no'])){
              $vehicle=Vehicles::model()->findByAttributes(array('imei_no'=> $imei));
              $vehicle=$vehicle['reg_no'];
            }
            if(!empty($vehicle)){    
                    $dt = date("Y-m-d");
                    $date=date('Y-m-d',strtotime("$dt +4 day"));
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("vehicle_reg_no=:vehicle_reg_no");
                    //$criteria->addCondition("start_date=:start_date");
                    //$criteria->params = array(':vehicle_reg_no' =>$vehicle,':start_date'=>$date);    

                    $criteria->addCondition("active=2 AND ((start_date>=:start_date AND start_date<=:start_date1) OR (end_date>=:start_date AND end_date<=:start_date1))");
                    $criteria->params = array(':vehicle_reg_no' =>$vehicle,':start_date'=>$dt, 'start_date1'=>$date);                      
                          
                    $model_v = Requisitions::model()->findAll($criteria);
                    $req = array();  
                    foreach ($model_v as $key => $value) {
                      $trip_end = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE requisition_id='".$value["id"]."' AND trip_id_end='1'")->queryRow();
                      if(!$trip_end)
                      $req[] = $value;
                                 
                    } 
                   
                    $response['requisitionCount']=count($req);
                }
          }

         //---End vehicle Requisition----

      $this->_sendResponse(200, CJSON::encode($response));
      return;          
      break;
      case 'mtomtracksingle':
        $vehicle=$_POST['vehicle'];  
        $vehicleid=$_POST['asset_id'];
        if(empty($vehicleid)){    
        $model=Vehicles::model()->findByPk($vehicle);
        $vehicleid=$model->asset_id;
        }
        $token=$this->mtoMAuthentication();
                       $postData = array(
                        "product" => "VTS API",
                        "version" => "1.0",
                        "apiName" => "getVehicleLocation",
                        "apiSecretKey" => "4d324d-415049-565453-4e4558444543414445",
                        "token"=>"$token", 
                        "vehicleID"=>"$vehicleid" 
                      );


                      $curl = curl_init();

                      curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.m2mbd.com/TrackSingle",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => json_encode($postData),
                          CURLOPT_HTTPHEADER => array(
                              "Accept: application/json",
                              "Cache-Control: no-cache"
                          ),
                      ));

                      $response = curl_exec($curl);
                      $err = curl_error($curl);

                      curl_close($curl);

                      if ($err) {
                          echo "cURL Error #:" . $err;
                      } else {
                         $response= json_decode($response);
                         $this->_sendResponse(200, CJSON::encode($response));
                       }
      return;     
      break;
      case 'mtomtrackhistory':
        $vehicle=$_POST['vehicle'];  
        $vehicleid=$_POST['asset_id'];
        $startDate=$_POST['startDate'];
        $endDate=$_POST['endDate'];
        if(empty($vehicleid)){    
        $model=Vehicles::model()->findByPk($vehicle);
        $vehicleid=$model->asset_id;
        }        
       
        $token=$this->mtoMAuthentication();
                       $postData = array(
                        "product" => "VTS API",
                        "version" => "1.0",
                        "apiName" => "getVehicleHistory",
                        "apiSecretKey" => "4d324d-415049-565453-4e4558444543414445",
                        "token"=>"$token", 
                        "vehicleID"=>"$vehicleid", 
                        "startDate"=>"$startDate", 
                        "endDate"=>"$endDate", 
                         "offset"=>0,  
                         "limit"=>10
                      );


                      $curl = curl_init();

                      curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.m2mbd.com/TrackHistory",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => json_encode($postData),
                          CURLOPT_HTTPHEADER => array(
                              "Accept: application/json",
                              "Cache-Control: no-cache"
                          ),
                      ));

                      $response = curl_exec($curl);
                      $err = curl_error($curl);

                      curl_close($curl);

                      if ($err) {
                          echo "cURL Error #:" . $err;
                      } else {
                         $response= json_decode($response);
                         $this->_sendResponse(200, CJSON::encode($response));
                       }
      return;     
      break;
      case 'accidentrecord':
        $_POST['created_by']=$_POST['driver_pin'];
        $_POST['created_date']=date("Y-m-d H:i:s");
        $model = new AccidentApp;  
        $result=$this->attributesCheck($_POST,$model); 
        if(isset($result['success']) && $result['success']==0){
          $this->_sendResponse(500, CJSON::encode($result));
          return;
        }
        if($result->save()){            
          $this->_sendResponse(200, CJSON::encode(array('id'=>0,'success'=>1,'message'=>'')));return;

        }
        else{
          $msg=$this->attributeError($result);
          $this->_sendResponse(500, CJSON::encode(array('id'=>0,'success'=>0,'message'=>$msg)));
          return;
         
        }
      return;
      case 'owncarimage':
      
   
      
      
         if(isset($_FILES['own_car_damage_image']) && $_FILES['own_car_damage_image']['size']>0){
                
                $target_file =Yii::getPathOfAlias('webroot')."/images/owncar/".basename($_FILES["own_car_damage_image"]["name"]);

                 $picture=move_uploaded_file($_FILES["own_car_damage_image"]["tmp_name"], $target_file);
               // $profile->upload_nid=$model->id.".".$ext;
                   if(!empty($picture)){                    
                    $this->_sendResponse(200, CJSON::encode(array('id'=>0,'success'=>1,'message'=>'')));
                     return;
                    }
                    else{
                       $this->_sendResponse(500, CJSON::encode(array('id'=>0,'success'=>0,'message'=>'Image cannot upload')));
                       return;
                    }
              }
           else{
                $this->_sendResponse(500, CJSON::encode(array('id'=>0,'success'=>0,'message'=>'Image object not found')));
                       return;
              }
              

      
     
      return;
     
      break;
      case 'othercarimage':
     
      
         if(isset($_FILES['other_car_damage_image']) && $_FILES['other_car_damage_image']['size']>0){
                
                $target_file=Yii::getPathOfAlias('webroot')."/images/othercar/".basename($_FILES["other_car_damage_image"]["name"]);

                 $picture=move_uploaded_file($_FILES["other_car_damage_image"]["tmp_name"], $target_file);
               // $profile->upload_nid=$model->id.".".$ext;
                   if(!empty($picture)){                    
                    $this->_sendResponse(200, CJSON::encode(array('id'=>0,'success'=>1,'message'=>'')));
                     return;
                    }
                    else{
                       $this->_sendResponse(500, CJSON::encode(array('id'=>0,'success'=>0,'message'=>'Image cannot upload')));
                       return;
                    }
              }
           else{
                $this->_sendResponse(500, CJSON::encode(array('id'=>0,'success'=>0,'message'=>'Image object not found')));
                       return;
              }
              

      
      return;
     
      break;
        default:
            $this->_sendResponse(501, 
                sprintf('Mode <b>create</b> is not implemented for model <b>%s</b>',
                $_GET['model']) );
                Yii::app()->end();
        }
    

    // Try to assign POST values to attributes
    foreach($_POST as $var=>$value) {
        // Does the model have this attribute? If not raise an error
        if($model->hasAttribute($var))
            $model->$var = $value;
        else
            $this->_sendResponse(500, 
                sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var,
                $_GET['model']) );
        }
    // Try to save the model
        if($model->save())
        $this->_sendResponse(200, CJSON::encode($model));
         else {
        // Errors occurred
        $msg = "<h1>Error</h1>";
        $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
        $msg .= "<ul>";
        foreach($model->errors as $attribute=>$attr_errors) {
            $msg .= "<li>Attribute: $attribute</li>";
            $msg .= "<ul>";
            foreach($attr_errors as $attr_error)
                $msg .= "<li>$attr_error</li>";
            $msg .= "</ul>";
        }
        $msg .= "</ul>";
        $this->_sendResponse(500, $msg );
        }
     }
     else{
        switch($_GET['model'])
        {
            case 'drivers':
             if($_POST['_token']=='81dc9bdb52d04dc20036dbd8313ed055'){
                if(!empty($_POST['userName']) && !empty($_POST['userPass']))
                {
                    $userPin=$_POST['userName'];
                    $userPass=md5($_POST['userPass']);
                    $models = Drivers::model()->findByAttributes(array('pin'=>$userPin,'password'=>$userPass));
                    if(empty($models)) 
                    {
                
                        $this->_sendResponse(200, 
                        sprintf('<b>User And Password Incorrect!</b>', $_GET['model']) );
                    }
                     else{
                        if($models->active=='No'){
                            $this->_sendResponse(200,                 
                        sprintf('<b>User Not Active</b>', $_GET['model']));
                        }
                        else{
                            $this->_sendResponse(200, CJSON::encode($models));
                        }
                     }
                }
                else{
                    $this->_sendResponse(200,                 
                        sprintf('<b>User Name Or Password Can Not Empty</b>', $_GET['model']));
                }
            }
            else{
                    $this->_sendResponse(200,                 
                        sprintf('<b>Token Miss Match</b>', $_GET['model']));
                }
                              
            break;            
            default:
            $this->_sendResponse(501, 
                sprintf('URL Not Found',
                $_GET['model']) );
                Yii::app()->end();
        }

     }
    }
    public function actionUpdate()
    {
        // Parse the PUT parameters. This didn't work: parse_str(file_get_contents('php://input'), $put_vars);
     $json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
     $put_vars = CJSON::decode($json,true);  //true means use associative array

        switch($_GET['model'])
        {
        // Find respective model
          case 'posts':
            $model = Post::model()->findByPk($_GET['id']);                    
            break;
            case 'feedbackRating':
            $model = new FeedbackRating;                    
            break;
            case 'requisitions':
            $model = Requisitions::model()->findByPk($_GET['id']);                    
            break;
        default:
            $this->_sendResponse(501, 
                sprintf( 'Error: Mode <b>update</b> is not implemented for model <b>%s</b>',
                $_GET['model']) );
            Yii::app()->end();
     }
    // Did we find the requested model? If not, raise an error
        if($model === null)
        $this->_sendResponse(400, 
                sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",
                $_GET['model'], $_GET['id']) );
        
    // Try to assign PUT parameters to attributes
        foreach($put_vars as $var=>$value) {
        // Does model have this attribute? If not, raise an error
        if($model->hasAttribute($var))
            $model->$var = $value;
        else {
            $this->_sendResponse(500, 
                sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>',
                $var, $_GET['model']) );
        }
    }
    // Try to save the model
        if($model->save())
            $this->_sendResponse(200, CJSON::encode($model));
        else
            // prepare the error $msg
            // see actionCreate
            // ...
            $this->_sendResponse(500, $msg );
    }
    protected function beforeSave()
    {
    
    // author_id may have been posted via API POST
    if(is_null($this->author_id) or $this->author_id=='')
        $this->author_id=Yii::app()->user->id;
    
    }
    public function actionDelete()
    {
        switch($_GET['model'])
        {
        // Load the respective model
        case 'posts':
            $model = Post::model()->findByPk($_GET['id']);                    
            break;
        case 'requisitions':
            $model = Requisitions::model()->findByPk($_GET['id']);                    
            break;
        default:
            $this->_sendResponse(501, 
                sprintf('Error: Mode <b>delete</b> is not implemented for model <b>%s</b>',
                $_GET['model']) );
            Yii::app()->end();
        }
    // Was a model found? If not, raise an error
        if($model === null)
        $this->_sendResponse(400, 
                sprintf("Error: Didn't find any model <b>%s</b> with ID <b>%s</b>.",
                $_GET['model'], $_GET['id']) );

    // Delete the model
        $num = $model->delete();
        if($num>0)
        $this->_sendResponse(200, $num);    //this is the only way to work with backbone
        else
        $this->_sendResponse(500, 
                sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.",
                $_GET['model'], $_GET['id']) );
    }
    public function actionDriverPin()
    {   
        if(!isset($_GET['driverpin']))
            $this->_sendResponse(500, 'Error: Parameter <b>driverpin</b> is missing' );
        switch($_GET['model'])
            {
                case 'posts':
                    $models = Post::model()->findAll();
                    break;
                case 'requisitions':
                    $driverPin=$_GET['driverpin'];
                    $date=date('Y-m-d');
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("driver_pin=:driver_pin");
                    $criteria->addCondition("start_date<=:start_date");
                    $criteria->addCondition("end_date>=:start_date");
                    $criteria->params = array(':driver_pin' => $driverPin,':start_date'=>$date);               
                    $models = Requisitions::model()->findAll($criteria);
                    break;
                case 'vehicles':
                $driverPin=$_GET['driverpin'];
                $criteria = new CDbCriteria();
                $criteria->addCondition("driver_pin=:driver_pin");
               // $criteria->addCondition('max(active)');
                $criteria->order = 'active DESC';
               // $criteria->limit = '1';
                $criteria->params = array(':driver_pin' => $driverPin);
                
                $models = Vehicles::model()->findAll($criteria);
                break;
                case 'drivers':
                $driverPin=$_GET['driverpin'];
                $criteria = new CDbCriteria();
                $criteria->addCondition("pin=:driver_pin");
               // $criteria->addCondition('max(active)');
                //$criteria->order = 'active DESC';
               // $criteria->limit = '1';
                $criteria->params = array(':driver_pin' => $driverPin);
                
                $models = Drivers::model()->findAll($criteria);
                break;
                case 'driverrating':
                $driverPin=$_GET['driverpin'];
                $query="SELECT pin, driver_rating FROM tbl_drivers where pin='$driverPin'";
                $models=Yii::app()->db->createCommand($query)->queryAll();  
                if(!empty($models[0]['pin'])){
                        $this->_sendResponse(200, CJSON::encode($models));
                        return;
                        }
                unset($models);          
                break;
                default:
                    // Model not implemented error
                    $this->_sendResponse(501, sprintf(
                        'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
                        $_GET['model']) );
                    Yii::app()->end();
            }
            // Did we get some results?
            if(empty($models)) {
                // No
                $this->_sendResponse(200, 
                        sprintf('<b>No Data Found </b>', $_GET['model']) );
            } else {
                // Prepare response
                $rows = array();
                foreach($models as $model)
                    $rows[] = $model->attributes;
                // Send the response
                $this->_sendResponse(200, CJSON::encode($rows));
            }
    }
    public function actionUserPin()
    {  
        if(!isset($_GET['userpin']))
            $this->_sendResponse(500, 'Error: Parameter <b>userpin</b> is missing' );
        switch($_GET['model'])
            {
                case 'posts':
                    $models = Post::model()->findAll();
                    break;
                case 'requisitions':
                    $userPin=$_GET['userpin'];
                    $status=$_GET['status'];                 
                    $rows=array('requisitions'=>array());
                    $rows_end=array('requisitions'=>array());
                    $rows_not_end=array('requisitions'=>array());
                    $re['trip_rating']=array('success'=>'false');
                    $dt = date("Y-m-d");
                    $date=date('Y-m-d',strtotime("$dt +7 day"));
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("active=2 AND user_pin=:user_pin");
                    $criteria->addCondition("(start_date>=:start_date AND start_date<=:start_date1) OR (end_date>=:start_date AND end_date<=:start_date1)");
                    $criteria->params = array(':user_pin' => $userPin,':start_date'=>$dt, 'start_date1'=>$date);
                    $criteria->order = 'start_date DESC, start_time DESC'; 

                    $models = Requisitions::model()->findAll($criteria);
                    
                    
                    $i=0;$j=0;$k=0;
                    if(count($models)>0){
                      foreach ($models as $key => $value) {  
                                      

                        $requsition=$value['id'];
                        $requisition_model=New Requisitions;
                        $requisition_model->unsetAttributes();
                        $sql="SELECT * FROM tbl_digital_log_book_history WHERE user_pin='$userPin' AND trip_id_end=1 AND requisition_id='$requsition'";
                        $history=Yii::app()->db->createCommand($sql)->queryRow();                 

                          if($history){

                            $sql_rating="SELECT  * FROM tbl_feedback_rating where id='".$requsition."'";
                            $rating_rs=Yii::app()->db->createCommand($sql_rating)->queryRow();
                           
                            if($rating_rs){$m=0;}
                            else{
                              $trip_id=$history['trip_id'];
                              $sql="SELECT * FROM tbl_digital_log_book_history WHERE user_pin='$userPin' AND requisition_id='$requsition' AND trip_id='$trip_id' ORDER BY create_time_server ";
                              $strart_reqisition=Yii::app()->db->createCommand($sql)->queryRow(); 
                              $re['trip_rating']['rating_status']=0;
                              $re['trip_rating']['requisition_id']=$requsition;
                              $re['trip_rating']['end_date_time']=$history['create_time_server'];
                              $re['trip_rating']['start_date_time']=$strart_reqisition['create_time_server'];
                              $re['trip_rating']['success']='true';
                              
                            }
                            
                            $requisition_model->attributes=$value['attributes'];
                            $requisition_model->billing_code="";
                            $rows_end['requisitions'][$i]=$requisition_model->attributes;
                            $i++;
                            $rows['requisitions'][$k]=$requisition_model->attributes; 
                            $k++; 

                            
                          }
                          else{
                           
                             $requisition_model->attributes=$value['attributes'];
                              $sql="SELECT * FROM tbl_digital_log_book_history WHERE user_pin='$userPin' AND  requisition_id='$requsition' AND date(create_time_server)='$dt' order by create_time_server";
                              $check_running=Yii::app()->db->createCommand($sql)->queryRow(); 
                              if($check_running){
                                $requisition_model->running=1;
                                $requisition_model->billing_code= $check_running['meter_number']; //start meter
                              } 

                             $rows_not_end['requisitions'][$j]=$requisition_model->attributes;
                             $j++;
                             $rows['requisitions'][$k]=$requisition_model->attributes; 
                             $k++;  
                          }
                         // $requisition_model->attributes=$value['attributes'];
                          

                      }
                   
                      if($status==1){   
                                 
                        $rows_end['trip_rating']=$re['trip_rating'];
                        $this->_sendResponse(200, CJSON::encode($rows_end));
                      }
                      if($status==2){       
                       
                        $rows_not_end['trip_rating']=$re['trip_rating'];                   
                                            
                        $this->_sendResponse(200, CJSON::encode($rows_not_end));
                      }
                       if($status==3){
                          
                          $rows['trip_rating']=$re['trip_rating'];

                        $this->_sendResponse(200, CJSON::encode($rows));
                      }
                    }
                    else{
                      $rows['trip_rating']=$re['trip_rating'];                     
                    
                      $this->_sendResponse(200, CJSON::encode($rows));
                    }
                    return;
                    break;                
                case 'userratings':
                    $userPin=$_GET['userpin'];
                    //$models = UserRating::model()->userRating1($userPin);
                    $query="SELECT user_pin,AVG(u_rating) as u_rating FROM tbl_user_rating where user_pin='$userPin'";
                    $models=Yii::app()->db->createCommand($query)->queryAll();
                    if(!empty($models[0]['user_pin'])){
                        $this->_sendResponse(200, CJSON::encode($models));
                        return;
                        }
                    unset($models);
                    break;
                case 'usertripinfo':
                    $userPin=$_GET['userpin'];
                    $call_value=$_GET['status'];
                    $page=($_GET['page']-1)*10;
                    

                    //$query="SELECT  * FROM tbl_digital_log_book_history where user_pin='".$userPin."' AND  trip_id <>'' order by  created_time_mobile DESC Limit 1";
                    if($call_value==1){
                      $query="SELECT  * FROM tbl_digital_log_book_history where user_pin='".$userPin."' AND  trip_id <>''   order by  created_time_mobile DESC Limit 1";
                      $model=Yii::app()->db->createCommand($query)->queryRow();
                      if($model){
                         $tripid=$model['trip_id'];
                         //$query="SELECT  * FROM tbl_digital_log_book_history where user_pin='".$userPin."' AND  trip_id ='$tripid' order by  created_time_mobile DESC"; 
                         $query="SELECT  * FROM tbl_digital_log_book_history where user_pin='".$userPin."' AND  trip_id ='$tripid' order by  created_time_mobile DESC"; 
                         
                         $model1=Yii::app()->db->createCommand($query)->queryAll();

                          $i=0;$j=0;$k=0;
                          $model['fuel']=array();
                         $model['otherBill']=array();
                         $model['ta']=array('morning'=>array('id'=>'0','amount'=>'0'),
                          'lunch'=>array('id'=>'0','amount'=>'0'),
                          'night'=>array('id'=>'0','amount'=>'0'),
                          'nighthalt'=>array('id'=>'0','amount'=>'0')
                          );
                         $model['initialmeter']=$model1[0]['meter_number'];
                         $model['overtime']='';
                         $end_date_time=$model1[0]['created_time_mobile'];
                         $start_date_time=$model1[count($model1)-1]['driver_start_time_mobile'];                                    
                         
                         $model['overtime']=$model1[0]['over_time'];

                         $model['start_point']=!empty($model1[count($model1)-1]['location_vts'])?$model1[count($model1)-1]['location_vts']:$model1[count($model1)-1]['location_mobile']; 
                         $model['end_point']=!empty($model1[0]['location_vts'])?$model1[0]['location_vts']:$model1[0]['location_mobile'];

                         $start_date_time_end=$model1[0]['driver_start_time_mobile'];
                        
                           foreach ($model1 as $key => $value) {
                            if($start_date_time_end==$value["driver_start_time_mobile"] && $model['initialmeter']>$value['meter_number']){
                              $model['initialmeter']=$value['meter_number'];
                            }
                            
                            if($value['fuel_quantity']!=0){
                            $model['fuel'][$i]['fuel_recipt_no']=$value['fuel_recipt_no'];
                            $model['fuel'][$i]['fuel_quantity']=$value['fuel_quantity'];
                            $model['fuel'][$i]['fuel_unit_price']=$value['fuel_unit_price'];
                            $model['fuel'][$i]['fuel_total_bill']=$value['fuel_total_bill'];
                            $i++;
                          }
                          if($value['others_bill']!=0){
                            $model['otherBill'][$j]['action_id']=$value['id'];
                            $model['otherBill'][$j]['others_cost']=$value['others_cost'];
                            $model['otherBill'][$j]['others_bill']=$value['others_bill'];
                            $model['otherBill'][$j]['others_description']=$value['others_description'];
                            $j++;
                          }
                          

      if($value['morning_ta']!=0){
        $model['ta']['morning']['id']=$value['id'];
        $model['ta']['morning']['amount']=$value['morning_ta'];        
      }
      if($value['lunch_ta']!=0){
        $model['ta']['lunch']['id']=$value['id'];
        $model['ta']['lunch']['amount']=$value['lunch_ta'];
      }
      if($value['night_ta']!=0){
        $model['ta']['night']['id']=$value['id'];
        $model['ta']['night']['amount']=$value['night_ta'];
      }
      if($value['night_halt_bill']!=0){
        $model['ta']['nighthalt']['id']=$value['id'];
        $model['ta']['nighthalt']['amount']=$value['night_halt_bill'];
      }
      if(!empty($value['over_time']))$model['overtime']=$value['over_time'];
                        }
                       if(!empty($model['requisition_id'])){
                          $requisition=$model['requisition_id'];
                          $query="SELECT  * FROM tbl_feedback_rating where id='".$requisition."'";
                          $requisition_rs=Yii::app()->db->createCommand($query)->queryRow();
                          
                          if($requisition_rs){
                            $re['trip_rating']['rating_status']=1;
                            $re['trip_rating']['requisition_id']=$requisition_rs['id'];

                          }
                          else{
                            $re['trip_rating']['rating_status']=0;
                            $re['trip_rating']['requisition_id']=$requisition;
                            $re['trip_rating']['end_date_time']=$end_date_time;
                            $re['trip_rating']['start_date_time']=$start_date_time;
                          }
                       }
                       else{
                        $re['trip_rating']=array(''=>null);
                       }
                       $model['trip_rating']=$re['trip_rating'];
                       //$model=array($model);
                        $this->_sendResponse(200, CJSON::encode($model));
                        
                      }
                      else {$model=null; 
                        $this->_sendResponse(200, CJSON::encode($model));}
                      return;
                  }
                  if($call_value==2){
                    $query="SELECT  * FROM tbl_digital_log_book_history where user_pin='".$userPin."' AND  trip_id <>'' AND (trip_id_end=1 OR day_end='Yes')  order by  created_time_mobile DESC LIMIT ".$page.", 10 ";
                    $models=Yii::app()->db->createCommand($query)->queryAll();
                    if(count($models)>0){
                      $count_summery=0;

                      foreach ($models as $key1 => $value1) {
                        $model[$count_summery]=$value1;
                         $tripid=$value1['trip_id'];
                         $value1_date=explode(" ",$value1['driver_start_time_mobile']);
                         $value1_date=$value1_date[0];
                         $query="SELECT  * FROM tbl_digital_log_book_history where user_pin='".$userPin."' AND  trip_id ='$tripid' AND DATE(driver_start_time_mobile)='$value1_date' order by  created_time_mobile DESC";                         
                         $model1=Yii::app()->db->createCommand($query)->queryAll();
                        
                         $i=0;$j=0;
                         $model[$count_summery]['fuel']=array();
                         $model[$count_summery]['otherBill']=array();
                         $model[$count_summery]['ta']=array('morning'=>'0','lunch'=>'0','night'=>'0','nighthalt'=>'0');
                         $model[$count_summery]['overtime']=$model1[0]['over_time'];

                         $model[$count_summery]['start_point']=!empty($model1[count($model1)-1]['location_vts'])?$model1[count($model1)-1]['location_vts']:$model1[count($model1)-1]['location_mobile']; 

                         $model[$count_summery]['end_point']=!empty($model1[0]['location_vts'])?$model1[0]['location_vts']:$model1[0]['location_mobile'];

                         if($model1[0]['driver_end_time_mobile']){
                          $end_date_time=$model1[0]['driver_end_time_mobile'];
                         }
                         else{
                          $end_date_time=$model1[0]['create_time_server'];
                         }
                         $start_date_time=$model1[count($model1)-1]['driver_start_time_mobile'];                         
                        $model[$count_summery]['initialmeter']=$model1[0]['meter_number'];
                        $start_date_time_end=$model1[0]['driver_start_time_mobile'];
                         foreach ($model1 as $key => $value) {
                          if($start_date_time_end==$value["driver_start_time_mobile"] && $model[$count_summery]['initialmeter']>$value['meter_number']){
                              $model[$count_summery]['initialmeter']=$value['meter_number'];
                            }
                           if($value['fuel_quantity']!=0){
                            $model[$count_summery]['fuel'][$i]['fuel_recipt_no']=$value['fuel_recipt_no'];
                            $model[$count_summery]['fuel'][$i]['fuel_quantity']=$value['fuel_quantity'];
                            $model[$count_summery]['fuel'][$i]['fuel_unit_price']=$value['fuel_unit_price'];
                            $model[$count_summery]['fuel'][$i]['fuel_total_bill']=$value['fuel_total_bill'];
                            $i++;
                          }
                          if($value['others_bill']!=0){
                            $model[$count_summery]['otherBill'][$j]['action_id']=$value['id'];
                            $model[$count_summery]['otherBill'][$j]['others_cost']=$value['others_cost'];
                            $model[$count_summery]['otherBill'][$j]['others_bill']=$value['others_bill'];
                            $model[$count_summery]['otherBill'][$j]['others_description']=$value['others_description'];
                            $j++;
                          }
                          if($value['morning_ta']!=0)$model[$count_summery]['ta']['morning']=$value['morning_ta'];
                          if($value['lunch_ta']!=0)$model[$count_summery]['ta']['lunch']=$value['lunch_ta'];
                          if($value['night_ta']!=0)$model[$count_summery]['ta']['night']=$value['night_ta'];
                          if($value['night_halt_bill']!=0)$model[$count_summery]['ta']['nighthalt']=$value['night_halt_bill'];
                         //if(!empty($value['over_time']))$model[$count_summery]['overtime']=$value['over_time'];
                        }
                         if(!empty($model[$count_summery]['requisition_id'])){
                          $requisition=$model[$count_summery]['requisition_id'];
                          $query="SELECT  * FROM tbl_feedback_rating where id='".$requisition."'";
                          $requisition_rs=Yii::app()->db->createCommand($query)->queryRow();
                          
                          if($requisition_rs){
                            $re['trip_rating']['rating_status']=1;
                            $re['trip_rating']['requisition_id']=$requisition_rs['id'];

                          }
                          else{
                            $re['trip_rating']['rating_status']=0;
                            $re['trip_rating']['requisition_id']=$requisition;
                            $re['trip_rating']['end_date_time']=$end_date_time;
                            $re['trip_rating']['start_date_time']=$start_date_time;
                          }
                       }
                       else{
                        $re['trip_rating']=array(''=>null);
                       }
                       $model[$count_summery]['trip_rating']=$re['trip_rating'];

                      $count_summery++;
                      }
                       $this->_sendResponse(200, CJSON::encode($model));
                       return;
                    }
                    else {
                      $model=array();
                      $this->_sendResponse(200, CJSON::encode($model));
                      return;
                    }                   
                  }
                  break;
                  case 'vehicle':
                    $userPin=$_GET['userpin'];
                    //$models = UserRating::model()->userRating1($userPin);
                    $query="SELECT v.*,d.phone as driver_phone FROM tbl_vehicles as v INNER JOIN tbl_drivers as d  ON v.driver_pin=d.pin where user_pin='$userPin'";
                    $models=Yii::app()->db->createCommand($query)->queryRow();
                    if($models){
                        $this->_sendResponse(200, CJSON::encode($models));
                        return;
                        }
                   else echo 0;
                    return;
                    break;
                default:
                    // Model not implemented error
                    $this->_sendResponse(501, sprintf(
                        'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
                        $_GET['model']) );
                    Yii::app()->end();
            }
            // Did we get some results?
            if(empty($models)) {
                // No
                $this->_sendResponse(200, 
                        sprintf('<b>No Data Found </b>', $_GET['model']) );
            } else {
                // Prepare response
                $rows = array();
                foreach($models as $model)
                    $rows[] = $model->attributes;
                // Send the response
                $this->_sendResponse(200, CJSON::encode($rows));
            }
    }
    public function actionAddress()
    {   
        if(!isset($_POST['address']))
            $this->_sendResponse(500, 'Error: Parameter <b>address</b> is missing' );
        switch($_GET['model'])
            {
                case 'posts':
                    $models = Post::model()->findAll();
                    break;
                case 'fuelstations':
                    $address=$_POST['address'];
                    
                    //$date=date('Y-m-d');
                    
                    if(!empty($address)){
                    $criteria = new CDbCriteria();
                    $criteria->compare('address',$address,true);
                    //$criteria->addCondition("address=:address");
                    //$criteria->addCondition("start_date=:start_date");
                   // $criteria->params = array(':address' => $address);               
                    $models = Fuelstations::model()->findAll($criteria);
                    }
                    else {
                    $models = Fuelstations::model()->findAll();
                    }
                    break;
                case 'vehicles':
                $driverPin=$_GET['driverpin'];
                $criteria = new CDbCriteria();
                $criteria->addCondition("driver_pin=:driver_pin");
               // $criteria->addCondition('max(active)');
                $criteria->order = 'active DESC';
               // $criteria->limit = '1';
                $criteria->params = array(':driver_pin' => $driverPin);
                
                $models = Vehicles::model()->findAll($criteria);
                break;
                case 'drivers':
                $driverPin=$_GET['driverpin'];
                $criteria = new CDbCriteria();
                $criteria->addCondition("pin=:driver_pin");
               // $criteria->addCondition('max(active)');
                //$criteria->order = 'active DESC';
               // $criteria->limit = '1';
                $criteria->params = array(':driver_pin' => $driverPin);
                
                $models = Drivers::model()->findAll($criteria);
                break;
                
                default:
                    // Model not implemented error
                    $this->_sendResponse(501, sprintf(
                        'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
                        $_GET['model']) );
                    Yii::app()->end();
            }
            // Did we get some results?
            if(empty($models)) {
                // No
                $this->_sendResponse(200, 
                        sprintf('<b>No Data Found </b>', $_GET['model']) );
            } else {
                // Prepare response
                $rows = array();
                foreach($models as $model)
                    $rows[] = $model->attributes;
                // Send the response
                $this->_sendResponse(200, CJSON::encode($rows));
            }
    }
    public function actionVehicle()
    {   
        if(!isset($_POST['vehicle']))
            $this->_sendResponse(500, 'Error: Parameter <b>address</b> is missing' );
        switch($_GET['model'])
            {
                case 'posts':
                    $models = Post::model()->findAll();
                    break;
                case 'vehiclerating': 
                $vehicle_no=$_POST['vehicle'];              
                $query="SELECT r.vehicle_reg_no, AVG(f.car_rating) as rating FROM tbl_requisitions as r join tbl_feedback_rating as f on r.id=f.id WHERE r.vehicle_reg_no='$vehicle_no'";
                $models=Yii::app()->db->createCommand($query)->queryAll();  
                if(!empty($models[0]['vehicle_reg_no'])){
                        $this->_sendResponse(200, CJSON::encode($models));
                        return;
                        }
                unset($models);          
                break;   
                case 'vehicleDateCheskStatus': 
                $vehicle_no=$_POST['vehicle'];              
                $date=$_POST['check_date'];              
                $criteria=new CDbCriteria;
                $criteria->addCondition('vehicle_reg_no = "'.$vehicle_no.'" AND check_date = "'.$date.'"');
                $models=DailyVehicleCheck::model()->findAll($criteria);
                if(!empty($models[0]['vehicle_reg_no'])){
                        //$this->_sendResponse(200, CJSON::encode($models));
                    echo 1;
                        return;
                        }
                else{
                    echo 0;

                    return;
                }
                unset($models);            
                break;   
                case 'maxmeter': 
                $vehicle_no=$_POST['vehicle'];  
                $sql = "SELECT end_meter as max_meter, vehicle_reg_no FROM tbl_movements  WHERE vehicle_reg_no =  '$vehicle_no' ORDER BY id DESC";                
                $command = Yii::app()->db->createCommand($sql);       
                $result1 = $command->queryRow();
                
                 $sql = "SELECT max(meter_number) as max_meter,vehicle_reg_no FROM tbl_digital_log_book_history  WHERE vehicle_reg_no =  '$vehicle_no' group by vehicle_reg_no";
                $command = Yii::app()->db->createCommand($sql); 
                $result2 = $command->queryRow();
                $response['maxmeter']=!empty($result1) ? $result1 : 0;
                if(!empty($result2) && $response['maxmeter'] < $result2) {
                  $response['maxmeter']=$result2;
                }
                  $this->_sendResponse(200, CJSON::encode($response));    
                
                return;
                break;
                case 'requisitions':
                    $vehicle=$_POST['vehicle'];
                    $dt = date("Y-m-d");
                    $date=date('Y-m-d',strtotime("$dt +4 day"));
                    $criteria = new CDbCriteria();
                    $criteria->addCondition("vehicle_reg_no=:vehicle_reg_no");
                    //$criteria->addCondition("start_date=:start_date");
                    //$criteria->params = array(':vehicle_reg_no' =>$vehicle,':start_date'=>$date);    

                    $criteria->addCondition("active=2 AND ((start_date>=:start_date AND start_date<=:start_date1) OR (end_date>=:start_date AND end_date<=:start_date1))");
                    $criteria->params = array(':vehicle_reg_no' =>$vehicle,':start_date'=>$dt, 'start_date1'=>$date);  
                    $criteria->order = 'start_date ASC, start_time ASC';         
                    $models = Requisitions::model()->findAll($criteria); 
                    $req = array();  
                    foreach ($models as $key => $value) {
                      $trip_end = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE requisition_id='".$value["id"]."' AND trip_id_end='1'")->queryRow();
                      if(!$trip_end)
                      $req[] = $value;
                                 
                    }            
                    $this->_sendResponse(200, CJSON::encode($req));
                    return;                   
                break;
                 case 'vehicletripinfo':
                    $vehicle_reg=$_POST['vehicle'];
                    $query="SELECT  * FROM tbl_digital_log_book_history where vehicle_reg_no='".$vehicle_reg."' AND  trip_id <>'' order by  created_time_mobile DESC Limit 1";
                    $model=Yii::app()->db->createCommand($query)->queryRow();
                    if($model){
                      $this->_sendResponse(200, CJSON::encode($model));
                      
                    }
                    else echo 0;
                    return;
                  break;
                default:
                    // Model not implemented error
                    $this->_sendResponse(501, sprintf(
                        'Error: Mode <b>list</b> is not implemented for model <b>%s</b>',
                        $_GET['model']) );
                    Yii::app()->end();
            }
            // Did we get some results?
            if(empty($models)) {
                // No
                $this->_sendResponse(200, 
                        sprintf('<b>No Data Found </b>', $_GET['model']) );
            } else {
                // Prepare response
                $rows = array();
                foreach($models as $model)
                    $rows[] = $model->attributes;
                // Send the response
                $this->_sendResponse(200, CJSON::encode($rows));
            }
    }
    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
            // set the status
            $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
            header($status_header);
            // and the content type
            header('Content-type: ' . $content_type);

            // pages with body are easy
            if($body != '')
            {
                // send the body
                echo $body;
            }
            // we need to create the body if none is passed
            else
            {
                // create some body messages
                $message = '';

                // this is purely optional, but makes the pages a little nicer to read
                // for your users.  Since you won't likely send a lot of different status codes,
                // this also shouldn't be too ponderous to maintain
                switch($status)
                {
                    case 401:
                        $message = 'You must be authorized to view this page.';
                        break;
                    case 404:
                        $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                        break;
                    case 500:
                        $message = 'The server encountered an error processing your request.';
                        break;
                    case 501:
                        $message = 'The requested method is not implemented.';
                        break;
                }

                // servers don't always have a signature turned on 
                // (this is an apache directive "ServerSignature On")
                $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

                // this should be templated in a real-world solution
                $body = '
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
        </head>
        <body>
            <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
            <p>' . $message . '</p>
            <hr />
            <address>' . $signature . '</address>
        </body>
        </html>';

                echo $body;
            }
            Yii::app()->end();
    }
    private function _getStatusCodeMessage($status)
    {
    // these could be stored in a .ini file and loaded
    // via parse_ini_file()... however, this will suffice
    // for an example
    $codes = Array(
        200 => 'OK',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
    );
    return (isset($codes[$status])) ? $codes[$status] : '';
    }

    public function sendMailIcress($email,$body,$sub)
    {
        try {
            $soapClient = new SoapClient("http://imail.brac.net:8080/isoap.comm.imail/EmailWS?wsdl");

            $job = new jobs;

            //$job->subject='Transport Requisition Notification';
            $job->jobContentType = 'html';
            $job->fromAddress = 'ifleet@brac.net';
            $job->udValue1 = 'iFleet';
            $job->requester = 'iFleet';

            //   $job->jobRecipients[0]=new jobRecipients;
            //   $job->jobRecipients[0]->recipientEmail="shouman.das@gmail.com";

            $job->jobRecipients[0] = new jobRecipients;
            //$job->jobRecipients[0]->recipientEmail = $model->email;
            $job->jobRecipients[0]->recipientEmail = $email;


            //$model1 = $hrdata->getHrUser($model->pin);
            //$rec_mail = $model1[0]['Email'];

           
                $job->subject = $sub;
                $job->body = nl2br($body);
            $jobs = array('jobs' => $job);
            $send_email = $soapClient->__call('sendEmail', array($jobs));
            echo $send_email->return->status;
            
        } catch (SoapFault $fault) {
           $error = 1;
            print($fault->faultcode . "-" . $fault->faultstring);
        }
    }
    public function sendSMS($user_cell, $message){

        $client = new EHttpClient(
            'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
            )
        );
        $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $message,
            'to_number' => $user_cell,
            'app_url' => 'http://ifleet.brac.net/',
        ));

        

        return $response = $client->request('POST');
        
    }

    private function mtoMAuthentication(){
      $postData = array(
                        "product" => "VTS API",
                        "version" => "1.0",
                        "apiName" => "authenticateUser",
                        "apiSecretKey" => "4d324d-415049-565453-4e4558444543414445",
                        "username" => "brac_ngo@api",
                        "password" => "brac.ngo@1906#"
                    );


                    $curl = curl_init();

                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://api.m2mbd.com/Authenticate",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => json_encode($postData),
                        CURLOPT_HTTPHEADER => array(
                            "Accept: application/json",
                            "Cache-Control: no-cache"
                        ),
                    ));

                    $response = curl_exec($curl);
                    $err = curl_error($curl);

                    curl_close($curl);

                    if ($err) {
                        echo "cURL Error #:" . $err;
                    } 
                    else {
                       $response= json_decode($response);
                       return $token=$response->response->token;
                    }

                }
    private function dailyVehicleCheck($receive,$type){
            if($type==1){
             $model = new DailyVehicleCheck; 
            }
            if($type==2){
              $model = new ErrorLog;
            }

  
            // Try to assign POST values to attributes
            $id=0;        
            foreach($receive as $var=>$value) {
                // Does the model have this attribute? If not raise an error
                if($model->hasAttribute($var)){
                   if($var=="id"){
                      $id=$value;
                    } 
                    else{
                      $model->$var = $value;
                      }
                  }
                  else{
                    return array('id'=>$id,
                                 'success'=>0,  
                                 'message'=>$var.' attribute not found',
                        );
                  }                
                }

            // Try to save the model
                if($model->save()){             
                                                        
                    return  array('id'=>$id,
                                 'success'=>1,  
                                 'message'=>'',
                        );               
               
                }
                else {              
                
                foreach($model->errors as $attribute=>$attr_errors) {
                    $msg .= "Attribute: $attribute";
                    $msg .= ",";
                    foreach($attr_errors as $attr_error)
                        $msg .= "$attr_error";
                    $msg .= ",";
                }
                $msg .= ",";                              
                return array('id'=>$id,
                              'success'=>0,  
                              'message'=>$msg,
                        );
                
              
                
              }
    }
    private function attributesCheck($receive,$model){
            $id=0;        
            foreach($receive as $var=>$value) {
                // Does the model have this attribute? If not raise an error
                if($model->hasAttribute($var)){
                   if($var=="id"){
                      $id=$value;
                    } 
                    else{
                      $model->$var = $value;
                      }
                  }
                  else{
                    return array('id'=>$id,
                                 'success'=>0,  
                                 'message'=>$var.' attribute not found',
                        );
                  }                
                }
                return $model;

    }
  private function attributeError($model){
     foreach($model->errors as $attribute=>$attr_errors) {
                    $msg .= "Attribute: $attribute";
                    $msg .= ",";
                    foreach($attr_errors as $attr_error)
                        $msg .= "$attr_error";
                    $msg .= ",";
                }
                return $msg;  

  }
  private function maxMeter($model){
    $lastLBRow = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE trip_id='".$model->trip_id."' AND driver_start_time_mobile='".$model->driver_start_time_mobile."' order by id ASC")->queryRow();
    $max_meter = 0;
    if($lastLBRow) {
      $max_meter = $lastLBRow['meter_number'] - $lastLBRow['running_km'];
    }
    if($max_meter == 0 || empty($model->trip_id)) {                  
      $result1 = Yii::app()->db->createCommand("SELECT end_meter as max_meter, vehicle_reg_no FROM tbl_movements  WHERE vehicle_reg_no ='".$model->vehicle_reg_no."' ORDER BY id DESC")->queryRow();           
      $result2 = Yii::app()->db->createCommand("SELECT max(meter_number) as max_meter,vehicle_reg_no FROM tbl_digital_log_book_history  WHERE vehicle_reg_no ='".$model->vehicle_reg_no."' ORDER BY created_time_mobile DESC")->queryRow(); 
      $max_meter = !empty($result1) ? $result1['max_meter'] : 0;
          if(!empty($result2) && $max_meter < $result2['max_meter']) {
            $max_meter = $result2['max_meter'];
          }    
    }
     $lastLBRow = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE vehicle_reg_no='".$model->vehicle_reg_no."' order by id DESC")->queryRow();
    return $max_meter + $lastLBRow['missing_KM'];    
  }
    
    private function digitatLogbookInsert($_POST){
      // Check duplicate trip entry
      $_trip_id = $_POST['trip_id'];
      $row = Yii::app()->db->createCommand("Select * FROM tbl_digital_log_book_history WHERE trip_id_end='1' AND trip_id='$_trip_id'")->queryRow();
      if($row) {
        $update_hit_counter=Yii::app()->db->createCommand("UPDATE tbl_digital_log_book_history set hit_counter='".($row["hit_counter"] + 1)."' WHERE id='".$row["id"]."'")->execute();
        $msg="The record already exists but still upated with latest";
        $this->_sendResponse(500, $msg );

        return;
      }

      $model = new DigitalLogBookHistory;
      $model->create_time_server = date('Y-m-d H:i:s');
        // Try to assign POST values to attributes
        foreach($_POST as $var=>$value) {
          // Does the model have this attribute? If not raise an error
          if($model->hasAttribute($var))
            $model->$var = $value;
          else
            $this->_sendResponse(500, sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $var, $_GET['model']) );
        }
        
        //find last meter              
        $max_meter = $this->maxMeter($model);
        $model->meter_number =  sprintf('%0.2f',(double)$max_meter + $model->running_km);   
        if(!(($_POST['trip_id_end'] == 1 && $_POST['day_end'] == 'No')||($_POST['trip_id_end'] == 0 && $_POST['day_end'] == 'Yes'))){
          if($model->save()){
            $this->_sendResponse(200, CJSON::encode($model));
          } else {
            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
            $msg .= "<ul>";
            foreach($model->errors as $attribute=>$attr_errors) {
              $msg .= "<li>Attribute: $attribute</li>";
              $msg .= "<ul>";
              foreach($attr_errors as $attr_error)
                $msg .= "<li>$attr_error</li>";
              $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, $msg );
          }
          return;
          break;
        }
        $requisition_id = $model->requisition_id;
        $user_pin = $model->user_pin;
        $user_name = $model->user_name;
        $Job_level = '';
        $user_cell = '0';
        $user_email = '';
        $user_address = '';
        try {
          if(!empty($user_pin)){
            $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
            $staffInfo = CJSON::decode(file_get_contents($uri));
            $Job_level = $staffInfo[0]['JobLevel'];
            $user_cell=$staffInfo[0]['MobileNo'];
            $user_email=$staffInfo[0]['EmailID'];
            $user_address=$staffInfo[0]['EmailID'];
          }
        } catch (Exception $e){
          $this->_sendResponse(500, sprintf('Unable to load user information from BRAC API!', $_GET['model']) );
          return;
          break;
        }        
        if(!$model->save()){
          // Errors occurred
          $msg = "<h1>Error</h1>";
          $msg .= sprintf("Couldn't create model <b>%s</b>", $_GET['model']);
          $msg .= "<ul>";
          foreach($model->errors as $attribute=>$attr_errors) {
            $msg .= "<li>Attribute: $attribute</li>";
            $msg .= "<ul>";
            foreach($attr_errors as $attr_error)
              $msg .= "<li>$attr_error</li>";
            $msg .= "</ul>";
          }
          $msg .= "</ul>";
          $this->_sendResponse(500, $msg );
          return;
          break;
        }

        $user_department = $model->department;
        $vehicle_reg_no = $model->vehicle_reg_no;
        $vehicletype_id = $model->vehicle_type_id;
        $driver_pin = $model->driver_pin;
        $driver_name = $model->driver_name;
        $dutytype_id = $model->dutytype_id;
        $duty_day = $model->duty_day;
        $offline = strpos($model->message_log,"Offline");

        if(!empty($model->driver_start_time_mobile) && $model->driver_start_time_mobile!="0000-00-00 00:00:00"){
          $statr_DT = explode(" ",$model->driver_start_time_mobile);
          $start_date = $statr_DT[0];
          if(!empty($statr_DT[1])){
            $start_time = $statr_DT[1];

          }
        }
        else{
          $statr_DT=explode(" ", $model->driver_start_time_vts);
          $start_date = $statr_DT[0];
          if(!empty($statr_DT[1])){
            $start_time = $statr_DT[1];
          }
        }
        if(!empty($model->driver_end_time_mobile) && $model->driver_end_time_mobile!="0000-00-00 00:00:00"){
          $end_DT = explode(" ", $model->driver_end_time_mobile);
          $end_date = $end_DT[0];
          if(!empty($end_DT[1])){
            $end_time = $end_DT[1];
          }
        }
        else{
          $end_DT=explode(" ", $model->driver_end_time_vts);
          $end_date = $end_DT[0];
          if(!empty($end_DT[1])){
            $end_time = $end_DT[1];
          }
        }
        $start_time_array=explode(":",  $start_time);
        if(isset($start_time_array[2])){
          $start_time_array[2] = "00";
          $start_time = implode(":", $start_time_array);
        }
        $end_time_array = explode(":",  $end_time);
        if(isset($end_time_array[2])){
          $end_time_array[2]="00";
          $end_time = implode(":", $end_time_array);
        }
        $a = new DateTime($start_time);
        $b = new DateTime($end_time);
        $interval = $a->diff($b);
        $total_time = $interval->format("%H:%I");
        $duty_day = $model->duty_day;
        $end_point = empty($model->location_vts)?$model->location_mobile:$model->location_vts;
        $end_meter = $model->meter_number;
        $sql = "SELECT * FROM tbl_digital_log_book_history Where  trip_id ='".$model->trip_id."' AND bill=0 order by created_time_mobile ASC";
        $sql_rs = Yii::app()->db->createCommand($sql)->queryAll();
        $start_point = empty($sql_rs[0]['location_vts'])?$sql_rs[0]['location_mobile']:$sql_rs[0]['location_vts'];
        $start_meter = $sql_rs[0]['meter_number'];
        $night_halt = 0;
        $nh_amount = 0;
        $morning_ta = 0;
        $lunch_ta = 0;
        $night_ta = 0;
        foreach ($sql_rs as $key => $value) {
          if($value['night_halt_bill']!=0) {
            $night_halt = 1;
            $nh_amount = $value['night_halt_bill'];
          }
          if($value['morning_ta']!=0) {
            $morning_ta = $value['morning_ta'];
          }
          if($value['lunch_ta']!=0) {
            $lunch_ta = $value['lunch_ta'];
          }
          if($value['night_ta']!=0) {
            $night_ta = $value['night_ta'];
          }
          $id_history = $value['id'];
          //$sql_update_history = "UPDATE tbl_digital_log_book_history SET bill=1  Where  id='$id_history'";
          //$sql_update_history_rs = Yii::app()->db->createCommand($sql_update_history)->execute();
         }
        $sup_mile_amount = 0;
        $total_run = $end_meter-$start_meter;
        $rph = $model->running_per_km_rate;
        $sql_dutytypes = "SELECT * FROM tbl_dutytypes WHERE id=".$dutytype_id;
        $sql_dutytypes_rs = Yii::app()->db->createCommand($sql_dutytypes)->queryRow();
        $sql_vehicletypes = "SELECT * FROM tbl_vehicletypes WHERE id=".$vehicletype_id;
        $sql_vehicletypes_rs = Yii::app()->db->createCommand($sql_vehicletypes)->queryRow();
        if($dutytype_id==1){
          $bill_amount = $sql_vehicletypes_rs['rate_per_km']*$total_run;
          $bill_amount = $bill_amount+$bill_amount*$sql_dutytypes_rs['service_charge'];
        }
        else if($dutytype_id==2) {
          $bill_amount = $sql_vehicletypes_rs['rate_per_km_personal']*$total_run;
          $bill_amount = $bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
        }
        else if($dutytype_id==3) {
          $hour = explode(":", $total_time);
          $hour_bill = $hour[0]*$sql_vehicletypes_rs['rph_visitor'];
          if($sql_vehicletypes_rs['minimum_rate']>$hour_bill) {
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_external']*$total_run;
            $bill_amount=$sql_vehicletypes_rs['minimum_rate']+$bill_amount* $sql_dutytypes_rs['service_charge'];
          }
          else{
            $bill_amount=$sql_vehicletypes_rs['rate_per_km_external']*$total_run;
            $bill_amount=$hour_bill+$bill_amount* $sql_dutytypes_rs['service_charge'];
          }
        }
        else if($dutytype_id==4){
          $bill_amount=$sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
          $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
        }
        else if($dutytype_id==5){
          $bill_amount=$sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
          $bill_amount=$bill_amount+$bill_amount* $sql_dutytypes_rs['service_charge'];
        }
        else{
          $bill_amount=$rph*$total_run;
        }

        $over_hour_minute=explode(":", $model->over_time);
        if(isset($over_hour_minute[1])){
          $total_ot_hour=$over_hour_minute[0].".".$over_hour_minute[1];
        }
        else{
          $total_ot_hour=$model->over_time;
        }
        $start_time_hour_minute=explode(":", $start_time);
        if(isset($start_time_hour_minute[1])){
          $start_time=$start_time_hour_minute[0].".".$start_time_hour_minute[1];
        }
        else{
          $start_time=$start_time;
        }
        $end_time_hour_minute=explode(":", $end_time);
        if(isset($end_time_hour_minute[1])){
          $end_time=$end_time_hour_minute[0].".".$end_time_hour_minute[1];
        }
        else{
          $end_time=$end_time;
        }
        $total_ot_amount=$model->over_time_bill;
        $created_by=$model->created_by;
        $created_time=date("Y-m-d h:i:s");
        $trip_id=$model->trip_id;
        if($duty_day=='Official') {
           $duty_day="Office Day";
        }

        $sql_rs = Yii::app()
          ->db
          ->createCommand()
          ->insert(
            'tbl_digital_log_book',
            array(
            'id'=>'',
            'requisition_id'=>$requisition_id,
            'user_pin'=>$user_pin,
            'user_name'=>$user_name,
            'user_level'=>$Job_level,
            'user_dept'=>$user_department,
            'email'=>$user_email,
            'user_cell'=>$user_cell,
            'user_address'=>'',
            'vehicle_reg_no'=>$vehicle_reg_no,
            'vehicletype_id'=>$vehicletype_id,
            'vehicle_location'=>$start_point,
            'driver_pin'=>$driver_pin,
            'driver_name'=>$driver_name,
            'helper_pin'=>'',
            'helper_name'=>'',
            'helper_ot'=>'',
            'dutytype_id'=>$dutytype_id,
            'dutyday'=>$duty_day,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
            'total_time'=>$total_time,
            'start_point'=>$start_point,
            'end_point'=>$end_point,
            'start_meter'=>$start_meter,
            'end_meter'=>$end_meter,
            'total_run'=>$total_run,
            'rph'=>$rph,
            'night_halt'=>$night_halt,
            'nh_amount'=>$nh_amount,
            'morning_ta'=>$morning_ta,
            'lunch_ta'=>$lunch_ta,
            'night_ta'=>$night_ta,
            'sup_mile_amount'=>'',
            'bill_amount'=>$bill_amount,
            'total_ot_hour'=>$total_ot_hour,
            'total_ot_amount'=>$total_ot_amount,
            'created_by'=>$created_by,
            'created_time'=>$created_time,
            'trip_id'=>$trip_id
          ));
    $this->_sendResponse(200, CJSON::encode($model));
    }
  
}



class jobs
{

    public $appUserId; // string
    public $attachments; // attachment
    public $bcc; // string
    public $body; // string
    public $caption; // string
    public $cc; // string
    public $complete; // boolean
    public $feedbackDate; // dateTime
    public $feedbackEmail; // string
    public $feedbackName; // string
    public $feedbackSent; // boolean
    public $fromAddress; // string
    public $fromText; // string
    public $gateway; // string
    public $jobContentType; // string
    public $jobId; // long
    public $jobRecipients; // jobRecipients
    public $mode; // string
    public $numberOfItem; // int
    public $numberOfItemFailed; // int
    public $numberOfItemSent; // int
    public $priority; // string
    public $requester; // string
    public $status; // string
    public $subject; // string
    public $toAddress; // string
    public $toText; // string
    public $udValue1; // string
    public $udValue2; // string
    public $udValue3; // string
    public $udValue4; // string
    public $udValue5; // string
    public $udValue6; // string
    public $udValue7; // string
    public $vtemplate; // string

}

class jobRecipients
{

    public $failCount; // int
    public $image; // base64Binary
    public $job; // jobs
    public $jobDetailId; // long
    public $recipientEmail; // string
    public $sent; // boolean
    public $sentDate; // dateTime
    public $toText; // string

}
?>