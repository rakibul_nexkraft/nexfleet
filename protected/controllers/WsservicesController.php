<?php

class WsservicesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getServiceName'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','excel','update','getName'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Wsservices;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsservices']))
		{
			$model->attributes=$_POST['Wsservices'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Wsservices']))
		{
			$model->attributes=$_POST['Wsservices'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        throw new CHttpException(404,'You are not authorized to delete.');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
    public function actionIndex()
    {
        $model=new Wsservices('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Wsservices']))
            $model->attributes=$_GET['Wsservices'];

        $this->render('index',array(
            'model'=>$model,
            'dataProvider'=>$model->search(),
        ));
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Wsservices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Wsservices']))
			$model->attributes=$_GET['Wsservices'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Wsservices::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function actionGetServiceName() {
			
	  if (!empty($_GET['term'])) {
	  	$qterm = '%'.$_GET['term'].'%';
	 	$sql = "SELECT ws.id as wsservice_id,ws.service_name as value from tbl_wsservices as ws WHERE ws.service_name LIKE '$qterm'";
	 	//print_r($sql);
	 	//$sql = "SELECT ws.service_name as value from tbl_wsservices as ws WHERE ws.service_name LIKE '$qterm'";
		$command = Yii::app()->db->createCommand($sql);				
		$result = $command->queryAll();
		
		$count=0;
		
		
		echo CJSON::encode($result); exit;
	  } else {
		return false;
	  }
	
}
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='wsservices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	 public function actionExcel()
    {
        $model=new Wsservices('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['criteria']))
            $model->attributes=$_GET['criteria'];
        
        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider'=> $model->search(),
            'title'=>'iFleet_Wsservices',
            //'autoWidth'=>true,
            'grid_mode'=>'export',
            'exportType'=>'Excel2007',
            'filename'=>'iFleet_Wsservices',

                'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
       
        'service_name',
        'service_category',
        'car',
        'micro_jeep',
        'pickup',
        'bus_coaster',
     //   'active',
/*
        'created_by',
        'created_time',
        'updated_by',
        'updated_time',
*/
    ),
            
        ));
        Yii::app()->end();
    }
    
	
	
	
}
