<?php
date_default_timezone_set("Asia/Dhaka");
class DriverTrainingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getName','Excel'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DriverTraining;
		$model->created_time=date('Y-m-d H:i:s');
		$model->created_by=Yii::app()->user->name;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DriverTraining']))
		{
			$model->attributes=$_POST['DriverTraining'];
			$training_year='';
			if(!empty($model->defence_traning_date) && $model->defence_traning_date!='0000-00-00')
			{
				$training_year=$model->defence_traning_date;
				$training_year=explode('-', $training_year);
				$training_year=$training_year[0];
				
			}
			else
			{
				if(!empty($model->gen_awarness_traning_date) && $model->gen_awarness_traning_date!='0000-00-00')
				{
					$training_year=$model->gen_awarness_traning_date;
					$training_year=explode('-', $training_year);
					$training_year=$training_year[0];
					
				}
				else
				{
					if(!empty($model->google_map_training_date) && $model->google_map_training_date!='0000-00-00')
					{
						$training_year=$model->google_map_training_date;
						$training_year=explode('-', $training_year);
						$training_year=$training_year[0];
						
					
					}
				}
				
			}
			if(!empty($training_year))
			{
				$sql="SELECT * FROM tbl_driver_training WHERE driver_pin='$model->driver_pin' AND (YEAR(defence_traning_date)='$training_year' OR YEAR(gen_awarness_traning_date)='$training_year' OR  YEAR(google_map_training_date)='$training_year')";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
				if($sql_rs)
				{
					$model=$this->loadModel($sql_rs['id']);
					$this->render('update',array(
									'model'=>$model,
								));
				}

			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->attributes=$_POST['FeedbackSettings'];
		$model->updated_time=date('Y-m-d H:i:s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DriverTraining']))
		{
			$model->attributes=$_POST['DriverTraining'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new DriverTraining('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DriverTraining']))
			$model->attributes=$_GET['DriverTraining'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DriverTraining('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DriverTraining']))
			$model->attributes=$_GET['DriverTraining'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DriverTraining::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='driver-training-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	 public function actionGetName()
    {
    	if (!empty($_GET['DriverTraining']['driver_pin'])) {
            $driver_pin = $_GET['DriverTraining']['driver_pin'];
            $driver_sql = "SELECT * FROM tbl_drivers where  pin = '$driver_pin'";
            $driver = Yii::app()->db->createCommand($driver_sql)->queryRow();
            echo CJSON::encode($driver['name']);
            return;
            if (!empty($driver)) {               

                echo CJSON::encode($driver['name']);
                exit;
            } else
                echo CJSON::encode("Not Found!");
            exit;
        } else {
            return false;
        }
    }
    public function actionExcel()
	{
		$model = new DriverTraining ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Driver Training Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Driver_Training_Information',
            //'stream'=>false,
           'columns'=>array(
						'driver_pin',
		'name',
		
		array(
            'name' => 'defence_traning',
            'type'=>'raw',
            'value' => '($data->defence_traning==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),
         'defence_traning_date',
		array(
            'name' => 'gen_awarness_traning',
            'type'=>'raw',
            'value' => '($data->gen_awarness_traning==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),      
        'gen_awarness_traning_date',
        array(
            'name' => 'google_map_training',
            'type'=>'raw',
            'value' => '($data->google_map_training==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),      
        'google_map_training_date',
			),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
}
