<?php
/* @var $this WsinsideController */
/* @var $model Wsinside */

$this->breadcrumbs=array(
	'Wsinsides'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Wsinside', 'url'=>array('index')),
	array('label'=>'Create Wsinside', 'url'=>array('create')),
	array('label'=>'Update Wsinside', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Wsinside', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Wsinside', 'url'=>array('admin')),
);
?>

<h1>View Wsinside #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'jobcard_id',
		'wsservicetype_id',
		'wssupplier_id',
		'wsrequisition_id',
		'item_id',
		'unit_price',
		'item_size',
		'quantity',
		'req_quantity',
		'issue_date',
		'remarks',
		'active',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
