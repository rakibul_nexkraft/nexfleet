<?php
/* @var $this WsinsideController */
/* @var $model Wsinside */

$this->breadcrumbs=array(
	'Wsinsides'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Wsinside', 'url'=>array('index')),
	array('label'=>'Create Wsinside', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('wsinside-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Wsinsides</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'wsinside-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'jobcard_id',
		'wsservicetype_id',
		'wssupplier_id',
		'wsrequisition_id',
		'item_id',
		/*
		'unit_price',
		'item_size',
		'quantity',
		'req_quantity',
		'issue_date',
		'remarks',
		'active',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
