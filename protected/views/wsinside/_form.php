<?php
/* @var $this WsinsideController */
/* @var $model Wsinside */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsinside-form',
	'enableAjaxValidation'=>true,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'jobcard_id'); ?>
		<?php echo $form->textField($model,'jobcard_id'); ?>
		<?php echo $form->error($model,'jobcard_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wsservicetype_id'); ?>
		<?php echo $form->textField($model,'wsservicetype_id'); ?>
		<?php echo $form->error($model,'wsservicetype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wssupplier_id'); ?>
		<?php echo $form->textField($model,'wssupplier_id'); ?>
		<?php echo $form->error($model,'wssupplier_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wsrequisition_id'); ?>
		<?php echo $form->textField($model,'wsrequisition_id'); ?>
		<?php echo $form->error($model,'wsrequisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_id'); ?>
		<?php echo $form->textField($model,'item_id'); ?>
		<?php echo $form->error($model,'item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unit_price'); ?>
		<?php echo $form->textField($model,'unit_price'); ?>
		<?php echo $form->error($model,'unit_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_size'); ?>
		<?php echo $form->textField($model,'item_size',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'item_size'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'req_quantity'); ?>
		<?php echo $form->textField($model,'req_quantity',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'req_quantity'); ?>
	</div>

	<div class="row">
        
        <?php echo $form->labelEx($model,'issue_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'issue_date',     
            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->issue_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:200px;'
        ),
        )); ?>





	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>


   

	<div class="row buttons">
		<?php //if(!Yii::app()->request->isAjaxRequest) echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
		<?php //else
			//{
				echo CHtml::ajaxSubmitButton("Save",CHtml::normalizeUrl(array('wsinside/create','render'=>false)),array('success'=>'js: function(data) {
                        $("#jobDialog").dialog("close");
                    }'),array('id'=>'closeJobDialog'));
			//}
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->