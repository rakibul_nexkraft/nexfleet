<?php
/* @var $this SmsservicesController */
/* @var $model Smsservices */

$this->breadcrumbs=array(
	'Smsservices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Smsservices', 'url'=>array('index')),
	array('label'=>'Create Smsservices', 'url'=>array('create')),
	array('label'=>'View Smsservices', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Smsservices', 'url'=>array('admin')),
);
}
?>

<h1>Update Smsservices <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>