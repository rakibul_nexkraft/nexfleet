<?php
/* @var $this SmsservicesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Smsservices',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Create Smsservices', 'url'=>array('create')),
	array('label'=>'Manage Smsservices', 'url'=>array('admin')),
);
}
?>

<h1>Smsservices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
