<?php
/* @var $this SmsservicesController */
/* @var $model Smsservices */

$this->breadcrumbs=array(
	'Smsservices'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Smsservices', 'url'=>array('index')),
	array('label'=>'Create Smsservices', 'url'=>array('create')),
	array('label'=>'Update Smsservices', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Smsservices', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Smsservices', 'url'=>array('admin')),
);
}
?>

<h1>View Smsservices #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sent_to',
		'message',
		'sent_by',
		'sent_time',
		'status',
	),
)); ?>
