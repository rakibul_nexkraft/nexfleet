<?php
/* @var $this SmsservicesController */
/* @var $model Smsservices */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'smsservices-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'sent_to'); ?>		
		<?php $sent_to = Routes::model()->findAll(array('select'=>'id,route_no','order' => 'id ASC'));
        echo $form->dropDownList($model,'sent_to', CHtml::listData($sent_to,'route_no',  'route_no'),array('empty' => 'Select Route...')); ?>
		<?php echo $form->error($model,'sent_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>50,'maxlength'=>125)); ?>
		125 Characters
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->