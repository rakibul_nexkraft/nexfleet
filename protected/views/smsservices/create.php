<?php
/* @var $this SmsservicesController */
/* @var $model Smsservices */

$this->breadcrumbs=array(
	'Smsservices'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Smsservices', 'url'=>array('index')),
	array('label'=>'Manage Smsservices', 'url'=>array('admin')),
);
}
?>

<h1>Create Smsservices</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>