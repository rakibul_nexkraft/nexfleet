<?php
/* @var $this WsmsexpensesController */
/* @var $model Wsmsexpenses */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsmsexpenses-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php 
    if(isset($erro) && !empty($erro)){ ?>
    <div class="errorSummary">
        <p>Please fix the following input errors:</p>
        <ul>
        <?php foreach ($erro as $key => $value) {
   

           echo "<li>".$value[0]."</li>";
        }?>
    </ul>
    </div>
    <?php }    ?>

	<?php echo $form->errorSummary($model); ?>
	<div class="containerForm">

	<div class="row">
		<?php echo $form->labelEx($model,'item_name'); ?>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'item_name'); ?>
	</div>

        <div class="row">
            <?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
            <?php

            $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'model'=>$model,
                'id'=>'Wsmsexpenses_vehicle_reg_no',
                'attribute' => 'vehicle_reg_no',

                'source'=>$this->createUrl('vehicles/allRegNo1'),

                // additional javascript options for the autocomplete plugin
                'options'=>array(
                    'minLength'=>'2',
                    'select'=>"js: function(event, ui) {

         }"
                ),
                'htmlOptions'=>array(
                    'style'=>'height:20px;'
                ),
            ));		?>


            <?php  echo $form->error($model,'vehicle_reg_no'); ?>
        </div>


        <div class="row">
        <?php echo $form->labelEx($model,'requisition_no'); ?>
        <?php echo $form->textField($model,'requisition_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'requisition_no'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'requisition_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'id'=>'requisition_date',
            'model'=>$model,
            'attribute'=>'requisition_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->requisition_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),
        )); ?>
        <?php echo $form->error($model,'requisition_date'); ?>
    </div>

<!--
	<div class="row">
		<?php echo $form->labelEx($model,'wsitemname_id'); ?>
		<?php echo $form->textField($model,'wsitemname_id'); ?>
		<?php echo $form->error($model,'wsitemname_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wssupplier_id'); ?>
		<?php echo $form->textField($model,'wssupplier_id'); ?>
		<?php echo $form->error($model,'wssupplier_id'); ?>
	</div>
-->

    <div class="row">
        <?php  echo $form->labelEx($model,'wssupplier_name'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'id'=>'wssupplier_name',
            'model'=>$model,
            'attribute' => 'wssupplier_name',
            'source'=>$this->createUrl('wssupplier/getSupplierName'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
         }"
            ),
            /*'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),*/
        ));		?>
        <?php  echo $form->error($model,'wssupplier_name'); ?>
    </div>
</div>
<div class="containerForm">
     <div class="row">
        <?php echo $form->labelEx($model,'challan_no'); ?>
        <?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'challan_no'); ?>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,'challan_date'); ?>
    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'challan_date',

        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->challan_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
    <?php echo $form->error($model,'challan_date'); ?>
    </div>
        <div class="row">
            <?php echo $form->labelEx($model,'bill_no'); ?>
            <?php echo $form->textField($model,'bill_no',array('size'=>60,'maxlength'=>127)); ?>
            <?php echo $form->error($model,'bill_no'); ?>
        </div>

    <div class="row">
        <?php echo $form->labelEx($model,'bill_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'id'=>'bill_date',
            'model'=>$model,
            'attribute'=>'bill_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->bill_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),
        )); ?>
        <?php echo $form->error($model,'bill_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'item_size'); ?>
        <?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
        <?php echo $form->error($model,'item_size'); ?>
    </div>
  </div>
<div class="containerForm">
    
    <div class="row">
        <?php echo $form->labelEx($model,'status'); ?>
        <?php //echo $form->textField($model,'licensetype',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->dropDownList($model, 'status', $remarkstype); ?>
        <?php echo $form->error($model,'status'); ?>
    </div>

    <div class="row">
		<?php echo $form->labelEx($model,'unit_price'); ?>
		<?php echo $form->textField($model,'unit_price',array('id'=>'misc_unit_price','onkeyup'=>'totaPriceCount()'));?>
		<?php echo $form->error($model,'unit_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity',array('id'=>'misc_quntity','onkeyup'=>'totaPriceCount()'));?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_price'); ?>
		<?php echo $form->textField($model,'total_price',array('id'=>'misc_total_price'));?>
		<?php echo $form->error($model,'total_price'); ?>
	</div>
     <script>
        function totaPriceCount(){
          var qunt=document.getElementById('misc_quntity').value;
          var unitP=document.getElementById('misc_unit_price').value;
          //document.getElementById('tyre_total_price').value=(qunt*unitP).toFixed(2);
          document.getElementById('misc_total_price').value=qunt*unitP;
        }
    </script>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>
</div>



	<div class="clear"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

    <script>
        function tot_price()
        {

            $('#Wsmsexpenses_total_price').val($('#Wsmsexpenses_unit_price').val()*$('#Wsmsexpenses_quantity').val());
        }
    </script>

</div><!-- form -->