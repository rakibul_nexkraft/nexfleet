<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsmsexpensesController */
/* @var $model Wsmsexpenses */

$this->breadcrumbs=array(
	'Wsmsexpenses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Misc Expenses', 'url'=>array('index'),'visible'=> in_array('List Misc Expenses', $user_rights)),
  /*array('label'=>'New Misc Expense', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/wsmsexpenses/create','id'=>$model->id),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
			return false;
		}",
	)))),*/

	array('label'=>'Update Misc Expense', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Misc Expense', $user_rights)),
	array('label'=>'Delete Misc Expense', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Misc Expense', $user_rights)),
	array('label'=>'Manage Misc Expenses', 'url'=>array('admin'),'visible'=> in_array('Manage Misc Expenses', $user_rights)),
);
?>

<h4>View Misc Expenses #<?php echo $model->id; ?></h4>

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_name',
        'vehicle_reg_no',
        'requisition_no',
        'requisition_date',
		'wsitemname_id',
		'wssupplier_id',
		'wssupplier_name',
        'bill_no',
		'bill_date',
		'unit_price',
		'item_size',
		'quantity',
		'total_price',
		'remarks',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); */?>
<?php 

$this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
			'id', 'item_name',
			'vehicle_reg_no',	'requisition_no',
			'challan_no',	'requisition_date',
			'challan_date',	'quantity', 
			 'wssupplier_name','unit_price',
				'bill_date','total_price',	
			'bill_no',	'item_size',					
			'updated_by','created_by',
			'updated_time','created_time',			
			'status','item_size',				
			),
		),
    ),
)); ?>

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
	)); 
?>
	
	<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
		<a class="close" data-dismiss="modal">&times;</a>
		<?php echo Yii::app()->user->getFlash('success'); ?>
	</div>
	
<?php $this->endWidget();
	endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
	)); ?>
 
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <br />
</div>
 
<div class="modal-body">
</div>
 
<?php 
	$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
 ?>