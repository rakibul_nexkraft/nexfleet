<?php
/* @var $this DriversController */
/* @var $model Drivers */

$this->breadcrumbs=array(
	'Drivers'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Drivers List', 'url'=>array('index')),
		array('label'=>'New Driver', 'url'=>array('create')),
	);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('drivers-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Drivers</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php  $this->widget('bootstrap.widgets.TbGridView', array(
   'id'=>'drivers-grid',
	'type'=>'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
	'template'=>"{items}\n{pager}",
	'columns'=>array(
		'id',
		'name',
		'pin',
		'phone',
		'dvr_license_no',
		'dvr_license_type',
		'issued_from',
		'issued_date',
		'valid_to',
        'blood_group',
		'active',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); 	?>

<?php /* $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'drivers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'pin',
		'phone',
		'active',
		array(
			'class'=>'CButtonColumn',
		),
	),
));  */ ?>
