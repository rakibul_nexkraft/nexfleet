<?php
/* @var $this DriversController */
/* @var $model Drivers */

$this->breadcrumbs=array(
	'Drivers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Drivers List', 'url'=>array('index')),
		array('label'=>'New Driver', 'url'=>array('create')),
		array('label'=>'View Driver', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Drivers', 'url'=>array('admin')),
	);
}
?>

<h4>Update Driver : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'active_status'=>$this->active_status)); ?>