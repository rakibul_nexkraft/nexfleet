<style>
.fontColorRed,.fontColorRed td a{color:red;}
</style>
<?php
/* @var $this DriversController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Drivers',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Driver', 'url'=>array('create')),
		array('label'=>'Manage Drivers', 'url'=>array('admin')),
		array('label'=>'Driver Training', 'url'=>array('/driverTraining/index')),
	);
}
?>

<h4>Drivers</h4>
<div class="search-form">
	<div class="wide form">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'id'); ?>
				<?php echo $form->textField($model,'id'); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'pin'); ?> 
				<?php echo $form->textField($model,'pin'); ?>
			</div>
		</div>



			<div class="clearfix"></div>
		    
		    <div align="left">
		    <?php $this->widget('bootstrap.widgets.TbButton',array(
		        'label' => 'Search',
		        'type' => 'primary',
		        'buttonType'=>'submit', 
		        'size' => 'medium'
		        ));
		    ?>
		        
		    </div>

		<?php $this->endWidget(); ?>
	</div>

</div><!-- search-form -->

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'drivers-grid',
	'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'rowCssClassExpression'=>'$data->color',
	'columns'=>array(
		'id',
		array(
            'name' => 'name',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->name),array("view","id"=>$data->id))',
        ),
		'pin',
		'phone',
		'dvr_license_no',
		'dvr_license_type',
		'issued_from',
		'issued_date',
		'valid_to',
		'driver_rating',
        'blood_group',
		'active',
	),
)); 


echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Drivers'])));

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#drivers-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Drivers List',
));
?>