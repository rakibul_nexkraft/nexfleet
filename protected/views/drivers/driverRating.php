
<?php
/* @var $this DriversController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Drivers Rating',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'New Driver', 'url'=>array('create')),
		array('label'=>'Feedback Rating', 'url'=>array('/FeedbackRating/index')),
		array('label'=>'Manage Drivers', 'url'=>array('admin')),
	);
}
?>

<h4>Drivers Rating</h4>
<div class="search-form">
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<!--<div class="fl">
	<div class="row">
		<?php //echo $form->label($model,'id'); ?><br />
		<?php //echo $form->textField($model,'id'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php //echo $form->label($model,'name'); ?> <br />
		<?php //echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
	</div>
</div>-->
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'pin'); ?> <br />
		<?php echo $form->textField($model,'pin'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_rating'); ?> <br />
		<?php echo $form->textField($model,'driver_rating'); ?>
	</div>
</div>
<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>
</fieldset>

	

	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
</div>
<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'drivers-grid',
	'dataProvider'=>$model->search1(),
    //'filter'=>$model,
    //'rowCssClassExpression'=>'$data->color',
	'columns'=>array(	
		
		array(
            'name' => 'pin',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->pin),array("view","id"=>$data->id))',
        ),
		'name',		
        'driver_rating',
		 'phone'
		
		
		
	),
)); 

/*$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#drivers-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Drivers List',
));*/
?>
<?php
//$total=$model->search()->getTotalItemCount();
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','Drivers'=>$_GET['Drivers'])));
?> &nbsp; &nbsp; &nbsp;