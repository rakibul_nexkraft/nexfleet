<?php
/* @var $this DailyVehicleCheckController */
/* @var $model DailyVehicleCheck */

$this->breadcrumbs=array(
	'Daily Vehicle Checks'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List DailyVehicleCheck', 'url'=>array('index')),
		array('label'=>'Create DailyVehicleCheck', 'url'=>array('create')),
		array('label'=>'Update DailyVehicleCheck', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete DailyVehicleCheck', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage DailyVehicleCheck', 'url'=>array('admin')),
	);
}
?>

<h1>View DailyVehicleCheck #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		'mobil',
		'battery',
		'extra_wheel',
		'break',
		'tools',
		'radiator_water',
		'indicator_light',
		'fornt_back_light',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		'check_date',
	),
)); ?>
