<?php
/* @var $this DailyVehicleCheckController */
/* @var $model DailyVehicleCheck */

$this->breadcrumbs=array(
	'Daily Vehicle Checks'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List DailyVehicleCheck', 'url'=>array('index')),
		array('label'=>'Create DailyVehicleCheck', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('daily-vehicle-check-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Daily Vehicle Checks</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'daily-vehicle-check-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_reg_no',
		'mobil',
		'battery',
		'extra_wheel',
		'break',
		/*
		'tools',
		'radiator_water',
		'indicator_light',
		'fornt_back_light',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		'check_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
