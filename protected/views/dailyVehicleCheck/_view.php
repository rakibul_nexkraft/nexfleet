<?php
/* @var $this DailyVehicleCheckController */
/* @var $data DailyVehicleCheck */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobil')); ?>:</b>
	<?php echo CHtml::encode($data->mobil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('battery')); ?>:</b>
	<?php echo CHtml::encode($data->battery); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extra_wheel')); ?>:</b>
	<?php echo CHtml::encode($data->extra_wheel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('break')); ?>:</b>
	<?php echo CHtml::encode($data->break); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tools')); ?>:</b>
	<?php echo CHtml::encode($data->tools); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('radiator_water')); ?>:</b>
	<?php echo CHtml::encode($data->radiator_water); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('indicator_light')); ?>:</b>
	<?php echo CHtml::encode($data->indicator_light); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fornt_back_light')); ?>:</b>
	<?php echo CHtml::encode($data->fornt_back_light); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('check_date')); ?>:</b>
	<?php echo CHtml::encode($data->check_date); ?>
	<br />

	*/ ?>

</div>