<?php
/* @var $this DailyVehicleCheckController */
/* @var $model DailyVehicleCheck */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'daily-vehicle-check-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobil'); ?>
		<?php echo $form->textField($model,'mobil'); ?>
		<?php echo $form->error($model,'mobil'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'battery'); ?>
		<?php echo $form->textField($model,'battery'); ?>
		<?php echo $form->error($model,'battery'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'extra_wheel'); ?>
		<?php echo $form->textField($model,'extra_wheel'); ?>
		<?php echo $form->error($model,'extra_wheel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'break'); ?>
		<?php echo $form->textField($model,'break'); ?>
		<?php echo $form->error($model,'break'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tools'); ?>
		<?php echo $form->textField($model,'tools'); ?>
		<?php echo $form->error($model,'tools'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'radiator_water'); ?>
		<?php echo $form->textField($model,'radiator_water'); ?>
		<?php echo $form->error($model,'radiator_water'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indicator_light'); ?>
		<?php echo $form->textField($model,'indicator_light'); ?>
		<?php echo $form->error($model,'indicator_light'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fornt_back_light'); ?>
		<?php echo $form->textField($model,'fornt_back_light'); ?>
		<?php echo $form->error($model,'fornt_back_light'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'check_date'); ?>
		<?php echo $form->textField($model,'check_date'); ?>
		<?php echo $form->error($model,'check_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->