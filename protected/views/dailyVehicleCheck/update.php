<?php
/* @var $this DailyVehicleCheckController */
/* @var $model DailyVehicleCheck */

$this->breadcrumbs=array(
	'Daily Vehicle Checks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
	array('label'=>'List DailyVehicleCheck', 'url'=>array('index')),
	array('label'=>'Create DailyVehicleCheck', 'url'=>array('create')),
	array('label'=>'View DailyVehicleCheck', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DailyVehicleCheck', 'url'=>array('admin')),
	);
}
?>

<h1>Update DailyVehicleCheck <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>