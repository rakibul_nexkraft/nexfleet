<?php
/* @var $this DailyVehicleCheckController */
/* @var $model DailyVehicleCheck */

$this->breadcrumbs=array(
	'Daily Vehicle Checks'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List DailyVehicleCheck', 'url'=>array('index')),
		array('label'=>'Manage DailyVehicleCheck', 'url'=>array('admin')),
	);
}
?>

<h1>Create DailyVehicleCheck</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>