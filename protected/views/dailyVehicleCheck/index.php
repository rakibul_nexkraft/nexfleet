<?php
/* @var $this DailyVehicleCheckController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Daily Vehicle Checks',
);

/*$this->menu=array(
	array('label'=>'Create DailyVehicleCheck', 'url'=>array('create')),
	array('label'=>'Manage DailyVehicleCheck', 'url'=>array('admin')),
);*/
?>

<h1>Daily Vehicle Checks</h1>
<div class="search-form">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
 </div>
<div id="def_container">
<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ 

$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'dailyVehicleChecks-grid',    
    'dataProvider'=>$dataProvider,
    

    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'vehicle_reg_no',
        array(
            'name' => 'mobil',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->mobil)',
        ),
       array(
            'name' => 'battery',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->battery)',
        ),array(
            'name' => 'extra_wheel',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->extra_wheel)',
        ),array(
            'name' => 'tools',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->tools)',
        ),array(
            'name' => 'radiator_water',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->radiator_water)',
        ),array(
            'name' => 'indicator_light',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->indicator_light)',
        ),array(
            'name' => 'fornt_back_light',
            'type'=>'raw',
            'value' => 'DailyVehicleCheck::model()->check($data->fornt_back_light)',
        ),
        'check_date',
        'created_by',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                'view'=>array(                    
                    'visible'=>'!Yii::app()->user->isViewUser()',
                ),
                'update'=>array(
                    'visible'=>'!Yii::app()->user->isViewUser()',
                ),
                'delete'=>array(
                    'visible'=>'!Yii::app()->user->isViewUser()',
                ),
            ),
        ),
	),
));
?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['DailyVehicleCheck'])));
?> 
</div>