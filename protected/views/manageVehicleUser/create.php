<?php
/* @var $this ManageVehicleUserController */
/* @var $model ManageVehicleUser */

$this->breadcrumbs=array(
	'Manage Vehicle Users'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Manage Vehicles User', 'url'=>array('index')),
		array('label'=>'Manage Manage Vehicles User', 'url'=>array('admin')),
	);
}
?>

<h1>Create Manage Vehicles User</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>