<?php
/* @var $this ManageVehicleUserController */
/* @var $model ManageVehicleUser */

$this->breadcrumbs=array(
	'Manage Vehicles By User'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Manage Vehicles By User', 'url'=>array('index')),
		array('label'=>'Create Manage Vehicles By User', 'url'=>array('create')),
		array('label'=>'View Manage Vehicles By User', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Manage Vehicles By User', 'url'=>array('admin')),
	);
}
?>

<h1>Update Manage Vehicles By User <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>