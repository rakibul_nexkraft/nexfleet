<?php
/* @var $this ManageVehicleUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manage Vehicle Users',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Manage Vehicles By User', 'url'=>array('create')),
		array('label'=>'Manage Manage Vehicles By User', 'url'=>array('admin')),
	);
}
?>

<h1>Manage Vehicles By  Users</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manage-vehicle-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_name',
		'vehicle_re_no',
		'created_by',
		'created_time',
		'updated_by',	
		'updated_time',		
		
	),
)); ?>
