<?php
/* @var $this ManageVehicleUserController */
/* @var $model ManageVehicleUser */

$this->breadcrumbs=array(
	'Manage Vehicle Users'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List ManageVehicleUser', 'url'=>array('index')),
		array('label'=>'Create ManageVehicleUser', 'url'=>array('create')),
	);
}

?>

<h1>Manage Manage Vehicle Users</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manage-vehicle-user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_name',
		'vehicle_re_no',
		'created_by',
		'created_time',
		'updated_by',	
		'updated_time',		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
