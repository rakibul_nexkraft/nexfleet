<?php
/* @var $this ManageVehicleUserController */
/* @var $model ManageVehicleUser */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manage-vehicle-user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php //echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>128)); 
				$this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'user_name',
            'data'=>$model->userName(),
            'options'  => array(
                'style'=>'width:60px',
                'allowClear'=>true,
                'placeholder'=>'Select User PIN',
                'minimumInputLength' => 1,
            ),
        ));

		?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_re_no'); ?>
		<?php //echo $form->textField($model,'vehicle_re_no',array('size'=>60,'maxlength'=>128)); 

			$this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'vehicle_re_no',
            'data'=>Requisitions::getAllVehicle(),
            'options'  => array(
                'style'=>'width:100%',
                'allowClear'=>true,
                'placeholder'=>'Select Vehicle Reg No',
                'minimumInputLength' => 1,
            ),
        ));


		?>
		<?php echo $form->error($model,'vehicle_re_no'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->