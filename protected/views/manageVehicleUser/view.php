<?php
/* @var $this ManageVehicleUserController */
/* @var $model ManageVehicleUser */

$this->breadcrumbs=array(
	'Manage Vehicle Users'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Manage Vehicles By User', 'url'=>array('index')),
		array('label'=>'Create Manage Vehicles By User', 'url'=>array('create')),
		array('label'=>'Update Manage Vehicles By User', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Manage Vehicles By User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Vehicle By User', 'url'=>array('admin')),
	);
}
?>

<h1>View Manage Vehicles By User #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_name',
		'vehicle_re_no',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
