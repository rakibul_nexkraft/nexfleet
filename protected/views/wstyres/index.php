<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WstyresController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wstyres',
);

$this->menu=array(
	//array('label'=>'New Tyre', 'url'=>array('create')),
	array('label'=>'New Tyre', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/wstyres/create'),
		'datatype'=>'json',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
            $('#popModal').css('z-index','1050');
            $('.modal-backdrop').css('z-index','1000');
            return false;
        }",
    )),'visible'=> in_array('Manage Tyres', $user_rights))),
    array('label'=>'Manage Tyres', 'url'=>array('admin'),'visible'=> in_array('Manage Tyres', $user_rights)),
    array('label'=>'Add Purchase Status', 'url'=>array('/purchaseStatus/create'),'visible'=> in_array('Tyre Purchase Status', $user_rights)),
);
//var_dump($model);
//$model_json=json_encode($model->errors); var_dump($model_json);

?>

<h4>Tyres</h4>
<?php if($model->hasErrors()){ $model_json_error=json_encode($model->errors); $model_json_attributes=json_encode($model->attributes);?>
    <script type="text/javascript">
       $.post('<?php echo Yii::app()->createAbsoluteUrl("wstyres/create");?>',{erro:'<?php echo $model_json_error;?>',attr:'<?php echo $model_json_attributes;?>'},function(data){$('#popModal').modal('show');
        $('.modal-body').html(data);
        $('#popModal').css("z-index","999");
        $('.modal-backdrop').css("z-index","990");
        return false;});
    </script>

    <?php $model->unsetAttributes();} ?>
    <div class="search-form">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
    </div>
    <div id="wst_container">
        <?php


        $var = $_REQUEST['Wstyres'];

        if($var):
            ?>



            <h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
            <?php if($var['purchase_date']) echo "User: ".$var['purchase_date']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
            <?php if($var['vehicle_reg_no'])echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

            <?php

            if($var['active']==0);
            else if($var['active']==1)$active= "Not Approved";
            else if($var['active']==2)$active= "Approved";
            if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
        <?php endif;?>
        
        <?php $this->widget('bootstrap.widgets.TbGridView', array(
            'type'=>'striped bordered condensed',
            'id'=>'wstyres-grid',
        //'dataProvider'=>$dataProvider,
            'dataProvider'=>$model->search(),
            'filter'=>$model,


            'columns'=>array(
                array(
                    'name' => 'id',
                    'type'=>'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),
                'defect_id',
                'vehicle_reg_no',
                array(
                    'name' => 'vehicle_type',
                    'type'=>'raw',
                    'value' => '$data->vehicletypes->type',
                ),
                'purchase_date',
                'requisition_no',
                'km_reading',
                'supplier',
                'challan_no',
                'bill_no',
                'bill_date',
                'unit_price',
                'total_price',
                'amount',
                'quantity',
                'warrenty_km',
                'warrenty_valid_km_upto',
                'actual_use_km',
                'status',
                
            ),
        ));
        ?>
        <?php
        echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Wstyres'])));
        ?> &nbsp; &nbsp; &nbsp;
        <?php if(Yii::app()->user->hasFlash('success')): ?>

        <?php $this->beginWidget('bootstrap.widgets.TbModal', array(
           'id'=>'alert_msg',
           'autoOpen'=>true,
    //'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
           'htmlOptions'=>array('style'=>'top:80%'),
           'options'=>array('backdrop'=>false),
           'events'=>array('shown'=>'js: function(){
            setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000);
        }'),
       )); 
       ?>
       
       <div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
          <a class="close" data-dismiss="modal">&times;</a>
          <?php echo Yii::app()->user->getFlash('success'); ?>
      </div>
      
      <?php $this->endWidget();?>

      <script type="text/javascript">
       $.post('<?php echo Yii::app()->createAbsoluteUrl("wstyres/create");?>',{id:'<?php echo $_GET['id'];?>'},function(data){$('#popModal').modal('show');
        $('.modal-body').html(data);
        $('#popModal').css("z-index","999");
        $('.modal-backdrop').css("z-index","990");
        return false;});
    </script>

<?php  endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:82%; height:70%; padding:1px; vertical-align:middle; left:25% !important;'),
)); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>


