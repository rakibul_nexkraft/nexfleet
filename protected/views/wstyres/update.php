<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WstyresController */
/* @var $model Wstyres */

$this->breadcrumbs=array(
	'Wstyres'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tyres', 'url'=>array('index'),'visible'=> in_array('List Tyres', $user_rights)),
	array('label'=>'New Tyre', 'url'=>array('create'),'visible'=> in_array('New Tyre', $user_rights)),
	array('label'=>'View Tyre', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Tyre', $user_rights)),
	array('label'=>'Manage Tyres', 'url'=>array('admin'),'visible'=> in_array('Manage Tyres', $user_rights)),
);
?>

<h4>Update Tyre <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'remarkstype'=>$remarkstype,)); ?>