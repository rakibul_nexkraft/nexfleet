<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WstyresController */
/* @var $model Wstyres */

$this->breadcrumbs=array(
	'Wstyres'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Tyres', 'url'=>array('index'),'visible'=> in_array('List Tyres', $user_rights)),
	array('label'=>'New Tyre', 'url'=>array('create'),'visible'=> in_array('New Tyre', $user_rights)),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('wstyres-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h4>Manage Tyres</h4>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'wstyres-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'defect_id',
						'vehicle_reg_no',
						'vehicle_type',
						'purchase_date',
						'requisition_no',
						'km_reading',
		/*
		'supplier',
		'bill_number',
		'bill_amount',
		'warrenty_km',
		'warrenty_valid_km_upto',
		'actual_use_km',
		'remarks',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
