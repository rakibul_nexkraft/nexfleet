<?php
/* @var $this InvoiceLineController */
/* @var $model InvoiceLine */

$this->breadcrumbs=array(
	'Invoice Lines'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InvoiceLine', 'url'=>array('index')),
	array('label'=>'Create InvoiceLine', 'url'=>array('create')),
	array('label'=>'View InvoiceLine', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage InvoiceLine', 'url'=>array('admin')),
);
?>

<h1>Update InvoiceLine <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>