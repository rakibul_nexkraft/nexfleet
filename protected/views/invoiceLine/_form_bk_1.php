<style>
	.form #invoice-line-form{
		margin-bottom: unset;
	}

</style>

<!-- Task by Rakib  -->
<script>
	
	$("#togglet-id").click(function(){
	  $("#invoice-div").slideToggle(500);
	  $(".toggle-closed").toggle();
	  $(".toggle-open").toggle();

	});

</script>
<!-- Task by Rakib  -->

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'invoice-line-form',
		'enableAjaxValidation'=>false,
	));
	$product_attributes_array= array(
		"Model" => "Model",
		"Vehicle Type" => "Vehicle Type",
		"Size" => "Size",
		"Parts No" => "Parts No",

	); 

	?>

	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Invoice Line</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>


	<?php echo $form->errorSummary($model); ?>
	<!-- togglet-id added by rakib -->
	<div class="toggle toggle-bg toggle-section-responsive">
		<div class="togglet" id="togglet-id">
			<i class="toggle-closed icon-ok-circle" id="toggle-closed"></i>
			<i class="toggle-open icon-remove-circle" id="toggle-open"></i>
		Create Invoice</div>
		<!-- togglet-id added by rakib -->
			<div class="togglec">
				<div id="invoice-div" style="display: none;">		
				<?php	
					$this->renderPartial('/invoice/invoice_form',array(
						'model'=>$invoice,				
					));		
				?>
				</div>
			</div>
		</div> 
	<!-- togglet-id added by rakib -->
	+

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'invoice_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<select data-trigger="" name="InvoiceLine[invoice_id]">
				<?php
				echo "<option value='' selected>Select Invoice Number</option>";
				$all_queried_value = Invoice::model()->findAll();
				foreach ($all_queried_value as $key => $value) {
					if (isset($_GET['InvoiceLine']['invoice_id']) && $_GET['InvoiceLine']['invoice_id'] == $value['id']) {
						echo "<option value=" . $value['id'] . " selected>" . $value['invoice_number'] . "</option>";
					} else {
						echo "<option value=" . $value['id'] . ">" . $value['invoice_number'] . "</option>";
					}
				}
				?>
			</select>
			<?php /*echo $form->textField($model,'invoice_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm'));*/ ?>
		</div>
		<?php echo $form->error($model, 'invoice_id'); ?>

		<?php echo $form->labelEx($model,'product_classification_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'product_classification_id',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'product_classification_id'); ?>
	</div>

 	<div class="form-group row">
		<?php echo $form->labelEx($model, 'product_attributes_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<!-- <?php 
			echo $form->dropDownList($model, 'product_attributes_id',$product_attributes_array, array('onblur'=>'itemsNameModel(this.value)','class' => 'form-control form-control-sm','empty'=>'Select Attribute Label','id'=>'attribute_label'));
			/*echo $form->textField($model,'product_attributes_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm'));*/ ?> -->
			
			<select data-trigger="" name="InvoiceLine[product_attributes_id]" onchange="itemsNameModel(this.value)">
				echo "<option value='1000000' selected>Select Attribute Label</option>";

				<?php foreach($product_attributes_array as $item){
		        ?>
		        	<option value="<?php echo $item; ?>"><?php echo $item; ?></option>
		        <?php
		        }
		        ?>
			</select> 
		</div>
		<?php echo $form->error($model, 'product_attributes_id'); ?>
		
		<?php echo $form->labelEx($model, 'Attribute Value', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;" id="attribute_values">
			<!-- <select data-trigger="" name="InvoiceLine[product_attributes_id]" > -->
				
				<?php 
					$this->renderPartial('itemlist_bk');
			/*echo $form->textField($model,'product_attributes_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm'));*/ ?>
			<!-- </select> -->
		</div>
		<?php echo $form->error($model, 'product_attributes_id'); ?>
		<script type="text/javascript">
			
			function itemsNameModel(v){
				
				if (v == '1000000'){
					alert("Select Correct Attribute");
					$('#attribute_values').html(
						'<div> <select data-trigger="" class="form-control form-control-sm">						<option value="">Select Attribute Value</option>						</select></div>'

						// '<div> <select data-trigger="" class="form-control form-control-sm" onchange="itemvalues(is.value)" id="Wsitemdists_item_name" name="InvoiceLine[product_attributes_id]">						<option value="">Select Attribute Value</option>						</select></div>'
					);
				}
				else{
					$.ajax({
						type:'GET',
						url:'<?php echo Yii::app()->createUrl("InvoiceLine/getItemsDataModel");?>',
						data:{
							v:v
						},
						success: function (data) {
							$('#attribute_values').html(data);
							
						}
					});
				
				}
				const choices = new Choices('[data-trigger]',
							{
								searchEnabled: true,
								itemSelectText: '',

							});
			}
		</script>		
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'unit_price', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'unit_price',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'unit_price'); ?>

		<?php echo $form->labelEx($model,'unit', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<!-- <?php 
			$sql = "SELECT value FROM tbl_product_attributes where label='Unit'";
			$command = Yii::app()->db->createCommand($sql);
			$role_name = $command->queryAll();
			echo $form->dropDownList($model, 'unit', CHtml::listData($role_name, 'value', 'value'), array('class' => 'form-control form-control-sm','empty'=>'Select Unit'));

			/*echo $form->textField($model,'unit',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm'));*/ ?> -->

			<select data-trigger="" name="InvoiceLine[unit_id]">
				<?php
				echo "<option value='' selected>Select Unit</option>";
				// $all_queried_value = Supplier::model()->findAll();
				$sql = "SELECT value FROM tbl_product_attributes where label='Unit'";
				$command = Yii::app()->db->createCommand($sql);
				$all_queried_value = $command->queryAll();
				foreach ($all_queried_value as $key => $value) {
					if (isset($_GET['InvoiceLine']['unit_id']) && $_GET['InvoiceLine']['unit_id'] == $value['id']) {
						echo "<option value=" . $value['id'] . " selected>" . $value['value'] . "</option>";
					} else {
						echo "<option value=" . $value['id'] . ">" . $value['value'] . "</option>";
					}
				}
				?>
			</select>
			<?php  /*echo $form->textField($model,'supplier_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); */ ?>
		</div>
		<?php echo $form->error($model,'unit'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'total_price', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'total_price',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'total_price'); ?>

		<?php echo $form->error($model, 'supplier_id'); ?>

		<?php echo $form->labelEx($model,'quantity', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'supplier_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<select data-trigger="" name="InvoiceLine[supplier_id]">

				<?php
				echo "<option value='' selected>Select Supplier</option>";
				$all_queried_value = Supplier::model()->findAll();
				foreach ($all_queried_value as $key => $value) {
					if (isset($_GET['InvoiceLine']['supplier_id']) && $_GET['InvoiceLine']['supplier_id'] == $value['id']) {
						echo "<option value=" . $value['id'] . " selected>" . $value['supplier_name'] . "</option>";
					} else {
						echo "<option value=" . $value['id'] . ">" . $value['supplier_name'] . "</option>";
					}
				}
				?>
			</select>
			<?php  /*echo $form->textField($model,'supplier_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); */ ?>
		</div>
		<?php echo $form->error($model,'supplier_id'); ?>

		<?php echo $form->labelEx($model,'manufacture_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<select data-trigger="" name="InvoiceLine[manufacture_id]">

				<?php
				echo "<option value='' selected>Select Manufacturer</option>";
				$all_queried_value = Manufacturer::model()->findAll();
				foreach ($all_queried_value as $key => $value) {
					if (isset($_GET['InvoiceLine']['manufacture_id']) && $_GET['InvoiceLine']['manufacture_id'] == $value['id']) {
						echo "<option value=" . $value['id'] . " selected>" . $value['name'] . "</option>";
					} else {
						echo "<option value=" . $value['id'] . ">" . $value['name'] . "</option>";
					}
				}
				?>
			</select>
			<?php /*echo $form->textField($model,'manufacture_id',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm'));*/ ?>
		</div>
		<?php echo $form->error($model,'manufacture_id'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
	const choices = new Choices('[data-trigger]',
	{
		searchEnabled: true,
		itemSelectText: '',

	});

</script>