<style>
  /*<!-- Task by Rakib -->*/
  .button-column {
    /*margin: 0 0 0 60px !important;
      display: flex !important;*/
    margin: 0 ;
    display: contents !important;
  }
  .table > thead > tr > th {
        border-bottom: none !important;
    vertical-align: top !important;
    padding: 12px 5px !important;
    width: 86px;

  }
  .table>tbody>tr>td{
        /*padding: 3px 5px;*/
        padding: 12px 12px;
    border: 0;
  }
  .table > tbody > tr > td:last-of-type {
    justify-content: center;
}
/*<!-- Task by Rakib -->*/
</style>

<?php

$this->breadcrumbs=array(
	'Invoice Lines'=>array('index'),
	'Manage',
);
?>


<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
));*/ ?>
</div><!-- search-form -->

<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin">
    <h4 class="heading-custom page_header_h4">List of Invoice Lines</h4>

    <input style="width: 20% !important;float: right;margin-bottom: 10px;" class="btn-search button-pink" id="search-button" onclick="invoiceLineCreate()" name="" type="submit" value="Create Invoice Line">
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'invoice-line-grid',
	'itemsCssClass' => 'table cart table_um',
    'htmlOptions' => array('class' => 'table-responsive table-with-overflow bottommargin, admin-table-responsive border-round-bottom', 'width'=>'40'),

	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'invoice_id',
    // <!-- Task by Rakib -->
		// 'product_classification_id',
    array('name'=>'product_classification_id',
      'type'=>'raw',
      'value'=>'InvoiceLine::getProductClassification($data->product_classification_id)',
    ),

    // 'product_attributes_id',
    array('name'=>'product_attributes_id',
      'type'=>'raw',
      'value'=>'InvoiceLine::getProductAttributes($data->product_attributes_id)',
    ),
		
		'quantity',
		'unit_price',
		'unit',
		'total_price',

		// 'manufacture_id',
    array('name'=>'manufacture_id',
      'type'=>'raw',
      'value'=>'InvoiceLine::getProductManufacturer($data->manufacture_id)',
      
    ),

		// 'supplier_id',
    array('name'=>'supplier_id',
      'type'=>'raw',
      'value'=>'InvoiceLine::getProductSupplier($data->supplier_id)',

    ),
    // <!-- Task by Rakib -->
    array(
        'class'=>'CButtonColumn',
        'template'=>'{view} {update} {delete} ',
        'buttons'=> array(
          'view' => array(
                          'url' => 'CHtml::normalizeUrl(array("/invoiceLine/userView/id/". $data->id . "/type/1&ajax=true"));',
                          'click' => 'function(e) {
                                        $("#ajaxModal").remove();
                                        e.preventDefault();
                                        var $this = $(this)
                                          , $remote = $this.data("remote") || $this.attr("href")
                                          , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                        $("body").append($modal);
                                        $modal.modal({backdrop: "static", keyboard: false});
                                        $modal.load($remote);
                                      }',
                          'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                      ),
          'update' => array(
                          'url' => 'CHtml::normalizeUrl(array("/invoiceLine/userView/id/". $data->id . "/type/2&ajax=true"));',
                          'click' => 'function(e) {
                                        $("#ajaxModal").remove();
                                        e.preventDefault();
                                        var $this = $(this)
                                          , $remote = $this.data("remote") || $this.attr("href")
                                          , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                        $("body").append($modal);
                                        $modal.modal({backdrop: "static", keyboard: false});
                                        $modal.load($remote);
                                      }',
                          'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                      ),
          'delete' => array(
                          'url' => 'CHtml::normalizeUrl(array("/invoiceLine/userView/id/". $data->id . "/type/3&ajax=true"));',
                          'click' => 'function(e) {
                                        $("#ajaxModal").remove();
                                        e.preventDefault();
                                        var $this = $(this)
                                          , $remote = $this.data("remote") || $this.attr("href")
                                          , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                        $("body").append($modal);
                                        $modal.modal({backdrop: "static", keyboard: false});
                                        $modal.load($remote);
                                      }',
                          'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                      ),
          'htmlOptions' => array('style' => 'width: 100px;text-align: left;'),
        ),
      ),
	),
)); ?>

<script>
	function invoiceLineCreate(id) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/invoiceLine/create");?>',{id:id},function(data){
                bsModalOpen(data);
            });
        
    }
</script>