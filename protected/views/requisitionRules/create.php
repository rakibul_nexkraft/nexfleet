<?php
/* @var $this RequisitionRulesController */
/* @var $model RequisitionRules */

$this->breadcrumbs=array(
	'Requisition Rules'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List RequisitionRules', 'url'=>array('index')),
	array('label'=>'Manage RequisitionRules', 'url'=>array('admin')),
);
}
?>

<h1>Create RequisitionRules</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>