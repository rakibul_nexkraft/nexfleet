<?php
/* @var $this RequisitionRulesController */
/* @var $model RequisitionRules */

$this->breadcrumbs=array(
	'Requisition Rules'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List RequisitionRules', 'url'=>array('index')),
		//array('label'=>'Create RequisitionRules', 'url'=>array('create')),
		//array('label'=>'Update RequisitionRules', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete RequisitionRules', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage RequisitionRules', 'url'=>array('admin')),
	);
}
?>

<h1>View RequisitionRules #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'requisition_rules',
		'create_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
