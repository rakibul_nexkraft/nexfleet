<?php
/* @var $this RequisitionRulesController */
/* @var $model RequisitionRules */

$this->breadcrumbs=array(
	'Requisition Rules'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List RequisitionRules', 'url'=>array('index')),
		//array('label'=>'Create RequisitionRules', 'url'=>array('create')),
		//array('label'=>'View RequisitionRules', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage RequisitionRules', 'url'=>array('admin')),
	);
}
?>

<h1>Update RequisitionRules <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>