<?php
/* @var $this RequisitionRulesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Requisition Rules',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	//array('label'=>'Create RequisitionRules', 'url'=>array('create')),
	array('label'=>'Manage RequisitionRules', 'url'=>array('admin')),
);
}
?>

<h1>Requisition Rules</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
