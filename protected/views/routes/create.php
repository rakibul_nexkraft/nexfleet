<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Routes List', 'url'=>array('index')),
	array('label'=>'Manage Routes', 'url'=>array('admin')),
);
}
?>

<h4>New Routes</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>