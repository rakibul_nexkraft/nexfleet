<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Routes List', 'url'=>array('index')),
	array('label'=>'New Route', 'url'=>array('create')),
	array('label'=>'View Route', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Routes', 'url'=>array('admin')),
);
}
?>

<h4>Update Routes : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>