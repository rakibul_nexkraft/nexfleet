<?php
/* @var $this RoutesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Routes',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'New Route', 'url'=>array('create')),
	array('label'=>'Manage Routes', 'url'=>array('admin')),
);
}
?>

<h4>Routes</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>
    <div class="search-form">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
    </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'routes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		array(
			'name' => 'route_no',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->route_no), array("view", "id"=>$data->id))',
		),
		'route_detail',
		'actual_seat',
		array(
			'name'=>'seat_capacity',
			'type'=>'raw',
			'value'=>'$data->borderPass($data->route_no)',
		),
		array(
			'name'=>'available_seat',
			'type'=>'raw',
			'value'=>'$data->availableSeat($data->actual_seat, $data->borderPass($data->route_no), $data->route_no)',
		),
		
		array(
			'name'=>'on_mat_leave',
			'type'=>'raw',
			'value'=>'$data->onMatLeave($data->route_no)',
		),
		//'remarks',
	),
));

echo CHtml::link('Export to Excel',array('routes/excel'),array('style'=>'margin-right:50px;'));

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#routes-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	'title' => 'Transport Department - Routes List',
));
?>