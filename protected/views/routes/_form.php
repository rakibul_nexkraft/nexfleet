<?php
/* @var $this RoutesController */
/* @var $model Routes */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'routes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'route_no'); ?>
		<?php echo $form->textField($model,'route_no',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'route_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'route_detail'); ?>
		<?php echo $form->textField($model,'route_detail',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'route_detail'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'actual_seat'); ?>
		<?php echo $form->textField($model,'actual_seat'); ?>
		<?php echo $form->error($model,'actual_seat'); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'seat_capacity'); ?>
		<?php echo $form->textField($model,'seat_capacity'); ?>
		<?php echo $form->error($model,'seat_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'available_seat'); ?>
		<?php echo $form->textField($model,'available_seat'); ?>
		<?php echo $form->error($model,'available_seat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>
-->
	<div class="clearfix"></div>
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->