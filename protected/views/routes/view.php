<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
	'Routes'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'Routes List ', 'url'=>array('index')),
	array('label'=>'New Route', 'url'=>array('create')),
	array('label'=>'Update Route', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Route', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Routes', 'url'=>array('admin')),
);
}
?>

<h4>View Routes : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'route_no',
		'route_detail',
		'actual_seat',
		array(
			'name'=>'seat_capacity',
			'type'=>'raw',
			'value'=>$model->borderPass($model->route_no),
		),
		array(
			'name'=>'available_seat',
			'type'=>'raw',
			'value'=>$model->availableSeat($model->actual_seat, $model->borderPass($model->route_no), $model->route_no),
		),
		array(
			'name'=>'on_mat_leave',
			'type'=>'raw',
			'value'=>$model->onMatLeave($model->route_no),
		),
		//'remarks',
		'created_by',
		'created_time',
		'active',
	),
)); ?>
