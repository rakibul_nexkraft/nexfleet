<?php
/* @var $this AccidentsController */
/* @var $model Accidents */
/* @var $form CActiveForm */
?>

<div class="create-form form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'accidents-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
<div>
	
<div class="row">
		<?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php 	
		
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {
        
         $('#Accidents_driver_id').val(ui.item['driver_pin']);
         }"        
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));		?>
		
		
		<?php  echo $form->error($model,'vehicle_reg_no'); ?>
	</div> 

	<div class="row">
		<?php echo $form->labelEx($model,'place'); ?>
		<?php echo $form->textField($model,'place',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accident_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'accident_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->accident_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
				),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'accident_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accident_time'); ?>
		<?php //echo $form->textField($model,'accident_time',array('maxlength'=>127)); ?>
        <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
                'model'     => $model,
                'attribute' => 'accident_time',
                'language'=> 'en',//default Yii::app()->language
                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
                
                'options'   => array(
                    //'dateFormat' => 'dd.mm.yy',
                    //'timeFormat' => 'hh:mm tt',//'hh:mm tt' default
                    'ampm' => 'true',
                ),
            ));
        ?>
    	<?php echo $form->error($model,'accident_time'); ?>
	</div>
	


	<div class="row">
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textField($model,'reason'); ?>
		<?php echo $form->error($model,'reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'demagedetail'); ?>
		<?php echo $form->textField($model,'demagedetail'); ?>
		<?php echo $form->error($model,'demagedetail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_id'); ?>
		<?php echo $form->textField($model,'driver_id'); ?>
		<?php echo $form->error($model,'driver_id'); ?>
	</div>
    
    <div class="row">
		<?php echo $form->labelEx($model,'action_taken'); ?>
		<?php echo $form->textField($model,'action_taken'); ?>
		<?php echo $form->error($model,'action_taken'); ?>
	</div>
    <div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php echo $form->textField($model,'amount'); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>

	<!--div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div-->
	
</div>

	<!--div class="row buttons" style="width:100%; float:right;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->