<?php
/* @var $this AccidentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Accidents',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Accident', 'url'=>array('create')),
		array('label'=>'Manage Accidents', 'url'=>array('admin')),
	);
}
?>

<h4>Accidents</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); 
?>
Total Amount:<?php echo $model->grandTotal($_GET['Accidents']);?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'accidents-grid',
	'dataProvider'=>$dataProvider,
	//'filter'=>$model,
	'columns'=>array(
		array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		array(
            'name' => 'vehicle_reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg_no),array("view","id"=>$data->id))',
        ),
		//'vehicle_reg_no',
		'place',
		'accident_date',
		'accident_time',
		'reason',
		'demagedetail',
		'driver_id',
		array(
	        'name' => 'driver_name',
	        'type'=>'raw',
	        'value' => '$data->driverName($data->driver_id)'
        ),
		'amount',
		
		/*
		'created_time',
		'created_by',
		'active',
		*/
	),
)); ?>
<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Accidents'])));
?> &nbsp; &nbsp; &nbsp;