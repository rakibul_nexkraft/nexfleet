<?php
/* @var $this AccidentsController */
/* @var $model Accidents */

$this->breadcrumbs=array(
	'Accidents'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>' Accidents List', 'url'=>array('index')),
		array('label'=>'Manage Accidents', 'url'=>array('admin')),
	);
}
?>

<h4>New Accident</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>