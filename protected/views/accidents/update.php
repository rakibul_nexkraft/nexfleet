<?php
/* @var $this AccidentsController */
/* @var $model Accidents */

$this->breadcrumbs=array(
	'Accidents'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>' Accidents List', 'url'=>array('index')),
		array('label'=>'New Accident', 'url'=>array('create')),
		array('label'=>'View Accident', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Accidents', 'url'=>array('admin')),
	);
}
?>

<h4>Update Accident : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>