<?php
/* @var $this AccidentsController */
/* @var $model Accidents */

$this->breadcrumbs=array(
	'Accidents'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>' Accidents List', 'url'=>array('index')),
		array('label'=>'New Accident', 'url'=>array('create')),
		array('label'=>'Update Accident', 'url'=>array('update', 'id'=>$model->id)),
	//	array('label'=>'Delete Accident', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Accidents', 'url'=>array('admin')),
	);
}
?>

<h4>View Accident : <?php echo $model->vehicle_reg_no; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		'place',
		'accident_date',
		'accident_time',
		'reason',
		'demagedetail',
		'driver_id',
		'created_time',
		'created_by',
		'action_taken',
		// 'active',
	),
)); ?>
