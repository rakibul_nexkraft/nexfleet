<?php
/* @var $this AccidentsController */
/* @var $model Accidents */
/* @var $form CActiveForm */
?>

<div class="wide form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'vehicle_reg_no'); ?><br />
			<?php echo $form->textField($model,'vehicle_reg_no',array('maxlength'=>127,'size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'place'); ?><br />
			<?php echo $form->textField($model,'place',array('maxlength'=>127,'size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'accident_date'); ?><br />
			<?php echo $form->textField($model,'accident_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'driver_id'); ?><br />
            <?php echo $form->textField($model,'driver_id',array('size'=>18,'class'=>'input-medium')); ?>
        </div>
    </div>

    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'from_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'from_date',  // name of post parameter
                    //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'to_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'to_date',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        //	'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->to_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </fieldset>



    <!--<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'accident_time'); ?><br />
			<?php echo $form->textField($model,'accident_time',array('maxlength'=>127,'size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'reason'); ?><br />
			<?php echo $form->textField($model,'reason',array('maxlength'=>127)); ?>
		</div>
	</div>
	<div class="row">
		<?php echo $form->label($model,'demagedetail'); ?>
		<?php echo $form->textField($model,'demagedetail',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_id'); ?>
		<?php echo $form->textField($model,'driver_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>-->

	<div class="clearfix"></div>
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->