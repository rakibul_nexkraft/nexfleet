<?php
/* @var $this WsrequisitionsController */
/* @var $model Wsrequisitions */

$this->breadcrumbs=array(
	'Wsrequisitions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Wsrequisitions', 'url'=>array('index')),
	array('label'=>'New Wsrequisition', 'url'=>array('create')),
	array('label'=>'View Wsrequisition', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Wsrequisitions', 'url'=>array('admin')),
);
?>

<h4>Update Wsrequisition <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>