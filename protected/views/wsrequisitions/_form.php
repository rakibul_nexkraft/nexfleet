<?php
/* @var $this WsrequisitionsController */
/* @var $model Wsrequisitions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsrequisitions-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<!--	<div class="row">
		<?php //echo $form->labelEx($model,'id'); ?>
		<?php //echo $form->textField($model,'id'); ?>
		<?php //echo $form->error($model,'id'); ?>
	</div> -->

    <div class="row">
        <?php echo $form->labelEx($model,'requisition_no'); ?>
        <?php echo $form->textField($model,'requisition_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'requisition_no'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'requisition_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'requisition_date',

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->requisition_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
        <?php echo $form->error($model,'requisition_date'); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php 	
		
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {        
         $('#Wsrequisitions_vmodel').val(ui.item['vmodel']);
         $('#Wsrequisitions_engine_no').val(ui.item['engine_no']);        
         $('#Wsrequisitions_vehicletype_id').val(ui.item['vehicletype_id']);        
         }"        
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));		?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

<div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

		<?php // echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'vehicletype_id'); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'vmodel'); ?>
		<?php echo $form->textField($model,'vmodel',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'vmodel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'engine_no'); ?>
		<?php echo $form->textField($model,'engine_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'engine_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vin'); ?>
		<?php echo $form->textField($model,'vin',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'vin'); ?>
	</div>
	-->
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'create_time'); ?>
		<?php echo $form->textField($model,'create_time'); ?>
		<?php echo $form->error($model,'create_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
-->
    <div align="left">
            <?php $this->widget('bootstrap.widgets.TbButton',array(
                'label' => 'Save',
                'type' => 'primary',
                'buttonType'=>'submit',
                'size' => 'medium'
            ));
            ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->