<?php
/* @var $this WsrequisitionsController */
/* @var $model Wsrequisitions */

$this->breadcrumbs=array(
	'Wsrequisitions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Wsrequisitions', 'url'=>array('index')),
	array('label'=>'Manage Wsrequisitions', 'url'=>array('admin')),
);
?>

<h4>New Wsrequisition</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>