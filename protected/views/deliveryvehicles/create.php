<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DeliveryvehiclesController */
/* @var $model Deliveryvehicles */

$this->breadcrumbs=array(
	'Deliveryvehicles'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Deliveryvehicles', 'url'=>array('index'),'visible'=> in_array('List Deliveryvehicles', $user_rights)),
		array('label'=>'Manage Deliveryvehicles', 'url'=>array('admin'),'visible'=> in_array('Manage Deliveryvehicles', $user_rights)),
	);
}
?>

<h4>New Delivery Vehicles</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>