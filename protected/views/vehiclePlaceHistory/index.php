<?php
/* @var $this VehiclePlaceHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vehicle Place Histories',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create VehiclePlaceHistory', 'url'=>array('create')),
		array('label'=>'Manage VehiclePlaceHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Vehicle Place Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
