<?php
/* @var $this VehiclePlaceHistoryController */
/* @var $model VehiclePlaceHistory */

$this->breadcrumbs=array(
	'Vehicle Place Histories'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List VehiclePlaceHistory', 'url'=>array('index')),
		array('label'=>'Create VehiclePlaceHistory', 'url'=>array('create')),
		array('label'=>'Update VehiclePlaceHistory', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete VehiclePlaceHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage VehiclePlaceHistory', 'url'=>array('admin')),
	);
}
?>

<h1>View VehiclePlaceHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'log_id',
		'trip_id',
		'place',
		'meter',
		'created_by',
		'created_time_mobile',
		'created_time_vts',
		'updated_by',
		'update_time',
	),
)); ?>
