<?php
/* @var $this VehiclePlaceHistoryController */
/* @var $model VehiclePlaceHistory */

$this->breadcrumbs=array(
	'Vehicle Place Histories'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List VehiclePlaceHistory', 'url'=>array('index')),
		array('label'=>'Create VehiclePlaceHistory', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('vehicle-place-history-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Vehicle Place Histories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vehicle-place-history-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'log_id',
		'trip_id',
		'place',
		'meter',
		'created_by',
		/*
		'created_time_mobile',
		'created_time_vts',
		'updated_by',
		'update_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
