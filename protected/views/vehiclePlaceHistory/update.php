<?php
/* @var $this VehiclePlaceHistoryController */
/* @var $model VehiclePlaceHistory */

$this->breadcrumbs=array(
	'Vehicle Place Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List VehiclePlaceHistory', 'url'=>array('index')),
		array('label'=>'Create VehiclePlaceHistory', 'url'=>array('create')),
		array('label'=>'View VehiclePlaceHistory', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage VehiclePlaceHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Update VehiclePlaceHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>