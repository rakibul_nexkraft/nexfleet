<?php
/* @var $this VehiclePlaceHistoryController */
/* @var $model VehiclePlaceHistory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vehicle-place-history-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'log_id'); ?>
		<?php echo $form->textField($model,'log_id',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'log_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trip_id'); ?>
		<?php echo $form->textField($model,'trip_id',array('size'=>60,'maxlength'=>225)); ?>
		<?php echo $form->error($model,'trip_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'place'); ?>
		<?php echo $form->textField($model,'place',array('size'=>60,'maxlength'=>525)); ?>
		<?php echo $form->error($model,'place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meter'); ?>
		<?php echo $form->textField($model,'meter'); ?>
		<?php echo $form->error($model,'meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time_mobile'); ?>
		<?php echo $form->textField($model,'created_time_mobile'); ?>
		<?php echo $form->error($model,'created_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time_vts'); ?>
		<?php echo $form->textField($model,'created_time_vts'); ?>
		<?php echo $form->error($model,'created_time_vts'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
		<?php echo $form->error($model,'update_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->