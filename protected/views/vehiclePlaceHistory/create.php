<?php
/* @var $this VehiclePlaceHistoryController */
/* @var $model VehiclePlaceHistory */

$this->breadcrumbs=array(
	'Vehicle Place Histories'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List VehiclePlaceHistory', 'url'=>array('index')),
		array('label'=>'Manage VehiclePlaceHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Create VehiclePlaceHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>