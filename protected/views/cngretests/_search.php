<?php
/* @var $this CngretestsController */
/* @var $model Cngretests */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'id'); ?><br />
                    <?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'vehicle_reg_no'); ?><br />
                    <?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'vehicle_type'); ?><br />
                    <?php
						$vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
						echo $form->dropDownList($model,'vehicle_type', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => '', 'style' => 'width:164px'));
					?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'conversion_date'); ?><br />
					<?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
				'attribute'=>'conversion_date',  // name of post parameter
				'options'=>array(
                                    'dateFormat'=>'yy-mm-dd',
                                    'changeYear'=>true,
                                    'changeMonth'=>true,
				),
				'htmlOptions'=>array(
                                    'style'=>'height:17px; width:138px;',
				),
                            ));
					?>
            </div>
    </div>
    <!--<div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'duedate_retest'); ?><br />
                    <?php echo $form->textField($model,'duedate_retest',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>-->
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'actual_date_retest'); ?><br />
                    <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
				'attribute'=>'actual_date_retest',  // name of post parameter
				'options'=>array(
                                    'dateFormat'=>'yy-mm-dd',
                                    'changeYear'=>true,
                                    'changeMonth'=>true,
				),
				'htmlOptions'=>array(
                                    'style'=>'height:17px; width:138px;',
				),
                            ));
					?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'next_duedate_retest'); ?><br />
                    <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
				'attribute'=>'next_duedate_retest',  // name of post parameter
				'options'=>array(
                                    'dateFormat'=>'yy-mm-dd',
                                    'changeYear'=>true,
                                    'changeMonth'=>true,
				),
				'htmlOptions'=>array(
                                    'style'=>'height:17px; width:138px;',
				),
                            ));
					?>
            </div>
    </div>
    <!--<div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'created_by'); ?><br />
                    <?php echo $form->textField($model,'created_by',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'created_time'); ?><br />
                    <?php echo $form->textField($model,'created_time',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'active'); ?><br />
                    <?php echo $form->textField($model,'active',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'updated_by'); ?><br />
                    <?php echo $form->textField($model,'updated_by',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>
    <div class="fl">
            <div class="row">
                    <?php echo $form->label($model,'updated_time'); ?><br />
                    <?php echo $form->textField($model,'updated_time',array('size'=>18,'class'=>'input-medium')); ?>
            </div>
    </div>-->
	
	<div class="clearfix"></div>
	
    <fieldset style="float:left; padding:0 0 0 10px; margin-right:10px;">
            <div class="fl">
                <div class="row">
                        <?php echo $form->label($model,'from_date'); ?><br />
                        <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				'options'=>array(
                                    'dateFormat'=>'yy-mm-dd',
                                    'changeYear'=>true,
                                    'changeMonth'=>true,
				),
				'htmlOptions'=>array(
                                    'style'=>'height:17px; width:138px;',
				),
                            ));
                        ?>
                </div>
            </div>
            <div class="fl">
                <div class="row">
                        <?php echo $form->label($model,'to_date'); ?><br />
                        <?php
                            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                'model'=>$model,
				'attribute'=>'to_date',  // name of post parameter
				'options'=>array(
                                    'dateFormat'=>'yy-mm-dd',
                                    'changeYear'=>true,
                                    'changeMonth'=>true,
				),
				'htmlOptions'=>array(
                                    'style'=>'height:17px; width:138px;',
				),
                            ));
                        ?>
                </div>
            </div>
    </fieldset>

    <div class="clearfix"></div>

	<!--div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div-->
    <div align="left">
        <?php 
		if(!(Yii::app()->request->isAjaxRequest))
		{
			$this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Search',
				'type' => 'primary',
				'buttonType'=>'submit', 
				'size' => 'medium'
			));
		}
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->