<?php
/* @var $this CngretestsController */
/* @var $model Cngretests */

$this->breadcrumbs=array(
	'CNG Re-Tests'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List CNG Retests', 'url'=>array('index')),
		array('label'=>'Manage CNG Retests', 'url'=>array('admin')),
	);
}
?>

<h4>Create CNG Re-Test</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>