<?php
/* @var $this CngretestsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'CNG Re-Tests',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New CNG Retest', 'url'=>array('create')),
		array('label'=>'Manage CNG Retests', 'url'=>array('admin')),
	);
}
?>

<h4>CNG Re-Tests</h4>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cngretests-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		array(
                    'name'=>'id',
                    'type'=>'raw',
                    'value'=>'CHtml::link(CHtml::encode($data->id), array("view","id"=>$data->id))',
                ),
		'vehicle_reg_no',
		array(
                    'name'=>'vehicle_type',
                    'type'=>'raw',
                    'value'=>'$data->vehicletypes->type',
                ),
		'conversion_date',
		'duedate_retest',
		'actual_date_retest',
		'next_duedate_retest',
		'remarks',
		/*
		'created_by',
		'created_time',
		'active',
		'updated_by',
		'updated_time',
		*/
	),
)); ?>

<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Cngretests'])));
?> &nbsp; &nbsp; &nbsp;