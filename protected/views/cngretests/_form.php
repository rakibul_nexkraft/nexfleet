<?php
/* @var $this CngretestsController */
/* @var $model Cngretests */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cngretests-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'model'=>$model,
                    'attribute' => 'vehicle_reg_no',   
                    'source'=>$this->createUrl('vehicles/getRegNo'),
                    // additional javascript options for the autocomplete plugin
                    'options'=>array(
                        'minLength'=>'2',
                        'select'=>"js: function(event, ui) {
                            $('#Cngretests_vehicle_type').val(ui.item['vehicletype_id']);
                        }"        

                    ),
                ));
                ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_type'); ?>
                <?php
                    $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
                    echo $form->dropDownList($model,'vehicle_type', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicle Type...'));
                ?>
		<?php echo $form->error($model,'vehicle_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'conversion_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,'attribute'=>'conversion_date',     

                        // additional javascript options for the date picker plugin
                        'options'=>array('autoSize'=>true,
                                'dateFormat'=>'yy-mm-dd',
                                'defaultDate'=>$model->conversion_date,
                                'changeYear'=>true,
                                'changeMonth'=>true,
                        ),

                        //'htmlOptions'=>array('style'=>'width:150px;'),
                )); ?>
		<?php echo $form->error($model,'conversion_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duedate_retest'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,'attribute'=>'duedate_retest',     

                        // additional javascript options for the date picker plugin
                        'options'=>array('autoSize'=>true,
                                'dateFormat'=>'yy-mm-dd',
                                'defaultDate'=>$model->duedate_retest,
                                'changeYear'=>true,
                                'changeMonth'=>true,
                        ),

                        //'htmlOptions'=>array('style'=>'width:150px;'),
                )); ?>
		<?php echo $form->error($model,'duedate_retest'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'actual_date_retest'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,'attribute'=>'actual_date_retest',     

                        // additional javascript options for the date picker plugin
                        'options'=>array('autoSize'=>true,
                                'dateFormat'=>'yy-mm-dd',
                                'defaultDate'=>$model->actual_date_retest,
                                'changeYear'=>true,
                                'changeMonth'=>true,
                        ),

                        //'htmlOptions'=>array('style'=>'width:150px;'),
                )); ?>
		<?php echo $form->error($model,'actual_date_retest'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'next_duedate_retest'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,'attribute'=>'next_duedate_retest',     

                        // additional javascript options for the date picker plugin
                        'options'=>array('autoSize'=>true,
                                'dateFormat'=>'yy-mm-dd',
                                'defaultDate'=>$model->next_duedate_retest,
                                'changeYear'=>true,
                                'changeMonth'=>true,
                        ),

                        //'htmlOptions'=>array('style'=>'width:150px;'),
                )); ?>
		<?php echo $form->error($model,'next_duedate_retest'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks'); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>-->
        
        <div align="left">
        <?php 
		if(!(Yii::app()->request->isAjaxRequest))
		{
			$this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Save',
				'type' => 'primary',
				'buttonType'=>'submit', 
				'size' => 'medium'
			));
		}
        ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->