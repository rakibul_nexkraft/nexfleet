<?php
/* @var $this CngretestsController */
/* @var $data Cngretests */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('conversion_date')); ?>:</b>
	<?php echo CHtml::encode($data->conversion_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('duedate_retest')); ?>:</b>
	<?php echo CHtml::encode($data->duedate_retest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actual_date_retest')); ?>:</b>
	<?php echo CHtml::encode($data->actual_date_retest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('next_duedate_retest')); ?>:</b>
	<?php echo CHtml::encode($data->next_duedate_retest); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	*/ ?>

</div>