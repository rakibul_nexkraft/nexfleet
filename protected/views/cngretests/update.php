<?php
/* @var $this CngretestsController */
/* @var $model Cngretests */

$this->breadcrumbs=array(
	'CNG Re-Tests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List CNG Retests', 'url'=>array('index')),
		array('label'=>'New CNG Retest', 'url'=>array('create')),
		array('label'=>'View CNG Retest', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage CNG Retests', 'url'=>array('admin')),
	);
}
?>

<h4>Update CNG Re-Test : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>