<?php
/* @var $this CngretestsController */
/* @var $model Cngretests */

$this->breadcrumbs=array(
	'CNG Re-Tests'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List CNG Retests', 'url'=>array('index')),
	array('label'=>'New CNG Retest', 'url'=>array('create')),
	array('label'=>'Update CNG Retest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CNG Retest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CNG Retests', 'url'=>array('admin')),
);
}
?>

<h4>View CNG Re-Test : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		'vehicle_type',
		'conversion_date',
		'duedate_retest',
		'actual_date_retest',
		'next_duedate_retest',
		'remarks',
		'created_by',
		'created_time',
		//'active',
		'updated_by',
		'updated_time',
	),
)); ?>
