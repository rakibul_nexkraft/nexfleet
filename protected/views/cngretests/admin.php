<?php
/* @var $this CngretestsController */
/* @var $model Cngretests */

$this->breadcrumbs=array(
	'CNG Re-Tests'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List CNG Re-Tests', 'url'=>array('index')),
		array('label'=>'New CNG Re-Test', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cngretests-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage CNG Re-Tests</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 
	$vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
	
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cngretests-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_reg_no',
		array(
			'name'=>'vehicle_type',
			'type'=>'raw',
			'value'=>'$data->vehicletypes->type',
			'filter'=>CHtml::dropDownList('Cngretests[vehicle_type]','',CHtml::listData($vehicletype_id,'id','type'),array('empty'=>'','style'=>'width:164px; height:20px;')),
		),
		'conversion_date',
		'duedate_retest',
		'actual_date_retest',
		'next_duedate_retest',
		'remarks',
		/*
		'created_by',
		'created_time',
		'active',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
