<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>
    AOS.init();
</script>


<style type="text/css">
    #all_route {
        display: none;
    }

    .cell-hover:hover {
        background: rgba(53, 217, 125, 0.1);
    }

    .history-box {
        position: relative;
        border: 1px solid rgba(0, 0, 0, 0.075);
        border-radius: 3px;
        text-align: center;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        background-color: #F5F5F5;
    }

    .history-extended {
        background-color: #FFF;
        text-align: left;
    }

    .history-header {
        /* border-bottom: 1px solid #dedede; */
        padding: 10px 20px;
        /*background-color: #2a57ff;*/
        /*background-image: linear-gradient(45deg, #fc3ba2, blue);*/
        /*box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);*/
        /* width: 95%; */
        font-family: Poppins, Helvetica, sans-serif;
        font-size: 1.2em;
        font-weight: 700;
        border-radius: 5px;
        margin: 0 auto;
        position: relative;
        top: 54px;
        color: white;
        z-index: 1;
    }

    .history-header>h3 {
        color: white;
    }

    .tbl-header {
        /*box-shadow: 0 3px 2px -2px rgba(0,0,0,.3); */
        /* border-radius: 5px; */
        /* border: 1px solid #dedede; */
        background-color: #f2f2f2;
    }

    .heading-block:after {
        display: none;
    }
</style>

<section class="responsive-section">
    <div class="col_half admin-dashboard-chart-responsive-label">
        <h3 class="center_style" style="display: flex;color: #444;margin-bottom: 0;">
            <span style="flex: 1;color: #444 !important">Number of Seats Sold</span>
            <?php echo CHtml::dropDownList('chartView', '', array('1' => 'Weekly', '2' => 'Monthly', '3' => 'Quarterly', '4' => 'Biyearly', '5' => 'Yearly'), array('onChange' => 'chartView(this.value)', 'class' => 'form-control', 'style' => 'flex:1')); ?>
        </h3>
        <div class="page_header_div" style="margin-top: 20px;text-align: center;padding: 30px;border-radius: 5px">
            <canvas id="barChartCanvas" class="admin-dashboard-chart-responsive" style="width:100%;height: 300px"></canvas>
        </div>
    </div>
    <div class="col_half col_last admin-dashboard-marketplace">
        <h3 class="center_style center_style_responsive">Earning From Market Place</h3>

        <div class="col-sm-12 col-md-11" data-aos="slide-down" data-aos-duration="1000">
            <div class="thumbnail thumbnail-box thumbnail-box-background--blue" style="box-shadow: 0 0 3px 1px #796df1 !important;">
                <div class="thumbnail-box-container">
                    <div class="thumbnail-box--earning thumbnail-box--earning-background--blue">10</div>
                    <div class="thumbnail-box--title">iFleet Earnings</div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-11" data-aos="slide-down" data-aos-duration="1000">
            <div class="thumbnail thumbnail-box thumbnail-box-background--red" style="box-shadow: 0 0 3px 1px #fd397a !important;">
                <div class="thumbnail-box-container">
                    <div class="thumbnail-box--earning thumbnail-box--earning-background--red">10</div>
                    <div class="thumbnail-box--title"">User Earnings</div>
                </div>
            </div>
        </div>
        
        
    </div>    
    <div class=" clear"></div>
</section>
<section id="section-about" class="page-section section nobg nomargin nopadding topmargin admin-dashboard-table" data-aos="fade-in" data-aos-duration="1000">
    <div class="col_full">
        <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
            <h4 class="heading-custom page_header_h4">Request List</h4>
        </div>
        <div class="col_full">

            <div class="clear"></div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(".pick_drop_summery").click(function() {
        $("#all_route").toggle("slow");

    });
    $(".route_list").click(function() {
        $("#all_route").slideUp("slow");

    });

    function routeSummery(route) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/seatRequestAdmin/routeSummery"); ?>', {
            route: route
        }, function(data) {
            //openModel('<h3>'+data+'</h3>');
            bsModalOpen('<h3>' + data + '</h3>');

        });

    }
    var label1, data1;
    var Chart1 = function(v) {
        if (v == 1) {
            label1 = ["1st Day", "2nd Day", "3rd Day", "4th Day", "5th Day", "6th Day", "7th Day"];
            data1 = [65, 59, 90, 81, 56, 55, 50];
        }
        if (v == 2) {
            label1 = ["1st Week", "2nd Week", "3rd Week", "4th Week", "5th Week"];
            data1 = [65, 59, 90, 81, 56];
        }
        if (v == 3) {
            label1 = ["1st Quarter", "2nd Quarter", "3rd Quarter", "4th Quarter"];
            data1 = [65, 59, 90, 81];
        }
        if (v == 4) {
            label1 = ["January", "February", "March", "April", "May", "June"];
            data1 = [65, 59, 90, 81, 56, 90];
        }
        if (v == 5) {
            label1 = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            data1 = [65, 59, 90, 81, 56, 90, 65, 59, 90, 81, 56, 90];
        }
        var barChartData = {
            labels: label1,
            datasets: [{
                fillColor: "rgba(220,220,220,0.5)",
                strokeColor: "rgba(220,220,220,1)",
                data: data1
            }]

        };

        var globalGraphSettings = {
            animation: Modernizr.canvas
        };

        function showBarChart() {
            var ctx = '';
            var ctx = document.getElementById("barChartCanvas").getContext("2d");
            new Chart(ctx).Bar(barChartData, globalGraphSettings);

        }

        showBarChart();
    };
    Chart1(1);

    function chartView(v) {
        $('#barChartCanvas').replaceWith('<canvas id="barChartCanvas" width="400" height="400"></canvas>');
        Chart1(v);
    }
</script>


<script>
    function status(id, value, preValue) {

        var message = "Do you want to change seat request status for seat request ID# " + id + "?";

        var con = confirm(message);
        if (con === true) {
            showDataProcess(true);
            jQuery.post('<?php echo $this->createUrl('/seatRequestAdmin/statusChange'); ?>', {
                id: id,
                value: value,
                preValue: preValue
            }, function(data) {
                $('#myModal').css('display', 'none');
                //var size_to_value=JSON.parse(data);
                //alert(data);
                //openModel(data);
                showDataProcess(false);
                bsModalOpen(data);
            });

        } else {
            //$(this).val()=preValue;
            //alert(preValue);
            ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }


    }
</script>