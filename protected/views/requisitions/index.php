<script type="text/javascript">
    $('#document').ready(function(){
        $('.openDlg').live('click', function(){
            var dialogId = $(this).attr('class').replace('openDlg ', "");
            $.ajax({
                'type': 'POST',
                'url' : $(this).attr('href'),
                success: function (data) {
                    $('#'+dialogId+' div.divForForm').html(data);
                    $( '#'+dialogId ).dialog( 'open' );
                },
                dataType: 'html'
            });
            return false; // prevent normal submit
        })
    });

    function getRequisitionDetail(id){
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/getRequisitionDetail");?>',
            data:{id:id},
            dataType:'json',
            success: function(data){
                var html = "";
                if (data){
                    $('#myModalLabel').html("<h4 style='border-bottom: 0px'>Requisition No - "+data.id+"</h4>");
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'>" + data.vehicle_reg_no + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'>" + data.driver_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'>" + data.driver_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Phone : </th><td style='width: 60%'>" + data.driver_phone + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Pin : </th><td style='width: 60%'>" + data.user_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Name : </th><td style='width: 60%'>" + data.user_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Dept Name : </th><td style='width: 60%'>" + data.dept_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Cell : </th><td style='width: 60%'>" + data.user_cell + "</td></tr>" +
                        "<tr><th style='width: 40%'>Start Point : </th><td style='width: 60%'>" + data.start_point + "</td></tr>" +
                        "<tr><th style='width: 40%'>End Point : </th><td style='width: 60%'>" + data.end_point + "</td></tr>" +
                        "<tr><th style='width: 40%'>Time : </th><td style='width: 60%'>From <strong>" + data.start_date +" "+ data.start_time + "</strong> To <strong>" + data.end_date+" "+data.end_time + "</strong></td></tr>" +
                        "</table>";
                    $('#requisitionData').html(html);
                } else {
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><td>No data found</td></tr>" +
                        "</table>";
                    $('#requisitionData').html(html);
                }
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyiFpj3wAKztW2X7NUkBSW0CW_lXvG45E&amp;libraries=places"></script>
<script>
    var map;
    var marker;
    function initialize(latitude,longitude) {
        $("#myModal").modal('show');
        var lat = parseFloat(latitude);
        var long = parseFloat(longitude);
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            center: {lat: lat, lng: long},
            zoom: 17
        });
        marker = new google.maps.Marker({
            draggable: true,
            position: new google.maps.LatLng(lat, long),
            map: map,
            animation: google.maps.Animation.DROP,
            title: "Vehicle position"
        });
    }

   function openMap(){
       var vehicle_reg_no = document.getElementsByClassName("select2-chosen")[0].innerText;
       $.ajax({
           type:'GET',
           url:'<?php echo Yii::app()->createUrl("requisitions/map");?>',
           data:{vehicle_reg_no: vehicle_reg_no},
           success: function(coordinates){
               if(coordinates == ','){
                   alert('No asset Id fond for this vehicle');
               } else {
                   var datas = coordinates.split(',');
                   // alert("Sucess!! "+datas);
                   initialize(datas[0],datas[1]);
               }
           }
       });
   }
</script>

<style>
.fontColorRed,.fontColorRed td a{color:red;}
</style>
<?php
/* @var $this RequisitionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Requisitions',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'New Requisition', 'url'=>array('create')),
	array('label'=>'Manage Requisitions', 'url'=>array('admin')),
    array('label'=>'Manage Requisition Rules', 'url'=>array('requisitionRules/admin')),
    array('label'=>'Requisition List', 'url'=>array('requisitionList')),
);
}
?>

<h4>Requisitions</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
    <!--<button style="margin:7px 15px 17px 0;" type="button" class="btn btn-success btn-lg" onclick="openMap()">Show On Map</button>
</div>-->

<!--    map starts -->
<style>
    #map_canvas {
        height: 400px;
    }
</style>
<div id="myModal" class="modal fade hide" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vehicle Location</h4>
            </div>
            <div class="modal-body" id="map_canvas"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--    map ends -->

<div id="req_container">
<?php
$var = $_REQUEST['Requisitions'];
if($var):
 ?>
<h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
<?php if($var['user_pin']) echo "User: ".$var['user_pin']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if($var['vehicle_reg_no']) echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php
	if(($var['active'] == '')) $active= "...";
	else if($var['active']==0) $active= "Pending";
	else if($var['active']==1) $active= "Not Approved";
	else if($var['active']==2) $active= "Approved";
	echo "Action: ". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php endif;?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'requisitions-grid',
    'dataProvider'=>$dataProvider,
    'rowCssClassExpression'=>'$data->color',
    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
            ),
       /* array(
            'name' => 'bpmt_ref_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->bpmt_ref_no),array("view","id"=>$data->id))',
        ),*/
        'vehicle_reg_no',
        		array(
			'name' => 'Vehicle Type',
			'type' => 'raw',
			'value' => '$data->vehicletypes->type',
		),
        'driver_pin',
        'driver_name',
        'user_pin',
        'user_name',
        'user_level',
		'dept_name',
		'start_date',
        'end_date',
		'end_point',
		//'billing_code',
        'start_time',
		array(
			'name' => 'dutytype_id',
			'type' => 'raw',
			'value' => '$data->dutytypes->type_name',
		),
		array(
			'name' => 'active',
			'type' => 'raw',
			'value' => 'Requisitions::model()->statusActive($data->active)',
		),
        'cancel_status_by_user',
		 array(
            'name' => 'User SMS',
            'type' => 'raw',
            'value' => 'Requisitions::model()->statusSms($data->user_cell,$data->updated_time,$data->id)',
        ),

        array(
            'name' => 'Driver SMS',
            'type' => 'raw',
            'value' => 'Requisitions::model()->statusSmsDriver($data->driver_phone,$data->updated_time,$data->id)',
        ),
        array(
            'name' => 'Copy',
            'type' => 'raw',
            //'value' => 'CHtml::textField("","1",array("id"=>"copyReq","width"=>100))',
            'value' => 'CHtml::link("Copy",array("copyCreate","id"=>$data->id))',
            'htmlOptions' => array(),
        )
		
	),
));
?>
</div>
<?php
	echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Requisitions'])));
?> &nbsp; &nbsp; &nbsp;
<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#req_container', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Requisition List',
));
?>
