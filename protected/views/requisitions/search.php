<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'enableAjaxValidation'=>true,
        'method'=>'get',
    )); ?>

    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'re_create_date_start'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'re_create_date_start',  // name of post parameter
                    //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                        'autocomplete'=>'off',
                    ),
                ));
                ?>
            </div>
        </div>
        <div class="fl">
            <div class="row">
                <?php echo $form->labelEx($model,'re_create_time_start'); ?><br />
                <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
                    'model'     => $model,
                    'attribute' => 're_create_time_start',
                    'language'=> 'en',//default Yii::app()->language
                    'mode'    => 'time',//'datetime' or 'time' ('datetime' default)

                    'options'   => array(
                        //'dateFormat' => 'dd.mm.yy',
                        'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                        //'ampm' => 'true',
                    ),
                    //'htmlOptions'=>array('readonly'=>'readonly'),
                    'htmlOptions'=>array('autocomplete'=>'off',),
                ));
                ?>
                <?php echo $form->error($model,'re_create_time_end'); ?>
            </div>
        </div>
    </fieldset>
    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'re_create_date_end'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'re_create_date_end',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        //	'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->to_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                        'autocomplete'=>'off',
                    ),
                ));
                ?>
            </div>
        </div>
        <div class="fl">
            <div class="row">
                <?php echo $form->labelEx($model,'re_create_time_end'); ?><br />
                <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
                    'model'     => $model,
                    'attribute' => 're_create_time_end',
                    'language'=> 'en',//default Yii::app()->language
                    'mode'    => 'time',//'datetime' or 'time' ('datetime' default)

                    'options'   => array(
                        //'dateFormat' => 'dd.mm.yy',
                        'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                        //'ampm' => 'true',
                    ),
                    //'htmlOptions'=>array('readonly'=>'readonly'),
                    'htmlOptions'=>array('autocomplete'=>'off',),
                ));
                ?>
                <?php echo $form->error($model,'re_create_time_end'); ?>
            </div>
        </div>
    </fieldset>


    <div class="clearfix"></div>

    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Search',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->


