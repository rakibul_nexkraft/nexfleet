<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */

$this->breadcrumbs=array(
    'Requisitions'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Drivers Copy',
);

$this->menu=array(
    array('label'=>'Back', 'url'=>array('view','id'=>$model->id)),
);
?>
    <style rel="stylesheet" type="text/css">
        .table-font{
            font-family: "Times New Roman", Georgia, Serif;
            font-size: 12px;
        }
    </style>

    <div id="driverscopy" style="font-size:13px;margin-top: -5px">
        <div class="table-font"  style="padding: 0px;margin: 0px" align="center">
            <strong>BRAC<br />Transport Department</strong>
        </div>
        <div style="margin-top: -4px">
            <table class="table-font" border="0" style="width:100%; border:none; padding: 0px;margin: 0px">
                <tr>
                    <td style="width: 33.33%;padding: 0px;margin: 0px"><strong>Requisition No # </strong><?php echo $model->id; ?></td>
                    <td style="width: 33.33%;padding: 0px;margin: 0px;text-align: center;"><strong>BPMT No # </strong><?php echo $model->bpmt_ref_no; ?></td>
                    <td style="width: 33.33%;padding: 0px;margin: 0px;text-align: right"><strong>Allocated By # </strong><?php echo $model->updated_by; ?></td>
                </tr>
            </table>
        </div>

        <div class="row-fluid" style="margin-top: -5px">
            <div style="width:100%;">
                <div class="row-fluid table-font"><strong><u>User's Information:</u></strong></strong></div>
                <div class="row-fluid table-font">
                    <table border="0" style="width:100%;border:none;padding: 0px;margin: 0px;">
                        <tr>
                            <td style="width: 50%">
                                <table border="0" style="width:100%; border:none;padding: 0px;margin: 0px;">
                                    <tr>
                                        <td style="width:25%;padding: 0px;">Name</td><td style="width:75%;padding: 0px;"><?php echo $model->user_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;padding: 0px;">PIN</td><td style="width:75%;padding: 0px;"><strong><?php echo $model->user_pin; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;padding: 0px;">Level</td><td style="width:75%;padding: 0px;"><?php echo $model->user_level; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;padding: 0px;" valign="top">Department</td><td style="width:75%;padding: 0px;" valign="top"><?php echo $model->dept_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:25%;padding: 0px;">Contact No</td><td style="width:75%;padding: 0px;"><?php if(!empty($model->user_cell)) {echo "0".$model->user_cell;} ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 50%">
                                <table border="0" style="width:100%; border:none;margin: 0px;">
                                    <tr>
                                        <td style="width:100%;padding: 0px;"><strong>Approved By:</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;padding: 0px;"><?php if(!empty($model->supervisor_name)) {echo $model->supervisor_name."<br />(PIN: <strong>".$model->supervisor_pin."</strong>, Level: ".$model->supervisor_level.")";} ?> &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;padding: 0px;"><strong>Billing Department: </strong><?php echo $model->dept_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;padding: 0px;"><strong>Billing Code: </strong><?php echo $model->billing_code; ?></td>
                                    </tr>
<!--                                    <tr>-->
<!--                                        <td style="width:100%;padding: 0px;"></td>-->
<!--                                    </tr>-->
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid" style="margin-top: -5px">
            <div style="width:100%;">
                <div class="row-fluid table-font"><strong><u>Travel Information:</u></strong></div>
                <div class="row-fluid table-font">
                    <table border="0" class="dataGrid" style="width:100%; border:none; margin-left:0; margin-bottom: 0;">
                        <tr>
                            <td style="padding: 0px;margin: 0px;width:18%">Type of Travel</td><td style="padding: 0px;width:27%"><?php echo $model->dutytypes->type_name; ?></td><td style="width: 1%"></td>
                            <td style="padding: 0px;margin: 0px;width:18%">Number of Passenger</td><td style="padding: 0px;width:36%"><?php echo $model->passenger; ?></td>
                        </tr>
                        <tr>
                            <td style="padding: 0px;margin: 0px;width:18%">Start Date</td><td style="padding: 0px;width:27%"><?php echo $model->start_date; ?></td><td style="width: 1%"></td>
                            <td style="padding: 0px;margin: 0px;width:18%">Start Time</td><td style="padding: 0px;width:36%"><?php echo $model->start_time; ?></td>
                        </tr>
                        <tr>
                            <td style="margin: 0px;padding: 0px;width:18%">Return Date</td><td style="padding: 0px;width:27%"><?php echo $model->end_date; ?></td><td style="width: 1%"></td>
                            <td style="margin: 0px;padding: 0px;width:18%">Return time</td><td style="padding: 0px;width:36%"><?php echo $model->end_time; ?></td>
                        </tr>
                        <tr>
                            <td valign="top" style="margin: 0px;padding: 0px;width:18%">Pick up Location</td><td valign="top" style="padding: 0px;width:27%"><?php echo $model->start_point; ?></td><td style="width: 1%"></td>
                            <td valign="top" style="margin: 0px;padding: 0px;width:18%">Destination(s)</td><td valign="top" style="text-align:justify;text-justify:inter-word;padding: 0px;width:36%"><?php echo $model->end_point; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row-fluid" style="margin-top: -5px">
            <div style="width:100%">
                <div class="row-fluid table-font"><strong><u>Vehicle &amp; Driver's Information:</u></strong></div>
                <div class="row-fluid table-font">
                    <table border="0" style="width:100%; border:none; padding: 0px;margin: 0px;">
                        <tr>
                            <td style="margin: 0px;width:15%;padding: 0px;"><strong>Vehicle No</strong></td><td style="width:20%;padding: 0px;"><strong><?php echo $model->vehicle_reg_no; ?></strong></td>
                            <td style="margin: 0px;width:5%;padding: 0px;"><strong>Type</strong></td><td style="width:17%;padding: 0px;"><?php echo $model->vehicletypes->type; ?></td>
                            <td style="margin: 0px;width:15%;padding: 0px;"><strong>Cell No</strong></td><td style="width:28%;padding: 0px;"><?php if(!empty($model->driver_phone)) {echo "0".$model->driver_phone;} ?></td>
                        </tr>
                        <tr>
                            <td valign="top" style="margin: 0px;width:15%;padding: 0px;"><strong>Driver's Name</strong></td><td valign="top" style="width:20%;padding: 0px;"><?php echo $model->driver_name; ?></td>
                            <td valign="top" style="margin: 0px;width:5%;padding: 0px;"><strong>PIN</strong></td><td valign="top" style="width:17%;padding: 0px;"><strong><?php echo $model->driver_pin; ?></strong></td>
                            <td valign="top" style="margin: 0px;width:15%;padding: 0px;"><strong>Remarks (If Any):</strong></td><td valign="top" style="text-align:justify;text-justify:inter-word;width:28%;padding: 0px;"><?php echo $model->remarks; ?> &nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#driverscopy', //element to be printed
    'title' => CHtml::image(Yii::app()->request->baseUrl."/themes/shadow_dancer/images/logo1.png"),
    //'title' => 'Report generated by: '.Yii::app()->user->name.' '.' =====  '.Yii::app()->name.',
));
?>