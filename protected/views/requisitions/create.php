<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Requisitions', 'url'=>array('index')),
	array('label'=>'Manage Requisitions', 'url'=>array('admin')),
);
}
?>

<h4>New Requisition</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'requisitionsCheckMultiple'=>$requisitionsCheckMultiple,'notice'=>$notice)); ?>