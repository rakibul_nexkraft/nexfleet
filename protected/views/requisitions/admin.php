<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */
$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	'Manage',
);


if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Requisitions', 'url'=>array('index')),
	array('label'=>'New Requisition', 'url'=>array('create')),
);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('requisitions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Requisitions</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php

$this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
	'id'=>'requisitions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'bpmt_ref_no',
		'vehicle_reg_no',
		'driver_pin',
		'driver_name',
		'user_pin',
		'end_point',
		array(
			'name' => 'active',
			'type' => 'raw',
			'value' => 'Requisitions::model()->statusActive($data->active)',
		),
		/*
		'user_name',
		'user_level',
		'dept_name',
		'start_point',
		'end_point',
		'start_date',
		'end_date',
		'start_time',
		'end_time',
		'passenger',
		'created_by',
		'created_time',
		'remarks',
		'dutytype_id',
		'active',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template'=>'{view}{update}',
			'buttons'=>array(
				'view'=>
					array(
						'url'=>'Yii::app()->createUrl("requisitions/view", array("id"=>$data->id))',
						'options'=>array(  
							'ajax'=>array(
								'type'=>'GET',
									// ajax post will use 'url' specified above 
								'url'=>"js:$(this).attr('href')",
								//'update'=>'#id_view',
								'success'=>'function(data){
									$("#dlg-address-view").dialog({title:"Detail View"})
														.dialog({buttons:{"close":function(){$("#id_view").attr("src",""); $(this).dialog("close");}}})
														.dialog("open");
									$("#id_view").html(data); return false;
								}',
							),
						),
					),
				
				'update'=>
					array(
						'url'=>'Yii::app()->createUrl("requisitions/update", array("id"=>$data->id))',
						'options'=>array(  
							'ajax'=>array(
								'type'=>'GET',
									// ajax post will use 'url' specified above 
								'url'=>"js:$(this).attr('href')",
								//'update'=>'#id_view',
								'success'=>'function(data){$("#dlg-address-view")
															.dialog({title:"Update Requisition"})
															.dialog({buttons:{
																"Save":function(){
																//$.fn.yiiGridView.update("requisitions-grid");
																$.post(
																	$("#requisitions-form").attr("action"),
																	$("#requisitions-form").serialize(),
																	$.fn.yiiGridView.update("requisitions-grid")
																);
																$("#id_view").attr("src","");
																$(this).dialog("close");
																},
																"Close":function(){$("#id_view").attr("src",""); $(this).dialog("close");}
															}})
															.dialog("open"); $("#id_view").html(data); return false;}',
							),
						),
					),
			),
		),
	),
));

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
'id'=>'dlg-address-view',
'options'=>array(
    'autoOpen'=>false, //important!
    'modal'=>true,
    'width'=>750,
    'height'=>'auto',
	'position'=>'top',
),
));
?>
<div id="id_view"></div>
<?php $this->endWidget();?>