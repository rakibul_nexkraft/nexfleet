<?php $this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
                    'id',			'supervisor_name',
                    'bpmt_ref_no', 	'supervisor_pin',
                    'vehicle_reg_no', 'supervisor_level',
                    array('name'=>'vehicletype_id', 'value'=>$model->vehicletypes->type),	'created_time',
                    'user_pin',		'updated_time',
                    'user_name',	'ifleet_approval_time',
                    'user_level',	'approve_status',
                    'user_cell',	'driver_pin',
                    'dept_name',	'driver_name',
                    'email',	'driver_phone',

					'start_point',		array('name'=>'dutytype_id','value'=>$model->dutytypes->type_name),
					'end_point',		array('name' => 'active', 'value' => Requisitions::model()->statusActive($model->active)),
					'start_date',		'vehicle_location',
                    'end_date',			'created_by',
					'start_time',		'updated_by',
					'end_time',			'remarks',
					'passenger',         'cancel_status_by_user',
			),
        ),
    ),
));

?>
