<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'enableAjaxValidation'=>true,
	'method'=>'get',
)); ?>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'id'); ?> <br />
		<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'bpmt_ref_no'); ?> <br />
		<?php echo $form->textField($model,'bpmt_ref_no',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>  <br />
		<?php //echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
        <?php
       $this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'vehicle_reg_no',
            'data'=>$model->getAllVehicle(),
            'options'  => array(
                'style'=>'width:100%',
                'allowClear'=>true,
                'placeholder'=>'Select Vehicle Reg No',
                'minimumInputLength' => 1,
            ),
        ));

        ?>
	</div>
</div>


<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>  <br />
		<?php echo $form->textField($model,'driver_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_name'); ?> <br />
		<?php echo $form->textField($model,'driver_name',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?> <br />
		<?php echo $form->textField($model,'user_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>

	<!--<div class="row">
		<?php echo $form->label($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_level'); ?>
		<?php echo $form->textField($model,'user_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dept_name'); ?>
		<?php echo $form->textField($model,'dept_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_point'); ?>
		<?php echo $form->textField($model,'start_point',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_point'); ?>
		<?php echo $form->textField($model,'end_point',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_date'); ?>
		<?php echo $form->textField($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_time'); ?>
		<?php echo $form->textField($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_time'); ?>
		<?php echo $form->textField($model,'end_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'passenger'); ?>
		<?php echo $form->textField($model,'passenger'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('rows'=>6, 'cols'=>50)); ?>
	</div>-->

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'dutytype_id'); ?> <br />
		<?php
			$dutytype = Dutytypes::model()->findAll(array('select'=>'id,type_name'));
			echo $form->dropDownList($model,'dutytype_id',CHtml::listData($dutytype, 'id','type_name'), array('empty' => '', 'style' => 'width:164px;'));
		?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'Vehicle Type'); ?> <br />
		<?php
			$vehicletype = Vehicletypes::model()->findAll(array('select'=>'id,type'));
			echo $form->dropDownList($model,'vehicletype_id',CHtml::listData($vehicletype, 'id','type'), array('empty' => '', 'style' => 'width:164px;'));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'active'); ?> <br />
		<?php echo $form->dropDownlist($model,'active',array(2=>"Approved", 1=>"Not Approved", 0=>"Pending"),array('empty'=>'','style'=>'width:164px;')); ?>
	</div>
</div>

<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>
</fieldset>


<!--<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">-->
<!--<div class="fl">-->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'re_create_date_start'); ?><!--<br />-->
<!--		--><?php
//			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
//				'model'=>$model,
//				'attribute'=>'re_create_date_start',  // name of post parameter
//				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
//				'options'=>array(
//					'dateFormat'=>'yy-mm-dd',
//					//'defaultDate'=>$model->from_date,
//					'changeYear'=>true,
//					'changeMonth'=>true,
//				),
//				'htmlOptions'=>array(
//					'style'=>'height:17px; width:138px;',
//					'autocomplete'=>'off',
//				),
//			));
//		?>
<!--	</div>-->
<!--</div>-->
<!--<div class="fl">-->
<!--<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'re_create_time_start'); ?><!--<br />-->
<!--		--><?php //$this->widget('ext.jui.EJuiDateTimePicker', array(
//                'model'     => $model,
//                'attribute' => 're_create_time_start',
//                'language'=> 'en',//default Yii::app()->language
//                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
//
//                'options'   => array(
//                    //'dateFormat' => 'dd.mm.yy',
//                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
//                    //'ampm' => 'true',
//                ),
//                //'htmlOptions'=>array('readonly'=>'readonly'),
//                'htmlOptions'=>array('autocomplete'=>'off',),
//            ));
//        ?>
<!--		--><?php //echo $form->error($model,'re_create_time_end'); ?>
<!--	</div>-->
<!--</div>-->
<!--</fieldset>-->
<!--<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">-->
<!--<div class="fl">-->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'re_create_date_end'); ?><!--<br />-->
<!--		--><?php
//			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
//				'model'=>$model,
//				'attribute'=>'re_create_date_end',
//				//'value'=>Yii::app()->request->cookies['to_date']->value,
//				'options'=>array(
//				//	'showAnim'=>'fold',
//					'dateFormat'=>'yy-mm-dd',
//					//'defaultDate'=>$model->to_date,
//					'changeYear'=>true,
//					'changeMonth'=>true,
//				),
//				'htmlOptions'=>array(
//					'style'=>'height:17px; width:138px;',
//					'autocomplete'=>'off',
//				),
//			));
//		?>
<!--	</div>-->
<!--</div>-->
<!--<div class="fl">-->
<!--<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'re_create_time_end'); ?><!--<br />-->
<!--		--><?php //$this->widget('ext.jui.EJuiDateTimePicker', array(
//                'model'     => $model,
//                'attribute' => 're_create_time_end',
//                'language'=> 'en',//default Yii::app()->language
//                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
//
//                'options'   => array(
//                    //'dateFormat' => 'dd.mm.yy',
//                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
//                    //'ampm' => 'true',
//                ),
//                //'htmlOptions'=>array('readonly'=>'readonly'),
//                'htmlOptions'=>array('autocomplete'=>'off',),
//            ));
//        ?>
<!--		--><?php //echo $form->error($model,'re_create_time_end'); ?>
<!--	</div>-->
<!--</div>-->
<!--</fieldset>-->


	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    <button type="button" class="btn btn-success btn-lg" onclick="openMap()">Show On Map</button>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->


