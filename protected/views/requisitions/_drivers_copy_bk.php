<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Drivers Copy',
);

$this->menu=array(
	array('label'=>'Back', 'url'=>array('view','id'=>$model->id)),
);
?>

<div id="driverscopy" style="font-size:13px;">
	<div align="center">
		<strong>BRAC<br />Transport Department</strong>
	</div>
	<br />
	
	<div>
		Sub: <strong>Allocation of vehicle for <?php echo $model->dutytypes->type_name; ?> use against Req# <?php echo $model->id; ?></strong>
		<legend></legend>
	</div>
	
	<div class="row-fluid">
		<div style="width:100%;">
			<div class="row-fluid"><strong>User's Information:</strong></div>
			<div class="row-fluid">
				<table border="0" style="width:95%; border:none; margin-left:5%;">
					<tr>
						<td style="width:25%;">Name</td><td style="width:75%"><?php echo $model->user_name; ?></td>
					</tr>
					<tr>
						<td style="width:25%;">PIN</td><td style="width:75%"><?php echo $model->user_pin; ?></td>
					</tr>
					<tr>
						<td style="width:25%;">Level</td><td style="width:75%"><?php echo $model->user_level; ?></td>
					</tr>
					<tr>
						<td style="width:25%;">Department</td><td style="width:75%"><?php echo $model->dept_name; ?></td>
					</tr>
					<tr>
						<td style="width:25%;">Contact No</td><td style="width:75%"><?php if(!empty($model->user_cell)) {echo "0".$model->user_cell;} ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<br />
	
	<div class="row-fluid">
		<div style="width:100%;">
			<div class="row-fluid"><strong>Travel Information:</strong></div>
			<div class="row-fluid">
				<table border="0" class="dataGrid" style="width:95%; border:none; margin-left:5%;">
					<tr>
						<td style="width:25%">Type of Travel</td><td style="width:25%"><?php echo $model->dutytypes->type_name; ?></td>
						<td style="width:25%"></td><td style="width:25%"></td>
					</tr>
					<tr>
						<td style="width:25%">Start Date</td><td style="width:25%"><?php echo $model->start_date; ?></td>
						<td style="width:25%">Start Time</td><td style="width:25%"><?php echo $model->start_time; ?></td>
					</tr>
					<tr>
						<td style="width:25%">Return Date</td><td style="width:25%"><?php echo $model->end_date; ?></td>
						<td style="width:25%">Return time</td><td style="width:25%"><?php echo $model->end_time; ?></td>
					</tr>
					<tr>
						<td style="width:25%">Pick up Location</td><td style="width:25%"><?php echo $model->start_point; ?></td>
						<td style="width:25%"></td><td style="width:25%"></td>
					</tr>
					<tr>
						<td style="width:25%">Destination(s)</td><td style="width:25%"><?php echo $model->end_point; ?></td>
						<td style="width:25%"></td><td style="width:25%"></td>
					</tr>
					<tr>
						<td style="width:25%">Number of Passenger</td><td style="width:25%"><?php echo $model->passenger; ?></td>
						<td style="width:25%"></td><td style="width:25%"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<br />
	
	<div class="row-fluid">
		<div style="width:100%">
			<div class="row-fluid"><strong>Vehicle &amp; Driver's Information:</strong></div>
			<div class="row-fluid">
				<table border="0" style="width:95%; border:none; margin-left:5%;">
					<tr>
						<td style="width:25%;">Vehicle No</td><td style="width:25%;"><?php echo $model->vehicle_reg_no; ?></td>
						<td style="width:25%;">Type</td><td style="width:25%"><?php echo $model->vehicletypes->type; ?></td>
					</tr>
					<tr>
						<td style="width:25%;">Driver's Name</td><td style="width:25%;"><?php echo $model->driver_name; ?></td>
						<td style="width:25%;">PIN</td><td style="width:25%"><?php echo $model->driver_pin; ?></td>
					</tr>
					<tr>
						<td style="width:25%;">Cell No</td><td style="width:25%;"><?php if(!empty($model->driver_phone)) {echo "0".$model->driver_phone;} ?></td>
						<td style="width:25%;"></td><td style="width:25%"></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<br />
	
	<div class="row-fluid">
		<div style="width:100%;">
			<div class="row-fluid"><strong>Billing Department:</strong></div>
			<div class="row-fluid" style="width:95%; margin-left:5%;"><?php echo $model->dept_name; ?></div>
		</div>
	</div>
	<br />
	
	<div class="row-fluid">
		<div style="width:100%;">
			<div class="row-fluid"><strong>Remarks (If Any):</strong></div>
			<div class="row-fluid" style="width:95%; margin-left:5%;"><?php echo $model->remarks; ?> &nbsp;</div>
		</div>
	</div>
	<br />
	
	<div class="row-fluid">
		<div style="width:100%;">
			<div class="row-fluid"><strong>Approved By:</strong></div>
			<div class="row-fluid" style="width:95%; margin-left:5%;"><?php if(!empty($model->supervisor_name)) {echo $model->supervisor_name."<br />(PIN: ".$model->supervisor_pin.", Level: ".$model->supervisor_level.")";} ?> &nbsp;</div>
		</div>
	</div>
	<br />
	
	<div class="row-fluid">
		<div style="width:100%;">
			<div class="row-fluid"><strong>Vehicle Allocated By:</strong></div>
			<div class="row-fluid" style="width:95%; margin-left:5%;"><?php echo $model->updated_by; ?></div>
		</div>
	</div>
</div>
<div class="clearfix">&nbsp;</div>
<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#driverscopy', //element to be printed
    'title' => CHtml::image(Yii::app()->request->baseUrl."/themes/shadow_dancer/images/logo1.png"),
	//'title' => 'Report generated by: '.Yii::app()->user->name.' '.' =====  '.Yii::app()->name.',
));
?>