<style>
    .top-container{
        height: 100%;
    }
    .dashboard-top .dashboard-bottom {
        padding: 0;
        margin: 0 auto 0;
    }

    .dashboard-top-container {
        max-width: 90%;
        margin: 0 auto 0 auto;
        background-color: #FFF;
        padding: 0;
        display: flex;
        align-items: stretch;
        flex-wrap: wrap;
    }

    .dashboard-item {
        width: 150px;
        height: 200px;
        background-color: #EFEFEF;
        color: #1a1a1a;
        /*line-height: 100px;*/
        text-align: center;
        font-weight: bold;
        /*font-size: 60px;*/
        /*padding: 20px;*/
        margin-bottom: 20px;
        flex: 1;
        display: flex;
    }
    .d-item{
        flex: 1;
    }
    .dashboard-item:nth-child(1){
        background-color: #fbfbfb;
    }
    .dashboard-item:nth-child(2){
        background-color: #fbe9fb;
    }
    .dashboard-item:nth-child(3){
        background-color: #daedfb;
    }
    .dashboard-item:nth-child(4){
        background-color: #d2fbec;
    }

    .dashboard-table-container{
        max-width: 90%;
        margin: -35px auto 0 auto;
        /*display: flex;*/
        /*flex-direction: column;*/
    }

    .dashboard-middle{
        max-width: 90%;
        margin: 0 auto 0 auto;
    }
    .table-item{
        flex: 1;
    }
    .form-fields-container{
        /*max-width: 90%;*/
        margin: 40px auto 0 auto;
        width: 70%;
        text-align: center;
        display: flex;
    }
    .form-field{
        /*width: 15%;*/
        flex: 1;
        text-align: center;
        vertical-align: middle;
        margin-top: 10px;
        /*padding: 20px;*/
        /*margin-bottom: 20px;*/
    }
    .margin-padding-0{
        margin: 0;padding: 0;
    }
    .top-table tr td{
        margin:0;
        padding:0;
    }
    .top-table tr td:nth-child(1){
        font-weight: bold;
    }

    .dTableHead tr th{
        text-align: center;
        font-weight: bold;
        font-size: 1.2em;
    }



    .button {
        background-color:#F1F1F1;
        border-radius: 5px;
        color: black!important;
        padding: .5em;
        text-decoration: none;
        font-weight: bold;
        text-shadow: none;
    }

    .button:focus,
    .button:hover {
        background-color: #F1F1F1;
    }


    table.fixed_header {
        border-collapse: separate;
        width: auto;
        text-align:center;
        display: block;
        -webkit-border-radius:3px;
        -moz-border-radius:3px;
        border-radius:3px;
    }
    thead.fixed_header_thead {
        background-color: #EFEFEF;
    }
    tbody.fixed_header_tbody {
        display: block;
    }
    tbody.fixed_header_tbody {
        overflow-y: scroll;
        overflow-x: auto;
        min-height: 250px;
        max-height: 350px;
    }
    thead.fixed_header_thead th {
        min-width: 100px;
        height: 25px;
        border: solid 1px #DEDEDE;
    }
    tbody.fixed_header_tbody td {
        min-width: 100px;
        height: 25px;
        /*border: dashed 1px lightblue;*/
    }
    .control-group{
        margin-bottom: 0;
    }
    #header {
        display: none;
    }
    #topnav {
        display: none;
    }
    #mainmenu {
        display: none;
    }
    #footer {
        display: none;
    }

    /*.fixed_header_thead tr th:nth-child(1){*/
    /*width: 17%;*/
    /*}*/
    /*.fixed_header_thead tr th:nth-child(2),*/
    /*.fixed_header_thead tr th:nth-child(3),*/
    /*.fixed_header_thead tr th:nth-child(4),*/
    /*.fixed_header_thead tr th:nth-child(5),*/
    /*.fixed_header_thead tr th:nth-child(6),*/
    /*.fixed_header_thead tr th:nth-child(7),*/
    /*.fixed_header_thead tr th:nth-child(8),*/
    /*.fixed_header_thead tr th:nth-child(9),*/
    /*.fixed_header_thead tr th:nth-child(10),*/
    /*.fixed_header_thead tr th:nth-child(11){*/
    /*width: 7%;*/
    /*}*/
    /*.fixed_header_thead tr th:nth-child(12){*/
    /*width: 13%;*/
    /*}*/
    /*!*.fixed_header_tbody tr:nth-child(1) >td{*!*/
    /*!*!*border: 0;*!*!*/
    /*!*height: 1px;*!*/
    /*!*!*padding: 0;*!*!*/
    /*!*margin: 0;*!*/
    /*!*}*!*/
    /*.fixed_header_tbody tr >td:nth-child(1){*/
    /*width: 17%;*/
    /*}*/
    /*.fixed_header_tbody tr >td:nth-child(2),*/
    /*.fixed_header_tbody tr >td:nth-child(3),*/
    /*.fixed_header_tbody tr >td:nth-child(4),*/
    /*.fixed_header_tbody tr >td:nth-child(5),*/
    /*.fixed_header_tbody tr >td:nth-child(6),*/
    /*.fixed_header_tbody tr >td:nth-child(7),*/
    /*.fixed_header_tbody tr >td:nth-child(8),*/
    /*.fixed_header_tbody tr >td:nth-child(9),*/
    /*.fixed_header_tbody tr >td:nth-child(10),*/
    /*.fixed_header_tbody tr >td:nth-child(11){*/
    /*width: 7%;*/
    /*}*/
    /*.fixed_header_tbody tr >td:nth-child(12){*/
    /*width: 13%;*/
    /*}*/
</style>
<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile('http://www.google.com/jsapi');
$cs->registerCoreScript('jquery');
//$cs->registerScriptFile($baseUrl.'/js/jquery.gvChart-1.0.1.min.js');
//$cs->registerScriptFile($baseUrl.'/js/pbs.init.js');
$cs->registerCssFile($baseUrl.'/css/jquery.css');
?>

<?php $this->pageTitle=Yii::app()->name; ?>
<div class=clearfix></div>
<!--<h4>Welcome to <i>--><?php //echo CHtml::encode(Yii::app()->name); ?><!--</i> Dashboard</h4>-->

<div class="top-container">
    <div class="dashboard-top" >
        <div class="dashboard-top-container">
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Available</h4>
                    <div id="topAvailable" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="availableVehicle">
                        <table class="top-table">
                            <tbody>
                            <tr>
                                <td>Bus</td>
                                <td> : </td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>Car</td>
                                <td> : </td>
                                <td>11</td>
                            </tr>
                            <tr>
                                <td>Jeep</td>
                                <td> : </td>
                                <td>28</td>
                            </tr>
                            <tr>
                                <td>Microbus</td>
                                <td> : </td>
                                <td>9</td>
                            </tr>
                            <tr>
                                <td>Pickup/Delivery Van</td>
                                <td> : </td>
                                <td>24</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">On-Duty</h4>
                    <div id="topOnDuty" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="ondutyVehicle">
<!--                        <span>Microbus : --><?php //echo $vehicleCount[0]; ?><!--</span><br>-->
<!--                        <span>Jeep : --><?php //echo $vehicleCount[1]; ?><!--</span><br>-->
<!--                        <span>Car : --><?php //echo $vehicleCount[2]; ?><!--</span><br>-->
<!--                        <span>Truck :  --><?php //echo $vehicleCount[3]; ?><!--</span><br>-->
<!--                        <span>Minivan :  --><?php //echo $vehicleCount[4]; ?><!--</span>-->
<!--                        <span>Minivan :  --><?php //echo $vehicleCount[5]; ?><!--</span>-->
                        <table class="top-table">
                            <tbody>
                            <tr>
                                <td>Bus</td>
                                <td> : </td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>Car</td>
                                <td> : </td>
                                <td>11</td>
                            </tr>
                            <tr>
                                <td>Jeep</td>
                                <td> : </td>
                                <td>28</td>
                            </tr>
                            <tr>
                                <td>Microbus</td>
                                <td> : </td>
                                <td>9</td>
                            </tr>
                            <tr>
                                <td>Pickup/Delivery Van</td>
                                <td> : </td>
                                <td>24</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Workshop</h4>
                    <div id="topWorkshop" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="workshopVehicle">
                        <table class="top-table">
                            <tbody>
                            <tr>
                                <td>Bus</td>
                                <td> : </td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>Car</td>
                                <td> : </td>
                                <td>11</td>
                            </tr>
                            <tr>
                                <td>Jeep</td>
                                <td> : </td>
                                <td>28</td>
                            </tr>
                            <tr>
                                <td>Microbus</td>
                                <td> : </td>
                                <td>9</td>
                            </tr>
                            <tr>
                                <td>Pickup/Delivery Van</td>
                                <td> : </td>
                                <td>24</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Staff Pick/Drop</h4>
                    <div id="topStaffPickDrop" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="pickdropVehicle">
                        <table class="top-table">
                            <tbody>
                            <tr>
                                <td>Bus</td>
                                <td> : </td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>Car</td>
                                <td> : </td>
                                <td>11</td>
                            </tr>
                            <tr>
                                <td>Jeep</td>
                                <td> : </td>
                                <td>28</td>
                            </tr>
                            <tr>
                                <td>Microbus</td>
                                <td> : </td>
                                <td>9</td>
                            </tr>
                            <tr>
                                <td>Pickup/Delivery Van</td>
                                <td> : </td>
                                <td>24</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-container" style="margin: 30px;">
    <table class="top-table">
        <tbody>
        <tr>
            <td>Total Jeep in Head Office</td>
            <td> : </td>
            <td><?php echo $vehicleCount[0]; ?></td>
        </tr>
        <tr>
            <td>Total Car in Head Office</td>
            <td> : </td>
            <td><?php echo $vehicleCount[1]; ?></td>
        </tr>
        <tr>
            <td>Total Microbus in Head Office</td>
            <td> : </td>
            <td><?php echo $vehicleCount[2]; ?></td>
        </tr>
        <tr>
            <td>Total Jeep in Nekaton</td>
            <td> : </td>
            <td><?php echo $vehicleCount[3]; ?></td>
        </tr>
        <tr>
            <td>Total Car in Nekaton</td>
            <td> : </td>
            <td><?php echo $vehicleCount[4]; ?></td>
        </tr>
        <tr>
            <td>Total Microbus in Nekaton</td>
            <td> : </td>
            <td><?php echo $vehicleCount[5]; ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="row" style="margin:30px;">
    <table class='table table-bordered dTableHead fixed_header'>
        <thead class='fixed_header_thead'>
        <tr>
            <th style='width:7%'>Requisition No</th>
            <th style='width:7%'>Vehicle No</th>
            <th style='width:7%'>Vehicle type</th>
            <th style='width:12%'>Driver Name</th>
            <th style='width:12%'>Driver Phone</th>
            <th style='width:7%'>User Name</th>
            <th style='width:7%'>Department Name</th>
            <th style='width:7%'>User Level</th>
            <th style='width:7%'>Start Point</th>
            <th style='width:7%'>End Point</th>
            <th style='width:10%'>Start Time</th>
            <th style='width:10%'>End Time</th>
        </tr>
        </thead>
    <tbody class='fixed_header_tbody'>
    <?php
        foreach($model as $item) {
          ?>
            <tr>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['id'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['vehicle_reg_no'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['type'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 12%;'><?php echo $item['driver_name'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 12%;'><?php echo $item['driver_phone'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['user_name'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['dept_name'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['user_level'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['start_point'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 7%;'><?php echo $item['end_point'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 10%;'><?php echo $item['start_time'];?></td>
                <td style=' border: 0;height: 1px;padding: 0;margin: 0;width: 10%;'><?php echo $item['end_time'];?></td>
            </tr>
    <?php
    }
    ?>
    </tbody>
    </table>
</div>
<input type="hidden" id="currentDate" value="<?php echo date('Y-m-d')?>" />
<div class="top-container">
    <marquee style="font-size: 20px; color: red; background: yellowgreen; height: 50px;"><p style="margin-top: 15px;"><?php  foreach($notifications as $notification){ echo '*'.$notification['description'].'  '; }?></p></marquee>
</div>

<script>
    var execute = false;
    var currentDate = $("#currentDate").val();
    var searchVehicle = function (inputData) {
        $('.vehicleSearchTableBody').fadeTo(500,.5);
        var data;
        if(inputData){
            data = inputData;
        }else {
            var searchType = $("select#searchType").val();
            var searchDate = $("#searchDate").val();
            var vehicleRegNo = $("#vehicleRegNo").val();
            var vehicleType = $('#vehicleType').val();
            data = {
                searchType: searchType,
                searchDate: searchDate,
                vehicleRegNo: vehicleRegNo,
                vehicleType: vehicleType
            };
        }
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("site/searchByCriteria");?>',
            data:data,
            success: function(html){
                $('#vehicleSearchTable').html(html);
                $('select#searchType').val(data.searchType);
                $('input#searchDate').val(data.searchDate);
                $('#vehicleRegNo').val(data.vehicleRegNo);
                $('#vehicleType').val(data.vehicleType);
                $('.vehicleSearchTableBody').fadeTo(500,1);
            },
            complete: function (data) {
                if (!execute){
                    getVehicleList();
                    execute = true;
                }
            }
        });
    };
    searchVehicle({searchType: 'onduty', searchDate: $("#currentDate").val(), vehicleRegNo: $('#vehicleRegNo').val(), vehicleType: $('#vehicleType').val()});
    setInterval(function () {
        searchVehicle({searchType: $('select#searchType').val(), searchDate: $("#searchDate").val(), vehicleRegNo: $('#vehicleRegNo').val(), vehicleType: $('#vehicleType').val()})
    }, 300000);


    var showAvailableVehicle = function(){
        var item = {};
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataAvailable");?>',
            data: {currentDate: currentDate},
            dataType:'json',
            success: function(data){
                $('div#topAvailable').text(data.count);
                $('div#availableVehicle').html(data.html);
            },
            complete: function (data) {
                setTimeout(showAvailableVehicle, 300000);
            }

        });
    };
    showAvailableVehicle();
    setTimeout(showAvailableVehicle, 300000);

    var showOnDutyVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataOnDuty");?>',
            data: {currentDate: currentDate},
            dataType:'json',
            success: function(data){
                $('div#topOnDuty').text(data.count);
                $('div#ondutyVehicle').html(data.html);
            }
        });
    };
    showOnDutyVehicle();
    setInterval(showOnDutyVehicle, 300000);

    var showWorkshopVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataWorkshop");?>',
            dataType:'json',
            success: function(data){
                $('div#topWorkshop').text(data.count);
                $('div#workshopVehicle').html(data.html);
            }
        });
    };
    showWorkshopVehicle();
    setInterval(showWorkshopVehicle, 300000);

    var showStaffPickDropVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("site/topDataStaffPickDrop");?>',
            dataType:'json',
            success: function(data){
                $('div#topStaffPickDrop').text(data.count);
                $('div#pickdropVehicle').html(data.html);
            }
        });
    };
    showStaffPickDropVehicle();
    setInterval(showStaffPickDropVehicle, 300000);

</script>