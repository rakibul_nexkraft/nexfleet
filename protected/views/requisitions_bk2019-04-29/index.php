<style>
.fontColorRed,.fontColorRed td a{color:red;}
</style>
<?php
/* @var $this RequisitionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Requisitions',
);

$this->menu=array(
	array('label'=>'New Requisition', 'url'=>array('create')),
	array('label'=>'Manage Requisitions', 'url'=>array('admin')),
    array('label'=>'Manage Requisition Rules', 'url'=>array('requisitionRules/admin')),
);
?>

<h4>Requisitions</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div>
<div id="req_container">
<?php


$var = $_REQUEST['Requisitions'];

if($var):
?>



<h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
<?php if($var['user_pin']) echo "User: ".$var['user_pin']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if($var['vehicle_reg_no'])echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

<?php 

	if($var['active']==0)$active= "Pending";
	else if($var['active']==1)$active= "Not Approved";
	else if($var['active']==2)$active= "Approved";
	if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php endif;?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'requisitions-grid',
    'dataProvider'=>$dataProvider,
    'rowCssClassExpression'=>'$data->color',
    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        array(
            'name' => 'bpmt_ref_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->bpmt_ref_no),array("view","id"=>$data->id))',
        ),
        'vehicle_reg_no',
        		array(
			'name' => 'Vehicle Type',
			'type' => 'raw',
			'value' => '$data->vehicletypes->type',
		),
        'driver_pin',
        'driver_name',
        'user_pin',
        'user_name',
        'user_level',
		'dept_name',
		'start_date',
        'end_date',
		'end_point',
		//'billing_code',
        'start_time',
		array(
			'name' => 'dutytype_id',
			'type' => 'raw',
			'value' => '$data->dutytypes->type_name',
		),
		array(
			'name' => 'active',
			'type' => 'raw',
			'value' => 'Requisitions::model()->statusActive($data->active)',
		),
        'cancel_status_by_user',
		 array(
            'name' => 'User SMS',
            'type' => 'raw',
            'value' => 'Requisitions::model()->statusSms($data->user_cell,$data->updated_time,$data->id)',
        ),

        array(
            'name' => 'Driver SMS',
            'type' => 'raw',
            'value' => 'Requisitions::model()->statusSmsDriver($data->driver_phone,$data->updated_time,$data->id)',
        ),
        array(
            'name' => 'Copy',
            'type' => 'raw',
            //'value' => 'CHtml::textField("","1",array("id"=>"copyReq","width"=>100))',
            'value' => 'CHtml::link("Copy",array("copyCreate","id"=>$data->id))',
            'htmlOptions' => array(),
        )
		
	),
));
?>
</div>
<?php
	echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Requisitions'])));
?> &nbsp; &nbsp; &nbsp;
<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#req_container', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Requisition List',
));
 ?>