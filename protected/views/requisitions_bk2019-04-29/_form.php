<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */
/* @var $form CActiveForm */
?>
<?php

if(isset($requisitionsCheckMultiple) && !empty($requisitionsCheckMultiple)){
	?>
<div style="border: 1px solid red;padding:15px;color:red">       
        <?php echo $requisitionsCheckMultiple; ?>   
</div>

<!-- END Rating  -->

<?php 
        } ?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'requisitions-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<!--<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div> -->
	
<div class="container1">
	<div class="row">
		<?php echo $form->labelEx($model,'bpmt_ref_no'); ?>
		<?php echo $form->textField($model,'bpmt_ref_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'bpmt_ref_no'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin'); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>
	-->
	<div>
				<?php echo $form->labelEx($model,'user_pin'); ?>
			<?php echo $form->textField($model, 'user_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET',
				 'dataType'=>'json',

		    'url'=>array("requisitions/CallHrUser"),
	
        'success'=>"js:function(string){
        				$('#Requisitions_billing_code').val(string.AccountsHeadShortCode);
                        $('#Requisitions_user_name').val(string.StaffName);
                        $('#Requisitions_user_level').val(string.JobLevel);
                        var myarr = string.ProjectName;
                        //var myarr = myarr.split(' ');
                        var myvar = myarr.replace(' Department','');

                        $('#Requisitions_dept_name').val(myvar);
                        $('#Requisitions_user_cell').val(string.MobileNo);
                        $('#Requisitions_email').val(string.EmailID);												
                        $('#Requisitions_user_address').val(string.ContactAddress);
												
                        
                    }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'user_pin'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_level'); ?>
		<?php echo $form->textField($model,'user_level'); ?>
		<?php echo $form->error($model,'user_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dept_name'); ?>
		<?php //echo $form->textField($model,'dept_name',array('size'=>60,'maxlength'=>127)); ?>
     <?php $dept_name = Departments::model()->findAll(array('select'=>'name','order' => 'name ASC'));
        echo $form->dropDownList($model,'dept_name', CHtml::listData($dept_name,'name',  'name'),array('empty' => 'Select Department...')); ?>

		<?php echo $form->error($model,'dept_name'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'billing_code'); ?>
		<?php echo $form->textField($model,'billing_code',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'billing_code'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'user_cell'); ?>
		<?php echo $form->textField($model,'user_cell',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'user_cell'); ?>
	</div>
	
	<!--<div class="row">
		<?php /*echo $form->labelEx($model,'user_address'); */?>
		<?php /*echo $form->textField($model,'user_address',array('size'=>60,'maxlength'=>127)); */?>
		<?php /*echo $form->error($model,'user_addresa'); */?>
	</div>-->
	


	<div class="row">
		<?php echo $form->labelEx($model,'start_point'); ?>
		<?php echo $form->textField($model,'start_point',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'start_point'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_point'); ?>
		<?php echo $form->textField($model,'end_point',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'end_point'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'start_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->start_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                    // 'minDate' => (!empty($model->id)?date($model->start_date):date("Y-m-d")),
                    'maxDate' => date("Y-m-d", strtotime( date("Y-m-d")." +15 day" )),
                    //'minDate' => date("Y-m-d"),
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'end_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->end_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                    //'minDate' => date("Y-m-d"),
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_time'); ?>
		<?php $this->widget('ext.jui.EJuiDateTimePicker', array(
                'model'     => $model,
                'attribute' => 'start_time',
                'language'=> 'en',//default Yii::app()->language
                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
                
                'options'   => array(
                    //'dateFormat' => 'dd.mm.yy',
                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                    //'ampm' => 'true',
                ),
                //'htmlOptions'=>array('readonly'=>'readonly'),
            ));
        ?>
		<?php echo $form->error($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_time'); ?>
		<?php $this->widget('ext.jui.EJuiDateTimePicker', array(
                'model'     => $model,
                'attribute' => 'end_time',
                'language'=> 'en',//default Yii::app()->language
                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
                
                'options'   => array(
                    //'dateFormat' => 'dd.mm.yy',
                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                    //'ampm' => 'true',
                ),
                //'htmlOptions'=>array('readonly'=>'readonly'),
            ));
        ?>
		<?php echo $form->error($model,'end_time'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model,'passenger'); ?>
        <?php echo $form->textField($model,'passenger'); ?>
        <?php echo $form->error($model,'passenger'); ?>
    </div>
	
	</div>
	<div class="container2">

	
	<div class="row">
		<?php echo $form->labelEx($model,'supervisor_pin'); ?>
		<?php //echo $form->textField($model,'supervisor_pin'); ?>
		<?php echo $form->textField($model, 'supervisor_pin',	    
			array('onblur'=>CHtml::ajax(array('type'=>'GET',
					'dataType'=>'json',
	
				'url'=>array("requisitions/CallHrSupervisor"),
		
				'success'=>"js:function(string){
					//alert(string.Fname);
					var name = string.Fname+' '+string.Mname+' '+string.Lname;
					$('#Requisitions_supervisor_name').val(string.StaffName);
					$('#Requisitions_supervisor_level').val(string.JobLevel);
				}"
	
				))),
				array('empty' => 'Select one of the following...')
			
			);
		?>
		<?php echo $form->error($model,'supervisor_pin'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'supervisor_name'); ?>
		<?php echo $form->textField($model,'supervisor_name'); ?>
		<?php echo $form->error($model,'supervisor_name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'supervisor_level'); ?>
		<?php echo $form->textField($model,'supervisor_level'); ?>
		<?php echo $form->error($model,'supervisor_level'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'approve_status'); ?>
		<?php echo $form->radioButton($model,'approve_status',array('value'=>'NO','uncheckValue'=>null)); ?>No &nbsp; &nbsp;
		<?php echo $form->radioButton($model,'approve_status',array('value'=>'YES','uncheckValue'=>null)); ?>Yes
		<?php echo $form->error($model,'approve_status'); ?>
	</div>
	
	<div class="row">
		<?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php

        $this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'vehicle_reg_no',
            'data'=>$model->getAllVehicle(),
            'options'  => array(
                'style'=>'width:100%',
                'allowClear'=>true,
                'placeholder'=>'Select Vehicle Reg No',
                'minimumInputLength' => 1,
            ),
        ));





		/*$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'1',
        'select'=>"js: function(event, ui) {

         $('#Requisitions_driver_pin').val(ui.item['driver_pin']);
         $('#Requisitions_vehicletype_id').val(ui.item['vehicletype_id']);
         //$('#Requisitions_location').val(ui.item['vehicle_location']);
         $('#Requisitions_vehicle_location').val(ui.item['location']);
         $('#Requisitions_driver_name').val(ui.item['driver_name']);
		 $('#Requisitions_driver_phone').val(ui.item['phone']);
         }"
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));*/
		?>
		
		
		<?php  echo $form->error($model,'vehicle_reg_no'); ?>
	</div> 
	
		<div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

		<?php // echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'vehicletype_id'); ?>
	</div>

    <div class="row">
            <?php echo $form->labelEx($model,'vehicle_location'); ?>
            <?php echo $form->textField($model,'vehicle_location',array('size'=>60,'maxlength'=>127)); ?>
            <?php echo $form->error($model,'vehicle_location'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
								<?php echo $form->textField($model, 'driver_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("requisitions/getName"),
	
        'success'=>"js:function(string){  
        
        						if(string=='allocated')
        							alert('Driver already allocated on this time slot');
        						      
                    $('#Requisitions_driver_name').val(string.name);
                    $('#Requisitions_driver_phone').val(string.phone);
         }",
         'error'=>"js:function(string){  
         alert(string)
         }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'driver_name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'driver_phone'); ?>
		<?php echo $form->textField($model,'driver_phone',array('size'=>60,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'driver_phone'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textArea($model,'remarks',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dutytype_id'); ?>
		<?php //echo $form->textField($model,'dutytype_id'); ?>
        <?php $dutytype_id = Dutytypes::model()->findAll(array('select'=>'id,type_name','order' => 'id ASC'));
    echo $form->dropDownList($model,'dutytype_id', CHtml::listData($dutytype_id,'id',  'type_name'),array('empty' => 'Select Duty Type...')); ?>
		<?php echo $form->error($model,'dutytype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->radioButton($model,'active',array('value'=>'0','uncheckValue'=>null)); ?>Pending &nbsp; &nbsp;
		<?php echo $form->radioButton($model,'active',array('value'=>'1','uncheckValue'=>null)); ?>Not Approved &nbsp; &nbsp;
		<?php echo $form->radioButton($model,'active',array('value'=>'2','uncheckValue'=>null)); ?>Approved
		<?php echo $form->error($model,'active'); ?>
	</div>

	
	
</div>

    <div class="clearfix"></div>
	<!--div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div-->
	
	<div align="left">
    <?php 
		if(!(Yii::app()->request->isAjaxRequest))
		{
			$this->widget('bootstrap.widgets.TbButton',array(
				'label' =>'Save',
				'type' => 'primary',
				'buttonType'=>'submit', 
				'size' => 'medium'
			));
		}
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(document).ready(function () {
        $('select#Requisitions_vehicle_reg_no').change(function () {
            var vehicleRegNo = $('select#Requisitions_vehicle_reg_no option:selected').text();
            $.ajax({
                type:'GET',
                url:'<?php echo Yii::app()->createUrl("requisitions/getVehicleDetail");?>',
                data:{vehicleRegNo:vehicleRegNo},
                dataType:'json',
                success: function(data){
                    $('#Requisitions_driver_pin').val(data.driver_pin);
                    $('#Requisitions_vehicletype_id').val(data.vehicletype_id);
                    $('#Requisitions_vehicle_location').val(data.location);
                    $('#Requisitions_driver_name').val(data.driver_name);
                    $('#Requisitions_driver_phone').val(data.phone);
                }
            });
        });
    });
</script>