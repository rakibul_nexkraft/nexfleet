<?php
/* @var $this RequisitionsController */
/* @var $model Requisitions */

$this->breadcrumbs=array(
	'Requisitions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Requisitions', 'url'=>array('index')),
	array('label'=>'New Requisition', 'url'=>array('create')),
	array('label'=>'View Requisition', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Requisitions', 'url'=>array('admin')),
);
?>

<h4>Update Requisition : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>