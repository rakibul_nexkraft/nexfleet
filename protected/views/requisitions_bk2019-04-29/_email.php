<br />
To: <?php echo $model->user_name ?>
<br />
Ref: Transport Requisition Notification - Requisition No# <?php echo $model->id; ?><?php if($model->bpmt_ref_no!=''){echo ' &amp; BPMT Ref No# '.$model->bpmt_ref_no;} ?>
<br />
<br />

Dear Concern,<br /><br />

This is to let you know that your transport requisition has been approved. Please find below the details:<br /><br />

Vehicle No: <?php echo $model->vehicle_reg_no; ?><br />

Vehicle Type: <?php echo $model->vehicletypes->type; ?><br />

Driver's Name: <?php echo $model->driver_name; ?><br />

Driver's Cell No: <?php echo "0".$model->driver_phone; ?><br />

Start Point: <?php echo $model->start_point; ?><br />

End Point: <?php echo $model->end_point; ?><br />

Start Date: <?php echo $model->start_date; ?><br />

End Date: <?php echo $model->end_date; ?><br />

Start Time: <?php echo $model->start_time; ?><br />

End Time: <?php echo $model->end_time; ?><br /><br />

Thanks<br />
Transport Department<br />
<br />
Note: For safe journey maximum speed limit of our vehicle in highway is 70 km/hr.