<style>
	.form #product-attributes-form{
		margin-bottom: unset;
	}
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-attributes-form',
	'enableAjaxValidation'=>false,
   'htmlOptions'=>array('onsubmit'=>"return false")
)); 

 $product_attributes_array= array(
		"Model" => "Model",
		"Vehicle Type" => "Vehicle Type",
		"Unit" => "Unit",
		"Size" => "Size",
		"Parts No" => "Parts No",
	);
?>

	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Product Attribute</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'label', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->dropDownList($model, 'label',$product_attributes_array, array('class' => 'form-control form-control-sm','empty'=>'Select Attribute Label','id'=>'attribute_label'));?>
		</div>
		<?php echo $form->error($model, 'label'); ?>

		<?php echo $form->labelEx($model,'value', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm','id'=>'attribute_value')); ?>
		</div>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color','onclick'=>'submitAttribute()')); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->


<script>

        function submitAttribute(){
        $("#product-attributes-form").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/ProductAttributes/create");?>', 
            type: 'post', 
            success:  function(data) { 
            	
            	if(data==2) {
            		alert("Data already Exist.");
            	}else {
            		alert("Data Saved Successfully.");
            	}          
            }
        });
    }
    </script>

