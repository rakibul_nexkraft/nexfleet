<style>
	.form #product-attributes-form{
		margin-bottom: unset;
	}
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-attributes-form',
	'enableAjaxValidation'=>false,
)); 

 $product_attributes_array= array(
		"Model" => "Model",
		"Vehicle Type" => "Vehicle Type",
		"Unit" => "Unit",
		"Size" => "Size",
		"Parts No" => "Parts No",
	);
?>
	<h3>Given Entry Already Exist</h3>
	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Product Attribute</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'label', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->dropDownList($model, 'label',$product_attributes_array, array('class' => 'form-control form-control-sm','empty'=>'Select Attribute Label'));?>
		</div>
		<?php echo $form->error($model, 'label'); ?>

		<?php echo $form->labelEx($model,'value', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'label'); ?>
		<?php echo $form->textField($model,'label',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'label'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>300)); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div> -->

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->