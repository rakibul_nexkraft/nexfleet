<br />
To: <?php echo $model->user_name ?><br />
<br />
Ref: Transport Requisition Request Notification - Serial No# <?php echo $model->id; ?><br />
<br />
<br />
Dear Concern,<br />
<br />
This is to let you know that you application regarding maternity leave has been received by Transport Department, and necessary action will taken in this regard. 
You are requested to return the Boarder Pass to Transport Department. Please find below your seat allocation details:<br />
<br />
Vehicle No: <?php echo $model->vehicle_reg_no; ?><br />
<br />
Vehicle Type: <?php echo $model->vehicletypes->type; ?><br />
<br />
Route No: <?php echo $model->present_route; ?><br />
<br />
Leave From: <?php echo $model->mt_leave_from; ?><br />
<br />
Leave To: <?php echo $model->mt_leave_to; ?><br />
<br />
<br />
Regards,<br />
Transport Department<br />
<br />
<br />
P.S. This is a computer generated document, hence, required no signature.