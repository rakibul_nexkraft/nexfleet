<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */

$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>' Seat Allocations List', 'url'=>array('index')),
		array('label'=>'New Seat Allocation', 'url'=>array('create')),
	);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('commonfleets-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Seat Allocations</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'commonfleets-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_pin',
		'user_name',
		//'user_level',
		'user_dept',
		'preferred_route',
		'present_route',
		'updated_time',
		'approve_status',
		/*
		'user_designation',
		'user_cell',
		'user_email',
		'telephone_ext',
		'application_type',
		'residence_address',
		'recommended_by',
		'recommended_by_desig',
		'expected_date',
		'mt_leave_from',
		'mt_leave_to',
		'vehicle_reg_no',
		'vehicletype_id',
		'user_remarks',
		'fleet_remarks',
		'created_by',
		'created_time',
		'approve_status',
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array(
				'view'=>
					array(
						'url'=>'Yii::app()->createUrl("commonfleets/view", array("id"=>$data->id))',
						'options'=>array(  
							'ajax'=>array(
								'type'=>'GET',
									// ajax post will use 'url' specified above 
								'url'=>"js:$(this).attr('href')",
								'success'=>'function(data){
									$("#dlg-address-view").dialog({title:"Detail View"})
														.dialog({buttons:{"close":function(){$("#id_view").attr("src",""); $(this).dialog("close");}}})
														.dialog("open");
									$("#id_view").html(data); return false;
								}',
							),
						),
					),
				
				'update'=>
					array(
						'url'=>'Yii::app()->createUrl("commonfleets/update", array("id"=>$data->id))',
						'options'=>array(  
							'ajax'=>array(
								'type'=>'GET',
									// ajax post will use 'url' specified above 
								'url'=>"js:$(this).attr('href')",
								'success'=>'function(data){$("#dlg-address-view")
															.dialog({title:"Update Seat Allocation"})
															.dialog({buttons:{
																"Save":function(){
																$.fn.yiiGridView.update("commonfleets-grid");
																$.post(
																	$("#commonfleets-form").attr("action"),
																	$("#commonfleets-form").serialize(),
																	$.fn.yiiGridView.update("commonfleets-grid")
																);
																$("#id_view").attr("src","");
																$(this).dialog("close");
																},
																"Close":function(){$("#id_view").attr("src",""); $(this).dialog("close");}
															}})
															.dialog("open"); $("#id_view").html(data); return false;}',
							),
						),
					),
			),
		),
	),
));

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
'id'=>'dlg-address-view',
'options'=>array(
    'autoOpen'=>false, //important!
    'modal'=>true,
    'width'=>750,
    'height'=>'auto',
	'position'=>'top',
),
));
?>
<div id="id_view"></div>
<?php $this->endWidget();?>
