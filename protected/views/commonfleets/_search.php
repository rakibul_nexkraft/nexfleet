<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'id'); ?><br />
			<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'user_pin'); ?><br />
			<?php echo $form->textField($model,'user_pin',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'user_name'); ?><br />
			<?php echo $form->textField($model,'user_name',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<!--<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'user_level'); ?><br />
			<?php echo $form->textField($model,'user_level',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'user_dept'); ?><br />
			<?php echo $form->textField($model,'user_dept',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<!--<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'user_designation'); ?><br />
			<?php echo $form->textField($model,'user_designation',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'user_cell'); ?><br />
			<?php echo $form->textField($model,'user_cell',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="row">
		<?php echo $form->label($model,'user_email'); ?>
		<?php echo $form->textField($model,'user_email',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telephone_ext'); ?>
		<?php echo $form->textField($model,'telephone_ext'); ?>
	</div>-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'application_type'); ?><br />
			<?php //echo $form->textField($model,'application_type',array('size'=>18,'class'=>'input-medium')); ?>
			<?php echo $form->dropDownList($model, 'application_type', $this->application_type, array('empty' => 'Select...', 'style' => 'width:165px;')); ?>
		</div>
	</div>
	<!--<div class="row">
		<?php echo $form->label($model,'residence_address'); ?>
		<?php echo $form->textField($model,'residence_address',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'recommended_by'); ?>
		<?php echo $form->textField($model,'recommended_by',array('size'=>40,'maxlength'=>40)); ?>
	</div>
	
	<div class="row">
		<?php echo $form->label($model,'recommended_by_desig'); ?>
		<?php echo $form->textField($model,'recommended_by_desig',array('size'=>11,'maxlength'=>11)); ?>
	</div>-->
	<!--<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'preferred_route'); ?><br />
			<?php //echo $form->textField($model,'preferred_route',array('size'=>18,'class'=>'input-medium')); ?>
			<?php 
				$prefroute = Routes::model()->findAll(array('select' => 'route_no', /*'order' => 'route_no ASC'*/));
				echo $form->dropDownList($model, 'preferred_route', CHtml::listData($prefroute,'route_no','route_no'), array('empty' => 'Select...', 'style' => 'width:164px;'));
			?>
		</div>
	</div>-->
	<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'present_route'); ?><br />
		<?php 
			$presentroute = Routes::model()->findAll(array('select' => 'route_no'));
				echo $form->dropDownList($model, 'present_route', CHtml::listData($presentroute,'route_no','route_no'), array('empty' => 'Select...', 'style' => 'width:164px;'));
		?>
	</div>
	</div>
	<!--<div class="row">
		<?php echo $form->label($model,'expected_date'); ?>
		<?php echo $form->textField($model,'expected_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mt_leave_from'); ?>
		<?php echo $form->textField($model,'mt_leave_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mt_leave_to'); ?>
		<?php echo $form->textField($model,'mt_leave_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicletype_id'); ?>
		<?php echo $form->textField($model,'vehicletype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_remarks'); ?>
		<?php echo $form->textField($model,'user_remarks',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fleet_remarks'); ?>
		<?php echo $form->textField($model,'fleet_remarks',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>-->
	
	<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;'
				),
			));
		?>
	</div>
</div>
</fieldset>
	
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'approve_status'); ?><br />
			<?php //echo $form->textField($model,'approve_status',array('size'=>18,'class'=>'input-medium')); ?>
			<?php echo $form->dropDownList($model, 'approve_status', $this->approve_status, array('empty' => 'Select...', 'style' => 'width:165px;')); ?>
		</div>
	</div>
	<!--<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>-->

	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->