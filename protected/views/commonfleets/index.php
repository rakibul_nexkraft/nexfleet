<!--<script>
	var $excelForm = $( '#commanfleets_excel' );
	$( '.excel_form a', $excelForm ).click( function() {
		$excelForm.attr( 'action', this.href ).submit();
		return false;
	});
</script>-->


<?php
/* @var $this CommonfleetsController */
/* @var $dataProvider CActiveDataProvider */
print_r($border_pass['present_route']);
$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Seat Allocation', 'url'=>array('create')),
		array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
	);
}
?>

<h4>Seat Allocations</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div id=req_container>
<?php
$var = $_GET['Commonfleets'];
if(!empty($var)){
?>
<?php if(!empty($var['from_date']) && !empty($var['to_date'])) echo "<h4>Date: ".$var['from_date']." to ". $var['to_date']."</h4>"; ?>
<?php if(!empty($var['from_date']) && empty($var['to_date'])) echo "<h4>Date: ".$var['from_date']."</h4>"; ?>
<?php if(empty($var['from_date']) && !empty($var['to_date'])) echo "<h4>Date: 0000-00-00 to ". $var['to_date']."</h4>"; ?>
<?php if(!empty($var['user_pin'])) echo "User: ".$var['user_pin']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['vehicle_reg_no'])) echo "Vehicle Regitration Number: ".$var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['approve_status'])) echo "Approve Status: ".$var['approve_status']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['application_type'])) echo "Application Type: ".$var['application_type']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
<?php if(!empty($var['present_route'])) {
    echo "Seat Capacity: ". Routes::model()->getSeatCapacity($var['present_route'])."<br>";
    echo "Maternity Leave: ". Commonfleets::model()->getMaternityLeaveCount($var['present_route']);
    }
    ?>
<?php } ?>

<?php /*$form=$this->beginWidget('CActiveForm', array(
	'id'=>'excel-form',
	'enableAjaxValidation'=>false,
));*/ ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'commonfleets-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
			'name' => 'id',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
		),
		array(
			'name' => 'user_pin',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->user_pin), array("view", "id"=>$data->id))',
		),
		'user_name',
		'user_level',
        'user_dept',
		//'user_designation',
		'user_cell',
        'user_email',
		'application_type',
		'present_route',
		//'preferred_route',
        'stoppage_point',
        'pick_up_time',
	//	'updated_time',
		'approve_status',
		
		/*'user_email',
		'telephone_ext',
		'residence_address',
		'recommended_by',
		'recommended_by_desig',
		'present_route',
		'expected_date',
		'mt_leave_from',
		'mt_leave_to',
		'vehicle_reg_no',
		'vehicletype_id',
		'user_remarks',
		'fleet_remarks',
		'created_by',
		'created_time',
		'approve_status',
		'active',
		*/
	),
)); 
?>
</div>
<?php
	echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Commonfleets'])));
?> &nbsp; &nbsp; &nbsp;

<?php //$this->endWidget(); ?>

<?php /*
	$row = $model->findAll();
	foreach ($row as $item)
		echo ($item->user_name)."<br />";*/ ?>

<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#req_container', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Seat Allocation List',
));
?>