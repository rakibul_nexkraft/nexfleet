<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'commonfleets-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<div class="container1">
	<!---<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin'); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>
	-->
	
	<div>
				<?php echo $form->labelEx($model,'user_pin'); ?>
			<?php echo $form->textField($model, 'user_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("vehicles/CallHrUser"),
	
        'success'=>"js:function(string){

        	$('#Commonfleets_user_name').val(string.StaffName);
            $('#Commonfleets_user_level').val(string.JobLevel);                                    
            
            var myarr = string.ProjectName;
            //var myarr = myarr.split(' ');
            var myvar = myarr.replace('Department','');
            $('#Commonfleets_user_dept').val(myvar);                        
            $('#Commonfleets_user_designation').val(string.DesignationName);
            $('#Commonfleets_user_cell').val(string.MobileNo);
            $('#Commonfleets_user_email').val(string.EmailID);
            $('#Commonfleets_residence_address').val(string.PresentAddress);


            //             //alert(string.Fname);
            //             var name = string.Fname+' '+string.Mname+' '+string.Lname;
            //             $('#Commonfleets_user_name').val(name);
            //             $('#Commonfleets_user_level').val(string.Level);
                                                
                        
            //             var myarr = string.Project;
												// //var myarr = myarr.split(' ');
												// var myvar = myarr.replace('Department','');
            //             $('#Commonfleets_user_dept').val(myvar);                        
            //             $('#Commonfleets_user_designation').val(string.Designation);
												// $('#Commonfleets_user_cell').val(string.Mobile);
												// $('#Commonfleets_user_email').val(string.Email);
												// $('#Commonfleets_residence_address').val(string.ContactAddress);
												
                        
                    }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_level'); ?>
		<?php echo $form->textField($model,'user_level'); ?>
		<?php echo $form->error($model,'user_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_dept'); ?>
		<?php echo $form->textField($model,'user_dept',array('size'=>11,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'user_dept'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_designation'); ?>
		<?php echo $form->textField($model,'user_designation',array('size'=>11,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'user_designation'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_cell'); ?>
		<?php echo $form->textField($model,'user_cell'); ?>
		<?php echo $form->error($model,'user_cell'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_email'); ?>
		<?php echo $form->textField($model,'user_email',array('size'=>11,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'user_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telephone_ext'); ?>
		<?php echo $form->textField($model,'telephone_ext'); ?>
		<?php echo $form->error($model,'telephone_ext'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'application_type'); ?>
		<?php // echo $form->textField($model,'application_type',array('size'=>40,'maxlength'=>40)); ?>
        <?php 
			if(!empty($model->present_route))
			{
				echo $form->dropDownList($model, 'application_type', $this->application_type, array('empty' => 'Select one of the following...'));
			}
			else
			{
				echo $form->dropDownList($model, 'application_type', array('Pick and Drop' =>'Pick and Drop'), array('empty' => 'Select one of the following...'));
			}
		?>
		<?php echo $form->error($model,'application_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'residence_address'); ?>
		<?php echo $form->textArea($model,'residence_address',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'residence_address'); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'recommended_by'); ?>
		<?php echo $form->textField($model,'recommended_by',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'recommended_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recommended_by_desig'); ?>
		<?php echo $form->textField($model,'recommended_by_desig',array('size'=>11,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'recommended_by_desig'); ?>
	</div>
	-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'preferred_route'); ?>
		<?php // echo $form->textField($model,'preferred_route',array('size'=>11,'maxlength'=>11)); ?>
        <?php $preferred_route1 = Routes::model()->findAll(array('select'=>'route_no','order' => 'id ASC'));
        echo $form->dropDownList($model,'preferred_route', CHtml::listData($preferred_route1,'route_no','route_no'),array('empty' => 'Select Routes...')); ?>

        <?php echo $form->error($model,'preferred_route'); ?>
	</div>

	<?php if ($this->action->id=='update'){ ?>
	<div class="row">
		<?php echo $form->labelEx($model,'present_route'); ?>
		<?php echo $form->textField($model,'present_route',array('size'=>11,'maxlength'=>11,'readonly'=>true)); ?>
        <?php //$present_route1 = Routes::model()->findAll(array('select'=>'route_no','order' => 'id ASC'));
        //echo $form->dropDownList($model,'present_route', CHtml::listData($present_route1,'route_no','route_no'),array('empty' => 'Select Routes...')); ?>

        <?php echo $form->error($model,'present_route'); ?>
	</div>
	<?php } ?>
	
</div>
<div class="container2">
        
	<div class="row">
		<?php echo $form->labelEx($model,'stoppage_point'); ?>
		<?php echo $form->textField($model,'stoppage_point',array('size'=>60)); ?>
		<?php echo $form->error($model,'stoppage_point'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'pick_up_time'); ?>
		<?php $this->widget('ext.jui.EJuiDateTimePicker', array(
			'model'     => $model,
			'attribute' => 'pick_up_time',
			'language'=> 'en',//default Yii::app()->language
			'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
			
			'options'   => array(
				//'dateFormat' => 'dd.mm.yy',
				'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
				//'ampm' => 'true',
			),
		));
		?>
		<?php echo $form->error($model,'pick_up_time'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'expected_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'expected_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->expected_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
		<?php echo $form->error($model,'expected_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mt_leave_from'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'mt_leave_from',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>false,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->mt_leave_from,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
		<?php echo $form->error($model,'mt_leave_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mt_leave_to'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'mt_leave_to',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->mt_leave_to,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
		<?php echo $form->error($model,'mt_leave_to'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_remarks'); ?>
        <?php echo $form->textField($model,'user_remarks',array('size'=>60,'maxlength'=>128)); ?>
        <?php echo $form->error($model,'user_remarks'); ?>
    </div>

    <!-- <div>Below all fields for Fleet Admin Only:</div> -->
	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php // echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'attribute' => 'vehicle_reg_no',
            'source'=>$this->createUrl('vehicles/getRegNo'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
         $('#Commonfleets_vehicletype_id').val(ui.item['vehicletype_id']);

         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));		?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
		<?php // echo $form->textField($model,'vehicletype_id'); ?>
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

        <?php echo $form->error($model,'vehicletype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fleet_remarks'); ?>
		<?php echo $form->textField($model,'fleet_remarks',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'fleet_remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approve_status'); ?>
		<?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'approve_status', $this->approve_status,
        array('empty' => 'Select one of the following...')); ?>
		<?php echo $form->error($model,'approve_status'); ?>
	</div>

<!--
    <div class="row">
		<?php // echo $form->labelEx($model,'created_by'); ?>
		<?php // echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php // echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'created_time'); ?>
		<?php // echo $form->textField($model,'created_time'); ?>
		<?php // echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php // echo $form->labelEx($model,'active'); ?>
		<?php // echo $form->textField($model,'active'); ?>
		<?php // echo $form->error($model,'active'); ?>
	</div>
-->
</div>

	<div class="clearfix"></div>
	
	<div align="left">
    <?php 
		if(!(Yii::app()->request->isAjaxRequest))
		{
			$this->widget('bootstrap.widgets.TbButton',array(
				'label' => 'Save',
				'type' => 'primary',
				'buttonType'=>'submit', 
				'size' => 'medium'
			));
		}
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->