<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */

$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>' Seat Allocations List', 'url'=>array('index')),
		array('label'=>'New Seat Allocation', 'url'=>array('create')),
		array('label'=>'View Seat Allocation', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
		array('label'=>'Routes List', 'url'=>array('routes/index')),
	);
}
?>

<h4>Update Seat Allocation : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'application_type'=>$application_type,'approve_status'=>$approve_status)); ?>