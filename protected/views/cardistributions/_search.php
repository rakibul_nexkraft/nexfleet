<?php
/* @var $this CardistributionsController */
/* @var $model Cardistributions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'id'); ?><br />
		<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>-->
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?><br />
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_type'); ?><br />
		<?php echo $form->textField($model,'vehicle_type',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?><br />
		<?php echo $form->textField($model,'user_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_name'); ?><br />
		<?php echo $form->textField($model,'user_name',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'recidence'); ?><br />
		<?php echo $form->textArea($model,'recidence',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>-->
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?><br />
		<?php echo $form->textField($model,'driver_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_name'); ?><br />
		<?php echo $form->textField($model,'driver_name',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'created_by'); ?><br />
		<?php echo $form->textField($model,'created_by',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'created_time'); ?><br />
		<?php echo $form->textField($model,'created_time',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'active'); ?><br />
		<?php echo $form->textField($model,'active',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>-->
    <div class="clearfix"></div>
	<div align="left">
		<?php $this->widget('bootstrap.widgets.TbButton',array(
                    'label' => 'Search',
                    'type' => 'primary',
                    'buttonType'=>'submit', 
                    'size' => 'medium'
                    ));
                ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- search-form -->