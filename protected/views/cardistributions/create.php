<?php
/* @var $this CardistributionsController */
/* @var $model Cardistributions */

$this->breadcrumbs=array(
	'Car Distributions'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Car Distributions', 'url'=>array('index')),
		array('label'=>'Manage Car Distributions', 'url'=>array('admin')),
	);
}
?>

<h4>New Car Distribution</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>