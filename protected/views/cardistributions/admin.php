<?php
/* @var $this CardistributionsController */
/* @var $model Cardistributions */

$this->breadcrumbs=array(
	'Card Dstributions'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Car Distributions', 'url'=>array('index')),
		array('label'=>'New Car Distribution', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('cardistributions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Car Distributions</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cardistributions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
                    'name'=>'id',
                    'type'=>'raw',
                    'value'=>'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),
		'vehicle_reg_no',
		//'user_pin',
		'user_name',
        'user_desig',
        'recidence',
		'driver_name',
		'driver_pin',
		/*
		array(
                    'name'=>'vehicle_type',
                    'type'=>'raw',
                    'value'=>'$data->vehicletypes->type',
                ),
		
		'created_by',
		'created_time',
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
