<?php
/* @var $this CardistributionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Card Dstributions',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Car Distribution', 'url'=>array('create')),
		array('label'=>'Manage Car Distributions', 'url'=>array('admin')),
	);
}
?>

<h4>Car Distributions</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cardistributions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
                    'name'=>'id',
                    'type'=>'raw',
                    'value'=>'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
                ),
		'vehicle_reg_no',
		//'user_pin',
		'user_name',
        'user_desig',
        'recidence',
		'driver_name',
		'driver_pin',
		/*
		array(
                    'name'=>'vehicle_type',
                    'type'=>'raw',
                    'value'=>'$data->vehicletypes->type',
                ),
		
		'created_by',
		'created_time',
		'active',
		*/
	),
)); ?>
