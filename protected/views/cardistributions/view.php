<?php
/* @var $this CardistributionsController */
/* @var $model Cardistributions */

$this->breadcrumbs=array(
	'Card Dstributions'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Car Distributions', 'url'=>array('index')),
		array('label'=>'New Car Distribution', 'url'=>array('create')),
		array('label'=>'Update Car Distribution', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Car Distribution', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Car Distributions', 'url'=>array('admin')),
	);
}
?>

<h4>View Car Distribution : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		array(
                    'name'=>'vehicle_type',
                    'type'=>'raw',
                    'value'=>$model->vehicletypes->type,
                ),
		'user_pin',
		'user_name',
        'user_desig',
		'recidence',
		'driver_pin',
		'driver_name',
		'created_by',
		'created_time',
		//'active',
	),
)); ?>
