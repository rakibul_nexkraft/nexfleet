<?php
/* @var $this CardistributionsController */
/* @var $model Cardistributions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cardistributions-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="container1">
	<!--<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php //echo $form->textField($model,'vehicle_reg_no',array('size'=>60)); ?>
                <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                    'model'=>$model,
                    'attribute' => 'vehicle_reg_no',   
                    'source'=>$this->createUrl('vehicles/getRegNo'),
                    // additional javascript options for the autocomplete plugin
                    'options'=>array(
                        'minLength'=>'2',
                        'select'=>"js: function(event, ui) {
                            $('#Cardistributions_vehicle_type').val(ui.item['vehicletype_id']);
                            $('#Cardistributions_driver_pin').val(ui.item['driver_pin']);
                            $('#Cardistributions_driver_name').val(ui.item['driver_name']);
                        }"        

                    ),
                ));
                ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_type'); ?>
		<?php
                    $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
                    echo $form->dropDownList($model,'vehicle_type', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); 
                ?>
		<?php echo $form->error($model,'vehicle_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('onblur'=>CHtml::ajax(array(
                        'type'=>'GET', 
                        'dataType'=>'json',
                        'url'=>array("vehicles/CallHrUser"),
                        'success'=>"js:function(string){
                            $('#Cardistributions_user_name').val(string.StaffName);												
                            $('#Cardistributions_recidence').val(string.ContactAddress);
                            $('#Cardistributions_user_desig').val(string.DesignationName);
                        }",
                    ))));
                ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_desig'); ?>
        <?php echo $form->textField($model,'user_desig',array('size'=>127)); ?>
        <?php echo $form->error($model,'user_desig'); ?>
    </div>
</div>
<div class="container2">
	<div class="row">
		<?php echo $form->labelEx($model,'recidence'); ?>
		<?php echo $form->textArea($model,'recidence',array('rows'=>4, 'cols'=>50)); ?>
		<?php echo $form->error($model,'recidence'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php //echo $form->textField($model,'driver_pin',array('size'=>60)); ?>
		<?php echo $form->textField($model, 'driver_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("drivers/getName"),
	
        'success'=>"js:function(string){        
                    $('#Cardistributions_driver_name').val(string.name);
         }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60)); ?>
		<?php echo $form->error($model,'driver_name'); ?>
	</div>
</div>
	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>-->
        
        <div class="clearfix"></div>
        
        <div align="left">
            <?php $this->widget('bootstrap.widgets.TbButton',array(
                'label' => 'Save',
                'type' => 'primary',
                'buttonType'=>'submit', 
                'size' => 'medium'
                ));
            ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->