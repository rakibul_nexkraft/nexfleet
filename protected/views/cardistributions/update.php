<?php
/* @var $this CardistributionsController */
/* @var $model Cardistributions */

$this->breadcrumbs=array(
	'Card Dstributions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Car Distributions', 'url'=>array('index')),
		array('label'=>'New Car Distributions', 'url'=>array('create')),
		array('label'=>'View Car Distributions', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Car Distributions', 'url'=>array('admin')),
	);
}
?>

<h4>Update Car Distribution : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>