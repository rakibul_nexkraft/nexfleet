<?php
/* @var $this FeedbackRatingController */
/* @var $model FeedbackRating */

$this->breadcrumbs=array(
	'Feedback Ratings'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Feedback Rating', 'url'=>array('index')),
		array('label'=>'Manage Feedback Rating', 'url'=>array('admin')),
	);
}
?>

<h1>Create FeedbackRating</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>