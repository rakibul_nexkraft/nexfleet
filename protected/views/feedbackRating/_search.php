<?php
/* @var $this FeedbackRatingController */
/* @var $model FeedbackRating */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'car_rating'); ?>
		<?php echo $form->textField($model,'car_rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'car_feedback'); ?>
		<?php echo $form->textField($model,'car_feedback',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_rating'); ?>
		<?php echo $form->textField($model,'driver_rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_feedback'); ?>
		<?php echo $form->textField($model,'driver_feedback',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ifleet_rating'); ?>
		<?php echo $form->textField($model,'ifleet_rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ifleet_feedback'); ?>
		<?php echo $form->textField($model,'ifleet_feedback',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->