<?php
/* @var $this FeedbackRatingController */
/* @var $model FeedbackRating */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'feedback-rating-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'car_rating'); ?>
		<?php echo $form->textField($model,'car_rating'); ?>
		<?php echo $form->error($model,'car_rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'car_feedback'); ?>
		<?php echo $form->textField($model,'car_feedback',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'car_feedback'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_rating'); ?>
		<?php echo $form->textField($model,'driver_rating'); ?>
		<?php echo $form->error($model,'driver_rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_feedback'); ?>
		<?php echo $form->textField($model,'driver_feedback',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'driver_feedback'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ifleet_rating'); ?>
		<?php echo $form->textField($model,'ifleet_rating'); ?>
		<?php echo $form->error($model,'ifleet_rating'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ifleet_feedback'); ?>
		<?php echo $form->textField($model,'ifleet_feedback',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'ifleet_feedback'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->