<?php
/* @var $this FeedbackRatingController */
/* @var $model FeedbackRating */

$this->breadcrumbs=array(
	'Feedback Ratings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Feedback Rating', 'url'=>array('index')),
		array('label'=>'Create Feedback Rating', 'url'=>array('create')),
		array('label'=>'View Feedback Rating', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Feedback Rating', 'url'=>array('admin')),
	);
}
?>

<h1>Update Feedback Rating <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>