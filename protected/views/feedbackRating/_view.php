<?php
/* @var $this FeedbackRatingController */
/* @var $data FeedbackRating */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car_rating')); ?>:</b>
	<?php echo CHtml::encode($data->car_rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('car_feedback')); ?>:</b>
	<?php echo CHtml::encode($data->car_feedback); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_rating')); ?>:</b>
	<?php echo CHtml::encode($data->driver_rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_feedback')); ?>:</b>
	<?php echo CHtml::encode($data->driver_feedback); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ifleet_rating')); ?>:</b>
	<?php echo CHtml::encode($data->ifleet_rating); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ifleet_feedback')); ?>:</b>
	<?php echo CHtml::encode($data->ifleet_feedback); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	*/ ?>

</div>