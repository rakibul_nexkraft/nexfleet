<style>
#chart1{
	display: none;
}
#chart2{
	display: none;
}
#chartDownLoad{
	display: none;
}
 .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        width: 100%;
        margin: auto;
        text-align: center;
        font-family: arial;
        float: left;
        min-height: 250px;
        overflow: hidden;
        border-radius: 40px;
        border: 1px solid transparent;
        border-top-color: #0085ff;
        border-right-color: #0085ff;
        border-left-color: #ec008c;
        border-bottom-color: #ec008c;
        background: white;
    }
    .card p {
        border: none;
        outline: 0;
        padding: 20px;
        color: white;
        text-align: center;
        cursor: pointer;
        font-weight: 600;
        font-size: 35px;
    }

    .card p:hover {
        opacity: 0.7;
    }
      .data-view {
        padding: 20px;
        margin-top: 35px;
        padding-bottom: 0;
    }
</style>
<?php
/* @var $this FeedbackRatingController */
/* @var $dataProvider CActiveDataProvider */
$this->breadcrumbs=array(
	'Feedback Ratings',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Feedback Rating', 'url'=>array('create')),
		array('label'=>'Manage Feedback Rating', 'url'=>array('admin')),
		array('label'=>'Driver Rating', 'url'=>array('/drivers/driverRating')),
	);
}
?>

<h1>Feedback Ratings</h1>
<div class="search-form">
	<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'enableAjaxValidation'=>true,
	'method'=>'get',
)); ?>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'id'); ?> <br />
		<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>  <br />
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
	</div>
</div>


<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>  <br />
		<?php echo $form->textField($model,'driver_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_rating'); ?> <br />
		<?php echo $form->textField($model,'driver_rating',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'ifleet_rating'); ?> <br />
		<?php echo $form->textField($model,'ifleet_rating',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'car_rating'); ?> <br />
		<?php echo $form->textField($model,'car_rating',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'created_by'); ?> <br />
		<?php echo $form->textField($model,'created_by',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>


<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>
</fieldset>






	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    <button type="button" class="btn btn-success btn-lg" onclick="openChart()">Show On Chart</button>
    </div>

<?php $this->endWidget(); ?>
	<div id="chartDownLoad">
		<a  href="http://ifleet.brac.net/protected/controllers/rating.xlsx" class="btn btn-success btn-lg" >Download Excel Chart</a>
	</div>
</div><!-- search-form -->



</div>
<div class="row-fluid">
        <div class="span12">
            <div class="span4">
                <div class="card" style="background: white; border-radius: 10px; width: 100%;" id="chart1">

                    <div id="pieChart1" style="width: 100%; height: 100%;">
                    	
                    </div>
                </div>
            </div>
              <div class="span4">
                <div class="card" style="background: white; border-radius: 10px; width: 100%;" id="chart2">

                    <div id="pieChart2" style="width: 100%; height: 100%;">
                    	
                    </div>
                </div>
            </div>
        </div>
</div>
<style>
    .redColor, .redColor td a{
        color: red;
    }

    .requisitionDetailsModalCustom{
        width: 900px !important;
        margin-left: -450px !important;
    }
</style>
<?php //$this->widget('zii.widgets.CListView', array(
	//'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',
//)); ?>

<?php
    $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'requisitionDetailModal', 'htmlOptions' => array('class'=>'requisitionDetailsModalCustom')));

    echo '<div id="form-modal" class="modal-body">';
    echo '<div id="view-body">Loading...</div>';
    echo '</div>';
    echo '<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>';

    $this->endWidget();
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feedback-rating-grid',
	'dataProvider'=>$model->search(),
    'rowCssClassExpression'=>'( $row%2 ? even : odd ) .(($data->ifleet_rating<3) || ($data->driver_rating<3)?"alert alert-error":"")',
//    'rowCssClassExpression'=>'( $row%2 ? even : odd ) .(($data->ifleet_rating<3) || ($data->driver_rating<3) || ($data->car_rating<3)?"alert alert-error":"")',
	//'filter'=>$model,
	'columns'=>array(
        array(
            'name'  => 'id',
            'value' => function($data){
                return CHtml::link(
                        $data->id,
                        "javascript:void(0)",
                        array(
                            "onclick"=>"requisitionDetail($data->id)",
                            "data-id"=>"$data->id",
                            "id"=>"myModalButton",
//                            "data-toggle"=>"modal",
//                            "data-target"=>"#requisitionDetailModal",
                            "role" =>"button"
                        )
                );
            },
            'type'  => 'raw',
        ),
		'ifleet_rating',
		'ifleet_feedback',
		
		'driver_rating',
		'driver_feedback',
		'car_rating',
		'car_feedback',
		array(          
            'name'=>'Driver Pin',
            'value'=>'FeedbackRating::driverPin($data->id)',
        ),
        array(          
            'name'=>'Driver Name',
            'value'=>'FeedbackRating::driverName($data->id)',
        ),
         /*array(          
            'name'=>'Driver Phone',
            'value'=>'FeedbackRating::driverContact($data->id)',
        ),*/
		
		 array(          
            'name'=>'Vehicle Reg No',
            'value'=>'FeedbackRating::vehicleReg($data->id)',
        ),
		'created_by',
		array(          
            'name'=>'User Name',
            'value'=>'FeedbackRating::userName($data->id)',
        ),
         array(          
            'name'=>'User Department',
            'value'=>'FeedbackRating::userDepartment($data->id)',
        ),
          array(          
            'name'=>'End Point',
            'value'=>'FeedbackRating::vehicleEndPoint($data->id)',
        ),
         /*array(          
            'name'=>'User Phone',
            'value'=>'FeedbackRating::userContact($data->id)',
        ),*/
         //'created_time',
		/*
		'ifleet_feedback',
		'created_by',
		'created_time',
		*/
		//array(
			//'class'=>'CButtonColumn',
		//),
	),
)); ?>
<?php
//$total=$model->search()->getTotalItemCount();
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['FeedbackRating'])));
?> &nbsp; &nbsp; &nbsp;
<script type="text/javascript">
    function requisitionDetail(id) {
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("requisitions/reqDetailView");?>',
            data:{id:id},
            success: function (html) {
                $("#requisitionDetailModal").modal("show");
                $("#view-body").html(html);
                return false;
            }
        });
    }
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script>

    function openChart(){
	    google.charts.load("current", {packages:["corechart"]});
	    google.charts.setOnLoadCallback(drawChart);
	    
	}
    function drawChart() {
    	var start_date=document.getElementById('FeedbackRating_from_date').value;
    	var to_date=document.getElementById('FeedbackRating_to_date').value;
    	if(start_date!='' && to_date!=''){
	    	$.post("<?php echo Yii::app()->createUrl("feedbackRating/chart");?>", {start_date:start_date,to_date:to_date}, function(result){

	   			var myJSON = JSON.parse(result);
	    		//alert(myJSON.driver3);
	    		myJSON.ifleet0=parseInt(myJSON.ifleet0);
	    		myJSON.ifleet1=parseInt(myJSON.ifleet1);
	    		myJSON.ifleet2=parseInt(myJSON.ifleet2);
	    		myJSON.ifleet3=parseInt(myJSON.ifleet3);
	    		myJSON.ifleet4=parseInt(myJSON.ifleet4);
	    		myJSON.ifleet5=parseInt(myJSON.ifleet5);
	    		myJSON.driver0=parseInt(myJSON.driver0);
	    		myJSON.driver1=parseInt(myJSON.driver1);
	    		myJSON.driver2=parseInt(myJSON.driver2);
	    		myJSON.driver3=parseInt(myJSON.driver3);
	    		myJSON.driver4=parseInt(myJSON.driver4);
	    		myJSON.driver5=parseInt(myJSON.driver5);
	        var data = google.visualization.arrayToDataTable([
	            ['iFleet_Rating', 'Rating'],
	            ['5 ~ 4',  myJSON.ifleet5],
	            ['4 ~ 3',  myJSON.ifleet4],
	            ['3 ~ 2',  myJSON.ifleet3],
	            ['2 ~ 1',  myJSON.ifleet2],
	            ['1 ~ 0',  myJSON.ifleet1],
	            ['0',  myJSON.ifleet0],
	        ]);

	        var options = {
	            title: 'iFleet Rating Pie Chart ',

	            colors: ['#2cab3f', '#7FFF00','#0041ab','#FF6347', '#f8b64b','red'],
	            is3D: true,
	            titleTextStyle: {
	                color: '#4c4c4c',
	                fontSize: 12
	            }
	        };

	        var chart = new google.visualization.PieChart(document.getElementById('pieChart1'));
	        chart.draw(data, options);
	        document.getElementById('chart1').style.display="block";

	       var data = google.visualization.arrayToDataTable([
	            ['Driver_Rating', 'Rating'],
	            ['5 ~ 4',  myJSON.driver5],
	            ['4 ~ 3',  myJSON.driver4],
	            ['3 ~ 2',  myJSON.driver3],
	            ['2 ~ 1',  myJSON.driver2],
	            ['1 ~ 0',  myJSON.driver1],
	            ['0',  myJSON.driver0],
	        ]);

	        var options = {
	            title: 'Driver Rating Pie Chart ',
	            colors: ['#2cab3f', '#7FFF00','#0041ab','#FF6347', '#f8b64b','red'],
	            is3D: true,
	            titleTextStyle: {
	                color: '#4c4c4c',
	                fontSize: 12
	            }
	        };

	        var chart = new google.visualization.PieChart(document.getElementById('pieChart2'));
	        chart.draw(data, options);
	        document.getElementById('chart2').style.display="block";
	        });
    	}
    	document.getElementById('chartDownLoad').style.display="block";
    }

</script>