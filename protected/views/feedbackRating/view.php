<?php
/* @var $this FeedbackRatingController */
/* @var $model FeedbackRating */

$this->breadcrumbs=array(
	'Feedback Ratings'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Feedback Rating', 'url'=>array('index')),
		array('label'=>'Create Feedbac kRating', 'url'=>array('create')),
		array('label'=>'Update Feedback Rating', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Feedback Rating', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Feedback Rating', 'url'=>array('admin')),
	);
}
?>

<h1>View Feedback Rating #<?php echo $model->id; ?></h1>

<?php $criteria = new CDbCriteria();
$criteria->condition = 'id=:id';
$criteria->params = array(':id'=>$model->id);
$Requisitions = Requisitions::model()->find($criteria);
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'car_rating',
		'car_feedback',
		'driver_rating',
		'driver_feedback',
		'ifleet_rating',
		'ifleet_feedback',
		array('name' => 'Driver Pin', 'value' =>$Requisitions['driver_pin']),
		array('name' => 'Driver name', 'value' =>$Requisitions['driver_name']),
		'created_by',
		array('name' => 'User Name', 'value' =>$Requisitions['user_name']),
		'created_time',

	),
)); ?>
