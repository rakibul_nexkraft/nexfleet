<?php
/* @var $this AuctionsController */
/* @var $model Auctions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="fl">
	<div class="row">
        <?php echo $form->label($model,'id'); ?> <br />
        <?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>

<div class="fl">
    <div class="row">  
        <?php echo $form->label($model,'vehicle_reg_no'); ?><br />
        <?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
    </div>
</div>

<div class="fl">
	<div class="row">
             <?php echo $form->label($model,'vehicletype_id'); ?><br/>
            <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...','class'=>'input-medium')); ?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'sold_to'); ?><br />
		<?php echo $form->textField($model,'sold_to',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>    
    
<div class="fl">
	<div class="row">
       <?php echo $form->label($model,'selling_price'); ?><br />
        <?php echo $form->textField($model,'selling_price',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>   



<div class="fl">
  <div class="row">
        <?php echo $form->label($model,'date_of_work_order'); ?><br />
        <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model'=>$model,
                'attribute'=>'date_of_work_order',  // name of post parameter
                //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                'options'=>array(
                    'dateFormat'=>'yy-mm-dd',
                    //'defaultDate'=>$model->from_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:17px; width:138px; margin',
                ),
            ));
        ?>

  </div>
</div>

<div class="clearfix"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Search',
            'type' => 'primary',
            'buttonType'=>'submit', 
            'size' => 'medium'
            ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->