<?php
/* @var $this AuctionsController */
/* @var $model Auctions */

$this->breadcrumbs=array(
	'Auctions'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Auctions', 'url'=>array('index')),
		//array('label'=>'New Auction', 'url'=>array('create')),
		array('label'=>'Update Auction', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete Auction', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Auctions', 'url'=>array('admin')),
	);
}
?>

<h4>View Auctions : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		 array('name'=>'vehicletype_id', 'value'=>$model->vehicletypes->type),
		'vehicle_reg_no',
		'sold_to',
		'selling_price',
		'date_of_work_order',
                array(
                    'name'=>'upload_scan',
                    'type'=>'raw',
                    'value'=>  CHtml::link($model->upload_scan, array('download','ufile'=>$model->upload_scan,'vehicle'=>$model->vehicle_reg_no)),
                    //'value'=>  CHtml::link($model->upload_scan, "http://".$_SERVER["SERVER_NAME"].Yii::app()->request->baseUrl."/images/scandoc/".$model->upload_scan),
                ),
                array(
                    'name'=>'active',
                    'type'=>'raw',
                    'value'=>$model->status($model->active),
                ),
	),
)); ?>
