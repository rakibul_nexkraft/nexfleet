<?php
/* @var $this AuctionsController */
/* @var $model Auctions */

$this->breadcrumbs=array(
	'Auctions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Auctions', 'url'=>array('index')),
		//array('label'=>'New Auction', 'url'=>array('create')),
		array('label'=>'View Auction', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Auctions', 'url'=>array('admin')),
	);
}
?>

<h4>Update Auctions : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'status'=>$status)); ?>