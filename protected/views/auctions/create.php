<?php
/* @var $this AuctionsController */
/* @var $model Auctions */

$this->breadcrumbs=array(
	'Auctions'=>array('index'),
	'New',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Auctions', 'url'=>array('index')),
		array('label'=>'Manage Auctions', 'url'=>array('admin')),
	);
}
?>

<h4>Create Auctions</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>