<?php
/* @var $this AuctionsController */
/* @var $data Auctions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />
    
    <b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
    <?php echo CHtml::encode($data->vehicle_reg_no); ?>
    <br />
    

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicletype_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicletype_id); ?>
	<br />



	<b><?php echo CHtml::encode($data->getAttributeLabel('sold_to')); ?>:</b>
	<?php echo CHtml::encode($data->sold_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('selling_price')); ?>:</b>
	<?php echo CHtml::encode($data->selling_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_of_work_order')); ?>:</b>
	<?php echo CHtml::encode($data->date_of_work_order); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('upload_scan')); ?>:</b>
	<?php echo CHtml::encode($data->upload_scan); ?>
	<br />


</div>