<?php
/* @var $this AuctionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Auctions',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	//array('label'=>'New Auction', 'url'=>array('create')),
    	array('label'=>'Manage Auctions', 'url'=>array('admin')),
    );
}
?>

<h4>Auctions</h4>

<?php // $this->widget('zii.widgets.CListView', array(
//	'itemView'=>'_view',
//)); ?>






<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'austions-grid',
    'dataProvider'=>$model->search(),
    //'filter'=>$model,
    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        array(
            'name' => 'vehicle_reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg_no),array("view","id"=>$data->id))',
        ),
        //'vehicle_reg_no',
        
           array(
            'name' => 'vehicletype_id',
            'type' => 'raw',
            'value' => '$data->vehicletypes->type',
        ),
        'sold_to',
        'selling_price',
        'date_of_work_order',
        'upload_scan',
        array(
            'name'=>'active',
            'type'=>'raw',
            'value'=>'$data->status($data->active)',
        ),
        
        /*
        'created_time',
        'created_by',
        */
    ),
)); ?>
<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['auctions'])));
?> &nbsp; &nbsp; &nbsp;