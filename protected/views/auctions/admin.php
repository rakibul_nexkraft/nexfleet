<?php
/* @var $this AuctionsController */
/* @var $model Auctions */

$this->breadcrumbs=array(
	'Auctions'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Auctions', 'url'=>array('index')),
		//array('label'=>'New Auction', 'url'=>array('create')),
	);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('auctions-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Auctions</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'auctions-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		array(
                    'name' => 'vehicletype_id',
                    'type' => 'raw',
                    'value' => '$data->vehicletypes->type',
                ),
		'vehicle_reg_no',
		'sold_to',
		'selling_price',
		'date_of_work_order',
                array(
                    'name'=>'active',
                    'type'=>'raw',
                    'value'=>'$data->status($data->active)',
                ),
		/*
		'upload_scan',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
