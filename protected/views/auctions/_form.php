<?php
/* @var $this AuctionsController */
/* @var $model Auctions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'auctions-form',
	'enableAjaxValidation'=>true,
        'stateful'=>true,
        'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
<?php
    if($status==2){
?>
    <p style="color:red; font-weight: bold;">This vehicle is sold.</p>
<?php
    }
    else {
?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	
    
    <div class="row">
        <?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
        
                <?php     
            
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
        'model'=>$model,
         'attribute' => 'vehicle_reg_no',   
        'source'=>$this->createUrl('vehicles/getRegNo'),
        // additional javascript options for the autocomplete plugin
        'options'=>array(
            'minLength'=>'2',
            'select'=>"js: function(event, ui) {
            
             $('#Requisitions_driver_pin').val(ui.item['driver_pin']);
             $('#Requisitions_vehicletype_id').val(ui.item['vehicletype_id']);
             $('#Requisitions_driver_name').val(ui.item['driver_name']);
             $('#Requisitions_driver_phone').val(ui.item['phone']);
             }"        
        ),
       'htmlOptions'=>array(
           'readonly'=>'readonly',
       ),
    ));        
    ?>
        
   <?php echo $form->error($model,'vehicle_reg_no'); ?>
    </div>
    
    
    <div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
        
        <?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
        echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...','readonly'=>'readonly')); ?>

		<?php echo $form->error($model,'vehicletype_id'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'sold_to'); ?>
		<?php echo $form->textField($model,'sold_to',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'sold_to'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'selling_price'); ?>
		<?php echo $form->textField($model,'selling_price'); ?>
		<?php echo $form->error($model,'selling_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_of_work_order'); ?>   
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'date_of_work_order',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->date_of_work_order,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:200px;'
        ),
        )); ?>        
		<?php echo $form->error($model,'date_of_work_order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'upload_scan'); ?>
		<?php echo CHtml::activeFileField($model,'upload_scan'); ?>
                <?php 
                    /*$this->widget('CMultiFileUpload',array(
                        'name'=>'upload_scan',
                        'accept'=>'jpg|png|gif|jpeg|txt|doc',
                        'max'=>5,
                        'remove'=>Yii::t('ui','Remove'),
                        'denied'=>'This type of file is not supported.', //message that is displayed when a file type is not allowed
                        'duplicate'=>'File already exist.', //message that is displayed when a file appears twice
                        //'htmlOptions'=>array('size'=>25),
                    ));*/
                ?>
		<?php echo $form->error($model,'upload_scan'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->dropDownList($model,'active',array(1=>'For Sale',2=>'Sold')); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<!--<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>-->
        
        <div class="clearfix"></div>
        
        <div align="left">
            <?php $this->widget('bootstrap.widgets.TbButton',array(
                'label' => 'Save',
                'type' => 'primary',
                'buttonType'=>'submit', 
                'size' => 'medium'
                ));
            ?>
        </div>

<?php } $this->endWidget(); ?>

</div><!-- form -->