<?php
/* @var $this EmailSmsController */
/* @var $model EmailSms */
/* @var $form CActiveForm */
?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'email-sms-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>	

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_type'); ?>
		<?php 
		 echo $form->dropDownList($model,'user_type', array('Driver'=>'Driver','Workshop' => 'Workshop'),array('empty' => 'Select A Category','onchange'=>'userType(this.value)')); ?>
	
		<?php echo $form->error($model,'user_type'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?>
		<div id="category">
			<?php 
			 echo $form->dropDownList($model,'category',$drop_down,array('empty' => 'Select A Category')); 
			 ?>
		</div>
		<?php echo $form->error($model,'category'); ?>
	</div>
	
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
	function userType(v) {		
	$.post('<?php echo Yii::app()->createAbsoluteUrl("/emailSms/category");?>',{v:v},function(data){                    
                $('#category').html(data);
        });
	}
</script>