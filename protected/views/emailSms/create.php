<?php
/* @var $this EmailSmsController */
/* @var $model EmailSms */

$this->breadcrumbs=array(
	'Email Sms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Driver SMS List', 'url'=>array('index')),
	//array('label'=>'Manage SMS', 'url'=>array('admin')),
);
?>

<h1>Send SMS</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model,'drop_down'=>$drop_down)); ?>