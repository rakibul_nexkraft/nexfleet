<?php
/* @var $this EmailSmsController */
/* @var $model EmailSms */

$this->breadcrumbs=array(
	'Email Sms'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Driver SMS List', 'url'=>array('index')),
	array('label'=>'Send SMS to Driver', 'url'=>array('create')),
	//array('label'=>'Update EmailSms', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete SMS', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage SMS', 'url'=>array('admin')),
);
?>

<h1>View SMS #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		//'template_id',
		//'route_id',
		//'sub',
		array(
			'name'=>'body',
			'type'=>'raw',
			'value'=>nl2br($model->body),

		),
		'created_by',
		'created_time',
		//'updated_by',
		//'updated_time',
		//'type',
		//'user_type',
	),
)); ?>
