<?php
/* @var $this EmailSmsController */
/* @var $model EmailSms */

$this->breadcrumbs=array(
	'Email Sms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmailSms', 'url'=>array('index')),
	array('label'=>'Create EmailSms', 'url'=>array('create')),
	array('label'=>'View EmailSms', 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>'Manage EmailSms', 'url'=>array('admin')),
);
?>

<h1>Update EmailSms <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>