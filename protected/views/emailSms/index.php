<?php
/* @var $this EmailSmsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Email Sms',
);

$this->menu=array(
	array('label'=>'Send SMS to Driver', 'url'=>array('create')),
	//array('label'=>'Manage SMS', 'url'=>array('admin')),
);
?>

<h1>Driver SMS List</h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'email-sms-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		//'template_id',
		//'route_id',
		//'sub',
		array(
            'name' => 'body',
            'type' => 'raw',
            'value' => 'nl2br($data->body)',                
        ),
        'user_type',	
        'category',	
		'created_by',		
		'created_time',
		/*'updated_by',
		'updated_time',
		'type',
		'user_type',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>
