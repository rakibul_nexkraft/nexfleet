<?php
/* @var $this FuelstationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fuel Stations',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Fuel Station', 'url'=>array('create')),
		array('label'=>'Manage Fuel Stations', 'url'=>array('admin')),
	);
}
?>

<h4>Fuel Stations</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuelstations-grid',
	'dataProvider'=>$dataProvider,
    'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name' => 'name',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->name),array("view","id"=>$data->id))',
        ),
		'address',
		'latitude',
		'longitude',
	),
)); 

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#fuelstations-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Fuel Stations List',
));
?>
<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Fuelstations'])));
?> &nbsp; &nbsp; &nbsp;