<?php
/* @var $this FuelstationsController */
/* @var $model Fuelstations */

$this->breadcrumbs=array(
	'Fuel Stations'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'Fuel Stations List ', 'url'=>array('index')),
    	array('label'=>'New Fuel Station', 'url'=>array('create')),
    );
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuelstations-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Fuel Stations</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'fuelstations-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'name',
        'address',
        'active',
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                'view' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/fuelstations/view", array("id"=>$data->primaryKey))',
                ),
                'update' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/fuelstations/update", array("id"=>$data->primaryKey))',
                ),
                'delete' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/fuelstations/delete", array("id"=>$data->primaryKey))',
                ),
            ),
        ),
    ),
)); ?>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuelstations-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'address',
		'active',
		array(
			'class'=>'CButtonColumn',
		),
	),
));*/ ?>
