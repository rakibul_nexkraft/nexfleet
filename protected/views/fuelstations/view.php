<?php
/* @var $this FuelstationsController */
/* @var $model Fuelstations */

$this->breadcrumbs=array(
	'Fuel Stations'=>array('index'),
	$model->name,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Fuel Stations List', 'url'=>array('index')),
		array('label'=>'New Fuel Station', 'url'=>array('create')),
		array('label'=>'Update Fuel Station', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete Fuelstation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Fuel Stations', 'url'=>array('admin')),
	);
}
?>

<h4>View Fuel Station : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'address',
		'latitude',
		'longitude',
		//'active',
	),
)); ?>
