<?php
/* @var $this FuelstationsController */
/* @var $model Fuelstations */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'name'); ?><br />
			<?php echo $form->textField($model,'name',array('maxlength'=>127)); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'address'); ?><br />
			<?php echo $form->textField($model,'address',array('maxlength'=>127)); ?>
		</div>
	</div>
	<!--<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>-->

	<div class="row buttons" align="right">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->