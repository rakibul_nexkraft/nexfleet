<?php
/* @var $this FuelstationsController */
/* @var $model Fuelstations */

$this->breadcrumbs=array(
	'Fuel Stations'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Fuel Stations List', 'url'=>array('index')),
		array('label'=>'New Fuel Station', 'url'=>array('create')),
		array('label'=>'View Fuel Station', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Fuel Stations', 'url'=>array('admin')),
	);
}
?>

<h4>Update Fuel Station : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>