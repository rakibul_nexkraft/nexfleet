<?php
/* @var $this FuelstationsController */
/* @var $model Fuelstations */

$this->breadcrumbs=array(
	'Fuel Stations'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Fuel Stations List', 'url'=>array('index')),
		array('label'=>'Manage Fuel Stations', 'url'=>array('admin')),
	);
}
?>

<h4>New Fuel Station</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>