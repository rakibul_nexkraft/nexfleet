<?php
/* @var $this FeedbackSettingsController */
/* @var $model FeedbackSettings */

$this->breadcrumbs=array(
	'Feedback Settings'=>array('index'),
	$model->id,
);

/*$this->menu=array(
	array('label'=>'List FeedbackSettings', 'url'=>array('index')),
	array('label'=>'Create FeedbackSettings', 'url'=>array('create')),
	array('label'=>'Update FeedbackSettings', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete FeedbackSettings', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FeedbackSettings', 'url'=>array('admin')),
);*/
?>

<h1>View Feedback Settings <?php //echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'heading',
		'user_title',
		'user_rating_title',
		'driver_title',
		'driver_rating_title',
		'ifleet_title',
		'ifleet_rating_title',
		'rating_popup',
		'created_time',
		'created_by',
		'updated_time',
		'updated_by',
	),
)); ?>
