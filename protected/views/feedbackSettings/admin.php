<?php
/* @var $this FeedbackSettingsController */
/* @var $model FeedbackSettings */

$this->breadcrumbs=array(
	'Feedback Settings'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Feedback Settings', 'url'=>array('index')),
		array('label'=>'Create Feedback Settings', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('feedback-settings-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Feedback Settings</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feedback-settings-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'heading',
		'user_title',
		'user_rating_title',
		'driver_title',
		'driver_rating_title',
		/*
		'ifleet_title',
		'ifleet_rating_title',
		'created_time',
		'created_by',
		'updated_time',
		'updated_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
