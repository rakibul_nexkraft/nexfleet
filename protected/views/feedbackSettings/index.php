<?php
/* @var $this FeedbackSettingsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Feedback Settings',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Feedback Settings', 'url'=>array('create')),
		array('label'=>'Manage Feedback Settings', 'url'=>array('admin')),
	);
}
?>

<h1>Feedback Settings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
