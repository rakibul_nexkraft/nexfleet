<?php
/* @var $this FeedbackSettingsController */
/* @var $model FeedbackSettings */

$this->breadcrumbs=array(
	'Feedback Settings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

//$this->menu=array(
	//array('label'=>'List FeedbackSettings', 'url'=>array('index')),
	//array('label'=>'Create FeedbackSettings', 'url'=>array('create')),
	//array('label'=>'View FeedbackSettings', 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>'Manage FeedbackSettings', 'url'=>array('admin')),
//);
?>

<h1>Update Feedback Settings <?php //echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>