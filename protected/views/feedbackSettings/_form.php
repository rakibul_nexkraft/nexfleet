<?php
/* @var $this FeedbackSettingsController */
/* @var $model FeedbackSettings */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'feedback-settings-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'heading'); ?>
		<?php echo $form->textField($model,'heading',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'heading'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_title'); ?>
		<?php echo $form->textField($model,'user_title',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'user_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_rating_title'); ?>
		<?php echo $form->textField($model,'user_rating_title',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'user_rating_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_title'); ?>
		<?php echo $form->textField($model,'driver_title',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'driver_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_rating_title'); ?>
		<?php echo $form->textField($model,'driver_rating_title',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'driver_rating_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ifleet_title'); ?>
		<?php echo $form->textField($model,'ifleet_title',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'ifleet_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ifleet_rating_title'); ?>
		<?php echo $form->textField($model,'ifleet_rating_title',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'ifleet_rating_title'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'rating_popup'); ?>
		<?php echo $form->textField($model,'rating_popup',array('size'=>60,'maxlength'=>128,'width'=>'50%')); ?>
		<?php echo $form->error($model,'rating_popup'); ?>
	</div>
	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->