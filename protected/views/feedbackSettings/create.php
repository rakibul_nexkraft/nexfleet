<?php
/* @var $this FeedbackSettingsController */
/* @var $model FeedbackSettings */

$this->breadcrumbs=array(
	'Feedback Settings'=>array('index'),
	'Create',
);

//$this->menu=array(
	//array('label'=>'List FeedbackSettings', 'url'=>array('index')),
	//array('label'=>'Manage FeedbackSettings', 'url'=>array('admin')),
//);
?>

<h1>Create Feedback Settings</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>