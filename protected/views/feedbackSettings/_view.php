<?php
/* @var $this FeedbackSettingsController */
/* @var $data FeedbackSettings */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('heading')); ?>:</b>
	<?php echo CHtml::encode($data->heading); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_title')); ?>:</b>
	<?php echo CHtml::encode($data->user_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_rating_title')); ?>:</b>
	<?php echo CHtml::encode($data->user_rating_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_title')); ?>:</b>
	<?php echo CHtml::encode($data->driver_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_rating_title')); ?>:</b>
	<?php echo CHtml::encode($data->driver_rating_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ifleet_title')); ?>:</b>
	<?php echo CHtml::encode($data->ifleet_title); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ifleet_rating_title')); ?>:</b>
	<?php echo CHtml::encode($data->ifleet_rating_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	*/ ?>

</div>