<?php
/* @var $this DepartmentsController */
/* @var $model Departments */

$this->breadcrumbs=array(
	'Departments'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Departments List', 'url'=>array('index')),
		array('label'=>'New Department', 'url'=>array('create')),
		array('label'=>'View Department', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Departments', 'url'=>array('admin')),
	);
}
?>

<h4>Update Department : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>