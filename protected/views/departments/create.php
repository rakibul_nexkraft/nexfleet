<?php
/* @var $this DepartmentsController */
/* @var $model Departments */

$this->breadcrumbs=array(
	'Departments'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Departments List', 'url'=>array('index')),
		array('label'=>'Manage Departments', 'url'=>array('admin')),
	);
}
?>

<h4>New Department</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>