<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'id'); ?></br>
		<?php echo $form->textField($model,'id'); ?>
	</div>
</div>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'defect_id'); ?></br>
		<?php echo $form->textField($model,'defect_id'); ?>
	</div>
</div>-->
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?></br>
		<?php echo $form->textField($model,'vehicle_reg_no'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_model'); ?></br>
		<?php echo $form->textField($model,'vehicle_model'); ?>
	</div>
</div>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'task_id'); ?></br>
		<?php echo $form->textField($model,'task_id'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'wsitemname_id'); ?></br>
		<?php echo $form->textField($model,'wsitemname_id'); ?>
	</div>
</div>-->
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'item_name'); ?></br>
		<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>
</div>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'quantity'); ?></br>
		<?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'item_size'); ?></br>
		<?php echo $form->textField($model,'item_size',array('size'=>20,'maxlength'=>20)); ?>
	</div>
</div>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'item_slno'); ?></br>
		<?php echo $form->textField($model,'item_slno',array('size'=>60,'maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'bill_no'); ?></br>
		<?php echo $form->textField($model,'bill_no'); ?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'mechanic_pin'); ?></br>
		<?php echo $form->textField($model,'mechanic_pin'); ?>
	</div>
</div>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'bill_date'); ?></br>
		<?php echo $form->textField($model,'bill_date'); ?>
	</div>
</div>-->
<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'bill_from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'bill_from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'bill_to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'bill_to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;'
				),
			));
		?>
	</div>
</div>
</fieldset>
<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'issue_from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'issue_from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'issue_to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'issue_to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;'
				),
			));
		?>
	</div>
</div>
</fieldset>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'total_price'); ?></br>
		<?php echo $form->textField($model,'total_price'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'active'); ?></br>
		<?php echo $form->textField($model,'active'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'created_by'); ?></br>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'created_time'); ?></br>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?></br>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'update_time'); ?></br>
		<?php echo $form->textField($model,'update_time'); ?>
	</div>
</div>-->
	<!--<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>-->
	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->