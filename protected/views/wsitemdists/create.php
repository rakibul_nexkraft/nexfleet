<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */

$this->breadcrumbs=array(
	'Item Distributions'=>array('index'),
	'New',
);

$this->menu=array(
	array('label'=>'List Item Distributions', 'url'=>array('index'),'visible'=> in_array('List Item Distributions', $user_rights)),
	array('label'=>'Manage Item Distributions', 'url'=>array('admin'),'visible'=> in_array('Manage Item Distributions', $user_rights)),
);
?>

<h4>New Item Distribution</h4>
<?php if(isset($_GET['iditemdists'])&& !empty($_GET['iditemdists'])){
	$id=$_GET['iditemdists'];
	$model=Wsitemdists::model()->findByAttributes(array('id'=>$id));
}?>

<?php echo $this->renderPartial('_form', array('model'=>$model,'unitlist'=>$unitlist)); ?>