<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemdistsController */
/* @var $model Wsitemdists */

$this->breadcrumbs=array(
	'Item Distributions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Item Distributions', 'url'=>array('index'),'visible'=> in_array('List Item Distributions', $user_rights)),
	array('label'=>'New Item Distribution', 'url'=>array('create'),'visible'=> in_array('New Item Distribution', $user_rights)),
	array('label'=>'View Item Distribution', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Item Distribution', $user_rights)),
	array('label'=>'Manage Item Distributions', 'url'=>array('admin'),'visible'=> in_array('Manage Item Distributions', $user_rights)),
);
?>

<h4>Update Item Distribution <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'up'=>1)); ?>