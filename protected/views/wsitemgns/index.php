<?php
/* @var $this WsitemgnsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wsitemgns',
);

$this->menu=array(
	//array('label'=>'New General Stock', 'url'=>array('create')),
    array('label'=>'New General Stock', 'url'=>array('create'), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('create'),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal();
			$('.modal-body').html(data);
			return false;
		}",
	)))),
	array('label'=>'Manage General Stock', 'url'=>array('admin')),
);
?>

<h4>General Stock</h4>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'wsitemgns-grid',
    //'dataProvider'=>$dataProvider,
    'dataProvider'=>$model->search(),
    'filter'=>$model,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'item_name',
        'wsrequisition_requisition_no',
        'vehicle_reg_no',
        'purchase_type',
        'purchase_date',
        'item_size',
        'quantity',
        'unit_price',
        'total_price',
        'wssupplier_name',

    ),
)); ?>

