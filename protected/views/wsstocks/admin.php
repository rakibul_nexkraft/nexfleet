<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsstocksController */
/* @var $model Wsstocks */

$this->breadcrumbs=array(
	'Wsstocks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Stocks', 'url'=>array('index'),'visible'=> in_array('List Stocks', $user_rights)),
    array('label'=>'Available Stock', 'url'=>array('report1'),'visible'=> in_array('Available Stock', $user_rights)),
    array('label'=>'Inventory List', 'url'=>array('report2'),'visible'=> in_array('Stock Inventory List', $user_rights)),
	// array('label'=>'Create Wsstocks', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('wsstocks-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Stocks</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'wsstocks-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'wsitemname_id',
		'wsitemname',
		'stock_in',
		'stock_out',
		'available_stock',
		/*
		'temp_stock',
		'price',
		'total_amount',
		'purchase_date',
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
