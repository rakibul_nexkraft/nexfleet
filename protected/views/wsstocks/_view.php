<?php
/* @var $this WsstocksController */
/* @var $data Wsstocks */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wsitemname_id')); ?>:</b>
	<?php echo CHtml::encode($data->wsitemname_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wsitemname')); ?>:</b>
	<?php echo CHtml::encode($data->wsitemname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stock_in')); ?>:</b>
	<?php echo CHtml::encode($data->stock_in); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stock_out')); ?>:</b>
	<?php echo CHtml::encode($data->stock_out); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('available_stock')); ?>:</b>
	<?php echo CHtml::encode($data->available_stock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_stock')); ?>:</b>
	<?php echo CHtml::encode($data->temp_stock); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_amount')); ?>:</b>
	<?php echo CHtml::encode($data->total_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('purchase_date')); ?>:</b>
	<?php echo CHtml::encode($data->purchase_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>