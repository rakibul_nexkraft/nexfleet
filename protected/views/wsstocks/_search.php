<?php
/* @var $this WsstocksController */
/* @var $model Wsstocks */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wsitemname_id'); ?>
		<?php echo $form->textField($model,'wsitemname_id'); ?>
	</div>-->
<div class="fl">

	<div class="row">
		<?php echo $form->label($model,'wsitemname'); ?></br>
		<?php echo $form->textField($model,'wsitemname'); ?>
	</div>
</div>
<div class="fl">

	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?></br>
		<?php echo $form->textField($model,'vehicle_reg_no'); ?>
	</div>
</div>
	<!--<div class="row">
		<?php echo $form->label($model,'stock_in'); ?>
		<?php echo $form->textField($model,'stock_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stock_out'); ?>
		<?php echo $form->textField($model,'stock_out'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'available_stock'); ?>
		<?php echo $form->textField($model,'available_stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp_stock'); ?>
		<?php echo $form->textField($model,'temp_stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_amount'); ?>
		<?php echo $form->textField($model,'total_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'purchase_date'); ?>
		<?php echo $form->textField($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>-->
	

	<!--<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>-->
	<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;'
				),
			));
		?>
	</div>
</div>
</fieldset>
<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'issue_from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'issue_from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'issue_to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'issue_to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;'
				),
			));
		?>
	</div>
</div>
</fieldset>

	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->