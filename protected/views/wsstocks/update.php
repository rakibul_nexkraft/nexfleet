<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsstocksController */
/* @var $model Wsstocks */

$this->breadcrumbs=array(
	'Wsstocks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Stocks', 'url'=>array('index'),'visible'=> in_array('List Stocks', $user_rights)),
//	array('label'=>'Create Wsstocks', 'url'=>array('create')),
	array('label'=>'View Stocks', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Stocks', $user_rights)),
	array('label'=>'Manage Stocks', 'url'=>array('admin'),'visible'=> in_array('Manage Stocks', $user_rights)),
);
?>

<h1>Update Wsstocks <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>