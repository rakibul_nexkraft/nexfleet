<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsstocksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wsstocks',
);

$this->menu=array(
	//array('label'=>'Create Wsstocks', 'url'=>array('create')),
	array('label'=>'Manage Stocks', 'url'=>array('admin'),'visible'=> in_array('Manage Stocks', $user_rights)),
    array('label'=>'Available Stock', 'url'=>array('report1'),'visible'=> in_array('Available Stock', $user_rights)),
    array('label'=>'Inventory List', 'url'=>array('report2'),'visible'=> in_array('Stock Inventory List', $user_rights)),
);
?>

<h4>Stocks</h4>
<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php 


$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
      'id'=>'wsstocks-grid',
      'dataProvider'=>$model->search(),
      'filter'=>$model,

    'columns'=>array(
       // 'id',
       array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
   
        'wsitem_id',
        'wsitemname_id',
    'wsitemname',
    'wsdist_id',
        'vehicle_reg_no',
        'vehicle_model',
        'parts_no',
    'stock_in',
    'stock_out',
    'available_stock',    
    'price',
    'total_amount',
    'purchase_date',
    'issue_date',
     'active',   
    
),
    )); ?>
    <?php //$total=$model->search()->getTotalItemCount();
//echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel1','totalSheet'=>$total)));
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel2','Wsstocks'=>$_GET['Wsstocks'])));
?>
