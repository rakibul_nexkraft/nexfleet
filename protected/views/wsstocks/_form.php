<?php
/* @var $this WsstocksController */
/* @var $model Wsstocks */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsstocks-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'wsitemname_id'); ?>
		<?php echo $form->textField($model,'wsitemname_id'); ?>
		<?php echo $form->error($model,'wsitemname_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'wsitemname'); ?>
		<?php echo $form->textField($model,'wsitemname'); ?>
		<?php echo $form->error($model,'wsitemname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stock_in'); ?>
		<?php echo $form->textField($model,'stock_in'); ?>
		<?php echo $form->error($model,'stock_in'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stock_out'); ?>
		<?php echo $form->textField($model,'stock_out'); ?>
		<?php echo $form->error($model,'stock_out'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'available_stock'); ?>
		<?php echo $form->textField($model,'available_stock'); ?>
		<?php echo $form->error($model,'available_stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp_stock'); ?>
		<?php echo $form->textField($model,'temp_stock'); ?>
		<?php echo $form->error($model,'temp_stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price'); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_amount'); ?>
		<?php echo $form->textField($model,'total_amount'); ?>
		<?php echo $form->error($model,'total_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'purchase_date'); ?>
		<?php echo $form->textField($model,'purchase_date'); ?>
		<?php echo $form->error($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>
	<?php if($model->id){?>
	<div class="row">
		<?php echo $form->labelEx($model,'wsitem_id'); ?>
		<?php echo $form->textField($model,'wsitem_id'); ?>
		<?php echo $form->error($model,'wsitem_id'); ?>
	</div>
	<?php } ?>
<!--
	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
-->
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->