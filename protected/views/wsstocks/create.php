<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsstocksController */
/* @var $model Wsstocks */

$this->breadcrumbs=array(
	'Wsstocks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Stocks', 'url'=>array('index'),'visible'=> in_array('List Stocks', $user_rights)),
	array('label'=>'Manage Stocks', 'url'=>array('admin'),'visible'=> in_array('Manage Stocks', $user_rights)),
);
?>

<h1>Create Wsstocks</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>