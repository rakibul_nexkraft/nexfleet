<?php
/* @var $this VehiclesController */
/* @var $model Vehicles */

$this->breadcrumbs=array(
	'Vehicles'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>' Vehicles List', 'url'=>array('index')),
    	array('label'=>'New Vehicle', 'url'=>array('create')),
    );
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('vehicles-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Vehicles</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
   <?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'vehicles-grid',
	'type'=>'striped bordered condensed',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'template'=>"{items}",
    'columns'=>array(
		'reg_no',
		array(
            'name' => 'vehicletype_id',
            'type'=>'raw',
            'value' => '$data->vehicletypes->type',
        ),
        //'vehicletype_id',
		'engine_no',		
		'cc',
		'vmodel',
		'driver_pin',
                array(
                    'name'=>'active',
                    'type'=>'raw',
                    'value'=>'$data->activity($data->active)',
                    'filter'=>array(1=>"Active",2=>"For Sale",3=>"Sold")
                ),
		array
        (
            'class'=>'CButtonColumn',
//            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                'view' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/vehicles/view", array("reg_no"=>$data->primaryKey))',
                ),
                'update' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/vehicles/update", array("reg_no"=>$data->primaryKey))',
                ),
                'delete' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/vehicles/deactivate", array("reg_no"=>$data->primaryKey))',
                    //'imageUrl'=>'($data->active == 0) ? '.(Yii::app()->request->baseUrl."/images/button/active.png").' : '.(Yii::app()->request->baseUrl."/assets/fb0826f4/gridview/delete.png"),
                    'label'=>('$data->active' == 1) ? 'For Auction':'Activate',
                ),
//                'remove' => array
//                (
//                    'label'=> 'Remove',
//                    'icon'=>'icon-remove',
//                    'url'=>'Yii::app()->createUrl("/vehicles/delete", array("reg_no"=>$data->primaryKey))',
//
//                ),
            ),
        ),
		),
    )); ?>
<?php /* $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vehicles-grid',
    'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'reg_no',
		'vehicletype_id',
		'engine_no',		
		'cc',
		'vmodel',
		/*
		'chassis_no',
		'country',
		'purchase_date',
		'purchase_price',
		'supplier',
		'seat_capacity',
		'load_capacity',
		'ac',
		'driver_pin',
		'created_time',
		'created_by',
		'active',
		*/

		/*array(
			'class'=>'CButtonColumn',
		),*/

      /*  array
        (
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                'view' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/vehicles/view", array("reg_no"=>$data->primaryKey))',
                ),
                'update' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/vehicles/update", array("reg_no"=>$data->primaryKey))',
                ),
                'delete' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/vehicles/delete", array("reg_no"=>$data->primaryKey))',
                ),
            ),
        ),
	),
)); */ ?>
