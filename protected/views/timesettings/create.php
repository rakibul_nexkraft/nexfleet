<?php
/* @var $this TimesettingsController */
/* @var $model Timesettings */

$this->breadcrumbs=array(
	'Time Settings'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Time Settings List ', 'url'=>array('index')),
		array('label'=>'Manage Time Settings', 'url'=>array('admin')),
	);
}
?>

<h4>New Time Settings</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>