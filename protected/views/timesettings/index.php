<?php
/* @var $this TimesettingsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Time Settings',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Time Settings', 'url'=>array('create')),
		array('label'=>'Manage Time Settings', 'url'=>array('admin')),
	);
}
?>

<h4>Time Settings</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Timesettings-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(

        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		'start_date',
        'end_date',
        'driver_office_start_time',
        'driver_office_end_time',
        'remarks',
	),
)); ?>