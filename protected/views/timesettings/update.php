<?php
/* @var $this TimesettingsController */
/* @var $model Timesettings */

$this->breadcrumbs=array(
	'Time Settings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Time Settings List', 'url'=>array('index')),
		array('label'=>'Create Time Settings', 'url'=>array('create')),
		array('label'=>'View Time Settings', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Time Settings', 'url'=>array('admin')),
	);
}
?>

<h4>Update Time Settings : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>