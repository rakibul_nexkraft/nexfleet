<?php
/* @var $this TimesettingsController */
/* @var $model Timesettings */

$this->breadcrumbs=array(
	'Time Settings'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Time Settings List', 'url'=>array('index')),
		array('label'=>'New Time Settings', 'url'=>array('create')),
		array('label'=>'Update Time Settings', 'url'=>array('update', 'id'=>$model->id)),
	//	array('label'=>'Delete Dutytype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Time Settingss', 'url'=>array('admin')),
	);
}
?>

<h4>View Time Settings : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'start_date',
		'end_date',
        'driver_office_start_time',
        'driver_office_end_time',
        'remarks'
	),
)); ?>
