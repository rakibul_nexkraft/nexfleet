<?php
/* @var $this TimesettingsController */
/* @var $data Timesettings */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_date); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('end_date')); ?>:</b>
    <?php echo CHtml::encode($data->end_date); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('driver_office_end_time')); ?>:</b>
    <?php echo CHtml::encode($data->driver_office_start_time); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('driver_office_end_time')); ?>:</b>
	<?php echo CHtml::encode($data->driver_office_end_time); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
    <?php echo CHtml::encode($data->remarks); ?>
    <br />


</div>