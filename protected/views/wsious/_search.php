<?php
/* @var $this WsiousController */
/* @var $model Wsious */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<!---
	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iou_date'); ?>
		<?php echo $form->textField($model,'iou_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'designation'); ?>
		<?php echo $form->textField($model,'designation',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iou_amount'); ?>
		<?php echo $form->textField($model,'iou_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'receive_date'); ?>
		<?php echo $form->textField($model,'receive_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'due_date_adjust'); ?>
		<?php echo $form->textField($model,'due_date_adjust'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'adjust_date'); ?>
		<?php echo $form->textField($model,'adjust_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>-->

    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'from_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'from_date',  // name of post parameter
                    //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'to_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'to_date',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        //	'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->to_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </fieldset>

</div>
<div class="clearfix"></div>

<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit',
        'size' => 'medium'
    ));
    ?>
</div>


<?php $this->endWidget(); ?>

</div><!-- search-form -->