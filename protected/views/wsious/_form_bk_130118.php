<?php
/* @var $this WsiousController */
/* @var $model Wsious */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsious-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <div class="container1">

	<div class="row">
		<?php echo $form->labelEx($model,'iou_date'); ?>
		<?php //echo $form->textField($model,'iou_date'); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'iou_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->iou_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'iou_date'); ?>
	</div>

            <div>
                <?php echo $form->labelEx($model,'user_pin'); ?>
                <?php echo $form->textField($model, 'user_pin',
                    array('onblur'=>CHtml::ajax(array('type'=>'GET',
                        'dataType'=>'json',

                        'url'=>array("vehicles/CallHrUser"),

                        'success'=>"js:function(string){
                        //alert(string.Fname);
                        var name = string.Fname+' '+string.Mname+' '+string.Lname;
                        $('#Wsious_user_name').val(name);
                        //$('#Requisitions_user_level').val(string.Level);


                        var myarr = string.Project;
												//var myarr = myarr.split(' ');
						//						var myvar = myarr.replace(' Department','');

                    }"

                    ))),
                    array('empty' => 'Select one of the following...')

                );
                ?>

                <?php echo $form->error($model,'user_pin'); ?>
            </div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'designation'); ?>
		<?php echo $form->textField($model,'designation',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'designation'); ?>
	</div>
            <div class="row">
                <?php echo $form->labelEx($model,'iou_amount'); ?>
                <?php echo $form->textField($model,'iou_amount'); ?>
                <?php echo $form->error($model,'iou_amount'); ?>
            </div>

            </div>

        <div class="container2">



	<div class="row">
		<?php echo $form->labelEx($model,'receive_date'); ?>
		<?php //echo $form->textField($model,'receive_date'); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'receive_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->receive_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'receive_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'due_date_adjust'); ?>
		<?php //echo  $form->textField($model,'due_date_adjust'); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'due_date_adjust',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->due_date_adjust,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'due_date_adjust'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'adjust_date'); ?>
		<?php //echo $form->textField($model,'adjust_date'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'adjust_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->adjust_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'adjust_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>
	
	<div class="row">
        <?php echo $form->labelEx($model,'location'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'location', $this->location,
        array('empty' => 'Select Location...')); ?>
        <?php echo $form->error($model,'location'); ?>
    </div>
	
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php //echo $form->textField($model,'created_time'); ?>
                  <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'created_time',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->created_time,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php //echo $form->textField($model,'updated_time'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'updated_time',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->updated_time,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

-->


</div>
<div class="clear"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->