<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsiousController */
/* @var $model Wsious */

$this->breadcrumbs=array(
	'Wsiouses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List IOUs', 'url'=>array('index'),'visible'=> in_array('List IOUs', $user_rights)),
	array('label'=>'New IOUs', 'url'=>array('create'),'visible'=> in_array('New IOUs', $user_rights)),
	array('label'=>'View IOU', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View IOU', $user_rights)),
	array('label'=>'Manage IOUs', 'url'=>array('admin'),'visible'=> in_array('Manage IOUs', $user_rights)),
);
?>

<h1>Update Wsious <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>