<?php
/* @var $this DriverTrainingController */
/* @var $model DriverTraining */

$this->breadcrumbs=array(
	'Driver Trainings'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Driver Training', 'url'=>array('index')),
		array('label'=>'Create Driver Training', 'url'=>array('create')),
		array('label'=>'Update Driver Training', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Driver Training', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Driver Training', 'url'=>array('admin')),
	);
}
?>

<h1>View Driver Training #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'driver_pin',
		'defence_traning',
		'gen_awarness_traning',
		'google_map_training',
		'defence_traning_date',
		'gen_awarness_traning_date',
		'google_map_training_date',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
