<?php
/* @var $this DriverTrainingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Driver Trainings',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Driver Training', 'url'=>array('create')),
		array('label'=>'Manage Driver Training', 'url'=>array('admin')),
	);
}
?>

<h1>Driver Trainings</h1>

<div class="search-form">
	<div class="wide form">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'driver_pin'); ?> 
				<?php echo $form->textField($model,'driver_pin'); ?>
			</div>
		</div>

		<div class="fl">
			<div class="row">

				<?php echo $form->label($model,'training'); ?> 
				<?php echo $form->dropDownList($model, 'training',  array('1'=>'Defensive Driving Training','2'=>'Gender Awareness Training','3'=>'Google Map Training','4'=>'All Training'),array('empty'=>'Select Option')); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'training_date_from'); ?> 
				<?php //echo $form->textField($model,'training_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'training_date_from',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->training_date_from,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'training_date_to'); ?> 
				<?php //echo $form->textField($model,'training_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'training_date_to',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->training_date_to,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
			</div>
		</div>

			<div class="clearfix"></div>
		    
		    <div align="left">
		    <?php $this->widget('bootstrap.widgets.TbButton',array(
		        'label' => 'Search',
		        'type' => 'primary',
		        'buttonType'=>'submit', 
		        'size' => 'medium'
		        ));
		    ?>
		        
		    </div>

		<?php $this->endWidget(); ?>
	</div>

</div><!-- search-form -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'driver-training-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		//'id',
		'driver_pin',
		'name',
		'phone',
		//array(
          //  'name' => 'name',
            //'type'=>'raw',
            //'value' => '$data->driverName($data->driver_pin)',
            
        //),
		//array(
          //  'name' => 'phone',
            //'type'=>'raw',
            //'value' => '$data->driverPhone($data->driver_pin)',
            
        //),
		array(
            'name' => 'defence_traning',
            'type'=>'raw',
            'value' => '($data->defence_traning==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),
         'defence_traning_date',
		array(
            'name' => 'gen_awarness_traning',
            'type'=>'raw',
            'value' => '($data->gen_awarness_traning==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),      
        'gen_awarness_traning_date',
        array(
            'name' => 'google_map_training',
            'type'=>'raw',
            'value' => '($data->google_map_training==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),      
        'google_map_training_date',
		//'created_by',
		//'created_time',
		//'updated_by',
		//'updated_time',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}',
			'buttons'=>array(
				'update' => array( 
				'visible'=>'$data->id!="" && !Yii::app()->user->isViewUser()',
				),
			),
		),
	),
)); 
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['DriverTraining'])));
?>