<?php
/* @var $this DriverTrainingController */
/* @var $data DriverTraining */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('defence_traning')); ?>:</b>
	<?php echo CHtml::encode($data->defence_traning); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gen_awarness_traning')); ?>:</b>
	<?php echo CHtml::encode($data->gen_awarness_traning); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('google_map_training')); ?>:</b>
	<?php echo CHtml::encode($data->google_map_training); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('defence_traning_date')); ?>:</b>
	<?php echo CHtml::encode($data->defence_traning_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gen_awarness_traning_date')); ?>:</b>
	<?php echo CHtml::encode($data->gen_awarness_traning_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('google_map_training_date')); ?>:</b>
	<?php echo CHtml::encode($data->google_map_training_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	*/ ?>

</div>