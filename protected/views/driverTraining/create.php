<?php
/* @var $this DriverTrainingController */
/* @var $model DriverTraining */

$this->breadcrumbs=array(
	'Driver Trainings'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Driver Training', 'url'=>array('index')),
		array('label'=>'Manage Driver Training', 'url'=>array('admin')),
	);
}
?>

<h1>Create DriverTraining</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>