<?php
/* @var $this DriverTrainingController */
/* @var $model DriverTraining */

$this->breadcrumbs=array(
	'Driver Trainings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Driver Training', 'url'=>array('index')),
		array('label'=>'Create Driver Training', 'url'=>array('create')),
		array('label'=>'View Driver Training', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Driver Training', 'url'=>array('admin')),
	);
}
?>

<h1>Update Driver Training <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>