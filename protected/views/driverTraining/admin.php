<?php
/* @var $this DriverTrainingController */
/* @var $model DriverTraining */

$this->breadcrumbs=array(
	'Driver Trainings'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'List Driver Training', 'url'=>array('index')),
    	//array('label'=>'Manage Driver Training', 'url'=>array('admin')),
    );
}

?>

<h1>Manage Driver Trainings</h1>




<div class="search-form" style="display:none">

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'driver-training-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'driver_pin',
		array(
            'name' => 'defence_traning',
            'type'=>'raw',
            'value' => '($data->defence_traning==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),
         'defence_traning_date',
		array(
            'name' => 'gen_awarness_traning',
            'type'=>'raw',
            'value' => '($data->gen_awarness_traning==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),      
        'gen_awarness_traning_date',
        array(
            'name' => 'google_map_training',
            'type'=>'raw',
            'value' => '($data->google_map_training==1)?"Yes":"NO"',
            'filter'=>array("0"=>"No","1"=>"Yes"),
        ),      
        'google_map_training_date',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
