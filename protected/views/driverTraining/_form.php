<?php
/* @var $this DriverTrainingController */
/* @var $model DriverTraining */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'driver-training-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="container1">
    <div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>
                                <?php echo $form->textField($model, 'driver_pin',       
        array('onblur'=>CHtml::ajax(array('type'=>'GET', 
                 'dataType'=>'json',

            'url'=>array("driverTraining/getName"),
    
            'success'=>"js:function(string){            
                             
                                          
                        $('#DriverTraining_name').val(string);
                       
             }",
             'error'=>"js:function(string){  
             alert(string)
             }"

            ))),
            array('empty' => 'Select one of the following...')
        
        );
         ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div>
	 

	<div class="row">
        <?php echo $form->labelEx($model,'defence_traning'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'defence_traning', array('0'=>'No','1'=>'Yes')); ?>
        <?php echo $form->error($model,'defence_traning'); ?>
    </div>
        
    <div class="row">
        <?php echo $form->labelEx($model,'gen_awarness_traning'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'gen_awarness_traning',  array('0'=>'No','1'=>'Yes')); ?>
        <?php echo $form->error($model,'gen_awarness_traning'); ?>
    </div>
     <div class="row">
        <?php echo $form->labelEx($model,'google_map_training'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'google_map_training',  array('0'=>'No','1'=>'Yes')); ?>
        <?php echo $form->error($model,'google_map_training'); ?>
    </div>
   
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
</div> 
<div class="container2"> 
    <?php if($model->isNewRecord){ ?> 
      <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div> 	
    <?php }
        else{
            $driver_sql = "SELECT * FROM tbl_drivers where  pin = '$model->driver_pin'";
            $driver = Yii::app()->db->createCommand($driver_sql)->queryRow();
            $model->name=$driver['name'];
    ?>
     <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div> 

    <?php
        }
    ?>
	 <div class="row">
        <?php echo $form->labelEx($model,'defence_traning_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'defence_traning_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->defence_traning_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
        <?php echo $form->error($model,'defence_traning_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'gen_awarness_traning_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'gen_awarness_traning_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->gen_awarness_traning_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
        <?php echo $form->error($model,'gen_awarness_traning_date'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'google_map_training_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'google_map_training_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->google_map_training_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
        <?php echo $form->error($model,'google_map_training_date'); ?>
    </div>
   
    
		
</div>
	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>-->

	

<?php $this->endWidget(); ?>

</div><!-- form -->