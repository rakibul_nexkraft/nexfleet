<?php
/* @var $this WsservicesController */
/* @var $model Wsservices */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'wsservices-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'service_name'); ?>
		<?php echo $form->textField($model,'service_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'service_name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'service_category'); ?>
		<?php echo $form->textField($model,'service_category',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'service_category'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'car'); ?>
		<?php echo $form->textField($model,'car',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'car'); ?>
	</div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'micro_jeep'); ?>
		<?php echo $form->textField($model,'micro_jeep',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'micro_jeep'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'pickup'); ?>
		<?php echo $form->textField($model,'pickup',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'pickup'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'bus_coaster'); ?>
		<?php echo $form->textField($model,'bus_coaster',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'bus_coaster'); ?>
	</div>
<!--
	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>


	<div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
    -->

    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->