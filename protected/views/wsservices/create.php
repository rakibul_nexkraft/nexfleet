<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsservicesController */
/* @var $model Wsservices */

$this->breadcrumbs=array(
	'Wsservices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Service List - Inside', 'url'=>array('index'),'visible'=> in_array('Service List - Inside', $user_rights)),
	array('label'=>'Manage Service list - Inside', 'url'=>array('admin'),'visible'=> in_array('Manage Service list - Inside', $user_rights)),
);
?>

<h4>New Service - Inside</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>