<?php
/* @var $this CostsController */
/* @var $data Costs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night_halt')); ?>:</b>
	<?php echo CHtml::encode($data->night_halt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parking_charge')); ?>:</b>
	<?php echo CHtml::encode($data->parking_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('toll')); ?>:</b>
	<?php echo CHtml::encode($data->toll); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seat_Rent')); ?>:</b>
	<?php echo CHtml::encode($data->seat_Rent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('police_case')); ?>:</b>
	<?php echo CHtml::encode($data->police_case); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('others')); ?>:</b>
	<?php echo CHtml::encode($data->others); ?>
	<br />


</div>