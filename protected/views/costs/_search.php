<?php
/* @var $this CostsController */
/* @var $model Costs */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'night_halt'); ?>
		<?php echo $form->textField($model,'night_halt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parking_charge'); ?>
		<?php echo $form->textField($model,'parking_charge'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'toll'); ?>
		<?php echo $form->textField($model,'toll'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'seat_Rent'); ?>
		<?php echo $form->textField($model,'seat_Rent'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'police_case'); ?>
		<?php echo $form->textField($model,'police_case',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'others'); ?>
		<?php echo $form->textField($model,'others',array('size'=>60,'maxlength'=>127)); ?>
	</div>
	
	<div class="row">
        <?php echo $form->labelEx($model,'bill_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$model,'attribute'=>'bill_date',
	
			// additional javascript options for the date picker plugin
			'options'=>array('autoSize'=>true,
				'dateFormat'=>'yy-mm-dd',
				'defaultDate'=>$model->bill_date,
				'changeYear'=>true,
				'changeMonth'=>true,
			),
	
			'htmlOptions'=>array('style'=>'width:150px;'),
		));	?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->