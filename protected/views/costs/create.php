<?php
/* @var $this CostsController */
/* @var $model Costs */

$this->breadcrumbs=array(
	'Costs'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Costs', 'url'=>array('index')),
		array('label'=>'Manage Costs', 'url'=>array('admin')),
	);
}
?>

<h4>Create Costs</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>