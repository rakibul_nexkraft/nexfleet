<?php
/* @var $this CostsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Costs',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Costs', 'url'=>array('create')),
		array('label'=>'Manage Costs', 'url'=>array('admin')),
	);
}
?>

<h4>Costs</h4>

<?php /* $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'costs-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'vehicle_reg_no',
			'type'=>'raw',
			'value' => 'CHtml::link($data->vehicle_reg_no,array("view","id"=>$data->id))'
		),
		//'night_halt',
		'parking_charge',
		'toll',
		'seat_Rent',
		'police_case',
		'breakfast',
		'lunchbreak',
		'others',
		'bill_date',
	),
)); ?>

<?php
	echo CHtml::link('Export to Excel',array('costs/excel', 'criteria'=>$_GET['Costs']));
?> &nbsp; &nbsp; &nbsp;

<?php
	$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#costs-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Miscellaneous Costs',
));
?>