<?php
/* @var $this CostsController */
/* @var $model Costs */

$this->breadcrumbs=array(
	'Costs'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Costs', 'url'=>array('index')),
		array('label'=>'Create Costs', 'url'=>array('create')),
		array('label'=>'Update Costs', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Costs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Costs', 'url'=>array('admin')),
	);
}
?>

<h4>View Costs #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'group1'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
				'id',					'vehicle_reg_no',	
				'parking_charge',       'toll_description',
				'parking_charge_date',	'toll_quantity',
				'toll',                 'seat_Rent',
				'toll_date',			'seat_rent_date',
				'police_case',          'breakfast',
				'police_case_date',     'breakfast_date',
				'lunchbreak',           'others',
				'lunchbreak_date',		'others_date',
				'bill_date',            'created_time',
				'driver_pin',			'created_by',
				array(),				'total_cost',
			),
		),
	),
)); ?>


<?php
	$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#yw0', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png").'<h4>Transport Department - Miscellaneous Costs</h4>',
	  //'title' => 'Transport Department - Miscellaneous Costs',
));
?>