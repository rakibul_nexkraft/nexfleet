<?php
/* @var $this CostsController */
/* @var $model Costs */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'costs-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<!--	<div class="row">
		<?php// echo $form->labelEx($model,'id'); ?>
		<?php //echo $form->textField($model,'id'); ?>
		<?php //echo $form->error($model,'id'); ?>
	</div>-->
    
    
    <div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {
        
         $('#Costs_driver_pin').val(ui.item['driver_pin']);
         }"        
        
    ),
    'htmlOptions'=>array(
        'style'=>''
    ),
)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>
    
	<!--div class="row">
		<?php echo $form->labelEx($model,'night_halt'); ?>
		<?php echo $form->textField($model,'night_halt'); ?>
		<?php echo $form->error($model,'night_halt'); ?>
	</div-->

	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'parking_charge'); ?>
			<?php echo $form->textField($model,'parking_charge'); ?>
			<?php echo $form->error($model,'parking_charge'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'parking_charge_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'parking_charge_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->parking_charge_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'parking_charge_date'); ?>
		</div>
	</div>

	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'toll'); ?>
			<?php echo $form->textField($model,'toll'); ?>
			<?php echo $form->error($model,'toll'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'toll_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'toll_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->toll_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'toll_date'); ?>
		</div>
	</div>

   <div class="row">
		<?php echo $form->labelEx($model,'toll_description'); ?>
		<?php echo $form->textField($model,'toll_description'); ?>
		<?php echo $form->error($model,'toll_description'); ?>
	</div>

   <div class="row">
		<?php echo $form->labelEx($model,'toll_quantity'); ?>
		<?php echo $form->textField($model,'toll_quantity'); ?>
		<?php echo $form->error($model,'toll_quantity'); ?>
	</div>


	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'seat_Rent'); ?>
			<?php echo $form->textField($model,'seat_Rent'); ?>
			<?php echo $form->error($model,'seat_Rent'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'seat_rent_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'seat_rent_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->seat_rent_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'seat_rent_date'); ?>
		</div>
	</div>

	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'police_case'); ?>
			<?php echo $form->textField($model,'police_case',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'police_case'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'police_case_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'police_case_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->police_case_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'police_case_date'); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'breakfast'); ?>
			<?php echo $form->textField($model,'breakfast'); ?>
			<?php echo $form->error($model,'breakfast'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'breakfast_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'breakfast_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->breakfast_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'breakfast_date'); ?>
		</div>
	</div>
	
	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'lunchbreak'); ?>
			<?php echo $form->textField($model,'lunchbreak'); ?>
			<?php echo $form->error($model,'lunchbreak'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'lunchbreak_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'lunchbreak_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->lunchbreak_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'lunchbreak_date'); ?>
		</div>
	</div>

	<div class="row">
		<div class="fl" style="margin-right:50px;">
			<?php echo $form->labelEx($model,'others'); ?>
			<?php echo $form->textField($model,'others'); ?>
			<?php echo $form->error($model,'others'); ?>
		</div>
		<div class="fl">
			<?php echo $form->labelEx($model,'others_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'others_date',
		
				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
					'dateFormat'=>'yy-mm-dd',
					'defaultDate'=>$model->others_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
			));	?>
			<?php echo $form->error($model,'others_date'); ?>
		</div>
	</div>
	
	<div class="row">
        <?php echo $form->labelEx($model,'bill_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
			'model'=>$model,'attribute'=>'bill_date',
	
			// additional javascript options for the date picker plugin
			'options'=>array('autoSize'=>true,
				'dateFormat'=>'yy-mm-dd',
				'defaultDate'=>$model->bill_date,
				'changeYear'=>true,
				'changeMonth'=>true,
			),
		));	?>
        <?php echo $form->error($model,'bill_date'); ?>
    </div>
	
	<div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>
        <?php echo $form->textField($model,'driver_pin'); ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div>
	
	<!--<div class="row">
        <?php echo $form->labelEx($model,'total_cost'); ?>
        <?php echo $form->textField($model,'total_cost',array('readonly'=>true)); ?>
        <?php echo $form->error($model,'total_cost'); ?>
    </div>-->

	<!--div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit',
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->