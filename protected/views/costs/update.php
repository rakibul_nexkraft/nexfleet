<?php
/* @var $this CostsController */
/* @var $model Costs */

$this->breadcrumbs=array(
	'Costs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Costs', 'url'=>array('index')),
		array('label'=>'Create Costs', 'url'=>array('create')),
		array('label'=>'View Costs', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Costs', 'url'=>array('admin')),
	);
}
?>

<h4>Update Costs <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>