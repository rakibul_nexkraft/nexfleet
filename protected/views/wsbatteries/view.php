<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsbatteriesController */
/* @var $model Wsbatteries */

$this->breadcrumbs=array(
	'Wsbatteries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Batteries', 'url'=>array('index')),
	//array('label'=>'New Battery', 'url'=>array('create')),
	/*array('label'=>'New Battery', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/wsbatteries/create','id'=>$model->id),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
			return false;
		}",
	)))),
	*/
	array('label'=>'Update Battery', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Battery', $user_rights)),
	array('label'=>'Delete Battery', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Battery', $user_rights)),
	array('label'=>'Manage Batteries', 'url'=>array('admin'),'visible'=> in_array('Manage Batteries', $user_rights)),
);
?>

<h4>View Battery #<?php echo $model->id; ?></h4>

<?php 
/*
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
        'defect_id',
		'vehicle_reg_no',
		//'vehicle_type',
		array('name'=>'vehicle_type', 'value'=>$model->vehicletypes->type),
		'purchase_date',
		'requisition_no',
        'requisition_date',
		'supplier',
		'challan_no',
        'challan_date',
        'price',
        'discount',
		'net_amount',
		'warrenty_month',
		'warrenty_valid_upto',
		'status',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); 
*/
?>

<?php 

$this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'group2'=>array(
			'ItemColumns' => 2,
			'attributes' => array(
				'id',			array('name'=>'vehicle_type', 'value'=>$model->vehicletypes->type),
				'defect_id',	'vehicle_reg_no',	
				'purchase_date',	'requisition_no',
				'requisition_date',	'supplier',
				'particulars', 'battery_size',
				'challan_no', 'challan_date',
				'bill_no','bill_date',
				'quantity','unit_price',
				'price', 'discount',
				'warrenty_month', 'net_amount',
				'warrenty_valid_upto','status',
				
				'updated_by','updated_time',
				'created_by','created_time',
				'km_reading','battery_no',
				'remarks',		
			),
		),
	),
)); ?>


<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>