<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssnacksController */
/* @var $model Wssnacks */

$this->breadcrumbs=array(
	'Wssnacks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Snacks', 'url'=>array('index'),'visible'=> in_array('List Snacks', $user_rights)),
	array('label'=>'New Snack', 'url'=>array('create'),'visible'=> in_array('New Snack', $user_rights)),
	array('label'=>'View Snack', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Snack', $user_rights)),
	array('label'=>'Manage Snacks', 'url'=>array('admin'),'visible'=> in_array('Manage Snacks', $user_rights)),
);
?>

<h4>Update snack <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>