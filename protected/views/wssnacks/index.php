<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssnacksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wssnacks',
);

$this->menu=array(
	//array('label'=>'New snack', 'url'=>array('create')),
	array('label'=>'New Snack', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('/Wssnacks/create','id'=>$model->id),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal('show');
			$('.modal-body').html(data);
			return false;
		}",
	)),'visible'=> in_array('New Snack', $user_rights))),
	array('label'=>'Manage Snacks', 'url'=>array('admin'),'visible'=> in_array('Manage Snacks', $user_rights)),
    array('label'=>'Setting Bill Amount', 'url'=>array('wssnacksbiilamount/create'),'visible'=>(Yii::app()->user->username=='admin'),'visible'=> in_array('Snack Bill Setting', $user_rights)),
);
?>

<h4>snacks</h4>
<div class="search-form">
    <?php $this->renderPartial('_search',array(
        'model'=>$model,
    )); ?>
</div>
<div id="wss_container">
    <?php


    $var = $_REQUEST['Wssnacks'];

    if($var):
        ?>



        <h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
        <?php if($var['bill_date']) echo "User: ".$var['bill_date']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
        <?php if($var['vehicle_reg_no'])echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

        <?php

        if($var['active']==0);
        else if($var['active']==1)$active= "Not Approved";
        else if($var['active']==2)$active= "Approved";
        if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
    <?php endif;?>


    <?php $this->widget('bootstrap.widgets.TbGridView', array(
        'type'=>'striped bordered condensed',
        'id'=>'wssnacks-grid',
    //'dataProvider'=>$dataProvider,
        'dataProvider'=>$model->search(),
        'filter'=>$model,

        'columns'=>array(
        //'id',
            array(
                'name' => 'id',
                'type'=>'raw',
                'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
            ),
            'user_name',
            'user_pin',
            'bill_date',
            'bill_amount',
            'created_by',
            'created_time',

        ),
    )); ?>
    <?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Wssnacks'])));
    ?> &nbsp; &nbsp; &nbsp;





    <?php if(Yii::app()->user->hasFlash('success')): ?>

    <?php $this->beginWidget('bootstrap.widgets.TbModal', array(
       'id'=>'alert_msg',
       'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
       'options'=>array('backdrop'=>false),
       'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
   )); 
   ?>
   
   <div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
      <a class="close" data-dismiss="modal">&times;</a>
      <?php echo Yii::app()->user->getFlash('success'); ?>
  </div>
  
  <?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>



