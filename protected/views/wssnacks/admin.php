<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssnacksController */
/* @var $model Wssnacks */

$this->breadcrumbs=array(
	'Wssnacks'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Snacks', 'url'=>array('index'),'visible'=> in_array('List Snacks', $user_rights)),
	array('label'=>'New Snack', 'url'=>array('create'),'visible'=> in_array('New Snack', $user_rights)),
);

Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
		});
		$('.search-form form').submit(function(){
			$.fn.yiiGridView.update('wssnacks-grid', {
				data: $(this).serialize()
				});
				return false;
				});
				");
				?>

				<h4>Manage SNACKs</h4>

				<p>
					You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
					or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
				</p>

				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
				<div class="search-form" style="display:none">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div><!-- search-form -->

				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'wssnacks-grid',
					'dataProvider'=>$model->search(),
					'filter'=>$model,
					'columns'=>array(
						'id',
						'user_name',
						'user_pin',
						'designation',
						'bill_date',
						'bill_amount',
		/*
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
