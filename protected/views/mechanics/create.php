<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this MechanicsController */
/* @var $model Mechanics */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Mechanics', 'url'=>array('index'),'visible'=> in_array('List Mechanics', $user_rights)),
		array('label'=>'Manage Mechanics', 'url'=>array('admin'),'visible'=> in_array('Manage Mechanics', $user_rights)),
	);
}
?>

<h1>Create Mechanics</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>