<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this MechanicsController */
/* @var $model Mechanics */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Mechanics', 'url'=>array('index'),'visible'=> in_array('List Mechanics', $user_rights)),
		array('label'=>'Create Mechanics', 'url'=>array('create'),'visible'=> in_array('Create Mechanics', $user_rights)),
		array('label'=>'View Mechanics', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Mechanics', $user_rights)),
		array('label'=>'Manage Mechanics', 'url'=>array('admin'),'visible'=> in_array('Manage Mechanics', $user_rights)),
	);
}
?>

<h1>Update Mechanics <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>