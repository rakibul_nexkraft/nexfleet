<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this MechanicsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mechanics',
);
if(!Yii::app()->user->isViewUser()) {
  $this->menu=array(
  	array('label'=>'Create Mechanics', 'url'=>array('create'),'visible'=> in_array('Create Mechanics', $user_rights)),
  	array('label'=>'Manage Mechanics', 'url'=>array('admin'),'visible'=> in_array('Manage Mechanics', $user_rights)),
    array('label'=>'Mechanics Works', 'url'=>array('workList'),'visible'=> in_array('Mechanics Works', $user_rights)),
  );
}
?>

<h1>Mechanics Work List</h1>

<?php //$this->widget('zii.widgets.CListView', array(
	//'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',
//)); ?>
<?php /* $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
      'id'=>'defects-grid',
      'dataProvider'=>$dataProvider,
      //'filter'=>$model,

    'columns'=>array(
      array(
            'name' => 'mechanic_name',
            'type'  => 'raw',
            'value' => function($data, $row){
                if (!empty($data->mechanic_name)){
                    return ($data->mechanic_name.", ".$data->getAllMechanicsByDefectId($data->id));
                }else{
                    return $data->getAllMechanicsByDefectId($data->id);
                }
            },
        ),
      array(
            //'name' => 'defect_description',
            'name'=>'All Job Names',
            'type'  => 'raw',
            'value' => function($data, $row){
                if (!empty($data->defect_description)){
                    return ($data->defect_description.", ".$data->getAllDefectsByDefectId($data->id));
                }else{
                    return $data->getAllDefectsByDefectId($data->id);
                }
            },
        ),
       
        array(
            'name' => 'Defect Id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("defects/view","id"=>$data->id))',
        ),
               
        
        
        
        
       

    ),

  ));*/ 
  $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'defects-grid',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    
    'columns'=>array(      
      'mechanic_name',
      'job_name',
      
      array(
        'name' => 'defect_id',
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode($data-> defect_id),array("defects/view","id"=>$data->  defect_id))',
      ),
      
      
      
      
      
      

    ),

  ));?>
