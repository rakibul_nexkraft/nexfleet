<?php
/* @var $this MovementsreviseController */
/* @var $model Movementsrevise */

$this->breadcrumbs=array(
	'Movementsrevises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Movementsrevise', 'url'=>array('index')),
	array('label'=>'Create Movementsrevise', 'url'=>array('create')),
	array('label'=>'View Movementsrevise', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Movementsrevise', 'url'=>array('admin')),
);
?>

<h1>Update Movementsrevise <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>