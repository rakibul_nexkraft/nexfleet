<style>
    table.detail-view{
        background: #fff;
    }
</style>
<?php
/* @var $this MovementsreviseController */
/* @var $model Movementsrevise */

$this->breadcrumbs=array(
	'Movementsrevises'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Movementsrevise', 'url'=>array('index')),
	/*array('label'=>'Create Movementsrevise', 'url'=>array('create')),
	array('label'=>'Update Movementsrevise', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Movementsrevise', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Movementsrevise', 'url'=>array('admin')),*/
);
?>

<div>
    <div style="float: left">
        <h2>Logbook Id <a href="<?php echo Yii::app()->createUrl("movements/view", array('id'=>$model->movements_id));?>">#<?php echo $model->movements_id; ?></a></h2>
    </div>
    <div style="float: right">
        <?php
        if($model->revise_status == 0) {
            echo CHtml::ajaxButton(
                'Approve',
                Yii::app()->createUrl('movementsrevise/decline', array('id' => $model->id)),
                array(
                    'dataType' => 'json',
                ),
                array(
                    'class' => 'btn btn-primary',
                    'confirm' => 'Are you sure you want to delete this item?'
                ));
        }
        ?>
        <?php
        if($model->revise_status == 0) {
            echo CHtml::ajaxButton(
                'Decline',
                Yii::app()->createUrl('movementsrevise/decline', array('id' => $model->id)),
                array(
                    'dataType' => 'json',
                ),
                array(
                    'class' => 'btn btn-danger',
                    'confirm' => 'Are you sure you want to delete this item?'
                )
            );
        }
        ?>
    </div>
    <div class="clearfix"></div>
</div>

<div style="padding: 16px;background: white;box-shadow: 0 2px 4px -2px rgba(0,0,0,0.3);">
    <h4>Purpose of this edit:</h4> <?php echo $model->edit_summary?>
</div>
<div class="fl" style="width: 50%;padding: 10px;box-sizing: border-box;">
    <h3>After Changes</h3>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$model,
        'attributes'=>array(
            /*'id',*/
            'requisition_id',
            'user_pin',
            'user_name',
            'user_level',
            'user_dept',
            'email',
            'user_cell',
            'user_address',
            'vehicle_reg_no',
            'vehicletype_id',
            'vehicle_location',
            'driver_pin',
            'driver_name',
            'helper_pin',
            'helper_name',
            /*'helper_ot',*/
            'dutytype_id',
            'dutyday',
            'start_date',
            'end_date',
            'start_time',
            'end_time',
            'total_time',
            'start_point',
            'end_point',
            'start_meter',
            'end_meter',
            'total_run',
            'rph',
            'night_halt',
            'nh_amount',
            'morning_ta',
            'lunch_ta',
            'night_ta',
            'sup_mile_amount',
            'bill_amount',
            'total_ot_hour',
            'total_ot_amount',
            'used_mileage',
            /*'created_by',
            'created_time',
            'updated_by',
            'updated_time',
            'active',
            'trip_id',
            'digi_id',*/
        ),
    )); ?>
</div>
<div class="fl" style="width: 50%;padding: 10px;box-sizing: border-box;">
    <h3>Before Changes</h3>
    <?php $this->widget('zii.widgets.CDetailView', array(
        'data'=>$movements_model,
        'attributes'=>array(
            /*'id',*/
            'requisition_id',
            'user_pin',
            'user_name',
            'user_level',
            'user_dept',
            'email',
            'user_cell',
            'user_address',
            'vehicle_reg_no',
            'vehicletype_id',
            'vehicle_location',
            'driver_pin',
            'driver_name',
            'helper_pin',
            'helper_name',
            /*'helper_ot',*/
            'dutytype_id',
            'dutyday',
            'start_date',
            'end_date',
            'start_time',
            'end_time',
            'total_time',
            'start_point',
            'end_point',
            'start_meter',
            'end_meter',
            'total_run',
            'rph',
            'night_halt',
            'nh_amount',
            'morning_ta',
            'lunch_ta',
            'night_ta',
            'sup_mile_amount',
            'bill_amount',
            'total_ot_hour',
            'total_ot_amount',
            'used_mileage',
            /*'created_by',
            'created_time',
            'updated_by',
            'updated_time',
            'active',
            'trip_id',
            'digi_id',*/
        ),
    )); ?>
    <!--<h3>Before Changes</h3>
    < ?php
    $this->widget('zii.widgets.XDetailView', array(
        'data'=>$movements_model,
        'attributes'=>array(
            'group2'=>array(
                'ItemColumns' => 2,
                'attributes' => array(
                    'user_pin',			'start_date',
                    'user_name', 	'end_date',
                    'user_level', 'start_time',
                    'user_dept',	'end_time',
                    'email',		'start_point',
                    'user_cell',	'end_point',
                    'vehicle_reg_no',	'start_meter',
                    //array('name'=>'vehicletype_id', 'type'=>'raw', 'value'=>$Requisitions->vehicletypes->type), 'end_meter',
                    '', 'end_meter',
                    'vehicle_location',	'night_halt',
                    'driver_pin',	'morning_ta',

                    'driver_name',		'lunch_ta',
                    'helper_pin',		'night_ta',
                    'helper_name',		'',
//                        array('name'=>'dutytype_id', 'type'=>'raw','value'=>$Requisitions->dutytypes->type_name),			'',
                    'dutyday',		'',
                ),
            ),
        ),
    ));
    ?>-->
</div>
<div class="clearfix"></div>


<script>
    function confirmApprove() {
        if(confirm("Are you sure you want to approve the request?"))
            return true;
        else
            return false;
    }
    function confirmDecline() {
        if(confirm("Are you sure you want to decline the request?"))
            return true;
        else
            return false;
    }
</script>