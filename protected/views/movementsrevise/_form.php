<?php
/* @var $this MovementsreviseController */
/* @var $model Movementsrevise */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'movementsrevise-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
		<?php echo $form->error($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_level'); ?>
		<?php echo $form->textField($model,'user_level'); ?>
		<?php echo $form->error($model,'user_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_dept'); ?>
		<?php echo $form->textField($model,'user_dept',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'user_dept'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_cell'); ?>
		<?php echo $form->textField($model,'user_cell'); ?>
		<?php echo $form->error($model,'user_cell'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_address'); ?>
		<?php echo $form->textField($model,'user_address',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'user_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
		<?php echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'vehicletype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_location'); ?>
		<?php echo $form->textField($model,'vehicle_location',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'vehicle_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'driver_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'helper_pin'); ?>
		<?php echo $form->textField($model,'helper_pin'); ?>
		<?php echo $form->error($model,'helper_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'helper_name'); ?>
		<?php echo $form->textField($model,'helper_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'helper_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'helper_ot'); ?>
		<?php echo $form->textField($model,'helper_ot'); ?>
		<?php echo $form->error($model,'helper_ot'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dutytype_id'); ?>
		<?php echo $form->textField($model,'dutytype_id'); ?>
		<?php echo $form->error($model,'dutytype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dutyday'); ?>
		<?php echo $form->textField($model,'dutyday',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'dutyday'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
		<?php echo $form->error($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_date'); ?>
		<?php echo $form->textField($model,'end_date'); ?>
		<?php echo $form->error($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_time'); ?>
		<?php echo $form->textField($model,'start_time',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_time'); ?>
		<?php echo $form->textField($model,'end_time',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'end_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_time'); ?>
		<?php echo $form->textField($model,'total_time',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'total_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_point'); ?>
		<?php echo $form->textField($model,'start_point',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'start_point'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_point'); ?>
		<?php echo $form->textField($model,'end_point',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'end_point'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_meter'); ?>
		<?php echo $form->textField($model,'start_meter'); ?>
		<?php echo $form->error($model,'start_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_meter'); ?>
		<?php echo $form->textField($model,'end_meter'); ?>
		<?php echo $form->error($model,'end_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_run'); ?>
		<?php echo $form->textField($model,'total_run'); ?>
		<?php echo $form->error($model,'total_run'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rph'); ?>
		<?php echo $form->textField($model,'rph'); ?>
		<?php echo $form->error($model,'rph'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'night_halt'); ?>
		<?php echo $form->textField($model,'night_halt'); ?>
		<?php echo $form->error($model,'night_halt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nh_amount'); ?>
		<?php echo $form->textField($model,'nh_amount'); ?>
		<?php echo $form->error($model,'nh_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'morning_ta'); ?>
		<?php echo $form->textField($model,'morning_ta'); ?>
		<?php echo $form->error($model,'morning_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lunch_ta'); ?>
		<?php echo $form->textField($model,'lunch_ta'); ?>
		<?php echo $form->error($model,'lunch_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'night_ta'); ?>
		<?php echo $form->textField($model,'night_ta'); ?>
		<?php echo $form->error($model,'night_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sup_mile_amount'); ?>
		<?php echo $form->textField($model,'sup_mile_amount'); ?>
		<?php echo $form->error($model,'sup_mile_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bill_amount'); ?>
		<?php echo $form->textField($model,'bill_amount'); ?>
		<?php echo $form->error($model,'bill_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_ot_hour'); ?>
		<?php echo $form->textField($model,'total_ot_hour'); ?>
		<?php echo $form->error($model,'total_ot_hour'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_ot_amount'); ?>
		<?php echo $form->textField($model,'total_ot_amount'); ?>
		<?php echo $form->error($model,'total_ot_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'used_mileage'); ?>
		<?php echo $form->textField($model,'used_mileage'); ?>
		<?php echo $form->error($model,'used_mileage'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trip_id'); ?>
		<?php echo $form->textField($model,'trip_id',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'trip_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'digi_id'); ?>
		<?php echo $form->textField($model,'digi_id'); ?>
		<?php echo $form->error($model,'digi_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'movements_id'); ?>
		<?php echo $form->textField($model,'movements_id'); ?>
		<?php echo $form->error($model,'movements_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'edit_summary'); ?>
		<?php echo $form->textField($model,'edit_summary',array('size'=>60,'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'edit_summary'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->