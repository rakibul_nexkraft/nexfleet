<?php
/* @var $this MovementsreviseController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Movementsrevises',
);

$this->menu=array(
	/*array('label'=>'Create Movementsrevise', 'url'=>array('create')),
	array('label'=>'Manage Movementsrevise', 'url'=>array('admin')),*/
);
?>

<h1>Logbook Edit Request</h1>

<!--< ?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>-->
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'movementsrevise-grid',
    'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'columns'=>array(
        'id',
        'requisition_id',
        'user_pin',
        'user_name',
        'user_level',
        'user_dept',
        /*
        'email',
        'user_cell',
        'user_address',
        'vehicle_reg_no',
        'vehicletype_id',
        'vehicle_location',
        'driver_pin',
        'driver_name',
        'helper_pin',
        'helper_name',
        'helper_ot',*/
        'dutytype_id',
        'dutyday',
        'start_date',
        'end_date',
        'start_time',
        'end_time',/*
        'total_time',
        'start_point',
        'end_point',
        'start_meter',
        'end_meter',
        'total_run',
        'rph',
        'night_halt',
        'nh_amount',
        'morning_ta',
        'lunch_ta',
        'night_ta',
        'sup_mile_amount',
        'bill_amount',
        'total_ot_hour',
        'total_ot_amount',
        'used_mileage',
        'created_by',
        'created_time',
        'updated_by',
        'updated_time',
        'active',
        'trip_id',
        'digi_id',
        */
        array(
            'header'=>'Logbook Id',
            'type'=>'raw',
            'value'=>'$data->movements_id'
        ),
        'edit_summary',
        array
        (
            'class'=>'CButtonColumn',
            'template'=>'{view}',
        )
    ),
)); ?>

