<?php
/* @var $this MovementsreviseController */
/* @var $model Movementsrevise */

$this->breadcrumbs=array(
	'Movementsrevises'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Movementsrevise', 'url'=>array('index')),
	array('label'=>'Manage Movementsrevise', 'url'=>array('admin')),
);
?>

<h1>Logbook Edit Request</h1>

<div style="padding: 16px;background: white;box-shadow: 0 2px 4px -2px rgba(0,0,0,0.3);">
    <h3 style="color: #ff4b4b;">Attention!!!</h3>
    <h6>Your edit request will be sent for admin approval. Please explain your reason to edit and submit</h6>
</div>

<?php echo $this->renderPartial('_create_form', array('model'=>$model)); ?>