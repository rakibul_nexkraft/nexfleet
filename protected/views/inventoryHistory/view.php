<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>

<?php

$this->breadcrumbs=array(
	'Inventory Histories'=>array('index'),
	$model->id,
);

?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Inventory History #<?php echo $model->id; ?></h3>
    </div>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'invoice_id',
		'inventory_id',
		'product_attributes_id',
		'product_classification_id',
		'unit',
		'base_quantity',
		'quantiy',
		'unit_price',
		'total_amount',
		'manufacture_id',
		'supplier_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
