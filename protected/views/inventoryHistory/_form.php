<style>
	.form #inventory-history-form{
		margin-bottom: unset;
	}
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inventory-history-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Inventory History</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'invoice_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'invoice_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'invoice_id'); ?>

		<?php echo $form->labelEx($model,'inventory_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'inventory_id',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'inventory_id'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'product_attributes_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'product_attributes_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'product_attributes_id'); ?>

		<?php echo $form->labelEx($model,'product_classification_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'product_classification_id',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'product_classification_id'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'unit', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'unit',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'unit'); ?>

		<?php echo $form->labelEx($model,'base_quantity', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'base_quantity',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'base_quantity'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'quantiy', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'quantiy',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'quantiy'); ?>

		<?php echo $form->labelEx($model,'unit_price', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'unit_price',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'unit_price'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'total_amount', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'total_amount',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'total_amount'); ?>

		<?php echo $form->labelEx($model,'manufacture_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'manufacture_id',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'manufacture_id'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'supplier_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'supplier_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'supplier_id'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->