<style>
	.detail-view{
		width: 100% !important;
	}
</style>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3 class="modal-title"><?php if($type==1) { echo ('Update Supplier')." ".$model->id; }
			if($type==2) {echo ('View Supplier')." ".$model->id;} ?></h3>
		</div>
		<div class="modal-body">
			<div class="well" style="margin-bottom: 0px;">
				<?php if($type==1) {
					?>
						<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row input-field">
		<?php echo $form->textField($model,'supplier_name',array('size'=>60,'maxlength'=>150, 'placeholder'=> '* Name')); ?>
		<?php echo $form->error($model,'supplier_name'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->textField($model,'supplier_address',array('size'=>60,'maxlength'=>300, 'placeholder'=> '* Address')); ?>
		<?php echo $form->error($model,'supplier_address'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->labelEx($model,'cell_no'); ?>
		<?php echo $form->textField($model,'cell_no',array('size'=>60,'maxlength'=>150, 'placeholder'=> '* Mobile No.')); ?>
		<?php echo $form->error($model,'cell_no'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->dropDownList($model, 'status', array('0'=>'Inactive','1'=>'Active')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>

						</div>
					</div><!-- form -->

					<?php $this->endWidget(); ?>
				<?php }
				
				
				if($type==2) {
					
					$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'supplier_name',
		'supplier_address',
		'cell_no',
		'status',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
));
				}

				?>

			</div>
		</div>

		<?php if($type==2) {
			?>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
			</div>
		<?php }else{?>

		<?php }?>
	</div>
</div>