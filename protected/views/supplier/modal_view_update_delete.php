<style>
	.detail-view {
		width: 100% !important;
		margin-bottom: unset;
	}

	.well form {
		margin-bottom: unset;
	}

	#user-form textarea {
		border: 1px solid #5a5a5a2e;
	}
</style>

<script>
	function placeOrder() {
		document.theForm.submit()
	}
</script>
<?php if(($type == 2) || ($type ==1)){?>
	<div class="modal-dialog" style="width: 715px; margin: 65px auto;">
	<?php }else{?>
		<div class="modal-dialog" style="margin: 206px 500px;">
	<?php }?>
	<?php if (($type == 2) || ($type ==1)) { ?>
		<div class="modal-content" style="width: 850px;">
		<?php } else { ?>
			<div class="modal-content">
			<?php } ?>
			<div class="modal-header modal-update">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title"><?php if ($type == 1) {
					echo ('') . " " . $model->supplier_name;
				}
				if ($type == 2) {
					echo ('Update') . " " . $model->supplier_name;
				}
				if ($type == 3) {
					echo ('Delete') . " " . $model->supplier_name;
				} ?></h3>
			</div>
			<div class="modal-body">
				<div class="well" style="padding: unset; margin-bottom: 0px;background-color: unset;
				border: unset;box-shadow: none;">
				<?php if ($type == 3) {
					?>
					<div class="modal-body" style="text-align: center;font-size: x-large;">
						Are you sure you want to delete this record?
					</div>

				<?php }
				if ($type == 1) { 

					$this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'supplier_name',
		'supplier_address',
		'cell_no',
		array('name'=>'status',
			'type'=>'raw',
			'value'=>$model->active($model->status),
		),
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
));
					?>

					

<?php }

if ($type == 2) {
	?>
	<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'supplier_name', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'supplier_name',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'supplier_name'); ?>

		<?php echo $form->labelEx($model,'supplier_address', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'supplier_address',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'supplier_address'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'cell_no', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'cell_no',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'cell_no'); ?>

		<?php echo $form->labelEx($model,'status', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->dropDownList($model, 'status', array('0'=>'Inactive','1'=>'Active'), array('class' => 'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<?php } ?>

<?php if ($type == 1) {
	?>
						<!-- <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
			
			
							<?php #echo CHtml::button('Submit',array('onclick'=>'placeOrder()'));
								?>
							<?php #echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); 
								?>
							<?php #$this->endWidget(); 
								?>
			
							</div> -->

						<?php }
						if ($type == 1) { ?>

							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
							</div>
						<?php }
						if ($type == 3) { ?>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">No</button>
								<?php echo CHtml::link('Yes', array('supplierDelete', 'id' => $model->id), array(
									'class' => 'btn btn-secondary button-pink',
									'style' => 'color:white !important;'
								)); ?>
								<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="()">Yes</button> -->
							</div>
						<?php } ?>
					</div>
				</div>