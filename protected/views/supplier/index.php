<?php
/* @var $this SupplierController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Suppliers',
);

$this->menu=array(
	array('label'=>'Create Supplier', 'url'=>array('create')),
	array('label'=>'Manage Suppliers', 'url'=>array('admin')),
);
?>

<?php
echo CHtml::link('Create Supplier', array("/supplier/create"), array('class' => 'button button-pink button-mini button-rounded', 'style' => "color: white !important;float: right;"));
?>

<h4>Suppliers</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'supplier-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id',
		'supplier_name',
		'supplier_address',
		'cell_no',
		'status',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
