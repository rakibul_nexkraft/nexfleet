<?php
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


	<div class="fl">
    <div class="row">
		<?php echo $form->label($model,'id'); ?>
        <div class="clearfix"></div>
		<?php echo $form->textField($model,'id'); ?>
	</div>
    </div>

	<div class="fl">
    <div class="row">
		<?php echo $form->label($model,'defect_id'); ?>
        <div class="clearfix"></div>
		<?php echo $form->textField($model,'defect_id'); ?>
	</div>
    </div>
    
    		<div class="fl">
    <div class="row">
		<?php echo $form->label($model,'service_provider'); ?>
        <div class="clearfix"></div>
		<?php echo $form->textField($model,'service_provider'); ?>
	</div>
	</div>

	<!--

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>
-->
  <div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div><br/>

<?php $this->endWidget(); ?>

</div>

<!-- search-form -->

