<?php
/* @var $this TasksController */
/* @var $model Tasks */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'tasks-form',
    'enableAjaxValidation'=>false,
)); ?>





<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>


<div class="row">
    <?php echo $form->labelEx($model,'defect_id'); ?>
    <?php echo $form->textField($model,'defect_id',array('size'=>60,'maxlength'=>127,'readonly'=>true)); ?>
    <?php /* echo $form->textField($model, 'defect_id',
        array('onblur'=>CHtml::ajax(array('type'=>'GET',
                'dataType'=>'json',

                'url'=>array("tasks/getVehicleData"),

                'success'=>"js:function(string){

					if(!string)
					{

						alert('Requisition ID not found or not approved');
							return;
							}

							$('#Tasks_vehicle_reg_no').val(string.vehicle_reg_no);

									  }"

            ))),
        array('empty' => 'Select one of the following...')

    ); */
    ?>

    <?php echo $form->error($model,'defect_id'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
    <?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
    <?php echo $form->error($model,'vehicle_reg_no'); ?>
</div>

<?php

if(isset($_REQUEST['type']))
     $type = $_REQUEST['type'];
else $type = $model->work_type;
   
?>
<div class="row">
    <?php echo $form->labelEx($model,'work_type'); ?>
    <?php echo $form->textField($model,'work_type',array('size'=>30,'maxlength'=>30,'readonly'=>true)); ?>
    <?php //echo $form->dropDownList($model, 'work_type', $model->work_type, array('readonly'=>true,'empty' => 'Select a work type...','style'=>'width:168px;')); ?>

    <?php echo $form->error($model,'work_type'); ?>
</div>

<?php if ($type == "inside" ){ ?>
    <div class="row">
        <?php  echo $form->labelEx($model,'service_name'); ?>
        <?php

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'attribute' => 'service_name',
            'source'=>$this->createUrl('wsservices/getServiceName'),
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
      $('#Tasks_wsservice_id').val(ui.item['wsservice_id']);         
         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));		?>


        <?php  echo $form->error($model,'service_name'); ?>
    </div>



    <div class="row">
        <?php //echo $form->labelEx($model,'wsservice_id'); ?>
        <?php //echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
        <?php  echo $form->hiddenField($model,'wsservice_id',array('size'=>60,'maxlength'=>127,'type'=>"hidden")); ?>
        <?php //echo $form->error($model,'wsservice_id'); ?>
    </div>
	    <div class="row">

        <?php echo $form->labelEx($model,'Delivery Date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'bill_date',
            'id'=>'defects_apply_date',
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->bill_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:206px;'
            ),
        )); ?>
        <?php echo $form->error($model,'Delivery Date'); ?>
    </div>
<?php } ?>


    <div class="row">
        <?php  echo $form->labelEx($model,'service_name'); ?>
        <?php echo $form->textField($model,'service_name',array('size'=>60,'maxlength'=>127)); ?>
        <?php /*

        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'model'=>$model,
            'readonly'=>true,
            'attribute' => 'service_name',
            'source'=>$this->createUrl('wsoutservices/getServiceNameOut'),
            // additional javascript options for the autocomplete plugin
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {
      $('#Tasks_wsservice_id').val(ui.item['wsservice_id']);
         }"
            ),
            'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),
        ));	*/	?>


        <?php  echo $form->error($model,'service_name'); ?>
    </div>

    <div class="row">
        <?php  echo $form->hiddenField($model,'wsservice_id',array('size'=>60,'maxlength'=>127,'type'=>"hidden")); ?>
    </div>

<?php 

if ($model->work_type != "inside" ){ ?>
    <div class="row">
        <?php  echo $form->labelEx($model,'service_provider'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
            'id'=>'wssupplier_name_ac',
            'model'=>$model,
            'attribute' => 'service_provider',
            'source'=>$this->createUrl('wssupplier/getSupplierName'),
            'options'=>array(
                'minLength'=>'2',
                'select'=>"js: function(event, ui) {

		         $('#Tasks_wssupplier_id').val(ui.item['id']);
         }"
            ),
            /*'htmlOptions'=>array(
                'style'=>'height:20px;'
            ),*/
        ));		?>
        <?php  echo $form->error($model,'service_provider'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'bill_no'); ?>
        <?php echo $form->textField($model,'bill_no',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'bill_no'); ?>
    </div>

    <div class="row">

        <?php echo $form->labelEx($model,'bill_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'bill_date',
            'id'=>'defects_apply_date',
            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->bill_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:206px;'
            ),
        )); ?>
        <?php echo $form->error($model,'bill_date'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'quantity'); ?>
        <?php echo $form->textField($model,'quantity',array('size'=>60,'maxlength'=>127)); ?>
        <?php echo $form->error($model,'quantity'); ?>
    </div>



        <div class="row">
            <?php echo $form->labelEx($model,'out_bill_amount'); ?>
            <?php echo $form->textField($model,'out_bill_amount',array('size'=>60,'maxlength'=>127,'readonly'=>true)); ?>
            <?php echo $form->error($model,'out_bill_amount'); ?>
        </div>
<?php } ?>
<div class="row">
    <?php echo $form->labelEx($model,'remarks'); ?>
    <?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
    <?php echo $form->error($model,'remarks'); ?>
</div>


<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit',
        'size' => 'medium'
    ));
    ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->