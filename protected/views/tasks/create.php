<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'List Tasks', 'url'=>array('index')),
    	array('label'=>'Manage Tasks', 'url'=>array('admin')),
    );
}
?>

    <?php if ($_REQUEST['type'] == "outside"){ ?>
    <h4 style="color: red">Outside Task</h4> <?php } ?>
    <?php if ($_REQUEST['type'] == "inside"){ ?>
    <h4 style="color: darkgreen">Inside Task</h4><?php } ?>
    <?php if ($_REQUEST['type'] == "other"){ ?>
    <h4 style="color: darkblue">Other Task</h4><?php } ?>



<?php echo $this->renderPartial('_form', array('model'=>$model,'work_type'=>$work_type)); ?>