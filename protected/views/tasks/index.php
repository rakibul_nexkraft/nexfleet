<?php
/* @var $this TasksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tasks',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	//array('label'=>'New Task', 'url'=>array('create')),
    	array('label'=>'Manage Tasks', 'url'=>array('admin')),
    );
}
?>

<h4>Tasks</h4>

<div class="search-form" style="">
    <?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	  'type'=>'striped bordered condensed',
      'id'=>'defects-grid',
      //'dataProvider'=>$dataProvider,
      'dataProvider'=>$model->search(),
      'filter'=>$model,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'vehicle_reg_no',
				'service_name',
        'service_provider',
        'defect_id',
        'bill_date',
        'bill_amount',
        'work_type',
        'created_by',
        'created_time',
        'updated_by',
        'updated_time',      
    ),
)); ?>

