<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Tasks', 'url'=>array('index')),
		array('label'=>'New Task', 'url'=>array('create')),
		array('label'=>'View Tasks', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Tasks', 'url'=>array('admin')),
	);
}
?>

<h4>Update Tasks <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_formU', array('model'=>$model,'work_type'=>$work_type)); ?>