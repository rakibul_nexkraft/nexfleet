<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Tasks', 'url'=>array('index')),
		array('label'=>'New Task', 'url'=>array('create')),
		array('label'=>'Update Tasks', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete Tasks', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	    array('label'=>'Delete Task', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),'confirm'=>'Are you sure you want to delete this item?'),'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken)),
		array('label'=>'Manage Tasks', 'url'=>array('admin')),
		//array('label'=>'Pick Item', 'url'=>array('wsitemdists/create','id'=>$model->id)),
	);
}
?>

<h4>View Tasks #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'defect_id',
        'vehicle_reg_no',
        'work_type',
        'service_name',
        'service_provider',
        'bill_date',
        'bill_amount',
        'remarks',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
<br />
<h5>Go to the Defect:
    <?php echo CHtml::link($model->defect_id, $this->createAbsoluteUrl('defects/view',array('id'=>$model->defect_id))); ?>
</h5>