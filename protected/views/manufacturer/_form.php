<style>
	.form #manufacturer-form{
		margin-bottom: unset;
	}
</style>

<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manufacturer-form',
	'enableAjaxValidation'=>false,
   'htmlOptions'=>array('onsubmit'=>"return false")
)); ?>
	
	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Manufacturer</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'name', array(
			'class' => 'col-md-3 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-md-6" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'name'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color','onclick'=>'submitManu()')); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script>

        function submitManu(){
        $("#manufacturer-form").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/manufacturer/create");?>', 
            type: 'post',
            success:  function(data) {

            		if(isNaN(data)){
            			bsModalOpen(data);
            		}else{
            			window.location.href ="view/"+data;
            		}     
            }
        });
    }
    </script>