<?php
/* @var $this ManufacturerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manufacturers',
);

$this->menu=array(
	array('label'=>'Create Manufacturer', 'url'=>array('create')),
	array('label'=>'Manage Manufacturers', 'url'=>array('admin')),
);
?>

<?php
echo CHtml::link('Create Manufacturer', array("/manufacturer/create"), array('class' => 'button button-pink button-mini button-rounded', 'style' => "color: white !important;float: right;"));
?>

<h4>Manufacturers</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manufacturer-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
