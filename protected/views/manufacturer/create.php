<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */

$this->breadcrumbs=array(
	'Manufacturers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List of Manufacturer', 'url'=>array('index')),
	array('label'=>'Manage Manufacturers', 'url'=>array('admin')),
);
?>

<h4>Create Manufacturer</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>