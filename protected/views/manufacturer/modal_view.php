<style>
	.detail-view{
		width: 100% !important;
	}
</style>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3 class="modal-title"><?php if($type==1) {echo  ('Update Manufacturer')." ".$model->id; }
			if($type==2) {echo ('View Manufacturer')." ".$model->id;} ?></h3>
		</div>
		<div class="modal-body">
			<div class="well">
				<?php if($type==1) {					
					?>
					<div class="form">

						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'manufacturer-form',
							'enableAjaxValidation'=>false,
						)); ?>

						<p class="note">Fields with <span class="required">*</span> are required.</p>

						<div class="row input-field">
							<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>300, 'placeholder'=> '* Name')); ?>
							<?php echo $form->error($model,'name'); ?>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
							
							
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
						</div>

					</div><!-- form -->
					<?php $this->endWidget(); ?>			
				<?php }
				
				
				if($type==2) {
					
					$this->widget('zii.widgets.CDetailView', array(
						'data'=>$model,
						'attributes'=>array(
							'id',
							'name',
							'created_by',
							'created_time',
							'updated_by',
							'updated_time',
						),
					));
				}

				?>

			</div>
		</div>

		<?php if($type==2) {
			?>
				<!-- <div class="modal-footer">
							<a href="#" class="btn btn white" data-dismiss="modal">Close</a>
						</div> -->
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
						</div>
					<?php }else{?>
						
					<?php } ?>
				</div>
			</div>
