<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemsController */
/* @var $model Wsitems */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	$model->id,
);
if(Yii::app()->user->username!='185036') {
$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index'),'visible'=> in_array('Stock-In List Items', $user_rights)),
	array('label'=>'New Item', 'url'=>array('create'),'visible'=> in_array('Stock-In New Item', $user_rights)),
	array('label'=>'Update Item', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Stock-In Update Item', $user_rights)),
	//array('label'=>'Delete Item', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Delete Item', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),'confirm'=>'Are you sure you want to delete this item?'),'params'=> array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken),'visible'=> in_array('Stock-In Delete Item', $user_rights)),
	array('label'=>'Manage Items', 'url'=>array('admin'),'visible'=> in_array('Stock-In Manage Items', $user_rights)),
);
}
?>

<h4>View Item #<?php echo $model->id; ?></h4>

<?php 
/*
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_name',
		'purchase_type',
		'purchase_date',
		'item_size',
		'wssupplier_id',
		'wssupplier_name',
		'item_slno',
		'bill_amount',
		'bill_date',
		'quantity',
		'warranty',
		'unit_price',
		'total_price',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); 
*/
?>



<?php $this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
			'id',				'item_size',
			'item_name',		'warranty',
			'purchase_type',	'quantity',
			'purchase_date',	'unit_price',
			'vehicle_model',		'total_price',
			'wssupplier_id', 'bill_date', 'wssupplier_name','bill_no','challan_date','vehicle_code',
			'challan_no',	'bill_amount',

                'wsrequisition_requisition_no',	'parts_no',
                'requisition_date',  'remarks',
                'created_by',		'updated_time',
                'created_time',	'updated_by',
            ),
		),
    ),
)); 
