<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemsController */
/* @var $model Wsitems */

$this->breadcrumbs=array(
	'Items'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Items', 'url'=>array('index'),'visible'=> in_array('Stock-In List Items', $user_rights)),
	array('label'=>'New Item', 'url'=>array('create'),'visible'=> in_array('Stock-In New Item', $user_rights)),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('wsitems-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Items</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'id'=>'wsitems-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'item_name',
        'wsrequisition_requisition_no',
        'vehicle_model',
        'parts_no',
        'vehicle_reg_no',
		'purchase_type',
		'bill_date',
        'quantity',
        'unit_price',
        'total_price',
        'item_size',
		'wssupplier_name',
		/*
		'wssupplier_name',
		'vehicle_reg_no',
		'item_slno',
		'bill_amount',
		'bill_date',
		'quantity',
		'warranty',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			 'template'=>'{view} {update} {delete}',
			 'buttons'=>array
            (
            	'delete'=>array(
            		'visible'=>'Yii::app()->user->username=="admin"',
            	),
            ),
		),
	),
)); ?>
