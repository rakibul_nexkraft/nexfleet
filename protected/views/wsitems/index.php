<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Items',
);
if(Yii::app()->user->username!='185036') {
    $this->menu=array(
       array('label'=>'New Item', 'url'=>'#','visible'=> in_array('Stock-In New Item', $user_rights), 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
          'type'=>'GET',
          'url'=>array('wsitems/create'),
          'datatype'=>'html',
          'success'=>"function(data){
             $('#popModal').modal('show');
             $('.modal-body').html(data);
             $('#popModal').css('z-index','1050');
             $('.modal-backdrop').css('z-index','1000');
             return false;
         }",
     )))),
       array('label'=>'New Item', 'url'=>array('create'),'visible'=> in_array('Stock-In New Item', $user_rights)),
       array('label'=>'Manage Items', 'url'=>array('admin'),'visible'=> in_array('Stock-In Manage Items', $user_rights)),
   );
}
?>

<h4>Items</h4>
<?php if($model->hasErrors()){ $model_json_error=json_encode($model->errors); $model_json_attributes=json_encode($model->attributes);?>
    <script type="text/javascript">
       $.post('<?php echo Yii::app()->createAbsoluteUrl("wsitems/create");?>',{erro:'<?php echo $model_json_error;?>',attr:'<?php echo $model_json_attributes;?>'},function(data){$('#popModal').modal('show');
        $('.modal-body').html(data);
        $('#popModal').css("z-index","999");
        $('.modal-backdrop').css("z-index","990");
        return false;});
    </script>

    <?php $model->unsetAttributes();} ?>

    <div class="search-form">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
    </div>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php /*if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
	)); 
?>
	
	<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
		<a class="close" data-dismiss="modal">&times;</a>
		<?php echo Yii::app()->user->getFlash('success'); ?>
	</div>
	
<?php $this->endWidget();
endif; */
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
 'type'=>'striped bordered condensed',
 'id'=>'wsitems-grid',
      //'dataProvider'=>$dataProvider,
 'dataProvider'=>$model->search(),
     // 'filter'=>$model,

 'columns'=>array(
        //'id',
    array(
        'name' => 'id',
        'type'=>'raw',
        'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
    ),
    'item_name',
    'wsrequisition_requisition_no',
    'requisition_date',
    'vehicle_reg_no',
    'vehicle_model',
    'parts_no',
    'purchase_type',
    'bill_date',
    'bill_no',
		//'purchase_date',
    'item_size',
    'quantity',
    'unit_price',
    'total_price',
    'wssupplier_name',
    'challan_no'
    
),
)); ?>

<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Wsitems'])));
?> &nbsp; &nbsp; &nbsp;

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
    'id'=>'alert_msg',
    'autoOpen'=>true,
        //'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
    'htmlOptions'=>array('style'=>'top:80%'),
    'options'=>array('backdrop'=>false),
    'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
));
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
    <a class="close" data-dismiss="modal">&times;</a>
    <?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();?>
<script type="text/javascript">
   $.post('<?php echo Yii::app()->createAbsoluteUrl("wsitems/create");?>',{id:'<?php echo $_GET['id'];?>'},function(data){$('#popModal').modal('show');
    $('.modal-body').html(data);
    $('#popModal').css("z-index","999");
    $('.modal-backdrop').css("z-index","990");
    return false;});
</script>

<?php  endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:82%; height:70%; padding:1px; vertical-align:middle; left:25% !important;'),
)); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <br />
</div>

<div class="modal-body">
</div>

<?php $this->endWidget(); ?>