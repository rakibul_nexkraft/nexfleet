<?php
/* @var $this MiscellaneousAccidentController */
/* @var $model MiscellaneousAccident */

$this->breadcrumbs=array(
	'Miscellaneous Incidence'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Miscellaneous Incidence', 'url'=>array('index')),
		array('label'=>'Create Miscellaneous Incidence', 'url'=>array('create')),
	);
}

?>

<h1>Manage Miscellaneous Incidence</h1>



<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//));
 ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'miscellaneous-accident-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'driver_pin',
		'vehicle_reg_no',
		'detail',
		'date',
		/*'created_by',
		
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
