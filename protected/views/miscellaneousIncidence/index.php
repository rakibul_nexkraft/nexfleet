<?php
/* @var $this MiscellaneousAccidentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Miscellaneous Incidence',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Miscellaneous Incidence', 'url'=>array('create')),
		array('label'=>'Manage Miscellaneous Incidence', 'url'=>array('admin')),
	);
}
?>

<h1>Miscellaneous Incidence</h1>
<div class="search-form">
	<div class="wide form">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'driver_pin'); ?> 
				<?php echo $form->textField($model,'driver_pin'); ?>
			</div>
		</div>

		
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'date_from'); ?> 
				<?php //echo $form->textField($model,'training_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'date_from',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->date_from,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'date_to'); ?> 
				<?php //echo $form->textField($model,'training_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'date_to',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->date_to,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
			</div>
		</div>

			<div class="clearfix"></div>
		    
		    <div align="left">
		    <?php $this->widget('bootstrap.widgets.TbButton',array(
		        'label' => 'Search',
		        'type' => 'primary',
		        'buttonType'=>'submit', 
		        'size' => 'medium'
		        ));
		    ?>
		        
		    </div>

		<?php $this->endWidget(); ?>
	</div>

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'miscellaneous-accident-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		//'id',
		'driver_pin',
		array(
            'name' => 'name',
            'type'=>'raw',
            'value' => 'DriverTraining::model()->driverName($data->driver_pin)',
            
        ),
		array(
            'name' => 'phone',
            'type'=>'raw',
            'value' => 'DriverTraining::model()->driverPhone($data->driver_pin)',
            
        ),
		
		'vehicle_reg_no',
		'detail',
		'date',
		/*'created_by',
		
		'created_time',
		'updated_by',
		'updated_time',
		*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}',	
			'buttons'=>array(
				'update' => array( 
				'visible'=>'!Yii::app()->user->isViewUser()',
				),
			),		
		),
	),
)); 

echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['MiscellaneousIncidence'])));
?>
