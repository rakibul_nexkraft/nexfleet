<?php
/* @var $this MiscellaneousAccidentController */
/* @var $model MiscellaneousAccident */

$this->breadcrumbs=array(
	'Miscellaneous Accidents'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Miscellaneous Incidence', 'url'=>array('index')),
		array('label'=>'Manage Miscellaneous Incidence', 'url'=>array('admin')),
	);
}
?>

<h1>Create Miscellaneous Incidence</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>