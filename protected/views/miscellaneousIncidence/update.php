<?php
/* @var $this MiscellaneousAccidentController */
/* @var $model MiscellaneousAccident */

$this->breadcrumbs=array(
	'Miscellaneous Incidence'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Miscellaneous Incidence', 'url'=>array('index')),
		array('label'=>'Create Miscellaneous Incidence', 'url'=>array('create')),
		array('label'=>'View Miscellaneous Incidence', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Miscellaneous Incidence', 'url'=>array('admin')),
	);
}
?>

<h1>Update Miscellaneous Incidence <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>