<?php
/* @var $this MiscellaneousAccidentController */
/* @var $model MiscellaneousAccident */

$this->breadcrumbs=array(
	'Miscellaneous Incidence'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Miscellaneous Incidence', 'url'=>array('index')),
		array('label'=>'Create Miscellaneous Incidence', 'url'=>array('create')),
		array('label'=>'Update Miscellaneous Incidence', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Miscellaneous Incidence', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Miscellaneous Incidence', 'url'=>array('admin')),
	);
}
?>

<h1>View MiscellaneousAccident #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'driver_pin',
		'vehicle_reg_no',
		'detail',
		'date',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
