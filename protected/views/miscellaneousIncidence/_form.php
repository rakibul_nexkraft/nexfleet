<?php
/* @var $this MiscellaneousAccidentController */
/* @var $model MiscellaneousAccident */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'miscellaneous-incidence-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	
	<div class="row">
        <?php echo $form->labelEx($model,'driver_pin'); ?>
                                <?php echo $form->textField($model, 'driver_pin',       
        array('onblur'=>CHtml::ajax(array('type'=>'GET', 
                 'dataType'=>'json',

            'url'=>array("miscellaneousIncidence/getName"),
    
            'success'=>"js:function(string){            
                            
                                          
                        $('#MiscellaneousIncidence_name').val(string);
                       
             }",
             'error'=>"js:function(string){  
             alert(string);
             }"

            ))),
            array('empty' => 'Select one of the following...')
        
        );
         ?>
        <?php echo $form->error($model,'driver_pin'); ?>
    </div>
    <?php if($model->isNewRecord){ ?> 
      <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div> 	
    <?php }
        else{
            $driver_sql = "SELECT * FROM tbl_drivers where  pin = '$model->driver_pin'";
            $driver = Yii::app()->db->createCommand($driver_sql)->queryRow();
            $model->name=$driver['name'];
    ?>
     <div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php echo $form->textField($model,'name'); ?>
        <?php echo $form->error($model,'name'); ?>
    </div> 

    <?php
        }
    ?>

		<div class="row">
		<?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php

        $this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'vehicle_reg_no',
            'data'=>Requisitions::model()->getAllVehicle(),
            'options'  => array(
                'style'=>'width:100%',
                'allowClear'=>true,
                'placeholder'=>'Select Vehicle Reg No',
                'minimumInputLength' => 1,
            ),
        ));
		?>
		
		
		<?php  echo $form->error($model,'vehicle_reg_no'); ?>
	</div> 


	<div class="row">
		<?php echo $form->labelEx($model,'detail'); ?>
		<?php echo $form->textArea($model,'detail',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'detail'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>
	    

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->