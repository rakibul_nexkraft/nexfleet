<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List Digital Log Book', 'url'=>array('index')),
		array('label'=>'Create Digital Log Book', 'url'=>array('create')),
		array('label'=>'View Digital Log Book', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Digital Log Book', 'url'=>array('admin')),
	);
}
?>

<h1>Update Digital Log Book <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>