<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_level'); ?>
		<?php echo $form->textField($model,'user_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_dept'); ?>
		<?php echo $form->textField($model,'user_dept',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_cell'); ?>
		<?php echo $form->textField($model,'user_cell'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_address'); ?>
		<?php echo $form->textField($model,'user_address',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicletype_id'); ?>
		<?php echo $form->textField($model,'vehicletype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_location'); ?>
		<?php echo $form->textField($model,'vehicle_location',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'helper_pin'); ?>
		<?php echo $form->textField($model,'helper_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'helper_name'); ?>
		<?php echo $form->textField($model,'helper_name',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'helper_ot'); ?>
		<?php echo $form->textField($model,'helper_ot'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dutytype_id'); ?>
		<?php echo $form->textField($model,'dutytype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dutyday'); ?>
		<?php echo $form->textField($model,'dutyday',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_date'); ?>
		<?php echo $form->textField($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_time'); ?>
		<?php echo $form->textField($model,'start_time',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_time'); ?>
		<?php echo $form->textField($model,'end_time',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_time'); ?>
		<?php echo $form->textField($model,'total_time',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_point'); ?>
		<?php echo $form->textField($model,'start_point',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_point'); ?>
		<?php echo $form->textField($model,'end_point',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_meter'); ?>
		<?php echo $form->textField($model,'start_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_meter'); ?>
		<?php echo $form->textField($model,'end_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_run'); ?>
		<?php echo $form->textField($model,'total_run'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'rph'); ?>
		<?php echo $form->textField($model,'rph'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'night_halt'); ?>
		<?php echo $form->textField($model,'night_halt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nh_amount'); ?>
		<?php echo $form->textField($model,'nh_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'morning_ta'); ?>
		<?php echo $form->textField($model,'morning_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lunch_ta'); ?>
		<?php echo $form->textField($model,'lunch_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'night_ta'); ?>
		<?php echo $form->textField($model,'night_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sup_mile_amount'); ?>
		<?php echo $form->textField($model,'sup_mile_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_amount'); ?>
		<?php echo $form->textField($model,'bill_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_ot_hour'); ?>
		<?php echo $form->textField($model,'total_ot_hour'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_ot_amount'); ?>
		<?php echo $form->textField($model,'total_ot_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'used_mileage'); ?>
		<?php echo $form->textField($model,'used_mileage'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trip_id'); ?>
		<?php echo $form->textField($model,'trip_id',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->