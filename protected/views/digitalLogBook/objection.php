<style>
    table, th, td {
        border: 1px solid #edf3fe;
        padding: 10px;
    }

    .heading {
        color: #006DCC;
        font-size: 15px;
        padding: 20px;
    }

    .bold {
        font-weight: bold;
    }

    .boldpadding {
        font-weight: bold;
        padding: 20px;
    }

    table caption {
        font-size: 15px;
        font-weight: bold;
    }

    .submit-button {
        margin: 20px 20px 20px 0px;
        text-align: center;
        padding: 20px 20px 20px 20px;
        background: #ffd60061;
        width: 13%;
    }
</style>

<?php
$this->breadcrumbs = array(
    'Digital Log Books' => array('index'),
    $model->id,
);
?>

<?php
$sql_rs = Yii::app()->db->createCommand("SELECT id,objection, created_time_vts, created_time_mobile,objection_solve FROM tbl_digital_log_book_history WHERE trip_id='" . $model->trip_id . "' AND (objection<>'' OR objection IS NOT NULL)")->queryAll();
?>

<h3>Driver Objection of: <?php echo $model->id; ?></h3>
<table>
    <?php if ($sql_rs) { ?>
        <tr style="background: #ebdada;">
            <td>
                <span class="bold">Status</span>
            </td>
            <td>
                <span class="bold">SI.</span>
            </td>
            <td><span class="bold">Driver Objection</span>
            </td>
            <td><span class="bold">Date & Time</span></td>
        </tr>
        <?php $i = 1;
        foreach ($sql_rs as $key => $value) { 
           
            ?>
            <tr style="background: #e2efd9;">
                <td><span class="boldpadding">                    
                    <input type="checkbox"  onclick="objectionSubmit(<?php echo $value['id'];?>)" <?php echo ($value['objection_solve']==1 ? "checked" :"")?>></span></td>
                <td><span class="boldpadding"><?php echo $i++; ?></span></td>     
                <td><span class="boldpadding"><?php echo $value['objection']; ?></span></td>
                <td><span class="boldpadding"><?php echo $value['created_time_mobile']; ?></span></td>
            </tr>
        <?php }
    } else { ?>
        <tr style="background: #e2efd9;">
            <td><span class="boldpadding"><?php echo "Driver No Objection Found!"; ?></span></td>
        </tr>
    <?php } ?>


</table>

</div>