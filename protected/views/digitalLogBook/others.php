<style>
    table, th, td {
        border: 1px solid #edf3fe;
        padding: 10px;
    }

    .heading {
        color: #006DCC;
        font-size: 15px;
        padding: 20px;
    }

    .bold {
        font-weight: bold;
    }

    .boldpadding {
        font-weight: bold;
    }

    table caption {
        font-size: 15px;
        font-weight: bold;
    }

    .submit-button {
        margin: 20px 20px 20px 0px;
        text-align: center;
        padding: 20px 20px 20px 20px;
        background: #ffd60061;
        width: 13%;
    }
</style>
<?php
$this->breadcrumbs = array(
    'Digital Log Books' => array('index'),
    $model->id,
);
?>

<?php

$sql_point = "SELECT * FROM tbl_vehicle_place_history WHERE trip_id='" . $model->trip_id . "'";
$sql_point_rs = Yii::app()->db->createCommand($sql_point)->queryAll();


$sql = "SELECT * FROM tbl_digital_log_book_history WHERE trip_id='" . $model->trip_id . "'";
$sql_rs = Yii::app()->db->createCommand($sql)->queryAll();

$morning_ta = 0;
$lunch_ta = 0;
$night_ta = 0;
$night_halt_bill = 0;
$fuel = array();
$othersbill = array();
$driver_objection = array();
$i = 0;
$j = 0;
$ij = 0;
foreach ($sql_rs as $key => $value) {
    if ($value['morning_ta'] != 0) {
        $morning_ta = $value['morning_ta'];
    }
    if ($value['lunch_ta'] != 0) {
        $lunch_ta = $value['lunch_ta'];
    }
    if ($value['night_ta'] != 0) {
        $night_ta = $value['night_ta'];
    }
    if ($value['night_halt_bill'] != 0) {
        $night_halt_bill = $value['night_halt_bill'];
    }
    if ($value['fuel_quantity'] != 0) {
        $fuel[$i]['fuel_recipt_no'] = $value['fuel_recipt_no'];
        $fuel[$i]['fuel_quantity'] = $value['fuel_quantity'];
        $fuel[$i]['fuel_unit_price'] = $value['fuel_unit_price'];
        $fuel[$i]['fuel_total_bill'] = $value['fuel_total_bill'];
        $i++;
    }
    if ($value['others_bill'] != 0) {
        $othersbill[$j]['id'] = $value['id'];
        $othersbill[$j]['others_cost'] = $value['others_cost'];
        $othersbill[$j]['others_bill'] = $value['others_bill'];
        $othersbill[$j]['location_mobile'] = $value['location_mobile'];
        $othersbill[$j]['others_description'] = $value['others_description'];
        $othersbill[$j]['cost_id'] = $value['cost_id'];
        $othersbill[$j]['created_time'] = empty($value['created_time_vts']) || $value['created_time_vts'] == '0000-00-00 00:00:00' ? $value['created_time_mobile'] : $value['created_time_vts'];
        if ($value['others_cost'] == "Repair") {
            $othersbill[$j]['repair_start_time'] == $value['repair_start_time'];
            $othersbill[$j]['repair_end_time'] == $value['repair_end_time'];
        }
        $j++;
    }
}

?>

<h3>Other Bills of: <?php echo $model->id; ?></h3>
<table>
    <form action='' method="post" onsubmit="charge();return false">

        <?php if ($othersbill) { ?>
            <tr style="background: #deeaf6;">
                <td>
                    <span class="bold">Others Bill</span>
                </td>
                <td>
                    <span class="bold">Others Bill Type</span>
                </td>
                <td>
                    <span class="bold">Time</span>
                </td>
                <td>
                    <span class="bold">Date</span>
                </td>
                <td>
                    <span class="bold">Location</span>
                </td>
                <td><span class="bold">Bill Amount</span></td>
                <td>
                    <span class="bold">Description</span>
                </td>
                <!--<td><span class="bold">Bill Submit</span></td>-->
                <td><span class="bold">Bill Copy Submit Status</span></td>
                <td><span class="bold">Total Others Bill</span></td>
            </tr>

            <?php $total_others = 0;
            foreach ($othersbill as $key => $value) { ?>
                <tr style="background: #e2efd9;">
                    <?php
                    $s = $value['created_time'];
                    $TimeStamp = explode(" ", $s);
                    ?>
                    <td><span class="boldpadding"><?php echo $i++; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_cost']; ?></span></td>
                    <td><span class="boldpadding"><?php echo $TimeStamp[1]; ?></span></td>
                    <td><span class="boldpadding"><?php echo $TimeStamp[0]; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['location_mobile'];; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_bill'];
                            $total_others = $total_others + $value['others_bill']; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_description']; ?></span></td>
                    <!--<td><span class="boldpadding"><input type="checkbox" name="charge[]"
                                                         value="<?php /*echo $value['id']; */?>"></span></td>-->
                    <td><span class="boldpadding"><?php $cost_s = $value['cost_id'] == 0 ? "No" : "Yes";
                            echo $cost_s; ?></span></td>
                    <td>
                        <span class="boldpadding"><?php echo $total = $total_others + $tolal_fuel + $total_ta; ?></span>
                    </td>
                </tr>

            <?php } ?>
            <tr>

                <!--<td colspan="3"><span class="boldpadding">Others Grand Total:</span></td>-->
                <!--<td><span class="boldpadding"><?php /*echo $total_others; */?></span></td>-->

                <!--<td><span class="heading"><input type="submit"
                                                 value="Submit"><?php /*//echo CHtml::link("Submit","javascript:void(0)",array("onclick"=>"charge()","role" =>"button",title=>"Submit"));  	*/?>
                </td>-->
               <!-- <td></td>-->
            </tr>

        <?php } else { ?>
            <tr>
                <td colspan="6"><span class="boldpadding"><?php echo "No Others Bill"; ?></span></td>
            </tr>
        <?php } ?>

        <tr>
            <!--<td colspan="3"><span class="heading boldpadding">Total Grand Bill:</span></td>-->
            <!--<td colspan="3"><span
                        class="heading boldpadding"><?php /*echo $total = $total_others + $tolal_fuel + $total_ta; */?></span>
            </td>-->
        </tr>
    </form>
</table>

<div align="right">
    <?php
        
            $this->widget('bootstrap.widgets.TbButton',array(
                'label' => "All Bills in Detail",
                'url' => array('/digitalLogBook/allBill','id'=>$model->id),
                'type' => 'primary',
                'size' => 'small',
            ));
    ?>
</div>

</div>