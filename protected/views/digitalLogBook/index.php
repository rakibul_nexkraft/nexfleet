<?php
/* @var $this DigitalLogBookController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Digital Log Books',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create DigitalLogBook', 'url'=>array('create')),
		array('label'=>'Manage DigitalLogBook', 'url'=>array('admin')),
	);
}
?>

<h1>Digital Log Books</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
