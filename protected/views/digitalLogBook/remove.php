<style>
.columnWidth{
	width:10px;

}
</style>
<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Digital Log Book', 'url'=>array('admin')),
		array('label'=>'Create Digital Log Book', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('digital-log-book-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Digital Log Books</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div><!-- search-form -->

<div style="overflow-x:auto;">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'digital-log-book-grid',
	'dataProvider'=>$model->search($remove=1),
	'filter'=>$model,
	'columns'=>array(
		    array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		'requisition_id',
		'driver_pin',
		'driver_name',
		'user_pin',
		'user_name',
		'user_level',
		'user_dept',
		
		//'email',
		//'user_cell',
		//'user_address',
		'vehicle_reg_no',
		//'vehicletype_id',
		//'vehicle_location',
		
		
		//'helper_pin',
		//'helper_name',
		//'helper_ot',
		//'dutytype_id',
		
		//'start_date',
		array(
            'header' => 'Start Date Time',
            'name'=>'start_date',
            'type'=>'raw',
            'value' => '$data->start_date." ".$data->start_time',            
        ),
        array(
            'header' => 'End Date Time',
            'name'=>'end_date',
            'type'=>'raw',
            'value' => '$data->end_date." ".$data->end_time',
        ),
		//'end_date',
		//'start_time',
		/*array(
			'header' => 'start date time',
			'type' => 'raw',
			'value' => '$data->start_date$data->start_time'
		),*/
		//'end_time',
		'total_time',
		 array(           
            'name'=>'start_point',
            'type'=>'raw',
            'value' => '$data->start_point',
            'htmlOptions' => array('class'=>'columnWidth'),
            'filterHtmlOptions' => array('class'=>'columnWidth'),
            'headerHtmlOptions' => array('class'=>'columnWidth'),
        ),
		
		 array(           
            'name'=>'end_point',
            'type'=>'raw',
            'value' => '$data->end_point',
            'htmlOptions' => array('class'=>'columnWidth'),
            'filterHtmlOptions' => array('class'=>'columnWidth'),
            'headerHtmlOptions' => array('class'=>'columnWidth'),
        ),
		
		'start_meter',
		'end_meter',
		'total_run',
		//'dutyday',
		array(
			'name' => 'dutytype_id',
			'type' => 'raw',
			'value' => 'CHtml::encode($data->dutytypes->type_name)'
		),
		//'rph',
		//'night_halt',
		//'nh_amount',
		//'morning_ta',
		//'lunch_ta',
		//'night_ta',
		//'sup_mile_amount',
		'bill_amount',
		'total_ot_hour',
		'total_ot_amount',
		//'used_mileage',
		//'created_by',
		//'created_time',
		//'updated_by',
		//'updated_time',
		//'active',
		//'trip_id',
		array(
			'header'=>'Submit',
			'type' => 'raw',			
			 'value'=>'($data->submit==0)?CHtml::link(CHtml::image('.Yii::app()->request->baseUrl.'"/images/submit.jpg","Submit"),"javascript:void(0)",array("onclick"=>"ditalLogBook($data->id,this)","role" =>"button",title=>"Submit")):""',
			 'cssClassExpression'=>'$data->id',
		),
		array(
			'header'=>'Others Bill',
			'type' => 'raw',			
			 'value'=>'CHtml::link("Others Bill",array("view","id"=>$data->id,"#"=>"othersbill"))',
		),
		array(
			'header'=>'Edit',
			'type' => 'raw',			
			 'value'=>'($data->submit==0)?CHtml::link(CHtml::image('.Yii::app()->request->baseUrl.'"/assets/update.png","Submit"),array("update","id"=>$data->id)):""',
		),
		
		
	),
)); ?>
</div>
<script>
	function ditalLogBook(id){

	
        
        var message ="Do You Want to Submit Data to Log Book?";
        
        var con=confirm(message);
        if(con===true){  
        	  
             jQuery.post('/index.php/digitalLogBook/logBook',{id:id},function(data){
                //var size_to_value=JSON.parse(data);
                if(data=="Data Save to Log Book successfully."){
                	$("."+id).parent().css( "display", "none" );
                }
                alert(data);
             });
   
        }
        else{
            //$(this).val(preValue);
            //alert(preValue);
        }
        

    }

   

</script>
