<style>
table, th, td {
  border: 1px solid #000000;
  padding: 10px;
}
.heading{
	color: #006DCC;
    font-size: 15px;
    padding: 20px;
}
.bold{
	font-weight: bold;
}
.boldpadding{
	font-weight: bold;
	padding: 20px;
}
table caption{
	font-size: 15px;
	font-weight: bold;
}
.submit-button{
	margin: 20px 20px 20px 0px;
    text-align: center;
    padding: 20px 20px 20px 20px;
    background: #ffd60061;
    width: 13%;
}
</style>
<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List DigitalLogBook', 'url'=>array('index')),
		array('label'=>'Create Digital Log Book', 'url'=>array('create')),
		array('label'=>'Update Digital Log Book', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Digital Log Book', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Digital Log Book', 'url'=>array('admin')),
	);
}
?>

<h1>View Digital Log Book #<?php echo $model->id; ?></h1>
<?php 
$model->dutytype_id = $model->dutytypes->type_name; 
$model->vehicletype_id = $model->vehicletypes->type;
?>

<?php 
$sql_point="SELECT * FROM tbl_vehicle_place_history WHERE trip_id='".$model->trip_id."'";
$sql_point_rs=Yii::app()->db->createCommand($sql_point)->queryAll();


$sql="SELECT * FROM tbl_digital_log_book_history WHERE trip_id='".$model->trip_id."'"; 
$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();
//echo "<pre>";
//var_dump($sql_rs);
//echo "<pre>";
$morning_ta=0;
$lunch_ta=0;
$night_ta=0;
$night_halt_bill=0;
$fuel=array();
$othersbill=array();
$driver_objection=array();
$GLOBALS['objection']="";
$i=0;
$j=0;
$ij=0;
foreach ($sql_rs as $key => $value) {
	if($value['morning_ta']!=0) {$morning_ta=$value['morning_ta'];}
	if($value['lunch_ta']!=0) {$lunch_ta=$value['lunch_ta'];}
	if($value['night_ta']!=0) {$night_ta=$value['night_ta'];}
	if($value['night_halt_bill']!=0) {$night_halt_bill=$value['night_halt_bill'];}
	if($value['location_mobile']!=0) {$night_halt_location_mobile=$value['location_mobile'];}
	if($value['location_vts']!=0) {$night_halt_location_vts=$value['location_vts'];}
	if($value['create_time_server']!=0) {$date_time=$value['create_time_server'];}
	if($value['created_time_vts']!=0) {$vts_time=$value['created_time_vts'];}
	if($value['fuel_quantity']!=0){
     $fuel[$i]['fuel_recipt_no']=$value['fuel_recipt_no'];
     $fuel[$i]['fuel_quantity']=$value['fuel_quantity'];
     $fuel[$i]['fuel_unit_price']=$value['fuel_unit_price'];
     $fuel[$i]['fuel_total_bill']=$value['fuel_total_bill'];
     $i++;
    }
    if($value['others_bill']!=0){
     $othersbill[$j]['id']=$value['id'];
     $othersbill[$j]['others_cost']=$value['others_cost'];
     $othersbill[$j]['others_bill']=$value['others_bill'];
     $othersbill[$j]['others_description']=$value['others_description'];
     $othersbill[$j]['cost_id']=$value['cost_id'];
     $othersbill[$j]['created_time']=empty($value['created_time_vts'])||$value['created_time_vts']=='0000-00-00 00:00:00'?$value['created_time_mobile']:$value['created_time_vts'];
     if($value['others_cost']=="Repair"){
      $othersbill[$j]['repair_start_time']==$value['repair_start_time'];
      $othersbill[$j]['repair_end_time']==$value['repair_end_time'];
     }
     $j++;
    }
    if(!empty($value['objection'])){
    	/*$driver_objection[$ij]['objection']=$value['objection'];
    	$driver_objection[$ij]['create_time']=empty($value['created_time_vts'])||$value['created_time_vts']=='0000-00-00 00:00:00'?$value['created_time_mobile']:$value['created_time_vts'];
    	$ij++;*/
    	$ij++;
    	$GLOBALS['objection'].= "(".$ij.") ".$value['objection'].". ";
    }
}
$GLOBALS['objection'] = !empty($GLOBALS['objection']) ? $GLOBALS['objection'] : "No objection";

/*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'requisition_id',
		'user_pin',
		'user_name',
		'user_level',
		'user_dept',
		'email',
		'user_cell',
		'user_address',
		'vehicle_reg_no',
		'vehicletype_id',
		'vehicle_location',
		'driver_pin',
		'driver_name',
		'helper_pin',
		'helper_name',
		'helper_ot',
		'dutytype_id',
		'dutyday',
		'start_date',
		'end_date',
		'start_time',
		'end_time',
		'total_time',
		'start_point',
		'end_point',
		'start_meter',
		'end_meter',
		'total_run',
		'rph',
		'night_halt',
		'nh_amount',
		'morning_ta',
		'lunch_ta',
		'night_ta',
		'sup_mile_amount',
		'bill_amount',
		'total_ot_hour',
		'total_ot_amount',
		'used_mileage',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		'active',
		'trip_id',
	),
));*/

$this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
                    'id',				'start_date',
                    'requisition_id',	'end_date',
                    'user_pin',			'start_time',
                    'user_name',		'end_time',
                    'user_level',		'start_point',
                    'user_dept',		'end_point',
                    'vehicle_reg_no',	'start_meter',
                    'vehicletype_id',	'end_meter',
                    'vehicle_location', 'driver_pin',
                    'night_halt',       'driver_name',
                    'helper_pin',       'dutytype_id',
                    'helper_name',      'dutyday',
                    'total_time',        'total_ot_hour',
                    array('name' => 'bill_amount', 'value' => $model->bill_amount." (BDT)"),array('name' => 'total_ot_amount', 'value' => $model->total_ot_amount." (BDT)"),
					'created_by',   'updated_by',
                    'created_time', 'updated_time',
                    'trip_id', array('name' => 'Driver objection', 'value' =>objection())
			),
		),
    ),
)); 
function objection(){
	return $GLOBALS['objection'];
}


?>
 <h3>Driver Objection:</h3>
<table>
    <?php if($driver_objection){?>
        <tr>
            <td>
                <span class="bold">SI.</span>
            </td>
            <td><span class="bold">Driver Objection</span>
            </td>
            <td><span class="bold">Date & Time</span></td>
        </tr>
        <?php $i=1;foreach ($driver_objection as $key => $value) {	?>
            <tr>
                <td><span class="boldpadding"><?php echo $i++;?></span></td>
                <td><span class="boldpadding"><?php echo $value['objection'];?></span></td>
                <td><span class="boldpadding"><?php echo $value['create_time'];?></span></td>
            </tr>
        <?php }}
    else{ ?>
        <tr>
            <td><span class="boldpadding"><?php echo "Driver No Objection Found!";?></span></td>
        </tr>
    <?php }?>


</table> 
<!-- <div class="submit-button">
<?php 
	//$logBook="SELECT * FROM tbl_movements WHERE digi_id='".$model->id."'";
	$logBook_rs=Yii::app()->db->createCommand("SELECT * FROM tbl_movements WHERE digi_id='".$model->id."'")->queryRow();
	
	if(empty($logBook_rs)){
		echo CHtml::link("Submit","javascript:void(0)",array("onclick"=>"ditalLogBook($model->id,this)","role" =>"button", "title"=>"Submit"));
	}
	else{
		echo CHtml::link("Log Book ID: ".$logBook_rs['id'],array("/movements/view","id"=>$logBook_rs['id']));
	}
?>
</div> -->
</br>
<table>
	<tr>
        <th colspan="8"><h3 style="text-align: center;">Comparison VTS vs Mobile App</h3></th>
    </tr>
	<tr style="background: #fbe4d5;">
        <th class="bold">Heading</th>
		<th class="bold">Start Time</th>
		<th class="bold">Start Place</th>
		<th class="bold">End Time</th>
		<th class="bold">End Place</th>
		<th class="bold">Comparison</th>
		<th class="bold">Route Details</th>
	</tr>
    <?php $count_row=count($sql_rs);?>
    <tr style="background: #e2efd9;">
        <td class="bold">VTS</td>
        <td><span class="bold"><?php echo $sql_rs[0]['driver_start_time_vts'];?></span></td>
        <td><span class="bold"><?php echo $sql_rs[0]['location_vts'];?></span></td>
        <td><span class="bold"><?php echo $sql_rs[$count_row-1]['driver_end_time_vts'];?></span></td>
        <td><span class="bold"><?php echo $sql_rs[$count_row-1]['location_vts'];?></span></td>
        <td><span class="bold"><?php
                $a = new DateTime($sql_rs[0]['driver_start_time_vts']);
                $b = new DateTime($sql_rs[0]['driver_start_time_mobile']);
                $interval = $a->diff($b);
                echo $total_time=$interval->format("%Y-%m-%d %H:%I:%s");

                ?></span>
        </td>
        <?php if(count($sql_point_rs)>0){
            $point="";
            foreach ($sql_point_rs as $keyp => $valuep) {
                $point.=$valuep['place']."->";
            }
            $point=substr($point,0,-2);
            ?>

            <td rowspan="2"><span class="bold"><?php echo $point;?></span></td>
        <?php }
        else {
            ?>
            <td rowspan="2"><span class="bold">No data Found!</span></td>
        <?php   }?>
    </tr>
    <tr style="background: #e2efd9;">
        <td class="bold">Mobile App</td>
        <td><span class="bold"><?php echo $sql_rs[0]['driver_start_time_mobile'];?></span></td>
        <td><span class="bold"><?php echo $sql_rs[0]['location_mobile'];?></span></td>
        <td><span class="bold"><?php echo $sql_rs[$count_row-1]['driver_end_time_mobile'];?></span></td>
        <td><span class="bold"><?php echo $sql_rs[$count_row-1]['location_mobile'];?></span></td>
        <td><span class="bold"><?php
                $a = new DateTime($sql_rs[$count_row-1]['driver_end_time_vts']);
                $b = new DateTime($sql_rs[$count_row-1]['driver_end_time_mobile']);
                $interval = $a->diff($b);
                echo $total_time=$interval->format("%Y-%m-%d %H:%I:%s");

                ?></span>
        </td>
    </tr>
</table>
<!-- <h3>Bill</h3>
<table>
	<tr>
		<td colspan="6" style="text-align: center"><span class="heading bold" >TA DA</span></td>
	</tr>
	<tr>
		<td><span class="bold" >Mornig TA:</span></td>
		<td colspan="5"><span class="bold" ><?php echo $morning_ta;?></span></td>
	</tr>
	<tr>
		<td><span class="bold" >Lunch TA:</span></td>
		<td colspan="5"><span class="bold" ><?php echo $lunch_ta;?></span></td>
	</tr>
	<tr>
		<td><span class="bold" >Night TA:</span></td>
		<td colspan="5"><span class="bold" ><?php echo $night_ta;	?></span></td>
	</tr>
	<tr>
		<td><span class="bold" >Night Halt:</span></td>
		<td colspan="5"><span class="bold" ><?php echo $night_halt_bill; ?></span></td>
	</tr>
	<tr>
		<td><span class="bold" >Total:</span></td>
		<td colspan="5"><span class="bold" ><?php echo $total_ta=$morning_ta+$lunch_ta+$night_ta+$night_halt_bill; ?></span></td>
	</tr>
	<tr>
		<td colspan="6" style="text-align: center"><span class="heading bold" >Fuel</span></td>
	</tr>
	<tr>
		<td ><span class="bold">Fuel Recipt No</span></td>
		<td ><span class="bold">Fuel Quantity</span></td>
		<td ><span class="bold">Fuel Unit Price</span></td>
		<td colspan="3"><span class="bold">Fuel Total Bill</span></td>
	</tr>



	<?php if($fuel){
		$tolal_fuel=0;
		foreach ($fuel as $key => $value) {	?>
	<tr>
		<td><span class="bold"><?php echo $value['fuel_recipt_no'];?></span></td>
		<td><span class="bold"><?php echo $value['fuel_quantity'];?></span></td>
		<td><span class="bold"><?php echo $value['fuel_unit_price'];	?></span></td>
		<td colspan="3"><span class="bold" ><?php echo $value['fuel_total_bill'];
											 $tolal_fuel=$tolal_fuel+$value['fuel_total_bill']; 	
											 ?></span></td>
		
	</tr>
	<?php }?> 
	<tr>
		<td colspan="3"><span class="bold">Fuel Grand Total</span></td>
		<td colspan="3"><span class="bold"><?php echo $tolal_fuel;?></span></td>
	</tr>
	<?php } 
	else{ ?>
	<tr>
		<td colspan="6"><span  class="boldpadding"><?php echo "No fuel taken";?></span></td>
	</tr>
	<?php }?>

	<tr>
		<td colspan="6" style="text-align: center"><span class="heading bold" >Others Bill</span></td>
	</tr>
	<form action='' method="post" onsubmit="charge();return false">

	<?php if($othersbill){ ?>
	<tr>
		<td>
			<span class="bold">Others Bill Type</span>
		</td>
		<td><span class="bold">Description</span>
		</td>
		<td><span class="bold">Date & Time</span>
		</td>
		<td><span class="bold">Bill Amount</span></td>
		<td><span class="bold">Bill Submit</span></td>
		<td><span class="bold">Bill Submit Status</span></td>
	</tr>
	<?php	$total_others=0;
	foreach ($othersbill as $key => $value) {	?>
	<tr>
	<td><span class="boldpadding"><?php echo $value['others_cost'];?></span></td>	
	<td><span class="boldpadding"><?php echo $value['others_description'];	?></span></td>
	<td><span class="boldpadding"><?php echo $value['created_time'];  	?></span></td>
	<td><span class="boldpadding"><?php echo $value['others_bill']; $total_others=$total_others+$value['others_bill'];?></span></td>
	<td><span class="boldpadding"><input type="checkbox" name="charge[]" value="<?php echo $value['id'];?>"></span></td>
	<td><span class="boldpadding"><?php $cost_s=$value['cost_id']==0?"No":"Yes"; echo $cost_s;?></span></td>
	</tr>
	
	<?php } ?>
	<tr>
		
		<td colspan="3"><span class="boldpadding">Others Grand Total:</span></td>
		<td><span class="boldpadding"><?php echo $total_others;?></span></td>

		<td><span class="heading"><input type="submit" value="Submit"><?php //echo CHtml::link("Submit","javascript:void(0)",array("onclick"=>"charge()","role" =>"button",title=>"Submit"));  	?>
		</td>
		<td></td>
	</tr>

	<?php } 
	else{ ?>
	<tr>
		<td colspan="6"><span class="boldpadding"><?php echo "No Others Bill";?></span></td>
	</tr>
	<?php }?>

	<tr>
		<td colspan="3"><span class="heading boldpadding">Total Grand Bill:</span></td>
		<td colspan="3"><span class="heading boldpadding"><?php echo $total=$total_others+$tolal_fuel+$total_ta;?></span></td>
	</tr>
	</table>
</form> -->
<table>
	<tr>
		<th colspan="9"><h2 style="text-align: center;">Bill Details</h2></th>
	</tr>
	<tr style="background: #fff2cc;">
		<th><span class="bold" >TA</span></th>
		<th><span class="bold" >Mornig</span></th>
        <th><span class="bold" >Lunch</span></th>
        <th><span class="bold" >Night</span></th>
        <th><span class="bold" >Night Halt</span></th>
        <th colspan="2"><span class="bold" >Night Halt Location</span></th>
        <th colspan="2"><span class="bold" >Total TA Bill</span></th>
	</tr>
	<tr style="background: #e2efd9;">
		<td><!--<span class="boldpadding">--><?php /*echo $i++; */?></span></td>
		<td ><span class="bold" ><?php echo $morning_ta;?></span></td>
		<td ><span class="bold" ><?php echo $lunch_ta;?></span></td>
		<td ><span class="bold" ><?php echo $night_ta;?></span></td>
		<td ><span class="bold" ><?php echo $night_halt_bill;?></span></td>
		<td ><span class="bold" >Mobile Location</span></td>
		<td ><span class="bold" >VTS Location</td>
		<td colspan="2"><span class="bold" ><?php echo $total_ta=$morning_ta+$lunch_ta+$night_ta+$night_halt_bill;;?></span></td>
	</tr>
    <tr style="background: #e2efd9;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td ><span class="bold" ><?php echo $night_halt_location_mobile;?></span></td>
        <td ><span class="bold" ><?php echo $night_halt_location_vts;?></span></td>
        <td colspan="2"></td>
    </tr>

	<tr>
		<td colspan="9" style="text-align: center"></td>
	</tr>
	<tr style="background: #fbe4d5;">
		<td ><span class="bold" >Fuel</span></td>
		<td ><span class="bold">Quantity</span></td>
		<td ><span class="bold">Unit Price</span></td>
		<td ><span class="bold">Bill Amount</span></td>
		<td ><span class="bold">Location of Fuel Stations</span></td>
		<td ><span class="bold">VTS Locations</span></td>
		<td ><span class="bold">VTS Time</span></td>
		<td ><span class="bold">Date & Time</span></td>
		<td ><span class="bold">Total Fuel Bill</span></td>
	</tr>



	<?php
    $sql = "SELECT  name FROM tbl_fuelstations INNER JOIN tbl_fuels ON tbl_fuelstations.id = tbl_fuels.fuel_station_id where tbl_fuels.receipt_no='".$value['fuel_recipt_no']."'";
    $fuel_station_name = Yii::app()->db->createCommand($sql)->queryRow();
    if($fuel){
		$tolal_fuel=0;
		foreach ($fuel as $key => $value) {	?>
	<tr style="background: #e2efd9;">
		<!--<td><span class="bold"><?php /*echo $value['fuel_recipt_no'];*/?></span></td>-->
        <td></td>
        <td><span class="bold"><?php echo $value['fuel_quantity'];?></span></td>
		<td><span class="bold"><?php echo $value['fuel_unit_price'];	?></span></td>
		<td><span class="bold" ><?php echo $value['fuel_total_bill'];
											 $tolal_fuel=$tolal_fuel+$value['fuel_total_bill']; 	
											 ?></span></td>
        <td><span class="bold"><?php echo $fuel_station_name['name'];?></span></td>
        <td><span class="bold"><?php echo $night_halt_location_vts;?></span></td>
        <td><span class="bold"><?php echo $vts_time; ?></span></td>
        <td><span class="bold"><?php echo $date_time;?></span></td>
        <td><span class="bold"><?php echo $tolal_fuel; ?></span></td>

	</tr>
	<?php }?> 
	<!--<tr>
		<td colspan="3"><span class="bold">Fuel Grand Total</span></td>
		<td colspan="3"><span class="bold"><?php /*echo $tolal_fuel;*/?></span></td>
	</tr>-->
	<?php } 
	else{ ?>
	<tr style="background: #e2efd9;">
		<td colspan="9"><span  class="boldpadding"><?php echo "No Fuel has Taken During the Trip.";?></span></td>
	</tr>
	<?php }?>

	<tr>
		<td colspan="9" style="text-align: center"></td>
	</tr>

    <form action='' method="post" onsubmit="charge();return false">

        <?php if ($othersbill) { ?>
            <tr style="background: #deeaf6;">
                <td>
                    <span class="bold">Others Bill</span>
                </td>
                <td>
                    <span class="bold">Others Bill Type</span>
                </td>
                <td>
                    <span class="bold">Time</span>
                </td>
                <td>
                    <span class="bold">Date</span>
                </td>
                <td>
                    <span class="bold">Location</span>
                </td>
                <td><span class="bold">Bill Amount</span></td>
                <td>
                    <span class="bold">Description</span>
                </td>
                <!--<td><span class="bold">Bill Submit</span></td>-->
                <td><span class="bold">Bill Copy Submit Status</span></td>
                <td><span class="bold">Total Others Bill</span></td>
            </tr>

            <?php $total_others = 0;
            foreach ($othersbill as $key => $value) { ?>
                <tr style="background: #e2efd9;">
                    <?php
                    $s = $value['created_time'];
                    $TimeStamp = explode(" ", $s);
                    ?>
                    <td><!--<span class="boldpadding"><?php /*echo $i++; */?></span>--></td>
                    <td><span class="boldpadding"><?php echo $value['others_cost']; ?></span></td>
                    <td><span class="boldpadding"><?php echo $TimeStamp[1]; ?></span></td>
                    <td><span class="boldpadding"><?php echo $TimeStamp[0]; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['location_mobile'];; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_bill'];
                            $total_others = $total_others + $value['others_bill']; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_description']; ?></span></td>
                    <!--<td><span class="boldpadding"><input type="checkbox" name="charge[]"
                                                         value="<?php /*echo $value['id']; */?>"></span></td>-->
                    <td><span class="boldpadding"><?php $cost_s = $value['cost_id'] == 0 ? "No" : "Yes";
                            echo $cost_s; ?></span></td>
                    <td>
                        <span class="boldpadding"><?php echo $total = $total_others + $tolal_fuel + $total_ta; ?></span>
                    </td>
                </tr>

            <?php } ?>
            <tr>

                <!--<td colspan="3"><span class="boldpadding">Others Grand Total:</span></td>-->
                <!--<td><span class="boldpadding"><?php /*echo $total_others; */?></span></td>-->

                <!--<td><span class="heading"><input type="submit"
                                                 value="Submit"><?php /*//echo CHtml::link("Submit","javascript:void(0)",array("onclick"=>"charge()","role" =>"button",title=>"Submit"));  	*/?>
                </td>-->
                <!-- <td></td>-->
            </tr>

        <?php } else { ?>
            <tr style="background: #e2efd9;">
                <td colspan="9"><span class="boldpadding"><?php echo "No Others Bill"; ?></span></td>
            </tr>
        <?php } ?>

        <tr>
            <!--<td colspan="3"><span class="heading boldpadding">Total Grand Bill:</span></td>-->
            <!--<td colspan="3"><span
                        class="heading boldpadding"><?php /*echo $total = $total_others + $tolal_fuel + $total_ta; */?></span>
            </td>-->
        </tr>
</table>
</form>

<script>
	function charge(){
		var chk_arr =  document.getElementsByName("charge[]");
		var chklength = chk_arr.length;
		var str='';
		for(k=0;k< chklength;k++)
			{
				if(chk_arr[k].checked==true){
    			chkl=chk_arr[k].value;
    				if(str==''){
    					str=chkl;    					
    				}
    				else{
    					str=str+','+chkl;
    				}
    			}
    			else{}
			} 
		
		if(str!=''){
			var message ="Do You Want to Check Data Save to Cost Table?";        
        	var con=confirm(message);
	        if(con===true){     
	             jQuery.post('/index.php/digitalLogBook/otherCost',{str:str},function(data){
	                //var size_to_value=JSON.parse(data);
	                alert(data);
	             });
	   
	        }
	        else{
	            //$(this).val(preValue);
	            //alert(preValue);
	        }
		}
	}
	
	function ditalLogBook(id){

	
        
        var message ="Do You Want to Submit Data to Log Book?";
        
        var con=confirm(message);
        if(con===true){  
        	  
             jQuery.post('<?php echo Yii::app()->createAbsoluteUrl("/digitalLogBook/logBook");?>',{id:id},function(data){
                //var size_to_value=JSON.parse(data);
                if(data=="Data Save to Log Book successfully."){
                	alert(data);
                	 window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("/digitalLogBook/admin");?>";
                }
                
             });
   
        }
        else{
            //$(this).val(preValue);
            //alert(preValue);
        }
        

    }


</script>	