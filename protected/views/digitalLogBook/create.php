<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List DigitalLogBook', 'url'=>array('index')),
		array('label'=>'Manage Digital Log Book', 'url'=>array('admin')),
	);
}
?>

<h1>Create Digital Log Book</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>