<style>
    .columnWidth {
        width: 10px;
    }

    .fontColorRed, .fontColorRed td {
        background: #ebdada;
    }

    .otherDetailsModalCustom {
        width: 900px !important;
        margin-left: -450px !important;
    }
</style>

<?php
$this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'Modal', 'htmlOptions' => array('class' => 'otherDetailsModalCustom')));

echo '<div id="form-modal" class="modal-body">';
echo '<div id="view-body">Loading...</div>';
echo '</div>';
echo '<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>';

$this->endWidget();
?>

<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs = array(
    'Digital Log Books' => array('index'),
    'Manage',
);
if (!Yii::app()->user->isViewUser()) {
    $this->menu = array(
        array('label' => 'List Digital Log Book', 'url' => array('admin')),
        array('label' => 'Create Digital Log Book', 'url' => array('create')),
    );
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('digital-log-book-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<h1>Manage Digital Log Books</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
//'model'=>$model,
//)); ?>
</div><!-- search-form -->

<div style="overflow-x:auto;">
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'digital-log-book-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'rowCssClassExpression' => '$data->color',
        'columns' => array(
            array(
                'name' => 'id',
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
            ),
            'requisition_id',
            'driver_pin',
            'driver_name',
            'user_pin',
            'user_name',
            'user_level',
            'user_dept',

            //'email',
            //'user_cell',
            //'user_address',
            'vehicle_reg_no',
            //'vehicletype_id',
            //'vehicle_location',


            //'helper_pin',
            //'helper_name',
            //'helper_ot',
            //'dutytype_id',

            //'start_date',
            array(
                'header' => 'Start Date Time',
                'name' => 'start_date',
                'type' => 'raw',
                'value' => '$data->start_date." ".$data->start_time',
            ),
            array(
                'header' => 'End Date Time',
                'name' => 'end_date',
                'type' => 'raw',
                'value' => '$data->end_date." ".$data->end_time',
            ),
            //'end_date',
            //'start_time',
            /*array(
                'header' => 'start date time',
                'type' => 'raw',
                'value' => '$data->start_date$data->start_time'
            ),*/
            //'end_time',
            'total_time',
            array(
                'name' => 'start_point',
                'type' => 'raw',
                'value' => '$data->start_point',
                'htmlOptions' => array('class' => 'columnWidth'),
                'filterHtmlOptions' => array('class' => 'columnWidth'),
                'headerHtmlOptions' => array('class' => 'columnWidth'),
            ),

            array(
                'name' => 'end_point',
                'type' => 'raw',
                'value' => '$data->end_point',
                'htmlOptions' => array('class' => 'columnWidth'),
                'filterHtmlOptions' => array('class' => 'columnWidth'),
                'headerHtmlOptions' => array('class' => 'columnWidth'),
            ),

            'start_meter',
            'end_meter',
            'total_run',
            //'dutyday',
            array(
                'name' => 'dutytype_id',
                'type' => 'raw',
                'value' => 'CHtml::encode($data->dutytypes->type_name)'
            ),
            //'rph',
            //'night_halt',
            //'nh_amount',
            //'morning_ta',
            //'lunch_ta',
            //'night_ta',
            //'sup_mile_amount',
            'bill_amount',
            'total_ot_hour',
            'total_ot_amount',
            //'used_mileage',
            //'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',
            //'active',
            //'trip_id',
            array(
                'header' => 'Bills',
                'type' => 'raw',
                'value' => function($data){
                    return CHtml::link(
                        "Others Bill",
                        "javascript:void(0)",
                        array(
                            "onclick"=>"otherDetail($data->id)",
                            "data-id"=>"$data->id",
                            "id"=>"myModalButton",
                            "role" =>"button"
                        )
                    );
                },
            ),
            array(
                'header' => '',
                'type' => 'raw',
                'value' => function($data){
                    return CHtml::link(
                        "All Bills",
                        "javascript:void(0)",
                        array(
                            "onclick"=>"allDetail($data->id)",
                            "data-id"=>"$data->id",
                            "id"=>"myModalButton",
                            "role" =>"button"
                        )
                    );
                },
            ),
            array(
                'header' => 'Actions',
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::image("http://localhost/ifleet/images/submit.jpg","Submit"),"javascript:void(0)",array("onclick"=>"ditalLogBook($data->id,this)","role" =>"button",title=>"Submit"))',
                'cssClassExpression' => '$data->id',
                'htmlOptions' => array(
                    'class' => 'btn-submit',
                ),

            ),
            array(
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::image("http://localhost/ifleet/assets/update.png","Submit"),array("update","id"=>$data->id))',
                'htmlOptions' => array(
                    'class' => 'btn-edit',
                ),
            ),
            array(
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::image("http://localhost/ifleet/assets/delete.png","Remove"),"javascript:void(0)",array("onclick"=>"ditalLogBookRemove($data->id,this)","role" =>"button",title=>"Remove"))',
                'cssClassExpression' => '$data->id',
            ),
            /*array(
                'header' => 'Driver Objection',
                'type' => 'raw',
                /*'value' => 'CHtml::link(CHtml::image("http://localhost/ifleet/images/objection.png","Submit"),"javascript:void(0)",array("onclick"=>"ditalLogBook($data->id,this)","role" =>"button",title=>"Submit"))',
                'cssClassExpression' => '$data->id',*/
                /*'value' => function ($data) {
                    return CHtml::link(
                        $data->id,
                        "javascript:void(0)",
                        array(
                            "onclick" => "objectionDetail($data->id)",
                            "data-id" => "$data->id",
                            "id" => "myModalButton",
                            "role" => "button"
                        )
                    );
                },
                'htmlOptions' => array(
                    'class' => 'btn-submit',
                ),
            ),*/
            array(
                'header' => 'Remarks',
                'value'=>'CHTML::activeTextArea($data,\'remarks\')',

                'type'=>'raw',

                'htmlOptions'=>array('width'=>'60px', 'height'=> '20px'),
            )
            //Yii::app()->request->baseUrl
            /*array(
                'class'=>'ButtonColumn',
                'header'=>'Edit',
                'template'=>'{update}',
                'evaluateID'=>true,
                'buttons'=>array(
                    'Submit' => array
                    (
                        'label'=>'Submit',
                        'imageUrl'=>Yii::app()->request->baseUrl.'/images/submit.jpg',
                        'url'=>'"#"',


                        'options'=>array("onclick"=>"ditalLogBook($data)"),
                        //"onclick"=>"function ditalLogBook($data->id)",

                    ),
                ),
            ),*/
        ),
    )); ?>
</div>
<script>
    function ditalLogBook(id) {


        var message = "Do You Want to Submit Data to Log Book?";

        var con = confirm(message);
        if (con === true) {

            jQuery.post('/index.php/digitalLogBook/logBook', {id: id}, function (data) {
                //var size_to_value=JSON.parse(data);
                if (data == "Data Save to Log Book successfully.") {
                    $("." + id).parent().css("display", "none");
                }
                alert(data);
            });

        }
        else {
            //$(this).val(preValue);
            //alert(preValue);
        }


    }

    function ditalLogBookRemove(id) {


        var message = "Do You Want to Remove From This List?";

        var con = confirm(message);
        if (con === true) {

            jQuery.post('/index.php/digitalLogBook/ditalLogBookRemove', {id: id}, function (data) {
                //var size_to_value=JSON.parse(data);
                if (data == "Data Remove successfully.") {
                    $("." + id).parent().css("display", "none");
                }
                alert(data);
            });

        }
        else {
            //$(this).val(preValue);
            //alert(preValue);
        }


    }

</script>

<script type="text/javascript">
    function allDetail(id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl("digitalLogBook/allBills");?>',
            data: {id: id},
            success: function (html) {
                $("#Modal").modal("show");
                $("#view-body").html(html);
                return false;
            }
        });
    }
</script>

<script type="text/javascript">
    function otherDetail(id) {
        $.ajax({
            type: 'GET',
            url: '<?php echo Yii::app()->createUrl("digitalLogBook/others");?>',
            data: {id: id},
            success: function (html) {
                $("#Modal").modal("show");
                $("#view-body").html(html);
                return false;
            }
        });
    }
</script>
<!--<script type="text/javascript">
    function objectionDetail(id) {
        $.ajax({
            type: 'GET',
            url: '<?php /*echo Yii::app()->createUrl("digitalLogBook/driverObjection");*/?>',
            data: {id: id},
            success: function (html) {
                $("#objectionModal").modal("show");
                $("#view-body").html(html);
                return false;
            }
        });
    }
</script>-->
<script>
    $(document).ready(function(){
        function autoSave()
        {
            var remarks = $('#DigitalLogBook_remarks').val();

            if(remarks != '')
            {
                $.ajax({
                    url:'<?php echo Yii::app()->createUrl("digitalLogBook/remarks");?>',
                    method:"POST",
                    data:{remarks:DigitalLogBook_remarks,postId:'$model->id'},
                    dataType:"text",
                    success:function(data)
                    {
                        if(data != '')
                        {
                            $('#postId').val(data);
                        }
                        $('#autoSave').text("Post save as draft");
                        setInterval(function(){
                            $('#autoSave').text('');
                        }, 300);
                    }
                });
            }
        }
        setInterval(function(){
            autoSave();
        }, 5000);
    });
</script>
