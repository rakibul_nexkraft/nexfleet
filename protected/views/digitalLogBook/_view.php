<?php
/* @var $this DigitalLogBookController */
/* @var $data DigitalLogBook */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_id')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_pin')); ?>:</b>
	<?php echo CHtml::encode($data->user_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_level')); ?>:</b>
	<?php echo CHtml::encode($data->user_level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_dept')); ?>:</b>
	<?php echo CHtml::encode($data->user_dept); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('user_cell')); ?>:</b>
	<?php echo CHtml::encode($data->user_cell); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_address')); ?>:</b>
	<?php echo CHtml::encode($data->user_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicletype_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicletype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_location')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_name')); ?>:</b>
	<?php echo CHtml::encode($data->driver_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_pin')); ?>:</b>
	<?php echo CHtml::encode($data->helper_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_name')); ?>:</b>
	<?php echo CHtml::encode($data->helper_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_ot')); ?>:</b>
	<?php echo CHtml::encode($data->helper_ot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dutytype_id')); ?>:</b>
	<?php echo CHtml::encode($data->dutytype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dutyday')); ?>:</b>
	<?php echo CHtml::encode($data->dutyday); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_time')); ?>:</b>
	<?php echo CHtml::encode($data->start_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_time')); ?>:</b>
	<?php echo CHtml::encode($data->end_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_time')); ?>:</b>
	<?php echo CHtml::encode($data->total_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_point')); ?>:</b>
	<?php echo CHtml::encode($data->start_point); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_point')); ?>:</b>
	<?php echo CHtml::encode($data->end_point); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_meter')); ?>:</b>
	<?php echo CHtml::encode($data->start_meter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_meter')); ?>:</b>
	<?php echo CHtml::encode($data->end_meter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_run')); ?>:</b>
	<?php echo CHtml::encode($data->total_run); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rph')); ?>:</b>
	<?php echo CHtml::encode($data->rph); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night_halt')); ?>:</b>
	<?php echo CHtml::encode($data->night_halt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nh_amount')); ?>:</b>
	<?php echo CHtml::encode($data->nh_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('morning_ta')); ?>:</b>
	<?php echo CHtml::encode($data->morning_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lunch_ta')); ?>:</b>
	<?php echo CHtml::encode($data->lunch_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night_ta')); ?>:</b>
	<?php echo CHtml::encode($data->night_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sup_mile_amount')); ?>:</b>
	<?php echo CHtml::encode($data->sup_mile_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bill_amount')); ?>:</b>
	<?php echo CHtml::encode($data->bill_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_ot_hour')); ?>:</b>
	<?php echo CHtml::encode($data->total_ot_hour); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_ot_amount')); ?>:</b>
	<?php echo CHtml::encode($data->total_ot_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('used_mileage')); ?>:</b>
	<?php echo CHtml::encode($data->used_mileage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trip_id')); ?>:</b>
	<?php echo CHtml::encode($data->trip_id); ?>
	<br />

	*/ ?>

</div>