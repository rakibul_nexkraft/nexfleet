<style>
table, th, td {
  border: 1px solid #edf3fe;
  padding: 10px;
}
.heading{
	color: #006DCC;
    font-size: 15px;
    padding: 20px;
}
.bold{
	font-weight: bold;
}
.boldpadding{
	font-weight: bold;
	/* padding: 20px; */
}
table caption{
	font-size: 15px;
	font-weight: bold;
}
.submit-button{
	margin: 20px 20px 20px 0px;
    text-align: center;
    padding: 20px 20px 20px 20px;
    background: #ffd60061;
    width: 13%;
}
</style>
<?php

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List DigitalLogBook', 'url'=>array('index')),
		array('label'=>'Create Digital Log Book', 'url'=>array('create')),
		array('label'=>'Update Digital Log Book', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Digital Log Book', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Digital Log Book', 'url'=>array('admin')),
	);
}
?>

<?php 
$model->dutytype_id = $model->dutytypes->type_name; 
$model->vehicletype_id = $model->vehicletypes->type;
?>

<?php
$sql_point="SELECT * FROM tbl_vehicle_place_history WHERE trip_id='".$model->trip_id."'";
$sql_point_rs=Yii::app()->db->createCommand($sql_point)->queryAll();


$sql="SELECT * FROM tbl_digital_log_book_history WHERE trip_id='".$model->trip_id."'"; 
$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();

$morning_ta=0;
$lunch_ta=0;
$night_ta=0;
$night_halt_bill=0;
$fuel=array();
$othersbill=array();
$driver_objection=array();
$i=0;
$j=0;
$ij=0;
foreach ($sql_rs as $key => $value) {
	if($value['morning_ta']!=0) {$morning_ta=$value['morning_ta'];}
	if($value['lunch_ta']!=0) {$lunch_ta=$value['lunch_ta'];}
	if($value['night_ta']!=0) {$night_ta=$value['night_ta'];}
	if($value['night_halt_bill']!=0) {$night_halt_bill=$value['night_halt_bill'];}
	if($value['location_mobile']!=0) {$night_halt_location_mobile=$value['location_mobile'];}
	if($value['create_time_server']!=0) {$date_time=$value['create_time_server'];}
	if($value['location_vts']!=0) {$night_halt_location_vts=$value['location_vts'];}
	if($value['created_time_vts']!=0) {$vts_time=$value['created_time_vts'];}
	if($value['fuel_quantity']!=0){
     $fuel[$i]['fuel_recipt_no']=$value['fuel_recipt_no'];
     $fuel[$i]['fuel_quantity']=$value['fuel_quantity'];
     $fuel[$i]['fuel_unit_price']=$value['fuel_unit_price'];
     $fuel[$i]['fuel_total_bill']=$value['fuel_total_bill'];
     $i++;
    }
    if($value['others_bill']!=0){
     $othersbill[$j]['id']=$value['id'];
     $othersbill[$j]['others_cost']=$value['others_cost'];
     $othersbill[$j]['others_bill']=$value['others_bill'];
     $othersbill[$j]['others_description']=$value['others_description'];
     $othersbill[$j]['cost_id']=$value['cost_id'];
     $othersbill[$j]['created_time']=empty($value['created_time_vts'])||$value['created_time_vts']=='0000-00-00 00:00:00'?$value['created_time_mobile']:$value['created_time_vts'];
     if($value['others_cost']=="Repair"){
      $othersbill[$j]['repair_start_time']==$value['repair_start_time'];
      $othersbill[$j]['repair_end_time']==$value['repair_end_time'];
     }
     $j++;
    }
    if(!empty($value['objection'])){
    	$driver_objection[$ij]['objection']=$value['objection'];
    	$driver_objection[$ij]['create_time']=empty($value['created_time_vts'])||$value['created_time_vts']=='0000-00-00 00:00:00'?$value['created_time_mobile']:$value['created_time_vts'];
    	$ij++;
    }
}

?>

<table>
	<tr>
		<th colspan="9"><h2>Bill Details</h2></th>
	</tr>
	<tr style="background: #fff2cc;">
		<th><span class="bold" >TA</span></th>
		<th><span class="bold" >Mornig</span></th>
        <th><span class="bold" >Lunch</span></th>
        <th><span class="bold" >Night</span></th>
        <th><span class="bold" >Night Halt</span></th>
        <th colspan="2"><span class="bold" >Night Halt Location</span></th>
        <th colspan="2"><span class="bold" >Total TA Bill</span></th>
	</tr>
	<tr style="background: #e2efd9;">
		<td><!--<span class="boldpadding">--><?php /*echo $i++; */?></span></td>
		<td ><span class="bold" ><?php echo $morning_ta;?></span></td>
		<td ><span class="bold" ><?php echo $lunch_ta;?></span></td>
		<td ><span class="bold" ><?php echo $night_ta;?></span></td>
		<td ><span class="bold" ><?php echo $night_halt_bill;?></span></td>
		<td ><span class="bold" >Mobile Location</span></td>
		<td ><span class="bold" >VTS Location</td>
		<td colspan="2"><span class="bold" ><?php echo $total_ta=$morning_ta+$lunch_ta+$night_ta+$night_halt_bill;;?></span></td>
	</tr>
    <tr style="background: #e2efd9;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td ><span class="bold" ><?php echo $night_halt_location_mobile;?></span></td>
        <td ><span class="bold" ><?php echo $night_halt_location_vts;?></span></td>
        <td colspan="2"></td>
    </tr>

	<tr>
		<td colspan="9" style="text-align: center"></td>
	</tr>
	<tr style="background: #fbe4d5;">
		<td ><span class="bold" >Fuel</span></td>
		<td ><span class="bold">Quantity</span></td>
		<td ><span class="bold">Unit Price</span></td>
		<td ><span class="bold">Bill Amount</span></td>
		<td ><span class="bold">Location of Fuel Stations</span></td>
		<td ><span class="bold">VTS Locations</span></td>
		<td ><span class="bold">VTS Time</span></td>
		<td ><span class="bold">Date & Time</span></td>
		<td ><span class="bold">Total Fuel Bill</span></td>
	</tr>



	<?php
    $sql = "SELECT  name FROM tbl_fuelstations INNER JOIN tbl_fuels ON tbl_fuelstations.id = tbl_fuels.fuel_station_id where tbl_fuels.receipt_no='".$value['fuel_recipt_no']."'";
    $fuel_station_name = Yii::app()->db->createCommand($sql)->queryRow();
    if($fuel){
		$tolal_fuel=0;
		foreach ($fuel as $key => $value) {	?>
	<tr style="background: #e2efd9;">
		<!--<td><span class="bold"><?php /*echo $value['fuel_recipt_no'];*/?></span></td>-->
        <td></td>
        <td><span class="bold"><?php echo $value['fuel_quantity'];?></span></td>
		<td><span class="bold"><?php echo $value['fuel_unit_price'];	?></span></td>
		<td><span class="bold" ><?php echo $value['fuel_total_bill'];
											 $tolal_fuel=$tolal_fuel+$value['fuel_total_bill']; 	
											 ?></span></td>
        <td><span class="bold"><?php echo $fuel_station_name['name'];?></span></td>
        <td><span class="bold"><?php echo $night_halt_location_vts;?></span></td>
        <td><span class="bold"><?php echo $vts_time; ?></span></td>
        <td><span class="bold"><?php echo $date_time;?></span></td>
        <td><span class="bold"><?php echo $tolal_fuel; ?></span></td>

	</tr>
	<?php }?> 
	<!--<tr>
		<td colspan="3"><span class="bold">Fuel Grand Total</span></td>
		<td colspan="3"><span class="bold"><?php /*echo $tolal_fuel;*/?></span></td>
	</tr>-->
	<?php } 
	else{ ?>
	<tr style="background: #e2efd9;">
		<td colspan="9"><span  class="boldpadding"><?php echo "No fuel has taken during the trip.";?></span></td>
	</tr>
	<?php }?>

	<tr>
		<td colspan="9" style="text-align: center"></td>
	</tr>

    <form action='' method="post" onsubmit="charge();return false">

        <?php if ($othersbill) { ?>
            <tr style="background: #deeaf6;">
                <td>
                    <span class="bold">Others Bill</span>
                </td>
                <td>
                    <span class="bold">Others Bill Type</span>
                </td>
                <td>
                    <span class="bold">Time</span>
                </td>
                <td>
                    <span class="bold">Date</span>
                </td>
                <td>
                    <span class="bold">Location</span>
                </td>
                <td><span class="bold">Bill Amount</span></td>
                <td>
                    <span class="bold">Description</span>
                </td>
                <!--<td><span class="bold">Bill Submit</span></td>-->
                <td><span class="bold">Bill Copy Submit Status</span></td>
                <td><span class="bold">Total Others Bill</span></td>
            </tr>

            <?php $total_others = 0;
            foreach ($othersbill as $key => $value) { ?>
                <tr style="background: #e2efd9;">
                    <?php
                    $s = $value['created_time'];
                    $TimeStamp = explode(" ", $s);
                    ?>
                    <td><!--<span class="boldpadding"><?php /*echo $i++; */?></span>--></td>
                    <td><span class="boldpadding"><?php echo $value['others_cost']; ?></span></td>
                    <td><span class="boldpadding"><?php echo $TimeStamp[1]; ?></span></td>
                    <td><span class="boldpadding"><?php echo $TimeStamp[0]; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['location_mobile'];; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_bill'];
                            $total_others = $total_others + $value['others_bill']; ?></span></td>
                    <td><span class="boldpadding"><?php echo $value['others_description']; ?></span></td>
                    <!--<td><span class="boldpadding"><input type="checkbox" name="charge[]"
                                                         value="<?php /*echo $value['id']; */?>"></span></td>-->
                    <td><span class="boldpadding"><?php $cost_s = $value['cost_id'] == 0 ? "No" : "Yes";
                            echo $cost_s; ?></span></td>
                    <td>
                        <span class="boldpadding"><?php echo $total = $total_others + $tolal_fuel + $total_ta; ?></span>
                    </td>
                </tr>

            <?php } ?>
            <tr>

                <!--<td colspan="3"><span class="boldpadding">Others Grand Total:</span></td>-->
                <!--<td><span class="boldpadding"><?php /*echo $total_others; */?></span></td>-->

                <!--<td><span class="heading"><input type="submit"
                                                 value="Submit"><?php /*//echo CHtml::link("Submit","javascript:void(0)",array("onclick"=>"charge()","role" =>"button",title=>"Submit"));  	*/?>
                </td>-->
                <!-- <td></td>-->
            </tr>

        <?php } else { ?>
            <tr style="background: #e2efd9;">
                <td colspan="9"><span class="boldpadding"><?php echo "This trip has no others bill."; ?></span></td>
            </tr>
        <?php } ?>

        <tr>
            <!--<td colspan="3"><span class="heading boldpadding">Total Grand Bill:</span></td>-->
            <!--<td colspan="3"><span
                        class="heading boldpadding"><?php /*echo $total = $total_others + $tolal_fuel + $total_ta; */?></span>
            </td>-->
        </tr>
</table>
</form>

<div align="right">
    <?php
        
            $this->widget('bootstrap.widgets.TbButton',array(
                'label' => "Details",
                'url' => array('digitalLogBook/view','id'=>$model->id),
                'type' => 'primary',
                'size' => 'small',
            ));
    ?>
</div>

</div>
<script>
	function charge(){
		var chk_arr =  document.getElementsByName("charge[]");
		var chklength = chk_arr.length;
		var str='';
		for(k=0;k< chklength;k++)
			{
				if(chk_arr[k].checked==true){
    			chkl=chk_arr[k].value;
    				if(str==''){
    					str=chkl;    					
    				}
    				else{
    					str=str+','+chkl;
    				}
    			}
    			else{}
			} 
		
		if(str!=''){
			var message ="Do You Want to Check Data Save to Cost Table?";        
        	var con=confirm(message);
	        if(con===true){     
	             jQuery.post('/index.php/digitalLogBook/otherCost',{str:str},function(data){
	                //var size_to_value=JSON.parse(data);
	                alert(data);
	             });
	   
	        }
	        else{
	            //$(this).val(preValue);
	            //alert(preValue);
	        }
		}
	}
	
	function ditalLogBook(id){
        
        var message ="Do You Want to Submit Data to Log Book?";
        
        var con=confirm(message);
        if(con===true){  
        	  
             jQuery.post('<?php echo Yii::app()->createAbsoluteUrl("/digitalLogBook/logBook");?>',{id:id},function(data){
                //var size_to_value=JSON.parse(data);
                if(data=="Data Save to Log Book successfully."){
                	alert(data);
                	 window.location.href = "<?php echo Yii::app()->createAbsoluteUrl("/digitalLogBook/admin");?>";
                }
                
             });
   
        }
        else{
            //$(this).val(preValue);
            //alert(preValue);
        }
    }
</script>	