<?php
/* @var $this FuelsController */
/* @var $model Fuels */

$this->breadcrumbs=array(
	'Fuels'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Fuels', 'url'=>array('index')),
		array('label'=>'Manage Fuels', 'url'=>array('admin')),
	);
}
?>

<h4>New Fuel</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'typefuel'=>$typefuel)); ?>