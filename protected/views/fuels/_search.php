<?php
/* @var $this FuelsController */
/* @var $model Fuels */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'vehicle_reg_no'); ?><br />
			<?php //echo $form->textField($model,'vehicle_reg_no',array('maxlength'=>127)); ?>
			<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
        'source'=>$this->createUrl('vehicles/getRegNo'),
        // additional javascript options for the autocomplete plugin
        'options'=>array(
        'minLength'=>'1',
        
        'select'=>"js: function(event, ui) {
         $('#Fuels_driver_pin').val(ui.item['driver_id']);
         }"        
        
        ),
        'htmlOptions'=>array(
            'size'=>18,
            'class'=>'input-medium',
        ),
)); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'fuel_type'); ?><br />
			<?php //echo $form->textField($model,'fuel_type',array('maxlength'=>30)); ?>
			<?php echo $form->dropDownList($model, 'fuel_type', $typefuel, array('empty' => 'Select a fuel type...','style'=>'height:30px;','class'=>'input-medium')); ?>
		</div>
	</div>
	<!--<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'quantity'); ?><br />
			<?php echo $form->textField($model,'quantity'); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'value'); ?><br />
			<?php echo $form->textField($model,'value'); ?>
		</div>
	</div>-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'purchase_date'); ?><br />
			<?php echo $form->textField($model,'purchase_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'fuel_station_id'); ?><br />
			<?php //echo $form->textField($model,'fuel_station_id'); ?>
			<?php $fuel_station_id = Fuelstations::model()->findAll(array('select'=>'id,name','order' => 'name ASC'));
        echo $form->dropDownList($model,'fuel_station_id', CHtml::listData($fuel_station_id,'id',  'name'),array('empty' => 'Select Fuel Station...','style'=>'height:30px; width:190px;','class'=>'input-medium'));
		?>
		</div>
	</div>
	<!--<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>


	</div>-->

    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'from_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'from_date',  // name of post parameter
                    //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'to_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'to_date',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        //	'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->to_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </fieldset>


    <div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->