<?php
/* @var $this FuelsController */
/* @var $model Fuels */
/* @var $form CActiveForm */
?>

<div class="create-form form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fuels-form',
	//'enableAjaxValidation'=>false,
    'enableAjaxValidation'=>false,


)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
<div>

	<!--div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div-->

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {
        
         $('#Fuels_driver_pin').val(ui.item['driver_pin']);
         }"        
        
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fuel_type'); ?>
		<?php //echo $form->textField($model,'fuel_type',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->dropDownList($model, 'fuel_type', $typefuel, array('empty' => 'Select a fuel type...','style'=>'width:168px;')); ?>
		<?php echo $form->error($model,'fuel_type'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'meter_reading'); ?>
		<?php echo $form->textField($model,'meter_reading',array('size'=>30)); ?>
		<?php echo $form->error($model,'meter_reading'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textField($model,'value'); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>



	<div class="row">
		<?php echo $form->labelEx($model,'purchase_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'purchase_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->purchase_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
				),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'purchase_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fuel_station_id'); ?>
		<?php $fuel_station_id = Fuelstations::model()->findAll(array('select'=>'id,name','order' => 'name ASC'));
        echo $form->dropDownList($model,'fuel_station_id', CHtml::listData($fuel_station_id,'id',  'name'),array('empty' => 'Select Fuel Station...'));
		?>
		<?php echo $form->error($model,'fuel_station_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<!--div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div-->

</div>

	<!--div class="row buttons" style="width:100%; float:right;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->