<style>
table, th, td {
  border: 1px solid #000000;
  padding: 10px;
}
td {
    font-weight: bold;
}
</style>

<?php
$this->breadcrumbs=array(
	'Fuels',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'New Fuel', 'url'=>array('create')),
    	array('label'=>'List Fuels', 'url'=>array('index')),
    );
}
?>
<div id="calculation-form-two">
<h4 style="text-align: center;border-bottom: none;">Running Throug Octen</h4>
<!-- <div style="margin-top: 2rem;">
    <span style="margin-left: .5em;">Driver Name: Rohim</span>
<span style="margin-left: 10rem;">Driver Pin: 123665</span>
<span style="margin-left: 10rem;">Month: June</span>
</div> -->
</br>
<table>
    <tr style="background: #b8e8ff;">
        <th>From</th>
        <th>To</th>
        <th>Total KM</th>
    </tr>
    
    <tr style="background: #C0C0C0;">
        <td></td>
        <td><span></span></td>
        <td><span></span></td>
    </tr>

    <tr style="background: #e2efd9;">
        <td></td>
        <td><span></span></td>
        <td><span></span></td>
    </tr>

    <tr style="background: #C0C0C0;">
        <td></td>
        <td><span></span></td>
        <td><span></span></td>
    </tr>
    
    <tr style="background: #e2efd9;">
        <td></td>
        <td><span></span></td>
        <td><span></span></td>
    </tr>

    <tr style="background: #C0C0C0;">
        <td></td>
        <td><span></span></td>
        <td><span></span></td>
    </tr>
    
</table>
</br>
<table>
    <tr>
        <th colspan="9"><h3 style="text-align: center;">Run Rate Table</h3></th>
    </tr>
    <tr style="background: #b8e8ff;">
        <th>Present Meter</th>
        <th>Privious Meter</th>
        <th>Total Running (KM)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td></td>
        <td><span></span></td>
        <td><span></span></td>
    </tr>
    <tr>
        <td colspan="9" style="text-align: center"></td>
    </tr>
    <tr style="background: #b8e8ff;">
        <th >Running at Octen (KM)</th>
        <th colspan="2">Running at CNG (KM)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td ></td>
        <td colspan="2"><span></span></td>
    </tr>

    <tr>
        <td colspan="9" style="text-align: center"></td>
    </tr>

    <tr style="background: #b8e8ff;">
        <th >Previous Balance (L)</th>
        <th colspan="2">Addition Octel/Desel Amount (L)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td ></td>
        <td colspan="2"><span></span></td>
    </tr>

    <tr>
        <td colspan="9" style="text-align: center"></td>
    </tr>

    <tr style="background: #b8e8ff;">
        <th colspan="3">BAD Starting Cost (L)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td colspan="3"></td>
    </tr>

    <tr>
        <td colspan="9" style="text-align: center"></td>
    </tr>

    <tr style="background: #b8e8ff;">
        <th >BAD Current Balance (L)</th>
        <th colspan="2">Total Used Octen (L)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td ></td>
        <td colspan="2"><span></span></td>
    </tr>

    <tr>
        <td colspan="9" style="text-align: center">Current Run Rate</td>
    </tr>

    <tr style="background: #b8e8ff;">
        <th >CNG (KM/M3)</th>
        <th colspan="2">Oil (KM/L)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td ></td>
        <td colspan="2"><span></span></td>
    </tr>

    <tr>
        <td colspan="9" style="text-align: center">Previous Run Rate</td>
    </tr>

    <tr style="background: #b8e8ff;">
        <th >CNG (KM/M3)</th>
        <th colspan="2">Oil (KM/L)</th>
    </tr>
    <tr style="background: #e2efd9;">
        <td ></td>
        <td colspan="2"><span></span></td>
    </tr>
    </table>
<div style="margin-top: 2rem;">
    <span>Driver Signature: </span><span style="text-decoration: underline;">Roxy</span>
<span style="margin-left: 55rem;">Checked By: </span><span style="text-decoration: underline;">Reza</span>
</div>
</div>
</br>
<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel')));
?> &nbsp; &nbsp; &nbsp;

<?php

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#calculation-form-two', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Fuel List',
));
?>
