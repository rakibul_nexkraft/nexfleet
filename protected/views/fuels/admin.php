<?php
/* @var $this FuelsController */
/* @var $model Fuels */

$this->breadcrumbs=array(
	'Fuels'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Fuels', 'url'=>array('index')),
		array('label'=>'New Fuel', 'url'=>array('create')),
	);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('fuels-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Fuels</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,'typefuel'=>$typefuel,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'id'=>'fuels-grid',
	'type'=>'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
	'template'=>"{items}",
    'columns'=>array(
        'id',
        'vehicle_reg_no',
        'fuel_type',
        'quantity',
        'value',
        'purchase_date',
        array(
			'name'=>'fuel_station_id',
			'type'=>'raw',
			'value'=>'$data->fuelstations->name',
		),
        // array('name'=>'Fuel Station','value'=>'$data->fuelstations->name'),
        /*
        'driver_pin',
        'created_by',
        'created_time',
        'active',
        */
        array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array
            (
                'view' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/fuels/view", array("id"=>$data->primaryKey))',
                ),
                'update' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/fuels/update", array("id"=>$data->primaryKey))',
                ),
                'delete' => array
                (
                    'url'=>'$this->grid->controller->createUrl("/fuels/delete", array("id"=>$data->primaryKey))',
                ),
            ),
        ),
    ),
)); ?>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuels-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_reg_no',
		'fuel_type',
		'quantity',
		'value',
		'purchase_date',
		'fuel_station_id', */
		/*
		'driver_pin',
		'created_by',
		'created_time',
		'active',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),
	),
));*/ ?>
