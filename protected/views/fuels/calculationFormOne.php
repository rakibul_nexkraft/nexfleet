<style>
table, th, td {
  border: 1px solid #000000;
  padding: 10px;
}
td {
    font-weight: bold;
}
/*.select2-container .select2-choice {
    border: 0px !important;
}*/
</style>

<?php
$this->breadcrumbs=array(
	'Fuels',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'New Fuel', 'url'=>array('create')),
    	array('label'=>'List Fuels', 'url'=>array('index')),
    );
}
?>
<style type="text/css">
   .form-style {        
        padding: 10px;
        border: 1px solid #006DCC;
        margin: 2rem 0px;
    }
</style>
<div id="calculation-form">
    <h3 style="text-align: center;border-bottom: none;">Fuel Calculation Table</h3>   
    <div class="form-style">
         <h4 style="text-align: left;border-bottom: none;">Search Option</h4>
        <?php $form=$this->beginWidget('CActiveForm', array(
                'action'=>Yii::app()->createUrl($this->route),
                'method'=>'get',
                )); ?>
        <span style="margin-left: .5em;">Reg No:         
            <?php 
                echo $form->textField($model,'vehicle_reg_no', array("list" => "vList", "id"=>'drivername', "placeholder" =>'Select A Vehicle' ,"onChange" => 'driverInfo(this.value,1)')); 
            ?>
            <datalist id="vList">
                <?php foreach (Requisitions::getAllVehicle() as $key => $value) {
                    echo "<option value='".$value."'>";
                } ?>          
            </datalist>        
        </span>
        <!-- <span style="margin-left: .5em;">Driver Name:         
            <?php 
                //echo $form->textField($model,'driver_name', array("list" => "nameList", "id"=>'drivername', "onChange" => 'driverInfo(this.value,1)','readonly'=>'readonly')); 
            ?>
            <datalist id="nameList">
                <?php foreach (Drivers::getAllDriverName() as $key => $value) {
                    //echo "<option value='".$value."'>";
                } ?>          
            </datalist>        
        </span> -->
        <!-- <span style="margin-left: 5rem;">Driver PIN: 
          
            <?php 
               // echo $form->textField($model,'driver_pin', array("list" => "pinList", "id"=>'driverpin', "onChange" => 'driverInfo(this.value,2)','readonly'=>'readonly')); 
            ?>
            <datalist id="pinList">
                <?php foreach (Drivers::getAllDriverPin() as $key => $value) {
                    //echo "<option value='".$value."'>";
                } ?>          
            </datalist>           
        </span> -->
        <span style="margin-left: 5rem;">Month: 
            <?php
            $this->widget('ext.EJuiMonthPicker.EJuiMonthPicker', array(
                            'model' => $model,  
                            'attribute' => 'purchase_date',
                            'options'=>array(
                                'yearRange'=>'-30:+99',
                                'dateFormat'=>'MM-yy',

                            ),
                            'htmlOptions'=>array(
                                //'onChange'=>'js:doSomething()',
                                'style'=>'border: 1px;padding: 8px;',
                                //'style'=>'width:210px;border: 0px;padding: 9px;',
                            ),
            ));
            ?>
        </span>
        <span style="margin-left: 5rem;">
            <?php echo CHtml::submitButton('Search',array('style'=>'background: #006DCC;
    color: white;')); ?>
        </span>
    <?php $this->endWidget();?>
    </div>
    </br>
    <?php if(is_array($data)) { 
        $not_table_key=array('cng','diesel','pin','name');
    ?>
    <div id="calculation-form-one">
        <div style="margin-bottom: 10px">
        <span><span style="font-weight: bold;">Reg. No. :</span> <?php echo $model->vehicle_reg_no;?></span>
        <span style="margin-left: 10px"><span style="font-weight: bold;">Driver Name : </span><?php echo $model->driver_name;?></span> 
        <span style="margin-left: 10px"><span style="font-weight: bold;">Pin : </span><?php echo $model->driver_pin;?></span> 
        <span style="margin-left: 10px"><span style="font-weight: bold;">Month: </span><?php echo $model->purchase_date;?></span>
        </div>
        <table style="width:100%">
            <tr style="background: #b8e8ff;">
                <th>Date</th>
                <th colspan="<?php echo $cng=(isset($data['cng']) && $data['cng']>2) ? $data['cng'] : 2; ?>">Amount of CNG taken M3</th>
                <th>Total M3</th>
                <th colspan="<?php echo $diesel=(isset($data['diesel']) && $data['diesel']>2) ? $data['diesel'] : 2; ?>">Amount of Octen/Disel taken L</th>
                <th>Total Liter</th>
            </tr>    
            <?php if($data['cng']>0 || $data['diesel']>0) {
                    $d_G_total = 0;      
                    $c_G_total = 0;  
                    foreach ($data as $key => $value) {
                    $d_total = 0;      
                    $c_total = 0; 
                        if(!in_array($key,$not_table_key)) {
            ?>  
                            <tr style="background: #C0C0C0;">
                                <td><?php echo $key;?></td>
                                <?php for($i=$cng;$i>0;$i--) { ?>
                                <td>
                                    <span><?php echo $value['cng'][$cng-$i+1];
                                                $c_total+= $value['cng'][$cng-$i+1];
                                                $c_G_total+=$value['cng'][$cng-$i+1];
                                            ?>                                    
                                    </span>
                                </td>
                                <?php } ?>
                                <td>
                                    <span>
                                        <?php echo $c_total;?>                            
                                    </span>
                                </td>
                                <?php for($i=$diesel;$i>0;$i--) { ?>
                                <td>
                                    <span><?php echo $value['diesel'][$diesel-$i+1];
                                                $d_total+= $value['diesel'][$diesel-$i+1];
                                                $d_G_total+=$value['diesel'][$diesel-$i+1];
                                            ?>        
                                    </span>
                                </td>
                                <?php } ?>
                                <td>
                                    <span>
                                        <?php echo $d_total;?>                                    
                                    </span>
                                </td>            
                            </tr>
                    <?php  }
                    }
                    ?>
                            <tr style="background: #C0C0C0;">            
                                <td colspan="<?php echo $cng+1;?>" style="text-align: right"><span>Total=</span></td>
                                <td><span><?php  echo $c_G_total;?></span></td>
                                <td colspan="<?php echo $diesel;?>" style="text-align: right"><span>Total=</span></td>
                                <td><span><?php  echo $d_G_total;?></span></td>
                            </tr> 
            <?php    }           
            else { 
            ?>
            <tr style="background: #C0C0C0;">            
                <td colspan="7"><span>No data found!</span></td>
            </tr> 
            <?php } ?>      
        </table>
        <div style="margin-top: 2rem;">
            <span>Driver Signature: </span><span style="text-decoration: underline;"></span>
            <span style="margin-left: 30rem;">Checked By: </span><span style="text-decoration: underline;"></span>
        </div>
    </div>
    <?php } ?>
</div>
</br>
<?php
    /*echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel')));*/
?> &nbsp; &nbsp; &nbsp;

<?php

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#calculation-form-one', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Fuel List',
));
?>
<script type="text/javascript">
    function driverInfo(v,f) {
       $.post('<?php echo Yii::app()->createAbsoluteUrl("/fuels/driverInfo");?>',{v:v,f:f},function(data){
                if(f==1) {
                    $('#driverpin').val(data);
                } 
                else if(f==2) {
                    $('#drivername').val(data);
                }  
                else{

                }            
            });
        }
    
</script>