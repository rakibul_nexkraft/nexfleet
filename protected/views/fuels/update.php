<?php
/* @var $this FuelsController */
/* @var $model Fuels */

$this->breadcrumbs=array(
	'Fuels'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Fuels', 'url'=>array('index')),
		array('label'=>'New Fuel', 'url'=>array('create')),
		array('label'=>'View Fuel', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Fuels', 'url'=>array('admin')),
	);
}
?>

<h4>Update Fuel : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'typefuel'=>$typefuel)); ?>