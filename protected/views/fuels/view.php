<?php
/* @var $this FuelsController */
/* @var $model Fuels */

$this->breadcrumbs=array(
	'Fuels'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Fuels', 'url'=>array('index')),
		array('label'=>'New Fuel', 'url'=>array('create')),
		array('label'=>'Update Fuel', 'url'=>array('update', 'id'=>$model->id)),
	    //array('label'=>'Delete Fuel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
	 	array('label'=>'Manage Fuels', 'url'=>array('admin')),
	);
}
?>

<h4>View Fuel : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		'fuel_type',
		'quantity',
		'value',
		'purchase_date',
		//'fuel_station_id',
		array('name'=>'fuel_station_id', 'value'=>$model->fuelstations->name),
		'driver_pin',
		'created_by',
		'created_time',
		// 'active',
	),
)); ?>

<h4>View Log of this Fuel</h4>
<?php

$config = array('sort'=>array('defaultOrder'=>'updated_time DESC',));

$dataProvider=new CArrayDataProvider($model->fuels_log,$config);
    
$this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,                
        'columns'=>array(    
            array('name'=>'ID','value'=>'$data->id'),    
            array('name'=>'Vehicle Reg No','value'=>'$data->vehicle_reg_no'),    
            array('name'=>'Fuel Type','value'=>'$data->fuel_type'),
            array('name'=>'Purchase Date','value'=>'$data->purchase_date'),
            array('name'=>'Fuel Station','value'=>'$data->fuelstations->name'),
            array('name'=>'Updated Time','value'=>'$data->updated_time'),    
            array('name'=>'Updated By','value'=>'$data->updated_by'),
        
        )
)); ?>