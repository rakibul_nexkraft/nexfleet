<?php
/* @var $this FuelsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Fuels',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'New Fuel', 'url'=>array('create')),
    	array('label'=>'Manage Fuels', 'url'=>array('admin')),
        array('label'=>'Caltulation Table', 'url'=>array('calculationForm')),
        array('label'=>'Fuel Caltulation Table', 'url'=>array('fuelCalculationForm'))
    );
}
?>

<h4>Fuels</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,'typefuel'=>$typefuel,
)); ?>
</div><!-- search-form -->
<div id="fue_container">
    <?php


    $var = $_REQUEST['Fuels'];

    if($var):
        ?>



        <h4>Date:  <?php echo $var['from_date']." to ". $var['to_date']?></h4>
        <?php if($var['purchase_date']) echo "User: ".$var['purchase_date']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
        <?php if($var['vehicle_reg_no'])echo "Vehicle Registration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

        <?php

        if($var['active']==0);
        else if($var['active']==1)$active= "Not Approved";
        else if($var['active']==2)$active= "Approved";
        if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
    <?php endif;?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'fuels-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		array(
            'name' => 'vehicle_reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg_no),array("view","id"=>$data->id))',
        ),
		//'vehicle_reg_no',
        'driver_pin',
		'fuel_type',
		'quantity',
        'unit_price',
		'value',
        'requisition_id',
        'receipt_no',
		'purchase_date',
        array(
            'name' => 'fuel_station_id',
            'type'=>'raw',
            'value' => '$data->fuelstations->name',
        ),
		//'fuel_station_id',
		/*
		'driver_pin',
		'created_by',
		'created_time',
		'active',
		*/
	),
)); 
?>

<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Fuels'])));
?> &nbsp; &nbsp; &nbsp;

<?php

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#fuels-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Fuel List',
));
?>
