<?php
/* @var $this RenewalsController */
/* @var $model Renewals */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<!--div class="fl">
		<div class="row">
			<?php echo $form->label($model,'id'); ?><br />
			<?php echo $form->textField($model,'id',array('size'=>5)); ?>
		</div>
	</div>-->
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'vehicle_reg_no'); ?><br />
			<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
				
		</div>
	</div>
	<!--<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'present_fitness_date'); ?><br />
			<?php echo $form->textField($model,'present_fitness_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'next_fitness_date'); ?><br />
			<?php echo $form->textField($model,'next_fitness_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'present_tax_date'); ?><br />
			<?php echo $form->textField($model,'present_tax_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>	
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'next_tax_date'); ?><br />
			<?php echo $form->textField($model,'next_tax_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'present_insurance_date'); ?><br />
			<?php echo $form->textField($model,'present_insurance_date',array('size'=>18,'class'=>'input-medium')); ?>
		</div>
	</div>
		<div class="row">
			<?php echo $form->label($model,'next_insurance_date'); ?>
			<?php echo $form->textField($model,'next_insurance_date'); ?>
		</div>

		<div class="row">
			<?php echo $form->label($model,'present_routpermit_date'); ?>
			<?php echo $form->textField($model,'present_routpermit_date'); ?>
		</div>

		<div class="row">
			<?php echo $form->label($model,'next_routpermit_date'); ?>
			<?php echo $form->textField($model,'next_routpermit_date'); ?>
		</div>

		<div class="row">
			<?php echo $form->label($model,'created_time'); ?>
			<?php echo $form->textField($model,'created_time'); ?>
		</div>

		<div class="row">
			<?php echo $form->label($model,'created_by'); ?>
			<?php echo $form->textField($model,'created_by'); ?>
		</div>

		<div class="row">
			<?php echo $form->label($model,'active'); ?>
			<?php echo $form->textField($model,'active'); ?>
		</div>-->
        <fieldset style="float:left; padding:0 0 0 10px; margin: 0 10px auto auto;">
        <div class="fl">
		<div class="row">
			<?php echo $form->label($model,'search_field'); ?><br />
			<?php echo $form->dropDownList($model,'search_field', $model->fields, array('empty'=>'', 'style'=>'width:164px;')); ?>
			<?php echo $form->error($model,'search_field',array('style'=>'padding-left:0;')); ?>
		</div>
	</div>
                        
        <div class="fl">
		<div class="row">
			<?php echo $form->label($model,'from'); ?><br />
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,'attribute'=>'from',     

                            // additional javascript options for the date picker plugin
                            'options'=>array('autoSize'=>true,
                                'dateFormat'=>'yy-mm-dd',
                                'defaultDate'=>$model->from,
                                'changeYear'=>true,
                                'changeMonth'=>true,
                            ),

                            'htmlOptions'=>array('style'=>'width:164px;'),
                        )); ?>
		</div>
	</div>
                        
        <div class="fl">
		<div class="row">
			<?php echo $form->label($model,'to'); ?><br />
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                            'model'=>$model,'attribute'=>'to',     

                            // additional javascript options for the date picker plugin
                            'options'=>array('autoSize'=>true,
                                    'dateFormat'=>'yy-mm-dd',
                                    'defaultDate'=>$model->to,
                                    'changeYear'=>true,
                                    'changeMonth'=>true,
                            ),

                            'htmlOptions'=>array('style'=>'width:164px;'),
                        )); ?>
                        <?php echo $form->error($model,'to',array('style'=>'padding-left:0;')); ?>
		</div>
	</div>
        </fieldset>
                        
<div class="clearfix"></div>
	<div align="left">
	
		<?php $this->widget('bootstrap.widgets.TbButton',array(
'label' => 'Search',
'type' => 'primary',
'buttonType'=>'submit', 
'size' => 'medium'
));
?>
	</div>
	

<?php $this->endWidget(); ?>

</div><!-- search-form -->