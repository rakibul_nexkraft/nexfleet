<?php
/* @var $this RenewalsController */
/* @var $model Renewals */

$this->breadcrumbs=array(
	'Renewals'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Renewals', 'url'=>array('index')),
		array('label'=>'Manage Renewals', 'url'=>array('admin')),
	);
}
?>

<h4>New Renewal</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>