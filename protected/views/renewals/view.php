<?php
/* @var $this RenewalsController */
/* @var $model Renewals */

$this->breadcrumbs=array(
	'Renewals'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
	array('label'=>'List Renewals', 'url'=>array('index')),
	array('label'=>'New Renewal', 'url'=>array('create')),
	array('label'=>'Update Renewal', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Renewal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
	array('label'=>'Manage Renewals', 'url'=>array('admin')),
	);
}
?>

<h4>View Renewal : <?php echo $model->vehicle_reg_no; ?></h4>

<?php /* $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'vehicle_reg_no',
		'present_fitness_date',
		'next_fitness_date',
		'present_tax_date',
		'next_tax_date',
		'present_insurance_date',
		'next_insurance_date',
		'present_routpermit_date',
		'next_routpermit_date',
		'created_time',
		'created_by',
		//'active',
	),
)); */?>

<?php $this->widget('zii.widgets.XDetailView', array(
    'data' => $model,
    'attributes' => array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
						'id',						'registration_date',
						'vehicle_reg_no',			'registration_amount',
						'vehicletype_id',			'insurance_amount',
                        'present_fitness_date',		'tax_token',
                        'next_fitness_date',		'routpermit_amount',
						'present_tax_date',			'advanced_tax_amount',
						'next_tax_date',			'miscellaneous_fee',
						'present_insurance_date',	'number_plate_cost',
						'next_insurance_date',   	'created_time',	
						'present_routpermit_date',	'created_by',
						'next_routpermit_date',		array(),					
						'present_ait_date','next_ait_date',

			),
		),
        /*'group3'=>array(
            'ItemColumns' => 1, // one pair per line here
            'attributes' => array(
                'long_text_field',
            ),
            
        ),
        */
    ),
)); ?>

<h4>View Log of this Renewal</h4>

<?php

$config = array('sort'=>array('defaultOrder'=>'updated_time DESC',));

$dataProvider=new CArrayDataProvider($model->renewals_log,$config);
    
$this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$dataProvider,                
        'columns'=>array(        
            array('name'=>'ID','value'=>'$data->id'),
            array('name'=>'Vehicle Reg No','value'=>'$data->vehicle_reg_no'),
            array('name'=>'Present Fitness Date','value'=>'$data->present_fitness_date'),
            array('name'=>'Next Fitness Date','value'=>'$data->next_fitness_date'),
            array('name'=>'Present Tax Date','value'=>'$data->present_tax_date'),
            array('name'=>'Next Tax Date','value'=>'$data->next_tax_date'),
            array('name'=>'Updated Time','value'=>'$data->updated_time'),
            array('name'=>'Updated By','value'=>'$data->updated_by'),
        )
    
)); 
?>