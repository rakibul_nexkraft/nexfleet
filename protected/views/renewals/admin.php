<?php
/* @var $this RenewalsController */
/* @var $model Renewals */

$this->breadcrumbs=array(
	'Renewals'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Renewals', 'url'=>array('index')),
		array('label'=>'New Renewal', 'url'=>array('create')),
	);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('renewals-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Renewals</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
'id'=>'renewals-grid',
'type'=>'striped bordered condensed',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
	'template'=>"{items}",
    'columns'=>array(
       // 'id',
        'vehicle_reg_no',
        'present_fitness_date',
        'next_fitness_date',
        'present_tax_date',
        'next_tax_date',
        /*
        'present_insurance_date',
        'next_insurance_date',
        'present_routpermit_date',
        'next_routpermit_date',
        'created_time',
        'created_by',
        'active',
        */
		array(
			'class'=>'CButtonColumn',
		),
    
    ),
)); ?>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'renewals-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'vehicle_reg_no',
		'present_fitness_date',
		'next_fitness_date',
		'present_tax_date',
		'next_tax_date',     */
		/*
		'present_insurance_date',
		'next_insurance_date',
		'present_routpermit_date',
		'next_routpermit_date',
		'created_time',
		'created_by',
		'active',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),
	),
));*/ ?>
