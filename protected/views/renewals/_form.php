<?php
/* @var $this RenewalsController */
/* @var $model Renewals */
/* @var $form CActiveForm */
?>

<div class="create-form form">
<div>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'renewals-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
</div>
<div class="container1">

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        
    ),
    'htmlOptions'=>array(
        'style'=>'width:150px;'
    ),
)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>



	<div class="row">
		<?php echo $form->labelEx($model,'present_fitness_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'present_fitness_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->present_fitness_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'present_fitness_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'next_fitness_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'next_fitness_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->next_fitness_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'next_fitness_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'present_tax_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'present_tax_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->present_tax_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'present_tax_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'next_tax_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'next_tax_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->next_tax_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'next_tax_date'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'present_insurance_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'present_insurance_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->present_insurance_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'present_insurance_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'next_insurance_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'next_insurance_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->next_insurance_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'next_insurance_date'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'present_routpermit_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'present_routpermit_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->present_routpermit_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'present_routpermit_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'next_routpermit_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'next_routpermit_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->next_routpermit_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'next_routpermit_date'); ?>
	</div>

    <div class="row">
        <?php echo $form->labelEx($model,'present_ait_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'present_ait_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->present_ait_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
        <?php echo $form->error($model,'present_ait_date'); ?>
    </div>
	
</div>
<div class="container2">

    <div class="row">
        <?php echo $form->labelEx($model,'next_ait_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
        'model'=>$model,'attribute'=>'next_ait_date',

        // additional javascript options for the date picker plugin
        'options'=>array('autoSize'=>true,
            'dateFormat'=>'yy-mm-dd',
            'defaultDate'=>$model->next_ait_date,
            'changeYear'=>true,
            'changeMonth'=>true,
        ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
    )); ?>
        <?php echo $form->error($model,'next_ait_date'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'registration_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'registration_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->registration_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'registration_date'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'registration_amount'); ?>
		<?php echo $form->textField($model,'registration_amount',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'registration_amount'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'insurance_amount'); ?>
		<?php echo $form->textField($model,'insurance_amount',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'insurance_amount'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'tax_token'); ?>
		<?php echo $form->textField($model,'tax_token',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'tax_token'); ?>
	</div>
    
    
    	<div class="row">
		<?php echo $form->labelEx($model,'fitness_amount'); ?>
		<?php echo $form->textField($model,'fitness_amount',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'fitness_amount'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'routpermit_amount'); ?>
		<?php echo $form->textField($model,'routpermit_amount',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'routpermit_amount'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'advanced_tax_amount'); ?>
		<?php echo $form->textField($model,'advanced_tax_amount',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'advanced_tax_amount'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'number_plate_cost'); ?>
		<?php echo $form->textField($model,'number_plate_cost',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'number_plate_cost'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'miscellaneous_fee'); ?>
		<?php echo $form->textField($model,'miscellaneous_fee',array('style'=>'width:150px;')); ?>
		<?php echo $form->error($model,'miscellaneous_fee'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>-->
	
</div>
<div class="clearfix"></div>

	<!--div class="row buttons" style="width:100%; float:right;">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->

