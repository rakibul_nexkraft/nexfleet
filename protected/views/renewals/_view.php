<?php
/* @var $this RenewalsController */
/* @var $data Renewals */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('present_fitness_date')); ?>:</b>
	<?php echo CHtml::encode($data->present_fitness_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('next_fitness_date')); ?>:</b>
	<?php echo CHtml::encode($data->next_fitness_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('present_tax_date')); ?>:</b>
	<?php echo CHtml::encode($data->present_tax_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('next_tax_date')); ?>:</b>
	<?php echo CHtml::encode($data->next_tax_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('present_insurance_date')); ?>:</b>
	<?php echo CHtml::encode($data->present_insurance_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('next_insurance_date')); ?>:</b>
	<?php echo CHtml::encode($data->next_insurance_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('present_routpermit_date')); ?>:</b>
	<?php echo CHtml::encode($data->present_routpermit_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('next_routpermit_date')); ?>:</b>
	<?php echo CHtml::encode($data->next_routpermit_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>