<?php
/* @var $this RenewalsController */
/* @var $model Renewals */

$this->breadcrumbs=array(
	'Renewals'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Renewals', 'url'=>array('index')),
		array('label'=>'New Renewal', 'url'=>array('create')),
		array('label'=>'View Renewal', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Renewals', 'url'=>array('admin')),
	);
}
?>

<h4>Update Renewal : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_formU', array('model'=>$model)); ?>