<?php
/* @var $this RenewalsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Renewals',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'New Renewal', 'url'=>array('create')),
    	array('label'=>'Manage Renewals', 'url'=>array('admin')),
    );
}
?>

<h4>Renewals</h4>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php
//echo strtotime("2013-04-24")-strtotime(date("2013-04-09"));
 
	$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'renewals-grid',
	'dataProvider'=>$dataProvider,
	//'filter'=>$model,
	'columns'=>array(
		array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		array(
            'name' => 'vehicle_reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->vehicle_reg_no),array("view","id"=>$data->id))',
        ),

		'present_fitness_date',
		 array(
            'name' => 'next_fitness_date',                                
            'type'=>'raw',                
            'cssClassExpression' => '(strtotime($data->next_fitness_date)-strtotime(date("Y-m-d"))) <= "1296000" && (strtotime($data->next_fitness_date)-strtotime(date("Y-m-d")))>0 ? "highlight_cell" : ""',			        	
        ),

		'present_tax_date',
		
		 array(
            'name' => 'next_tax_date',
            'type'=>'raw',                
            'cssClassExpression' => '(strtotime($data->next_tax_date)-strtotime(date("Y-m-d"))) <= "1296000" && (strtotime($data->next_tax_date)-strtotime(date("Y-m-d")))>0 ? "highlight_cell" : ""',			        	
        ),
		
		'present_insurance_date',		
		array(
            'name' => 'next_insurance_date',
            'type'=>'raw',                
            'cssClassExpression' => '(strtotime($data->next_insurance_date)-strtotime(date("Y-m-d"))) <= "1296000" && (strtotime($data->next_insurance_date)-strtotime(date("Y-m-d")))>0 ? "highlight_cell" : ""',			        	
        ),
		'present_routpermit_date',
		array(
            'name' => 'next_routpermit_date',
            'type'=>'raw',                
            'cssClassExpression' => '(strtotime($data->next_routpermit_date)-strtotime(date("Y-m-d"))) <= "1296000" && (strtotime($data->next_routpermit_date)-strtotime(date("Y-m-d")))>0 ? "highlight_cell" : ""',			        	
        ),
		
        'present_ait_date',
        array(
            'name' => 'next_ait_date',
            'type'=>'raw',                
            'cssClassExpression' => '(strtotime($data->next_ait_date)-strtotime(date("Y-m-d"))) <= "1296000" && (strtotime($data->next_ait_date)-strtotime(date("Y-m-d")))>0 ? "highlight_cell" : ""',			        	
        ),
        
		/*
		'created_time',
		'created_by',
		'active',
		*/
	),
));
?>
<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Renewals'])));
?> &nbsp; &nbsp; &nbsp;
<?php

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#renewals-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Reenewals List',
));

?>
