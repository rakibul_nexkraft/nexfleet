<style>
	.form #rights-form{
		margin-bottom: unset;
	}
</style>

<?php
/* @var $this RightsController */
/* @var $model Rights */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'rights-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Rights</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'rights_name', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'rights_name',array('size'=>60,'maxlength'=>128,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'rights_name'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->