<?php
/* @var $this HolidaysController */
/* @var $model Holidays */

$this->breadcrumbs=array(
	'Holidays'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Holiday List', 'url'=>array('index')),
		array('label'=>'Create Holiday', 'url'=>array('create')),
		array('label'=>'View Holidays', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Holidays', 'url'=>array('admin')),
	);
}
?>

<h4>Update Holiday: <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>