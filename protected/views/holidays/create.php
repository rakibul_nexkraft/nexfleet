<?php
/* @var $this HolidaysController */
/* @var $model Holidays */

$this->breadcrumbs=array(
	'Time Settings'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Holiday List ', 'url'=>array('index')),
		array('label'=>'Manage Holidays', 'url'=>array('admin')),
	);
}
?>

<h4>New Holiday</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>