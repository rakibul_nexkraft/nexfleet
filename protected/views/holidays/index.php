<?php
/* @var $this HolidaysController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Time Settings',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Holiday', 'url'=>array('create')),
		array('label'=>'Holiday Settings', 'url'=>array('admin')),
	);
}
?>

<h4>Time Settings</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'Holidays-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(

        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		'start_date',
        'end_date',
        'remarks',
	),
)); ?>