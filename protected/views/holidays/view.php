<?php
/* @var $this HolidaysController */
/* @var $model Holidays */

$this->breadcrumbs=array(
	'Holidays'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Holiday List', 'url'=>array('index')),
		array('label'=>'New Holiday', 'url'=>array('create')),
		array('label'=>'Update Holiday', 'url'=>array('update', 'id'=>$model->id)),
	//	array('label'=>'Delete Dutytype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Holiday', 'url'=>array('admin')),
	);
}
?>

<h4>View Time Settings : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'start_date',
		'end_date',
        'remarks'
	),
)); ?>
