<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $data UserdigitalLogBookHistory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_id')); ?>:</b>
	<?php echo CHtml::encode($data->login_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trip_id')); ?>:</b>
	<?php echo CHtml::encode($data->trip_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_id')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_end')); ?>:</b>
	<?php echo CHtml::encode($data->login_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trip_id_end')); ?>:</b>
	<?php echo CHtml::encode($data->trip_id_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('duty_day')); ?>:</b>
	<?php echo CHtml::encode($data->duty_day); ?>
	<br />

	
	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_name')); ?>:</b>
	<?php echo CHtml::encode($data->driver_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_pin')); ?>:</b>
	<?php echo CHtml::encode($data->user_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('department')); ?>:</b>
	<?php echo CHtml::encode($data->department); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dutytype_id')); ?>:</b>
	<?php echo CHtml::encode($data->dutytype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_start_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->user_start_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_end_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->user_end_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_number')); ?>:</b>
	<?php echo CHtml::encode($data->meter_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->location_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->created_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />



</div>