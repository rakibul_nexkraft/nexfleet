<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $model UserdigitalLogBookHistory */

$this->breadcrumbs=array(
	'Userdigital Log Book Histories'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List UserdigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Create UserdigitalLogBookHistory', 'url'=>array('create')),
	);
}
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('userdigital-log-book-history-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Userdigital Log Book Histories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'userdigital-log-book-history-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'login_id',
		'trip_id',
		'requisition_id',
		'login_end',
		'trip_id_end',
		/*
		'duty_day',
		'driver_pin',
		'driver_name',
		'user_pin',
		'user_name',
		'department',
		'vehicle_reg_no',
		'vehicle_type_id',
		'dutytype_id',
		'user_start_time_mobile',
		'user_end_time_mobile',
		'meter_number',
		'location_mobile',
		'created_time_mobile',
		'created_by',
		'updated_time_mobile',
		'updated_by',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
