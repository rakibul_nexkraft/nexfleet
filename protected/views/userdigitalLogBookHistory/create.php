<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $model UserdigitalLogBookHistory */

$this->breadcrumbs=array(
	'Userdigital Log Book Histories'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List UserdigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Manage UserdigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Create UserdigitalLogBookHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>