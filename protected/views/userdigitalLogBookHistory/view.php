<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $model UserdigitalLogBookHistory */

$this->breadcrumbs=array(
	'Userdigital Log Book Histories'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List UserdigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Create UserdigitalLogBookHistory', 'url'=>array('create')),
		array('label'=>'Update UserdigitalLogBookHistory', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete UserdigitalLogBookHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage UserdigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>View UserdigitalLogBookHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'login_id',
		'trip_id',
		'requisition_id',
		'login_end',
		'trip_id_end',
		'duty_day',
		'driver_pin',
		'driver_name',
		'user_pin',
		'user_name',
		'department',
		'vehicle_reg_no',
		'vehicle_type_id',
		'dutytype_id',
		'user_start_time_mobile',
		'user_end_time_mobile',
		'meter_number',
		'location_mobile',
		'created_time_mobile',
		'created_by',
		'updated_time_mobile',
		'updated_by',
	),
)); ?>
