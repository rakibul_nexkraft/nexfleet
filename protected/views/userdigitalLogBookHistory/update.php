<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $model UserdigitalLogBookHistory */

$this->breadcrumbs=array(
	'Userdigital Log Book Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List UserdigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Create UserdigitalLogBookHistory', 'url'=>array('create')),
		array('label'=>'View UserdigitalLogBookHistory', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage UserdigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Update UserdigitalLogBookHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>