<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Userdigital Log Book Histories',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create UserdigitalLogBookHistory', 'url'=>array('create')),
		array('label'=>'Manage UserdigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Userdigital Log Book Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
