<?php
/* @var $this UserdigitalLogBookHistoryController */
/* @var $model UserdigitalLogBookHistory */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'userdigital-log-book-history-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'login_id'); ?>
		<?php echo $form->textField($model,'login_id',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'login_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trip_id'); ?>
		<?php echo $form->textField($model,'trip_id',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'trip_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
		<?php echo $form->error($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'login_end'); ?>
		<?php echo $form->textField($model,'login_end',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'login_end'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trip_id_end'); ?>
		<?php echo $form->textField($model,'trip_id_end',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'trip_id_end'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duty_day'); ?>
		<?php echo $form->textField($model,'duty_day',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'duty_day'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'driver_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'department'); ?>
		<?php echo $form->textField($model,'department',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'department'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_type_id'); ?>
		<?php echo $form->textField($model,'vehicle_type_id'); ?>
		<?php echo $form->error($model,'vehicle_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dutytype_id'); ?>
		<?php echo $form->textField($model,'dutytype_id'); ?>
		<?php echo $form->error($model,'dutytype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_start_time_mobile'); ?>
		<?php echo $form->textField($model,'user_start_time_mobile'); ?>
		<?php echo $form->error($model,'user_start_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_end_time_mobile'); ?>
		<?php echo $form->textField($model,'user_end_time_mobile'); ?>
		<?php echo $form->error($model,'user_end_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meter_number'); ?>
		<?php echo $form->textField($model,'meter_number'); ?>
		<?php echo $form->error($model,'meter_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location_mobile'); ?>
		<?php echo $form->textField($model,'location_mobile',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'location_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time_mobile'); ?>
		<?php echo $form->textField($model,'created_time_mobile'); ?>
		<?php echo $form->error($model,'created_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time_mobile'); ?>
		<?php echo $form->textField($model,'updated_time_mobile'); ?>
		<?php echo $form->error($model,'updated_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->