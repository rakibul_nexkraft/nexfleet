<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>
<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->name,
);

?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Mechanic #<?php echo $model->name; ?></h3>
    </div>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'userpin',
		'name',
		array(
			'name' => 'active',
			'value' => $model->statusType($model->active),
		),
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
