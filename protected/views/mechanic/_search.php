<?php
/* @var $this MechanicController */
/* @var $model Mechanic */
/* @var $form CActiveForm */
?>

<div class="wide form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	)); ?>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'id'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'id'); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'userpin'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'userpin',array('size'=>60,'maxlength'=>150)); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'name'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>150)); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'active'); ?>
			<div class="clearfix"></div>
			<?php echo $form->textField($model,'active'); ?>
		</div>
	</div>

	<div class="clearfix"></div>
    
    <div align="left" style="padding-left:15px;">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div><br/>

	<?php $this->endWidget(); ?>

</div><!-- search-form -->