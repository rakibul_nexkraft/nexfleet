<?php
/* @var $this MechanicController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mechanics',
);

$this->menu=array(
	array('label'=>'Create Mechanic', 'url'=>array('create')),
	array('label'=>'Manage Mechanics', 'url'=>array('admin')),
);
?>

<?php
echo CHtml::link('Create mechanic', array("/mechanic/create"), array('class' => 'button button-pink button-mini button-rounded', 'style' => "color: white !important;float: right;"));
?>

<h4>Mechanics</h4>

<div class="search-form">
<?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'mechanic-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
		'userpin',
		'name',
		array(                                        
            'name' => 'active',
            'type'=>'raw',
            'value' => '$data->statusType($data->active)'
        ),
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>

