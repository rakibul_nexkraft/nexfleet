<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List of Mechanics', 'url'=>array('index')),
	array('label'=>'Manage Mechanics', 'url'=>array('admin')),
);
?>

<h4>Create Mechanic</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>