<style>
	.form #mechanic-form{
		margin-bottom: unset;
	}
</style>

<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mechanic-form',
	'enableAjaxValidation'=>false,
   'htmlOptions'=>array('onsubmit'=>"return false")
)); ?>
	
	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Mechanic</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'userpin', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'userpin',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'userpin'); ?>

		<?php echo $form->labelEx($model, 'name', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'name'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'active', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->dropDownList($model, 'active', array('0'=>'Inactive','1'=>'Active'), array('class' => 'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'active'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color','onclick'=>'submitMechanic()')); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script>

        function submitMechanic(){
        $("#mechanic-form").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/mechanic/create");?>', 
            type: 'post',
            success:  function(data) {

            		if(isNaN(data)){
            			bsModalOpen(data);
            		}else{
            			window.location.href ="view/"+data;
            		}     
            }
        });
    }
    </script>