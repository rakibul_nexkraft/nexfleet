<?php
/* @var $this MechanicController */
/* @var $model Mechanic */

$this->breadcrumbs=array(
	'Mechanics'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List of Mechanics', 'url'=>array('index')),
	array('label'=>'Create Mechanic', 'url'=>array('create')),
	array('label'=>'View Mechanic', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Mechanics', 'url'=>array('admin')),
);
?>

<h4>Update Mechanic <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>