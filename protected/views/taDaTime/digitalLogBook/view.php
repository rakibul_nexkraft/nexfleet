<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DigitalLogBook', 'url'=>array('index')),
	array('label'=>'Create DigitalLogBook', 'url'=>array('create')),
	array('label'=>'Update DigitalLogBook', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DigitalLogBook', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DigitalLogBook', 'url'=>array('admin')),
);
?>

<h1>View DigitalLogBook #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'requisition_id',
		'user_pin',
		'user_name',
		'user_level',
		'user_dept',
		'email',
		'user_cell',
		'user_address',
		'vehicle_reg_no',
		'vehicletype_id',
		'vehicle_location',
		'driver_pin',
		'driver_name',
		'helper_pin',
		'helper_name',
		'helper_ot',
		'dutytype_id',
		'dutyday',
		'start_date',
		'end_date',
		'start_time',
		'end_time',
		'total_time',
		'start_point',
		'end_point',
		'start_meter',
		'end_meter',
		'total_run',
		'rph',
		'night_halt',
		'nh_amount',
		'morning_ta',
		'lunch_ta',
		'night_ta',
		'sup_mile_amount',
		'bill_amount',
		'total_ot_hour',
		'total_ot_amount',
		'used_mileage',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		'active',
		'trip_id',
	),
)); ?>
