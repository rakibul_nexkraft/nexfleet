<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DigitalLogBook', 'url'=>array('index')),
	array('label'=>'Create DigitalLogBook', 'url'=>array('create')),
	array('label'=>'View DigitalLogBook', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DigitalLogBook', 'url'=>array('admin')),
);
?>

<h1>Update DigitalLogBook <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>