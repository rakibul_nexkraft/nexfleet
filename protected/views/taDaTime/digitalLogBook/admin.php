<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DigitalLogBook', 'url'=>array('index')),
	array('label'=>'Create DigitalLogBook', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('digital-log-book-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Digital Log Books</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'digital-log-book-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'requisition_id',
		'user_pin',
		'user_name',
		'user_level',
		'user_dept',
		/*
		'email',
		'user_cell',
		'user_address',
		'vehicle_reg_no',
		'vehicletype_id',
		'vehicle_location',
		'driver_pin',
		'driver_name',
		'helper_pin',
		'helper_name',
		'helper_ot',
		'dutytype_id',
		'dutyday',
		'start_date',
		'end_date',
		'start_time',
		'end_time',
		'total_time',
		'start_point',
		'end_point',
		'start_meter',
		'end_meter',
		'total_run',
		'rph',
		'night_halt',
		'nh_amount',
		'morning_ta',
		'lunch_ta',
		'night_ta',
		'sup_mile_amount',
		'bill_amount',
		'total_ot_hour',
		'total_ot_amount',
		'used_mileage',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		'active',
		'trip_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
