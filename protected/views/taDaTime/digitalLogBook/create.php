<?php
/* @var $this DigitalLogBookController */
/* @var $model DigitalLogBook */

$this->breadcrumbs=array(
	'Digital Log Books'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DigitalLogBook', 'url'=>array('index')),
	array('label'=>'Manage DigitalLogBook', 'url'=>array('admin')),
);
?>

<h1>Create DigitalLogBook</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>