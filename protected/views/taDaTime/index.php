<?php
/* @var $this TaDaTimeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ta Da Times',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create TaDaTime', 'url'=>array('create')),
		array('label'=>'Manage TaDaTime', 'url'=>array('admin')),
	);
}
?>

<h1>Ta Da Times</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
