<?php
/* @var $this TaDaTimeController */
/* @var $model TaDaTime */

$this->breadcrumbs=array(
	'Ta Da Times'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List TaDaTime', 'url'=>array('index')),
		array('label'=>'Manage TaDaTime', 'url'=>array('admin')),
	);
}
?>

<h1>Create TaDaTime</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>