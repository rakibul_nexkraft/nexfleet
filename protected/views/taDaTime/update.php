<?php
/* @var $this TaDaTimeController */
/* @var $model TaDaTime */

$this->breadcrumbs=array(
	'Ta Da Times'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List TaDaTime', 'url'=>array('index')),
		array('label'=>'Create TaDaTime', 'url'=>array('create')),
		array('label'=>'View TaDaTime', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage TaDaTime', 'url'=>array('admin')),
	);
}
?>

<h1>Update TaDaTime <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>