<?php
/* @var $this TaDaTimeController */
/* @var $model TaDaTime */

$this->breadcrumbs=array(
	'Ta Da Times'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List TaDaTime', 'url'=>array('index')),
		array('label'=>'Create TaDaTime', 'url'=>array('create')),
		array('label'=>'Update TaDaTime', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete TaDaTime', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage TaDaTime', 'url'=>array('admin')),
	);
}
?>

<h1>View TaDaTime #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'ta_da_name',
		'start_time',
		'end_time',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
