<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemnamesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Item Names',
);

$this->menu=array(
	array('label'=>'New Item Name', 'url'=>'#', 'linkOptions'=>array('onclick'=>CHtml::ajax(array(
		'type'=>'GET',
		'url'=>array('create'),
		'datatype'=>'html',
		'success'=>"function(data){
			$('#popModal').modal();
			$('.modal-body').html(data);
			return false;
		}",
	))),'visible'=> in_array('New Item Name', $user_rights)),
	array('label'=>'Manage Item Names', 'url'=>array('admin'),'visible'=> in_array('Manage Item Names', $user_rights)),
);
?>

<h4>Item Names</h4>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
	'type'=>'striped bordered condensed',
	'id'=>'wsitemnames-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,

	'columns'=>array(
		array(
			'name' => 'id',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
		),
		'name',
		'parts_no',
		'vehicle_model',
		'vehicle_type',
		'active',
	),
));
?>

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
	<a class="close" data-dismiss="modal">&times;</a>
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:50%; left:45% !important;'),
)); ?>

<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<br />
</div>

<div class="modal-body">
</div>

<?php $this->endWidget(); ?>