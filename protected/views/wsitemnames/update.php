<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemnamesController */
/* @var $model Wsitemnames */

$this->breadcrumbs=array(
	'Item Names'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Item Names', 'url'=>array('index'),'visible'=> in_array('List Item Names', $user_rights)),
	array('label'=>'New Item Name', 'url'=>array('create'),'visible'=> in_array('New Item Name', $user_rights)),
	array('label'=>'View Item Name', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Item Name', $user_rights)),
	array('label'=>'Manage Item Names', 'url'=>array('admin'),'visible'=> in_array('Manage Item Names', $user_rights)),
);
?>

<h4>Update Wsitemnames <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>