<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemnamesController */
/* @var $model Wsitemnames */

$this->breadcrumbs=array(
	'Item Names'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Item Names', 'url'=>array('index'),'visible'=> in_array('List Item Names', $user_rights)),
	array('label'=>'New Item Name', 'url'=>array('create'),'visible'=> in_array('New Item Name', $user_rights)),
	array('label'=>'Update Item Name', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Item Name', $user_rights)),
	array('label'=>'Delete Item Name', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Item Name', $user_rights)),
	array('label'=>'Manage Item Names', 'url'=>array('admin'),'visible'=> in_array('Manage Item Names', $user_rights)),
);
?>

<h4>View Item Name #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'parts_no',
		'vehicle_model',
		'vehicle_type',
		'active',
	),
)); ?>
