<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsitemnamesController */
/* @var $model Wsitemnames */

$this->breadcrumbs=array(
	'Item Names'=>array('index'),
	'New',
);

$this->menu=array(
	array('label'=>'List Item Names', 'url'=>array('index'),'visible'=> in_array('List Item Names', $user_rights)),
	array('label'=>'Manage Item Names', 'url'=>array('admin'),'visible'=> in_array('Manage Item Names', $user_rights)),
);
?>

<h4>New Item Name</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>