<?php
/* @var $this WssnacksbiilamountController */
/* @var $model Wssnacksbiilamount */

$this->breadcrumbs=array(
	'Wssnacksbiilamounts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Snacks Biil Amount', 'url'=>array('index')),
	array('label'=>'Create Snacks Biil Amount', 'url'=>array('create')),
	array('label'=>'Update Snacks Biil Amount', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Snacks Biil Amount', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Snacks Biil Amount', 'url'=>array('admin')),
);
?>

<h1>View Snacks Biil Amount #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'bill_amount',
		'create_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
