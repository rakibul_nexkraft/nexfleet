<?php
/* @var $this WssnacksbiilamountController */
/* @var $model Wssnacksbiilamount */

$this->breadcrumbs=array(
	'Wssnacksbiilamounts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Snacks Biil Amount', 'url'=>array('index')),
	array('label'=>'Manage Snacks Biil Amount', 'url'=>array('admin')),
);
?>

<h1>Create Snacks Biil Amount</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>