<?php
/* @var $this BillratesController */
/* @var $model Billrates */

$this->breadcrumbs=array(
	'Bill Rates'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Bill Rates List', 'url'=>array('index')),
		//array('label'=>'New Bill Rate', 'url'=>array('create')),
		array('label'=>'Update Bill Rate', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete Billrate', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Bill Rates', 'url'=>array('admin')),
	);
}
?>

<h4>View Bill Rate : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'rate_type',
		'rate_amount',
		'created_by',
		'created_time',
		//'active',
	),
)); ?>
