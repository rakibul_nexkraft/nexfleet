<?php
/* @var $this BillratesController */
/* @var $model Billrates */

$this->breadcrumbs=array(
	'Bill Rates'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Bill Rates List', 'url'=>array('index')),
		array('label'=>'Manage Bill Rates', 'url'=>array('admin')),
	);
}
?>

<h4>New Bill Rate</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>