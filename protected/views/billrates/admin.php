<?php
/* @var $this BillratesController */
/* @var $model Billrates */

$this->breadcrumbs=array(
	'Bill Rates'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Bill Rates', 'url'=>array('index')),
		//array('label'=>'New Bill Rate', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('billrates-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Bill Rates</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'billrates-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'rate_type',
		'rate_amount',
		'created_by',
		'created_time',
		'active',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
