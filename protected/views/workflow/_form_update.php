<?php
/* @var $this WorkflowController */
/* @var $model Workflow */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'workflow-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'workflow'); ?>
		<?php echo $form->textField($model,'workflow',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'workflow'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duration'); ?>
		<?php echo $form->textField($model,'duration',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>
	
	<div id="isParent" class="row">
		<?php echo $form->labelEx($model,'is_parent'); ?>
		<?php echo $form->dropDownList($model, 'is_parent', array('0'=>'No','1'=>'Yes')); ?>
		<?php echo $form->error($model,'is_parent'); ?>
	</div>

	<div id="parent" class="row" style="display: none;">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->textField($model,'parent_id'); ?>
		<?php echo $form->error($model,'parent_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$("#Workflow_is_parent").change(function() {
  if ($(this).val() == 1) {
    $('#parent').show();
    $('#Workflow_parent_id').attr('required', '');
    $('#Workflow_parent_id').attr('data-error', 'This field is required.');
  } else {
    $('#parent').hide();
    $('#Workflow_parent_id').removeAttr('required');
    $('#Workflow_parent_id').removeAttr('data-error');
  }
});
</script>