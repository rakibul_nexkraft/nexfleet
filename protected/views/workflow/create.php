<?php
/* @var $this WorkflowController */
/* @var $model Workflow */

$this->breadcrumbs=array(
	'Workflows'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List of Workflows', 'url'=>array('index')),
	array('label'=>'Manage Workflows', 'url'=>array('admin')),
);
?>

<h4>Create Workflow</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>