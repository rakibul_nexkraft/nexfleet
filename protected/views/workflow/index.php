<?php
/* @var $this WorkflowController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Workflows',
);

$this->menu=array(
	array('label'=>'Create Workflow', 'url'=>array('create')),
	array('label'=>'Manage Workflows', 'url'=>array('admin')),
);
?>

<?php
echo CHtml::link('Create workflow', array("/workflow/create"), array('class' => 'button button-pink button-mini button-rounded', 'style' => "color: white !important;float: right;"));
?>

<h4>Workflows</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'workflow-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id',
		'workflow',
		'duration',
		array(
			'name' => 'is_parent',
			'value' => '$data->statusType($data->is_parent)'
		),
		'parent_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
