<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>
<?php
$this->breadcrumbs=array(
	'Workflows'=>array('index'),
	$model->id,
);
?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Workflow #<?php echo $model->id; ?></h3>
    </div>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'workflow',
		'duration',
		array(
			'name' => 'is_parent',
			'value' => $model->statusType($model->is_parent),
		),
		'parent_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
