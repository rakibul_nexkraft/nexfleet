<style>
	.form #workflow-form{
		margin-bottom: unset;
	}
</style>
<?php
/* @var $this WorkflowController */
/* @var $model Workflow */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'workflow-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Workflow</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'workflow', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'workflow',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'workflow'); ?>

		<?php echo $form->labelEx($model, 'duration', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'duration',array('size'=>60,'maxlength'=>45,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'duration'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'is_parent', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->dropDownList($model, 'is_parent', array('0'=>'No','1'=>'Yes'), array('class' => 'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'is_parent'); ?>

		<?php echo $form->labelEx($model, 'parent_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'parent_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'parent_id'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script>
$("#Workflow_is_parent").change(function() {
  if ($(this).val() == 1) {
    $('#parent').show();
    $('#Workflow_parent_id').attr('required', '');
    $('#Workflow_parent_id').attr('data-error', 'This field is required.');
  } else {
    $('#parent').hide();
    $('#Workflow_parent_id').removeAttr('required');
    $('#Workflow_parent_id').removeAttr('data-error');
  }
});
</script>