<?php

$this->breadcrumbs=array(
	'Invoices'=>array('index'),
	'Manage',
);
?>


<?php /*$this->renderPartial('_search',array(
	'model'=>$model,
));*/ ?>
</div><!-- search-form -->

<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin">
    <h4 class="heading-custom page_header_h4">List of Invoices</h4>

    <input style="width: 20% !important;float: right;margin-bottom: 10px;" class="btn-search button-pink" id="search-button" onclick="invoiceCreate()" name="" type="submit" value="Create Invoice">
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'invoice-grid',
	'itemsCssClass' => 'table cart table_um',
    'htmlOptions' => array('class' => 'table-responsive bottommargin, admin-table-responsive border-round-bottom'),
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
		'invoice_number',
		'total_quantity',
		'total_amount',
		'challan_no',
		'challan_date',
		'invoice_date',

		/*'created_by',
		'created_time',
		'updated_by',
		'updated_time',*/
		
	array(
      'class'=>'CButtonColumn',
      'template'=>'{view} {update} {delete} ',
      'buttons'=> array(
        'view' => array(
                        'url' => 'CHtml::normalizeUrl(array("/invoice/userView/id/". $data->id . "/type/1&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
        'update' => array(
                        'url' => 'CHtml::normalizeUrl(array("/invoice/userView/id/". $data->id . "/type/2&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
        'delete' => array(
                        'url' => 'CHtml::normalizeUrl(array("/invoice/userView/id/". $data->id . "/type/3&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
        'htmlOptions' => array('style' => 'width: 100px;text-align: left;'),
      ),
    ),
	),
)); ?>

<script>
	function invoiceCreate(id) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/invoice/create");?>',{id:id},function(data){
                bsModalOpen(data);
            });
        
    }
</script>

