<style>
	.form #invoice-form{
		margin-bottom: unset;
	}
</style>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'invoice-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Invoice</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'invoice_number', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'invoice_number',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'invoice_number'); ?>

		<?php echo $form->labelEx($model,'total_quantity', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'total_quantity',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'total_quantity'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'total_amount', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'total_amount',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'total_amount'); ?>

		<?php echo $form->labelEx($model,'challan_no', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'challan_no',array('size'=>60,'maxlength'=>300,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'challan_no'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'challan_date', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'challan_date',array('size'=>60,'maxlength'=>150,'class'=>'datepicker form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'challan_date'); ?>

		<?php echo $form->labelEx($model,'invoice_date', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'invoice_date',array('size'=>60,'maxlength'=>300,'class'=>'datepicker form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'invoice_date'); ?>
	</div>


	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {});</script>