<?php
/* @var $this ProductGroupingController */
/* @var $model ProductGrouping */

$this->breadcrumbs=array(
	'Product Groupings'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductGrouping', 'url'=>array('index')),
	array('label'=>'Create ProductGrouping', 'url'=>array('create')),
	array('label'=>'View ProductGrouping', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductGrouping', 'url'=>array('admin')),
);
?>

<h1>Update ProductGrouping <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>