<?php
/* @var $this ProductGroupingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Product Groupings',
);

$this->menu=array(
	array('label'=>'Create ProductGrouping', 'url'=>array('create')),
	array('label'=>'Manage ProductGrouping', 'url'=>array('admin')),
);
?>

<h1>Product Groupings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
