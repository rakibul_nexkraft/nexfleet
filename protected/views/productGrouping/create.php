<?php
/* @var $this ProductGroupingController */
/* @var $model ProductGrouping */

$this->breadcrumbs=array(
	'Product Groupings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProductGrouping', 'url'=>array('index')),
	array('label'=>'Manage ProductGrouping', 'url'=>array('admin')),
);
?>

<h1>Create ProductGrouping</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>