<?php
/* @var $this MovementsController */
/* @var $model Movements */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>-->
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?><br />
		<?php echo $form->textField($model,'requisition_id',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?><br />
		<?php echo $form->textField($model,'user_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_name'); ?><br />
		<?php echo $form->textField($model,'user_name',array('size'=>18,'class'=>'input-medium','maxlength'=>40)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_level'); ?><br />
		<?php echo $form->textField($model,'user_level',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_dept'); ?><br />
		<?php echo $form->textField($model,'user_dept',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?><br />
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>

	<!--<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dutytype_id'); ?>
		<?php echo $form->textField($model,'dutytype_id'); ?>
	</div>-->
<div class="fl">
<div class="row">
    <?php echo $form->label($model,'driver_pin'); ?><br />
    <?php echo $form->textField($model,'driver_pin',array('size'=>18,'class'=>'input-medium')); ?>
</div>
</div>

<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'dutytype_id'); ?><br />
			<?php //echo $form->textField($model,'dutytype_id',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
			<?php 
				$duty_type = Dutytypes::model()->findAll(array('select'=>'id,type_name'));
				echo $form->dropDownList($model,'dutytype_id',CHtml::listData($duty_type,'id','type_name'),array('empty'=>'','style'=>'width:164px;'));
			?>
		</div>
	</div>

	<!--<div class="row">
		<?php echo $form->label($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_date'); ?>
		<?php echo $form->textField($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_time'); ?>
		<?php echo $form->textField($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_time'); ?>
		<?php echo $form->textField($model,'end_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_point'); ?>
		<?php echo $form->textField($model,'start_point',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_point'); ?>
		<?php echo $form->textField($model,'end_point',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_meter'); ?>
		<?php echo $form->textField($model,'start_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_meter'); ?>
		<?php echo $form->textField($model,'end_meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'night_halt'); ?>
		<?php echo $form->textField($model,'night_halt'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bill_amount'); ?>
		<?php echo $form->textField($model,'bill_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
	</div>-->
	
<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;'
				),
			));
		?>
	</div>
</div>
</fieldset>

	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->