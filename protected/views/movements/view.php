<?php
/* @var $this MovementsController */
/* @var $model Movements */

$this->breadcrumbs=array(
	'Log Book'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Log Books', 'url'=>array('index')),
		array('label'=>'New Log Book', 'url'=>array('create')),
		array('label'=>'Update Log Book', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete Log Book', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Log Books', 'url'=>array('admin')),
	);
}
?>

<h4>View Log Book : <?php echo $model->id; ?></h4>

<?php 
$model->dutytype_id = $model->dutytypes->type_name; 
$model->vehicletype_id = $model->vehicletypes->type;
?>

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'requisition_id',
		'user_pin',
		'user_name',
		'user_level',
		'user_dept',
		'vehicle_reg_no',
        'vehicletype_id',
		'driver_pin',
		'driver_name',
		'dutytype_id',
		'dutyday',
		'start_date',
		'end_date',
		'start_time',
		'end_time',
		'start_point',
		'end_point',
		'start_meter',
		'end_meter',
		'night_halt',
		'bill_amount',
		//'created_by',
		//'created_time',
		//'active',
	),
));*/ ?>

<?php $this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
                    'id',				'start_date',
                    'requisition_id',	'end_date',
                    'user_pin',			'start_time',
                    'user_name',		'end_time',
                    'user_level',		'start_point',
                    'user_dept',		'end_point',
                    'vehicle_reg_no',	'start_meter',
                    'vehicletype_id',	'end_meter',
                    'vehicle_location', 'driver_pin',
                    'night_halt',       'driver_name',
                    'helper_pin',       'dutytype_id',
                    'helper_name',      'dutyday',
                    'total_time',        'total_ot_hour',
                    array('name' => 'bill_amount', 'value' => $model->bill_amount." (BDT)"),array('name' => 'total_ot_amount', 'value' => $model->total_ot_amount." (BDT)"),
					'created_by',   'updated_by',
                    'created_time', 'updated_time'
			),
		),
    ),
)); 

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#yw0', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Detail Log Book',
));
?>