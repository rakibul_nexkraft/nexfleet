<?php
/* @var $this MovementsController */
/* @var $model Movements */
/* @var $form CActiveForm */
?>
<div style="width:50%; float: left">
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'movements-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<div class="container1">
		<div class="row">
		<?php echo $form->labelEx($model,'requisition_id'); ?>
			<?php echo $form->textField($model, 'requisition_id',	    
				array('onblur'=>CHtml::ajax(array('type'=>'GET', 
					'dataType'=>'json',

					'url'=>array("movements/getRequisitionData"),
		
					'success'=>"js:function(string){
					
					if(!string)
					{
					
						alert('Requisition ID not found or not approved');
							return;
							}
						
							$('#Movements_user_name').val(string.user_name);
							$('#Movements_user_pin').val(string.user_pin);
							$('#Movements_driver_name').val(string.driver_name);
							$('#Movements_driver_pin').val(string.driver_pin);
							$('#Movements_user_level').val(string.user_level);
							$('#Movements_user_dept').val(string.dept_name);
							$('#Billing_Code').val(string.billing_code);
							$('#Movements_user_level').val(string.user_level);
							$('#Movements_user_cell').val(string.user_cell);
							$('#Movements_email').val(string.email);
							$('#Movements_user_address').val(string.user_address	);
							$('#Movements_vehicle_reg_no').val(string.vehicle_reg_no);
							$('#Movements_vehicle_location').val(string.vehicle_location);
							$('#Movements_dutytype_id').val(string.dutytype_id);
							$('#Movements_vehicletype_id').val(string.vehicletype_id);
							$('#Movements_start_meter').val(string.start_meter);
							//$('#Movements_start_meter').attr('readonly', true);
						}"

				))),
				array('empty' => 'Select one of the following...')
	    
			);
	     ?>
		
		<?php echo $form->error($model,'requisition_id'); ?>
		</div>

		<!--<div class="row">
			<?php echo $form->labelEx($model,'user_pin'); ?>
			<?php echo $form->textField($model,'user_pin'); ?>
			<?php echo $form->error($model,'user_pin'); ?>
		</div>
-->
	<div>
				<?php echo $form->labelEx($model,'user_pin'); ?>
			<?php echo $form->textField($model, 'user_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("movements/CallHrUser"),
	
        'success'=>"js:function(string){
        				$('#Billing_Code').val(string.AccountsHeadShortCode);
                        $('#Movements_user_name').val(string.StaffName);
                        $('#Movements_user_level').val(string.JobLevel);
                        
                        var myarr = string.ProjectName;
                        //var myarr = myarr.split(' ');
                        var myvar = myarr.replace(' Department','');

                        $('#Movements_user_dept').val(myvar);
                        $('#Movements_user_cell').val(string.MobileNo);
                        $('#Movements_email').val(string.EmailID);												
                        $('#Movements_user_address').val(string.ContactAddress);
												
                       	  
                    }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'user_pin'); ?>
	</div>
		<div class="row">
			<?php echo $form->labelEx($model,'user_name'); ?>
			<?php echo $form->textField($model,'user_name',array('size'=>40,'maxlength'=>40)); ?>
			<?php echo $form->error($model,'user_name'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'user_level'); ?>
			<?php echo $form->textField($model,'user_level'); ?>
			<?php echo $form->error($model,'user_level'); ?>
		</div>
			<div class="row">
		<?php echo $form->labelEx($model,'user_dept'); ?>
		<?php //echo $form->textField($model,'dept_name',array('size'=>60,'maxlength'=>127)); ?>
     <?php $dept_name = Departments::model()->findAll(array('select'=>'name','order' => 'name ASC'));
        echo $form->dropDownList($model,'user_dept', CHtml::listData($dept_name,'name',  'name'),array('empty' => 'Select Department...')); ?>

		<?php echo $form->error($model,'user_dept'); ?>
	</div>
	
			<div class="row">
		<?php echo chtml::label('Billing Code'); ?>
		<?php echo chtml::textField('Billing Code','',array('size'=>60,'maxlength'=>127,'readonly'=>true)); ?>

	</div>


			<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	
		<div class="row">
		<?php echo $form->labelEx($model,'user_cell'); ?>
		<?php echo $form->textField($model,'user_cell',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'user_cell'); ?>
	</div>
	
	<!--<div class="row">
		<?php /*echo $form->labelEx($model,'user_address'); */?>
		<?php /*echo $form->textField($model,'user_address',array('size'=>60,'maxlength'=>127)); */?>
		<?php /*echo $form->error($model,'user_addresa'); */?>
	</div>-->
	

    <div class="row">
		<?php  echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php 	
		
		$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
		'model'=>$model,
		'attribute' => 'vehicle_reg_no',   
    'source'=>$this->createUrl('vehicles/getRegNo'),
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'2',
        'select'=>"js: function(event, ui) {
        
         $('#Movements_driver_pin').val(ui.item['driver_pin']);
         $('#Movements_helper_pin').val(ui.item['helper_pin']);
         $('#Movements_helper_name').val(ui.item['helper_name']);
         $('#Movements_vehicletype_id').val(ui.item['vehicletype_id']);
         $('#Movements_location').val(ui.item['vehicle_location']);
         $('#Movements_driver_name').val(ui.item['driver_name']);
         $('#Movements_start_meter').val(ui.item['start_meter']);
         $('#Movements_vehicle_location').val(ui.item['location']);
         if(ui.item['start_meter']==0)
         {
         
         	
         	//$('#Movements_start_meter').attr('readonly', false);
         	}
         	else
         	{
         	//$('#Movements_start_meter').attr('readonly', true);
         	}
         }"        
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));		?>
		
		
		<?php  echo $form->error($model,'vehicle_reg_no'); ?>
	  </div> 
	
		
		<div class="row">
			<?php echo $form->labelEx($model,'vehicletype_id'); ?>
			<?php $vehicletype_id = Vehicletypes::model()->findAll(array('select'=>'id,type','order' => 'id ASC'));
				echo $form->dropDownList($model,'vehicletype_id', CHtml::listData($vehicletype_id,'id',  'type'),array('empty' => 'Select Vehicles...')); ?>

			<?php // echo $form->textField($model,'vehicletype_id'); ?>
			<?php echo $form->error($model,'vehicletype_id'); ?>
		</div>

        <div class="row">
            <?php echo $form->labelEx($model,'vehicle_location'); ?>
            <?php echo $form->textField($model,'vehicle_location',array('size'=>60,'maxlength'=>127)); ?>
            <?php echo $form->error($model,'vehicle_locaiton'); ?>
        </div>
	
		<div class="row">
			<?php echo $form->labelEx($model,'driver_pin'); ?>
			
						<?php echo $form->textField($model, 'driver_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("movements/getName"),
	
        'success'=>"js:function(string){        
                    $('#Movements_driver_name').val(string.name);
         }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
			<?php echo $form->error($model,'driver_pin'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'driver_name'); ?>
			<?php echo $form->textField($model,'driver_name',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'driver_name'); ?>
		</div>

			<div class="row">
			<?php echo $form->labelEx($model,'dutytype_id'); ?>
			<?php $dutytype_id = Dutytypes::model()->findAll(array('select'=>'id,type_name','order' => 'id ASC'));
				echo $form->dropDownList($model,'dutytype_id', CHtml::listData($dutytype_id,'id',  'type_name'),array('empty' => 'Select Duty Type...')); ?>		
			<?php echo $form->error($model,'dutytype_id'); ?>
		</div>


	
	
	</div>
	<div class="container2">
		
	
		

		
		<div class="row">
			<?php echo $form->labelEx($model,'start_date'); ?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,'attribute'=>'start_date',

				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
						  'mode'=>'datetime',
						  'dateFormat' => 'yy-mm-dd', // save to db format
						  'changeMonth'=>'true',
						  'changeYear'=>'true',
						  'yearRange'=>'2000:2020',



                    'onSelect' => "js:
                            function(){
                                jQuery.ajax({
                                    'dataType':'html',
                                    'data':{'start_date': $('#Movements_start_date').val()},
                                    'type':'post',
                                    'success':function(result) {
                                        $(\"#Movements_end_date\").val($('#Movements_start_date').val());
                                        var start='';
                                        start=$('#Movements_start_date').val();

                                        var dayOfWeek = new Date($('#Movements_start_date').val()).getDay();

                                        //var day =  isNaN(dayOfWeek) ? null : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
                                        if(start=='2019-08-24'){
                                        	$(\"#Movements_dutyday\").val('Office Day');
                                        }
                                        else if (dayOfWeek ==5 || dayOfWeek ==6)
                                        {
                                            $(\"#Movements_dutyday\").val('Holiday');
                                        }
                                        else if(result)
                                        {
                                            $(\"#Movements_dutyday\").val('Holiday');
                                        }
                                        else
                                        {
                                            $(\"#Movements_dutyday\").val('Office Day');
                                        }


                                    },
                                        
                                        'url':'holidays/checkHoliday','cache':false
                                 });
                                 return false;
                            }
                
                         ",

				),

			'htmlOptions'=>array('style'=>'width:150px;'
			),
			)); ?>
			
			<?php echo $form->error($model,'start_date'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'end_date'); ?>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,'attribute'=>'end_date',

				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
						  'mode'=>'datetime',
						  'dateFormat' => 'yy-mm-dd', // save to db format
						  'changeMonth'=>'true',
						  'changeYear'=>'true',
						  'yearRange'=>'2000:2020',
				),

			'htmlOptions'=>array('style'=>'width:150px;'
			),
			)); ?>
			<?php echo $form->error($model,'end_date'); ?>
		</div>
		
		<div class="row">
			<?php
				echo $form->labelEx($model,'dutyday');?>
				<?php echo $form->dropDownList($model, 'dutyday',
				array('Office Day' => 'Office Day', 'Holiday' => 'Holiday'));	       
			?>		
			<?php echo $form->error($model,'dutyday'); ?>						
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'start_time'); ?>
				  <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
				  'model'     => $model,
				  'attribute' => 'start_time',
				  'language'=> 'en',//default Yii::app()->language
				  'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
				  
				  'options'   => array(
					  //'dateFormat' => 'dd.mm.yy',
					  'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
					  //'ampm' => 'false',                  
				   ),
				));
		?>			
		<?php echo $form->error($model,'start_time'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'end_time'); ?>
				<?php //echo $form->textField($model,'accident_time',array('maxlength'=>127)); ?>
		  <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
				  'model'     => $model,
				  'attribute' => 'end_time',
				  'language'=> 'en',//default Yii::app()->language
				  'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
				  
				  'options'   => array(
					  //'dateFormat' => 'dd.mm.yy',
					  'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
					 // 'ampm' => 'false',
					 ),
				));
		?>		
			<?php echo $form->error($model,'end_time'); ?>
		</div>
		
		

		<div class="row">
			<?php echo $form->labelEx($model,'start_point'); ?>
			<?php echo $form->textField($model,'start_point',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'start_point'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'end_point'); ?>
			<?php echo $form->textField($model,'end_point',array('size'=>60,'maxlength'=>127)); ?>
			<?php echo $form->error($model,'end_point'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'start_meter'); ?>
			<!--<?php echo $form->textField($model,'start_meter',array('readonly'=>false)); ?>-->
			<?php echo $form->textField($model,'start_meter'); ?>
			<label id="lblDiff" style=color:red;></label>
			<?php echo $form->error($model,'start_meter'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'end_meter'); ?>
			<?php echo $form->textField($model,'end_meter',
			array('onblur'=>
			
			"js:
			var meterDiff = $('#Movements_end_meter').val()-$('#Movements_start_meter').val();
			if(meterDiff <= '0'){alert('End Meter can\'t be less or equal to Start Meter');return false;}
			document.getElementById('lblDiff').innerHTML='Meter Difference: '+meterDiff"
			)
			); ?>
			
			<?php echo $form->error($model,'end_meter'); ?>
		</div>
		
			<div class="row">
		<?php 
			
			echo $form->labelEx($model,'night_halt');?>
			<?php echo $form->dropDownList($model, 'night_halt',
			array('0' => 'No','1' => 'Yes'));	    
			   
			 ?>		
			<?php echo $form->error($model,'night_halt'); ?>
		</div>
	<div class="row">
		<?php 
			
			echo $form->labelEx($model,'morning_ta');?>
			<?php echo $form->radioButton($model,'morning_ta',array('value'=>'1','uncheckValue'=>null)); ?>Yes &nbsp; &nbsp;
			<?php echo $form->radioButton($model,'morning_ta',array('value'=>'0','uncheckValue'=>null)); ?>No
			<?php echo $form->error($model,'morning_ta'); ?>
		</div>
		
		<div class="row">
		<?php 
			
			echo $form->labelEx($model,'lunch_ta');?>
			<?php echo $form->radioButton($model,'lunch_ta',array('value'=>'1','uncheckValue'=>null)); ?>Yes &nbsp; &nbsp;
			<?php echo $form->radioButton($model,'lunch_ta',array('value'=>'0','uncheckValue'=>null)); ?>No
			<?php echo $form->error($model,'lunch_ta'); ?>
		</div>
		
		<div class="row">
		<?php 
			
			echo $form->labelEx($model,'night_ta');?>
			<?php echo $form->radioButton($model,'night_ta',array('value'=>'1','uncheckValue'=>null)); ?>Yes &nbsp; &nbsp;
			<?php echo $form->radioButton($model,'night_ta',array('value'=>'0','uncheckValue'=>null)); ?>No
			<?php echo $form->error($model,'night_ta'); ?>
		</div>
		
			<div>
				<?php echo $form->labelEx($model,'helper_pin'); ?>
			<?php echo $form->textField($model, 'helper_pin',	    
	    array('onblur'=>CHtml::ajax(array('type'=>'GET', 
				 'dataType'=>'json',

		    'url'=>array("movements/callHrUserForHelper"),
	
        'success'=>"js:function(string){
                        $('#Movements_helper_name').val(string.StaffName);                        
                        
                    }"

		    ))),
		    array('empty' => 'Select one of the following...')
	    
	    );
	     ?>
		
		<?php echo $form->error($model,'helper_pin'); ?>
	</div>
		<div class="row">
			<?php echo $form->labelEx($model,'helper_name'); ?>
			<?php echo $form->textField($model,'helper_name',array('size'=>40,'maxlength'=>40)); ?>
			<?php echo $form->error($model,'helper_name'); ?>
		</div>




	</div>
	<!--div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<div style="width: 50%;float: right;" id='requisition_info'>
	<?php if(!empty($model->requisition_id)){
			$Requisitions=Requisitions::model()->findByPk($model->requisition_id);
			$this->widget('zii.widgets.XDetailView', array(
			    'data'=>$Requisitions,
			    'attributes'=>array(
			        'group2'=>array(
			            'ItemColumns' => 2,
			            'attributes' => array(
			                    'id',			'supervisor_name',
			                    'bpmt_ref_no', 	'supervisor_pin',
			                    'vehicle_reg_no', 'supervisor_level',
			                    array('name'=>'vehicletype_id', 'value'=>$Requisitions->vehicletypes->type),	'created_time',
			                    'user_pin',		'updated_time',
			                    'user_name',	'ifleet_approval_time',
			                    'user_level',	'approve_status',
			                    'user_cell',	'driver_pin',
			                    'dept_name',	'driver_name',
			                    'email',	'driver_phone',

								'start_point',		array('name'=>'dutytype_id','value'=>$Requisitions->dutytypes->type_name),
								'end_point',		array('name' => 'active', 'value' => Requisitions::model()->statusActive($Requisitions->active)),
								'start_date',		'vehicle_location',
			                    'end_date',			'created_by',
								'start_time',		'updated_by',
								'end_time',			'remarks',
								'passenger',         'cancel_status_by_user',
						),
			        ),
			    ),
			));
	 ?>
	<?php } ?>
</div>
<script>
    $(document).ready(function () {

        $('#Movements_start_date').on('blur', function () {

            jQuery.ajax({
                'dataType': 'html',
                'data': {'start_date': $('#Movements_start_date').val()},
                'type': 'post',
                'success': function (result) {
                    $('#Movements_end_date').val($('#Movements_start_date').val());
                    var dayOfWeek = new Date($('#Movements_start_date').val()).getDay();

                    //var day =  isNaN(dayOfWeek) ? null : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
                    if($('#Movements_start_date').val()=="2019-08-24"){
                    	 $('#Movements_dutyday').val('Office Day');
                    }

                    else if (dayOfWeek == 5 || dayOfWeek == 6) {
                        $('#Movements_dutyday').val('Holiday');
                    }
                    else if (result) {
                        $('#Movements_dutyday').val('Holiday');
                    }
                    else {
                        $('#Movements_dutyday').val('Office Day');
                    }
                },

                'url': '<?php echo Yii::app()->createAbsoluteUrl("holidays/checkHoliday");?>', 'cache': false
            });


        });
        $('#Movements_requisition_id').on('blur', function () {
        	var id=$('#Movements_requisition_id').val();
        	$.post('<?php echo Yii::app()->createAbsoluteUrl("requisitions/getView");?>',{id:id,},function(data){
				$('#requisition_info').html(data);
				

				});
		
        });


    });

</script>