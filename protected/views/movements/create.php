<?php
/* @var $this MovementsController */
/* @var $model Movements */

$this->breadcrumbs=array(
	'Log Book'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Log Books List', 'url'=>array('index')),
		array('label'=>'Manage Log Books', 'url'=>array('admin')),
	);
}
?>

<h4>New Log Book</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>