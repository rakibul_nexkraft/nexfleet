<?php
/* @var $this MovementsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Log Book Excel Request Mail',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Log Book', 'url'=>array('create')),
		array('label'=>'Manage Log Books', 'url'=>array('admin')),
	);
}
?>

<h4>Log Book</h4>
<?php 
    echo "<p style='text-align: center'>Thanks, we're processing your report request. It should be ready to download shortly in the table on this page. In certain cases it may take longer depending on the volume of activity.</br>After completing your request, you will get a download link via email.</p>";
?>

