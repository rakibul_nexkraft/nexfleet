<?php
/* @var $this MovementsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Log Book',
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'New Log Book', 'url'=>array('create')),
    	array('label'=>'Manage Log Books', 'url'=>array('admin')),
    );
}
?>

<h4>Log Book</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'movements-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'ajaxUpdate'=>false,
    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'requisition_id',
        'driver_pin',
        'driver_name',
        'user_pin',
        'user_name',
        'user_level',
        'user_dept',
        'vehicle_reg_no',
        'vehicle_location',
		'start_date',
        'end_date',
		'start_time',
        'end_time',
        'end_point',
        'start_meter',
        'end_meter',
        'total_run',
		array(
			'name' => 'dutytype_id',
			'type' => 'raw',
			'value' => 'CHtml::encode($data->dutytypes->type_name)'
		),
		array(
			'name' => 'bill_amount',
			'type' => 'raw',
			'header' => 'Bill Amount (BDT)',
			'value' => '$data->bill_amount'
		),
		'total_ot_hour',
        'total_ot_amount',
        array(
            'name' => 'digi_id',
            'type' => 'raw',           
            'value' => 'CHtml::link(CHtml::encode($data->digi_id),array("/digitalLogBook/view","id"=>$data->digi_id))'
        ),
        /*
        

        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'start_point',
        'start_meter',
        'end_meter',
        'night_halt',
        'bill_amount',
        'created_by',
        'created_time',
        'active',
        */
    ),
)); 

?>

<?php
$total=$model->search()->getTotalItemCount();
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Movements'],'totalSheet'=>$total)));
?> &nbsp; &nbsp; &nbsp;

<?php
$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#movements-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Log Book List',
));

?>