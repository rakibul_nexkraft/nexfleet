<?php
/* @var $this MovementsController */
/* @var $model Movements */

$this->breadcrumbs=array(
	'Log Book'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Log Books', 'url'=>array('index')),
		array('label'=>'New Log Book', 'url'=>array('create')),
		array('label'=>'View Log Book', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Log Books', 'url'=>array('admin')),
	);
}
?>

<h4>Update Log Book : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>