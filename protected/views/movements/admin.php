<?php
/* @var $this MovementsController */
/* @var $model Movements */

$this->breadcrumbs=array(
	'Log Book'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Log Books List', 'url'=>array('index')),
		array('label'=>'New Log Books', 'url'=>array('create')),
	);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('movements-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Log Book</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'movements-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'requisition_id',
        'driver_pin',
        'driver_name',
		'user_pin',
		'user_name',
		'user_level',
		'user_dept',
        'vehicle_reg_no',
        'start_date',
        'end_date',
        'dutyday',
        'night_halt',
        'bill_amount',
		'total_ot_hour',
        'total_ot_amount',
        /*
	    'dutytype_id',
		'start_time',
		'end_time',
		'start_point',
		'end_point',
		'start_meter',
		'end_meter',

		'created_by',
		'created_time',
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
			'buttons'=>array('view'=>
				array(
					'url'=>'Yii::app()->createUrl("movements/view", array("id"=>$data->id))',
					'options'=>array(  
						'ajax'=>array(
							'type'=>'GET',
								// ajax post will use 'url' specified above 
							'url'=>"js:$(this).attr('href')",
							//'update'=>'#id_view',
							'success'=>'function(data){$("#dlg-address-view").dialog("open"); $("#id_view").html(data); return false;}',
						),
					),
				),
			),
		),
	),
));

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
	'id'=>'dlg-address-view',
	'options'=>array(
		'title'=>'Detail view',
		'autoOpen'=>false, //important!
		'modal'=>false,
		'width'=>750,
		'height'=>'auto',
		'buttons'=>array('Close'=>'js:function(){$(this).dialog("close");}'),
	),
)); ?>
<div id="id_view"></div>
<?php $this->endWidget(); ?>