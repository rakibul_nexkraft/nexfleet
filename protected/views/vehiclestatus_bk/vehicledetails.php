<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/15/2019
 * Time: 12:37 PM
 */
?>
<style>
    .loading-anim{

        /*background: url(< ?php echo Yii::app()->baseUrl?>'/common/assets/imagimages/ellipsis-loader.gif') no-repeat center center;*/

        display: block;
        width: 100%;
        height: 100%;
        background: url("<?php echo Yii::app()->baseUrl?>/common/assets/images/preloader.gif") center center no-repeat #FFF;
    }
</style>
<!--<pre>< ?php var_dump($vehicle->attributes) ?></pre>-->
<div class="col_full animated fadeIn">
    <h3>Current Location</h3>

    <div class="map-container">
        <?php
        /*CHECK IF VEHICLE HAS ASSET ID*/
        if (is_null($vehicle->asset_id) || strlen($vehicle->asset_id) < 20) {
            echo "<div id='page-title' class='page-title-center'><div class='clearfix'><h1>Vehicle Asset ID not found</h1><span>Please search for another vehicle</span></div></div>";
        } else {
            echo '<div id="map"></div>';
        }
        ?>

        <!--<div id="map-overlay">
            <div id="google-map">
                <div class="nk-loader" style=""></div>
            </div>
        </div>-->
    </div>
</div>
<input type="hidden" id="vehicle_data" value="<?php echo $vehicle['reg_no'].','.$vehicle['asset_id'] ?>"">

<div class="clear"></div>

<div class="col_half animated fadeIn">
    <div class="fancy-title title-border">
        <h4>Driver Information</h4>
    </div>

    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur facilis cumque amet suscipit odio.</p>-->

    <ul class="iconlist nobottommargin">
        <li><i class="icon-time color"></i> <strong>Driver Name:</strong> <?php echo $vehicle['driver_name'] ?></li>
        <li><i class="icon-time color"></i> <strong>Driver PIN:</strong> <?php echo $vehicle['driver_pin'] ?></li>
        <li><i class="icon-time color"></i> <strong>Mobile:</strong> <?php echo '01304882244' ?></li>
    </ul>
    <!--<div class="panel panel-default product-meta">
        <div class="panel-body">
            <span itemprop="productID" class="sku_wrapper">SKU: <span class="sku">8465415</span></span>
            <span class="posted_in">Category: <a href="#" rel="tag">Dress</a>.</span>
            <span class="tagged_as">Tags: <a href="#" rel="tag">Pink</a>, <a href="#" rel="tag">Short</a>, <a href="#" rel="tag">Dress</a>, <a href="#" rel="tag">Printed</a>.</span>
        </div>
    </div>-->
    <!--<div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
            <strong class="d-inline-block mb-2 text-primary">World</strong>
            <h3 class="mb-0">
                <a class="text-dark" href="#">Featured post</a>
            </h3>
            <div class="mb-1 text-muted">Nov 12</div>
            <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
            <a href="#">Continue reading</a>
        </div>

    </div>-->
</div>
<div class="col_half col_last animated fadeIn">
    <div class="fancy-title title-border">
        <h4>Regional Office</h4>
    </div>

    <!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam aspernatur facilis cumque amet suscipit odio.</p>-->

    <ul class="iconlist nobottommargin">
        <li><i class="icon-time color"></i> <strong>Address:</strong> 58 Lake Circus, Kalabagan, Dhanmondi, Dhaka 1207</li>
        <li><i class="icon-time color"></i> <strong>Weekdays:</strong> Sunday - Thursday 8.30AM to 5PM</li>
        <li><i class="icon-time text-danger"></i> <strong>Closed:</strong> Friday & Saturday</li>
    </ul>
    <!--<div class="panel panel-default product-meta">
        <div class="panel-body">
            <span itemprop="productID" class="sku_wrapper">SKU: <span class="sku">8465415</span></span>
            <span class="posted_in">Category: <a href="#" rel="tag">Dress</a>.</span>
            <span class="tagged_as">Tags: <a href="#" rel="tag">Pink</a>, <a href="#" rel="tag">Short</a>, <a href="#" rel="tag">Dress</a>, <a href="#" rel="tag">Printed</a>.</span>
        </div>
    </div>-->
    <!--<div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
            <strong class="d-inline-block mb-2 text-primary">World</strong>
            <h3 class="mb-0">
                <a class="text-dark" href="#">Featured post</a>
            </h3>
            <div class="mb-1 text-muted">Nov 12</div>
            <p class="card-text mb-auto">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
            <a href="#">Continue reading</a>
        </div>

    </div>-->
</div>

<div class="clear"></div>

<div class="col_full animated fadeIn">
    <div class="fancy-title title-border">
        <h4>Upcoming 7 Days Requisition</h4>
    </div>
    <div class="table-responsive" id="requisitionListTable"></div>
</div>

<div id="requisitionDetailModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="form-modal" class="modal-body">
                <div id="view-body">Loading...</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyiFpj3wAKztW2X7NUkBSW0CW_lXvG45E&amp;libraries=places"></script>-->
<script>
//    var markerIcon = "< ?php echo Yii::app()->baseUrl; ?>/assets/icons/new/car-map.png";
//    var map;
//    var marker;
    // Initialize and add the map

    <?php
//    if (!is_null($vehicle->asset_id) && strlen($vehicle->asset_id) > 20) {
//        echo "$('map').html(\"<div class='loading-anim'></div><div class='center' style='font-size: 16px;position: relative;bottom: 7rem;'>Loading vehicle location ...</div>\");";
//    }
    ?>
    function mapInitWrapper() {
        $('#map').html("<div class='loading-anim'></div><div class='center' style='font-size: 16px;position: relative;bottom: 7rem;'>Loading vehicle location ...</div>");
        initMap();
    }

    function initMap() {
        var location = {};
        var vehicleData = document.getElementById('vehicle_data').value;
        jQuery.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/getVehicleLocation");?>',
            dataType:'json',
            data: {vehicle_data: vehicleData},
            success: function(data){
                if (data.success){
//                    document.getElementById('map').innerHTML = data.message;
                    // The location of Uluru
                    var v = data.coordinates.split(",");
                    var uluru = {lat: parseFloat(v[0]), lng: parseFloat(v[1])};
                    // The map, centered at Uluru
                    var map = new google.maps.Map(
                        document.getElementById('map'), {zoom: 14, center: uluru});
                    // The marker, positioned at Uluru
                    var marker = new google.maps.Marker({position: uluru, map: map});
                } else {
                    document.getElementById('map').innerHTML = data.message;
                }
            }
        });
    }

    setInterval(initMap, 60000)

</script>
<!--Load the API from the specified URL
* The async attribute allows the browser to render the page while the API loads
* The key parameter will contain your own API key (which is not needed for this tutorial)
* The callback parameter executes the initMap() function
-->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyiFpj3wAKztW2X7NUkBSW0CW_lXvG45E&callback=mapInitWrapper">
</script>

<script async>
    function getRequisitionList() {
        var vehicleData = document.getElementById('vehicle_data').value;
        //console.log(vehicleData);
        jQuery.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/getRequisitionList");?>',
            dataType:'html',
            data: {vehicle_data: vehicleData},
            success: function(html){
                $('#requisitionListTable').html(html);
            }
        });
    }
    getRequisitionList();
    setInterval(getRequisitionList, 60000)

    function requisitionDetail(id) {
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/requisitionDetailView");?>',
            data:{id:id},
            success: function (html) {
                $("#requisitionDetailModal").modal("show");
                $("#view-body").html(html);
                return false;
            }
        });
    }
</script>