<?php
/* @var $this DefectRequestAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Defect Request Admins',
);

/*$this->menu=array(
	array('label'=>'Create Defect Request', 'url'=>array('create')),
	array('label'=>'Manage DefectRequest', 'url'=>array('admin')),
);*/
?>

<h1>Defects Request </h1>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'defect-request-admin-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'driver_pin',
		'category',
		'sub_category',
		'reg_no',
		'meter',		
		'apply_time',
		'admin_pin',
		'workshop_pin',
		array('name' =>'admin_status',
			'type' =>'raw',
			'value'=>'$data->admin_status==0 ? "Pending" :(($data->admin_status==1) ? "Approved" : "Reject")',
			'filter' => array('0'=>'Pending','1'=>'Approved','-1'=>'Reject'),
		 ),
		array('name' =>'workshop_status',
			'type' =>'raw',
			'value'=>'$data->workshop_status==0 ? "Pending" :(($data->workshop_status==1) ? "Approved" : "Reject")',
			'filter' => array('0'=>'Pending','1'=>'Approved','-1'=>'Reject'),
		 ),
		
		'reason',
		/*'created_time',
		'workshop_time',
		'admin_time',
		'created_by',
		'updated_by',
		'update_time',		
		*/
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
)); ?>