<?php
/* @var $this DefectRequestAdminController */
/* @var $model DefectRequestAdmin */

$this->breadcrumbs=array(
	'Defect Request Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DefectRequestAdmin', 'url'=>array('index')),
	array('label'=>'Manage DefectRequestAdmin', 'url'=>array('admin')),
);
?>

<h1>Create DefectRequestAdmin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>