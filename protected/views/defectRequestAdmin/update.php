<?php
/* @var $this DefectRequestAdminController */
/* @var $model DefectRequestAdmin */

$this->breadcrumbs=array(
	'Defect Request Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List DefectRequestAdmin', 'url'=>array('index')),
	array('label'=>'Create DefectRequestAdmin', 'url'=>array('create')),
	array('label'=>'View DefectRequestAdmin', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage DefectRequestAdmin', 'url'=>array('admin')),
);
?>

<h1>Update DefectRequestAdmin <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>