<?php
/* @var $this DefectRequestAdminController */
/* @var $model DefectRequestAdmin */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category'); ?>
		<?php echo $form->textField($model,'category',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sub_category'); ?>
		<?php echo $form->textField($model,'sub_category',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reg_no'); ?>
		<?php echo $form->textField($model,'reg_no',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meter'); ?>
		<?php echo $form->textField($model,'meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apply_time'); ?>
		<?php echo $form->textField($model,'apply_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'admin_pin'); ?>
		<?php echo $form->textField($model,'admin_pin',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'workshop_pin'); ?>
		<?php echo $form->textField($model,'workshop_pin',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'admin_status'); ?>
		<?php echo $form->textField($model,'admin_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'workshop_status'); ?>
		<?php echo $form->textField($model,'workshop_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'workshop_time'); ?>
		<?php echo $form->textField($model,'workshop_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'admin_time'); ?>
		<?php echo $form->textField($model,'admin_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reason'); ?>
		<?php echo $form->textArea($model,'reason',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->