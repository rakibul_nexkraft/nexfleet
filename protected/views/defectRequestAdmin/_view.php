<?php
/* @var $this DefectRequestAdminController */
/* @var $data DefectRequestAdmin */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category')); ?>:</b>
	<?php echo CHtml::encode($data->category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub_category')); ?>:</b>
	<?php echo CHtml::encode($data->sub_category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter')); ?>:</b>
	<?php echo CHtml::encode($data->meter); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apply_time')); ?>:</b>
	<?php echo CHtml::encode($data->apply_time); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_pin')); ?>:</b>
	<?php echo CHtml::encode($data->admin_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workshop_pin')); ?>:</b>
	<?php echo CHtml::encode($data->workshop_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_status')); ?>:</b>
	<?php echo CHtml::encode($data->admin_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workshop_status')); ?>:</b>
	<?php echo CHtml::encode($data->workshop_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('workshop_time')); ?>:</b>
	<?php echo CHtml::encode($data->workshop_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_time')); ?>:</b>
	<?php echo CHtml::encode($data->admin_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reason')); ?>:</b>
	<?php echo CHtml::encode($data->reason); ?>
	<br />

	*/ ?>

</div>