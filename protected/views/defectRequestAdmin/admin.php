<?php
/* @var $this DefectRequestAdminController */
/* @var $model DefectRequestAdmin */

$this->breadcrumbs=array(
	'Defect Request Admins'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List DefectRequestAdmin', 'url'=>array('index')),
	array('label'=>'Create DefectRequestAdmin', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('defect-request-admin-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Defect Request Admins</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'defect-request-admin-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'driver_pin',
		'category',
		'sub_category',
		'reg_no',
		'meter',
		/*
		'apply_time',
		'admin_pin',
		'workshop_pin',
		'admin_status',
		'workshop_status',
		'created_time',
		'workshop_time',
		'admin_time',
		'created_by',
		'updated_by',
		'update_time',
		'reason',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
