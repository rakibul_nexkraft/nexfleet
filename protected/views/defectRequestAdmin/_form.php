<?php
/* @var $this DefectRequestAdminController */
/* @var $model DefectRequestAdmin */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'defect-request-admin-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?>
		<?php echo $form->textField($model,'category',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sub_category'); ?>
		<?php echo $form->textField($model,'sub_category',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'sub_category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reg_no'); ?>
		<?php echo $form->textField($model,'reg_no',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meter'); ?>
		<?php echo $form->textField($model,'meter'); ?>
		<?php echo $form->error($model,'meter'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apply_time'); ?>
		<?php echo $form->textField($model,'apply_time'); ?>
		<?php echo $form->error($model,'apply_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_pin'); ?>
		<?php echo $form->textField($model,'admin_pin',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'admin_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workshop_pin'); ?>
		<?php echo $form->textField($model,'workshop_pin',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'workshop_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_status'); ?>
		<?php echo $form->textField($model,'admin_status'); ?>
		<?php echo $form->error($model,'admin_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workshop_status'); ?>
		<?php echo $form->textField($model,'workshop_status'); ?>
		<?php echo $form->error($model,'workshop_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'workshop_time'); ?>
		<?php echo $form->textField($model,'workshop_time'); ?>
		<?php echo $form->error($model,'workshop_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_time'); ?>
		<?php echo $form->textField($model,'admin_time'); ?>
		<?php echo $form->error($model,'admin_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
		<?php echo $form->error($model,'update_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textArea($model,'reason',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'reason'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->