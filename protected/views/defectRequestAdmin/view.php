<?php
/* @var $this DefectRequestAdminController */
/* @var $model DefectRequestAdmin */

$this->breadcrumbs=array(
	'Defect Request Admins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List DefectRequestAdmin', 'url'=>array('index')),
	array('label'=>'Create DefectRequestAdmin', 'url'=>array('create')),
	array('label'=>'Update DefectRequestAdmin', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DefectRequestAdmin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DefectRequestAdmin', 'url'=>array('admin')),
);
?>

<h1>View DefectRequestAdmin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'driver_pin',
		'category',
		'sub_category',
		'reg_no',
		'meter',
		'apply_time',
		'admin_pin',
		'workshop_pin',
		'admin_status',
		'workshop_status',
		'created_time',
		'workshop_time',
		'admin_time',
		'created_by',
		'updated_by',
		'update_time',
		'reason',
	),
)); ?>
