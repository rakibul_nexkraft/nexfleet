<?php
/* @var $this ProductAttributeController */
/* @var $model ProductAttribute */

$this->breadcrumbs=array(
	'Product Attributes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ProductAttribute', 'url'=>array('index')),
	array('label'=>'Create ProductAttribute', 'url'=>array('create')),
);

?>

<?php #$this->renderPartial('_search',array(
	#'model'=>$model,
#)); ?>
</div><!-- search-form -->

<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin">
    <h4 class="heading-custom page_header_h4">List of Product Attributes</h4>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-attribute-grid',
	'itemsCssClass' => 'table cart table_um',
    'htmlOptions' => array('class' => 'table-responsive bottommargin, admin-table-responsive border-round-bottom'),
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
    array(
            'name' => 'product_id',
            'type' => 'raw',
            'value' => 'Product::getProductName($data->product_id)',
            ),
		/*'category',*/
		'label',
		'value',
    array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => 'Yii::app()->user->getUserName($data->created_by)',
            ),
		/*'created_time',
		'updated_by',
		'updated_time',*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view} {update} {delete}',
			'buttons'=> array(
				'view' => array(
                        'url' => 'CHtml::normalizeUrl(array("/productAttribute/userView/id/". $data->id . "/type/1&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
				'update' => array(
                        'url' => 'CHtml::normalizeUrl(array("/productAttribute/userView/id/". $data->id . "/type/2&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
        'delete' => array(
                        'url' => 'CHtml::normalizeUrl(array("/productAttribute/userView/id/". $data->id . "/type/3&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
				'htmlOptions' => array('style' => 'width: 100px;text-align: left;'),
			),
		),
	),
)); ?>

<script>
	function productAttributeCreate(id) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/ProductAttribute/create");?>',{id:id},function(data){
                bsModalOpen(data);
            });
        
    }
</script>