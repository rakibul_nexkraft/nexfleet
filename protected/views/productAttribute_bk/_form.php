<?php
/* @var $this ProductAttributeController */
/* @var $model ProductAttribute */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-attribute-form',
	'enableAjaxValidation'=>false,
)); 
$all_attributes=array(
    "p_model" => "Model",
    "p_type" => "Type",
    "p_color" => "Color",
    "p_size" => "Size",
    "p_shape" => "Shape",
    "p_volume" => "Volume",
);
?>

<div class="modal-header">
		<h3 class="modal-title" id="exampleModalLabel">Create Product Attribute</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row input-field">
		<?php echo $form->dropDownList($model,'category',$all_attributes ,array('empty' => '* Select Category')); ?>
		<?php echo $form->error($model,'category'); ?>
	</div>
	
	<div class="row input-field">
		<?php echo $form->textField($model,'label',array('size'=>60,'maxlength'=>150, 'placeholder'=> '* Label')); ?>
		<?php echo $form->error($model,'label'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>300, 'placeholder'=> '* Value')); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->textField($model,'product_id',array('placeholder'=> '* Product')); ?>
		<?php echo $form->error($model,'product_id'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
		
		
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>

		
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->