<style>
	.detail-view{
		width: 100% !important;
	}
</style>

<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3 class="modal-title"><?php if($type==1) { echo ('Update Product Attribute')." ".$model->id; }
			if($type==2) {echo ('View Product Attribute')." ".$model->id;} ?></h3>
		</div>
		<div class="modal-body">
			<div class="well" style="margin-bottom: 0px;">
				<?php if($type==1) {
					?>
						<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-attribute-form',
	'enableAjaxValidation'=>false,
)); 
$all_attributes=array(
    "p_model" => "Model",
    "p_type" => "Type",
    "p_color" => "Color",
    "p_size" => "Size",
    "p_shape" => "Shape",
    "p_volume" => "Volume",
);
?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row input-field">
		<?php echo $form->dropDownList($model,'category',$all_attributes ,array('empty' => '* Select Category')); ?>
		<?php echo $form->error($model,'category'); ?>
	</div>
	
	<div class="row input-field">
		<?php echo $form->textField($model,'label',array('size'=>60,'maxlength'=>150, 'placeholder'=> '* Label')); ?>
		<?php echo $form->error($model,'label'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->textField($model,'value',array('size'=>60,'maxlength'=>300, 'placeholder'=> '* Value')); ?>
		<?php echo $form->error($model,'value'); ?>
	</div>

	<div class="row input-field">
		<?php echo $form->textField($model,'product_id',array('placeholder'=> '* Product')); ?>
		<?php echo $form->error($model,'product_id'); ?>
	</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>

						</div>
					</div><!-- form -->

					<?php $this->endWidget(); ?>
				<?php }
				
				
				if($type==2) {
					
					$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'category',
		'label',
		'value',
		'product_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
));
				}

				?>

			</div>
		</div>

		<?php if($type==2) {
			?>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
			</div>
		<?php }else{?>

		<?php }?>
	</div>
</div>
