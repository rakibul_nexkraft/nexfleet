<?php
/* @var $this AccidentAppController */
/* @var $model AccidentApp */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'accident-app-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'own_car_damage_left'); ?>
		<div style="margin-left: 5.5rem;">
			<?php echo $form->checkbox($model,'own_car_damage_left',array('value'=>'1','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'own_car_damage_left'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'own_car_damage_right'); ?>
		<div style="margin-left: 5rem;">
			<?php echo $form->checkbox($model,'own_car_damage_right',array('value'=>'0','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'own_car_damage_right'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'own_car_damage_back'); ?>
		<div style="margin-left: 5.2rem;">
			<?php echo $form->checkbox($model,'own_car_damage_back',array('value'=>'1','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'own_car_damage_back'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'own_car_damage_front'); ?>
		<div style="margin-left: 5rem;">
			<?php echo $form->checkbox($model,'own_car_damage_front',array('value'=>'NO','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'own_car_damage_front'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'other_car_damage_left'); ?>
		<div style="margin-left: 5.3rem;">
			<?php echo $form->checkbox($model,'other_car_damage_left',array('value'=>'NO','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'other_car_damage_left'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'other_car_damage_right'); ?>
		<div style="margin-left: 4.8rem;">
			<?php echo $form->checkbox($model,'other_car_damage_right',array('value'=>'NO','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'other_car_damage_right'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'other_car_damage_back'); ?>
		<div style="margin-left: 4.9rem;">
			<?php echo $form->checkbox($model,'other_car_damage_back',array('value'=>'NO','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'other_car_damage_back'); ?>
	</div>

	<div class="row" style="display: flex;">
		<?php echo $form->labelEx($model,'other_car_damage_front'); ?>
		<div style="margin-left: 4.9rem;">
			<?php echo $form->checkbox($model,'other_car_damage_front',array('value'=>'NO','uncheckValue'=>null)); ?>
		</div>
		<?php echo $form->error($model,'other_car_damage_front'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'settlement_type'); ?>
		<?php echo $form->textField($model,'settlement_type'); ?>
		<?php echo $form->error($model,'settlement_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remark'); ?>
		<?php echo $form->textField($model,'remark',array('size'=>60,'maxlength'=>2000)); ?>
		<?php echo $form->error($model,'remark'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accident_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'accident_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    #'defaultDate'=>$model->end_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                    //'minDate' => date("Y-m-d"),
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'accident_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accident_time'); ?>
		<?php $this->widget('ext.jui.EJuiDateTimePicker', array(
                'model'     => $model,
                'attribute' => 'accident_time',
                'language'=> 'en',//default Yii::app()->language
                'mode'    => 'time',//'datetime' or 'time' ('datetime' default)
                
                'options'   => array(
                    //'dateFormat' => 'dd.mm.yy',
                    'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                    //'ampm' => 'true',
                ),
                //'htmlOptions'=>array('readonly'=>'readonly'),
            )); ?>
		<?php echo $form->error($model,'accident_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'accident_place'); ?>
		<?php echo $form->textField($model,'accident_place',array('size'=>60,'maxlength'=>180)); ?>
		<?php echo $form->error($model,'accident_place'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->