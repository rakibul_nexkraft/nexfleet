<?php
/* @var $this AccidentAppController */
/* @var $model AccidentApp */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'own_car_damage_left'); ?>
		<?php echo $form->textField($model,'own_car_damage_left'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'own_car_damage_right'); ?>
		<?php echo $form->textField($model,'own_car_damage_right'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'own_car_damage_back'); ?>
		<?php echo $form->textField($model,'own_car_damage_back'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'own_car_damage_front'); ?>
		<?php echo $form->textField($model,'own_car_damage_front'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'other_car_damage_left'); ?>
		<?php echo $form->textField($model,'other_car_damage_left'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'other_car_damage_right'); ?>
		<?php echo $form->textField($model,'other_car_damage_right'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'other_car_damage_back'); ?>
		<?php echo $form->textField($model,'other_car_damage_back'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'other_car_damage_front'); ?>
		<?php echo $form->textField($model,'other_car_damage_front'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'settlement_type'); ?>
		<?php echo $form->textField($model,'settlement_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remark'); ?>
		<?php echo $form->textField($model,'remark',array('size'=>60,'maxlength'=>2000)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accident_time'); ?>
		<?php echo $form->textField($model,'accident_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accident_place'); ?>
		<?php echo $form->textField($model,'accident_place',array('size'=>60,'maxlength'=>180)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->