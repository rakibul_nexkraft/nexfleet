<?php
/* @var $this AccidentAppController */
/* @var $model AccidentApp */

$this->breadcrumbs=array(
	'Accident Apps'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List AccidentApp', 'url'=>array('index')),
		array('label'=>'Create AccidentApp', 'url'=>array('create')),
		array('label'=>'View AccidentApp', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage AccidentApp', 'url'=>array('admin')),
	);
}
?>

<h1>Update AccidentApp <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>