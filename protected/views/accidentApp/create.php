<?php
/* @var $this AccidentAppController */
/* @var $model AccidentApp */

$this->breadcrumbs=array(
	'Accident Apps'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List AccidentApp', 'url'=>array('index')),
		array('label'=>'Manage AccidentApp', 'url'=>array('admin')),
	);
}
?>

<h1>Create AccidentApp</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>