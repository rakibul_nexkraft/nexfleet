<?php
/* @var $this AccidentAppController */
/* @var $model AccidentApp */

$this->breadcrumbs=array(
	'Accident Apps'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List AccidentApp', 'url'=>array('index')),
		array('label'=>'Create AccidentApp', 'url'=>array('create')),
		array('label'=>'Update AccidentApp', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete AccidentApp', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage AccidentApp', 'url'=>array('admin')),
	);
}
?>

<h1>View AccidentApp #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'own_car_damage_left',
		'own_car_damage_right',
		'own_car_damage_back',
		'own_car_damage_front',
		'other_car_damage_left',
		'other_car_damage_right',
		'other_car_damage_back',
		'other_car_damage_front',
		'settlement_type',
		'remark',
		'accident_time',
		'accident_place',
		'driver_pin',
		'vehicle_reg_no',
	),
)); ?>
