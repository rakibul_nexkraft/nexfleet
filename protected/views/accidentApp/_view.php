<?php
/* @var $this AccidentAppController */
/* @var $data AccidentApp */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('own_car_damage_left')); ?>:</b>
	<?php echo CHtml::encode($data->own_car_damage_left); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('own_car_damage_right')); ?>:</b>
	<?php echo CHtml::encode($data->own_car_damage_right); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('own_car_damage_back')); ?>:</b>
	<?php echo CHtml::encode($data->own_car_damage_back); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('own_car_damage_front')); ?>:</b>
	<?php echo CHtml::encode($data->own_car_damage_front); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other_car_damage_left')); ?>:</b>
	<?php echo CHtml::encode($data->other_car_damage_left); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other_car_damage_right')); ?>:</b>
	<?php echo CHtml::encode($data->other_car_damage_right); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('other_car_damage_back')); ?>:</b>
	<?php echo CHtml::encode($data->other_car_damage_back); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('other_car_damage_front')); ?>:</b>
	<?php echo CHtml::encode($data->other_car_damage_front); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('settlement_type')); ?>:</b>
	<?php echo CHtml::encode($data->settlement_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remark')); ?>:</b>
	<?php echo CHtml::encode($data->remark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accident_time')); ?>:</b>
	<?php echo CHtml::encode($data->accident_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('accident_place')); ?>:</b>
	<?php echo CHtml::encode($data->accident_place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>