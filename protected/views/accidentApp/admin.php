<?php
/* @var $this AccidentAppController */
/* @var $model AccidentApp */

$this->breadcrumbs=array(
	'Accident Apps'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AccidentApp', 'url'=>array('index')),
	array('label'=>'Create AccidentApp', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('accident-app-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Accident Apps</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'accident-app-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'own_car_damage_left',
		'own_car_damage_right',
		'own_car_damage_back',
		'own_car_damage_front',
		'other_car_damage_left',
		/*
		'other_car_damage_right',
		'other_car_damage_back',
		'other_car_damage_front',
		'settlement_type',
		'remark',
		'accident_time',
		'accident_place',
		'driver_pin',
		'vehicle_reg_no',
		'created_by',
		'created_date',
		'updated_by',
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
