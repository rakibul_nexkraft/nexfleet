<?php
/* @var $this AccidentAppController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Accident Apps',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create AccidentApp', 'url'=>array('create')),
		array('label'=>'Manage AccidentApp', 'url'=>array('admin')),
	);
}
?>

<h1>Accident Apps</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
