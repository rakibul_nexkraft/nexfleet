<?php
/* @var $this PurchaseStatusController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Purchase Statuses',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create PurchaseStatus', 'url'=>array('create')),
		array('label'=>'Manage PurchaseStatus', 'url'=>array('admin')),
	);
}
?>

<h1>Purchase Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
