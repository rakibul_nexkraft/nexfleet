<?php
/* @var $this PurchaseStatusController */
/* @var $model PurchaseStatus */

$this->breadcrumbs=array(
	'Purchase Statuses'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List PurchaseStatus', 'url'=>array('index')),
		array('label'=>'Create PurchaseStatus', 'url'=>array('create')),
		//array('label'=>'Update PurchaseStatus', 'url'=>array('update', 'id'=>$model->id)),
		//array('label'=>'Delete PurchaseStatus', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		//array('label'=>'Manage PurchaseStatus', 'url'=>array('admin')),
	);
}
?>

<h1>View PurchaseStatus #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'purchase_status',
		'create_by',
		'created_time',
	),
)); ?>
