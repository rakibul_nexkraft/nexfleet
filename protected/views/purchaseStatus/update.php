<?php
/* @var $this PurchaseStatusController */
/* @var $model PurchaseStatus */

$this->breadcrumbs=array(
	'Purchase Statuses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List PurchaseStatus', 'url'=>array('index')),
		array('label'=>'Create PurchaseStatus', 'url'=>array('create')),
		array('label'=>'View PurchaseStatus', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage PurchaseStatus', 'url'=>array('admin')),
	);
}
?>

<h1>Update PurchaseStatus <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>