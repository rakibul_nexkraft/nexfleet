<?php
/* @var $this PurchaseStatusController */
/* @var $model PurchaseStatus */

$this->breadcrumbs=array(
	'Purchase Statuses'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List PurchaseStatus', 'url'=>array('index')),
		array('label'=>'Manage PurchaseStatus', 'url'=>array('admin')),
	);
}
?>

<h1>Create PurchaseStatus</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>