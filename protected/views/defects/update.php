<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DefectsController */
/* @var $model Defects */

$this->breadcrumbs=array(
	'Defects'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Defects', 'url'=>array('index'), 'visible'=> in_array('List Defects', $user_rights)),
		array('label'=>'New Defects', 'url'=>array('create'), 'visible'=> in_array('New Defects', $user_rights)),
		array('label'=>'View Defects', 'url'=>array('view', 'id'=>$model->id), 'visible'=> in_array('View Defects', $user_rights)),
		array('label'=>'Manage Defects', 'url'=>array('admin'), 'visible'=> in_array('Manage Defects', $user_rights)),
	);
}
?>

<h4>Update Defects <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>