<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DefectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Defects',
);

$this->menu=array(

 array('label'=>'New Defect', 'url'=>array('defects/create','id'=>$model->id)),
 array('label'=>'Pending List', 'url'=>array('pending')),
);
?>

<h4>Defects</h4>

<div class="search-form">
    <?php $this->renderPartial('_searchpending',array(
        'model'=>$model,
    )); ?>
</div>
<div id="def_container">
    <?php


    $var = $_REQUEST['Defects'];

    if($var):
        ?>



        <h4>Date:  <?php echo $var['from_p_date']." to ". $var['to_p_date']?></h4>
        <?php if($var['apply_date']) echo "User: ".$var['apply_date']."&nbsp;&nbsp;&nbsp;&nbsp;";?>
        <?php if($var['vehicle_reg_no'])echo "Vehicle Regitration Number:". $var['vehicle_reg_no']."&nbsp;&nbsp;&nbsp;&nbsp;";?>

        <?php

        if($var['active']==0);
        else if($var['active']==1)$active= "Not Approved";
        else if($var['active']==2)$active= "Approved";
        if($active)echo "Action:". $active."&nbsp;&nbsp;&nbsp;&nbsp;";?>
    <?php endif;?>







<?php /* $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */ ?>


<?php

if(($var['from_p_date'] != null) or ($var['vehicle_reg_no'] != null) or ($var['to_p_date'] != null) or ($var['id'] != null)){

    $this->widget('bootstrap.widgets.TbGridView', array(
     'type'=>'striped bordered condensed',
     'id'=>'defects-grid',
     'dataProvider'=>$dataProvider,

     'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'vehicle_reg_no',
        'driver_name',
        'apply_date',
        'defect_description',
        'remarks',

    ),

 ));
}
?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Defects'])));
?> &nbsp; &nbsp; &nbsp;

<?php if(Yii::app()->user->hasFlash('success')): ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'alert_msg',
	'autoOpen'=>true,
	//'htmlOptions'=>array('onload'=>'$("#alert_msg").modal("toggle");'),
	'options'=>array('backdrop'=>false),
	'events'=>array('shown'=>'js: function(){setTimeout("$(\'#alert_msg\').modal(\'toggle\');", 3000)}'),
)); 
?>

<div class="alert in alert-block fade alert-success" style="margin-bottom: 0 !important;">
  <a class="close" data-dismiss="modal">&times;</a>
  <?php echo Yii::app()->user->getFlash('success'); ?>
</div>

<?php $this->endWidget();
endif; 
?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array(
	'id'=>'popModal',
	'htmlOptions'=>array('style'=>'width:700px; height:500px; left:45% !important;'),
)); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <br />
</div>

<div class="modal-body">
</div>

<?php 
$this->endWidget();
	//Yii::app()->clientScript->registerScript("modalClose", "$(window).unload(function(e){ e.preventDefault(); $('#popModal').modal('hide');});");  
?>