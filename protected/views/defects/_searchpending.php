<?php
/* @var $this DefectsController */
/* @var $model Defects */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>



    
     <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'id'); ?>
        <div class="clearfix"></div>
		<?php echo $form->textField($model,'id',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>
    
    <div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>
        <div class="clearfix"></div>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
    </div>



    <fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'from_p_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'from_p_date',  // name of post parameter
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'to_p_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'to_p_date',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </fieldset>

    
    
    <div class="clearfix"></div>
    
    <div align="left" style="padding-left:15px;">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div><br/>

<?php $this->endWidget(); ?>


</div>
<!-- search-form -->