<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this DefectsController */
/* @var $model Defects */

$this->breadcrumbs=array(
	'Defects'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Defects', 'url'=>array('index'), 'visible'=> in_array('List Defects', $user_rights)),
		array('label'=>'Manage Defects', 'url'=>array('admin'), 'visible'=> in_array('Manage Defects', $user_rights)),
	);
}
?>

<h4>New Defects</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>