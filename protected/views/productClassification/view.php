<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>
<?php
/* @var $this ProductClassificationController */
/* @var $model ProductClassification */

$this->breadcrumbs=array(
	'Product Classifications'=>array('index'),
	$model->name,
);

?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Product Classification #<?php echo $model->id; ?></h3>
    </div>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'name',
		array(
			'name' => 'product_grouping_id',
			'value' => ProductGrouping::getGroupName($model->product_grouping_id),
		),
		array(
			'name' => 'manufacturer_id',
			'value' => Manufacturer::getManufacturerName($model->manufacturer_id),
		),
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
