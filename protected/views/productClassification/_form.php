<style>
	.form #product-classification-form{
		margin-bottom: unset;
	}
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-classification-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Product Classification</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'name', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'name'); ?>

		<?php echo $form->labelEx($model, 'product_grouping_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php
			$grouping_name = ProductGrouping::model()->findAll(array('select' => 'id, name', 'order' => 'id ASC'));
			echo $form->dropDownList($model, 'product_grouping_id', CHtml::listData($grouping_name, 'id', 'name'), array('class' => 'form-control form-control-sm','empty'=>'Select Product Grouping')); ?>
		</div>
		<?php echo $form->error($model, 'product_grouping_id'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'manufacturer_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php
			$manufacturer_name = Manufacturer::model()->findAll(array('select' => 'id, name', 'order' => 'id ASC'));
			echo $form->dropDownList($model, 'manufacturer_id', CHtml::listData($manufacturer_name, 'id', 'name'), array('class' => 'form-control form-control-sm','empty'=>'Select Manufacturer')); ?>
		</div>
		<?php echo $form->error($model, 'manufacturer_id'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>

	<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->