<?php
/* @var $this ProductClassificationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Product Classifications',
);

$this->menu=array(
	array('label'=>'Create ProductClassification', 'url'=>array('create')),
	array('label'=>'Manage ProductClassification', 'url'=>array('admin')),
);
?>

<h1>Product Classifications</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
