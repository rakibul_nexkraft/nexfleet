<?php
/* @var $this ProductClassificationController */
/* @var $model ProductClassification */

$this->breadcrumbs=array(
	'Product Classifications'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductClassification', 'url'=>array('index')),
	array('label'=>'Create ProductClassification', 'url'=>array('create')),
	array('label'=>'View ProductClassification', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductClassification', 'url'=>array('admin')),
);
?>

<h1>Update ProductClassification <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>