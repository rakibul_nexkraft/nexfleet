<?php
/* @var $this DeptcostsController */
/* @var $model Deptcosts */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'deptcosts-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cost_type'); ?>
		<?php echo $form->textField($model,'cost_type',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'cost_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'amount'); ?>
		<?php echo $form->textField($model,'amount'); ?>
		<?php echo $form->error($model,'amount'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin'); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>
	<!-- <div class="row">
		<?php echo $form->labelEx($model,'spend_date'); ?>
		<?php echo $form->textField($model,'spend_date'); ?>
		<?php echo $form->error($model,'spend_date'); ?>
	</div> -->
	
	<div class="row">
	<?php echo $form->labelEx($model,'spend_date'); ?>
        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'spend_date',     
            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->spend_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:200px;'
        ),
        )); ?>
		<?php echo $form->error($model,'spend_date'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->