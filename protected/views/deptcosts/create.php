<?php
/* @var $this DeptcostsController */
/* @var $model Deptcosts */

$this->breadcrumbs=array(
	'Deptcosts'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Deptcosts', 'url'=>array('index')),
		array('label'=>'Manage Deptcosts', 'url'=>array('admin')),
	);
}
?>

<h4>Create Deptcosts</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>