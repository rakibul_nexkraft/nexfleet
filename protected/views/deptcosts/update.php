<?php
/* @var $this DeptcostsController */
/* @var $model Deptcosts */

$this->breadcrumbs=array(
	'Deptcosts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Deptcosts', 'url'=>array('index')),
		array('label'=>'New Deptcosts', 'url'=>array('create')),
		array('label'=>'View Deptcosts', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Deptcosts', 'url'=>array('admin')),
	);
}
?>

<h4>Update Deptcosts : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>