<?php
/* @var $this DeptcostsController */
/* @var $model Deptcosts */

$this->breadcrumbs=array(
	'Deptcosts'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Deptcosts', 'url'=>array('index')),
		array('label'=>'New Deptcosts', 'url'=>array('create')),
		array('label'=>'Update Deptcosts', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete Deptcosts', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage Deptcosts', 'url'=>array('admin')),
	);
}
?>

<h4>View Deptcosts : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cost_type',
		'amount',
		'spend_date',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
