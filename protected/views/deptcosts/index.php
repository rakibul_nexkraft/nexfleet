<?php
/* @var $this DeptcostsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Deptcosts',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Deptcosts', 'url'=>array('create')),
		array('label'=>'Manage Deptcosts', 'url'=>array('admin')),
	);
}
?>

<h4>Deptcosts</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); */ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'deptcosts-grid',
	'dataProvider'=>$dataProvider,
	'columns'=>array(
		array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        array(
            'name' => 'cost_type',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->cost_type),array("view","id"=>$data->id))',
        ),
//		'cost_type',
        'user_pin',
		'spend_date',
		'amount',
		
		//'fuel_station_id',
		/*
		'driver_pin',
		'created_by',
		'created_time',
		'active',
		*/
	),
)); 
?>
