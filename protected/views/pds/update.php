<?php
/* @var $this PdsController */
/* @var $model Pds */

$this->breadcrumbs=array(
	'Pds'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List Pds', 'url'=>array('index')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'View', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage', 'url'=>array('admin')),
	);
}
?>

<h1>Update Pick & Drop Service <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>