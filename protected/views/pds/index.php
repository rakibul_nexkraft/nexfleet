<?php
/* @var $this PdsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pds',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'Manage', 'url'=>array('admin')),
	);
}
?>

<h1>Pick & Drop Service</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
