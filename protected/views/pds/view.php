<?php
/* @var $this PdsController */
/* @var $model Pds */

$this->breadcrumbs=array(
	'Pds'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		//array('label'=>'List Pds', 'url'=>array('index')),
		array('label'=>'Create', 'url'=>array('create')),
		array('label'=>'Update', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage', 'url'=>array('admin')),
	);
}
?>

<h1>View Pds #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'service_date',
		'route',
		'provided',
		'reasons',
		'remarks',
//		'created_by',
//		'created_date',
//		'updated_by',
//		'updated_date',
//		'active',
	),
)); ?>
