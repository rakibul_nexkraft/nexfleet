<?php
/* @var $this PdsController */
/* @var $model Pds */

$this->breadcrumbs=array(
	'Pds'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		///array('label'=>'List Pds', 'url'=>array('index')),
		array('label'=>'Manage', 'url'=>array('admin')),
	);
}
?>

<h1>Create Pick & Drop Service</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>