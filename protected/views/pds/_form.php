<?php
/* @var $this PdsController */
/* @var $model Pds */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pds-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'service_date'); ?>		
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'service_date',     

            // additional javascript options for the date picker plugin
            'options'=>array('autoSize'=>true,
                    'dateFormat'=>'yy-mm-dd',
                    'defaultDate'=>$model->service_date,
                    'changeYear'=>true,
                    'changeMonth'=>true,
                ),

        'htmlOptions'=>array('style'=>'width:150px;'
        ),
        )); ?>
		<?php echo $form->error($model,'service_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'route'); ?>
		<?php //$route = Routes::model()->findAll(array('select'=>'id,route_no','order' => 'id ASC'));
           //             echo $form->dropDownList($model,'route', CHtml::listData($route,'route_no',  'route_no'),array('empty' => 'Select Route...')); 
           ?>
      <?php echo $form->textField($model,'route',array('size'=>60,'maxlength'=>127)); ?>     
		<?php echo $form->error($model,'route'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'provided'); ?>
		 <?php $provided = array (
                            "Not Provided"=>"Not Provided",
                            "Provided in morning"=>"Provided in morning",	
                            "Provided in afternoon"=>"Provided in afternoon",                                                  
                            );
                            echo $form->dropDownList($model,'provided', $provided );
                            ?>
		<?php echo $form->error($model,'provided'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reasons'); ?>
		 <?php $reasons = array (
                            "Blockade"=>"Blockade",
                            "Strike"=>"Strike",	
                            "Strike & Blockade"=>"Strike & Blockade",
                            "Vehicle Break Down"=>"Vehicle Break Down",                         
                            "Vehicle Not Available"=>"Vehicle Not Available", 
                            "Others"=>"Others",
                            );
                 ?>                 
                  
                  <div id =reasons>
                   <?php 
                  echo $form->dropDownList($model,'reasons', $reasons,
                         array(
                            'empty'    => 'Select Category',
                            'onclick' => "javascript:changeField(this);",
                          ));
                            
                ?>                              
                </div>
            
		<?php echo $form->error($model,'reasons'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

<!--	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
		<?php echo $form->error($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by'); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
		<?php echo $form->error($model,'updated_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    
    function changeField(item)
    {        
        if(item.value == 'Others')
        document.getElementById('reasons').innerHTML = "<input id='Pds_reasons' type='text' name='Pds[reasons]'>"; 
    }
</script>
    