<?php
/* @var $this DutytypesController */
/* @var $model Dutytypes */

$this->breadcrumbs=array(
	'Duty Types'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Duty Types List', 'url'=>array('index')),
		array('label'=>'New Duty Type', 'url'=>array('create')),
		array('label'=>'Update Duty Type', 'url'=>array('update', 'id'=>$model->id)),
	//	array('label'=>'Delete Dutytype', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
		array('label'=>'Manage Duty Types', 'url'=>array('admin')),
	);
}
?>

<h4>View Duty Type : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type_name',
		'service_charge',
	),
)); ?>
