<?php
/* @var $this DutytypesController */
/* @var $model Dutytypes */

$this->breadcrumbs=array(
	'Duty Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Duty Types List', 'url'=>array('index')),
		array('label'=>'Create Duty Types', 'url'=>array('create')),
		array('label'=>'View Duty Types', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage Duty Types', 'url'=>array('admin')),
	);
}
?>

<h4>Update Duty Type : <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>