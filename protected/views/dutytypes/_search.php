<?php
/* @var $this DutytypesController */
/* @var $model Dutytypes */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!--div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div-->

	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'type_name'); ?><br />
			<?php echo $form->textField($model,'type_name',array('maxlength'=>127)); ?>
		</div>
	</div>
	<div class="fl">
		<div class="row">
			<?php echo $form->label($model,'service_charge'); ?><br />
			<?php echo $form->textField($model,'service_charge',array('maxlength'=>20)); ?>
		</div>
	</div>
	<div class="row buttons" align="right">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->