<?php
/* @var $this DutytypesController */
/* @var $model Dutytypes */

$this->breadcrumbs=array(
	'Duty Types'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Duty Types List ', 'url'=>array('index')),
		array('label'=>'Manage Duty Types', 'url'=>array('admin')),
	);
}
?>

<h4>New Duty Type</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>