<?php
/* @var $this DutytypesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Duty Types',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'New Duty Type', 'url'=>array('create')),
		array('label'=>'Manage Duty Types', 'url'=>array('admin')),
	);
}
?>

<h4>Duty Types</h4>

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'dutytypes-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id',
        array(
            'name' => 'type_name',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->type_name),array("view","id"=>$data->id))',
        ),
		'service_charge',
	),
)); ?>