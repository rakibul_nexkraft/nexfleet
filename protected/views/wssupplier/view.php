<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WssupplierController */
/* @var $model Wssupplier */

$this->breadcrumbs=array(
	'Supplier'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Supplier', 'url'=>array('index'),'visible'=> in_array('List Supplier', $user_rights)),
	array('label'=>'New Supplier', 'url'=>array('create'),'visible'=> in_array('New Supplier', $user_rights)),
	array('label'=>'Update Supplier', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Supplier', $user_rights)),
	array('label'=>'Delete Supplier', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Supplier', $user_rights)),
	array('label'=>'Manage Supplier', 'url'=>array('admin'),'visible'=> in_array('Manage Supplier', $user_rights)),
);
?>

<h4>View Supplier #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'supplier_name',
		'supplier_address',
		'cell_no',
		'active',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
