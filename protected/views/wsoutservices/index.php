<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsoutservicesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Wsoutservices',
);

$this->menu=array(
	array('label'=>'New Service - Outside', 'url'=>array('create'),'visible'=> in_array('New Service - Outside', $user_rights)),
	array('label'=>'Manage Service List - Outside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Outside', $user_rights)),
);
?>

<h4>Service List - Outside</h4>


<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'wsoutservices-grid',
    //'dataProvider'=>$dataProvider,
    'dataProvider'=>$model->search(),
    'filter'=>$model,


    'columns'=>array(
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ),
        'service_name',
        //'quantity',
        'car',
        'micro_jeep',
        'pickup',
        'bus_coaster',


    ),
));
?>
