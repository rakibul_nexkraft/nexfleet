<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this WsoutservicesController */
/* @var $model Wsoutservices */

$this->breadcrumbs=array(
	'Wsoutservices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Service List - Outside', 'url'=>array('index'),'visible'=> in_array('Service List - Outside', $user_rights)),
	array('label'=>'New Service - Outside', 'url'=>array('create'),'visible'=> in_array('New Service - Outside', $user_rights)),
	array('label'=>'Update Service - Outside', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Service - Outside', $user_rights)),
	array('label'=>'Delete Service - Outside', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Service - Outside', $user_rights)),
	array('label'=>'Manage Service List - Outside', 'url'=>array('admin'),'visible'=> in_array('Manage Service List - Outside', $user_rights)),
);
?>

<h4>Service - Outside #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'service_name',
		'service_category',
		'quantity',
		'car',
		'micro_jeep',
		'pickup',
		'bus_coaster',
		'active',
		'updated_by',
		'updated_time',
	),
)); ?>
