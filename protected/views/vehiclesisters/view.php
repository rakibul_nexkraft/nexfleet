<?php
/* @var $this VehiclesistersController */
/* @var $model Vehiclesisters */

$this->breadcrumbs=array(
	'Vehiclesisters'=>array('index'),
	$model->reg_no,
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Vehicle Sisters', 'url'=>array('index')),
	array('label'=>'New Vehicle Sisters', 'url'=>array('create')),
	array('label'=>'Update Vehicle Sister', 'url'=>array('update', 'id'=>$model->reg_no)),
	array('label'=>'Delete Vehicle Sisters', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->reg_no),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Vehicle Sisters', 'url'=>array('admin')),
);
}
?>

<h4>View Vehicle - Sister Concern #<?php echo $model->reg_no; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'reg_no',
		'vehicletype_id',
		'engine_no',
		'cc',
		'vmodel',
		'model_code',
		'remarks',
		'chassis_no',
		'country',
		'purchase_date',
		'purchase_price',
		'supplier',
		'seat_capacity',
		'load_capacity',
		'ac',
		'driver_pin',
		'location',
		'driver_name',
		'helper_pin',
		'helper_name',
		'created_time',
		'created_by',
		'active',
	),
)); ?>
