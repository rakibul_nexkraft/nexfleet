<?php
/* @var $this VehiclesistersController */
/* @var $model Vehiclesisters */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vehiclesisters-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<div class="container1">
	<div class="row">
		<?php echo $form->labelEx($model,'reg_no'); ?>
		<?php echo $form->textField($model,'reg_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicletype_id'); ?>
		<?php echo $form->textField($model,'vehicletype_id'); ?>
		<?php echo $form->error($model,'vehicletype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'engine_no'); ?>
		<?php echo $form->textField($model,'engine_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'engine_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cc'); ?>
		<?php echo $form->textField($model,'cc',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'cc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vmodel'); ?>
		<?php echo $form->textField($model,'vmodel',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'vmodel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'model_code'); ?>
		<?php echo $form->textField($model,'model_code',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'model_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'chassis_no'); ?>
		<?php echo $form->textField($model,'chassis_no',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'chassis_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->textField($model,'country',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'purchase_date'); ?>
		<?php //echo $form->textField($model,'purchase_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'purchase_date',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->purchase_date,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>

		<?php echo $form->error($model,'purchase_date'); ?>
	</div>
</div>
<div class="container2">
	<div class="row">
		<?php echo $form->labelEx($model,'purchase_price'); ?>
		<?php echo $form->textField($model,'purchase_price'); ?>
		<?php echo $form->error($model,'purchase_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'supplier'); ?>
		<?php echo $form->textField($model,'supplier',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'supplier'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'seat_capacity'); ?>
		<?php echo $form->textField($model,'seat_capacity'); ?>
		<?php echo $form->error($model,'seat_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'load_capacity'); ?>
		<?php echo $form->textField($model,'load_capacity',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'load_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ac'); ?>
		<?php echo $form->textField($model,'ac',array('size'=>5,'maxlength'=>5)); ?>
		<?php echo $form->error($model,'ac'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_name'); ?>
		<?php echo $form->textField($model,'driver_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'driver_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'helper_pin'); ?>
		<?php echo $form->textField($model,'helper_pin'); ?>
		<?php echo $form->error($model,'helper_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'helper_name'); ?>
		<?php echo $form->textField($model,'helper_name',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'helper_name'); ?>
	</div>
<!----
	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php //echo $form->textField($model,'created_time'); ?>
		 <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model'=>$model,'attribute'=>'created_time',

            'options'=>array('autoSize'=>true,
                'dateFormat'=>'yy-mm-dd',
                'defaultDate'=>$model->created_time,
                'changeYear'=>true,
                'changeMonth'=>true,
            ),

            'htmlOptions'=>array('style'=>'width:150px;'
            ),
        )); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>
--->
	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>
    </div>
<div class="clear"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Save',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->