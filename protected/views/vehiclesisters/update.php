<?php
/* @var $this VehiclesistersController */
/* @var $model Vehiclesisters */

$this->breadcrumbs=array(
	'Vehiclesisters'=>array('index'),
	$model->reg_no=>array('view','id'=>$model->reg_no),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Vehicle Sisters', 'url'=>array('index')),
	array('label'=>'New Vehicle Sister', 'url'=>array('create')),
	array('label'=>'View Vehicle Sister', 'url'=>array('view', 'id'=>$model->reg_no)),
	array('label'=>'Manage Vehicle Sisters', 'url'=>array('admin')),
);
}
?>

<h4>Update Vehicle - Sister Concern <?php echo $model->reg_no; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>