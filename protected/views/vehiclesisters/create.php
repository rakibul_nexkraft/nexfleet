<?php
/* @var $this VehiclesistersController */
/* @var $model Vehiclesisters */

$this->breadcrumbs=array(
	'Vehiclesisters'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Vehiclesisters', 'url'=>array('index')),
	array('label'=>'Manage Vehiclesisters', 'url'=>array('admin')),
);
}
?>

<h4>New Vehicle Sister Concern</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>