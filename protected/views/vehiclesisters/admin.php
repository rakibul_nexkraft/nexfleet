<?php
/* @var $this VehiclesistersController */
/* @var $model Vehiclesisters */

$this->breadcrumbs=array(
	'Vehiclesisters'=>array('index'),
	'Manage',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'List Vehiclesisters', 'url'=>array('index')),
	array('label'=>'Create Vehiclesisters', 'url'=>array('create')),
);
}

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('vehiclesisters-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h4>Manage Vehicle Sister Concerns</h4>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vehiclesisters-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'reg_no',
		'vehicletype_id',
		'engine_no',
		'cc',
		'vmodel',
		'model_code',
		/*
		'remarks',
		'chassis_no',
		'country',
		'purchase_date',
		'purchase_price',
		'supplier',
		'seat_capacity',
		'load_capacity',
		'ac',
		'driver_pin',
		'location',
		'driver_name',
		'helper_pin',
		'helper_name',
		'created_time',
		'created_by',
		'active',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
