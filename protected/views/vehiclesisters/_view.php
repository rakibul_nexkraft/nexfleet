<?php
/* @var $this VehiclesistersController */
/* @var $data Vehiclesisters */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('reg_no')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->reg_no), array('view', 'id'=>$data->reg_no)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicletype_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicletype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('engine_no')); ?>:</b>
	<?php echo CHtml::encode($data->engine_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cc')); ?>:</b>
	<?php echo CHtml::encode($data->cc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vmodel')); ?>:</b>
	<?php echo CHtml::encode($data->vmodel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('model_code')); ?>:</b>
	<?php echo CHtml::encode($data->model_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('chassis_no')); ?>:</b>
	<?php echo CHtml::encode($data->chassis_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country')); ?>:</b>
	<?php echo CHtml::encode($data->country); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('purchase_date')); ?>:</b>
	<?php echo CHtml::encode($data->purchase_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('purchase_price')); ?>:</b>
	<?php echo CHtml::encode($data->purchase_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier')); ?>:</b>
	<?php echo CHtml::encode($data->supplier); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('seat_capacity')); ?>:</b>
	<?php echo CHtml::encode($data->seat_capacity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('load_capacity')); ?>:</b>
	<?php echo CHtml::encode($data->load_capacity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ac')); ?>:</b>
	<?php echo CHtml::encode($data->ac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_name')); ?>:</b>
	<?php echo CHtml::encode($data->driver_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_pin')); ?>:</b>
	<?php echo CHtml::encode($data->helper_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('helper_name')); ?>:</b>
	<?php echo CHtml::encode($data->helper_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	*/ ?>

</div>