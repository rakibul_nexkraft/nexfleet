<?php
/* @var $this VehiclesistersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vehiclesisters',
);
if(!Yii::app()->user->isViewUser()) {
$this->menu=array(
	array('label'=>'New Vehicle Sister', 'url'=>array('create')),
	array('label'=>'Manage Vehicle Sisters', 'url'=>array('admin')),
);
}
?>

<h4>Vehicle - Sister Concerns</h4>



<?php $this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered condensed',
    'id'=>'vehiclesisters-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
            'name' => 'reg_no',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->reg_no),array("view","reg_no"=>$data->reg_no))',
        ),
        array(
            'name' => 'vehicletype_id',
            'type'=>'raw',
            'value' => '$data->vehicletypes->type',
        ),
        'engine_no',
        'model_code',
        'remarks',

    ),
));

?>
<?php
    echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['Vehiclesisters'])));
?> &nbsp; &nbsp; &nbsp;
