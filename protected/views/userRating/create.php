<?php
/* @var $this UserRatingController */
/* @var $model UserRating */

$this->breadcrumbs=array(
	'User Ratings'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List UserRating', 'url'=>array('index')),
		array('label'=>'Manage UserRating', 'url'=>array('admin')),
	);
}
?>

<h1>Create UserRating</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>