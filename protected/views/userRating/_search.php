<?php
/* @var $this UserRatingController */
/* @var $model UserRating */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<div class="clearfix"></div>
		<?php echo $form->textField($model,'id'); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?>
		<div class="clearfix"></div>
		<?php echo $form->textField($model,'requisition_id',array('size'=>60,'maxlength'=>128)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?>
		<div class="clearfix"></div>
		<?php echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'u_rating'); ?>
		<div class="clearfix"></div>
		<?php echo $form->textField($model,'u_rating',array('size'=>60,'maxlength'=>128)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<div class="clearfix"></div>
		<?php echo $form->textField($model,'driver_pin',array('size'=>60,'maxlength'=>128)); ?>
	</div>
</div>
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
	</div>
</div>-->
<!--<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
	</div>
</div>
<div class="fl">

	<div class="row">
		<?php echo $form->label($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
	</div>
</div>-->

<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'from_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'from_date',  // name of post parameter
                    //'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->from_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;',
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="fl">
            <div class="row">
                <?php echo $form->label($model,'to_date'); ?><br />
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'to_date',
                    //'value'=>Yii::app()->request->cookies['to_date']->value,
                    'options'=>array(
                        //	'showAnim'=>'fold',
                        'dateFormat'=>'yy-mm-dd',
                        //'defaultDate'=>$model->to_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                    'htmlOptions'=>array(
                        'style'=>'height:17px; width:138px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </fieldset>

	<div class="clearfix"></div>
    
    <div align="left" style="padding-left:15px;">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div><br/>


<?php $this->endWidget(); ?>

</div><!-- search-form -->