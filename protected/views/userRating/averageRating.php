<?php
/* @var $this UserRatingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Ratings',
);
if(!Yii::app()->user->isViewUser()) {
  $this->menu=array(
  	array('label'=>'All Rating List', 'url'=>array('index')),
  	//array('label'=>'Manage UserRating', 'url'=>array('admin')),
  );
}
?>

<h4>User Ratings</h4>
<div class="search-form">
        <?php //$this->renderPartial('_search',array(
            //'model'=>$model,
        //)); ?>
 </div>
<div id="def_container">



<?php 
//var_dump($dataProvider);die();
$this->widget('bootstrap.widgets.TbGridView', array(
	  'type'=>'striped bordered condensed',
      'id'=>'defects-grid',
      //'filter' => $model,
      'dataProvider'=>$dataProvider,

    'columns'=>array(   
        
        'user_pin',
        array('header'=>'User Rating',
              'type'=>'raw',
              'value'=>'$data->u_rating',
        ),
             

    ),

)); ?>
<?php
//echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['UserRating'])));
?> 
</div>