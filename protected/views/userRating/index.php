<?php
/* @var $this UserRatingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Ratings',
);

$this->menu=array(
	array('label'=>'Individual User Rating', 'url'=>array('averageRating')),
	//array('label'=>'Manage UserRating', 'url'=>array('admin')),
);
?>

<h4>User Ratings</h4>
<div class="search-form">
        <?php $this->renderPartial('_search',array(
            'model'=>$model,
        )); ?>
 </div>
<div id="def_container">



<?php $this->widget('bootstrap.widgets.TbGridView', array(
	  'type'=>'striped bordered condensed',
      'id'=>'defects-grid',
      'dataProvider'=>$dataProvider,

    'columns'=>array(
        //'id',
        array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("view","id"=>$data->id))',
        ), 
        array('name'=>'requisition_id','type'=>'raw',
            'value' => 'CHtml::link($data->requisition_id,array("requisitions/".$data->requisition_id))',),   
        
        'user_pin',
        'u_rating',
        'driver_pin',
        'created_time',        

    ),

)); ?>
<?php
echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['UserRating'])));
?> 
</div>