<?php
/* @var $this TimesheetController */
/* @var $model Timesheet */

$this->breadcrumbs=array(
	'Timesheets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List of Timesheets', 'url'=>array('index')),
	array('label'=>'Create Timesheet', 'url'=>array('create')),
	array('label'=>'View Timesheet', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Timesheets', 'url'=>array('admin')),
);
?>

<h4>Update Timesheet <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>