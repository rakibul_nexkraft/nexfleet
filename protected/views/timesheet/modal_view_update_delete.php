<style>
	.detail-view {
		width: 100% !important;
		margin-bottom: unset;
	}

	.well form {
		margin-bottom: unset;
	}

	#user-form textarea {
		border: 1px solid #5a5a5a2e;
	}
</style>

<script>
	function placeOrder() {
		document.theForm.submit()
	}
</script>
<?php if(($type == 2) || ($type ==1)){?>
	<div class="modal-dialog" style="width: 715px; margin: 65px auto;">
	<?php }else{?>
		<div class="modal-dialog" style="margin: 206px 500px;">
	<?php }?>
	<?php if (($type == 2) || ($type ==1)) { ?>
		<div class="modal-content" style="width: 850px;">
		<?php } else { ?>
			<div class="modal-content">
			<?php } ?>
			<div class="modal-header modal-update">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title"><?php if ($type == 1) {
					echo ('Timesheet #') . "" . $model->id;
				}
				if ($type == 2) {
					echo ('Update Timesheet #') . "" . $model->id;
				}
				if ($type == 3) {
					echo ('Delete Timesheet #') . "" . $model->id;
				} ?></h3>
			</div>
			<div class="modal-body">
				<div class="well" style="padding: unset; margin-bottom: 0px;background-color: unset;
				border: unset;box-shadow: none;">
				<?php if ($type == 3) {
					?>
					<div class="modal-body" style="text-align: center;font-size: x-large;">
						Are you sure you want to delete this record?
					</div>

				<?php }
				if ($type == 1) { 

					$this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'start_date',
		'end_date',
		'vehicle_id',
		'mechanic_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
));
					?>

					

<?php }

if ($type == 2) {
	?>
	<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'timesheet-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'start_date', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model, 'start_date', array('size'=>60,'maxlength'=>150,'placeholder' => 'Start Date', 'class' => 'datepicker form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'start_date'); ?>
		
		<?php echo $form->labelEx($model, 'end_date', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>

		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model, 'end_date', array('size'=>60,'maxlength'=>150,'placeholder' => 'End Date', 'class' => 'datepicker form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model,'end_date'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'vehicle_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'vehicle_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'vehicle_id'); ?>

		<?php echo $form->labelEx($model, 'mechanic_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php echo $form->textField($model,'mechanic_id',array('size'=>60,'maxlength'=>150,'class'=>'form-control form-control-sm')); ?>
		</div>
		<?php echo $form->error($model, 'mechanic_id'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {});</script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',

        });

</script>

<?php } ?>

<?php if ($type == 1) {
	?>
						<!-- <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
			
			
							<?php #echo CHtml::button('Submit',array('onclick'=>'placeOrder()'));
								?>
							<?php #echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); 
								?>
							<?php #$this->endWidget(); 
								?>
			
							</div> -->

						<?php }
						if ($type == 1) { ?>

							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
							</div>
						<?php }
						if ($type == 3) { ?>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">No</button>
								<?php echo CHtml::link('Yes', array('timesheetDelete', 'id' => $model->id), array(
									'class' => 'btn btn-secondary button-pink',
									'style' => 'color:white !important;'
								)); ?>
								<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="()">Yes</button> -->
							</div>
						<?php } ?>
					</div>
				</div>