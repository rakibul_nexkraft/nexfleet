<?php
/* @var $this TimesheetController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Timesheets',
);

$this->menu=array(
	array('label'=>'Create Timesheet', 'url'=>array('create')),
	array('label'=>'Manage Timesheets', 'url'=>array('admin')),
);
?>

<?php
echo CHtml::link('Create Timesheet', array("/timesheet/create"), array('class' => 'button button-pink button-mini button-rounded', 'style' => "color: white !important;float: right;"));
?>

<h4>Timesheets</h4>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'timesheet-grid',
	'dataProvider'=>$dataProvider,
	'filter'=>$model,
	'columns'=>array(
		'id',
		'start_date',
		'end_date',
		'vehicle_id',
		'mechanic_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>

