<?php
/* @var $this TimesheetController */
/* @var $model Timesheet */

$this->breadcrumbs=array(
	'Timesheets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List of Timesheets', 'url'=>array('index')),
	array('label'=>'Manage Timesheets', 'url'=>array('admin')),
);
?>

<h4>Create Timesheet</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>