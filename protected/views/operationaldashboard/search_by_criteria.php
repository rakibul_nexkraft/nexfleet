<style>
    .progress .bg-white{
        background-color: #ffffff !important;
        background-image: linear-gradient(to bottom, #ffffff, #e7e7e7);
    }
    .vehicle-list table{
        background: #FFFFFF;
        width:100%;
        height:100%;
        padding:0;
        margin:0;
    }
    /*.vehicle-list table:hover{*/
    /*background: #FFFFFF;*/
    /*}*/
    .vehicle-list-n th{
        text-align: center;
        background: #deeaf6;
    }
    .vehicle-list-n tr:hover th{
        background-color: #deeaf6;
    }
    .table-hover tbody tr:hover th {
        background-color: #deeaf6;
    }

    /*thead, tbody { display: block; }

    tbody {
        height: 100px;       !* Just for the demo          *!
        overflow-y: auto;    !* Trigger vertical scroll    *!
        overflow-x: hidden;  !* Hide the horizontal scroll *!
    }*/
    /*.vehicle-list tr .vl-1{*/
    /*padding:0;*/
    /*margin:0;*/
    /*}
</style>
<?php
//$s = "SELECT r.`vehicle_reg_no`,r.`start_date`, r.`end_date`, GROUP_CONCAT(r.`start_time`), GROUP_CONCAT(r.`end_time`) FROM `tbl_requisitions` r WHERE r.`start_date`>'2017-11-01' AND r.`end_date`<'2017-11-10' AND active=2 GROUP BY r.`vehicle_reg_no` ORDER BY r.`vehicle_reg_no` DESC";
//$s = "SELECT r.`vehicle_reg_no`,r.`start_date`, r.`end_date`, r.`start_time`, r.`end_time` FROM `tbl_requisitions` r WHERE r.`start_date`>'2017-11-01' AND r.`end_date`<'2017-11-10' AND active=2 GROUP BY r.`vehicle_reg_no` ORDER BY r.`vehicle_reg_no` DESC";
//$date = '2017-11-02';

//$s = "SELECT r.id, r.`vehicle_reg_no`,r.`start_date`, r.`end_date`,
//CASE
//WHEN r.`start_date` < '2017-11-02' THEN '00:00:00'
//ELSE r.`start_time`
//END AS start_time,
//
//
//CASE
//WHEN r.`end_date` > '2017-11-02' THEN '23:59:00'
//ELSE r.`end_time`
//END AS end_time
//
//FROM `tbl_requisitions` r WHERE r.`start_date` <= '2017-11-02' AND r.`end_date` >= '2017-11-02' AND active=2
//GROUP BY r.`vehicle_reg_no` ORDER BY r.`vehicle_reg_no` DESC ";
//
//$c = Yii::app()->db->createCommand($s);
//$r = $c->queryAll();


$baseTime = strtotime($date);



$total_time = 24*60*60;
$offset = 0;
$time_start = 0;
$time_end = 24*60*60;
$time_selected = 0;
$start_time_array = array();
$end_time_array = array();

$first = 0;
$second = 0;
$third = 0;
$tt=30;
$rr=20;
$ee=50;
?>
<table class="table table-bordered table-hover vehicle-list" id="">
    <thead class="" >
    <tr class="vlh">
        <th style="border-bottom:1px solid #dedede;padding-left:25px;width: 20%;text-align: center;background-color: #deeaf6">Vehicle Reg No</th>
        <th style="border-bottom:1px solid #dedede;width: 80%;text-align: center;background-color: #deeaf6">Duty Time</th>
    </tr>
    <tr>
        <td class="vl-1" style="padding: 0;margin: 0;background-color: #deeaf6;width: 20%;"></td>
        <td class="vl-1" style="padding: 0;margin: 0;background-color: #deeaf6;width: 80%;">
            <table class="vehicle-list-n">
                <tr style="font-size: 1.2em">
                    <th style="border-left: 0;">00</th>
                    <th>01</th>
                    <th>02</th>
                    <th>03</th>
                    <th>04</th>
                    <th>05</th>
                    <th>06</th>
                    <th>07</th>
                    <th>08</th>
                    <th>09</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                    <th>14</th>
                    <th>15</th>
                    <th>16</th>
                    <th>17</th>
                    <th>18</th>
                    <th>19</th>
                    <th>20</th>
                    <th>21</th>
                    <th>22</th>
                    <th>23</th>
                </tr>
            </table>
        </td>


    </tr>
    </thead>
    <tbody>
    <tr>
        <!--            <td></td>-->
        <td style="padding: 0;margin: 0;width: 100%;" colspan="2">
            <div style="padding: 0;margin: 0;max-height: calc(100vh - 650px);overflow-y: auto;min-height: 300px">
                <table style="margin: 0;padding: 0;width: 100%;">
                    <?php if($searchType == 'available') : ?>
                        <?php if(count($result)>0) : ?>
                            <?php foreach ($result as $item => $val) :?>
                                <?php
                                if($val["start_date"] != $val["end_date"]) $val["end_date"] = $val["start_date"];

                                //                    $startTime = strtotime($date+ " " + $val["start_time"]) - $baseTime;
                                //                    $endTime = strtotime($date+ " " + $val["end_time"]) - $baseTime;
                                //                    $totalTime = 24*60*60*1000;
                                //                $totalTime = date("Y-m-d H:i:s", mktime(00,00,00, date('n'), date('j')+1, date('Y'))) - $baseTime;








                                $start_time_array = explode(":",$val["start_time"]);
                                $start_time_seconds = $start_time_array[0]*60*60 + $start_time_array[1]*60;

                                $end_time_array = explode(":",$val["end_time"]);
                                $end_time_seconds = $end_time_array[0]*60*60 + $end_time_array[1]*60;
                                $first = ($start_time_seconds - $time_start)*$total_time/100;
                                $second = (($end_time_seconds<$time_end?$end_time_seconds:$time_end)-$start_time_seconds)*$total_time/100;
                                $third = ($total_time-$end_time_seconds)*$total_time/100;



                                //                $startTime =$start_time_seconds;
                                //                $endTime = $end_time_seconds;
                                //                $totalTime = 24*60*60;
                                $startTime =$start_time_seconds;
                                $dutyTime = $end_time_seconds - $start_time_seconds;
                                $endTime = $totalTime - $end_time_seconds;
                                $totalTime = 24*60*60;

                                //                $firstTimeDiff = $totalTime - $startTime;
                                //                $secondTimeDiff = $totalTime - $endTime;

                                //                $startTimePer = number_format( ($startTime/$totalTime)*100, 2);
                                //                $endTimePer =  number_format(($endTime/$totalTime)*100, 2);
                                $startTimePer = number_format( ($startTime/$totalTime)*100, 2);
                                $dutyTimePer = number_format( ($dutyTime/$totalTime)*100, 2);
                                $endTimePer =  number_format(($endTime/$totalTime)*100, 2);

                                $progressBarTimes = array();


                                $count = 0;
                                if($startTimePer > 0){
                                    $progressBarTimes[$count++] = array('start'=> 0, 'end' => $startTimePer, 'time' => $val["start_time"] .' to '. $val["end_time"], 'cls' => 'bar bar-success');
                                }

                                $progressBarTimes[$count++] = array('start'=> $startTimePer, 'end' => $endTimePer, 'time' => $val["start_time"] .' to '. $val["end_time"], 'cls' => 'bar bar-warning');

                                if($endTimePer < 100){
                                    $progressBarTimes[$count++] = array('start'=> $endTimePer, 'end' => 100, 'time' => $val["start_time"] .' to '. $val["end_time"], 'cls' => 'bar bar-success');
                                }



                                ?>
                                <tr class="even">
                                    <td style="border-bottom:1px solid #dedede;padding-left:15px;background-color: #FAFFFB;width: 20%;">
                                        <div class="progress">
                                            <div class="label" style="font-size: 1.2em"><?php echo $val["vehicle_reg_no"]; ?></div>
                                        </div>
                                    </td>
                                    <td style="border-bottom:1px solid #dedede;;background-color: #FAFFFB;width: 80%;">
                                        <!--                        <div class="progress">-->
                                        <div class="progress">
                                            <div title="<?php echo $startTimePer ?>" class="bar bar-success bg-white" style="width: <?php echo $startTimePer ?>%;color: #000;font-size: 1.2em">Not Allocated For Requisition</div>
                                            <a title="Click here for details" role="button" onclick="getRequisitionDetail(<?php echo $val["id"];?>)" data-id="<?php echo $val["id"];?>" id="myModalButton" data-toggle="modal" data-target="#myModal" style="cursor: hand;" href="#">
                                                <div class="bar bar-success" style="width: <?php echo $dutyTimePer ?>%;font-size: 1.2em">From <?php echo $val['start_date'].' '.$val['start_time'].' to '.$val['end_date'].' '.$val['end_time'] ?></div>
                                            </a>
                                            <div title="<?php echo $endTimePer ?>" class="bar bar-success bg-white" style="width: <?php echo $endTimePer ?>%;color: #000;font-size: 1.2em">Not Allocated For Requisition</div>
                                        </div>
                                        <!--                            --><?php //foreach($progressBarTimes as $key => $item): ?>
                                        <!--                            <div title="width: --><?php //echo $item['end'].'%'; ?><!--" class="label --><?//= $item['cls'] ?><!--" style="width: --><?php //echo $item['end'].'%'; ?><!--">--><?php //echo $item['time']; ?><!--</div>-->
                                        <!--                            --><?php //endforeach; ?>
                                        <!--                        </div>-->
                                    </td>

                                </tr>
                            <?php endforeach;?>
                        <?php else: ?>
                            <tr class=even>
                                <td style="border-bottom:1px solid #dedede;padding-left:15px" colspan="5">No Data Found</td>
                            </tr>
                        <?php endif;?>
                    <?php elseif ($searchType == 'workshop') : ?>
                        <?php if(count($result)>0) : ?>
                            <?php foreach ($result as $item => $val) :?>
                                <tr class="even"><!--fff9fb-->
                                    <td style="border-bottom:1px solid #dedede;padding-left:15px;background-color: #FAFFFB;width: 20%;">
                                        <div class="progress">
                                            <div class="label" style="font-size: 1.2em;"><?php echo $val["vehicle_reg_no"]; ?></div>
                                        </div>
                                    </td>
                                    <td style="border-bottom:1px solid #dedede;background-color: #FAFFFB;width: 80%;" >
                                        <div class="progress">
                                            <a title="Click here for details" role="button" onclick="getDefectDetail(<?= $val["id"];?>)" id="myModalButton2" data-toggle="modal" data-target="#myModal2" style="cursor: hand;" href="#">
                                                <div class="bar bar-success" style="width: 100%;font-size: 1.2em">Delivery Date: <?php echo $val["end_date"];?></div>
                                            </a>
                                        </div>
                                    </td>

                                </tr>
                            <?php endforeach;?>
                        <?php else: ?>
                            <tr class=even>
                                <td style="border-bottom:1px solid #dedede;padding-left:15px" colspan="5">No Data Found</td>
                            </tr>
                        <?php endif;?>
                    <?php endif;?>
                </table>
            </div>
        </td>
    </tr>



    </tbody>
</table>

<!--<table class=items width=50% style=background-color:#B8CCE4; id="">-->
<!--    <tr class="">-->
<!--        <th style="border-bottom:1px solid #dedede;padding-left:15px">Vehicle Reg No</th>-->
<!--        <th style="border-bottom:1px solid #dedede;">Type</th>-->
<!--        <th style="border-bottom:1px solid #dedede;">If booked when it will be free</th>-->
<!--        <th style="border-bottom:1px solid #dedede;">Duty Type</th>-->
<!--        <th style="border-bottom:1px solid #dedede;">Present Route</th>-->
<!--    </tr>-->
<!--    <tr class=even>-->
<!--        <td style="border-bottom:1px solid #dedede;padding-left:15px" colspan="5">-->
<!--            <div class="progress">-->
<!--                <div class="bar bar-success" style="width: 35%;"></div>-->
<!--                <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--                <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--            </div>-->
<!--        </td>-->
<!--    </tr>-->
<!--    --><?php //if(count($result)>0) : ?>
<!--        --><?php //foreach ($result as $item => $val) :?>
<!--            <tr class=even>-->
<!--                <td style="border-bottom:1px solid #dedede;padding-left:15px">--><?php //echo $val['vehicleRegNo']; ?><!--</td>-->
<!--                <td style="border-bottom:1px solid #dedede;">--><?php //echo $val['vehicleType']; ?><!--</td>-->
<!--                <td style="border-bottom:1px solid #dedede;">--><?php //echo $val['endDate']; ?><!--</td>-->
<!--                <td style="border-bottom:1px solid #dedede;">--><?php //echo $val['dutyType']; ?><!--</td>-->
<!--                <td style="border-bottom:1px solid #dedede;">--><?php //echo $val['presentRoute']; ?><!--</td>-->
<!--            </tr>-->
<!--            --><?php //endforeach;?>
<!--    --><?php //else: ?>
<!--        <tr class=even>-->
<!--            <td style="border-bottom:1px solid #dedede;padding-left:15px" colspan="5">No Data Found</td>-->
<!--        </tr>-->
<!--    --><?php //endif;?>
<!---->
<!--</table>-->

<!--<div class="row">-->
<!--<div class="span2">-->
<!--    <div class="progress">-->
<!--        <div>DM-11-1111</div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div>DM-11-2222</div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div>DM-11-3333</div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div>DM-11-4444</div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div>DM-11-5555</div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="span10">-->
<!--    <div class="progress">-->
<!--        <div class="bar bar-success" style="width: 35%;"></div>-->
<!--        <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--        <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div class="bar bar-success" style="width: 35%;"></div>-->
<!--        <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--        <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div class="bar bar-success" style="width: 35%;"></div>-->
<!--        <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--        <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div class="bar bar-success" style="width: 35%;"></div>-->
<!--        <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--        <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--    </div>-->
<!--    <div class="progress">-->
<!--        <div class="bar bar-success" style="width: 35%;"></div>-->
<!--        <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--        <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--    </div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="span2"></div>-->
<!--    <div class="span10">-->
<!--        <div class="progress">-->
<!--            <div class="bar bar-success" style="width: 35%;"></div>-->
<!--            <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--            <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="span2">-->
<!--        <div>DM-11-1111</div>-->
<!--    </div>-->
<!--    <div class="span10">-->
<!--        <div class="progress">-->
<!--            <div class="bar bar-success" style="width: 35%;"></div>-->
<!--            <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--            <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="span2">-->
<!--        <div>DM-11-2222</div>-->
<!--    </div>-->
<!--    <div class="span10">-->
<!--        <div class="progress">-->
<!--            <div class="bar bar-success" style="width: 35%;"></div>-->
<!--            <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--            <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="span2">-->
<!--        <div>DM-11-3333</div>-->
<!--    </div>-->
<!--    <div class="span10">-->
<!--        <div class="progress">-->
<!--            <div class="bar bar-success" style="width: 35%;"></div>-->
<!--            <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--            <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="row">-->
<!--    <div class="span2">-->
<!--        <div>DM-11-1111</div>-->
<!--    </div>-->
<!--    <div class="span10">-->
<!--        <div class="progress">-->
<!--            <div class="bar bar-success" style="width: 35%;"></div>-->
<!--            <div class="bar bar-warning" style="width: 20%;"></div>-->
<!--            <div class="bar bar-danger" style="width: 10%;"></div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

