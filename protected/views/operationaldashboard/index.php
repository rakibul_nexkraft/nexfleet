<style>
    .top-container{
        height: 100%;
    }
    .dashboard-top .dashboard-bottom {
        padding: 0;
        margin: 0 auto 0;
    }

    .dashboard-top-container {
        max-width: 90%;
        margin: 0 auto 0 auto;
        background-color: #FFF;
        padding: 0;
        display: flex;
        align-items: stretch;
        flex-wrap: wrap;
    }

    .dashboard-item {
        width: 150px;
        height: 200px;
        background-color: #EFEFEF;
        color: #1a1a1a;
        /*line-height: 100px;*/
        text-align: center;
        font-weight: bold;
        /*font-size: 60px;*/
        /*padding: 20px;*/
        margin-bottom: 20px;
        flex: 1;
        display: flex;
    }
    .d-item{
        flex: 1;
    }
    .dashboard-item:nth-child(1){
        background-color: #fbfbfb;
    }
    .dashboard-item:nth-child(2){
        background-color: #fbe9fb;
    }
    .dashboard-item:nth-child(3){
        background-color: #daedfb;
    }
    .dashboard-item:nth-child(4){
        background-color: #d2fbec;
    }

    .dashboard-table-container{
        max-width: 90%;
        margin: -35px auto 0 auto;
        /*display: flex;*/
        /*flex-direction: column;*/
    }

    .dashboard-middle{
        max-width: 90%;
        margin: 0 auto 0 auto;
    }
    .table-item{
        flex: 1;
    }
    .form-fields-container{
        /*max-width: 90%;*/
        margin: 40px auto 0 auto;
        width: 70%;
        text-align: center;
        display: flex;
    }
    .form-field{
        /*width: 15%;*/
        flex: 1;
        text-align: center;
        vertical-align: middle;
        margin-top: 10px;
        /*padding: 20px;*/
        /*margin-bottom: 20px;*/
    }
    .margin-padding-0{
        margin: 0;padding: 0;
    }
    .top-table tr td{
        margin:0;
        padding:0;
    }
    .top-table tr td:nth-child(1){
        font-weight: bold;
    }

    .dTableHead tr th{
        text-align: center;
        font-weight: bold;
        font-size: 1.2em;
    }



    .button {
        background-color:#F1F1F1;
        border-radius: 5px;
        color: black!important;
        padding: .5em;
        text-decoration: none;
        font-weight: bold;
        text-shadow: none;
    }

    .button:focus,
    .button:hover {
        background-color: #F1F1F1;
    }


    table.fixed_header {
        border-collapse: separate;
        width: auto;
        text-align:center;
        display: block;
        -webkit-border-radius:3px;
        -moz-border-radius:3px;
        border-radius:3px;
    }
    thead.fixed_header_thead {
        background-color: #EFEFEF;
    }
    tbody.fixed_header_tbody {
        display: block;
    }
    tbody.fixed_header_tbody {
        overflow-y: scroll;
        overflow-x: auto;
        min-height: 250px;
        max-height: 350px;
    }
    thead.fixed_header_thead th {
        min-width: 100px;
        height: 25px;
        border: solid 1px #DEDEDE;
    }
    tbody.fixed_header_tbody td {
        min-width: 100px;
        height: 25px;
        /*border: dashed 1px lightblue;*/
    }
    .control-group{
        margin-bottom: 0;
    }

    /*.fixed_header_thead tr th:nth-child(1){*/
    /*width: 17%;*/
    /*}*/
    /*.fixed_header_thead tr th:nth-child(2),*/
    /*.fixed_header_thead tr th:nth-child(3),*/
    /*.fixed_header_thead tr th:nth-child(4),*/
    /*.fixed_header_thead tr th:nth-child(5),*/
    /*.fixed_header_thead tr th:nth-child(6),*/
    /*.fixed_header_thead tr th:nth-child(7),*/
    /*.fixed_header_thead tr th:nth-child(8),*/
    /*.fixed_header_thead tr th:nth-child(9),*/
    /*.fixed_header_thead tr th:nth-child(10),*/
    /*.fixed_header_thead tr th:nth-child(11){*/
    /*width: 7%;*/
    /*}*/
    /*.fixed_header_thead tr th:nth-child(12){*/
    /*width: 13%;*/
    /*}*/
    /*!*.fixed_header_tbody tr:nth-child(1) >td{*!*/
    /*!*!*border: 0;*!*!*/
    /*!*height: 1px;*!*/
    /*!*!*padding: 0;*!*!*/
    /*!*margin: 0;*!*/
    /*!*}*!*/
    /*.fixed_header_tbody tr >td:nth-child(1){*/
    /*width: 17%;*/
    /*}*/
    /*.fixed_header_tbody tr >td:nth-child(2),*/
    /*.fixed_header_tbody tr >td:nth-child(3),*/
    /*.fixed_header_tbody tr >td:nth-child(4),*/
    /*.fixed_header_tbody tr >td:nth-child(5),*/
    /*.fixed_header_tbody tr >td:nth-child(6),*/
    /*.fixed_header_tbody tr >td:nth-child(7),*/
    /*.fixed_header_tbody tr >td:nth-child(8),*/
    /*.fixed_header_tbody tr >td:nth-child(9),*/
    /*.fixed_header_tbody tr >td:nth-child(10),*/
    /*.fixed_header_tbody tr >td:nth-child(11){*/
    /*width: 7%;*/
    /*}*/
    /*.fixed_header_tbody tr >td:nth-child(12){*/
    /*width: 13%;*/
    /*}*/
</style>
<?php
$baseUrl = Yii::app()->theme->baseUrl;
$cs = Yii::app()->getClientScript();
//$cs->registerScriptFile('http://www.google.com/jsapi');
$cs->registerCoreScript('jquery');
//$cs->registerScriptFile($baseUrl.'/js/jquery.gvChart-1.0.1.min.js');
//$cs->registerScriptFile($baseUrl.'/js/pbs.init.js');
$cs->registerCssFile($baseUrl.'/css/jquery.css');

?>

<?php $this->pageTitle=Yii::app()->name; ?>
<div class=clearfix></div>
<h4>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i> Dashboard</h4>

<div class="top-container">
    <div class="dashboard-top" >
        <div class="dashboard-top-container">
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Available</h4>
                    <div id="topAvailable" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="availableVehicle">
                        <span>Microbus : 0</span><br>
                        <span>Jeep : 0</span><br>
                        <span>Car : 0</span><br>
                        <span>Truck : 0</span><br>
                        <span>Minivan : 0</span>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">On-Duty</h4>
                    <div id="topOnDuty" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="ondutyVehicle">
                        <span>Microbus : 0</span><br>
                        <span>Jeep : 0</span><br>
                        <span>Car : 0</span><br>
                        <span>Truck : 0</span><br>
                        <span>Minivan : 0</span>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Workshop</h4>
                    <div id="topWorkshop" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="workshopVehicle">
                        <span>Microbus : 0</span><br>
                        <span>Jeep : 0</span><br>
                        <span>Car : 0</span><br>
                        <span>Truck : 0</span><br>
                        <span>Minivan : 0</span>
                    </div>
                </div>
            </div>
            <div class="dashboard-item">
                <div class="d-item">
                    <h4 style="text-align: center">Staff Pick/Drop</h4>
                    <div id="topStaffPickDrop" style="font-size: 100px;margin-top: 50px">0</div>
                </div>
                <div class="d-item">
                    <h4 style="text-align: center">&nbsp;</h4>
                    <div style="font-size: 12px" id="pickdropVehicle">
                        <span>Microbus : 2</span><br>
                        <span>Jeep : 4</span><br>
                        <span>Car : 8</span><br>
                        <span>Truck : 12</span><br>
                        <span>Minivan : 20</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-bottom" >
        <div class="dashboard-table-container">
            <div class="table-item">
                <div class="search-form" style="padding-left: 50px;border-radius: 3px;background-color: #FBFBFB;">
                    <form id="yw0" action="#" method="get" style="margin-bottom: 0">
                        <div class="form-fields-container">
                            <div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <select class="control-label" name="searchType" id="searchType"  >
                                            <option value="available">Available</option>
                                            <option value="onduty">On-duty</option>
                                            <option value="workshop">Workshop</option>
                                            <option value="staffpickdrop">Staff Pick/Drop</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="control-group">
                                    <div class="controls">
                                        <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                            'name'=>'searchDate',
                                            'id'=>'searchDate',
                                            'value' => '',
                                            'options'=>array('autoSize'=>true,
                                                'dateFormat'=>'yy-mm-dd',
                                                'changeYear'=>true,
                                                'changeMonth'=>true,
                                            ),

                                            'htmlOptions'=>array('style'=>'width:206px;height:20px;border-radius:2px;','placeholder'=>'Select Date'
                                            ),
                                        )); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-field">
                                <div class="control-group">
                                    <button onclick="searchVehicle()" name="searchButton" id="searchButton" type="button" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="dashboard-middle">
        <div class="table-item">
            <div style="width: 100%;margin-top: -29px;">

                <div class="grid-view" style="" id="vehicleSearchTable">
                    <table class="table table-bordered dTableHead">
                        <thead>
                        <tr>
                            <th style='width:17%'>Vehicle Reg No</th>
                            <th style='width:7%'>09</th>
                            <th style='width:7%'>10</th>
                            <th style='width:7%'>11</th>
                            <th style='width:7%'>12</th>
                            <th style='width:7%'>13</th>
                            <th style='width:7%'>14</th>
                            <th style='width:7%'>15</th>
                            <th style='width:7%'>16</th>
                            <th style='width:7%'>17</th>
                            <th style='width:7%'>18</th>
                            <th style='width:13%'>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="12">Loading Data...</td></tr>
                        </tbody>
                    </table>
                </div>
                <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <h4 style="border-bottom: 0px" id="myModalLabel">Requisition No - </h4>
                    </div>
                    <div class="modal-body">
                        <div id="requisitionData">
                            <table class='table table-hover'>
                                <tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Driver Phone : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>User Pin : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>User Name : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Dept Name : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>User Cell : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Start Point : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>End Point : </th><td style='width: 60%'></td></tr>
                                <tr><th style='width: 40%'>Time : </th><td style='width: 60%'></td></tr>
                            </table>
                        </div>
                        <!--        --><?php //$this->renderPartial('requisition_detail_modal',array('model'=>$model));?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
                <div id="myModal2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
                    <div class="modal-header">
                        <h4 style="border-bottom: 0px" id="myModalLabel2">Defect No - </h4>
                    </div>
                    <div class="modal-body">
                        <div id="defectData">
                            <table class="table table-hover">
                                <tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Apply Date : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Delivery Date : </th><td style='width: 60%'> </td></tr>
                                <tr><th style='width: 40%'>Action Taken : </th><td style='width: 60%'> </td></tr>
                            </table>
                        </div>
                        <!--        --><?php //$this->renderPartial('requisition_detail_modal',array('model'=>$model));?>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="currentDate" value="<?php echo date('Y-m-d')?>" />

</div>

<script>
    var searchVehicle = function (inputData) {
        var data;
        var currentDate = $("#currentDate").val();
        if(inputData){
            data = inputData;
        }else {
            var searchType = $("select#searchType").val();
            var searchDate = $("#searchDate").val();
            data = {
                searchType: searchType,
                searchDate: searchDate
            };
        }
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/searchByCriteria");?>',
            data:data,
            success: function(html){
                $('#vehicleSearchTable').html(html);
                $('select#searchType').val(data.searchType);
                $('input#searchDate').val(data.searchDate);
            }
        });
    };
    searchVehicle({searchType: 'available', searchDate: $("#currentDate").val()});
    setInterval(function () {
        searchVehicle({searchType: $('select#searchType').val(), searchDate: $("#searchDate").val()})
    }, 60000);

    var getVehicleCountTable = function(data) {
        var item = {};
        var html = "";
        var count = 0;
        for (i = 0; i < data.length; i++) {
            count = count + parseInt(data[i].countNo);
        }
        $.each(data, function (key, value) {
            if (value.type != "Pickup/Delivery Van" && value.type != "Refrigerator Van") {
                item[value.type] = value.countNo;
            } else {
                if (value.type === "Pickup/Delivery Van") {
                    item["PickupDeliveryVan"] = value.countNo;
                } else {
                    item["RefrigeratorVan"] = value.countNo;
                }
            }
        });
        html +=     "<table class='top-table'>" +
            "<tr><td>Ambulance</td><td> : </td><td>" + (item.Ambulance===undefined?0:item.Ambulance) + "</td></tr>" +
            "<tr><td>Bus</td><td> : </td><td>" + (item.Bus===undefined?0:item.Bus) + "</td></tr>" +
            "<tr><td>Car</td><td> : </td><td>" + (item.Car===undefined?0:item.Car) + "</td></tr>" +
            "<tr><td>Jeep</td><td> : </td><td>" + (item.Jeep===undefined?0:item.Jeep) + "</td></tr>" +
            "<tr><td>Microbus</td><td> : </td><td>" + (item.Microbus===undefined?0:item.Microbus) + "</td></tr>" +
            "<tr><td>Pickup/Delivery Van</td><td> : </td><td>" + (item.PickupDeliveryVan===undefined?0:item.PickupDeliveryVan) + "</td></tr>" +
            "<tr><td>Refrigerator Van</td><td> : </td><td>" + (item.RefrigeratorVan===undefined?0:item.RefrigeratorVan) + "</td></tr>" +
            "</table>";
        return {html:html, count:count};
    };
    var showAvailableVehicle = function(){
        var item = {};
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/topDataAvailable");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topAvailable').text(data.count);
                $('div#availableVehicle').html(data.html);
            },
            complete: function (data) {
                setTimeout(showAvailableVehicle, 30000);
            }

        });
    };
    showAvailableVehicle();
    setTimeout(showAvailableVehicle, 30000);

    var showOnDutyVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/topDataOnDuty");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topOnDuty').text(data.count);
                $('div#ondutyVehicle').html(data.html);
            }
        });
    };
    showOnDutyVehicle();
    setInterval(showOnDutyVehicle, 30000);

    var showWorkshopVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/topDataWorkshop");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topWorkshop').text(data.count);
                $('div#workshopVehicle').html(data.html);
            }
        });
    };
    showWorkshopVehicle();
    setInterval(showWorkshopVehicle, 30000);

    var showStaffPickDropVehicle = function(){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/topDataStaffPickDrop");?>',
            dataType:'json',
            success: function(data){
                var data = getVehicleCountTable(data);
                $('div#topStaffPickDrop').text(data.count);
                $('div#pickdropVehicle').html(data.html);
            }
        });
    };
    showStaffPickDropVehicle();
    setInterval(showStaffPickDropVehicle, 30000);

    function getRequisitionDetail(id){
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/getRequisitionDetail");?>',
            data:{id:id},
            dataType:'json',
            success: function(data){
                var html = "";
                if (data){
                    $('#myModalLabel').html("<h4 style='border-bottom: 0px'>Requisition No - "+data.id+"</h4>");
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'>" + data.vehicle_reg_no + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Pin : </th><td style='width: 60%'>" + data.driver_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Name : </th><td style='width: 60%'>" + data.driver_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Driver Phone : </th><td style='width: 60%'>" + data.driver_phone + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Pin : </th><td style='width: 60%'>" + data.user_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Name : </th><td style='width: 60%'>" + data.user_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Dept Name : </th><td style='width: 60%'>" + data.dept_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>User Cell : </th><td style='width: 60%'>" + data.user_cell + "</td></tr>" +
                        "<tr><th style='width: 40%'>Start Point : </th><td style='width: 60%'>" + data.start_point + "</td></tr>" +
                        "<tr><th style='width: 40%'>End Point : </th><td style='width: 60%'>" + data.end_point + "</td></tr>" +
                        "<tr><th style='width: 40%'>Time : </th><td style='width: 60%'>From <strong>" + data.start_date +" "+ data.start_time + "</strong> To <strong>" + data.end_date+" "+data.end_time + "</strong></td></tr>" +
                        "</table>";
                    $('#requisitionData').html(html);
                } else {
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><td>No data found</td></tr>" +
                        "</table>";
                    $('#requisitionData').html(html);
                }
            }
        });
    }
    function getDefectDetail(id){
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("operationaldashboard/getDefectDetail");?>',
            data:{id:id},
            dataType:'json',
            success: function(data){
                console.log("asdasdasdad "+data.id);
                var html = "";
                if (data){
                    $('#myModalLabel2').html("<h4 style='border-bottom: 0px'>Defect No - "+data.id+"</h4>");
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><th style='width: 40%'>Vehicle Reg No : </th><td style='width: 60%'>" + data.vehicle_reg_no + "</td></tr>" +
                        "<tr><th style='width: 40%'>driver_pin : </th><td style='width: 60%'>" + data.driver_pin + "</td></tr>" +
                        "<tr><th style='width: 40%'>driver_name : </th><td style='width: 60%'>" + data.driver_name + "</td></tr>" +
                        "<tr><th style='width: 40%'>Apply Date : </th><td style='width: 60%'>" + data.apply_date + "</td></tr>" +
                        "<tr><th style='width: 40%'>Delivery Date : </th><td style='width: 60%'>" + data.delivery_date + "</td></tr>" +
                        "<tr><th style='width: 40%'>Action Taken : </th><td style='width: 60%'>" + data.action_taken + "</td></tr>" +
                        "</table>";
                    $('#defectData').html(html);
                } else {
                    html += "" +
                        "<table class='table table-hover'>" +
                        "<tr><td>No data found</td></tr>" +
                        "</table>";
                    $('#defectData').html(html);
                }
            }
        });
    }

</script>