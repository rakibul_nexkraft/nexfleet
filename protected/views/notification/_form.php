<?php
/* @var $this NotificationController */
/* @var $model Notification */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'notification-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'label'); ?>
		<?php echo $form->textField($model,'label',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'label'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'display_from'); ?>
<!--		--><?php //echo $form->textField($model,'display_from'); ?>
<!--		--><?php //echo $form->error($model,'display_from'); ?>
<!--	</div>-->

    <div class="row">
        <?php echo $form->labelEx($model,'display_from'); ?>
        <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
            'model'     => $model,
            'attribute' => 'display_from',
            'language'=> 'en',//default Yii::app()->language
            'mode'    => 'datetime',//'datetime' or 'time' ('datetime' default)

            'options'   => array(
                'dateFormat' => 'yy.mm.dd',
                'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                //'ampm' => 'true',
            ),
            //'htmlOptions'=>array('readonly'=>'readonly'),
        ));
        ?>
        <?php echo $form->error($model,'display_from'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'display_until'); ?>
        <?php $this->widget('ext.jui.EJuiDateTimePicker', array(
            'model'     => $model,
            'attribute' => 'display_until',
            'language'=> 'en',//default Yii::app()->language
            'mode'    => 'datetime',//'datetime' or 'time' ('datetime' default)

            'options'   => array(
                'dateFormat' => 'yy.mm.dd',
                'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                //'ampm' => 'true',
            ),
            //'htmlOptions'=>array('readonly'=>'readonly'),
        ));
        ?>
        <?php echo $form->error($model,'display_until'); ?>
    </div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->labelEx($model,'display_until'); ?>
<!--		--><?php //echo $form->textField($model,'display_until'); ?>
<!--		--><?php //echo $form->error($model,'display_until'); ?>
<!--	</div>-->

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status'); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->