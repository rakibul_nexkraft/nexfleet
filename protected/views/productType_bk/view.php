<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>

<?php
/* @var $this ProductTypeController */
/* @var $model ProductType */

$this->breadcrumbs=array(
	'Product Types'=>array('index'),
	$model->name,
);
?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Product Type #<?php echo $model->name; ?></h3>
    </div>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table cart table_custom table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'name',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
