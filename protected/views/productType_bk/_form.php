<style>
	.form #product-type-form{
		margin-bottom: unset;
	}
</style>
<?php
/* @var $this ProductTypeController */
/* @var $model ProductType */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-type-form',
	'enableAjaxValidation'=>false,
)); ?>

<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Product Type</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'name',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
			)); ?>
			<div class="col-md-6" style="margin: -6px 0 5px -13px;">
		<?php echo $form->textField($model,'name',array('size'=>45,'maxlength'=>45,'class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
		
		
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>

		
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->