<?php
/* @var $this RoleController */
/* @var $model Role */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
       'id'=>'role-form',
       'enableAjaxValidation'=>false,
   )); ?>

   <p class="note">Fields with <span class="required">*</span> are required.</p>

   <?php echo $form->errorSummary($model); ?>

   <div class="row">
      <?php echo $form->labelEx($model,'role_name'); ?>
      <?php echo $form->textField($model,'role_name',array('size'=>60,'maxlength'=>128)); ?>
      <?php echo $form->error($model,'role_name'); ?>
  </div>
  
  <div class="row">
    <?php echo $form->labelEx($model,'Assign Rights'); ?>
    <?php  $all_rights=Rights::model()->findALL();

    if(isset($model->id)&&!empty($model->id)){
        $user_rights=RoleRights::model()->findALL('role_id=:roleid',array(':roleid'=>$model->id));
        $user_rights_code=array_map(function($element) {
           return $element['rights_id'];
       }, $user_rights);
        
    }
    foreach($all_rights as $row=>$value){

       $rights=$value['id'];
       ?>
       <div class="span4">

        <?php echo $form->checkBox($model,'rights['.$rights.']',array('checked'=>in_array($rights,$user_rights_code) ?'checked':''))." ".$value['rights_name']." "; 
        ?>
    </div>   
<?php } ?>
</div>

<div class="row buttons">
  <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
</div>

<?php $this->endWidget(); ?>

</div><!-- form -->