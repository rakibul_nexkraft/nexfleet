<style>
  .form #role-form{
    margin-bottom: unset;
  }
</style>
<?php
/* @var $this RoleController */
/* @var $model Role */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
       'id'=>'role-form',
       'enableAjaxValidation'=>false,
   )); ?>
  
  <div class="modal-header modal-create">
    <h3 class="modal-title" id="exampleModalLabel">Create Role</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>

   <p class="note">Fields with <span class="required">*</span> are required.</p>

   <?php echo $form->errorSummary($model); ?>
    
   <div class="form-group row">
    <?php echo $form->labelEx($model, 'role_name', array(
      'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
    )); ?>
    <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
      <?php echo $form->textField($model,'role_name',array('size'=>60,'maxlength'=>128,'class'=>'form-control form-control-sm')); ?>
    </div>
    <?php echo $form->error($model, 'role_name'); ?>
  </div>
  
  <div class="form-group row">
    <?php echo $form->labelEx($model,'Assign Rights', array(
      'class' => 'col-full col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
    )); ?>
    <br>
    <?php  $all_rights=Rights::model()->findALL();

    if(isset($model->id)&&!empty($model->id)){
        $user_rights=RoleRights::model()->findALL('role_id=:roleid',array(':roleid'=>$model->id));
        $user_rights_code=array_map(function($element) {
           return $element['rights_id'];
       }, $user_rights);
        
    }
    
    foreach($all_rights as $row=>$value){

       $rights=$value['id'];
    
       ?>
       <div class="col-md-3" style="margin-bottom:10px; ">
        <?php echo $form->checkBox($model,'rights['.$rights.']',array('checked'=>in_array($rights,$user_rights_code) ?'checked':''))." ".$value['rights_name']." ";
        ?>
      </div>
<?php  } ?>
</div>

  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


  </div>

  <?php $this->endWidget(); ?>
</div><!-- form -->