<?php
/* @var $this ErrorLogController */
/* @var $model ErrorLog */

$this->breadcrumbs=array(
	'Error Logs'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List ErrorLog', 'url'=>array('index')),
		array('label'=>'Create ErrorLog', 'url'=>array('create')),
		array('label'=>'Update ErrorLog', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete ErrorLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage ErrorLog', 'url'=>array('admin')),
	);
}
?>

<h1>View ErrorLog #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'message',
		'location',
		'logid',
		'tripid',
		'vehicle_reg_no',
		'driver_pin',
		'message_type',
		'message_details',
		'created_time',
	),
)); ?>
