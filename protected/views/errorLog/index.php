<?php
/* @var $this ErrorLogController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Error Logs',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create Error Log', 'url'=>array('create')),
		array('label'=>'Manage Error Log', 'url'=>array('admin')),
	);
}
?>

<h1>Error Logs</h1>
<div style="overflow-x:auto;">
<?php //$this->widget('zii.widgets.CListView', array(
	//'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',

 $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'error-log-grid',
	'htmlOptions' => array('style' => 'width:100%'),
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
			'htmlOptions'=>array('style'=>'width:2%'),
		),
		
		array(
			'name'=>'message',
			'type'=>'raw',
			'value'=>'$data->message',
			'htmlOptions'=>array('style'=>'width:20%'),
		),
		array(
			'name'=>'location',
			'type'=>'raw',
			'value'=>'$data->location',
			//'htmlOptions'=>array('style'=>'width:20%'),
		),
		array(
			'name'=>'logid',
			'type'=>'raw',
			'value'=>'$data->logid',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		array(
			'name'=>'tripid',
			'type'=>'raw',
			'value'=>'$data->tripid',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		
		array(
			'name'=>'vehicle_reg_no',
			'type'=>'raw',
			'value'=>'$data->vehicle_reg_no',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		array(
			'name'=>'driver_pin',
			'type'=>'raw',
			'value'=>'$data->driver_pin',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		array(
			'name'=>'message_type',	
			'type'=>'raw',
			'value'=>'$data->message_type',	
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
				
		
			
		array(
			'name'=>'message_details',
			'type'=>'raw',
			'value'=>'nl2br($data->message_details)',
			//'htmlOptions'=>array('style'=>'width:20%'),
		),
		array(
			'name'=>'network_status',
			'type'=>'raw',
			'value'=>'$data->network_status',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		array(
			'name'=>'requisition_id',
			'type'=>'raw',
			'value'=>'$data->requisition_id',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		array(
			'name'=>'user_message',
			'type'=>'raw',
			'value'=>'$data->user_message',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		
		array(
			'name'=>'created_time',
			'type'=>'raw',
			'value'=>'$data->created_time',
			//'htmlOptions'=>array('style'=>'width:5%'),
		),
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>


