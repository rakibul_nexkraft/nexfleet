<?php
/* @var $this ErrorLogController */
/* @var $model ErrorLog */

$this->breadcrumbs=array(
	'Error Logs'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List ErrorLog', 'url'=>array('index')),
		array('label'=>'Manage ErrorLog', 'url'=>array('admin')),
	);
}
?>

<h1>Create ErrorLog</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>