<?php
/* @var $this ErrorLogController */
/* @var $model ErrorLog */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'error-log-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textField($model,'message',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'logid'); ?>
		<?php echo $form->textField($model,'logid',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'logid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tripid'); ?>
		<?php echo $form->textField($model,'tripid',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'tripid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'vehicle_reg_no'); ?>
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'vehicle_reg_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message_type'); ?>
		<?php echo $form->textField($model,'message_type',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'message_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'message_details'); ?>
		<?php echo $form->textArea($model,'message_details',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'message_details'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'network_status'); ?>
		<?php echo $form->textField($model,'network_status'); ?>
		<?php echo $form->error($model,'network_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->