<style>
	.form #product-form{
		margin-bottom: unset;
	}
</style>

<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'product-form',
		'enableAjaxValidation' => false,
	));

	?>

	<div class="modal-header modal-create">
		<h3 class="modal-title" id="exampleModalLabel">Create Product</h3>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'name', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php #echo $form->textField($model, 'name', array('size' => 45, 'maxlength' => 45, 'class' => 'form-control form-control-sm')); 
			$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
				'id' => 'product_name',
				'model' => $model,
				'attribute' => 'name',
				'source' => $this->createUrl('/product/productName'),
				// additional javascript options for the autocomplete plugin
				'options' => array(
					'minLength' => '1',
					'select' => "js: function(event, ui) {
				 }"
				),
				'htmlOptions' => array(
					'class' => 'form-control form-control-sm',
					'placeholder' => 'Select or Create New Name',
				),
			));
			?>
		</div>
		<?php echo $form->error($model, 'name'); ?>

		<?php echo $form->labelEx($model, 'product_type_id', array(
			'class' => 'col-sm-2 col-form-label col-form-label-sm', 'style' => 'padding: 0px 0 20px 30px;'
		)); ?>
		<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
			<?php #echo $form->textField($model, 'product_type_id', array('class' => 'form-control form-control-sm')); 
			$this->widget('zii.widgets.jui.CJuiAutoComplete', array(
				'id' => 'product_type_id',
				'model' => $model,
				'attribute' => 'product_type_id',
				'source' => $this->createUrl('/product/productTypeId'),
				// additional javascript options for the autocomplete plugin
				'options' => array(
					'minLength' => '2',
					'select' => "js: function(event, ui) {
				 }"
				),
				'htmlOptions' => array(
					'class' => 'form-control form-control-sm',
					'placeholder' => 'Select Product Type',
					'autocomplete'=> 'off',
				),
			));
			?>
		</div>
		<?php echo $form->error($model, 'product_type_id'); ?>
	</div>
	<h3 class="modal-title" id="exampleModalLabel" style="padding: 0 0 25px 10px;">Product Attribute</h3>
	<?php
	$i = 1;
	foreach ($model_attribute->product_attributes_array as $key => $array_value) {
		if ($i % 2 != 0) {
			?>
			<div class="form-group row">
			<?php } ?>
			<label style="padding: 0px 0 20px 30px;" class="col-sm-2 col-form-label col-form-label-sm" for=""><?php echo $array_value; ?></label>
			<div class="col-sm-4" style="margin: -6px 0 5px -13px;">
				<input class="form-control form-control-sm" placeholder="<?php echo $array_value; ?>" name="product_attributes[<?php echo $key; ?>]" type="text">
			</div>

			<?php if ($i % 2 == 0 || count($model_attribute->product_attributes_array) == $i) { ?>
			</div>
	<?php }
		$i++;
	}
	?>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>


		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>


	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->