<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>
<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->name,
);
?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Product #<?php echo $model->id; ?></h3>
    </div>

<?php $this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>array(
		'id',
		'name',
		array(
			'name' => 'product_type_id',
			'value' => ProductType::getProductType($model->product_type_id),
		),
		'created_by',
		'created_time',
		/*'updated_by',
		'updated_time',*/
	),
)); ?>


<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4">Product Attributes #<?php echo $model->id; ?></h3>
    </div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-attribute-grid',
	'itemsCssClass' => 'table cart table_um',
    'htmlOptions' => array('class' => 'table-responsive bottommargin, admin-table-responsive border-round-bottom'),
	'dataProvider'=>$model_attri->search(),
	'columns'=>array(
		'id',
    array(
            'name' => 'product_id',
            'type' => 'raw',
            'value' => 'Product::getProductName($data->product_id)',
            ),
		/*'category',*/
		'label',
		'value',
    array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => 'Yii::app()->user->getUserName($data->created_by)',
            ),
		/*'created_time',
		'updated_by',
		'updated_time',*/
		)));?>