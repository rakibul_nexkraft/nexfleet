<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Manage',
);

?>

<?php #$this->renderPartial('_search',array(
	#'model'=>$model,
#)); ?>
</div><!-- search-form -->

<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin">
    <h4 class="heading-custom page_header_h4">List of Products</h4>

    <input style="width: 20% !important;float: right;margin-bottom: 10px;" class="btn-search button-pink" id="search-button" onclick="userCreate()" name="" type="submit" value="Create Product">
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-grid',
	'itemsCssClass' => 'table cart table_um',
    'htmlOptions' => array('class' => 'table-responsive bottommargin, admin-table-responsive border-round-bottom'),
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'id',
    'name',
    array(
            'name' => 'product_type_id',
            'type' => 'raw',
            'value' => 'ProductType::getProductType($data->product_type_id)',
            ),
    array(
            'name' => 'created_by',
            'type' => 'raw',
            'value' => 'Yii::app()->user->getUserName($data->created_by)',
            ),
		/*'created_time',
		'updated_by',
		'updated_time',*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{attributes} {view} {update} {delete} ',
			'buttons'=> array(
        'attributes' => array(
                              'label'=>'View Attributes',
                              'imageUrl'=>Yii::app()->request->baseUrl.'/images/submit.jpg',
                              'url'=>'Yii::app()->createUrl("productAttribute/admin", array("id"=>$data->id))',
                              'options' => array('style' => 'padding: 5px 5px 0 0 !important;'),
                    ),
				'view' => array(
                        'url' => 'CHtml::normalizeUrl(array("/product/userView/id/". $data->id . "/type/1&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
				'update' => array(
                        'url' => 'CHtml::normalizeUrl(array("/product/userView/id/". $data->id . "/type/2&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
				'delete' => array(
                        'url' => 'CHtml::normalizeUrl(array("/product/userView/id/". $data->id . "/type/3&ajax=true"));',
                        'click' => 'function(e) {
                                      $("#ajaxModal").remove();
                                      e.preventDefault();
                                      var $this = $(this)
                                        , $remote = $this.data("remote") || $this.attr("href")
                                        , $modal = $("<div class=\'modal\' id=\'ajaxModal\'><div class=\'modal-body\'><h5 align=\'center\'> <img src=\'' . Yii::app()->request->baseUrl . '/images/ajax-loader.gif\'>&nbsp;  Please Wait .. </h5></div></div>");
                                      $("body").append($modal);
                                      $modal.modal({backdrop: "static", keyboard: false});
                                      $modal.load($remote);
                                    }',
                        'options' => array('data-toggle' => 'ajaxModal','style' => 'padding:4px;'),
                    ),
				'htmlOptions' => array('style' => 'width: 100px;text-align: left;'),
			),
		),
	),
)); ?>

<script>
	function userCreate(id) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/Product/Create");?>',{id:id},function(data){
                bsModalOpen(data);
            });
        
    }
</script>