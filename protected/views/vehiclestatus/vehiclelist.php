<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/15/2019
 * Time: 12:49 PM
 */?>
<div class="widget quick-contact-widget clearfix">
    <h4>Vehicle List</h4>
    <div class="list-group fadeIn animated">
        <?php
        if (count($vehicles) == 0) echo "No vehicles found!";
        foreach ($vehicles as $vehicle) {
            echo
                '<a class="list-group-item list-group-item-action active cursor-pointer" onclick="getVehicleDetails(' . "'" . $vehicle['reg_no'] . "'" . ')">'.
                    $vehicle['reg_no'].
                '</a>';
        }?>

    </div>
</div>
<script>
    function getVehicleDetails(vreg) {
        $("#vehicleDetails").html("<div class='nk-loader'></div>");
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/getVehicleDetails");?>',
            data: {vehicle_reg_no: vreg},
            dataType:'html',
            success: function(html){
                $("#vehicleDetails").html(html);
            }
        });
    }

</script>