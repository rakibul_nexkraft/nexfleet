<style>
    .loading-anim{

        /*background: url(< ?php echo Yii::app()->baseUrl?>'/common/assets/imagimages/ellipsis-loader.gif') no-repeat center center;*/

        display: block;
        width: 100%;
        height: 100%;
        background: url("<?php echo Yii::app()->baseUrl?>/common/assets/images/preloader.gif") center center no-repeat #FFF;
    }
    #map {
        height: 68vh;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
    }
</style>
<!--<pre>< ?php var_dump($vehicle->attributes) ?></pre>-->
<div class="col_full animated fadeIn">
    <h3>Vehicle Map</h3>

    <div class="map-container">
        <div id="map"></div>

        <!--<div id="map-overlay">
            <div id="google-map">
                <div class="nk-loader" style=""></div>
            </div>
        </div>-->
    </div>
</div>
<input type="hidden" id="vehicle_data" value="<?php echo $vehicle['reg_no'].','.$vehicle['asset_id'] ?>"">

<div class="clear"></div>
<script>
    /* GOOGLE MAP */
    var markerIcon = "<?php echo Yii::app()->baseUrl; ?>/assets/icons/new/car-map.png";
    var map;
    var marker;
    var vehicles = [];
    function initialize(latitude,longitude) {
        var lat = parseFloat(latitude);
        var long = parseFloat(longitude);
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: lat, lng: long},
            zoom: 16
        });
        marker = new google.maps.Marker({
            draggable: true,
            position: new google.maps.LatLng(lat, long),
            map: map,
            animation: google.maps.Animation.DROP,
            title: "Brac Center"
        });
        getVehiclesWithAssetId(map, marker);
        //setMarker(map, marker);
    }

    function addMarker(map, vehicle) {
        var v = vehicle.coordinates.split(",");
        var infowindow = new google.maps.InfoWindow({
//            content: '<span style="padding: 3px 5px;font-size: 1rem;background-color: white">'+ vehicle.vehicle_reg_no + '</span>',
            content:'<div id="content" style="background: white;">'+
            '<h5 id="firstHeading" class="firstHeading">' + vehicle.vehicle_reg_no + '</h5>'+
            '<div><a class="cursor-pointer link" onclick="getVehicleDetails(' + '\'' + vehicle.vehicle_reg_no + '\'' + ')"> Get Details</a></div>'+
            '</div>',
            disableAutoPan: true
        });
        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(v[0], v[1]),
            label: {
                text: vehicle.vehicle_reg_no,
                color: '#000',
                fontSize: '14px',
                fontWeight: 'bold'
            },
            icon: {
                url: markerIcon,
                labelOrigin: new google.maps.Point(0, 0)
            }
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
        //new google.maps.event.trigger( marker, 'click' );

    }

    /*GET VEHICLES WITH ASSET ID*/
    function getVehiclesWithAssetId(map, marker) {
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/getVehiclesWithAssetId");?>',
            dataType:'json',
            success: function(data){
                vehicles = data;
                setMarker(map, marker, vehicles);
            }
        });
    }

    // Testing the addMarker function
    function setMarker(map, marker, vehicles) {
        vehicles.forEach(function (vehicle) {
            var vehicleData = vehicle.reg_no + ',' + vehicle.asset_id;
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl("vehiclestatus/getVehicleLocation");?>',
                dataType:'json',
                data: {vehicle_data: vehicleData},
                success: function(data){
                    if (data.success){
                        console.log(data);
                        addMarker(map, data);
                    }
                }
            });
        });
    }
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    //    google.maps.event.addDomListener(window, 'load', initialize);
    //    initialize(23.780, 90.410);
    initialize(23.7798801, 90.4102569);

</script>