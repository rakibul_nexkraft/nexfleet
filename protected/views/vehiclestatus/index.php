<?php
/* @var $this VehiclestatusController */

?>
<style>
    /*@media (min-width: 1200px) {
        .container{
            max-width: 970px;
        }
    }*/

    @media (min-width: 768px) {
        .bd-placeholder-img-lg {
            font-size: 3.5rem;
        }
    }
    @media (max-width: 991px) {
        #logo {
            float: left;
        }
        #header-trigger, .top-advert, .header-extras {
            display: block;
        }
    }
    .box-shadow {
        box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .05);
    }

    #vehicles::-webkit-scrollbar {
        width: 8px;
        border-radius: 10px;
    }

    #vehicles::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background: white;
    }

    #vehicles::-webkit-scrollbar-thumb {
        background-color: blue;
        outline: 1px solid slategrey;
        border-radius: 5px;
    }

    .list-group .list-group-item{
        border-radius: 0 !important;
    }

    .search-bar {
        background-color: rgba(28,206,234,0.82) !important;
        background: linear-gradient(-45deg, rgba(147,26,222,0.83) 0%, rgba(28,206,234,0.82) 100%) !important;
    }
    .form-search{
        max-width: 650px;
        padding: 0 15px;
        position: relative;
    }
    .form-search .form-control-search{
        background-color: rgba(115,186,221,0.35);
        box-shadow: none;
        border: medium none;
        border-radius: 30px;
        box-shadow: 0 0 0;
        color: #FFFFFF;
        display: block;
        font-size: 20px;
        font-weight: 300;
        height: 50px;
        line-height: 1.42857;
        padding: 6px 20px;
        vertical-align: middle;
        transition: background-color .2s;
    }

    input.form-control-search:focus, input.form-control-search:hover{
        background-color: #b5dcef59;
        color: white !important;
    }

    .search-bar::-webkit-input-placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        color: white !important;
        opacity: 1; /* Firefox */
    }

    .search-bar:-ms-input-placeholder { /* Internet Explorer 10-11 */
        color: white !important;;
    }

    .search-bar:-ms-input-placeholder { /* Microsoft Edge */
        color: white !important;;
    }
    ::-webkit-input-placeholder{
        color: white !important;
    }
    ::-moz-placeholder{
        color: white !important;
    }
    .header-extras li {
        float: left;
        margin-left: 20px;
        height: 40px;
        overflow: hidden;
        list-style: none;
        /*width: 60px;*/
    }
	.loading-anim{

        /*background: url(< ?php echo Yii::app()->baseUrl?>'/common/assets/imagimages/ellipsis-loader.gif') no-repeat center center;*/

        display: block;
        width: 100%;
        height: 100%;
        background: url("<?php echo Yii::app()->baseUrl?>/common/assets/images/preloader.gif") center center no-repeat #FFF;
    }
</style>

<!-- Header
		============================================= -->
<header id="header" class="sticky-style-1">

    <div class="container clearfix">

        <!-- Logo
        ============================================= -->
        <!--<div id="logo">
            <a href="<?php /*echo Yii::app()->baseUrl; */?>/index.php/vehiclestatus" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?php /*echo Yii::app()->baseUrl; */?>/common/assets/images/ifleet-logo-2.png" alt="BRAC Logo"></a>
            <a href="<?php /*echo Yii::app()->baseUrl; */?>/index.php/vehiclestatus" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?php /*echo Yii::app()->baseUrl; */?>/common/assets/images/ifleet-logo-2.png" alt="BRAC Logo"></a>
        </div>--><!-- #logo end -->
        <div id="logo" style="display: flex;align-items: center;">
            <a href="<?php echo Yii::app()->user->myDeskUser==1?Yii::app()->createUrl('/vehiclestatus/index'):Yii::app()->baseUrl; ?>" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?php echo Yii::app()->baseUrl; ?>/common/assets/images/ifleet-logo-2.png" alt="iFleet Logo"></a>
            <a href="<?php echo Yii::app()->user->myDeskUser==1?Yii::app()->createUrl('/vehiclestatus/index'):Yii::app()->baseUrl; ?>" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?php echo Yii::app()->baseUrl; ?>/common/assets/images/ifleet-logo-2.png" alt="iFleet Logo"></a>
            <span class="txt" style="font-size: 38px;color: #868686;font-weight:bold;margin-left: 10px;">iFleet</span>
        </div>

<!--        <div class="header-extras">-->
<!--            <img src="--><?php //echo Yii::app()->baseUrl; ?><!--/common/assets/images/ifleet-logo.png" alt="iFleet Logo">-->
<!--        </div>-->
		<div class="header-extras tright">
            <div class="he-text" style="display: flex;justify-content: flex-end;align-items: center">
                <span style="font-weight:bold"><?php echo Yii::app()->user->firstname. " ". Yii::app()->user->lastname; ?></span>
            </div>
            <div class="he-text" style="display: flex;justify-content: flex-end;align-items: center">
                <a href="<?php echo Yii::app()->createUrl('/user/logout'); ?>" class="link">Logout</a>
            </div>
        </div>
        <!--<ul class="header-extras">
            <li>

                <div class="he-text" style="display: flex;justify-content: center;align-items: center">
                    <img src="<?php echo Yii::app()->baseUrl; ?>/common/assets/images/ifleet-logo.png" alt="iFleet Logo" style="width: 60px;"> <span style="font-size: 30px;color: #868686;font-weight:bold">iFleet</span>
                </div>
            </li>
            <li>
                <i class="i-plain icon-call nomargin"></i>
                <div class="he-text">
                    Get in Touch
                    <span>01X-XXXX-YYYY</span>
                </div>
            </li>
        </ul>-->

    </div>

</header><!-- #header end -->

<!--<section id="page-title" class="page-title-center page-title-nobg noborder">
    <div class="container clearfix">
        <h1>Vehicle Status</h1>
        <span>Showcase of requisition status of 39 vehicles</span>
    </div>
</section>-->
<section id="content" style="margin-bottom: 0px;margin-top: 10px" onload="searchVehicle()">

    <div class="content-wrap notoppadding nobottompadding ">

        <div class="single-event">

            <div class="container clearfix">

                <!-- Vehicle Search Left Panel
                =============================== -->
                <div class="col_one_fourth ">

                    <div class="widget quick-contact-widget clearfix" style="/*background-color: #ffa131;*/background-color: #36275d;">

                        <div style="padding: 10px" class="search-bar form-search">
                            <h4 style="text-align: center;font-weight: 600;text-transform: uppercase;color: white">Search Vehicle</h4>

                            <div class="col_full nobottommargin" style="border-radius: 30px !important;border: none;overflow: hidden;">
                                <input type="text" id="inputReg" onkeyup="searchVehicle()" class="required sm-form-control input-block-level nomargin noborder form-control-search" placeholder="Enter Vehicle Reg No" autocomplete="off">
                            </div>
                            <div class="text-center widget quick-contact-widget clearfix" style="margin: 10px auto;">
                                <h4 style="text-align: center;font-weight: 600;text-transform: uppercase;color: white" class="nobottommargin">Or</h4>
                            </div>
                            <div class="col_full bottommargin-sm" style="border-radius: 30px !important;border: none;overflow: hidden;">
                                <input type="text" id="inputPin" onkeyup="searchVehicle()" class="required sm-form-control input-block-level nomargin noborder form-control-search" placeholder="Enter Driver Pin" autocomplete="off">
                            </div>
                        </div>
                        <div class="list-group fadeIn animated custom-height" id="vehicles" style="max-height: 45vh; overflow: hidden; overflow-y: auto">
                            <?php
                            foreach ($vehicles as $vehicle) {
                                echo
                                    '<a class="list-group-item list-group-item-action cursor-pointer" onclick="getVehicleDetails(' . "'" . $vehicle['reg_no'] . "'" . ')">'.
                                    '<div id="vReg">'.
                                        $vehicle['reg_no'].
                                    '</div>'.
                                    '<div id="dPin" class="small">Driver Pin: '.
                                        $vehicle['driver_pin'].
                                    '</div>'.
                                    '</a>';
                            }
                            echo '<a class="list-group-item list-group-item-action cursor-pointer" id="notFound"><i>No Vehicles Matched!</i></a>';
                            ?>

                        </div>

                    </div>
                    <div id="vehicleList"></div>

                </div><!-- Vehicle Search Left Panel End -->

                <!-- Vehicle Details Right Body
                ================================ -->
                <div class="col_three_fourth col_last">

                    <div class="col_full" id="vehicleDetails">
                        <section id="page-title" class="page-title-center">

                            <div class="clearfix">
                                <div class='loading-anim'></div><div class='center' style='font-size: 16px;position: relative;bottom: 7rem;'>Loading Map ...</div>
                            </div>

                        </section>
                    </div>

                </div><!-- Vehicle Details Right Body End -->

                <div class="clear"></div>

            </div>

        </div>

    </div>

</section>

<script>
    function getVehicleDetails(vreg) {
        $("#vehicleDetails").html("<div class='nk-loader'></div>");
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/getVehicleDetails");?>',
            data: {vehicle_reg_no: vreg},
            dataType:'html',
            success: function(html){
                $("#vehicleDetails").html(html);
            }
        });
    }
    function searchVehicle() {
        var inputReg, inputPin, filterReg, filterPin, vehicles, vItems, reg, pin, i, txtValueReg, txtValuePin, counter=0;
        inputReg = document.getElementById("inputReg");
        inputPin = document.getElementById("inputPin");
        filterReg = inputReg.value.toUpperCase();
        filterPin = inputPin.value.toUpperCase();
        vehicles = document.getElementById("vehicles");
        vItems = vehicles.getElementsByTagName("a");
        for (i = 0; i < vItems.length; i++) {
            reg = vItems[i].getElementsByTagName("div")[0];
            pin = vItems[i].getElementsByTagName("div")[1];
            txtValueReg = reg.textContent || reg.innerText;
            txtValuePin = pin.textContent || pin.innerText;
            if (txtValueReg.toUpperCase().indexOf(filterReg) > -1 && txtValuePin.toUpperCase().indexOf(filterPin) > -1) {
                vItems[i].style.display = "";
            } else {
                vItems[i].style.display = "none";
                counter++;
            }
            if((filterReg.length > 0 || filterPin.length > 0) && counter == vItems.length-1) {
                document.getElementById('notFound').style.display = "block";
            } else {
                document.getElementById('notFound').style.display = "none";
            }
        }
    }

</script>
<script>
    function initMainMap() {
        $.ajax({
            type:'GET',
            url:'<?php echo Yii::app()->createUrl("vehiclestatus/getMainMap");?>',
            dataType:'html',
            success: function(html){
                $("#vehicleDetails").html(html);
            }
        });
    }
    //initMainMap();


    setInterval(function () {
        window.location.reload(true)
    }, 300000);
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyiFpj3wAKztW2X7NUkBSW0CW_lXvG45E&callback=initMainMap">
</script>