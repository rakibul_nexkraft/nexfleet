<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this FieldrepairController */
/* @var $model Fieldrepair */

$this->breadcrumbs=array(
	'Fieldrepairs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List Field Repair', 'url'=>array('index'),'visible'=> in_array('List Field Repair', $user_rights)),
		array('label'=>'Create Field Repair', 'url'=>array('create'),'visible'=> in_array('Create Field Repair', $user_rights)),
		array('label'=>'View Field Repair', 'url'=>array('view', 'id'=>$model->id),'visible'=> in_array('View Field Repair', $user_rights)),
		array('label'=>'Manage Field Repair', 'url'=>array('admin'),'visible'=> in_array('Manage Field Repair', $user_rights)),
	);
}
?>

<h4>Update Fieldrepair <?php echo $model->id; ?></h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>