<?php 
$user_rights = Yii::app()->user->getUserRights();
?>

<?php
/* @var $this FieldrepairController */
/* @var $model Fieldrepair */

$this->breadcrumbs=array(
	'Fieldrepairs'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
    $this->menu=array(
    	array('label'=>'List Field Repair', 'url'=>array('index'),'visible'=> in_array('List Field Repair', $user_rights)),
    	array('label'=>'Create Field Repair', 'url'=>array('create'),'visible'=> in_array('Create Field Repair', $user_rights)),
    	array('label'=>'Update Field Repair', 'url'=>array('update', 'id'=>$model->id),'visible'=> in_array('Update Field Repair', $user_rights)),
    	array('label'=>'Delete Field Repair', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'visible'=> in_array('Delete Field Repair', $user_rights)),
    	array('label'=>'Manage Field Repair', 'url'=>array('admin'),'visible'=> in_array('Manage Field Repair', $user_rights)),
    );
}
?>

<h4>View Fieldrepair #<?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.XDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'group2'=>array(
            'ItemColumns' => 2,
            'attributes' => array(
                'id',				    'onsite_name',
                'vehicle_reg_no',		'approve_by',
                array('name'=>'vehicletype_id', 'value'=>$model->vehicletypes->type),	'program_name',
                'driver_name',			    'spare_amount',
                'driver_pin',		        'spare_description',
                'last_meter',		        'labor_amount',
                'meter_reading',		    'labor_description',
                'apply_date',		        'machine_shop_amount',
                'fieldrepair_description',	'machine_shop_description',
                array('name'=>'created by','value'=>$user_profile->firstname ),	 'vehicle_towchain_amount',
                'created_time',		        'vehicle_towchain_description',
                'updated_by',		        'remarks',
                'updated_time',


            ),
        ),
    ),
));
?>
<?php //$this->widget('zii.widgets.CDetailView', array(
//	'data'=>$model,
//	'attributes'=>array(
//		'id',
//		'vehicle_reg_no',
//		'vehicletype_id',
//		'driver_name',
//		'driver_pin',
//		'last_meter',
//		'meter_reading',
//		'apply_date',
//		'fieldrepair_description',
//		'onsite_name',
//		'approve_by',
//		'program_name',
//		'spare_amount',
//		'spare_description',
//		'labor_amount',
//		'labor_description',
//		'machine_shop_amount',
//		'machine_shop_description',
//		'vehicle_towchain_amount',
//		'vehicle_towchain_description',
//		'remarks',
//		'created_by',
//		'created_time',
//		'updated_by',
//		'updated_time',
//	),
//)); ?>
