<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $model DigitalLogBookHistory */

$this->breadcrumbs=array(
	'Digital Log Book Histories'=>array('index'),
	$model->id,
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List DigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Create DigitalLogBookHistory', 'url'=>array('create')),
		array('label'=>'Update DigitalLogBookHistory', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete DigitalLogBookHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage DigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>View DigitalLogBookHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'login_id',
		'trip_id',
		'requisition_id',
		'login_end',
		'trip_id_end',
		'duty_day',
		'driver_pin',
		'user_pin',
		'user_name',
		'department',
		//'duty_type',
		'dutytype_id',
		'driver_start_time_vts',
		'driver_start_time_mobile',
		'driver_end_time_vts',
		'driver_end_time_mobile',
		'meter_number',
		'location_vts',
		'location_mobile',
		'fuel_recipt_no',
		'fuel_total_bill',
		'night_halt_bill',
		'morning_ta',
		'lunch_ta',
		'night_ta',
		'running_per_km_rate',
		'running_km',
		'running_km_bill',
		'over_time_hour_rate',
		'over_time',
		'over_time_bill',
		'total_bill',
		'created_time_vts',
		'created_time_mobile',
		'created_by',
		'updated_time_vts',
		'updated_time_mobile',
		'updated_by',
	),
)); ?>
