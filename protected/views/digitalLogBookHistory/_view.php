<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $data DigitalLogBookHistory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_id')); ?>:</b>
	<?php echo CHtml::encode($data->login_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trip_id')); ?>:</b>
	<?php echo CHtml::encode($data->trip_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('miei')); ?>:</b>
	<?php echo CHtml::encode($data->miei); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ignition_status')); ?>:</b>
	<?php echo CHtml::encode($data->ignition_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('speed')); ?>:</b>
	<?php echo CHtml::encode($data->speed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('requisition_id')); ?>:</b>
	<?php echo CHtml::encode($data->requisition_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_end')); ?>:</b>
	<?php echo CHtml::encode($data->login_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trip_id_end')); ?>:</b>
	<?php echo CHtml::encode($data->trip_id_end); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('day_end')); ?>:</b>
	<?php echo CHtml::encode($data->day_end); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('duty_day')); ?>:</b>
	<?php echo CHtml::encode($data->duty_day); ?>
	<br />

	 
	<?php echo CHtml::encode($data->getAttributeLabel('driver_pin')); ?>:</b>
	<?php echo CHtml::encode($data->driver_pin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_pin')); ?>:</b>
	<?php echo CHtml::encode($data->user_pin); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_name')); ?>:</b>
	<?php echo CHtml::encode($data->driver_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('department')); ?>:</b>
	<?php echo CHtml::encode($data->department); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_reg_no); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dutytype_id')); ?>:</b>
	<?php echo CHtml::encode($data->dutytype_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_start_time_vts')); ?>:</b>
	<?php echo CHtml::encode($data->driver_start_time_vts); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_start_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->driver_start_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_end_time_vts')); ?>:</b>
	<?php echo CHtml::encode($data->driver_end_time_vts); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('driver_end_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->driver_end_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('meter_number')); ?>:</b>
	<?php echo CHtml::encode($data->meter_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_mode')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_mode); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('vts_longitude')); ?>:</b>
	<?php echo CHtml::encode($data->vts_longitude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vts_latitude')); ?>:</b>
	<?php echo CHtml::encode($data->vts_latitude); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('location_vts')); ?>:</b>
	<?php echo CHtml::encode($data->location_vts); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->location_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuel_recipt_no')); ?>:</b>
	<?php echo CHtml::encode($data->fuel_recipt_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuel_quantity')); ?>:</b>
	<?php echo CHtml::encode($data->fuel_quantity); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('fuel_unit_price')); ?>:</b>
	<?php echo CHtml::encode($data->fuel_unit_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fuel_total_bill')); ?>:</b>
	<?php echo CHtml::encode($data->fuel_total_bill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night_halt_bill')); ?>:</b>
	<?php echo CHtml::encode($data->night_halt_bill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('morning_ta')); ?>:</b>
	<?php echo CHtml::encode($data->morning_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lunch_ta')); ?>:</b>
	<?php echo CHtml::encode($data->lunch_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('night_ta')); ?>:</b>
	<?php echo CHtml::encode($data->night_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('running_per_km_rate')); ?>:</b>
	<?php echo CHtml::encode($data->running_per_km_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('running_km')); ?>:</b>
	<?php echo CHtml::encode($data->running_km); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('running_km_bill')); ?>:</b>
	<?php echo CHtml::encode($data->running_km_bill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('over_time_hour_rate')); ?>:</b>
	<?php echo CHtml::encode($data->over_time_hour_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('over_time')); ?>:</b>
	<?php echo CHtml::encode($data->over_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('over_time_bill')); ?>:</b>
	<?php echo CHtml::encode($data->over_time_bill); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_bill')); ?>:</b>
	<?php echo CHtml::encode($data->total_bill); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('others_cost')); ?>:</b>
	<?php echo CHtml::encode($data->others_cost); ?>
	
	
	<br /><b><?php echo CHtml::encode($data->getAttributeLabel('others_bill')); ?>:</b>
	<?php echo CHtml::encode($data->others_bill); ?>
	<br />
	<b><?php echo CHtml::encode($data->getAttributeLabel('others_description')); ?>:</b>
	<?php echo CHtml::encode($data->others_description); ?>

	
	<br /><b><?php echo CHtml::encode($data->getAttributeLabel('first_on_time')); ?>:</b>
	<?php echo CHtml::encode($data->first_on_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_off_time')); ?>:</b>
	<?php echo CHtml::encode($data->last_off_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_on_time')); ?>:</b>
	<?php echo CHtml::encode($data->total_on_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_on_count')); ?>:</b>
	<?php echo CHtml::encode($data->total_on_count); ?>
	<br />		

	<b><?php echo CHtml::encode($data->getAttributeLabel('api_status')); ?>:</b>
	<?php echo CHtml::encode($data->api_status); ?>
	<br />	

	<b><?php echo CHtml::encode($data->getAttributeLabel('vts_message')); ?>:</b>
	<?php echo CHtml::encode($data->vts_message); ?>
	<br />		
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('remark')); ?>:</b>
	<?php echo CHtml::encode($data->remark); ?>
	<br />		
		
	<b><?php echo CHtml::encode($data->getAttributeLabel('objection')); ?>:</b>
	<?php echo CHtml::encode($data->objection); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('repair_start_time')); ?>:</b>
	<?php echo CHtml::encode($data->repair_start_time); ?>
	<br />		
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('repair_end_time')); ?>:</b>
	<?php echo CHtml::encode($data->repair_end_time); ?>
	<br />	
			
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time_vts')); ?>:</b>
	<?php echo CHtml::encode($data->created_time_vts); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->created_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time_vts')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time_vts); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	 

</div>