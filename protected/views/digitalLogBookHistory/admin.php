<style>
tr>td>a>img {
	max-width: 30px;
}
</style>
<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $model DigitalLogBookHistory */

$this->breadcrumbs=array(
	'Digital Log Book Histories'=>array('index'),
	'Manage',
);

//$this->menu=array(
	//array('label'=>'List DigitalLogBookHistory', 'url'=>array('index')),
	//array('label'=>'Create DigitalLogBookHistory', 'url'=>array('create')),
//);

/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('digital-log-book-history-grid', {
		data: $(this).serialize()
	});
	return false;
});
");*/
?>

<!--<h1>Manage Digital Log Book Histories</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div><!-- search-form -->
<h1>Digital Log Book History</h1>
<div class="search-form">
	<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'enableAjaxValidation'=>true,
	'method'=>'get',
)); ?>


<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?>  <br />
		<?php echo $form->textField($model,'requisition_id',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
	</div>
</div>
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>  <br />
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium','maxlength'=>127)); ?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>  <br />
		<?php echo $form->textField($model,'driver_pin',array('size'=>18,'class'=>'input-medium')); ?>
	</div>
</div>



<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'from_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'from_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'to_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'to_date',
				//'value'=>Yii::app()->request->cookies['to_date']->value,
				'options'=>array(
				//	'showAnim'=>'fold',
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->to_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
				),
			));
		?>
	</div>
</div>
</fieldset>






	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

</div>
<div class="search-form">
	<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>'',	
	'id'=>'meter-adjust',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('onsubmit' =>'return false'),
)); ?>

<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'vehicle_reg_no'); ?>  <br />
		<?php echo $form->textField($model,'vehicle_reg_no',array('size'=>18,'class'=>'input-medium','maxlength'=>127, 'id'=>'vehicle_no')); ?>
	</div>
</div>
<fieldset name="Start Date Wise Sorting" style="float:left; padding:0 0 0 10px; margin-right:10px;">
<div class="fl">
	<div class="row">
		<?php echo $form->label($model,'adjust_date'); ?><br />
		<?php
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,
				'attribute'=>'adjust_date',  // name of post parameter
				//'value'=>Yii::app()->request->cookies['from_date']->value,  // value comes from cookie after submittion
				'options'=>array(
					'dateFormat'=>'yy-mm-dd',
					//'defaultDate'=>$model->from_date,
					'changeYear'=>true,
					'changeMonth'=>true,
				),
				'htmlOptions'=>array(
					'style'=>'height:17px; width:138px;',
					'autocomplete'=>'off',
					'id'=>'adjust_date'
				),
			));
		?>
	</div>
</div>

</fieldset>






	<div class="clearfix"></div>
    
    <div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Adjust',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium',
        'htmlOptions'=>array('onclick'=>'adjustMeter()'),
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->

</div>

<div style="overflow-x:auto;">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'digital-log-book-history-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'login_id',
		'trip_id',
		'requisition_id',
		/*array(
			'name'=>'login_end',
			'type'=>'raw',	
			'value'=>'($data->login_end=="1"?"Logout":($data->login_end=="0"?"Login":$data->login_end))',
		),
		array(
			'name'=>'trip_id_end',
			'type'=>'raw',	
			'value'=>'($data->trip_id_end=="1"?"Trip End":($data->trip_id_end=="0"?"Trip":$data->trip_id_end))',
		),*/		
		'login_end',
		'trip_id_end',
		'vehicle_reg_no',
		'duty_day',
		'driver_pin',
		'user_pin',
		'user_name',
		'department',
		'vehicle_type_id',
		'dutytype_id',
		'driver_start_time_vts',
		'driver_start_time_mobile',
		'driver_end_time_vts',
		'driver_end_time_mobile',
		'meter_number',
		'new_meter_number',
		'vehicle_mode',
		'location_vts',
		'location_mobile',
		'fuel_recipt_no',
		'fuel_unit_price',
		'fuel_quantity',
		'fuel_total_bill',
		'night_halt_bill',
		'morning_ta',
		'lunch_ta',
		'night_ta',
		'running_per_km_rate',
		'running_km',
		'new_km',
		'running_km_bill',
		'over_time_hour_rate',
		'over_time',
		'over_time_bill',
		'total_bill',
		'others_cost',
		'others_bill',
		'others_description',
		'objection',
		'repair_start_time',
		'repair_end_time',
		'day_end',
		'created_time_vts',
		'created_time_mobile',
		'missing_KM',
		'created_by',
		'message_log',
		'bundle',

		//'updated_time_vts',
		//'updated_time_mobile',
		//'updated_by',
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}',
			'buttons'=>array
    		(
        		'update' => array
        		(
        			'label'=>'Update',
        			'imageUrl'=>Yii::app()->request->baseUrl.'/images/update.jpg',
        		)
        	),
        	
		),
	),
)); ?>
<?php
    $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'requisitionDetailModal', 'htmlOptions' => array('class'=>'requisitionDetailsModalCustom')));

    echo '<div id="form-modal" class="modal-body">';
    echo '<div id="view-body">Loading...</div>';
    echo '</div>';
   /* echo '<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>';*/

    $this->endWidget();
?>
</div>
 <?php echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excel','criteria'=>$_GET['DigitalLogBookHistory'])));
?> &nbsp; &nbsp; &nbsp;

<script>
	function adjustMeter() {
		var adjust_date = $("#adjust_date").val();
		var vehicle_no = $("#vehicle_no").val();
		if(!vehicle_no && !adjust_date)
			alert('Adjust date from and Vehicle No can not empty.');
		else if(!adjust_date)
			alert('Adjust date from can not empty.');
		else if(!vehicle_no)
			alert('Vehicle No can not empty.');
		else {
			 $("#requisitionDetailModal").modal("show");
             $("#view-body").html("Procecing . . . .");
			$.post('<?php echo Yii::app()->createAbsoluteUrl("digitalLogBookHistory/meterAdjust");?>',{adjust_date:adjust_date,vehicle_no:vehicle_no},function(data) { 
				var v ='<div id="form-modal" class="modal-body"><div id="view-body">'+data+'</div></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>';              
				$("#form-modal").html(v);
            });
		}
	}
</script>