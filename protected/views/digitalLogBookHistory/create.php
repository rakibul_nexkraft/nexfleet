<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $model DigitalLogBookHistory */

$this->breadcrumbs=array(
	'Digital Log Book Histories'=>array('index'),
	'Create',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List DigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Manage DigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Create DigitalLogBookHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>