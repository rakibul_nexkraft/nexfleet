<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $model DigitalLogBookHistory */

$this->breadcrumbs=array(
	'Digital Log Book Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'List DigitalLogBookHistory', 'url'=>array('index')),
		array('label'=>'Create DigitalLogBookHistory', 'url'=>array('create')),
		array('label'=>'View DigitalLogBookHistory', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Manage DigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Update DigitalLogBookHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>