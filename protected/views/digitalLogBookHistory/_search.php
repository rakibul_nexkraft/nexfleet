<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $model DigitalLogBookHistory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_id'); ?>
		<?php echo $form->textField($model,'login_id',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trip_id'); ?>
		<?php echo $form->textField($model,'trip_id',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'requisition_id'); ?>
		<?php echo $form->textField($model,'requisition_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_end'); ?>
		<?php echo $form->textField($model,'login_end',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'trip_id_end'); ?>
		<?php echo $form->textField($model,'trip_id_end',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'duty_day'); ?>
		<?php echo $form->textField($model,'duty_day',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_pin'); ?>
		<?php echo $form->textField($model,'driver_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'department'); ?>
		<?php echo $form->textField($model,'department',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'duty_type'); ?>
		<?php echo $form->textField($model,'duty_type',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dutytype_id'); ?>
		<?php echo $form->textField($model,'dutytype_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_start_time_vts'); ?>
		<?php echo $form->textField($model,'driver_start_time_vts'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_start_time_mobile'); ?>
		<?php echo $form->textField($model,'driver_start_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_end_time_vts'); ?>
		<?php echo $form->textField($model,'driver_end_time_vts'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'driver_end_time_mobile'); ?>
		<?php echo $form->textField($model,'driver_end_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'meter_number'); ?>
		<?php echo $form->textField($model,'meter_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'location_vts'); ?>
		<?php echo $form->textField($model,'location_vts',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'location_mobile'); ?>
		<?php echo $form->textField($model,'location_mobile',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuel_recipt_no'); ?>
		<?php echo $form->textField($model,'fuel_recipt_no',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fuel_total_bill'); ?>
		<?php echo $form->textField($model,'fuel_total_bill',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'night_halt_bill'); ?>
		<?php echo $form->textField($model,'night_halt_bill'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'morning_ta'); ?>
		<?php echo $form->textField($model,'morning_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lunch_ta'); ?>
		<?php echo $form->textField($model,'lunch_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'night_ta'); ?>
		<?php echo $form->textField($model,'night_ta'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'running_per_km_rate'); ?>
		<?php echo $form->textField($model,'running_per_km_rate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'running_km'); ?>
		<?php echo $form->textField($model,'running_km'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'running_km_bill'); ?>
		<?php echo $form->textField($model,'running_km_bill'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'over_time_hour_rate'); ?>
		<?php echo $form->textField($model,'over_time_hour_rate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'over_time'); ?>
		<?php echo $form->textField($model,'over_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'over_time_bill'); ?>
		<?php echo $form->textField($model,'over_time_bill'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_bill'); ?>
		<?php echo $form->textField($model,'total_bill'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time_vts'); ?>
		<?php echo $form->textField($model,'created_time_vts'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_time_mobile'); ?>
		<?php echo $form->textField($model,'created_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time_vts'); ?>
		<?php echo $form->textField($model,'updated_time_vts'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_time_mobile'); ?>
		<?php echo $form->textField($model,'updated_time_mobile'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->