<?php
/* @var $this DigitalLogBookHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Digital Log Book Histories',
);
if(!Yii::app()->user->isViewUser()) {
	$this->menu=array(
		array('label'=>'Create DigitalLogBookHistory', 'url'=>array('create')),
		array('label'=>'Manage DigitalLogBookHistory', 'url'=>array('admin')),
	);
}
?>

<h1>Digital Log Book Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
