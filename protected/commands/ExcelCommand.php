<?php 
class ExcelCommand extends CConsoleCommand {
    public function actionIndex() {
      
        $request_query="SELECT * FROM tbl_large_excel_send";
        $request_data=Yii::app()->db->createCommand($request_query)->query();
        if($request_data){
            foreach ($request_data as $rkey => $rvalue) {
                $id= $rvalue['id'];
                $username=$rvalue['username'];
                $user_email=$rvalue['email'];
                $data=json_decode($rvalue['data_get']);
                //var_dump($data);
                $model_name=$rvalue['model_name'];
                $total_row=$rvalue['total_row'];
            
                $model = new $model_name;
           
            
                ini_set('memory_limit', '-1');
                Yii::import('application.extensions.PHPExcel1.Classes.PHPExcel');
                $objPHPExcel = new PHPExcel();
                for ($k=0; $k <ceil($total_row/2000); $k++) {              
                     $model->attributes =$data;           
                    $data_provider=$model->searchExcel($k,$data);          
        
                    $i=2;$fl=0;$page=$k;
                    foreach ($data_provider as $key => $value) {            
                        if($fl==0){            
                            $objPHPExcel->createSheet();
                            $objPHPExcel->setActiveSheetIndex($page)
                                            ->setCellValue('A1', 'Id')
                                            ->setCellValue('B1', 'Requisition')
                                            ->setCellValue('C1', 'Driver PIN')
                                            ->setCellValue('D1', 'Driver Name')
                                            ->setCellValue('E1', 'User PIN')
                                            ->setCellValue('F1', 'User Name')
                                            ->setCellValue('G1', 'User Level')
                                            ->setCellValue('H1', 'Department')
                                            ->setCellValue('I1', 'Vehicle Reg No')
                                            ->setCellValue('J1', 'Start Date')
                                            ->setCellValue('K1', 'End Date')
                                            ->setCellValue('L1', 'Start Meter')
                                            ->setCellValue('M1', 'End Meter')
                                            ->setCellValue('N1', 'Total Run')
                                            ->setCellValue('O1', 'Duty Type')
                                            ->setCellValue('P1', 'Bill Amount (BDT)');
                            
                            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);              
                            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);          
                            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);           
                            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20); 
                            $fl=1;              
                        }
                         $objPHPExcel->setActiveSheetIndex($page)
                            ->setCellValue('A'.$i, $value['id'])
                            ->setCellValue('B'.$i, $value['requisition_id'])
                            ->setCellValue('C'.$i, $value['driver_pin'])
                            ->setCellValue('D'.$i, $value['driver_name'])
                            ->setCellValue('E'.$i, $value['user_pin'])
                            ->setCellValue('F'.$i, $value['user_name'])
                            ->setCellValue('G'.$i, $value['user_level'])
                            ->setCellValue('H'.$i, $value['user_dept'])
                            ->setCellValue('I'.$i, $value['vehicle_reg_no'])
                            ->setCellValue('J'.$i, $value['start_date'])
                            ->setCellValue('K'.$i, $value['end_date'])
                            ->setCellValue('L'.$i, $value['start_meter'])
                            ->setCellValue('M'.$i, $value['end_meter'])
                            ->setCellValue('N'.$i, $value['total_run'])
                            ->setCellValue('O'.$i, $value['dutytypes']->type_name)
                            ->setCellValue('P'.$i, $value['bill_amount']);
                         $i++;
                

                    } 
                }    
                
                $date=$rvalue['created_time'];       
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $path_save='../var/www/html/ifleet/excel/'.$date.'_'.$username.'_iFleet_Log_Book_.xlsx';
           
                $objWriter->save($path_save);    
                $this->sendMailIcress($user_email,$username,$date);
                $request_query="DELETE FROM tbl_large_excel_send WHERE id='".$id."'";
                $request_data=Yii::app()->db->createCommand($request_query)->query();
                ini_set('memory_limit', '256MB');
            }
        }
        
    }
public function sendMailIcress($email,$username,$date)
    {
        try {
            $soapClient = new SoapClient("http://imail.brac.net:8080/isoap.comm.imail/EmailWS?wsdl");

            $job = new jobs;

            //$job->subject='Transport Requisition Notification';
            $job->jobContentType = 'html';
            $job->fromAddress = 'ifleet@brac.net';
            $job->udValue1 = 'iFleet';
            $job->requester = 'iFleet';

            //   $job->jobRecipients[0]=new jobRecipients;
            //   $job->jobRecipients[0]->recipientEmail="shouman.das@gmail.com";

            $job->jobRecipients[0] = new jobRecipients;
            //$job->jobRecipients[0]->recipientEmail = $model->email;
            //$job->jobRecipients[0]->recipientEmail = "ehsan@nexkraft.com";
            $job->jobRecipients[0]->recipientEmail = $email;


    
            //$model1 = $hrdata->getHrUser($model->pin);
            //$rec_mail = $model1[0]['Email'];

           
                $job->subject = 'Transport Excel Report Request Completed';
                $job->body = nl2br("Dear User \n\nYour excel report request is completed.\n\nPlease, download to click <a href='http://35.185.141.246/excel/".$date."_".$username."_iFleet_Log_Book_.xlsx'>here</a>.\n\nThanks\n\nTransport Team");
            $jobs = array('jobs' => $job);
            $send_email = $soapClient->__call('sendEmail', array($jobs));
            
            
        } catch (SoapFault $fault) {
            $error = 1;
            print($fault->faultcode . "-" . $fault->faultstring);
        }
    }

}
class jobs
{

    public $appUserId; // string
    public $attachments; // attachment
    public $bcc; // string
    public $body; // string
    public $caption; // string
    public $cc; // string
    public $complete; // boolean
    public $feedbackDate; // dateTime
    public $feedbackEmail; // string
    public $feedbackName; // string
    public $feedbackSent; // boolean
    public $fromAddress; // string
    public $fromText; // string
    public $gateway; // string
    public $jobContentType; // string
    public $jobId; // long
    public $jobRecipients; // jobRecipients
    public $mode; // string
    public $numberOfItem; // int
    public $numberOfItemFailed; // int
    public $numberOfItemSent; // int
    public $priority; // string
    public $requester; // string
    public $status; // string
    public $subject; // string
    public $toAddress; // string
    public $toText; // string
    public $udValue1; // string
    public $udValue2; // string
    public $udValue3; // string
    public $udValue4; // string
    public $udValue5; // string
    public $udValue6; // string
    public $udValue7; // string
    public $vtemplate; // string

}

class jobRecipients
{

    public $failCount; // int
    public $image; // base64Binary
    public $job; // jobs
    public $jobDetailId; // long
    public $recipientEmail; // string
    public $sent; // boolean
    public $sentDate; // dateTime
    public $toText; // string

}

?>