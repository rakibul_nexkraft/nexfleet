<?php 

date_default_timezone_set("Asia/Dhaka");
class UpdateDigitalLogBookCommand extends CConsoleCommand {
    public function actionIndex() {   

      $rows = Yii::app()->db->createCommand("SELECT * FROM tbl_digital_log_book_history WHERE (trip_id_end = '1' or day_end = 'Yes') and moved_to_dlb = 0 and failed_count <= 3")->queryAll();
         //var_dump($rows);exit();
        foreach ($rows as $row_key => $row) {
        // fetch vehicle info
        $reg = $this->vehicle($row['vehicle_reg_no']);
        // Path finder KM
        $st = explode(' ',date("d/m/Y H:i",strtotime($row['driver_start_time_mobile'])));
        $et = explode(' ',date("d/m/Y H:i",strtotime($row['driver_end_time_mobile'])));        
        $km = $this->pathFinderKM($reg['asset_id'],$st,$et);

        if($km == -1) {
          // update log book history for moved_to_dlb
          $updata_history = Yii::app()->db->createCommand("UPDATE tbl_digital_log_book_history set failed_count = '".($row["failed_count"] + 1)."' WHERE id='".$row["id"]."'")->execute();
          continue;
        }

       
        //$row['meter_number'] = $first_meter['meter_number'] + $km;
        $requisition_id = $row['requisition_id'];
        $user_pin = $row['user_pin'];
        $user_name = $row['user_name'];
        $Job_level = '';
        $user_cell = '0';
        $user_email = '';
        $user_address = '';
        try {
          if(!empty($user_pin)) {
            $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
            $staffInfo = CJSON::decode(file_get_contents($uri));
            $Job_level = $staffInfo[0]['JobLevel'];
            $user_cell = $staffInfo[0]['MobileNo'];
            $user_email = $staffInfo[0]['EmailID'];
            $user_address = $staffInfo[0]['EmailID'];
          }
        } catch (Exception $e) {
          $this->_sendResponse(500, sprintf('Unable to load user information from BRAC API!', $_GET['model']) );
          return;
          break;
        } 
        $user_department = $row['department'];
        $vehicle_reg_no = $row['vehicle_reg_no'];
        $vehicletype_id = $row['vehicle_type_id'];
        $driver_pin = $row['driver_pin'];
        $driver_name = $row['driver_name'];
        $dutytype_id = $row['dutytype_id'];
        $duty_day = $row['duty_day'];
        $offline = strpos($row['message_log'],"Offline");
        if(!empty($row['driver_start_time_mobile']) && $row['driver_start_time_mobile']!="0000-00-00 00:00:00") {
          $statr_DT = explode(" ",$row['driver_start_time_mobile']);
          $start_date = $statr_DT[0];
          if(!empty($statr_DT[1])){
            $start_time = $statr_DT[1];
          }
        }
        else {
          $statr_DT=explode(" ", $row['driver_start_time_vts']);
          $start_date = $statr_DT[0];
          if(!empty($statr_DT[1])){
            $start_time = $statr_DT[1];
          }
        }
        if(!empty($row['driver_end_time_mobile']) && $row['driver_end_time_mobile']!="0000-00-00 00:00:00"){
          $end_DT = explode(" ", $row['driver_end_time_mobile']);
          $end_date = $end_DT[0];
          if(!empty($end_DT[1])){
            $end_time = $end_DT[1];
          }
        }
        else{
          $end_DT=explode(" ", $row['driver_end_time_vts']);
          $end_date = $end_DT[0];
          if(!empty($end_DT[1])){
            $end_time = $end_DT[1];
          }
        }
        $start_time_array = explode(":",  $start_time);
        if(isset($start_time_array[2])) {
          $start_time_array[2] = "00";
          $start_time = implode(":", $start_time_array);
        }
        $end_time_array = explode(":",  $end_time);
        if(isset($end_time_array[2])) {
          $end_time_array[2] = "00";
          $end_time = implode(":", $end_time_array);
        }
        $a = new DateTime($start_time);
        $b = new DateTime($end_time);
        $interval = $a->diff($b);
        $total_time = $interval->format("%H:%I");
        $duty_day = $row['duty_day'];
        $end_point = empty($row['location_vts'])?$row['location_mobile']:$row['location_vts'];
        //$end_meter = $row['meter_number'];
        $trip_date = date('Y-m-d',strtotime($row['driver_start_time_mobile']));
        $sql = "SELECT * FROM tbl_digital_log_book_history Where trip_id ='".$row['trip_id']."' AND bill=0 AND DATE(driver_start_time_mobile)='".$trip_date."'order by created_time_mobile ASC";
        $sql_rs = Yii::app()->db->createCommand($sql)->queryAll();        
        $start_point = empty($sql_rs[0]['location_vts']) ? $sql_rs[0]['location_mobile'] : $sql_rs[0]['location_vts'];
        $start_meter = $sql_rs[0]['meter_number'];
        $row['meter_number'] = $end_meter = $start_meter + $km;
        $night_halt = 0;
        $nh_amount = 0;
        $morning_ta = 0;
        $lunch_ta = 0;
        $night_ta = 0;
        foreach ($sql_rs as $key => $value) {
          if($value['night_halt_bill']!=0) {
            $night_halt = 1;
            $nh_amount = $value['night_halt_bill'];
          }
          if($value['morning_ta']!=0) {
            $morning_ta = $value['morning_ta'];
          }
          if($value['lunch_ta']!=0) {
            $lunch_ta = $value['lunch_ta'];
          }
          if($value['night_ta']!=0) {
            $night_ta = $value['night_ta'];
          }
          $id_history = $value['id'];
          $sql_update_history = "UPDATE tbl_digital_log_book_history SET bill=1  Where  id='$id_history'";
          $sql_update_history_rs = Yii::app()->db->createCommand($sql_update_history)->execute();
         }
        $sup_mile_amount = 0;
        $total_run = $end_meter-$start_meter;
        $rph = $row['running_per_km_rate'];
        $sql_dutytypes = "SELECT * FROM tbl_dutytypes WHERE id=".$dutytype_id;
        $sql_dutytypes_rs = Yii::app()->db->createCommand($sql_dutytypes)->queryRow();
        $sql_vehicletypes = "SELECT * FROM tbl_vehicletypes WHERE id=".$vehicletype_id;
        $sql_vehicletypes_rs = Yii::app()->db->createCommand($sql_vehicletypes)->queryRow();
        if($dutytype_id==1) {
          $bill_amount = $sql_vehicletypes_rs['rate_per_km'] * $total_run;
          $bill_amount = $bill_amount + $bill_amount * $sql_dutytypes_rs['service_charge'];
        }
        else if($dutytype_id==2) {
          $bill_amount = $sql_vehicletypes_rs['rate_per_km_personal'] * $total_run;
          $bill_amount = $bill_amount + $bill_amount * $sql_dutytypes_rs['service_charge'];
        }
        else if($dutytype_id==3) {
          $hour = explode(":", $total_time);
          $hour_bill = $hour[0]*$sql_vehicletypes_rs['rph_visitor'];
          if($sql_vehicletypes_rs['minimum_rate']>$hour_bill) {
            $bill_amount = $sql_vehicletypes_rs['rate_per_km_external'] * $total_run;
            $bill_amount = $sql_vehicletypes_rs['minimum_rate'] + $bill_amount * $sql_dutytypes_rs['service_charge'];
          }
          else{
            $bill_amount = $sql_vehicletypes_rs['rate_per_km_external']*$total_run;
            $bill_amount = $hour_bill + $bill_amount * $sql_dutytypes_rs['service_charge'];
          }
        }
        else if($dutytype_id==4) {
          $bill_amount = $sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
          $bill_amount = $bill_amount + $bill_amount * $sql_dutytypes_rs['service_charge'];
        }
        else if($dutytype_id==5) {
          $bill_amount = $sql_vehicletypes_rs['rate_per_km_sister']*$total_run;
          $bill_amount = $bill_amount + $bill_amount * $sql_dutytypes_rs['service_charge'];
        }
        else {
          $bill_amount = $rph*$total_run;
        }

        $over_hour_minute = explode(":", $row['over_time']);
        if(isset($over_hour_minute[1])) {
          $total_ot_hour = $over_hour_minute[0].".".$over_hour_minute[1];
        }
        else {
          $total_ot_hour = $row['over_time'];
        }
        $start_time_hour_minute = explode(":", $start_time);
        if(isset($start_time_hour_minute[1])) {
          $start_time = $start_time_hour_minute[0].".".$start_time_hour_minute[1];
        }
        else {
          $start_time = $start_time;
        }
        $end_time_hour_minute = explode(":", $end_time);
        if(isset($end_time_hour_minute[1])) {
          $end_time = $end_time_hour_minute[0].".".$end_time_hour_minute[1];
        }
        else {
          $end_time = $end_time;
        }
        $total_ot_amount = $row['over_time_bill'];
        $created_by = $row['created_by'];
        $created_time = date("Y-m-d h:i:s");
        $trip_id = $row['trip_id'];
        if($duty_day=='Official') {
           $duty_day = "Office Day";
        }
       /* $sql_rs = Yii::app()
          ->db
          ->createCommand()
          ->insert(
            'tbl_digital_log_book',
            array(
            'id'=>'',
            'requisition_id'=>$requisition_id,
            'user_pin'=>$user_pin,
            'user_name'=>$user_name,
            'user_level'=>$Job_level,
            'user_dept'=>$user_department,
            'email'=>$user_email,
            'user_cell'=>$user_cell,
            'user_address'=>'',
            'vehicle_reg_no'=>$vehicle_reg_no,
            'vehicletype_id'=>$vehicletype_id,
            'vehicle_location'=>$start_point,
            'driver_pin'=>$driver_pin,
            'driver_name'=>$driver_name,
            'helper_pin'=>'',
            'helper_name'=>'',
            'helper_ot'=>'',
            'dutytype_id'=>$dutytype_id,
            'dutyday'=>$duty_day,
            'start_date'=>$start_date,
            'end_date'=>$end_date,
            'start_time'=>$start_time,
            'end_time'=>$end_time,
            'total_time'=>$total_time,
            'start_point'=>$start_point,
            'end_point'=>$end_point,
            'start_meter'=>$start_meter,
            'end_meter'=>$end_meter,
            'total_run'=>$total_run,
            'rph'=>$rph,
            'night_halt'=>$night_halt,
            'nh_amount'=>$nh_amount,
            'morning_ta'=>$morning_ta,
            'lunch_ta'=>$lunch_ta,
            'night_ta'=>$night_ta,
            'sup_mile_amount'=>'',
            'bill_amount'=>$bill_amount,
            'total_ot_hour'=>$total_ot_hour,
            'total_ot_amount'=>$total_ot_amount,
            'created_by'=>$created_by,
            'created_time'=>$created_time,
            'trip_id'=>$trip_id
          ));*/
       /* if($sql_rs) {
           // update log book history for moved_to_dlb
          $updata_history = Yii::app()->db->createCommand("UPDATE tbl_digital_log_book_history set moved_to_dlb=1 WHERE id='".$row["id"]."'")->execute();
        } */       
      }
    
    }
    private function vehicle($reg) {
      $sql = Yii::app()->db->createCommand("SELECT * FROM tbl_vehicles WHERE reg_no='".$reg."'")->queryRow();
      return $sql;
    }

    private function pathFinderKM($asset_id,$st,$et) {     
      try {
        $KM = file_get_contents('https://api.finder-lbs.com/api/54f2cb4b421aa9493557fc09/getdistancebyasset?email=bracblc123@gmail.com&asset_id='.$asset_id.'='.$st['0'].'%20'.$st['1'].'&tt='.$et['0'].'%20'.$et['1']);
        $KM = json_decode($KM);

        return $km = $KM->success ? $KM->output[0]->distance_in_kms : 0;
      } catch(Exception $e) {
      } 
      return -1;     
    }
}
?>