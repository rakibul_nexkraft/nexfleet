<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class CustomController extends Controller
{
    public $layout='//layouts/main_workshop';

	public function beforeAction() {
        // if(!isset(Yii::app()->user->__name)) return true;
        /*if (Yii::app()->user->__name == 'myDesk' && $action->controller->id != 'vehiclestatus') {
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->baseUrl . '/index.php/user/login/client');
        }*/
        $cs=Yii::app()->clientScript;
        $cs->scriptMap=array(
            'jquery.js' => false, 
            'bootstrap.css' => false,
            'bootstrap-yii.css' => false,
            'bootstrap-responsive.css'=>false,
            'jquery-ui-bootstrap.css' => false,
            'styles.css' => false,
            'jquery.ba-bbq.js' => false,
            'bootstrap.bootbox.min.js' => false,
            'bootstrap.js' => false,
            'jquery-ui.min.js' => false,
            // 'jquery.yiigridview.js' => false,
            'jquery-ui-bootstrap.css' => false,
            'jquery.yiiactiveform.js'=>false
    //            'jquery-1.12.4.js'=>Yii::app()->theme->baseUrl.'/js/jquery-1.12.4.js',
        );
        return true;
    }
}