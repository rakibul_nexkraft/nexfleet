<?php

class WorkshopAdminController extends CustomController
{
	public $defaultAction = 'admin';
	
	private $_model;

	public function filters()
	{
		return CMap::mergeArray(parent::filters(),array(
			'accessControl',
		));
	}

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('admin','delete','create','update','view','userCreate','userView','userUpdate','userDelete'),
				'users'=>UserModule::getAdmins(),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	public function actionAdmin()
	{
		$model=new User('search');
        $model->unsetAttributes();  
        if(isset($_GET['User']))
            $model->attributes=$_GET['User'];

        $this->render('/admin/index_for_workshop',array(
            'model'=>$model,
        ));
	}

	public function actionView()
	{
		$model = $this->loadModel();
		$this->render('/admin/view',array(
			'model'=>$model,
		));
	}

	public function actionUserView()
	{
		if($_GET['type']==3) { $model = $this->loadModel(); } 
		if($_GET['type']==1) { $model = $this->actionUserUpdate(); }
		if($_GET['type']==2) { $model = $this->loadModel(); }
		$this->renderpartial('/admin/modal_view',array(
			'model'=>$model,'type'=>$_GET['type'],
		));
		
	 }

	public function actionUserCreate()
	{
		$model=new User;
		$profile=new Profile;
		$this->performAjaxValidation(array($model,$profile));
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
			$profile->attributes=$_POST['Profile'];
			$profile->user_id=0;
			if($model->validate()&&$profile->validate()) {
				$model->password=Yii::app()->controller->module->encrypting($model->password);
				$model->lastvisit_at= new CDbExpression('NOW()');
				if($model->save()) {
					$profile->user_id=$model->id;
					$profile->save();
				}
				$this->redirect(array('view','id'=>$model->id));
			} else $profile->validate();
		}

		$this->renderpartial('/admin/_User_form',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}
	public function actionCreate()
	{
		$model=new User;
		$profile=new Profile;
		$this->performAjaxValidation(array($model,$profile));
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
			$profile->attributes=$_POST['Profile'];
			$profile->user_id=0;
			if($model->validate()&&$profile->validate()) {
				$model->password=Yii::app()->controller->module->encrypting($model->password);
				$model->lastvisit_at= new CDbExpression('NOW()');
				if($model->save()) {
					$profile->user_id=$model->id;
					$profile->save();
				}
				$this->redirect(array('view','id'=>$model->id));
			} else $profile->validate();
		}

		$this->render('/admin/create',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	public function actionUserUpdate()
	{
		$model=$this->loadModel();
		$profile=$model->profile;
		$this->performAjaxValidation(array($model,$profile));
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$profile->attributes=$_POST['Profile'];
			
			if($model->validate()&&$profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password!=$model->password) {
					$model->password=Yii::app()->controller->module->encrypting($model->password);
					$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
				}
				$model->save();
				$profile->save();
				$this->redirect(array('view','id'=>$model->id));
			} else $profile->validate();
		}
		return array('model'=>$model,
			'profile'=>$profile);
	}
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$profile=$model->profile;
		$this->performAjaxValidation(array($model,$profile));
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$profile->attributes=$_POST['Profile'];
			
			if($model->validate()&&$profile->validate()) {
				$old_password = User::model()->notsafe()->findByPk($model->id);
				if ($old_password->password!=$model->password) {
					$model->password=Yii::app()->controller->module->encrypting($model->password);
					$model->activkey=Yii::app()->controller->module->encrypting(microtime().$model->password);
				}
				$model->save();
				$profile->save();
				$this->redirect(array('view','id'=>$model->id));
			} else $profile->validate();
		}
        
		$this->render('/admin/update',array(
			'model'=>$model,
			'profile'=>$profile,
		));
	}

	public function actionUserDelete()
	{
			$model = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			$profile->delete();
			$model->delete();
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		
	}

	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			$model = $this->loadModel();
			$profile = Profile::model()->findByPk($model->id);
			$profile->delete();
			$model->delete();
			if(!isset($_POST['ajax']))
				$this->redirect(array('/user/admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

    protected function performAjaxValidation($validate)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
        {
            echo CActiveForm::validate($validate);
            Yii::app()->end();
        }
    }

	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=User::model()->notsafe()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}
	
}