<?php

class LoginController extends Controller
{
	public $defaultAction = 'login';

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{ 
		/*if (Yii::app()->user->isGuest) {
			$model=new UserLogin;
			// collect user input data
			if(isset($_POST['UserLogin']))
			{
				$model->attributes=$_POST['UserLogin'];
				// validate user input and redirect to previous page if valid
				if($model->validate()) {
					$this->lastViset();
					if (Yii::app()->user->returnUrl=='/index.php')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
			}
			// display the login form
			$this->render('/user/login',array('model'=>$model));
		} else
			$this->redirect(Yii::app()->controller->module->returnUrl);*/
		if(isset($_GET['pin']) && isset($_GET['fleet']) && !empty($_GET['pin']) && !empty($_GET['fleet'])) {
			$model=new UserLogin;
			$model->username = $_GET['pin'];
			$model->password = $_GET['fleet'];				
			if($model->validate()) {					
					$this->lastViset();
					if (Yii::app()->user->returnUrl=='/index.php')
						$this->redirect(Yii::app()->controller->module->returnUrl);
					else
						$this->redirect(Yii::app()->user->returnUrl);
				}
		}
		else {
		$this->redirect("http://localhost/nexfleet/pickndrop/index.php/user/login");
		}
	}
	
	public function actionClient(){
		$cmodel = new ClientLogin();
		if (isset($_POST['ClientLogin'])){
			$cmodel->attributes = $_POST['ClientLogin'];
			//if(!$cmodel->validate()){
				//return false;
                //if (empty($cmodel->username)) $cmodel->addError('username', 'Username cannot be empty');
                //if (empty($cmodel->email)) $cmodel->addError('email', 'Email cannot be empty');
            //}
			if(trim($_POST['ClientLogin']['username']) == '') {
                $cmodel->addError('username', 'Username cannot be empty');
            }
            if(trim($_POST['ClientLogin']['username']) == '') {
                $cmodel->addError('email', 'Email cannot be empty');
            }
            $uri = "http://api.brac.net/v1/staffs/".$cmodel->username."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProgramName,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
            $staffInfo = rtrim(file_get_contents($uri), "\0");
			$staffInfo = json_decode($staffInfo, true);
			$staffInfo = $staffInfo[0];
			
            //var_dump($staffInfo["StaffName"]);die;
            //var_dump($staffInfo);die;
			if ($cmodel->email == $staffInfo['EmailID']){
                $model=new UserLogin;
				$model->attributes = $cmodel->attributes;
                $model->username="myDesk";
                $model->password="654@321";

                $first_name=substr($staffInfo['StaffName'],0,strpos($staffInfo['StaffName'],' '));
                $last_name=substr($staffInfo['StaffName'],strpos($staffInfo['StaffName'],' '),-1);

                if($model->validate()) {
                    Yii::app()->user->username=$_POST['ClientLogin']['username'];
                    Yii::app()->user->email=$staffInfo['EmailID'];
                    Yii::app()->user->lastvisit_at=date('Y-m-d H:i:s');
                    Yii::app()->user->firstname= $first_name;
                    Yii::app()->user->lastname= $last_name;
                    Yii::app()->user->level= (int)$staffInfo['JobLevel'];
                    Yii::app()->user->department=$staffInfo['ProgramName'];
                    Yii::app()->user->designation=$staffInfo['DesignationName'];
                    Yii::app()->user->phone=$staffInfo['MobileNo'];
                    Yii::app()->user->address=$staffInfo['PresentAddress'];
                    Yii::app()->user->myDeskUser=1;

                    $this->redirect(Yii::app()->baseUrl.'/index.php/vehiclestatus/index');

                }
            } else {
                $cmodel->addError('email', 'Username and Email does not match');
            }
        }
		$this->render('/user/client-login', array(
	        'model'=>$cmodel
        ));
	}
	
	private function lastViset() {
		$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
		$lastVisit->lastvisit = time();
		$lastVisit->save();
	}
	
	

}