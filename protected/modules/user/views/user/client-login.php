<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
	UserModule::t("Login"),
);
?>
<div class="login_container">
<!--<h2><?php echo UserModule::t("Login"); ?></h2>-->

<div class="login_container_bottom">

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

<div class="success">
	<?php echo Yii::app()->user->getFlash('loginMessage'); ?>
</div>

<?php endif; ?>

<!--<p><?php echo UserModule::t("Please fill out the following form with your login credentials:"); ?></p>-->

<div class="form">
<?php echo CHtml::beginForm(); ?>



	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'username'); ?>
		<?php echo CHtml::activeTextField($model,'username', array('required' => 'required')) ?>
	</div>

	<div class="row">
		<?php echo CHtml::activeLabelEx($model,'email'); ?>
		<?php echo CHtml::activeTextField($model,'email', array('required' => 'required')) ?>
	</div>

    <div id="clientPass"></div>
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<div class="row submit">
		<?php //echo CHtml::submitButton(UserModule::t("Login")); ?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Login',
		'type' => 'primary',
		'buttonType'=>'submit',
		'size' => 'medium'
		));
		?>
	</div>



<?php echo CHtml::endForm(); ?>
    <hr>
    <div class="" style="width: 230px;margin: auto;">
        <div class="">
            <span for="">Are you an iFleet user? <a class="link" href="<?php echo Yii::app()->createUrl('/user/login') ?>">Click Here</a></span>
        </div>
    </div>
</div><!-- form -->
</div>
</div>
<!--< ?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>-->