<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}

	.well form{
		margin-bottom: unset;
	}

	#user-form textarea{
		border: 1px solid #5a5a5a2e;
	}
</style>

<script>
	function placeOrder () {
    document.theForm.submit()
}
</script>
<?php if(($type == 2) || ($type ==1)){?>
	<div class="modal-dialog" style="width: 715px; margin: 65px auto;">
	<?php }else{?>
		<div class="modal-dialog" style="margin: 206px 500px;">
	<?php }?>
<div class="modal-dialog">
	<?php if(($type == 2) || ($type ==1)){?>
	<div class="modal-content" style="width: 850px;">
	<?php }else{?>
		<div class="modal-content">
	<?php }?>
		<div class="modal-header modal-update">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3 class="modal-title"><?php if($type==1) {$profile = $model['profile'];$model = $model['model']; echo  UserModule::t('Update User #')."".$model->username; }
			if($type==2) {echo  UserModule::t('User #')."".$model->username;} if($type==3){echo  UserModule::t('Delete #')."".$model->username;} ?></h3>
		</div>
		<div class="modal-body">
			<div class="well" style="padding: unset; margin-bottom: 0px;background-color: unset;
    border: unset;box-shadow: none;">
    <?php if($type==3) {
					?>
				  <div class="modal-body" style="text-align: center;font-size: x-large;">
				    Are you sure you want to delete this record?
				  </div>

				<?php } if($type==1) {
					?>
					<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'user-form',
		'enableAjaxValidation'=>true,
		'htmlOptions' => array('enctype'=>'multipart/form-data', 'style'=>'padding:5px;', 'class'=> 'table'),
	));
	?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'username',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
            <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
				<?php echo $form->textField($model,'username',array('size'=>40,'maxlength'=>20,'class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model,'username'); ?>

		<?php echo $form->labelEx($model,'password',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
            <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
				<?php echo $form->passwordField($model,'password',array('size'=>40,'maxlength'=>128,'class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model, 'role_id',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
            <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
            	<?php
		$role_name = Role::model()->findAll(array('select'=>'id, role_name','order' => 'id ASC'));
		echo $form->dropDownList($model, 'role_id', CHtml::listData($role_name, 'id', 'role_name'),array('class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model, 'role_id'); ?>

		<?php echo $form->labelEx($model,'email',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
            <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
            	<?php echo $form->textField($model,'email',array('size'=>40,'maxlength'=>128,'class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group row">
		<?php echo $form->labelEx($model,'superuser',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
            <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
            	<?php echo $form->dropDownList($model,'superuser',User::itemAlias('AdminStatus'),array('class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model,'superuser'); ?>

		<?php echo $form->labelEx($model,'status',array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
            <div class="col-sm-4" style="margin: -6px 0 5px -13px;">
            	<?php echo $form->dropDownList($model,'status',User::itemAlias('UserStatus'),array('class'=>'form-control form-control-sm')); ?>
			</div>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<?php 
	$profileFields=$profile->getFields();
	if ($profileFields) { $i=1;
		foreach($profileFields as $field) {
			if ($i%2!=0) {
			
			?>
			<div class="form-group row">
				<?php } 
				echo $form->labelEx($profile,$field->varname,array('class'=>'col-sm-2 col-form-label col-form-label-sm', 'style'=> 'padding: 0px 0 20px 30px;'
            )); ?>
				<?php 
				if ($widgetEdit = $field->widgetEdit($profile)) {
					echo $widgetEdit;
				} elseif ($field->range) {
					echo '<div class="col-sm-4" style="margin: -6px 0 5px -13px;">';
					echo $form->dropDownList($profile,$field->varname,Profile::range($field->range),array('class'=>'form-control form-control-sm'));
					echo '</div>';
				} elseif ($field->field_type=="TEXT") {
					echo '<div class="col-sm-4" style="margin: -6px 0 5px -13px;">';
					echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>2, 'cols'=>40,'class'=>'form-control form-control-sm'));
					echo '</div>';
				} else {
					echo '<div class="col-sm-4" style="margin: -6px 0 5px -13px;">';
					echo $form->textField($profile,$field->varname,array('size'=>40,'maxlength'=>(($field->field_size)?$field->field_size:255),'placeholder'=>'Enter Your '.$field->title,'class'=>'form-control form-control-sm'));
					echo '</div>';
				}
				?>
				<?php echo $form->error($profile,$field->varname);
				if ($i%2==0 || count($profileFields)==$i) { ?>
			</div>
			<?php }
			$i++;
		}
	}
	?>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
						
						
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',$htmlOptions=array('class' => 'btn btn-secondary button-pink button-text-color',)); ?>
						
						</div>
					</div><!-- form -->

					<?php $this->endWidget(); ?>
				<?php }
				
				if($type==2) {
					$attributes = array(
						'id',
						'username',
						array(
							'name'=>'role_id',
							'type'=>'raw',
							'value'=>$model->roleName($model->role_id))
					);

					$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
					if ($profileFields) {
						foreach($profileFields as $field) {
							array_push($attributes,array(
								'label' => UserModule::t($field->title),
								'name' => $field->varname,
								'type'=>'raw',
								'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
							));
						}
					}

					array_push($attributes,
		//'password',
						'email',
		//'activkey',
						'create_at',
						'lastvisit_at',
						array(
							'name' => 'superuser',
							'value' => User::itemAlias("AdminStatus",$model->superuser),
						),
						array(
							'name' => 'status',
							'value' => User::itemAlias("UserStatus",$model->status),
						)
					);

					$this->widget('zii.widgets.XDetailView', array(
						'data'=>$model,
						'htmlOptions' => array('class' => 'table table_custom cart table_um all-header-workshop'),
						'ItemColumns' => 2,
						'attributes'=>$attributes,

					));
				}

				?>

			</div>
		</div>

		<?php if($type==1) {
			?>
			<!-- <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
			
			
							<?php #echo CHtml::button('Submit',array('onclick'=>'placeOrder()'));?>
							<?php #echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
							<?php #$this->endWidget(); ?>
			
						</div> -->
						
		<?php }if ($type==2){?>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
			</div>			
		<?php }if ($type==3){ ?>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">No</button>
				<?php echo CHtml::link('Yes',array('userDelete','id'=>$model->id),array(
                'class'=>'btn btn-secondary button-pink',
                'style'=>'color:white !important;'
            )); ?>
				<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="()">Yes</button> -->
			</div>
		<?php }?>
	</div>
</div>