<style>
	.detail-view{
		width: 100% !important;
		margin-bottom: unset;
	}
</style>

<?php
$this->breadcrumbs=array(
	UserModule::t('Users')=>array('admin'),
	$model->username,
);

$this->menu=array(
	array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
	array('label'=>UserModule::t('Update User'), 'url'=>array('update','id'=>$model->id)),
	array('label'=>UserModule::t('Delete User'), 'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>UserModule::t('Are you sure to delete this item?'))),
	array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
	array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
	array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
);
?>

<div class="col_full page_header_div">
        <h3 class="heading-custom page_header_h4"><?php echo UserModule::t('User #').''.$model->username.''; ?></h3>
    </div>
<?php

$attributes = array(
	'id',
	'username',
	array(
		'name'=>'role_id',
		'type'=>'raw',
		'value'=>$model->roleName($model->role_id))
);

$profileFields=ProfileField::model()->forOwner()->sort()->findAll();
if ($profileFields) {
	foreach($profileFields as $field) {
		array_push($attributes,array(
			'label' => UserModule::t($field->title),
			'name' => $field->varname,
			'type'=>'raw',
			'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
		));
	}
}

array_push($attributes,
		//'password',
	'email',
		//'activkey',
	'create_at',
	'lastvisit_at',
	array(
		'name' => 'superuser',
		'value' => User::itemAlias("AdminStatus",$model->superuser),
	),
	array(
		'name' => 'status',
		'value' => User::itemAlias("UserStatus",$model->status),
	)
);

$this->widget('zii.widgets.XDetailView', array(
	'data'=>$model,
	'htmlOptions' => array('class' => 'table cart table_um all-header-workshop'),
	'ItemColumns' => 2,
	'attributes'=>$attributes,
));


?>
