<?php
date_default_timezone_set("Asia/Dhaka");
class DefectRequestController extends Controller
{
	/*public function actionIndex()
	{
		$this->render('index');
	}*/
	public function actionCreate() {
		$model = new DefectRequest;				
		foreach($_POST['DefectRequest'] as $var=>$value) {
        // Does the model have this attribute? If not raise an error
	        if($model->hasAttribute($var)){
	            $model->$var = $value;
	        }
	        else{
	        	Yii::app()->printMessage->getPrint(500,0,"",$var." Parameter is not allowed");
	        	return;
	        }            
        }
        if($_POST['DefectRequest']['is_sister']!=1) {
	        $vehicle = Yii::app()->db->createCommand("SELECT end_meter  FROM tbl_movements where vehicle_reg_no = '".$model->reg_no."' order by id desc limit 1")->queryRow();  
	        $model->meter = $vehicle['end_meter'];
    	}
    	else {
    		if(empty($_POST['DefectRequest']['sister_concern'])) {
    			Yii::app()->printMessage->getPrint(500,0,"","Sister concern cannot be empty");
	        	return;
    		}
    	}
        $model->created_by = $model->driver_pin; 
        $model->created_time = date("Y-m-d H:i:s"); 
        if($model->save()) {        	
        	Yii::app()->printMessage->getPrint(200,1,array(),"");
        	return;
        }		
		$error = Yii::app()->printMessage->getModelError($model->errors);
		Yii::app()->printMessage->getPrint(500,0,"",$error);		
		return;
	}

	public function actionUpdate($id) {
		$model = DefectRequest::model()->findByPk($id);
		$old_model = DefectRequest::model()->findByPk($id);
		$json = file_get_contents('php://input');	

		$_POST = CJSON::decode($json,true);
		/*$p['DefectRequest']=array('admin_pin'=>14857,'admin_status'=>1,'workshop_pin'=>'','workshop_status'=>'');*/		
		if(empty($model)) {
			Yii::app()->printMessage->getPrint(500,0,"","ID not found");		
			return;
		}
		if(!empty($_POST['DefectRequest']['admin_pin']) && ($_POST['DefectRequest']['admin_status']==1 || $_POST['DefectRequest']['admin_status']==-1)) {
			if($_POST['DefectRequest']['admin_status']==-1 && empty($_POST['DefectRequest']['reason'])){
				Yii::app()->printMessage->getPrint(500,0,"","Reason cannot be empty.");	
				return;
			}
			$model->admin_pin = $_POST['DefectRequest']['admin_pin'];
			$model->admin_status = $_POST['DefectRequest']['admin_status'];
			$model->reason = $_POST['DefectRequest']['reason'];
			$model->admin_time = date("Y-m-d H:i:s");			  
	        if($model->save()) {        	
	        	Yii::app()->printMessage->getPrint(200,1,array(),"");
	        	return;
	        }		
			$error = Yii::app()->printMessage->getModelError($model->errors);
			Yii::app()->printMessage->getPrint(500,0,"",$error);		
			return;
		}
		elseif(!empty($_POST['DefectRequest']['workshop_pin']) && ($_POST['DefectRequest']['workshop_status']==1 || $_POST['DefectRequest']['workshop_status']==-1)) {
			if($_POST['DefectRequest']['workshop_status']==-1 && empty($_POST['DefectRequest']['reason'])){
				Yii::app()->printMessage->getPrint(500,0,"","Reason cannot be empty.");	
				return;
			}
			$driver = Yii::app()->db->createCommand("SELECT * FROM tbl_drivers where pin = '".$model->driver_pin."'")->queryRow();
			if(empty($driver)) {
				Yii::app()->printMessage->getPrint(500,0,"","Driver information not found");		
				return;
			} 
			$vehicle = Yii::app()->db->createCommand("SELECT * FROM tbl_vehicles where reg_no = '".$model->reg_no."'")->queryRow();	
			if(empty($vehicle)) {
				Yii::app()->printMessage->getPrint(500,0,"","Vehicle information not found");		
				return;
			}				 
			$model->workshop_pin = $_POST['DefectRequest']['workshop_pin'];
			$model->workshop_status = $_POST['DefectRequest']['workshop_status'];
			$model->reason = $_POST['DefectRequest']['reason'];
			$model->workshop_time = date("Y-m-d H:i:s");			  
	        if($model->save()) { 
	        	if($model->workshop_status==1) {
		        	$model_defect = New Defects;
		        	$model_defect->vehicletype_id = $vehicle ['vehicletype_id'];
		        	$model_defect->vehicle_reg_no  = $model->reg_no;
		        	$model_defect->driver_name = $driver['name'];
		        	$model_defect->last_meter = $model->meter;
		        	$model_defect->driver_pin = $model->driver_pin;
		        	$model_defect->total_amount = 0;
		        	$model_defect->defect_description = $model->category." ".$model->sub_category;
		        	$model_defect->apply_date = date("Y-m-d", strtotime($model->apply_time));
		        	$model_defect->created_time = $model->workshop_time;
		        	$model_defect->created_by = $model->workshop_pin;
		        	if($model_defect->save()) {	        	
			        	Yii::app()->printMessage->getPrint(200,1,array(),"");
			        	return;
		        	}	        	    	
					$old_model->save();
		        	$error = Yii::app()->printMessage->getModelError($model_defect->errors);
					Yii::app()->printMessage->getPrint(500,0,"",$error);		
					return;
				}
				Yii::app()->printMessage->getPrint(200,1,array(),"");
			    return;
	        }		
			$error = Yii::app()->printMessage->getModelError($model->errors);
			Yii::app()->printMessage->getPrint(500,0,"",$error);		
			return;
		}
		else {
			Yii::app()->printMessage->getPrint(500,0,"","Parameter is not valid");	
			return;
		}		
	}
	
	public function actionView($id) {
		//$model = DefectRequest::model()->findByPk($id);
		$model = Yii::app()->db->createCommand("SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE r.id='".$id."'")->queryRow();
		if(empty($model)) {
			Yii::app()->printMessage->getPrint(500,0,"","ID not found");		
			return;
		}
		Yii::app()->printMessage->getPrint(200,1,array($model),"");
        	return;
	}

	public function actionList() {
		
	}

	public function actionAllList($v,$p) {
		/**
		*
		* $v = 1(admin pending list), 2(admin approved list), 3(workshop pending  list), 4(workshop  approved list), 5(admin Reject list), 5(workshop Reject list), 7(All list)
		* $p = page number
		*
		**/
		if($v==1) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name, r.reason FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE r.admin_status=0 ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		elseif($v==2) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name, r.reason FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE r.admin_status=1 ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		elseif($v==3) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name, r.reason FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE r.admin_status=1 AND	r.workshop_status=0 ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		elseif($v==4) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name, r.reason FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE 	r.admin_status=1 AND r.workshop_status=1 ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		elseif($v==5) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name, r.reason FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE 	r.admin_status=-1 ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		elseif($v==6) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name, r.reason FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin WHERE 	r.admin_status=1 AND r.workshop_status=-1 ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		elseif($v==7) {
			$sql = "SELECT r.id, r.driver_pin, r.category, r.sub_category, r.reg_no, r.meter, r.apply_time,r.admin_pin,r.workshop_pin,r.admin_status,r.workshop_status,d.phone,d.name FROM tbl_defect_request as r INNER JOIN tbl_drivers as d ON r.driver_pin=d.pin ORDER BY r.id DESC LIMIT ".(($p-1)*10).", 10 ";
		}
		else {}
		$sql = Yii::app()->db->createCommand($sql)->queryAll();
	    if(empty($sql)) {
	    	$sql =array();
	    }
		Yii::app()->printMessage->getPrint(200,1,$sql,"");
        	return;

	}
	

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}