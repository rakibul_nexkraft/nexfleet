<?php

class UserController extends Controller
{
	/*public function actionIndex()
	{
		$this->render('index');
	}*/
	public function actionCreate() {
		$model = new User;	
		if(!isset($_POST['User'])) {
			Yii::app()->printMessage->getPrint(500,0,"","Parameter is not valid");
	        	return;
		}
		$user = Yii::app()->db->createCommand("SELECT * FROM tbl_user WHERE mobile_no='".$_POST['User']['mobile_no']."'")->queryRow();
		if($user) {
			Yii::app()->printMessage->getPrint(500,0,"","User Already Exist!");
	        	return;
		}	
		foreach($_POST['User'] as $var=>$value) {
        // Does the model have this attribute? If not raise an error
	        if($model->hasAttribute($var)){
	            $model->$var = $value;
	        }
	        else{
	        	Yii::app()->printMessage->getPrint(500,0,"",$var." Parameter is not allowed");
	        	return;
	        }
            
        }
        $model->created_by = $model->mobile_no; 
        $model->created_time = date("Y-m-d H:i:s"); 
        if($model->save()) {        	
        	Yii::app()->printMessage->getPrint(200,1,array(),"");
        	return;
        }		
		$error = Yii::app()->printMessage->getModelError($model->errors);
		Yii::app()->printMessage->getPrint(500,0,"",$error);		
		return;
	}
	public function actionImage() {
		
		if(!isset($_POST['mobile_no'])) {
			Yii::app()->printMessage->getPrint(500,0,"","Parameter is not valid");
	        	return;
		}
		$user = Yii::app()->db->createCommand("SELECT * FROM tbl_user WHERE mobile_no='".$_POST['mobile_no']."'")->queryRow();
		if(!$user) {
			Yii::app()->printMessage->getPrint(500,0,"","User Not Exist!");
	        	return;
		}
		if(!isset($_FILES['pro_image']['name'])) {              
            Yii::app()->printMessage->getPrint(500,0,"","Image not found!");
	        return;
       	}
        else {
            if(empty($_FILES["pro_image"]["tmp_name"])) { 
            	Yii::app()->printMessage->getPrint(500,0,"","Image empty found!");
	        	return;
            }
            
            $target_file =Yii::getPathOfAlias('webroot')."/images/profile/".basename($_FILES["pro_image"]["name"]);
            $profile = move_uploaded_file($_FILES["pro_image"]["tmp_name"], $target_file);
            if(!empty($profile)) {
            	$uploaded_image = Yii::app()->db->createCommand("UPDATE tbl_user SET image_path ='/images/profile/".$_FILES["pro_image"]["name"]."' WHERE mobile_no='".$_POST["mobile_no"]."'")->execute();	
            	if($uploaded_image==1 || $user['image_path']=="/images/profile/".$_FILES["pro_image"]["name"]) {
            		Yii::app()->printMessage->getPrint(200,1,array(),"");
        			return;
            	}
                Yii::app()->printMessage->getPrint(500,0,"","Image Cannot Save!");
	        	return; 
            }
            Yii::app()->printMessage->getPrint(500,0,"","Image Cannot Upload!");
	        	return;                   
        }
	}
	public function actionView() {
		$user[] = Yii::app()->db->createCommand("SELECT * FROM tbl_user WHERE mobile_no='".$_GET['id']."'")->queryRow();
		if(!$user[0]) {
			Yii::app()->printMessage->getPrint(500,0,"","User Not Exist!");
	        	return;
		}
		Yii::app()->printMessage->getPrint(200,1,$user,"");
        return;
	}
	public function actionRegistration() {
		$model = new User;	
		if(!isset($_POST['User'])) {
			Yii::app()->printMessage->getPrint(500,0,"","Parameter is not valid");
	        	return;
		}
		$user = Yii::app()->db->createCommand("SELECT * FROM tbl_user WHERE mobile_no='".$_POST['User']['mobile_no']."'")->queryRow();
		if($user) {
			Yii::app()->printMessage->getPrint(500,0,"","User Already Exist!");
	        	return;
		}
		if(!isset($_FILES['pro_image']['name'])) {              
            Yii::app()->printMessage->getPrint(500,0,"","Image not found!");
	        return;
       	}
        else {
            if(empty($_FILES["pro_image"]["tmp_name"])) { 
            	Yii::app()->printMessage->getPrint(500,0,"","Image empty found!");
	        	return;
            } 
            $type = explode('/',$_FILES["pro_image"]["type"]);
            if(!isset($type[1]) || empty($type[1])) {
            	Yii::app()->printMessage->getPrint(500,0,"","Image type is not found!");
				return;
            }
            $image_url = "/images/profile/".$_POST['User']['mobile_no'].".".$type[1];
            $target_file = Yii::getPathOfAlias('webroot').$image_url;
            $profile = move_uploaded_file($_FILES["pro_image"]["tmp_name"],$target_file);
            if(!empty($profile)) {        	
            	foreach($_POST['User'] as $var=>$value) {
			        // Does the model have this attribute? If not raise an error
				    if($model->hasAttribute($var)){
				         $model->$var = $value;
					}
	        		else{
				        Yii::app()->printMessage->getPrint(500,0,"",$var." Parameter is not allowed");
				        return;
	        			}            
        			}
			        $model->created_by = $model->mobile_no; 
			        $model->created_time = date("Y-m-d H:i:s"); 
			        $model->image_path = $image_url;
			        if($model->save()) {        	
			        	Yii::app()->printMessage->getPrint(200,1,array(),"");
			        	return;
			        }	
			        $error = Yii::app()->printMessage->getModelError($model->errors);
			        Yii::app()->printMessage->getPrint(500,0,"",$error);		
					return;           		
            }
            Yii::app()->printMessage->getPrint(500,0,"","Image Cannot Upload!");
	        return;                                          
        }		
		Yii::app()->printMessage->getPrint(500,0,"","Please, Try Again!");
	       return;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}