<?php

/**
 * This is the model class for table "{{defect_request}}".
 *
 * The followings are the available columns in table '{{defect_request}}':
 * @property integer $id
 * @property string $driver_pin
 * @property string $category
 * @property string $sub_category
 * @property string $reg_no
 * @property integer $meter
 * @property string $apply_time
 * @property string $admin_pin
 * @property string $workshop_pin
 * @property integer $admin_status
 * @property integer $workshop_status
 * @property string $created_time
 * @property string $workshop_time
 * @property string $admin_time
 * @property string $created_by
 * @property string $updated_by
 * @property string $update_time
 */
class DefectRequest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DefectRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{defect_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('driver_pin, category, sub_category, reg_no, meter, apply_time, created_by', 'required'),
			array('meter, admin_status, workshop_status,is_sister', 'numerical', 'integerOnly'=>true),
			array('reason', 'length', 'max'=>2000),
			array('driver_pin, category, sub_category, reg_no, admin_pin, workshop_pin, created_by, updated_by,sister_concern', 'length', 'max'=>128),
			array('created_time, workshop_time, admin_time, update_time,sister_concern', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, driver_pin, category, sub_category, reg_no, meter, apply_time, admin_pin, workshop_pin, admin_status, workshop_status, created_time, workshop_time, admin_time, created_by, updated_by, update_time,sister_concern,is_sister', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'driver_pin' => 'Driver Pin',
			'category' => 'Category',
			'sub_category' => 'Sub Category',
			'reg_no' => 'Reg No',
			'meter' => 'Meter',
			'apply_time' => 'Apply Time',
			'admin_pin' => 'Admin Pin',
			'workshop_pin' => 'Workshop Pin',
			'admin_status' => 'Admin Status',
			'workshop_status' => 'Workshop Status',
			'created_time' => 'Created Time',
			'workshop_time' => 'Workshop Time',
			'admin_time' => 'Admin Time',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'update_time' => 'Update Time',
			'reason' => "Reason",
			'sister_concern'=>'Sister Concern',
			'is_sister'=>'Sister Concern Use'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('driver_pin',$this->driver_pin,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('sub_category',$this->sub_category,true);
		$criteria->compare('reg_no',$this->reg_no,true);
		$criteria->compare('meter',$this->meter);
		$criteria->compare('apply_time',$this->apply_time,true);
		$criteria->compare('admin_pin',$this->admin_pin,true);
		$criteria->compare('workshop_pin',$this->workshop_pin,true);
		$criteria->compare('admin_status',$this->admin_status);
		$criteria->compare('workshop_status',$this->workshop_status);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('workshop_time',$this->workshop_time,true);
		$criteria->compare('admin_time',$this->admin_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('sister_concern',$this->sister_concern,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}