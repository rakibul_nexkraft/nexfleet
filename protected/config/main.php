<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
error_reporting(1);
@ini_set('Display_errors', 1);
@ini_set('memory_limit', '-1');
Return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'theme'=>'shadow_dancer',
	'name'=>'iFleet',
    'defaultController' => 'site/index',

	// preloading 'log' component
	'preload'=>array('log','input','bootstrap'),

	// autoloading model and component classes
	'import'=>array(
    'application.models.*',
	'application.components.*',
    'application.modules.user.models.*',
    'application.modules.user.components.*',
	'application.extensions.EchMultiSelect.*',
    'application.extensions.EHttpClient.*',
	'ext.ESelect2.ESelect2',
	'ext.phpexcel.EExcelView',
	'application.extensions.crontab.*',
	 'ext.yii-mail.YiiMailMessage',
	),

	'modules'=>array(
		'workshopapi',
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
		 'generatorPaths' => array(
          'bootstrap.gii'
       ),
			'class'=>'system.gii.GiiModule',
			'password'=>'pass123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
		 'user'=>array(
            # encrypting method (php hash function)

            'hash' => 'md5',
 
            # send activation email
            'sendActivationMail' => true,
 
            # allow access for non-activated users
            'loginNotActiv' => false,
 
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
 
            # automatically login from registration
            'autoLogin' => true,
 
            # registration path
            'registrationUrl' => array('/user/registration'),
 
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
 
            # login form path
            'loginUrl' => array('/user/login'),
 
            # page after login
            'returnUrl' => array('/user/profile'),
 
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
		
	),

	// application components
	'components'=>array(

/*        'request'=>array(
            'enableCsrfValidation'=>false,
        ),*/
		
	'bootstrap' => array(
	    'class' => 'ext.bootstrap.components.Bootstrap',
	    'responsiveCss' => true,
	),

        'input'=>array(
            'class'         => 'CmsInput',
            'cleanPost'     => true,
            'cleanGet'      => true,
        ),
		
		
		'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ),

        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				
		 array('apiAccident/list', 'pattern'=>'apiAccident/<model:\w+>', 'verb'=>'GET'),
        array('apiAccident/view', 'pattern'=>'apiAccident/<model:\w+>/<id:\d+>', 'verb'=>'GET'),        
        array('apiAccident/update', 'pattern'=>'apiAccident/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
        array('apiAccident/delete', 'pattern'=>'apiAccident/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
        array('apiAccident/create', 'pattern'=>'apiAccident/<model:\w+>', 'verb'=>'POST'),

		array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
        array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),        
        array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
        array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
        array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
        array('api/driverPin', 'pattern'=>'api/<model:\w+>/<driverpin:\d+>/driver', 'verb'=>'GET'),
        array('api/userPin', 'pattern'=>'api/<model:\w+>/<userpin:\d+>/user', 'verb'=>'GET'), 
        array('api/userPin', 'pattern'=>'api/<model:\w+>/<userpin:\d+>/<status:\d+>/user', 'verb'=>'GET'),        
        array('api/userPin', 'pattern'=>'api/<model:\w+>/<userpin:\d+>/<status:\d+>/<page:\d+>/user', 'verb'=>'GET'),        
        array('api/address', 'pattern'=>'api/<model:\w+>/address', 'verb'=>'POST'),
        array('api/vehicle', 'pattern'=>'api/<model:\w+>/vehicle', 'verb'=>'POST'),
       
       	// workshop Api
        array('/workshopapi/<controller>/view', 'pattern'=>'/workshopapi/<controller>/<id:\d+>', 'verb'=>'GET'),
        array('/workshopapi/<controller>/list', 'pattern'=>'/workshopapi/<controller>', 'verb'=>'GET'),
        array('/workshopapi/<controller>/create', 'pattern'=>'/workshopapi/<controller>', 'verb'=>'POST'),
        array('/workshopapi/<controller>/update', 'pattern'=>'/workshopapi/<controller>/<id:\d+>', 'verb'=>'PUT'),            
        array('/workshopapi/<controller>/delete', 'pattern'=>'/workshopapi/<controller>/<id:\d+>', 'verb'=>'DELETE'),
        array('/workshopapi/<controller>/allList', 'pattern'=>'/workshopapi/<controller>/list/<v:\d+>/<p:\d+>', 'verb'=>'GET'), 

				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
	/*'db'=>array(
		'connectionString' => 'mysql:host=35.185.149.147;dbname=ifleetdb',
		'emulatePrepare' => true,
		'username' => 'ifleetdbuser',
		'password' => 'ifleetdb@321',
		'charset' => 'utf8',
        'tablePrefix'=>'tbl_',
        'enableParamLogging'=>true,
	),*/
	'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=pickndrop',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '1234',
			'charset' => 'utf8',
			'tablePrefix'=>'tbl_',
			'enableParamLogging'=>true,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		  'assetManager' => array(
            'linkAssets' => true,
        ),

		  'bracmail'=>array(
            'class' => 'BracMail',
        ),
		 'ButtonColumn'=>array(
            'class' => 'ButtonColumn',
        ),
		  'printMessage' => array(
            'class' => 'application.modules.workshopapi.components.PrintMessage',
        ),
		 /* 'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'smtp-relay.gmail.com',
                'username'=>'imailservice@brac.net',
                'password'=>'Br@c$mtp',
                'encryption'=>'ssl',
                'port'=>'587',           
            ),
            'viewPath' => 'application.views.mail',       
        ),*/
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'smtp.gmail.com',
                'username'=>'imailservice@brac.net',
                'password'=>'Br@c$mtp',
                'encryption'=>'ssl',
                'port'=>'465',           
            ),
            'viewPath' => 'application.views.requisitions',       
        ),
		/*'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				array(
					'class'=>'CWebLogRoute',
				),
			),
		),*/

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'shouman.d@brac.net',
		'mydeskdb' => array(
            //live
            //'host' => '192.168.2.17',
            //'port' => '3306',
            //'user' => 'deskuser',
            //'pass' => 'desk@123',
            //'dbname' => 'ifleetdb'
            //local
            'host' => '35.185.149.147',
            'port' => '3306',
            'user' => 'bractech',
            'pass' => '99.9%available',
            'dbname' => 'flowdb'
        ),
	),
);
