<?php

class HomepageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'notification'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'download', 'deleteFile'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Homepage;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Homepage']))
		{
			$model->attributes=$_POST['Homepage'];
            $image_file = CUploadedFile::getInstance($model,'image_url');

            $model->created_by = Yii::app()->user->username;
            $model->created_time = date('Y:m:d H:i:s');

            if(!empty($image_file)){
                $file_dir = "uploads/homepage/";
                if (!is_dir($file_dir)) {
                    if (!is_dir( "uploads/")) {
                        mkdir("uploads", 0777, true);
                    }
                    mkdir("uploads/homepage", 0777, true);
                }
                $image_name = mktime().$model->name.".".$image_file->extensionName;
                $model->image_url = $image_name;
            }

			if($model->save()) {
                if (!empty($image_file)) {
                    $image_file->saveAs($file_dir . $image_name);
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Homepage']))
		{
		    $image_old = $model->image_url;
			$model->attributes=$_POST['Homepage'];
            $image_file = CUploadedFile::getInstance($model,'image_url');

            if (!empty($image_old) && empty($_POST['Homepage']['image_url'])){
                $model->image_url = $image_old;
            }

            $model->updated_by = Yii::app()->user->username;
            $model->updated_time = date('Y:m:d H:i:s');

            if(!empty($image_file)){
                $file_dir = "uploads/homepage/";
                if (!is_dir($file_dir)) {
                    if (!is_dir( "uploads/")) {
                        mkdir("uploads", 0777, true);
                    }
                    mkdir("uploads/homepage", 0777, true);
                }
                $image_name = mktime().$model->name.".".$image_file->extensionName;
                $model->image_url = $image_name;
            }

			if($model->save())
                if(!empty($image_file)) {
                    $image_file->saveAs($file_dir.$image_name);
                    unlink($file_dir.$image_old);
                }
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		if ($model->delete()){
		    unlink(Homepage::COMMENT_IMAGE_DIRECTORY.$model->image_url);
        }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model=new Homepage('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Homepage']))
            $model->attributes=$_GET['Homepage'];
		$this->render('index',array(
            'model' => $model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Homepage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Homepage']))
			$model->attributes=$_GET['Homepage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Homepage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='homepage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * @param $dir
     * @param $src
     * @param $id
     * @return string
     */
    public function getFileName($dir, $src, $id) {
        $files = array_diff(scandir($dir), array('.', '..'));
        $file_rows = "";

        $row = '';
        if(file_exists($dir.$src) && !empty($src)) {
            $row = '<tr class="odd"><td align="center">' . CHtml::link($src, array("download", 'src' => $dir, 'name' => $src)) . '</td>';
            $row .= '<td align="center"> ' . CHtml::link('Remove', array("deleteFile", 'src' => $dir, 'name' => $src, 'keyid' => $id), array('class'=>'btn btn-xs btn-danger', 'confirm' => 'Do you want to delete this file?')) . '</td>';
            $row .= '</tr> <br>';
        }

        $file_rows .= $row;

        return $file_rows;
    }

    /**
     * @param $src
     * @param $name
     * Download file
     */
    public function actionDownload($src,$name) {
        $dir = $src.$name;

//        $var = new Response();
//        $var->sendFile($src . $name);
//        $var->send();

        if (file_exists($dir)) {
            Yii::app()->request->sendFile(
                $name, file_get_contents($dir), $name, TRUE
            );
        }
//        unlink($src . $name);
    }

    /**
     * @param $src
     * @param $name
     * @param $keyid
     */
    public function actionDeleteFile($src, $name, $keyid) {
        $dir = $src . $name;
        if (file_exists($dir)) {
            unlink($dir);
        }
        $event = $this->loadModel($keyid);
        $event->image_url = null;
        if($event->save(false)){
            $url = Yii::app()->request->urlReferrer;
            $this->redirect($url);
        }

    }

    public function actionNotification(){

        if (!isset(Yii::app()->user->username)) return false;
        $notifications = Yii::app()->db->createCommand("SELECT * FROM tbl_pickdrop_notification WHERE is_notified IS FALSE AND username = '".Yii::app()->user->username."'")->queryAll();
        //Change notification status
        foreach ($notifications as $notification){
            $notific = PickdropNotification::model()->findByPk($notification['id']);
            $notific->is_notified = true;
            $notific->save();
        }
        echo json_encode($notifications);
        die;
    }
}
