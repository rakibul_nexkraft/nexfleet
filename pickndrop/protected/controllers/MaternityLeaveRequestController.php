<?php
date_default_timezone_set("Asia/Dhaka");
class MaternityLeaveRequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','leave'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','maternityLeaveRequestSearch','maternityLeaveRouteSearch'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MaternityLeaveRequest;
		$user_pin=Yii::app()->user->username;
		$model->user_pin = $user_pin;
		$model->status=0;
		$model->created_by = $user_pin;
		$model->created_time = date('Y-m-d H:i:s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MaternityLeaveRequest']))
		{
			$model->attributes=$_POST['MaternityLeaveRequest'];
			if($model->save()){
				$model=new MaternityLeaveRequest('search');
				$model->unsetAttributes(); 
				$this->redirect('index',array(
					'model'=>$model,
				));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->update_time=date("Y-m-d H:i:s");
		$model->updated_by=Yii::app()->user->username; 

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MaternityLeaveRequest']))
		{
			$model->attributes=$_POST['MaternityLeaveRequest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('MaternityLeaveRequest');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		$model=new MaternityLeaveRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaternityLeaveRequest']))
			$model->attributes=$_GET['MaternityLeaveRequest'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MaternityLeaveRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaternityLeaveRequest']))
			$model->attributes=$_GET['MaternityLeaveRequest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=MaternityLeaveRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='maternity-leave-request-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function ActionMaternityLeaveRequestSearch(){
		$from=$_POST['from'];
		$to=$_POST['to'];
		$expectedDate=$_POST['expectedDate'];
		$vehicleTypeId=$_POST['vehicleTypeId'];

		$temp=$this->renderPartial('maternityLeaveRequestSearch',array(
			'model'=>$from,
		));
		echo $temp;
		
	}
	public function actionMaternityLeaveRouteSearch(){
		$model=new MaternityLeaveRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaternityLeaveRequest']))
			$model->attributes=$_GET['MaternityLeaveRequest'];

		$this->render('maternityLeaveRouteSearch',array(
			'model'=>$model,
		));
	}
	public function actionLeave(){

		$model=new MaternityLeaveRequest;
		$user_pin=Yii::app()->user->username;
		$model->user_pin = $user_pin;
		$model->status=0;
		$model->created_by = $user_pin;
		$model->created_time = date('Y-m-d H:i:s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MaternityLeaveRequest']))
		{
			$model->attributes=$_POST['MaternityLeaveRequest'];
			if($model->save()){
				$model=new MaternityLeaveRequest('search');
				$model->unsetAttributes(); 
				$this->redirect('index',array(
					'model'=>$model,
				));
			}
		}

		$model_list=new MaternityLeaveRequest('search');
		$model_list->unsetAttributes();  // clear any default values
		if(isset($_GET['MaternityLeaveRequest']))
			$model_list->attributes=$_GET['MaternityLeaveRequest'];		

		$this->render('leave',array(
			'model'=>$model,'model_list'=>$model_list,
		));
		
	}
}
