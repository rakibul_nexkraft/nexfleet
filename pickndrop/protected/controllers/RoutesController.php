<?php

class RoutesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','mig'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel','createData'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				//'users'=>array('admin'),
				'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Routes;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Routes']))
		{
			$model->attributes=$_POST['Routes'];
			$model->created_by = Yii::app()->user->username;
			$model->available_seat=$model->actual_seat - $model->seat_capacity;
			
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$previous_actual_seat=$model->actual_seat;

		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Routes']))
		{
			$model->attributes=$_POST['Routes'];
						
			$model->seat_fill_up=Routes::borderPass($model->route_no);
			$model->available_seat=$model->actual_seat - $model->seat_fill_up;
			
			if($model->actual_seat<$model->seat_fill_up){
				$model->addError('actual_seat','Seat Capasity Can\'t Smaller Than Seat Fill-up');
			}
			
			if($model->save()){
				if($previous_actual_seat<$model->actual_seat){
					$exta_seat=$model->actual_seat-$previous_actual_seat;

					for($i=0;$i<$exta_seat;$i++){
						$mid=$model->id;
						$sql_rs=SeatRequestAdmin::model()->findByAttributes(
							array("route_id"=>"$mid","status"=>1),
							'queue>:queue',
							array(':queue'=>0)							
						);
					
					//$sql="SELECT * FROM tbl_seat_request WHERE route_id='$model->id' AND status=1 AND queue>0 order by queue asc";
					//$sql_rs=Yii::app()->db->CreateCommand($sql)->queryRow();
						if($sql_rs){
						//	$r_id=$sql_rs->id;
							$seat_request_admin=Yii::app()->createController('seatRequestAdmin');//returns array containing controller instance and action index.
							$seat_request_admin=$seat_request_admin[0]; //get the controller instance.				
							$seat_request_admin->updateWaitingList($sql_rs); //use a public method.

							//var_dump($cat);exit();

						}
					
					}
				}
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('Routes');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
		
		$model=new Routes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Routes']))
			$model->attributes=$_GET['Routes'];
		
		$sql = "SELECT * FROM tbl_commonfleets WHERE id='1'";
		$border_pass = Yii::app()->db->createCommand($sql)->queryAll();
		$this->render('index',array(
			'model'=>$model,
			'border_pass'=>$border_pass,
		));
	}

    public function actionGetRouteNo() {
        if (!empty($_GET['term'])) {
            $qterm = '%'.$_GET['term'].'%';
            $sql = "SELECT route_no as value,route_detail FROM tbl_routes  WHERE route_no LIKE '$qterm'";

            $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            echo CJSON::encode($result); exit;
        } else {
            return false;
        }
    }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Routes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Routes']))
			$model->attributes=$_GET['Routes'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Routes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='routes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionExcel()
	{
		$model = new Routes('search');
		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Routes',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Routes',
			'columns'=>array(
				'name' => 'route_no',
				'route_detail',
				'actual_seat',
				'seat_capacity',
				'available_seat',
				array(
					'name'=>'on_mat_leave',
					'type'=>'raw',
					'value'=>'$data->onMatLeave($data->route_no)',
				),
			),
		));
	}
	public function actionCreateData() {
		$model=new Routes;
		if(isset($_POST['Routes'])) {
			$model->attributes=$_POST['Routes'];
			$model->created_by = Yii::app()->user->username;
			$model->available_seat=$model->actual_seat - $model->seat_capacity;			
			if($model->save())
				echo 1;
			else
			$this->renderPartial('route_form', array('model'=>$model));
		}		
	}
	public function actionMig() {
		//$active_user = Yii::app()->db->createCommand("SELECT * FROM `tbl_commonfleets_list` WHERE application_type<>'Cancel Seat' AND application_type<>'' AND `present_route` IN(SELECT route_no FROM pickndrop.tbl_routes) AND user_pin<>''")->queryAll();	
		//$active_user = Yii::app()->db->createCommand("SELECT * FROM `tbl_commonfleets_list` WHERE application_type<>'Cancel Seat' AND application_type<>'' AND `present_route` IN(SELECT route_no FROM pickndrop.tbl_routes) AND user_pin<>'' AND migrate<>1")->queryAll();	
		$active_user = Yii::app()->db->createCommand("SELECT * FROM `tbl_commonfleets_list` WHERE application_type<>'Cancel Seat' AND application_type='Maternity leave' AND `present_route` IN(SELECT route_no FROM pickndrop.tbl_routes) AND user_pin='173834'")->queryAll();	
		//$active_user = Yii::app()->db->createCommand("SELECT * FROM `tbl_commonfleets_list` WHERE application_type<>'Cancel Seat' AND application_type<>'' AND `present_route` IN(SELECT route_no FROM pickndrop.tbl_routes) AND user_pin=''")->queryAll();	
		var_dump($active_user);exit();
		foreach ($active_user as $key => $value) {
			$value['user_level'] = !empty($value['user_level']) ? $value['user_level'] : 0;
			$value['user_designation'] = !empty($value['user_designation']) ? $value['user_designation'] : "NO Designation";
			$value['user_cell'] = !empty($value['user_cell']) ? $value['user_cell'] : "01713158394";
			$value['user_email'] = !empty($value['user_email']) ? $value['user_email'] : "sharifa.tithee@brac.net";
			$route = Yii::app()->db->createCommand("SELECT * FROM tbl_routes WHERE route_no='".$value["present_route"]."'")->queryRow();
				$stoppage = "";
			if(!empty($value["stoppage_point"])) {	
				$stoppage = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$route['id']."' AND stoppage='".$value["present_route"]."'")->queryRow();
			}
			if(empty($stoppage)) {
				$stoppage = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$route['id']."'")->queryRow();
			}
			if($value['expected_date']=="0000-00-00") {
				if($value['created_time']!="0000-00-00 00:00:00") {
					$d = explode(" ", $value['created_time']);
					$value['expected_date'] = $d[0];
				}
				else {
					$d = explode(" ", $value['updated_time']);
					$value['expected_date'] = $d[0];
				}					
			}
			if(empty($value['created_by'])) {
				$value['created_by'] = 'admin';					
			}
			if($value['created_time']=="0000-00-00 00:00:00") {
				$value['created_time'] = $value['expected_date']." 00:00:00";
			}
			if(empty($value['created_by'])) {
				$value['created_by'] = 'admin';					
			}
			if(empty($value['updated_by'])) {
				$value['updated_by'] = 'admin';					
			}
			if($value['updated_time']=="0000-00-00 00:00:00") {
				$value['updated_time'] = $value['created_time'];
			}
			//if($value['application_type']!="Pick and Drop") continue;	
			//var_dump(expression)			
				
			/*$sql_rs = Yii::app()
                ->db
                ->createCommand()
                ->insert(
	                'tbl_seat_request',
	                array(
	                  	'id'=>'',                       
	        			"user_pin"=>trim($value['user_pin']),
	        			"user_name"=>trim($value['user_name']),
	        			"user_department"=>trim($value['user_dept']),
	        			"user_level"=>trim($value['user_level']),
	        			"user_cell"=>trim($value['user_cell']),
	        			"email"=>trim($value['user_email']),
	        			"route_id"=>trim($route['id']),
	        			"stoppage_id"=>trim($stoppage['id']),
	        			"expected_date"=>trim($value['expected_date']),
	        			"remarks"=>trim($value['fleet_remarks']),
	        			"status"=>"1",
	        			"user_status"=>"0",
	        			"queue"=>"-1",
	        			"created_by"=>trim($value['created_by']),	        			
	        			"action"=>"1",
	        			"created_time"=>trim($value['created_time']),
	        			"updated_by"=>trim($value['updated_by']),
	        			"updated_time"=>trim($value['updated_time']),
	        			"user_designation"=>trim($value['user_designation']),
	        			"residence_address"=>$value['residence_address'],
	        			"maternity_leave"=>"",
	            	)
	            );*/	
	            echo trim($value['updated_time']);
/*
	        if($value['application_type']=="Maternity Leave"){
	        	
	        	$sql_rs = Yii::app()
                ->db
                ->createCommand()
                ->insert(
	                'tbl_maternity_leave_request',
	                array(
	                  	'id'=>'',                       
	        			"user_pin"=>trim($value['user_pin']),	        			
	        			"from_date"=>trim($value['mt_leave_from']),
	        			"to_date"=>trim($value['mt_leave_to']),
	        			"maternity_user_pin"=>'',
	        			"maternity_id"=>'',
	        			"status"=>1,
	        			"created_by"=>trim($value['created_by']),
	        			"updated_by"=>trim($value['updated_by']),
	        			'created_time'=>trim($value['mt_leave_from']." 00:00:00"),
	        			'update_time'=>trim($value['updated_time']),
	            	)
	            );	
	           
	        }*/       
	        /*if($sql_rs) {
	        	$vehicletype_id = Yii::app()->db->createCommand("SELECT * FROM tbl_vehicles WHERE reg_no='".$route['vehicle_reg_no']."'")->queryRow();
	        	$sql_rs = Yii::app()
                ->db
                ->createCommand()
                ->insert(
	                'tbl_commonfleets',
	                array(
	                  	'id'=>trim($value['id']),                       
	        			"user_pin"=>trim($value['user_pin']),
	        			"user_name"=>trim($value['user_name']),
	        			"user_dept"=>trim($value['user_dept']),
	        			"user_level"=>trim($value['user_level']),
	        			"user_designation" =>trim($value['user_designation']),
	        			"user_cell"=>trim($value['user_cell']),	        			
	        			"user_email"=>trim($value['user_email']),
	        			"telephone_ext"=>trim($value['telephone_ext']),
	        			"application_type" => trim($value['application_type']),    			
	        			"residence_address"=>$value['residence_address'],
	        			"recommended_by"=>trim($value['recommended_by']),
	        			"recommended_by_desig"=>trim($value['recommended_by_desig']),
	        			"previous_route"=>trim($value['previous_route']),
	        			"preferred_route"=>trim($value['preferred_route']),
	        			"present_route"=>trim($value['present_route']),
	        			"stoppage_point"=>trim($stoppage['stoppage']),
	        			"pick_up_time"=>trim($stoppage['pickup_time']),
	        			"expected_date"=>trim($value['expected_date']),	        			
	        			"mt_leave_from"=>trim($value['mt_leave_from']),
	        			"mt_leave_to"=>trim($value['mt_leave_to']),
	        			"vehicle_reg_no"=>trim($route['vehicle_reg_no']),
	        			"vehicletype_id"=>trim($vehicletype_id['vehicletype_id']),
	        			"user_remarks"=>trim($value['user_remarks']),
	        			"fleet_remarks"=>trim($value['fleet_remarks']),
	        			"created_by"=>trim($value['created_by']),
	        			"created_time"=>trim($value['created_time']),
	        			"updated_by"=>trim($value['updated_by']),
	        			"updated_time"=>trim($value['updated_time']),
	        			"approve_status"=>trim($value['approve_status']),
	        			"active"=>trim($value['active']),	        			
	        			"maternity_assign_id"=>"",	        			
	            	)
	            );

		        if($sql_rs) {
		        	$sql_rs = Yii::app()->db->createCommand("UPDATE tbl_commonfleets_list SET migrate=1 WHERE id='".trim($value['id'])."'")->execute();
		        }
		       
			}*/

	    }
			

	}


}
