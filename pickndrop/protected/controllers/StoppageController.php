<?php
date_default_timezone_set("Asia/Dhaka");
class StoppageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excel','createData'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($route_id=null)
	{
		$model=new Stoppage;
		$model->created_time=date('Y-m-d H:i:s');
		$model->created_by=Yii::app()->user->name;
		
		$model->route_id=$route_id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Stoppage']))
		{
			$model->attributes=$_POST['Stoppage'];
			$time=explode(".", $model->pickup_time);
			if(isset($time[1])){
				$model->pickup_time=$time[0].":".$time[1].":00";
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated_time=date('Y-m-d H:i:s');
		$model->updated_by=Yii::app()->user->name;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Stoppage']))
		{
			$model->attributes=$_POST['Stoppage'];
			$time=explode(".", $model->pickup_time);
			if(isset($time[1])){
				$model->pickup_time=$time[0].":".$time[1].":00";
			}
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('Stoppage');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$model=new Stoppage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Stoppage']))
			$model->attributes=$_GET['Stoppage'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Stoppage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Stoppage']))
			$model->attributes=$_GET['Stoppage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Stoppage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='stoppage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionExcel()
	{	
		$model = new Stoppage('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];
		
		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->search(),
			'title'=>'iFleet_Route_Stoppages',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Route_Stoppages',
			'columns'=>array(
						'id',
						array('name'=>'route_id',
							'type'=>'raw',
							'value'=>'$data->routeNo($data->route_id)',							
						),
						
						'stoppage',
						'pickup_time',
						'created_by',
						'created_time',
						
						'updated_by',
						'updated_time',		
						
							),
					));
	}
	public function actionCreateData() {
		$model=new Stoppage;
		$model->created_time=date('Y-m-d H:i:s');
		$model->created_by=Yii::app()->user->username;			
		if(isset($_POST['Stoppage'])) {			
			$model->attributes=$_POST['Stoppage'];
			$route_id=Yii::app()->db->createCommand("SELECT * FROM tbl_routes where route_no='".$_POST["Stoppage"]["route_id"]."'")->queryRow();
			$model->route_id = $route_id['id'];
			$time=explode(".", $model->pickup_time);
			if(isset($time[1])) {
				$model->pickup_time=$time[0].":".$time[1].":00";
			}
			if($model->save())
				echo 1;
			else {
			$model->route_id = $_POST['Stoppage']['route_id'];
			$this->renderPartial('stoppage_form', array('model'=>$model));
			}
		}

		
	}
}
