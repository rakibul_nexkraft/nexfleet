<?php
date_default_timezone_set("Asia/Dhaka");
class SeatBookingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','seatBookingForm','bookSeat','bookingHistory','bookingHistoryAdmin','collection','seatToday','shohochorReport'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','excelshohochorReport','seatAV','seatCancel','seatTransfer','transferOther'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SeatBooking;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatBooking']))
		{
			$model->attributes=$_POST['SeatBooking'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatBooking']))
		{
			$model->attributes=$_POST['SeatBooking'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SeatBooking');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SeatBooking('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatBooking']))
			$model->attributes=$_GET['SeatBooking'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SeatBooking::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seat-booking-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionSeatBookingForm(){  
		$directions = substr($_POST['sid'], -1); 
		$_POST['sid'] = substr($_POST['sid'], 0, -1); 
		$seats_av = $this->seatAVCheck($_POST['sid'],$directions);
		if($seats_av<= 0){
			echo "<span style='padding-left:20px'>Sorry, Seat Not Available, Try For Another Route.</span>";
			return;
		}
        if($_POST['sid'][0]=='s'){
	        $_POST['sid']=substr($_POST['sid'],1);
	        $sale = Yii::app()->db->createCommand("SELECT s.*, r.route_no FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id WHERE s.id='".$_POST['sid']."'")->queryRow();
        }
        else{
        	
        	$route = Routes::model()->findByPk($_POST['sid']);
        	/*$tr_dr = 1;
        	$tr_dr = (date('H:i:s')<="17:00:00" && date('H:i:s')>="09:00:00") ? 2 : 1;
        	$date=date("Y-m-d", strtotime("+1 day"));
        	$date =(date('H:i:s')<="17:00:00" && date('H:i:s')>="09:00:00") ? date('Y-m-d') : date("Y-m-d", strtotime("+1 day"));*/
        	$tr_dr = $directions=="O" ? 2 : ($directions=="I" ? 1 : "");
        	$date = $directions=="O" ? date('Y-m-d') : ($directions=="I" ? date("Y-m-d", strtotime("+1 day")) : "");
        	$sale = array(
        		'route_id'=>$route['id'],
        		'route_no'=>$route['route_no'],
        		'price'=>$route['market_price'],
        		'travel_direction'=>$tr_dr,
        		'from_date'=>$date
        	);
        }        
        $this->renderPartial('booking_form', array(
            'sale'=>$sale
        ));
         return;
    }
    public function actionSeatAV() {
    	$directions = substr($_POST['sid'], -1); 
		$_POST['sid'] = substr($_POST['sid'], 0, -1);
    	$seats_av = $this->seatAVCheck($_POST['sid'],$directions);
    	echo 'Seat Available : <span style="font-size:20px; "><strong>'.$seats_av .'</strong></span>';
        	return;
    }
    private function seatAVCheck($sid,$directions){
    	$seat_count = 0; 
    	if($sid[0]=='s'){
	        $sid = substr($sid,1);
	        $sale = Yii::app()->db->createCommand("SELECT s.route_id as id, r.route_no, s.from_date,s.travel_direction, r.actual_seat FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id WHERE s.id='".$sid."'")->queryRow();
	        $seat_sale = Yii::app()->db->createCommand("SELECT count(id) as total_seat FROM tbl_seat_sale  WHERE route_id='".$sale['id']."' AND from_date='".$sale["from_date"]."' AND travel_direction='".$sale["travel_direction"]."' AND id NOT IN(SELECT sale_id FROM tbl_seat_booking WHERE status=0)")->queryRow();
	        $seat_count = $seat_sale['total_seat'];

        }
        else {        	
        	$sale = Routes::model()->findByPk($sid);        	
        }
        if($sale) {
        	$seats_oc = Routes::borderPass($sale['route_no']);
        	$sql = ($directions=="O") ? " AND date_from='".date('Y-m-d')."' AND direction_travale=2" : " AND date_from='".date("Y-m-d", strtotime("+1 day"))."' AND direction_travale=1";

            $route_seat_sale = Yii::app()->db->createCommand("SELECT count(id) as rs FROM tbl_seat_booking WHERE route_id='".$sale['id']."' AND sale_id=0 AND status=0 ".$sql)->queryRow(); 
        	return  $sale['actual_seat']-$seats_oc-$route_seat_sale['rs']+$seat_count;        	
        }
        return 0;
    }
    public function actionBookSeat(){
    	$directions = $_POST['SeatBooking']['direction_travale']==2 ? "O" : "I"; 
    	if(!empty($_POST['SeatBooking']['sale_id'])){
    		$sale_seat_id='s'.$_POST['SeatBooking']['sale_id'];
    		$seats_av = $this->seatAVCheck($sale_seat_id,$directions);
    	}
    	else{
    		$seats_av = $this->seatAVCheck($_POST['SeatBooking']['route_id'],$directions);
    	}
    	if($seats_av<= 0){
			echo "Sorry, Seat Not Available, Try For Another Route.";
			return;
		}
    	$model=new SeatBooking;
        $model->user_pin = Yii::app()->user->username;           
        $model->created_by = Yii::app()->user->username;           
        $model->created_time = date("Y-m-d H:i:s");  
        if(isset($_POST['SeatBooking'])){
			$model->attributes=$_POST['SeatBooking'];
			$b_user_history_last = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin='".$model->user_pin."' ORDER BY id DESC")->queryRow();
			if(empty($b_user_history_last)){
				echo "Sorry, You Have No  Balance.";
				return;
			}
			else{
				$b_user_balance = $b_user_history_last['balance']-$model->price;
				if($b_user_balance<0){
					echo "Sorry, You Have Not Enough Balance.";
					return;
				}
			}
			$balance = !empty($tr_history_last) ? $tr_history_last['balance'] : 0;
			if($model->save()) {
				$price =  ($model->price*60)/100;
				$tr_history_last = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin = '0' ORDER BY id DESC")->queryRow();
				$balance = !empty($tr_history_last) ? $tr_history_last['balance'] : 0;
				$sale_entry= array(
					'user_pin' =>'0',
					'sale_id'=>$model->sale_id,
					'booking_id'=>0,
					'credit'=>$model->price,
					'debit' => 0,
					'payment_method'=>'',
					'transaction_id'=>'',
					'balance' =>$balance+$model->price,
					'sale_booking_id'=>$model->id,
					'created_by'=>Yii::app()->user->username,
					'created_time'=>date("Y-m-d H:i:s"),
				);
				if(!empty($model->sale_id) && ($model->sale_id!=0)) {
					$seat_sale = SeatSale::model()->findByPk($model->sale_id);
					$tr_history_last = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin='".$seat_sale["user_pin"]."' ORDER BY id DESC")->queryRow();
					$balance = !empty($tr_history_last) ? $tr_history_last['balance'] : 0;
					$sale_entry= array(
					'credit'=>$price,
					'balance' =>$balance+$price,
					'user_pin' =>$seat_sale["user_pin"],					
					);					
				}
				$this->transactionBook($sale_entry);					
				$book_entry= array(
				'user_pin' =>$model->user_pin,
				'sale_id'=>0,
				'booking_id'=>$model->id,
				'credit'=>0,
				'debit'=>$model->price,
				'payment_method'=>'',
				'transaction_id'=>'',
				'balance' =>$b_user_balance,
				'sale_booking_id'=>0,
				'created_by'=>Yii::app()->user->username,
				'created_time'=>date("Y-m-d H:i:s"),
				);
				$this->transactionBook($book_entry);		

				echo "Your Data Save Successfully.";
				return;
			}
		}
		echo "Please, Try Again!";
		return;
    }
    private function transactionBook($sale_entry){
    	$model=new TransactionHistory;
    	$model->unsetAttributes(); 
    	$model->attributes=$sale_entry;
		$model->save();
    }
    public function actionBookingHistory(){
        $model=Yii::app()->db->createCommand("SELECT b.*, r.route_no FROM tbl_seat_booking as b INNER JOIN tbl_routes as r ON r.id=b.route_id WHERE b.user_pin='".Yii::app()->user->username."' ORDER BY id desc")->queryAll();

        $this->render('booking_history',array(
			'model'=>$model,
		));
        
    }
    public function actionBookingHistoryAdmin(){
        $model=Yii::app()->db->createCommand("SELECT b.*, r.route_no, r.price FROM tbl_seat_booking as b INNER JOIN tbl_routes as r ON r.id=b.route_id  ORDER BY id desc")->queryAll();

        $this->render('booking_history',array(
			'model'=>$model,
		));
        
    }
   	public function actionAdminBooking(){
        $model=new SeatSale('search');
        $model->unsetAttributes();
        $use_route= Yii::app()->db->createCommand("SELECT r.* FROM  tbl_commonfleets as c INNER JOIN  tbl_routes as r ON r.route_no=c.present_route WHERE c.application_type<>'Cancel Seat' AND c.user_pin='".Yii::app()->user->username."'")->queryRow();
        
        $this->render('user_shohochor',array(
            'use_route'=>$use_route,
            'model'=>$model
            ));
        
    }
    public function actionCollection(){
        $collection=Yii::app()->db->createCommand("SELECT SUM(b.price) as sum_price, b.route_id, r.route_no FROM tbl_seat_booking as b INNER JOIN tbl_routes as r ON r.id=b.route_id GROUP BY b.route_id")->queryAll();        
        $this->render('collection',array(            
            'collection'=> $collection
            ));
        
    }
    public function actionSeatToday(){
    	$booking = Yii::app()->db->createCommand("SELECT r.*, b.sale_id, b.direction_travale as tr FROM  tbl_seat_booking as b INNER JOIN tbl_routes as r ON r.id=b.route_id WHERE b.date_from='".date("Y-m-d")."' AND status=0")->queryAll();
    	$route_list=array();    	
    	if(count($booking)>0) {
    		foreach ($booking as $key => $value) {    		
	    		if (array_key_exists($value['id'], $route_list)) {
	    			if($value['sale_id']==0) { $route_list[$value['id']]['ifleet']++; }
	    			else{
	                	if($value['tr']==1) {$route_list[$value['id']]['HO-IN']++;}    
	                    
	                	if($value['tr']==2) { $route_list[$value['id']]['HO-OUT']++; }
	                }                                
                }
	            else {
	    		$route_list[$value['id']]=array(
	                'id'=>$value['id'],
	                'route_no'=>$value['route_no'],
	                'ifleet'=>($value['sale_id']==0 ? 1 : 0),
	                'type'=>$value['type'],
	                'HO-IN'=>(($value['tr'] == 1 && $value['sale_id'] != 0) ? 1 : 0),
	                'HO-OUT'=>(($value['tr']== 2 && $value['sale_id'] != 0 )? 1 : 0),
	                );
	            }
    		}
    	}
    	$this->render('open_seat',array(
			'model'=>$route_list,
		));
    	
    }
    public function actionShohochorReport(){
    	$model = new SeatBooking('shohochorReport');
    	$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatBooking']))
			$model->attributes=$_GET['SeatBooking'];
    	$this->render('shohochorReport',array(
			'model'=>$model,
		));
    }
    public function actionExcelshohochorReport(){
    	$model = new SeatBooking('shohochorReport');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['criteria']))
			$model->attributes=$_GET['criteria'];

		$this->widget('ext.phpexcel.EExcelView', array(
			'dataProvider'=> $model->shohochorReport(),
			'title'=>'iFleet_Shohochor_Report',
			//'autoWidth'=>true,
			'grid_mode'=>'export',
			'exportType'=>'Excel2007',
			'filename'=>'iFleet_Shohochor_Report',
			'columns' => array(            
            'user_pin',   
            array(
                'header' => 'Total Booking',
                'type' => 'raw',
                'value' => '$data->total_book',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),  
            array(
                'header' => 'Total Expenditure',
                'type' => 'raw',
                'value' => '$data->total_spend',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
         ),
		));
		Yii::app()->end();
    }
    public function actionSeatCancel(){
    	$cancel = Yii::app()->db->createCommand("UPDATE tbl_seat_booking SET status=-1 WHERE id='".$_POST["id"]."'")->execute();
    	if($cancel){
    		$book = SeatBooking::model()->findByPk($_POST["id"]);
    		$route = Routes::model()->findByPk($book['route_id']); 
    		echo "Your seat booking cancel for route no. ".$route['route_no'].".";
    	}
    	else {
    		echo "Sorry, try again!";
    	}
    }
    public function actionSeatTransfer() {    
    	$user_pin = $phone = $message = "";
    	
    	if(isset($_POST['user_pin']) && !empty($_POST['user_pin'])) {  	
    		$user_pin=$pin = $_POST['user_pin'];
            $image_url='../photo/'.$pin.".png";
            $base64string=file_get_contents("http://api.brac.net/v1/staffs/".$pin."/photo?Key=960d5569-a088-4e97-91bf-42b6e5b6d101");
            file_put_contents($image_url, base64_decode($base64string)); 
            $uri = "http://api.brac.net/v1/staffs/".$pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
            $staffInfo = CJSON::decode(file_get_contents($uri)); 
            if(count($staffInfo)>0)
           		$phone = $staffInfo[0]['MobileNo'];
           	else{
           		$message = "User Not Found!";
           	}  

        }
    	$this->renderPartial('bookingTransfer', array(
            'id'=>$_POST['id'],
            'user_pin'=>$user_pin,
            'phone' => $phone,
            'message' =>$message, 
        ));
        return; 
    }
    public function actionTransferOther() {     
     $transfer = Yii::app()->db->createCommand("UPDATE tbl_seat_booking SET transfer_user='".$_POST["SeatBooking"]["transfer_user"]."', phone='".$_POST["SeatBooking"]["phone"]."' WHERE id='".$_POST["SeatBooking"]["id"]."'")->execute();
    	if($transfer){
    		$book = SeatBooking::model()->findByPk($_POST["SeatBooking"]["id"]);
    		$route = Routes::model()->findByPk($book['route_id']); 
    		echo "Your seat booking transfer to user pin ".$book["transfer_user"]." for route no. ".$route['route_no'].".";
    	}
    	else {
    		echo "Sorry, try again!";
    	}
    }
}
