<?php
date_default_timezone_set("Asia/Dhaka");
class SeatSaleController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','saleSeat','saleHistory','saleHistoryAdmin','timeCheck'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','seatOpen'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SeatSale;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatSale']))
		{
			$model->attributes=$_POST['SeatSale'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatSale']))
		{
			$model->attributes=$_POST['SeatSale'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SeatSale');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SeatSale('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatSale']))
			$model->attributes=$_GET['SeatSale'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SeatSale::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seat-sale-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionSaleSeat(){
        $model = new SeatSale;
        $model->user_pin = Yii::app()->user->username;           
        $model->created_by = Yii::app()->user->username;           
        $model->created_time = date("Y-m-d H:i:s");
		if(isset($_POST['SeatSale']))
		{
			$model->attributes=$_POST['SeatSale'];
			$check_sale = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_sale WHERE user_pin='".$model->user_pin."' AND route_id='".$model->route_id."' AND from_date='".$model->from_date."' AND travel_direction='".$model->travel_direction."'")->queryRow();
			if($check_sale) {
				echo "Your All Ready Sale This Seat.";
				return;
			}
			
			if($model->save()){
				echo "Your Data Save Successfully.";
				return;
			}

		}

		echo "Please, Try Again!";
		return;
    }
    public function actionSaleHistory(){
        $model=Yii::app()->db->createCommand("SELECT s.*, r.route_no FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id WHERE s.user_pin='".Yii::app()->user->username."' ORDER BY id desc")->queryAll();
        $this->render('sale_history',array(
			'model'=>$model,
		));
        
    }
    public function actionSaleHistoryAdmin(){
        $model=Yii::app()->db->createCommand("SELECT s.*, r.route_no FROM tbl_seat_sale as s INNER JOIN tbl_routes as r ON r.id=s.route_id  ORDER BY id desc")->queryAll();
        $this->render('sale_history',array(
			'model'=>$model,
		));
        
    }
    public function actionTimeCheck(){    	
    	
    	if($_POST['SeatSale']['travel_direction'] == 1 && date('Y-m-d', strtotime("+1 day")) == $_POST['SeatSale']['from_date'] && date('H:i:s')<"20:00:00"){
    		echo 1;
    		
    	}
    	else if($_POST['SeatSale']['travel_direction'] == 2 && date('Y-m-d') == $_POST['SeatSale']['from_date'] && date('H:i:s')<"16:30:00"){
    		echo 1;    		
    	}
    	else{
    		echo 2;

    	}
    	
    }
    public function actionSeatOpen() {
    	$selling=Yii::app()->db->createCommand("SELECT r.*, vt.type, ss.travel_direction as tr FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id INNER JOIN tbl_seat_sale as ss ON ss.route_id=r.id WHERE ss.from_date='".date("Y-m-d")."'")->queryAll();
  		$valid_routes = Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id INNER JOIN tbl_stoppage as s ON s.route_id=r.id GROUP BY r.id")->queryAll();
  		$route_list=array(); 
	    foreach ($valid_routes as $key => $value) {
	           $seats_oc = Routes::borderPass($value['route_no']);  
	           $seats_av = $value['actual_seat']-$seats_oc;         
	           if($seats_av!=0) {
	            $route_list[$value['id']] = array(
	                'id'=>$value['id'],
	                'route_no'=>$value['route_no'],
	                'av'=>$seats_av,
	                'type'=>$value['type'],
	                'HO-IN'=>0,
	                'HO-OUT'=>0,
	                );
	           }       
	    }
    if(isset($selling) && count($selling)>0) {
            foreach ($selling as $key => $value) {
                if (array_key_exists($value['id'], $route_list)) {
                	if($value['tr']==1)     
                    $route_list[$value['id']]['HO-IN']++;
                	if($value['tr']==2)
                    $route_list[$value['id']]['HO-OUT']++;                   
                }
                else {

                $route_list[$value['id']]=array(
                'id'=>$value['id'],
                'route_no'=>$value['route_no'],
                'av'=>$seats_av,
                'type'=>$value['type'],
                'HO-IN'=>($value['tr']==1 ? 1 : 0),
                'HO-OUT'=>$value['tr']==2 ? 1 : 0,
                );
                }
             }
     }   
      $this->render('open_seat',array(
			'model'=>$route_list,
		));
 }

    
}
