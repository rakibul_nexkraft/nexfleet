<?php
date_default_timezone_set("Asia/Dhaka");
class SeatRequestController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','requestList','maternityUpdated','userMarketPlaceSummery','FindRouteList','pointSearch'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','zone','route','routeInformation','userRouetRquest','routeInformationUpdate','userRouteRquestUpdate','dashboardRouteDetail','findRoute','routeSubmit','requestDetail'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new SeatRequest;
		/*$user_pin=$_GET['user_pin'];
		$model->user_pin = $user_pin;
		$model->created_by = $user_pin;

		//$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
		$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
        $staffInfo = CJSON::decode(file_get_contents($uri));
    
        $model->user_level = $staffInfo[0]['JobLevel'];
        $model->user_name= $staffInfo[0]['StaffName'];
        $model->email=$staffInfo[0]['EmailID'];
        $model->user_cell=$staffInfo[0]['MobileNo'];
        $model->user_department=$staffInfo[0]['ProjectName'];
        $model->user_designation=$staffInfo[0]['DesignationName'];
        $model->residence_address=$staffInfo[0]['PresentAddress'];
		$model->created_time = date('Y-m-d H:i:s');*/

		$user_pin=Yii::app()->pickanddrop->username;
		$model->user_pin = $user_pin;
		$model->created_by = $user_pin;

		
    
        $model->user_level = Yii::app()->pickanddrop->level;
        $model->user_name= Yii::app()->pickanddrop->firstname." ".Yii::app()->pickanddrop->lastname;
        $model->email=Yii::app()->pickanddrop->email;
        $model->user_cell=Yii::app()->pickanddrop->phone;
        $model->user_department=Yii::app()->pickanddrop->department;
        $model->user_designation=Yii::app()->pickanddrop->designation;
        $model->residence_address=Yii::app()->pickanddrop->address;
		$model->created_time = date('Y-m-d H:i:s');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequest']))
		{
			$model->attributes=$_POST['SeatRequest'];
			if($model->save()){				
				$this->redirect(array('view','id'=>$model->id,'user_pin'=>$user_pin));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequest']))
		{
			$model->attributes=$_POST['SeatRequest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		$model->status=3;
		$model->save();
		$this->actionIndex();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		//if(!isset($_GET['ajax']))
			//$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('SeatRequest');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		$model->attributes=$_POST['SeatRequest'];
		if(isset($_POST['SeatRequest'])){
			$sql="SELECT * FROM tbl_stoppage WHERE ";
			if(!empty($_POST['SeatRequest']['from'])&&!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."' OR stoppage='".$_POST['SeatRequest']['to']."'";
			else if(!empty($_POST['SeatRequest']['from']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."'";
			else if(!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['to']."'";
			else {
				$sql="ALL";
			}
			$route_id="";
			$sql_route="SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id";
			$sql_vehicle_type=0;
			if(!empty($_POST['SeatRequest']['vehicle_type_id'])){
				$sql_route.=" WHERE v.vehicletype_id='".$_POST['SeatRequest']['vehicle_type_id']."'";
				$sql_vehicle_type=1;
			}
//			echo $sql.'<br>';
			if($sql!="ALL"){
				$route_id_rs=Yii::app()->db->createCommand($sql)->queryAll();
				if(count($route_id_rs)>0){
					foreach ($route_id_rs as $key => $value) {
						$route_id.=$value['route_id'].",";
					}
					
				}
				$route_id=rtrim($route_id,',');
				if(strlen($route_id) > 0){
                    if($sql_vehicle_type==0)
                        $sql_route.=" WHERE r.id IN($route_id)";
                    else
                        $sql_route.=" AND r.id IN($route_id)";
                }
			}
//            echo $sql_route.'<br>';
			$routes_rs=Yii::app()->db->createCommand($sql_route)->queryAll();

//			die;
			$this->render('index',array(
			'model'=>$model,
			'routes_rs'=>$routes_rs,			
			));
			return;
			
		}

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequest']))
			$model->attributes=$_GET['SeatRequest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SeatRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seat-request-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionZone(){
		$id=$_POST['id'];
		$sql="SELECT id,route_no,route_detail from tbl_routes where zone_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		//var_dump($aql_rs);exit();
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}
	}
	public function actionRoute(){
		$id=$_POST['id']; 
		$sql="SELECT id,pickup_time,stoppage from tbl_stoppage where route_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}

	}
    public function actionRouteInformation() {
    	$route_id = $_POST['route_id'];
    	$user_pin= Yii::app()->user->username;
    	$check_route_sql = "SELECT * FROM tbl_seat_request WHERE  user_pin='$user_pin'";
    	$check_route_sql = Yii::app()->db->createCommand($check_route_sql)->queryRow();
    	$stoppageId = $_POST['stoppageId'];
    	$expectedDate = $_POST['expectedDate'];
    	$changeRoute = $_POST['changeRoute'];
    	if(empty($check_route_sql)|| !empty($changeRoute)){ 
    	$html="<div class='center s002 '>
	   			<h4 class='heading-custom'>SUBMIT YOUR INFORMATION</h4>	   	
	   		 </div>";
    	if(!empty($route_id)){//route id have
    		$route=Routes::model()->findByPk($route_id);
    		if(!empty($route)){//Route found
    			$total_seat=$route['actual_seat'];
    			$borderPass=Routes::borderPass($route['route_no']);
    			$av_seat=$total_seat-$borderPass;
    			$loop_seats=$total_seat%4==0?$total_seat:$total_seat+(4-$total_seat%4);

    			$stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='".$route_id."' ORDER BY pickup_time";
    			$stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();

    			$driver_info_sql="SELECT d.* FROM tbl_vehicles as v INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin WHERE v.reg_no='".$route['vehicle_reg_no']."'";
    			$driver_info_rs=Yii::app()->db->createCommand($driver_info_sql)->queryRow();

    			$html.='<div class="accordion accordion-border clearfix">';
	   			$html.='<div class="col_one_third">';
	   			$html.='<div class="information-box">';
	   			$html.='<div class="steering clearfix">';
	   			$html.='<img  src="'.Yii::app()->theme->baseUrl.'/images/car/steering.jpg" alt="Steering">';
	   			$html.='</div>';
	   			$html.='<div class="clearfix"></div>';
	   			if($total_seat>0){//check total seat
	   				
	   				for($i=1; $i<=$loop_seats;$i++) {//seat loop start	   					
	   					if($i==1){
	   						$html.='<div class="seat-plan">';
	   					}
	   					if($i<=$borderPass){
	   						$html.='<div class="seat-location">'.$i.'</div>';
	   					}
	   					else{
	   						if($i>$total_seat){
	   						$html.='<div class="seat-location-white">X</div>';
	   						}
	   						else{
	   							$html.='<div class="seat-location-white">'.$i.'</div>';
	   						}
	   					}
	   					if($i%2==0 && $i%4!=0){
	   						   $html.='<div class="seat-location-gap"></div>';
	   					}	   						
	   					if($i%4==0){	   					
	   						$html.='</div>';
	   					$html.='<div class="clearfix"></div>';
	   						if(($i+1)<=$loop_seats)	   					
	   					$html.='<div class="seat-plan">';
	   					}   					
	   				}// seat loop end
	   				$html.='<div class="min-hight"></div>';
	   			}//check total seat
	   				$html.='</div>';
	   			$html.='</div>';
	   			//stoppage time div start
	   		$html.='<div class="col_one_third">';
	   			$html.='<div class="information-box">';
	   					$html.='<div class="center s002 ">';  

   							$html.='<h4 class="heading-custom">Route '.$route['route_no'].' Stoppages List</h4>';
   	
   						$html.='</div>';
   						$html.='<div class="clearfix"></div>';
   						$html.='<div class="stoppage-table">';
   						$html.='<div class="table-responsive">';

						$html.='<table class="table cart">';
							$html.='<thead>';
								$html.='<tr>';
									$html.='<th class="cart-product-price"><strong>Stoppage</strong></th>';
									$html.='<th class="cart-product-price"><strong>Time</strong></th>';
									
								$html.='</tr>';
							$html.='</thead>';
							$html.='<tbody>';
							if(count($stoppage_rs)>0){
								foreach ($stoppage_rs as $key => $value) {							
								$html.='<tr class="cart_item">';
									$html.='<th  class="cart-product-price">';
										$html.= $value['stoppage'];
									$html.='</th >';

									$html.='<th  class="cart-product-price">';
										$html.= $value['pickup_time'];
									$html.='</th >';

								$html.='</tr>';
								}
							}
							else{
								$html.='<tr class="cart_item">';
								$html.='<th colspan="2"  class="cart-product-price">';
								$html.= 'Stoppage Not Found';
								$html.='</th >';
								$html.='</tr>';

							}						
							$html.='</tbody>';

						$html.='</table>';

					$html.='</div>';
   						$html.='</div>';
	   			$html.='</div>';
	   		$html.='</div>';
	   		// Information div start
	   	
	   		$html.='<div class="col_one_third col_last">';
	   			$html.='<div class="information-box-last">';
	   				$html.='<div class="driver-info">';
	   				if($driver_info_rs){
	   					$image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/".$driver_info_rs['pin'].".png";
                            if (!file_exists($image_url)) {
                                $image_save='../photo/'.$driver_info_rs['pin'].".png";
                                $base64string=file_get_contents("http://api.brac.net/v1/staffs/".$driver_info_rs['pin']."/photo?Key=960d5569-a088-4e97-91bf-42b6e5b6d101");
        						file_put_contents($image_save, base64_decode($base64string));  
                            }
	   					$html.='<div class="driver-info-row">';
		   					$html.= '<img src="'.$image_url.'" style="" class="img-responsive" alt="User image">';
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Name :</strong> '. $driver_info_rs['name'];
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Pin :</strong> ' . $driver_info_rs['pin'];
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Phone :</strong> ' . $driver_info_rs['phone'];
		   				$html.='</div>';
		   			}
		   			else{
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Driver Information :</strong>  Not Found!';
		   				$html.='</div>';
		   			}
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Route No:</strong> '.$route['route_no'];
		   				$html.='</div>';
		   				$html.='<div class="driver-info-row">';
		   					$html.='<strong>Price :</strong> '.$route['price'];
		   				$html.='</div>';
	   				$html.='</div>';
	   				
	   			$html.='</div>';
	   			$html.='<div class="information-submit-button-div">';
	   				if(empty($changeRoute)){
	   				$html.=CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeSubmit('.$route_id.','.$stoppageId.',"'.$expectedDate.'")')); }
	   				if(!empty($changeRoute)){
	   					$html.=CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeSubmit('.$route_id.','.$stoppageId.',"'.$expectedDate.'","'.$changeRoute.'")')); }
	   			$html.='</div>';
	   		$html.='</div>';
	   		
	   	$html.='</div>';


    			
    		}//Route found
    		else{//Route not found
    			$html.='<div class="accordion accordion-border clearfix">
    								Route Not Found!
    							</div>';
    		}//Route not found
    	} //route id have
    	else{//route id not have
    		$html.='<div class="accordion accordion-border clearfix">
    								Route ID Not Found!
    							</div>';
    	}//route id not have
    	echo $html;
    }
    else{
    	
    	$route=Routes::model()->findByPk($check_route_sql['route_id']);
    	$model=$this->loadModel($check_route_sql['id']);
    	
    	$stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='".$check_route_sql['route_id']."' ORDER BY created_time";
    	$stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();
    	echo "<div class='center s002 '>
	   			<h4 class='heading-custom'>UPDATE YOUR  ROUTE NO: ".$route['route_no']." INFORMATION</h4>	   	
	   		 </div>";
	   
	  echo "<div class='center s002 '>";
		$form=$this->beginWidget('CActiveForm', array(
		   'id'=>'request-update-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('onSubmit'=>'return updateSeatRequest()'),
	)); 			
       		echo "<input type='hidden' name='SeatRequest[id]' id='SeatRequest-id' value='$model->id'>";
		echo "<div class='inner-form'> 
       		<div class='col_half'>
       		<label>Expected Date</label>
       		<div class='input-field first-wrap'>

		    			<div class='icon-wrap'>
			    			<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
				    			<path d='M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z'>				    	
				    			</path>
                			</svg>
						</div>";

					echo $form->textField($model,'expected_date',array('placeholder'=>'Expected Date','id'=>'depart1','class'=>'datepicker')); 
                
     		echo "</div></div>";
     	
     		echo "<div class='col_half'> 
	    		    <label>Stoppage</label>
				<div class='input-field first-wrap'>
		    		<div class='icon-wrap'>
			    		<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
							<path d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z'>						
							</path>
		        		</svg>
					</div>";						
				
				 
           
            echo "<select data-trigger='' name='SeatRequest[stoppage_id]' id='SeatRequest-stoppage-id'>";
            	
            	 foreach($stoppage_rs as $key=>$value)
				   {
	        			if($value['id']==$model->stoppage_id) 
	        				echo "<option value='".$value['id']."'  selected>".$value['stoppage']."</option>";		
	        			else	
	        		echo "<option value='".$value['id']."'>".$value['stoppage']."</option>";	
	        	}
				
			echo "</select>";	
			echo "</div></div></div>";
			echo "<div class='inner-form'> 
       		<div class='col_half'> 
	    		     <label>Status</label>
				<div class='input-field first-wrap'>
		    		<div class='icon-wrap'>
			    		<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24'>
							<path d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z'>						
							</path>
		        		</svg>
					</div>";				
           
            echo "<select data-trigger='' name='SeatRequest[status]' id='SeatRequest-status'>";
            	
            	
	        			if($model->status==1){
	        				echo "<option value='1'  selected>Approved</option>";		
	        				echo "<option value='3'  >Cancel Request</option>";
	        			}
	        			if($model->status==0){
	        				echo "<option value='0'  selected>Pending</option>";       				
	        			}

	        			if($model->status==-1){
	        				echo "<option value='-1'  selected>Cancel</option>";       				
	        				echo "<option value='0'>Approved Request</option>";       				
	        			}	
	        			if($model->status==3){	        				     				
	        				echo "<option value='1'>Cancel Request</option>";       				
	        			}	
	        			if($model->status==2){
	        				echo "<option value='2'  selected>Maternity Leave Use</option>";       				
	        			}        				
	        	
				
			echo "</select>";	
			echo "</div></div>";
     	 echo "<div class='col_half'>
     		 <label>Update</label>
       		<div class='input-field first-wrap'>";
		          echo CHtml::submitButton("Update",array('class'=>'btn-search','id'=>'search-button')); 		
		echo "</div></div></div>";
     
    $this->endWidget();   
 	echo "</div>";
 	if(empty($changeRoute)&&$route_id!=$check_route_sql['route_id']){
	 		echo "<div class='center s002 '>";
	 		echo "<div class='input-field first-wrap'>";
		    echo CHtml::submitButton("Change Route Request",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeInformation('.$route_id.',"'.$stoppageId.'","'.$expectedDate.'","'.$check_route_sql['route_id'].'")')); 
		    echo "</div>";
	 		echo "</div>";
 		}
    	
    }
}
   public function actionUserRouetRquest(){
 
   		$model=new SeatRequest;
		/*$user_pin=$_GET['user_pin'];
		$model->user_pin = $user_pin;
		$model->created_by = $user_pin;

		//$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
		$uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101&fields=StaffName,JobLevel,ProjectName,DesignationName,MobileNo,EmailID,PresentAddress,ProjectID";
        $staffInfo = CJSON::decode(file_get_contents($uri));
    
        $model->user_level = $staffInfo[0]['JobLevel'];
        $model->user_name= $staffInfo[0]['StaffName'];
        $model->email=$staffInfo[0]['EmailID'];
        $model->user_cell=$staffInfo[0]['MobileNo'];
        $model->user_department=$staffInfo[0]['ProjectName'];
        $model->user_designation=$staffInfo[0]['DesignationName'];
        $model->residence_address=$staffInfo[0]['PresentAddress'];
		$model->created_time = date('Y-m-d H:i:s');*/

		$user_pin = Yii::app()->user->username;
		$app_route = Yii::app()->db->createCommand("SELECT r.* FROM tbl_commonfleets as c INNER JOIN tbl_routes as r ON r.route_no = c.present_route WHERE c.user_pin = '".Yii::app()->user->username."' AND  c.application_type <>'Cancel Seat'")->queryRow();
		if($app_route){			
			if($_POST['route_id'] == $app_route['id']){
				echo "Sorry, You Already Use This Route!";
				return;
			}
		}
		$sql = "SELECT * FROM tbl_seat_request WHERE user_pin = '".Yii::app()->user->username."' AND status<>'-1'";		
		if(($_POST['changeRoute']!=1) && (!empty(Yii::app()->db->createCommand($sql." AND  user_status = 0")->queryRow()))) {
			echo "Please, cancel your previous route request.";
				return;
		}
		if(($_POST['changeRoute']==1) && (!empty(Yii::app()->db->createCommand($sql." AND  user_status = 1")->queryRow()))) {
			echo "Please, cancel your previous  change route request.";
				return;
		}		
		$model->user_pin = $user_pin;
		$model->user_name = Yii::app()->user->firstname." ".Yii::app()->user->lastname;
		$model->user_department = Yii::app()->user->department;
		$model->user_level = Yii::app()->user->level;
		$model->user_cell = Yii::app()->user->phone;
		$model->email = Yii::app()->user->email;
		$route_id = $_POST['route_id']; 
		$routes = Routes::model()->findByPk($route_id);
		$model->route_id = $route_id;		
		$model->stoppage_id = $_POST['stoppageId']; 
		$model->expected_date = $_POST['expectedDate'];
		if($_POST['changeRoute']) {
			$model->user_status = 1; 
			$message = "Your change route no:".$routes['route_no']." request send to iFleet management for approved.";
		}
		
		else {	
		$model->user_status = 0; 	
			$message = "Your New route  request for route no: ".$routes['route_no']." send to iFleet management for approved.";
		}
		$model->queue = 0; 
		$model->created_by = $user_pin; 
		$model->created_time = date('Y-m-d H:i:s');       
        $model->user_designation = Yii::app()->user->designation;
        $model->residence_address = Yii::app()->user->address;	 
        $model->action = 0;	
    	if($model->save()){//route id have
    		$sms = SeatRequestAdmin::sendSMS($model->user_cell,$message);    					
			$sub = "iFleet Route Request Status";
			$body = $message;
			//$email = $this->sendMailIcress($model->email,$body,$sub);
			$message = new YiiMailMessage;
			$message->subject = $sub;
	 		$params = array('message'=>$body); ;
	 		$message->view = "_email";
	 		$message->setBody($params, 'text/html');                
     		$message->addTo($model->email);      
     		$message->setFrom(array('imailservice@brac.net' => 'Brac iFleet')); 
     		Yii::app()->mail->send($message);
     		if($model->expected_date<=date("Y-m-d", strtotime("+1 day"))) {
     			$booking_check = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_booking WHERE route_id='".$model->route_id."' AND sale_id=0 AND date_from>='".date("Y-m-d")."' AND date_from<='".date("Y-m-d", strtotime("+1 day"))."' AND status=0 ORDER BY date_from DESC")->queryRow();
     			if($booking_check) {
     				echo "Your request send to addmin successfully. Sorry, You can not use the route before ". $booking_check['date_from'];
     				return;
     			}
     		}
    		echo "Your request send to addmin successfully.";
    		return;
    	}//route id have
    	else{//route id not have
    		$error= $model->getErrors();
    		foreach ($error as $key => $value) {
    			echo $value[0]."</br>";
    		}
    	   		echo "Sorry, Please Try Again!";
    	}

   }
   public function actionRequestList()
	{
		//$dataProvider=new CActiveDataProvider('SeatRequest');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		//echo "string";
		//exit();
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequest']))
			$model->attributes=$_GET['SeatRequest'];

		$this->render('requestList',array(
			'model'=>$model,
		));
	}
	public function actionRouteInformationUpdate()
	{	
		$id=$_POST['requestId'];
		$model=$this->loadModel($id);
		$model->user_status=$_POST['updateRequest'];
		$routes = Routes::model()->findByPk($model->route_id);
		if($model->status==0){
			$model->maternity_leave=0;
		}
		$model->updated_by=Yii::app()->user->username;
		$model->updated_time=date('Y-m-d H:i:s');
		$model->action = 0;	
		if($model->save()) {
			$message = "Your route no: ".$routes['route_no']."  cancel request send to iFleet management.";
			$sms = SeatRequestAdmin::sendSMS($model->user_cell,$message);					
			$sub = "iFleet Route Request Status";
			$body = "Your route no: ".$routes['route_no']." cancel request send to iFleet management.";
			//$email = $this->sendMailIcress($model->email,$body,$sub);	
			$message = new YiiMailMessage;
			$message->subject = $sub;
	 		$params = array('message'=>$body); ;
	 		$message->view = "_email";
	 		$message->setBody($params, 'text/html');                
     		$message->addTo($model->email);      
     		$message->setFrom(array('imailservice@brac.net' => 'Brac iFleet')); 
     		Yii::app()->mail->send($message);		
			echo "Your Route no: ".$routes['route_no']." Cancel Request Send to iFleet Management.";
		}
		else{
			echo "Please, Try Again!";
		}
				
		

		
	}
	public function actionUserRouteRquestUpdate(){
		$id=$_POST['id'];
		$expDate=$_POST['expDate'];
		$seatRequestStoppageId=$_POST['seatRequestStoppageId'];
		$seatRequestStatus=$_POST['seatRequestStatus'];

		$model=$this->loadModel($id);
		$model->expected_date=$expDate;
		$model->stoppage_id=$seatRequestStoppageId;
		$model->status=$seatRequestStatus;
		$model->updated_by=Yii::app()->user->username;
		$model->updated_time=date('Y-m-d H:i:s');		
		if($model->save()){
			echo "1";
		}
		else{
    		$error= $model->getErrors();
    		//var_dump($error);
    		echo "Please, Try Again!";

    		
    	}			
		
	}
	public function actionMaternityUpdated(){
		$maternityId=$_POST['maternityId'];
		$sql_user_seat_request="SELECT * FROM tbl_seat_request WHERE user_pin='".Yii::app()->user->username."' AND status=1 AND queue>=1";
		$rs_user_seat_request=Yii::app()->db->createCommand($sql_user_seat_request)->queryRow();

		$seat_request_id=$rs_user_seat_request['id'];
		$maternity_user_pin=$rs_user_seat_request['user_pin'];
		$updated_time=date('Y-m-d H:i:s');

		$sql_update_user_seat_request="UPDATE tbl_seat_request SET maternity_leave=1 WHERE user_pin='".Yii::app()->user->username."' AND status=1 AND queue>=1";
		$rs_update_user_seat_request=Yii::app()->db->createCommand($sql_update_user_seat_request)->execute();

		$sql_maternity_leave_assign="INSERT INTO tbl_maternity_leave_assign (id,seat_request_id,maternity_user_pin,meternity_request_id,created_by,created_time) VALUES(null,'$seat_request_id','$maternity_user_pin',$maternityId,'$maternity_user_pin','$updated_time')";
		$rs_maternity_leave_assign=Yii::app()->db->createCommand($sql_maternity_leave_assign)->execute();
		if($rs_maternity_leave_assign){
			echo "<h3>Please, Wait For Admin Approval.</h3>";
		}
		else{
			echo "<h3>Please, Try Again!.</h3>";
		}
		
		
	}
 	public function actionDashboardRouteDetail(){
 		$username=Yii::app()->user->username;
 		$sql="SELECT * FROM tbl_seat_request WHERE user_pin='$username ' AND status=1";
 		$rs_sql=Yii::app()->db->createCommand($sql)->queryRow();
 		if($rs_sql){ 		
 			$route_id=$rs_sql['route_id'];	
			//$route=Routes::model()->findByPk($route_id);
			$stoppages=Stoppage::model()->findAllByAttributes(array('route_id'=>$route_id));	
			
			//$vehicle_information=Vehicles::model()->findByPk($route['vehicle_reg_no']);		
			//$driver_information=Drivers::model()->findByPk($vehicle_information['driver_pin']);	
			$sql_info="SELECT * FROM tbl_routes as r INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id";
			$sql_info_rs=Yii::app()->db->createCommand($sql_info)->queryRow();	
 			echo "<div class='center s002 '>
	   			    <h4 class='heading-custom'>ROUTE INFORMATION</h4>	   	
	   		     </div>";
	   		     $html.='<div class="table-responsive">';

						$html.='<table class="table cart">';
							$html.='<thead>';
								$html.='<tr>';
									$html.='<th class="cart-product-price"><strong>Route No</strong></th>';
									$html.='<th class="cart-product-price"><strong>Vehicle No</strong></th>';
									$html.='<th class="cart-product-price"><strong>Vehicle Type</strong></th>';	
									$html.='<th class="cart-product-price"><strong>Status</strong></th>';	
									$html.='<th class="cart-product-price"><strong>Queue</strong></th>';	
								$html.='</tr>';
							$html.='</thead>';
							$html.='<tbody>';
							if($sql_info_rs){													
								$html.='<tr class="cart_item">';
									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['route_no'];
									$html.='</th >';

									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['reg_no'];
									$html.='</th >';
									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['type'];
									$html.='</th >';									
									$html.='<th  class="cart-product-price">';
										$html.= SeatRequest::status($rs_sql['status']);
									$html.='</th >';
									$html.='<th  class="cart-product-price">';
										$html.= SeatRequest::queueCheck($rs_sql['queue'],$rs_sql['status']);
									$html.='</th >';

								$html.='</tr>';
								
							}
							else{
								$html.='<tr class="cart_item">';
								$html.='<th colspan="4"  class="cart-product-price">';
								$html.= 'Stoppage Not Found';
								$html.='</th >';
								$html.='</tr>';

							}						
							$html.='</tbody>';

						$html.='</table>';

					$html.='</div>';
					$html.='<div class="center s002 " style="margin-top:20px">';  

   							$html.='<h4 class="heading-custom">Driver Information</h4>';
   	
   					$html.='</div>';  	

	   		     $html.='<div class="table-responsive">';

						$html.='<table class="table cart">';
							$html.='<thead>';
								$html.='<tr>';									
									$html.='<th class="cart-product-price"><strong>Driver Pin</strong></th>';
									$html.='<th class="cart-product-price"><strong>Photo</strong></th>';
									$html.='<th class="cart-product-price"><strong>Driver Name</strong></th>';
									$html.='<th class="cart-product-price"><strong>Driver Cell</strong></th>';
									$html.='<th class="cart-product-price"><strong>Blood Group</strong></th>';
								$html.='</tr>';
							$html.='</thead>';
							$html.='<tbody>';
							if($sql_info_rs){													
								$html.='<tr class="cart_item">';					

									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['driver_pin'];
									$html.='</th >';
									$image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/".$sql_info_rs['driver_pin'].".png";

                                    if (!file_exists($image_url)) {
                                    $image_save='../photo/'.$sql_info_rs['driver_pin'].".png";
                                    $base64string=file_get_contents("http://api.brac.net/v1/staffs/".$sql_info_rs['driver_pin']."/photo?Key=960d5569-a088-4e97-91bf-42b6e5b6d101");
        							file_put_contents($image_save, base64_decode($base64string));                             	
                                        //$image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/"."person1_web.jpg";
                                    }
									$html.='<th  class="cart-product-price">';
										$html.= '<img src="'.$image_url.'" style="" class="img-responsive" alt="User image">';
									$html.='</th >';
									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['name'];
									$html.='</th >';
									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['phone'];
									$html.='</th >';
									$html.='<th  class="cart-product-price">';
										$html.= $sql_info_rs['blood_group'];
									$html.='</th >';
									

								$html.='</tr>';
								
							}
							else{
								$html.='<tr class="cart_item">';
								$html.='<th colspan="4"  class="cart-product-price">';
								$html.= 'Stoppage Not Found';
								$html.='</th >';
								$html.='</tr>';

							}						
							$html.='</tbody>';

						$html.='</table>';

					$html.='</div>';
					
					$html.='<div class="center s002 " style="margin-top:20px">';  

   							$html.='<h4 class="heading-custom">Route\'s  Stoppages List</h4>';
   	
   					$html.='</div>';  					
   						$html.='<div class="table-responsive">';
						$html.='<table class="table cart">';
							$html.='<thead>';
								$html.='<tr>';
									$html.='<th class="cart-product-price"><strong>Stoppage</strong></th>';
									$html.='<th class="cart-product-price"><strong>Time</strong></th>';
									
								$html.='</tr>';
							$html.='</thead>';
							$html.='<tbody>';
							if(count($stoppages)>0){
								foreach ($stoppages as $key => $value) {
									$html.='<tr class="cart_item">';
									if($rs_sql['stoppage_id']==$value['id']){
									$html.='<th  class="cart-product-price" style="color:#5d9174">';
									$html.= $value['stoppage'];
									$html.='</th >';
									$html.='<th  class="cart-product-price" style="color:#5d9174">';
									$html.= $value['pickup_time'];
									$html.='</th >';
									}
									else{
									$html.='<th  class="cart-product-price">';
									$html.= $value['stoppage'];
									$html.='</th >';
									$html.='<th  class="cart-product-price">';
									$html.= $value['pickup_time'];
									$html.='</th >';
									}							
								
									

								$html.='</tr>';
								}
							}
							else{
								$html.='<tr class="cart_item">';
								$html.='<th colspan="2"  class="cart-product-price">';
								$html.= 'Stoppage Not Found';
								$html.='</th >';
								$html.='</tr>';

							}						
							$html.='</tbody>';

						$html.='</table>';

					$html.='</div>';
   					
					echo $html;
					
 		}
 		else{
 		echo "<h3>You have no route</h3>";
 		}
 	

 	}
 	public function actionUserMarketPlaceSummery(){
 		$username=Yii::app()->user->username;
 		$sql_seal="SELECT * FROM tbl_marketplace WHERE created_by='$username' AND status=1";
 		$rs_sql_seal=Yii::app()->db->createCommand($sql_seal)->queryAll();
 		$sql_book="SELECT * FROM tbl_booking WHERE created_by='$username' AND status=1";
 		$rs_sql_book=Yii::app()->db->createCommand($sql_book)->queryAll();
 		echo $this->renderPartial('userMarketPlaceSummery', array(
 			'rs_sql_seal'=>$rs_sql_seal,
 			'rs_sql_book'=>$rs_sql_book,
 		));

 	}
 	public function actionFindRoute(){

		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		$model->attributes=$_POST['SeatRequest'];
		/*if(isset($_POST['SeatRequest'])){
			$sql="SELECT * FROM tbl_stoppage WHERE ";
			if(!empty($_POST['SeatRequest']['from'])&&!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."' OR stoppage='".$_POST['SeatRequest']['to']."'";
			else if(!empty($_POST['SeatRequest']['from']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."'";
			else if(!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['to']."'";
			else {
				$sql="ALL";
			}
			$route_id="";
			$sql_route="SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id";
			$sql_vehicle_type=0;
			if(!empty($_POST['SeatRequest']['vehicle_type_id'])){
				$sql_route.=" WHERE v.vehicletype_id='".$_POST['SeatRequest']['vehicle_type_id']."'";
				$sql_vehicle_type=1;
			}
//			echo $sql.'<br>';
			if($sql!="ALL"){
				$route_id_rs=Yii::app()->db->createCommand($sql)->queryAll();
				if(count($route_id_rs)>0){
					foreach ($route_id_rs as $key => $value) {
						$route_id.=$value['route_id'].",";
					}
					
				}
				$route_id=rtrim($route_id,',');
				if(strlen($route_id) > 0){
                    if($sql_vehicle_type==0)
                        $sql_route.=" WHERE r.id IN($route_id)";
                    else
                        $sql_route.=" AND r.id IN($route_id)";
                }
			}
//            echo $sql_route.'<br>';
			$routes_rs=Yii::app()->db->createCommand($sql_route)->queryAll();

//			die;
			$this->render('findRoute',array(
			'model'=>$model,
			'routes_rs'=>$routes_rs,			
			));
			return;
			
		}*/		
		$routes_rs = Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id")->queryAll();
		/*$use_route= Yii::app()->db->createCommand("SELECT * FROM  tbl_commonfleets as c INNER JOIN tbl_seat_request as s on c.user_pin=s.user_pin WHERE c.application_type<>'Cancel Seat' AND s.status=1 AND s.queue=-1 AND  c.user_pin='".Yii::app()->user->name."'")->queryAll();*/
		$use_route= Yii::app()->db->createCommand("SELECT c.*,r.id as rid FROM  tbl_commonfleets as c INNER JOIN  tbl_routes as r ON r.route_no=c.present_route WHERE c.application_type<>'Cancel Seat' AND c.user_pin='".Yii::app()->user->username."'")->queryRow();
		//var_dump($use_route);exit();
		
		//$routes_rs = $this->routeListView($routes_rs);
		//var_dump($use_route);exit();
		$this->render('findRoute',array(
			'model' => $model,
			'routes_rs' => $routes_rs,
			'use_route'=>$use_route
		));

 	}

 	public function actionFindRouteList(){

 		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		$model->attributes=$_POST['SeatRequest'];
		if(isset($_POST['SeatRequest'])){
			$sql="SELECT * FROM tbl_stoppage WHERE ";
			if(!empty($_POST['SeatRequest']['from'])&&!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."' OR stoppage='".$_POST['SeatRequest']['to']."'";
			else if(!empty($_POST['SeatRequest']['from']))
				$sql.="stoppage ='".$_POST['SeatRequest']['from']."'";
			else if(!empty($_POST['SeatRequest']['to']))
				$sql.="stoppage ='".$_POST['SeatRequest']['to']."'";
			else {
				$sql="ALL";
			}
			$route_id="";
			$sql_route="SELECT r.*, vt.type FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id";
			$sql_vehicle_type=0;
			if(!empty($_POST['SeatRequest']['vehicle_type_id'])){
				$sql_route.=" WHERE v.vehicletype_id='".$_POST['SeatRequest']['vehicle_type_id']."'";
				$sql_vehicle_type=1;
			}
//			echo $sql.'<br>';
			if($sql!="ALL"){
				$route_id_rs=Yii::app()->db->createCommand($sql)->queryAll();
				if(count($route_id_rs)>0){
					foreach ($route_id_rs as $key => $value) {
						$route_id.=$value['route_id'].",";
					}
					
				}
				$route_id=rtrim($route_id,',');
				if(strlen($route_id) > 0){
                    if($sql_vehicle_type==0)
                        $sql_route.=" WHERE r.id IN($route_id)";
                    else
                        $sql_route.=" AND r.id IN($route_id)";
                }
			}
//            echo $sql_route.'<br>';
			$routes_rs=Yii::app()->db->createCommand($sql_route)->queryAll();

//			die;
			
			$this->renderPartial('findRouteList',array(
			'model'=>$model,
			'routes_rs'=>$routes_rs,			
			));
			return;
			
		}
 	}
 	private function routeListView($routes_rs){

    $view='<div id="route-list">
   	<div class="center s002" style="margin-top:13px">  

   		<h4 class="heading-custom">ROUTES LIST</h4>
   	
   	</div>
  
    <div class="table-responsive bottommargin">

						<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-price">Route No</th>
									<th class="cart-product-price">Route Detail</th>
									<th class="cart-product-price">Vehicle Type</th>
									<th class="cart-product-price">Total Seat</th>
									<th class="cart-product-price">Available Seat</th>
									<th class="cart-product-price">Waiting</th>
									<th class="cart-product-price">Leave</th>
									<th class="cart-product-price">Price</th>									
									<th class="cart-product-price"></th>
								</tr>
							</thead>
							<tbody>';
								
								if(count($routes_rs)>0){
								foreach($routes_rs as $key=>$value){						
									$view.='<tr class="cart_item">
									<td  class="cart-product-price">';
										 $view.= $value['route_no'];
									$view.='</td >
									<td  class="cart-product-price">';						
    									$stoppage_rs = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$value['id']."' ORDER BY pickup_time")->queryAll();
    									$stoppage_id = 0;
    									if(count($stoppage_rs)>0) {
											foreach ($stoppage_rs as $key1 => $value_stoppage) {
												if($value_stoppage['stoppage']==$model->from || $value_stoppage['stoppage']==$model->to) {
													$view.="<span style='color:#47ae4b'>".$value_stoppage['stoppage']."</span>";
														$stoppage_id=$value_stoppage['id'];
													}
												else {
													$view.= $value_stoppage['stoppage'];
													}

												if(count($stoppage_rs)>$key1+1)$view.= "-";
												} 
										    }
										
									$view.='</td >
									<td  class="cart-product-price">';
										$view.= $value['type'];
									$view.='</td >
									<td  class="cart-product-price">';
										$view.=$total_seat=$value['actual_seat'];
									$view.='</td >
									<td  class="cart-product-price">';					
    									$borderPass=Routes::borderPass($value['route_no']);
    									$view.= $av_seat=$total_seat-$borderPass;		
									$view.='</td >
									<td  class="cart-product-price">';					
									$queue_rs=Yii::app()->db->createCommand("SELECT max(queue) as maxqueue FROM tbl_seat_request WHERE status=1 AND route_id='".$value['id']."' AND queue>=0")->queryRow();
									if($av_seat>0) {
										$view.= 0;
									}
									else {
										$view.= $queue_rs['maxqueue']+1;
									}										
									$view.='</td >
									<td  class="cart-product-price">
										<a href="#"  >';
										$view.= Routes::onMatLeave($value['route_no']);
										$view.='</a>
									</td >
									<td  class="cart-product-price">';
										$view.= $value['price'];
									$view.='</td >
									<td  class="cart-product-price">
										<div class="input-field first-wrap">';
		    							if(isset($_GET['id']) && $_GET['id']==1){
		    								$view.= CHtml::submitButton("Continue",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeInformation('.$value["id"].',"'.$stoppage_id.'","'.$model->expected_date.'",1)'));	
		    							}
		    							else
		    							$view.= CHtml::submitButton("Continue",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeInformation('.$value["id"].',"'.$stoppage_id.'","'.$model->expected_date.'",0)'));
										$view.='</div>
									</td >
								</tr>';
									} 
								}
								else{
								$view.='<tr>
									<td  colspan="9" class="cart-product-price">
										No Route Found!
									</td >
								</tr>';
								 } 							
								
							$view.='</tbody>

						</table>

					</div>
			
		</div>';
		return $view;
 	
	}
	public function actionPointSearch() {
		$model=new SeatRequest('search');
		$model->unsetAttributes();  // clear any default values
		//$model->attributes=$_POST['SeatRequest'];
		$rest_sql="INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id";
		$routes_rs = !empty($_POST['p']) ? 
		Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_stoppage as s INNER JOIN tbl_routes as r ON s.route_id=r.id ".$rest_sql."  WHERE stoppage LIKE '%".$_POST['p']."%' GROUP BY r.id")->queryAll() : Yii::app()->db->createCommand("SELECT r.*, vt.type FROM tbl_routes as r ".$rest_sql)->queryAll();
		$this->renderPartial('findRouteList',array(
			'model'=>$model,
			'routes_rs'=>$routes_rs,
			'f'=>$_POST['f'],			
			));
			return;
		//var_dump($routes_rs);
		//$routes_rs = 
		//$routes_rs = $this->routeListView($routes_rs);
	}
	public function actionRouteSubmit(){
		$model=new SeatRequest('search');
		$model->unsetAttributes();		
		$this->renderPartial('_routeSubmit',array(
			'model'=>$model,
			'rid'=>$_POST['rid'],
			'f'=>$_POST['f'],			
			));
			return;
	}
	public function actionRequestDetail(){
			$id=$_POST['id'];
			//echo "SELECT t.*,d.phone,d.name,v.reg_no, d.pin tbl_seat_request as t INNER JOIN tbl_routes as r ON r.id=t.route_id INNER JOIN tbl_vehicles as v ON v.reg_no=r.vehicle_reg_no INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin WHERE t.id='".$id."'";
			//return;
			$detail =Yii::app()->db->createCommand("SELECT t.*,r.route_no,r.price,r.route_detail,d.phone,d.name,v.reg_no,d.pin, vt.type FROM tbl_seat_request as t INNER JOIN tbl_routes as r ON r.id=t.route_id INNER JOIN tbl_vehicles as v ON v.reg_no=r.vehicle_reg_no INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id WHERE t.id='".$id."'")->queryRow();
			
		
		$this->renderPartial('_routeDetail',array(
				'detail'=>$detail,	
			));
			return;
	}
	
public function sendMailIcress($email,$body,$sub) {
        try {
            $soapClient = new SoapClient("http://imail.brac.net:8080/isoap.comm.imail/EmailWS?wsdl");
            $job = new jobs;
            //$job->subject='Transport Requisition Notification';
            $job->jobContentType = 'html';
            $job->fromAddress = 'ifleet@brac.net';
            $job->udValue1 = 'iFleet';
            $job->requester = 'iFleet';
            //   $job->jobRecipients[0]=new jobRecipients;
            //   $job->jobRecipients[0]->recipientEmail="shouman.das@gmail.com";
            $job->jobRecipients[0] = new jobRecipients;
            //$job->jobRecipients[0]->recipientEmail = $model->email;
            $job->jobRecipients[0]->recipientEmail = $email;
            //$model1 = $hrdata->getHrUser($model->pin);
            //$rec_mail = $model1[0]['Email'];           
            $job->subject = $sub;
            $job->body = nl2br($body);
            $jobs = array('jobs' => $job);
            $send_email = $soapClient->__call('sendEmail', array($jobs));
            $send_email->return->status;
            
        } catch (SoapFault $fault) {
           $error = 1;
           // print($fault->faultcode . "-" . $fault->faultstring);
        }
    }
}

class jobs
{

    public $appUserId; // string
    public $attachments; // attachment
    public $bcc; // string
    public $body; // string
    public $caption; // string
    public $cc; // string
    public $complete; // boolean
    public $feedbackDate; // dateTime
    public $feedbackEmail; // string
    public $feedbackName; // string
    public $feedbackSent; // boolean
    public $fromAddress; // string
    public $fromText; // string
    public $gateway; // string
    public $jobContentType; // string
    public $jobId; // long
    public $jobRecipients; // jobRecipients
    public $mode; // string
    public $numberOfItem; // int
    public $numberOfItemFailed; // int
    public $numberOfItemSent; // int
    public $priority; // string
    public $requester; // string
    public $status; // string
    public $subject; // string
    public $toAddress; // string
    public $toText; // string
    public $udValue1; // string
    public $udValue2; // string
    public $udValue3; // string
    public $udValue4; // string
    public $udValue5; // string
    public $udValue6; // string
    public $udValue7; // string
    public $vtemplate; // string

}

class jobRecipients
{

    public $failCount; // int
    public $image; // base64Binary
    public $job; // jobs
    public $jobDetailId; // long
    public $recipientEmail; // string
    public $sent; // boolean
    public $sentDate; // dateTime
    public $toText; // string

}