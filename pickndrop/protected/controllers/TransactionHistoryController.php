<?php

class TransactionHistoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','excel'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','userBlance','createData'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TransactionHistory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TransactionHistory']))
		{
			$model->attributes=$_POST['TransactionHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TransactionHistory']))
		{
			$model->attributes=$_POST['TransactionHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TransactionHistory');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TransactionHistory('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TransactionHistory']))
			$model->attributes=$_GET['TransactionHistory'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=TransactionHistory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='transaction-history-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionUserBlance() {
		$model = new TransactionHistory;
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TransactionHistory'])) {	
			$model->attributes=$_GET['TransactionHistory'];			
		}
		$this->render('user_blance',array(
			'model'=>$model,
		)); 
    }
    public function actionCreateData() {
    	$model = new TransactionHistory;      	  	
		if(isset($_POST['TransactionHistory'])) {
			$model->attributes = $_POST['TransactionHistory'];
			$model->created_time = date('Y-m-d H:i:s');
			$model->created_by = Yii::app()->user->username;
			$model->user_pin = Yii::app()->user->username;
			if(empty($model->payment_method))
				$model->addError('payment_method',"Payment method cann't be empty.");
			if(empty($model->transaction_id)) {
				$model->addError('transaction_id',"Transaction Id cann't be empty.");
			}
			else {				
				if(!empty( Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE transaction_id='".$model->transaction_id."'")->queryRow())) {
					$model->addError('transaction_id',"Transaction Id allready used.");
				}
			}
			if(empty($model->credit))
			$model->addError('credit',"Amount cann't be empty.");
			if($model->getErrors()){
				$this->renderPartial('balance_form', array('model'=>$model));
				return;
			}
			$pre_last_r = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin='".$model->user_pin."' order by id DESC")->queryRow();
			if(!empty($pre_last_r)){
				$model->balance = $pre_last_r['balance'] + $model->credit;
			}
			else {
				$model->balance = $model->credit;
			}
			if($model->save())
				echo 1;
			else
			$this->renderPartial('balance_form', array('model'=>$model));
		}		
    }
    public function actionExcel()
	{
		$model = new TransactionHistory ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Transaction History Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Transaction_History_Information',
            //'stream'=>false,
           'columns'=>array(
           		
			array(
                'name' => 'user_pin',
                'type' => 'raw',
                'value' => '$data->user_pin == "0" ? "iFleet" : $data->user_pin ',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
       
            array(
                'name' => 'sale_id',
                'type' => 'raw',
                'value' => '$data->sale_id==0 ?"No" : "Yes"',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'booking_id',
                'type' => 'raw',
                'value' => '$data->booking_id==0 ?"No" : "Yes"',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'credit',
                'type' => 'raw',
                'value' => '$data->credit',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'debit',
                'type' => 'raw',
                'value' => '$data->debit',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),     

            array(
                'name' => 'payment_method',
                'type' => 'raw',
                'value' => '$data->payment_method',                
                'htmlOptions' => array('class' => 'cart-product-price', 'title' => 'Route Detail'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'transaction_id',
                'type' => 'raw',
                'value' => '$data->transaction_id',                
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'balance',
                'type' => 'raw',
                'value' => '$data->balance',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(  
                    'name'=>'created_time', 
                    'type' => 'raw',            
                    'value'=>'$data->created_time',                                           
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
			),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
}
