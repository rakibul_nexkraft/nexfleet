<?php
date_default_timezone_set("Asia/Dhaka");
//Yii::app()->theme = 'pick';
class SeatRequestAdminController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	//public $layout='//layouts/columnpick';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','zone','callHrUser','excel','routeSummery'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','statusChange','route'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SeatRequestAdmin;
		$model->created_by = Yii::app()->user->username;           
        $model->created_time = date("Y-m-d H:i:s");

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequestAdmin']))
		{
			$model->attributes=$_POST['SeatRequestAdmin'];
			if($model->status==1){//For Approved
				$route=Routes::model()->findByPk($model->route_id);
				$route_no=$route['route_no'];
				$vehicle_reg_no=$route['vehicle_reg_no'];
				$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);				
				$actual_seat=$route['actual_seat'];
				$occupied_seat=Routes::model()->borderPass($route_no);
				$av_seat=$actual_seat-$occupied_seat;
				if($av_seat>0){// seat available
					$this->assign($model);
					$model->queue=-1;
				}

				else{// seat not available
					$sql = "SELECT max(queue) as maxq FROM tbl_seat_request WHERE route_id='".$model->route_id."' GROUP BY route_id";
					$sql_rs = Yii::app()->db->createCommand($sql)->queryRow(); 
					$model->queue = $sql_rs['maxq']+1;
				}
        	
        	}
        	$model->action = $model->status;
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$previous_status=$model->status;
		$model->updated_time=date("Y-m-d H:i:s");
		$model->updated_by=Yii::app()->user->username; 
		$route=Routes::model()->findByPk($model->route_id);
		$route_no=$route['route_no'];
		$model->zone=$route['zone_id'];
		$previous_status=$model->status;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SeatRequestAdmin']))
		{
			$model->attributes=$_POST['SeatRequestAdmin'];

			if($previous_status==4 && $model->status==1){
			$sql="SELECT * FROM tbl_seat_request WHERE  status=1  AND user_pin='".$model->user_pin."'";
			$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
			if($sql_rs){
				$sql_update="UPDATE tbl_seat_request SET status=-1,queue=0,updated_time='$updated_time' WHERE id='".$sql_rs['id']."'";
				$rs_sql_update=Yii::app()->db->createCommand($sql_update)->execute();
				}
			}

			if($model->status==1 && $model->queue!=-1){ //Approved and not assign				
				$vehicle_reg_no=$route['vehicle_reg_no'];
				$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);
				
				$actual_seat=$route['actual_seat'];
				$occupied_seat=Routes::model()->borderPass($route_no);
				$av_seat=$actual_seat-$occupied_seat;
				

				if($av_seat>0){
					$this->assign($model);
					$model->queue=-1;
				}

				else{
					$sql="SELECT max(queue) as maxq FROM tbl_seat_request WHERE route_id='".$model->route_id."' GROUP BY route_id";
					$sql_rs=Yii::app()->db->createCommand($sql)->queryRow(); 
					$model->queue=$sql_rs['maxq']+1;
				}

				
			}
			else if($model->status!=1 && $model->queue==-1){// change status approved to other, but previous assign
				//$sql="DELETE FROM tbl_commonfleets WHERE user_pin='".$model->user_pin."'";
				$sql="UPDATE tbl_commonfleets SET application_type='Cancel Seat',present_route=NULL WHERE user_pin='".$model->user_pin."'";
				$sql_rs=Yii::app()->db->createCommand($sql)->execute(); //Delete assign
				$model->queue=0; //not queue and not assign 
				$this->updateWaitingList($model);

			}
			else if($model->status!=1 && $model->queue>0){
				$q=$model->queue;
				$model->queue=0;
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>$q order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){
					
					foreach ($sql_rs as $key => $value) {
						
					 //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);

						
						
					}
				}

			}
			else{//same as request
				$model->queue=0;
			}
			$model->action = $model->status;
			if($model->save()){
				$routes = Routes::model()->findByPk($model->route_id);
				$massage = $this->adminMessage($routes,$model);
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function assign($model){
		$this->assignRequest($model);
		$this->assignToPickNDrop($model);
		
	}
	public function assignToPickNDrop($model){
		$route=Routes::model()->findByPk($model->route_id);
		$vehicle_reg_no=$route['vehicle_reg_no'];
		$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);
		$route_no=$route['route_no'];
		$stoppage=Stoppage::model()->findByPk($model->stoppage_id);
		$model1=new Commonfleets;
		$model1->user_pin=$model->user_pin;
		$model1->user_name=$model->user_name;
		$model1->user_level=$model->user_level;
		$model1->user_dept=$model->user_department;
		$model1->user_designation=$model->user_designation;
		$model1->user_cell=(int) trim($model->user_cell);
		$model1->user_email=trim($model->email);
		$model1->telephone_ext=0;
		$model1->application_type="Pick and Drop";					
		$model1->residence_address=$model->residence_address;
		$model1->recommended_by='';
		$model1->recommended_by_desig='';
		$model1->previous_route='';
		$model1->preferred_route='';
		$model1->present_route=$route_no;
		$model1->expected_date=$model->expected_date;
		$model1->mt_leave_from="0000-00-00";
		$model1->mt_leave_to="0000-00-00";
		$model1->vehicle_reg_no=$vehicle_reg_no;
		$model1->vehicletype_id=$vehicle['vehicletype_id'];
		$model1->user_remarks=$model->remarks;
		$model1->fleet_remarks='';
		$model1->created_by=Yii::app()->user->username;
		$model1->created_time=date("Y-m-d H:i:s");
		$model1->approve_status="Approve";
		$model1->stoppage_point=$stoppage['stoppage'];
		$model1->pick_up_time=$stoppage['pickup_time'];					
		$model1->active=0;		
		$model1->save();

	}	

	public function assignRequest($model){
		$sql="DELETE FROM tbl_commonfleets WHERE user_pin='".$model->user_pin."'";
		$sql_rs=Yii::app()->db->createCommand($sql)->execute();
		
		$sql="UPDATE  tbl_seat_request SET status=-1, queue=0 WHERE user_pin='".$model->user_pin."' AND status=1 AND queue=-1";
		$sql_rs_seat=Yii::app()->db->createCommand($sql)->execute();	 
		
		
	}
	public function updateWaitingList($model){
		$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>0 order by queue ASC";
		$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
			if(count($sql_rs)>0){
				$i=0;
				foreach ($sql_rs as $key => $value) {
					if($i==0){// first queue assign
						$model_assign=$this->loadModel($value['id']);					
						$model_assign->updated_time=date("Y-m-d H:i:s");
						$model_assign->updated_by=Yii::app()->user->username; 
						$this->assign($model_assign);
						$model_assign->queue=-1;
						$model_assign->save();
						unset($model_assign);
						}
						else{ //update queue
							$model_update=$this->loadModel($value['id']);				
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;		
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);

						}
						$i++;
					}
				}
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{ 
		//$dataProvider=new CActiveDataProvider('SeatRequestAdmin');
		//$this->render('index',array(
			//'dataProvider'=>$dataProvider,
		//));
		$model=new SeatRequestAdmin('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequestAdmin']))
			$model->attributes=$_GET['SeatRequestAdmin'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SeatRequestAdmin('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SeatRequestAdmin']))
			$model->attributes=$_GET['SeatRequestAdmin'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=SeatRequestAdmin::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='seat-request-admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionCallHrUser()
    {
    	$user_pin = $_REQUEST['SeatRequestAdmin']['user_pin'];
    	


        $uri = "http://api.brac.net/v1/staffs/".$user_pin."?key=960d5569-a088-4e97-91bf-42b6e5b6d101";
        

        $staffInfo = CJSON::decode(file_get_contents($uri)); 
        

        echo CJSON::encode($staffInfo[0]);die;
       
    }
	public function actionZone(){
		$id=$_POST['id'];
		$sql="SELECT id,route_no,route_detail from tbl_routes where zone_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		//var_dump($aql_rs);exit();
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}
	}
	public function actionStatusChange(){
		$id = $_POST['id'];
		$value = $_POST['value'];
		
		$model=$this->loadModel($id);
		$updated_time=date("Y-m-d H:i:s");
		$updated_by=Yii::app()->user->username; 
		
		

		if($model->status==4 && $_POST['value']==1){
			$sql="SELECT * FROM tbl_seat_request WHERE  status=1  AND user_pin='".$model->user_pin."'";
			$sql_rs=Yii::app()->db->createCommand($sql)->queryRow();
			if($sql_rs){
				$sql_update="UPDATE tbl_seat_request SET status=-1,queue=0,updated_time='$updated_time' WHERE id='".$sql_rs['id']."'";
				$rs_sql_update=Yii::app()->db->createCommand($sql_update)->execute();
			}
		}
		
		$model->status=$_POST['value'];
		if($model->status==1 && $model->queue!=-1){ //Approved and not assign
				$route=Routes::model()->findByPk($model->route_id);
				$vehicle_reg_no=$route['vehicle_reg_no'];
				$vehicle=Vehicles::model()->findByPk($vehicle_reg_no);
				$route_no=$route['route_no'];
				$actual_seat=$route['actual_seat'];
				$occupied_seat=Routes::model()->borderPass($route_no);
				$av_seat=$actual_seat-$occupied_seat;
				

				if($av_seat>0){
					$this->assign($model);
					$model->queue=-1;
				}

				else{
					$sql="SELECT max(queue) as maxq FROM tbl_seat_request WHERE route_id='".$model->route_id."' GROUP BY route_id";
					$sql_rs=Yii::app()->db->createCommand($sql)->queryRow(); 
					$model->queue=$sql_rs['maxq']+1;
				}

				
		}
		else if($model->status!=1 && $model->queue==-1){// change status approved to other, but previous assign
				//$sql="DELETE FROM tbl_commonfleets WHERE user_pin='".$model->user_pin."'";
				$sql="UPDATE tbl_commonfleets SET application_type='Cancel Seat',present_route=NULL  WHERE user_pin='".$model->user_pin."'";
				$sql_rs=Yii::app()->db->createCommand($sql)->execute(); //Delete assign
				$model->queue=0; //not queue and not assign 
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>0 order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){
					$i=0;
					foreach ($sql_rs as $key => $value) {
						if($i==0){// first queue assign
							$model_assign=$this->loadModel($value['id']);							
							$model_assign->updated_time=date("Y-m-d H:i:s");
							$model_assign->updated_by=Yii::app()->user->username; 
							$this->assign($model_assign);
							$model_assign->queue=-1;
							$model_assign->save();
							unset($model_assign);
						}
						else{ //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);

						}
						$i++;
					}
				}

		}
		else if($model->status!=1 && $model->queue>0){
				$q=$model->queue;
				$model->queue=0;
				$sql="SELECT * FROM tbl_seat_request WHERE route_id='".$model->route_id."' AND status=1 AND queue>$q order by queue ASC";
				$sql_rs=Yii::app()->db->createCommand($sql)->queryAll();// Same route Approved queue list
				if(count($sql_rs)>0){					
					foreach ($sql_rs as $key => $value) {						
					 //update queue
							$model_update=$this->loadModel($value['id']);							
							$model_update->updated_time=date("Y-m-d H:i:s");
							$model_update->updated_by=Yii::app()->user->username;							
							$model_update->queue=$value['queue']-1;
							$model_update->save();
							unset($model_update);					
						
					}
				}

			}
		else{//same as request
				$model->queue=0;
		}

		$model->updated_time=date("Y-m-d H:i:s");
		$model->updated_by=Yii::app()->user->username; 

		//$sql="UPDATE tbl_seat_request SET status=$value,updated_time='$updated_time',updated_by='$updated_by'  WHERE id=$id";
		
		//$sql_rs=Yii::app()->db->createCommand($sql)->execute();
		$model->action = $model->status;
		if($model->save()){
			$routes = Routes::model()->findByPk($model->route_id);
			$massage = $this->adminMessage($routes,$model);
			
			echo "Data save successfully";
		}
		else{
			echo "Try again!";
		}
	}
private function adminMessage($routes,$model) {
	if($model->status==0){
		$message = " Your route no is ".$routes['route_no'].". iFleet management has changed your route status to pending";
	}
	elseif($model->status==1 && $model->queue==-1){
		$message = " Your route no is ".$routes['route_no'].". iFleet management has approved";
	}
	elseif($model->status==1 && $model->queue>0){
		$message = " Your route no is ".$routes['route_no'].". iFleet management has approved and your queue no is ".$model->queue;
	}
	elseif($model->status==3 ){
		$message = " Your route no is ".$routes['route_no'].". iFleet management has changed your route status to cancel request";
	}
	elseif($model->status==4 ){
		$message = " Your route no is ".$routes['route_no'].". iFleet management has changed your route status to change request";
	}
	elseif($model->status==-1 ){
		$message = " Your route no is ".$routes['route_no'].". iFleet management has canceled your route";
	}
	else{
		$message ="";
	}
	$sms = SeatRequestAdmin::sendSMS($model->user_cell,$message);					
	$sub = "iFleet Management Changed Your Route Request Status";
	$body = $message;
	//$email = $this->sendMailIcress($model->email,$body,$sub);
	 $message = new YiiMailMessage;
	 $message->subject    = $sub;
	 $params = array('message'=>$body); ;
	 $message->view = "_email";
	 $message->setBody($params, 'text/html');                
     $message->addTo($model->email);

      //$message->from = 'imailservice@brac.net,"join"';
     $message->setFrom(array('imailservice@brac.net' => 'Brac iFleet')); 
     Yii::app()->mail->send($message);

}
public function actionRoute(){
		$id=$_POST['id']; 
		$sql="SELECT id,pickup_time,stoppage from tbl_stoppage where route_id='".$id."'";
		$aql_rs=Yii::app()->db->createCommand($sql)->queryAll();
		
		
		if(count($aql_rs)>0){
			echo $route=json_encode($aql_rs);
		}
		else{
			$error['error']="Not Found!";
			echo $error=json_encode($error);

		}

	}
public function actionExcel()
	{
		$model = new SeatRequestAdmin ('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['criteria']))
            $model->attributes = $_GET['criteria'];

        $this->widget('ext.phpexcel.EExcelView', array(
            'dataProvider' => $model->search(),
            'title' => 'iFleet Seat Request Information',
            //'autoWidth'=>true,
            'grid_mode' => 'export',
            'exportType' => 'Excel2007',
            'filename' => 'iFleet_Seat_Request_Information',
            //'stream'=>false,
           'columns'=>array(
           		
				'id',	
				'user_pin',
				'user_name',
				'user_department',	
				'user_level',	
				'user_cell',
				'email',	
				array(	
							'name'=>'route_id',	
							'type' => 'raw',			
							'value'=>'SeatRequest::routeDetail($data->route_id,3)',	
							
						),
				'expected_date',		
	
		array(	
					'name'=>'queue',	
					'type' => 'raw',			
					'value'=>'$data->queueCheck($data->queue,$data->status)',
					
				),
		array(	
					'name'=>'status',	
					'type' => 'raw',			
					'value'=>'$data->status==0?"Pending":($data->status==1?"Approved":($data->status==3?"Cancel Request":($data->status==-1?"Cancel":($data->status==4?"Route Change Request":"Not Found"))))',
					
				),
		
			),
        ));
        Yii::app()->end();
        $this->endWidget();
	}
	public function actionRouteSummery(){
		$to_day=date("Y-m-d");
		$route_no=$_POST['route'];
		$route=Routes::model()->findByAttributes(array('route_no'=>$route_no));
		$id=$route['id'];
		
		$sql_to_day_new_seat_request="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=0  AND DATE(created_time)='$to_day'";
		$rs_to_day_new_seat_request=Yii::app()->db->createCommand($sql_to_day_new_seat_request)->queryRow();
		
		$sql_previous_new_seat_request="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=0  AND DATE(created_time)<>'$to_day'";
		$rs_previous_new_seat_request=Yii::app()->db->createCommand($sql_previous_new_seat_request)->queryRow();

		$sql_to_day_new_cancel_seat_request="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=3  AND DATE(updated_time)='$to_day'";
		$rs_to_day_new_cancel_seat_request=Yii::app()->db->createCommand($sql_to_day_new_cancel_seat_request)->queryRow();
		
		$sql_previous_new_cancel_seat_request="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=3  AND DATE(updated_time)<>'$to_day'";
		$rs_previous_new_cancel_seat_request=Yii::app()->db->createCommand($sql_previous_new_cancel_seat_request)->queryRow();

		$sql_to_day_cancel_seat="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=-1 AND DATE(updated_time)='$to_day'";
		$rs_to_day_cancel_seat=Yii::app()->db->createCommand($sql_to_day_cancel_seat)->queryRow();
		
		$sql_previous_cancel_seat="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=-1  AND DATE(updated_time)<>'$to_day'";

		

		$rs_previous_cancel_seat=Yii::app()->db->createCommand($sql_previous_cancel_seat)->queryRow();

		$sql_to_day_change_seat_request="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=4 AND DATE(created_time)='$to_day'";
		$rs_to_day_change_seat_request=Yii::app()->db->createCommand($sql_to_day_change_seat_request)->queryRow();
		
		$sql_previous_change_seat_request="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=4  AND DATE(created_time)<>'$to_day'";
		$rs_previous_change_seat_request=Yii::app()->db->createCommand($sql_previous_change_seat_request)->queryRow();

		$sql_to_day_approved="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=1 AND DATE(updated_time)='$to_day'";
		$rs_to_day_approved=Yii::app()->db->createCommand($sql_to_day_approved)->queryRow();
		
		$sql_previous_approved="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=1  AND DATE(updated_time)<>'$to_day'";
		$rs_previous_approved=Yii::app()->db->createCommand($sql_previous_approved)->queryRow();

		/*$sql_to_day_maternity_assign="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=1 AND DATE(updated_time)='$to_day'";
		$rs_to_day_maternity_assign=Yii::app()->db->createCommand($sql_to_day_maternity_assign)->queryRow();
		
		$sql_previous_maternity_assign="SELECT count(id) as request FROM tbl_seat_request WHERE route_id='$id' AND status=1  AND DATE(updated_time)<>'$to_day'";
		$rs_previous_approved=Yii::app()->db->createCommand($sql_previous_maternity_assign)->queryRow();*/

		$sql_to_day_maternity_request="SELECT count(m.id) as request FROM tbl_maternity_leave_request as m INNER JOIN tbl_seat_request as s ON m.user_pin=s.user_pin WHERE m.status=0 AND DATE(m.created_time)='$to_day' AND s.status=1 AND s.queue=-1 AND s.route_id='$id'";
		$rs_to_day_maternity_request=Yii::app()->db->createCommand($sql_to_day_maternity_request)->queryRow();
		
		$sql_previous_maternity_request="SELECT count(m.id) as request FROM tbl_maternity_leave_request as m INNER JOIN tbl_seat_request as s ON m.user_pin=s.user_pin WHERE m.status=0 AND DATE(m.created_time)<>'$to_day' AND s.status=1 AND s.queue=-1 AND s.route_id='$id'";
		$rs_previous_maternity_request=Yii::app()->db->createCommand($sql_previous_maternity_request)->queryRow();

		
		$sql_to_day_maternity_approved="SELECT count(m.id) as request FROM tbl_maternity_leave_request as m INNER JOIN tbl_seat_request as s ON m.user_pin=s.user_pin WHERE m.status=1 AND DATE(m.update_time)='$to_day' AND s.status=1 AND s.queue=-1 AND s.route_id='$id'";
		$rs_to_day_maternity_approved=Yii::app()->db->createCommand($sql_to_day_maternity_approved)->queryRow();
		
		$sql_previous_maternity_approved="SELECT count(m.id) as request FROM tbl_maternity_leave_request as m INNER JOIN tbl_seat_request as s ON m.user_pin=s.user_pin WHERE m.status=1 AND DATE(m.update_time)<>'$to_day' AND s.status=1 AND s.queue=-1 AND s.route_id='$id'";
		$rs_previous_maternity_approved=Yii::app()->db->createCommand($sql_previous_maternity_approved)->queryRow();

//$rs_to_day_new_seat_request['request'];
	echo $this->renderPartial('routeSummery', array(
		'rs_to_day_new_seat_request'=>$rs_to_day_new_seat_request,
		'rs_previous_new_seat_request'=>$rs_previous_new_seat_request,
		'rs_to_day_new_cancel_seat_request'=>$rs_to_day_new_cancel_seat_request,
		'rs_previous_new_cancel_seat_request'=>$rs_previous_new_cancel_seat_request,
		'rs_to_day_cancel_seat'=>$rs_to_day_cancel_seat,
		'rs_previous_cancel_seat'=>$rs_previous_cancel_seat,
		'rs_to_day_change_seat_request'=>$rs_to_day_change_seat_request,
		'rs_previous_change_seat_request'=>$rs_previous_change_seat_request,
		'rs_to_day_approved'=>$rs_to_day_approved,
		'rs_previous_approved'=>$rs_previous_approved,
		'rs_previous_change_seat_request'=>$rs_previous_change_seat_request,
		'rs_to_day_maternity_request'=>$rs_to_day_maternity_request,
		'rs_previous_maternity_request'=>$rs_previous_maternity_request,
		'rs_to_day_maternity_approved'=>$rs_to_day_maternity_approved,
		'rs_previous_maternity_approved'=>$rs_previous_maternity_approved,
		'route_no'=>$route_no,
		'id'=>$id,
	));
	
	
	}

public function sendMailIcress($email,$body,$sub) {
        try { 
        	//echo $email; echo $body; echo $sub;
            $soapClient = new SoapClient("http://imail.brac.net:8080/isoap.comm.imail/EmailWS?wsdl");

            $job = new jobs;

           // $job->subject = $sub;
            $job->jobContentType = 'html';
            $job->fromAddress = 'ifleet@brac.net';
            $job->udValue1 = 'iFleet';
            $job->requester = 'iFleet';

            //   $job->jobRecipients[0]=new jobRecipients;
            //   $job->jobRecipients[0]->recipientEmail="shouman.das@gmail.com";

            $job->jobRecipients[0] = new jobRecipients;
            //$job->jobRecipients[0]->recipientEmail = $model->email;
            $job->jobRecipients[0]->recipientEmail = $email;


            //$model1 = $hrdata->getHrUser($model->pin);
            //$rec_mail = $model1[0]['Email'];

           
                $job->subject = $sub;
                $job->body = nl2br($body);
            $jobs = array('jobs' => $job);
            $send_email = $soapClient->__call('sendEmail', array($jobs));
            //$send_email->return->status;
            
        } catch (SoapFault $fault) {
           $error = 1;
           // print($fault->faultcode . "-" . $fault->faultstring);
        }
    }
}

class jobs
{

    public $appUserId; // string
    public $attachments; // attachment
    public $bcc; // string
    public $body; // string
    public $caption; // string
    public $cc; // string
    public $complete; // boolean
    public $feedbackDate; // dateTime
    public $feedbackEmail; // string
    public $feedbackName; // string
    public $feedbackSent; // boolean
    public $fromAddress; // string
    public $fromText; // string
    public $gateway; // string
    public $jobContentType; // string
    public $jobId; // long
    public $jobRecipients; // jobRecipients
    public $mode; // string
    public $numberOfItem; // int
    public $numberOfItemFailed; // int
    public $numberOfItemSent; // int
    public $priority; // string
    public $requester; // string
    public $status; // string
    public $subject; // string
    public $toAddress; // string
    public $toText; // string
    public $udValue1; // string
    public $udValue2; // string
    public $udValue3; // string
    public $udValue4; // string
    public $udValue5; // string
    public $udValue6; // string
    public $udValue7; // string
    public $vtemplate; // string

}

class jobRecipients
{

    public $failCount; // int
    public $image; // base64Binary
    public $job; // jobs
    public $jobDetailId; // long
    public $recipientEmail; // string
    public $sent; // boolean
    public $sentDate; // dateTime
    public $toText; // string

}
