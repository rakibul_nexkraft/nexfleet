<?php
/* @var $this MaternityLeaveRequestAdminController */
/* @var $model MaternityLeaveRequestAdmin */

$this->breadcrumbs=array(
	'Maternity Leave Request Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request Admin', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Request Admin', 'url'=>array('create')),
	array('label'=>'View MaternityLeave Request Admin', 'url'=>array('view', 'id'=>$model->id)),
	//array('label'=>'Manage Maternity Leave Request Admin', 'url'=>array('admin')),
);
?>

<h1>Update Maternity Leave Request Admin <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>