<?php
/* @var $this MaternityLeaveRequestAdminController */
/* @var $model MaternityLeaveRequestAdmin */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'maternity-leave-request-admin-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php //echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128)); 
			
			$user = SeatRequestAdmin::model()->findAll(				
				array('select'=>'user_pin,user_name','condition'=>'status=1','order' => 'user_pin ASC','group'=>'user_pin'));
			
			echo $form->dropDownList($model,'user_pin', CHtml::listData($user,'user_pin','user_pin'),array('empty' => 'Select User Pin','style'=>'height:30px; width:190px;','class'=>'input-medium'));
		?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from_date'); ?>
		<?php //echo $form->textField($model,'from_date'); 
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,'attribute'=>'from_date',

				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
						  'mode'=>'datetime',
						  'dateFormat' => 'yy-mm-dd', // save to db format
						  'changeMonth'=>'true',
						  'changeYear'=>'true',
						  'yearRange'=>'2000:2020',
				),

			'htmlOptions'=>array('style'=>'width:150px;','autocomplete'=>'off'
			),
			));

		?>
		<?php echo $form->error($model,'from_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'to_date'); ?>
		<?php //echo $form->textField($model,'to_date'); 
			$this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,'attribute'=>'to_date',

				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
						  'mode'=>'datetime',
						  'dateFormat' => 'yy-mm-dd', // save to db format
						  'changeMonth'=>'true',
						  'changeYear'=>'true',
						  'yearRange'=>'2000:2020',
				),

			'htmlOptions'=>array('style'=>'width:150px;','autocomplete'=>'off'
			),
			));
		?>
		<?php echo $form->error($model,'to_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php //echo $form->textField($model,'status'); 
			echo $form->dropDownList($model, 'status',
			array('0' => 'Pending','1' => 'Approved','-1' => 'Cancel'));	  
		?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
		<?php echo $form->error($model,'update_time'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->