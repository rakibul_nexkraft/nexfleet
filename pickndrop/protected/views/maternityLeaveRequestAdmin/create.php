<?php
/* @var $this MaternityLeaveRequestAdminController */
/* @var $model MaternityLeaveRequestAdmin */

$this->breadcrumbs=array(
	'Maternity Leave Request Admins'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request Admin', 'url'=>array('index')),
	//array('label'=>'Manage Maternity Leave Request Admin', 'url'=>array('admin')),
);
?>

<h1>Create Maternity Leave Request Admin</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>