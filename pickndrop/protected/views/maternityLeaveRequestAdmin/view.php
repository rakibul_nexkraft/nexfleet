<?php
/* @var $this MaternityLeaveRequestAdminController */
/* @var $model MaternityLeaveRequestAdmin */

$this->breadcrumbs=array(
	'Maternity Leave Request Admins'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request Admin', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Request Admin', 'url'=>array('create')),
	array('label'=>'Update Maternity Leave Request Admin', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Maternity Leave Request Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage MaternityLeaveRequestAdmin', 'url'=>array('admin')),
);
?>

<h1>View Maternity Leave Request Admin #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_pin',
		'from_date',
		'to_date',
		array('name'=>'status',
            'type'=>'raw',
			'value'=>SeatRequestAdmin::status($model->status),
		),
		'created_by',
		'updated_by',
		'created_time',
		'update_time',
	),
)); ?>
