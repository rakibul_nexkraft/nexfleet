<?php
/* @var $this SeatSaleController */
/* @var $model SeatSale */

$this->breadcrumbs=array(
	'Seat Sales'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SeatSale', 'url'=>array('index')),
	array('label'=>'Create SeatSale', 'url'=>array('create')),
	array('label'=>'Update SeatSale', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SeatSale', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SeatSale', 'url'=>array('admin')),
);
?>

<h1>View SeatSale #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_pin',
		'route_id',
		'price',
		'from_date',
		'to_date',
		'travel_direction',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
