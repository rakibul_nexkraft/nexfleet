<?php
/* @var $this SeatSaleController */
/* @var $model SeatSale */

$this->breadcrumbs=array(
	'Seat Sales'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SeatSale', 'url'=>array('index')),
	array('label'=>'Create SeatSale', 'url'=>array('create')),
	array('label'=>'View SeatSale', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SeatSale', 'url'=>array('admin')),
);
?>

<h1>Update SeatSale <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>