<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
<style>
    .fs-12{
        font-size: 12px;
    }
</style>
<div id="route-request-list" class="custom-box-design responsive-sell-history admin-dashboard-table">
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
        <h4 class="heading-custom page_header_h4">Open Seats</h4>
    </div>
    <div class="table-responsive bottommargin responsive-sell-history-table border-round-bottom" style="border: none !important; background: transparent !important;">

        <table class="table cart ">
            <thead>
                <tr>                    
                    <th class="cart-product-price">Route No</th>
                    <th class="cart-product-price">v. Type</th>
                    <th class="cart-product-price">ifleet</th>
                    <th class="cart-product-price">HO IN</th>
                    <!-- <th class="cart-product-price">User Pin</th> -->
                    <th class="cart-product-price">HO OUT</th>
                    <th class="cart-product-price">TOTAL</th>
                    <!-- <th class="cart-product-price">Status</th> -->
                    <!--<th class="cart-product-price">Action</th>-->
                </tr>
            </thead>
            <tbody>
            <?php
            if (count($model) > 0): ?>
                <?php foreach ($model as $index => $sell): ?>
                    <tr class="cart_item">
                        <td class="cart-product-price">
                            <?php
                            echo $sell['route_no'];
                             ?>
                        </td>
                        <td class="cart-product-price"><?php echo $sell['type']; ?></td>
                        <td class="cart-product-price"><?php echo $sell['av']; ?></td>
                        <td class="cart-product-price"><?php echo $sell['HO-IN']; ?></td>
                        <td class="cart-product-price"><?php echo $sell['HO-OUT']; ?></td>
                        <!-- <td class="cart-product-price"><?php //echo $sell['user_pin']; ?></td> -->
                        <td class="cart-product-price"><?php echo $sell['av']+$sell['HO-OUT']+$sell['HO-IN']; ?></td>
                        <!-- <td class="cart-product-price"><button class='btn-search' id="search-button" onclick="saleRoute()">Buy History</button> </td>  -->
                        <!--<td class="cart-product-price">
                            <a href="javascript:void(0)" class="button button-rounded button-reveal button-mini" onclick="showModal(< ?php echo $sell->id; ?>)"><i class="icon-edit"></i><span>Update</span></a>
                        </td>-->
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7" class="empty"><span class="empty">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>




