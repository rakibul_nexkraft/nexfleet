<style type="text/css">
	.choices__list--single .choices__item {    
    padding-left: 20px;
}
</style>
<div id="route-request-list" class="seat-request-admin">

    <div class="center s002 searchbar">
        <?php $form = $this->beginWidget('CActiveForm', array( 
        	'id' => 'sale-route',    
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
           	'htmlOptions' => array('onsubmit' => 'return false',),
        )); ?>
        <input type='hidden' name='SeatSale[route_id]' value="<?php echo $use_route['id'] ?>">
        <div class="inner-form seat-request-form">
            <div class='col_half'>
                <div class="seat-request-label">Route No</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                   
                    <?php   	            
		            $model->route_no=$use_route['route_no'];
		            echo $form->textField($model,'route_no',array('readonly'=>'readonly'));	      
                    ?>

                </div>
            </div>
            <div class='col_half col_last'>
                <div class="seat-request-label">Seat Price</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                   <?php   	            
		            $model->price=$use_route['market_price'];
		            echo $form->textField($model,'price',array('readonly'=>'readonly'));	      
                    ?>

                </div>

            </div>
        </div>
        <div class="inner-form seat-request-form">
            <div class='col_half'>
                <div class="seat-request-label">Date</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
				    </path>
                </svg>
                    </div>
                    <?php echo $form->textField($model,'from_date',array('placeholder'=>'From Date','id'=>'depart','class'=>'datepicker')); ?>

                </div>
            </div>
            <div class='col_half col_last'>
                <div class="seat-request-label">Travel  Direction</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                   
                    <select data-trigger="" name="SeatSale[travel_direction]">
                        <option value='1'>HO IN</option>
                        <option value='2'>HO OUT</option>            
                    </select>
                </div>
            </div> 
            
        </div>
        <div class="inner-form seat-request-form" style="justify-content: flex-end">
            <div class='seat-request-form-box'>
                <div class="seat-request-label col_one_third"></div>
                <div class='input-field first-wrap searchbar-search-button col_two_third'>
                    <?php echo CHtml::submitButton("Submit", array('class' => 'btn-search button-pink', 'id' => 'search-button', 'onclick'=>'saleRoute()')); ?>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
<script type="text/javascript">
  function saleRoute(v){
    $("#sale-route").ajaxSubmit(
        {
        url: '<?php echo Yii::app()->createAbsoluteUrl("/seatSale/timeCheck");?>', 
        type: 'post', 
        success:  function(data) { 
            if(data==1) {
                var data= 'Do you really want to sell your seat?'+'<br><br>'+'1. User will get 60% of seat price'+'<br><br>'+'2. iFleet will get 40% of seat price'+'<br><br>'+'I agree with the Terms & Conditions'+'<br><br>'+'<button class="btn btn-success" onclick="saleRoute1()">Confirm</button>';
                //openModel(data);
                bsModalOpen(data);
            }
            if(data==2){
                //openModel("Check Your Date!");
                bsModalOpen("Check Your Date!");
            }
                  
        }
    }
);
  	
    		
	}
function saleRoute1(){		
		$("#sale-route").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/seatSale/saleSeat");?>',
            type: 'post',
            success:  function(data) {
                //openModel(data);
                bsModalOpen(data);
            }
		});

	}

</script> 
 <script>flatpickr(".datepicker", {defaultDate: "today",minDate: "today"});</script>