<?php
/* @var $this SeatSaleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seat Sales',
);

$this->menu=array(
	array('label'=>'Create SeatSale', 'url'=>array('create')),
	array('label'=>'Manage SeatSale', 'url'=>array('admin')),
);
?>

<h1>Seat Sales</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
