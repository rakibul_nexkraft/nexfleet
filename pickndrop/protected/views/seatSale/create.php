<?php
/* @var $this SeatSaleController */
/* @var $model SeatSale */

$this->breadcrumbs=array(
	'Seat Sales'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SeatSale', 'url'=>array('index')),
	array('label'=>'Manage SeatSale', 'url'=>array('admin')),
);
?>

<h1>Create SeatSale</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>