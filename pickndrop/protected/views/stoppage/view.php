<?php
/* @var $this StoppageController */
/* @var $model Stoppage */

$this->breadcrumbs = array(
    'Stoppages' => array('index'),
    $model->id,
);

/*$this->menu=array(
	array('label'=>'List Stoppage', 'url'=>array('index')),
	//array('label'=>'Create Stoppage', 'url'=>array('create')),
	array('label'=>'Update Stoppage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Stoppage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Stoppage', 'url'=>array('admin')),
);*/
?>
<div id="route-request-list">
   <!-- <div class="center s002 ">
        <div class="col_one_sixth">
            <div class="toggle toggle-border">
                <div class="togglet">Menu</div>
                <div class="togglec" style="display: none;">
                    <?php
/*                    $this->widget('zii.widgets.CMenu', array(
                        'items' => array(
                            array('label' => 'List Stoppage', 'url' => array('/stoppage/index')),
                            array('label' => 'New Stoppage', 'url' => array('/stoppage/create')),
                            array('label' => 'Add Stoppage', 'url' => array('/stoppage/create', 'route_id' => $model->route_id)),

                        ),
                    ));

                    */?>
                </div>
            </div>
        </div>
    </div>-->
    <div class="col_full page_header_div" >
        <h4 class="heading-custom page_header_h4">View Stoppage ID#<?php echo $model->id; ?></h4>
    </div>

    <div class="s002 update-form-background">
        <div class='table-responsive bottommargin overflow-auto stoppage-table-responsive'>

            <?php $this->widget('zii.widgets.CDetailView', array(
                'htmlOptions' => array('class' => 'table cart'),
                'data' => $model,
                'attributes' => array(
                    'id',

                    array(
                        'name' => 'route_id',
                        'type' => 'raw',
                        'value' => SeatRequest::routeDetail($model->route_id, 1),
                    ),
                    'stoppage',
                    'pickup_time',
                    'created_by',
                    'created_time',
                    'updated_by',
                    'updated_time',
                ),
            ));

            ?>
        </div>
    </div>
</div>