<?php
/* @var $this StoppageController */
/* @var $model Stoppage */

$this->breadcrumbs = array(
    'Stoppages' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

/*$this->menu=array(
    array('label'=>'List Stoppage', 'url'=>array('index')),
    //array('label'=>'Create Stoppage', 'url'=>array('create')),
    array('label'=>'View Stoppage', 'url'=>array('view', 'id'=>$model->id)),
    array('label'=>'Manage Stoppage', 'url'=>array('admin')),
);*/
?>
    <div class="center s002 ">
        <!-- Floating Menu Button -->
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                 class="floating-menu dropdown-toggle animated fadeInRight">
                <!--                <i class="floating-menu-label vs-icon notranslate icon-scale vs-button--icon  feather icon-settings null"></i>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'List Stoppages', 'url' => array('/stoppage/index')),
                    ),
                ));

                ?>
            </div>
        </div>
    </div>
    <div class="col_full page_header_div" style="border-bottom: 1px solid #f2ecec;">
        <h4 class="heading-custom page_header_h4">Update Stoppage <?php echo $model->stoppage; ?></h4>
    </div>


<?php echo $this->renderPartial('_form', array('model' => $model)); ?>