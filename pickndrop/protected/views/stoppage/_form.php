<?php
/* @var $this StoppageController */
/* @var $model Stoppage */
/* @var $form CActiveForm */
?>

<div class="clo_full s002 stoppage_update stoppage-uptade update-form-background">

    <?php
    if (isset($model->route_id) && !empty($model->route_id)) {
        $_GET['route_id'] = $model->route_id;
    }
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'stoppage-form',
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php /*echo $form->errorSummary($model); */
    ?>


    <div class="inner-form seat-request-form">
        <div class="col_one_third seat-request-form-box">
                <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'route_id'); ?></div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php $route = Routes::model()->findAll(array('order' => 'id ASC')); ?>
                    <select data-trigger="" name="Stoppage[route_id]">
                        <option value=''>A Route Select</option>
                        <?php foreach ($route as $key => $value) {
                            if (isset($_GET['route_id']) && $_GET['route_id'] == $value['id']) {
                                echo "<option value='" . $value['id'] . "' selected>" . $value['route_no'] . "</option>";
                            } else {
                                echo "<option value='" . $value['id'] . "'>" . $value['route_no'] . "</option>";
                            }

                        }
                        ?>
                    </select>
                </div>
                <?php echo $form->error($model, 'route_id'); ?>
        </div>

        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'stoppage'); ?></div>
            <div class="input-field first-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'stoppage'); ?>
            </div>
            <?php echo $form->error($model, 'stoppage'); ?>
        </div>


        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'pickup_time'); ?></div>
            <div class="input-field first-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php
                $this->widget('ext.jui.EJuiDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'pickup_time',
                    'language' => 'en',//default Yii::app()->language
                    'mode' => 'time',//'datetime' or 'time' ('datetime' default)

                    'options' => array(
                        //'dateFormat' => 'dd.mm.yy',
                        'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                        //'ampm' => 'true',
                    ),
                    'htmlOptions' => array('autocomplete' => 'off'),
                ));
                ?>
            </div>
            <?php echo $form->error($model, 'pickup_time'); ?>
        </div>
    </div>

    <div class="col_full" style="width: 15%;float: right;margin: 0 0 -32px 0;">
        <div class="input-field first-wrap searchbar-search-button">
            <?php echo CHtml::submitButton("Save", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
        </div>

    </div>

    <?php $this->endWidget(); ?>

</div>
<?php if (isset($_GET['route_id'])) {
    $route_id = $_GET['route_id'];
    $stoppage_sql = "SELECT * FROM tbl_stoppage WHERE route_id='" . $route_id . "' ORDER BY pickup_time";
    $stoppage_rs = Yii::app()->db->createCommand($stoppage_sql)->queryAll();

    ?>

    <div class="col_full page_header_div">
        <h4 class="heading-custom page_header_h4">This Route's Stoppages List</h4>
    </div>
    <div class='table-responsive bottommargin admin admin-table-responsive' style="padding: 2rem !important;">

        <table class="table cart stoppage-table">
            <thead>
            <tr>
                <th class="cart-product-price"><strong>Stoppage</strong></th>
                <th class="cart-product-price"><strong>Time</strong></th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($stoppage_rs) > 0) {
                foreach ($stoppage_rs as $key => $value) {
                    ?>
                    <tr class="cart_item">
                        <td class="cart-product-price"><?php echo $value['stoppage']; ?></td>
                        <td class="cart-product-price"><?php echo $value['pickup_time']; ?></td>
                    </tr>
                <?php }
            } else {
                ?>
                <tr class="cart_item">
                    <td colspan="2" class="cart-product-price">Stoppage Not Found</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>


    </div>
<?php } ?>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>


<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',
        });

</script> 