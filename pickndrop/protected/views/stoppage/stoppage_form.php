<?php
/* @var $this StoppageController */
/* @var $model Stoppage */
/* @var $form CActiveForm */
?>

<div class="s002 searchbar">

    <?php
   
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'stoppage-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('onsubmit' => 'return false'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php /*echo $form->errorSummary($model); */
    ?>


    <div class="inner-form seat-request-form">
        <div class="col_full">
            <div class="seat-request-label"><?php echo $form->labelEx($model, 'route_id'); ?></div>
            <div class="input-field fouth-wrap seat-request-input-box">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'route_id', array('placeholder' => 'Route No.', 'id' => "search", 'list' => 'route_list_form'));
                ?>
                <?php $route = Routes::model()->findAll(array('order' => 'id ASC')); ?>
                <datalist id="route_list_form">
                    <?php
                    foreach ($route as $key => $value) {
                        echo "<option value='" . $value['route_no'] . "'>";
                    }
                    ?>
                </datalist>
            </div>
            <?php echo $form->error($model, 'route_id'); ?>
        </div>

        <div class="col_full">
            <div class="seat-request-label "><?php echo $form->labelEx($model, 'stoppage'); ?></div>
            <div class="input-field first-wrap seat-request-input-box ">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'stoppage'); ?>
            </div>
            <?php echo $form->error($model, 'stoppage'); ?>
        </div>

        <div class="col_full">
            <div class="seat-request-label"><?php echo $form->labelEx($model, 'pickup_time'); ?></div>
            <div class="input-field first-wrap seat-request-input-box">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php
                $this->widget('ext.jui.EJuiDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'pickup_time',
                    'language' => 'en',//default Yii::app()->language
                    'mode' => 'time',//'datetime' or 'time' ('datetime' default)

                    'options' => array(
                        //'dateFormat' => 'dd.mm.yy',
                        'timeFormat' => 'hh.mm tt',//'hh:mm tt' default
                        //'ampm' => 'true',
                    ),
                    'htmlOptions' => array('autocomplete' => 'off'),
                ));
                ?>
            </div>
            <?php echo $form->error($model, 'pickup_time'); ?>
        </div>
    </div>
    <div class="inner-form" style="justify-content: flex-end;">
        <div>
            <div class="input-field first-wrap searchbar-search-button">
                <?php echo CHtml::submitButton("Save", array('class' => 'btn-search button-pink', 'id' => 'search-button', 'onclick'=>'submitStoppage()')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    function submitStoppage(){
        $("#stoppage-form").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/stoppage/createData");?>', 
            type: 'post', 
            success:  function(data) { 
                if(data==1) {
                    bsModalOpen("Stoppage is saved successfully!");
                }
                else {
                    $("#stoppage-div").html(data);            
                }         
            }
        });
    }
</script>