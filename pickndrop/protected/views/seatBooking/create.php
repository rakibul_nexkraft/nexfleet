<?php
/* @var $this SeatBookingController */
/* @var $model SeatBooking */

$this->breadcrumbs=array(
	'Seat Bookings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SeatBooking', 'url'=>array('index')),
	array('label'=>'Manage SeatBooking', 'url'=>array('admin')),
);
?>

<h1>Create SeatBooking</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>