
<style type="text/css">
    .choices__list--single .choices__item {    
    padding-left: 20px;
}
</style>
<div id="route-request-list" class="seat-request-admin">

    <div class="center s002 searchbar">
        <?php $form = $this->beginWidget('CActiveForm', array( 
            'id' => 'book-route',    
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'htmlOptions' => array('onsubmit' => 'return false',),
        )); ?>
        <input type='hidden' name='SeatBooking[sale_id]' value="<?php echo $sale['id'] ?>">
        <input type='hidden' name='SeatBooking[route_id]' value="<?php echo $sale['route_id'] ?>">
        
        <div class="inner-form seat-request-form">
            <div class='col_half seat-request-form-box'>
                <div class="seat-request-label col_one_third">Route No</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                      
                    <input type='text' name='SeatBooking[route_no]' value="<?php echo $sale['route_no']; ?>" readonly="readonly">
                </div>
            </div>
            <div class='col_half seat-request-form-box'>
                <div class="seat-request-label col_one_third">Seat Price</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                                  
                   
                    <input type='text' name='SeatBooking[price]' value="<?php echo $sale['price']; ?>" readonly="readonly">             

                </div>

            </div>
        </div>
        <div class="inner-form seat-request-form">
            <div class='col_half seat-request-form-box'>
                <div class="seat-request-label col_one_third">From</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">                     
                    </path>
                </svg>
                    </div>
                      <input type='text' name='SeatBooking[date_from]' value="<?php echo $sale['from_date']; ?>"  id='depart', class='datepicker'>  
                   

                </div>
            </div>
            <div class='col_half seat-request-form-box'>
                <div class="seat-request-label col_one_third">To</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">                     
                            </path>
                        </svg>
                    </div>
                     <input type='text' name='SeatBooking[date_to]' value="<?php echo $sale['to_date']; ?>"  id='depart', class='datepicker'>  
                    

                </div>
            </div>
        </div> 
        <div class="inner-form seat-request-form">          
             <div class='col_half seat-request-form-box'>
                <div class="seat-request-label col_one_third">Travel  Direction</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                         <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                   
                    <select data-trigger="" name="SeatBooking[direction_travale]">
                        <?php
                        if($sale['travel_direction']==3) { ?><option value='3'>HO OUT</option>
                        <?php } elseif($sale['travel_direction']==2) {?>   <option value='2'>HO IN</option>
                        <?php } else { ?>
                        <option value='1'>Both</option>
                        <!-- <option value='2'>HO IN</option>
                        <option value='3'>HO OUT</option> -->
                        ?>           
                        <?php } ?> 
                    </select>
                </div>
            </div>                
            <div class='col_half seat-request-form-box'>
                <div class="seat-request-label col_one_third"></div>
                <div class='input-field first-wrap searchbar-search-button col_two_third'>
                    <?php echo CHtml::submitButton("Submit", array('class' => 'btn-search', 'id' => 'search-button', 'onclick'=>'bookRoute()')); ?>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
</div>
<script type="text/javascript">
function bookRoute(){      
        $("#book-route").ajaxSubmit(
        {
        url: '<?php echo Yii::app()->createAbsoluteUrl("/seatBooking/bookSeat");?>', 
        type: 'post', 
        success:  function(data) {
            bsModalOpen(data);
            }
        }
    );

}
</script>