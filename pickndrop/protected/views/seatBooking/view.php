<?php
/* @var $this SeatBookingController */
/* @var $model SeatBooking */

$this->breadcrumbs=array(
	'Seat Bookings'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List SeatBooking', 'url'=>array('index')),
	array('label'=>'Create SeatBooking', 'url'=>array('create')),
	array('label'=>'Update SeatBooking', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SeatBooking', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SeatBooking', 'url'=>array('admin')),
);
?>

<h1>View SeatBooking #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sale_id',
		'user_pin',
		'date_from',
		'date_to',
		'direction_travale',
		'route_id',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
