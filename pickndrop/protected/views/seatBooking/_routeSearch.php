<style type="text/css">
	.choices__list--single .choices__item {
    width: 100%;
    padding-left: 20px;
}
</style>
<div style='text-align: right;margin: 0px 0 -18px 0px; padding-right:12px;' id="seat-av"> </div>
<div class="clearfix"></div>
<div class="s002 searchbar" >    

   <?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'movements-form',
				'enableAjaxValidation' => false,	
				'htmlOptions' => array('onsubmit' => 'return searchRoute(this)'),		
	)); ?>  

    <div class="inner-form" style="margin: 21px 0 -18px 0px;
    padding: 12px;">
        <div class="input-field first-wrap" style="width:100%">
            <div class="icon-wrap">
			    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">						
					</path>
		        </svg>
            </div>   
            
             <?php if((date('H:i:s')>="08:30:00") && (date('H:i:s')<="20:30:00")) {  ?> 
            <select data-trigger="" name="Stoppage[id]" id="" onchange="seatBookForm(this.value)">
                <option value=''>Search Your Pick Point</option>
                    <?php  foreach ($route_list as $key => $value) {
                        $stoppage_rs = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$value['id']."' ORDER BY pickup_time")->queryAll();    	
                        	$stop = '[Route No: '.$value['route_no'].'] ';
    					if(count($stoppage_rs)>0) {
							foreach ($stoppage_rs as $key1 => $value_stoppage) {
							$stop.= $value_stoppage['stoppage'];
								if(count($stoppage_rs)>$key1+1)$stop.=  " -> ";
							} 
						}
                       
                        echo "<option value='" . $key . "'>" . $stop. ", ".$value['travel_direction']."-".$value['type']."-".$value['market_price']."TK</option>";
                       }
                        ?>
            </select>
            <?php } else { ?>
            <input type="text" value="No options found for seat booking" readonly="readonly">
            <?php } ?>
           
	    </div>       
    </div>

    
    <?php $this->endWidget(); ?>
</div> 
<div id='book_form'>
</div>
<script>
    function seatBookForm(sid){

        $.post('<?php echo Yii::app()->createAbsoluteUrl("/seatBooking/seatBookingForm");?>',{sid:sid},function(data){           
            $('#book_form').html(data);
            flatpickr(".datepicker", {minDate: "today"});
             const choices = new Choices('[data-trigger]',
                {
                    searchEnabled:false,
                    itemSelectText: '',
                });
                $.post('<?php echo Yii::app()->createAbsoluteUrl("/seatBooking/seatAV");?>',{sid:sid},function(sdata){                    
                $('#seat-av').html(sdata);
                });
                       
            });

    }
</script>