<div id="route-request-list" class="seat-request-admin search-box-responsive">   
    <div class="center s002 searchbar update-form-background2" style="padding: 0 20px;">
        <?php
             $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>
        <div class="inner-form seat-request-form">
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">User Pin</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'user_pin'); ?>
                </div>
            </div>
                               
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">Date From</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'date_from', array('placeholder' => 'Date Form', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">Date To</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                   <?php echo $form->textField($model, 'date_to', array('placeholder' => 'Date To', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div> 
            <div class='col_one_fourth seat-request-form-box col_last'>
                <div  style="display:flex;justify-content: flex-end">
                    <div>
                        <div class='input-field first-wrap searchbar-search-button'>
                            <?php echo CHtml::submitButton("Search", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">
        <h4 class="heading-custom page_header_h4">Shohochor Report</h4>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'seat-request-admin-grid',
        'itemsCssClass' => 'table cart',
        'htmlOptions' => array('class' => 'table-responsive table-responsive2 bottommargin table-overflow-x admin-seat-request-table-responsive border-round-bottom'),

        'rowCssClass' => array('cart_item'),
        'dataProvider' =>$model->shohochorReport(),
        //'filter' => $model,
        'columns' => array(            
            'user_pin',   
            array(
                'header' => 'Total Booking',
                'type' => 'raw',
                'value' => '$data->total_book',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),  
             array(
                'header' => 'Total Sale',
                'type' => 'raw',
                'value' => '0',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
             array(
                'header' => 'Balance',
                'type' => 'raw',
                'value' => '0',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
          /*  array(
                'header' => 'Total Expenditure',
                'type' => 'raw',
                'value' => '$data->total_spend',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/

            /* array(
                'name' => 'sister',
                'type' => 'raw',
                'value' => '$data->sister',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),  
             */

         

        ),
    )); ?>

</div>

<div class="s002 export_to_excel_button export_to_excel_button_rout export-to-excel-button-responsive button-pink">
    <?php

    echo CHtml::normalizeUrl(CHtml::link('Export to Excel', array('ExcelshohochorReport', 'criteria' => $_GET['Commonfleets'])));
    ?>
</div>

<?php 
    function booking($user_pin){
       $total_book = Yii::app()->db->createCommand("SELECT COUNT(user_pin) as t_book FROM tbl_seat_booking WHERE user_pin='".$user_pin."'")->queryRow();
       return $total_book['t_book'];
    }
    function earning($user_pin){
       $total_book = Yii::app()->db->createCommand("SELECT SUM(price) as t_earning FROM tbl_seat_booking WHERE user_pin='".$user_pin."'")->queryRow();
       return $total_book['t_earning'];
    }

?>
<script>
    function status(id, value, preValue) {

        var message = "Do you want to change seat request status for seat request ID# " + id + "?";

        var con = confirm(message);
        if (con === true) {
            jQuery.post('<?php echo $this->createUrl('statusChange');?>', {
                id: id,
                value: value,
                preValue: preValue
            }, function (data) {
                //var size_to_value=JSON.parse(data);
                //alert(data);
                bsModalOpen(data);
            });

        }
        else {
            //$(this).val()=preValue;
            //alert(preValue);
            ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }


    }
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {});</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',

        });

</script>


