<?php
/* @var $this SeatBookingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seat Bookings',
);

$this->menu=array(
	array('label'=>'Create SeatBooking', 'url'=>array('create')),
	array('label'=>'Manage SeatBooking', 'url'=>array('admin')),
);
?>

<h1>Seat Bookings</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
