<?php
/* @var $this SeatBookingController */
/* @var $model SeatBooking */

$this->breadcrumbs=array(
	'Seat Bookings'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SeatBooking', 'url'=>array('index')),
	array('label'=>'Create SeatBooking', 'url'=>array('create')),
	array('label'=>'View SeatBooking', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SeatBooking', 'url'=>array('admin')),
);
?>

<h1>Update SeatBooking <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>