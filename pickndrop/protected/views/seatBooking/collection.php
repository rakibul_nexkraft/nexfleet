<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
<style>
    .fs-12{
        font-size: 12px;
    }
</style>
<div id="route-request-list" class="custom-box-design seat-booking-collection-responsive">
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
        <h4 class="heading-custom page_header_h4">Collection</h4>
    </div>

    <div class="table-responsive bottommargin seat-booking-collection-table-responsive border-round-bottom" style="border: none !important; background: transparent !important;">

        <table class="table cart">
            <thead>
                <tr>                    
                    <th class="cart-product-price">Route No</th>
                    <th class="cart-product-price">iFleet's Earning</th>           
                    <th class="cart-product-price">User's Earning</th>
                    <!-- <th class="cart-product-price">User Pin</th> -->
                    <th class="cart-product-price">Total Price</th>
                    <!-- <th class="cart-product-price">Status</th> -->
                    <!--<th class="cart-product-price">Action</th>-->
                </tr>
            </thead>
            <tbody>
            <?php 
            if (count($collection) > 0): ?>
                <?php foreach ($collection as $key=> $value): ?>
                    <tr class="cart_item">
                        <td class="cart-product-price">
                            <?php echo $value['route_no']; ?>
                        </td>
                        <td class="cart-product-price"><?php echo (40*$value['sum_price']/100);?>
                        	
                        </td>
                        <td class="cart-product-price"><?php echo (60*$value['sum_price']/100);?>
                        	
                        </td>                       
                        <td class="cart-product-price"><?php echo $value['sum_price']; ?>
                        	
                        </td>
                      
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7" class="empty"><span class="empty">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
       
    </div>
</div>




