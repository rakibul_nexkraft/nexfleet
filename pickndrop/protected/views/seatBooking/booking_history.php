<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
<style>
    .fs-12{
        font-size: 12px;
    }
</style>
<div id="route-request-list" class="custom-box-design booking-history responsive-booking-history">
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">

        <h4 class="heading-custom page_header_h4">Booking History</h4>
    </div>
    <div class="table-responsive bottommargin responsive-sell-booking-table responsive-booking-history-table border-round-bottom">

        <table class="table cart">
            <thead>
            <tr>
                <th class="cart-product-price">Booking Date</th>
               
                <th class="cart-product-price">Route</th>
                <th class="cart-product-price">Date</th>             
                <th class="cart-product-price">Travel Direction</th>
                <th class="cart-product-price">Price</th>
               
            </tr>
            </thead>
            <tbody>
            <?php if (count($model) > 0): ?>
                <?php foreach ($model as $index => $booking): ?>
                    <tr class="cart_item">
                        <td class="cart-product-price">
                            <?php
                            $created_time = date_create($booking->created_time);
                            echo date_format($created_time,"d/m/Y h:i A");
                            //                            echo $booking->created_time; ?>
                        </td>
                        <td class="cart-product-price"><?php echo $booking['route_no']; ?></td>
                        
                        <td class="cart-product-price"><?php echo $booking['date_from']; ?></td>                     
                        <td class="cart-product-price"><?php echo ($booking['direction_travale']==1) ? "HO IN" : "HO OUT"; ?></td></td>
                        <td class="cart-product-price"><?php echo $booking['price']; ?></td>
                       
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7" class="empty"><span class="empty">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        
    </div>
</div>


<div class="modal fade" id="marketplaceUpdateModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Request</h4>
            </div>
            <div class="modal-body">
                <div style='text-align: center;'>
                    <input type='radio' name='request' value='-1'> Cancel my seat release<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px' />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>


<script>
    var showModal = function (marketplaceId) {
        console.log(marketplaceId);
        $('#marketplaceUpdateModal').modal({
            show: true
        });
    };
    function routeInformationUpdate(requestId){

        var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Choose Your Request</h5><div id='notice'></div><input type='radio' name='request' value='0'>Request For Approved<br><input type='radio' name='request' value='3'>Request For Cancel<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);

    }
    function submitRequestUpdated(requestId){
        var v=$("input[name='request']:checked").val();
        if(v==null){
            $("#notice").html("<h5 style='color:red;margin-bottom:5px;'>Please, Select A Request</h5>");
        }
        else{

            $.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>',{requestId:requestId,updateRequest:v},function(data){
                bsModalOpen(data);
                //alert(data);
            });

        }
    }

</script>