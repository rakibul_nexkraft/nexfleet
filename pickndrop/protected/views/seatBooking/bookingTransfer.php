<div id="route-request-list" class="seat-request-admin">

    <div class="center s002 searchbar">
        <?php $form = $this->beginWidget('CActiveForm', array( 
            'id' => 'transfer-other',    
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'htmlOptions' => array('onsubmit' => 'return false',),
        )); ?>
        <div class="inner-form seat-request-form">
        <input type='hidden' name='SeatBooking[id]' value="<?php echo $id; ?>">   
            <div class='col_three_fourth  seat-request-form-box'>
                <div class="seat-request-label">Transfer To (User Pin)</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                      
                    <input type='text' name='SeatBooking[transfer_user]' id="user-pin" placeholder="User Pin" <?php if(!empty($user_pin)) {echo "value='$user_pin'";}?> >
                </div>
            </div>
            <div class='seat-request-form-box'>
                <div class='input-field first-wrap searchbar-search-button'>
                    <input type='button'  onclick = "userInfo(<?php echo $id; ?>)"  class = 'btn-search button-pink', id = 'search-button' value="Search">
                   
                </div>
            </div>
            
        </div> 
        <?php if(!empty($user_pin) && !empty($message)) {           
        ?> 
        <div class="inner-form seat-request-form">        
            <div class='col_one_fourth seat-request-form-box'>     
                <?php echo $message;?>               
            </div>
        </div>

         <?php } ?>        
        <?php 
        if(!empty($user_pin) && empty($message)) { ?> 
         <div class="inner-form seat-request-form">        
              <div class='col_one_fourth seat-request-form-box'>                                                   
                    <?php
                    $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/".$user_pin.".png";
                    if (!file_exists("../photo/".$user_pin.".png")) {
                        $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/"."person1_web.jpg";
                    }
                    ?>
                    <img src="<?php echo $image_url; ?>" style="" class="img-responsive" alt="User image">     
            </div>        
            <div class='col_three_fourth seat-request-form-box'>
                <div class='col_full seat-request-form-box'>
                    <div class="seat-request-label">Transfer User Phone</div>
                    <div class="input-field fouth-wrap seat-request-input-box">
                            <div class="icon-wrap">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                                    </path>
                                </svg>
                            </div> 
                             <input type='text' name='SeatBooking[phone]'  value="<?php echo $phone;?>" readonly="readonly">                           
                    </div>
                </div>
            
            
                 <div class='col_full seat-request-form-box'>           
                        <div style="display: flex;justify-content: flex-end;">
                            <?php echo CHtml::submitButton("Submit", array('class' => 'btn-search button-pink', 'id' => 'search-button', 'onclick'=>'transferOther()', 'style'=>"width: 120px !important;")); ?>
                        </div>
                    
                </div>
            </div>
          </div>
        <?php } ?>
        <?php $this->endWidget(); ?>
    </div>
</div>

