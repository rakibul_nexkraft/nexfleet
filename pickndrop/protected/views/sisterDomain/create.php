<?php
/* @var $this SisterDomainController */
/* @var $model SisterDomain */

$this->breadcrumbs=array(
	'Sister Domains'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SisterDomain', 'url'=>array('index')),
	array('label'=>'Manage SisterDomain', 'url'=>array('admin')),
);
?>

<h1>Create SisterDomain</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>