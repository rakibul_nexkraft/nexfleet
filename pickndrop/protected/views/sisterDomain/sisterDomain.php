<style>
.toggle.toggle-bg .togglet, .toggle.toggle-bg .toggleta {
	background-color: #fff;
    padding: 0 0 0 10px;
    box-shadow: 0 2px 4px rgba(153, 153, 153, 0.24);
    border-radius: 5px;
}
.toggle.toggle-bg .togglec {
    margin-top: 13px;
    padding: 0px;
}
.toggle.toggle-bg .togglet i {
    left: 97%;
}

</style>

<div class="col_full" style="margin-bottom: 50px">
<div class="toggle toggle-bg" style="background: white;">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Sister Concern Domin
	</div>
	<div class="togglec" style="display: none;">
		<div id="sister-div" style="padding: 0 20px">
            <?php
            $this->renderPartial('/sisterDomain/sister_form',array(
                'model'=>$model,
            ));
            ?>
		</div>
	</div>
</div>
</div>
<div id="route-request-list" class="seat-request-admin search-box-responsive">

    <div class="center s002 searchbar update-form-background2" style="padding: 0 20px;">
    <?php 
	     $form=$this->beginWidget('CActiveForm', array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
	)); ?>

        <div class="inner-form seat-request-form">
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Name</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="SisterDomain[sister]">

                        <?php
                        echo "<option value='' selected>Select Name </option>";
                        $domain = SisterDomain::model()->findAll();
                        foreach ($domain as $key => $value) {
                            if (isset($_GET['SisterDomain']['sister']) && $_GET['SisterDomain']['sister'] == $value['sister']) {
                                echo "<option value=" . $value['sister'] . " selected>" . $value['sister'] . "</option>";
                            } else {
                                echo "<option value=" . $value['sister'] . ">" . $value['sister'] . "</option>";
                            }
                        }

                        ?>
                    </select>


                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Domain</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="SisterDomain[domain]">

                        <?php
                        echo "<option value='' selected>Select Domain </option>";             
                       
                        foreach ($domain as $key => $value) {
                            if (isset($_GET['SisterDomain']['domain']) && $_GET['SisterDomain']['domain'] == $value['domain']) {
                                echo "<option value=" . $value['domain'] . " selected>" . $value['domain'] . "</option>";
                            } else {
                                echo "<option value=" . $value['domain'] . ">" . $value['domain'] . "</option>";
                            }
                        }

                        ?>
                    </select>


                </div>
            </div>                     
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Date From</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'date_from', array('placeholder' => 'Date Form', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div>
              <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Date To</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                   <?php echo $form->textField($model, 'date_to', array('placeholder' => 'Date To', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div> 
        </div>

        <div class="inner-form seat-request-form" style="justify-content: flex-end;">
            <div class='seat-request-form-box'>
                <div class='input-field first-wrap searchbar-search-button'>
                    <?php echo CHtml::submitButton("Search", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>

    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top " style="border-bottom: 1px solid #efefef;">
        <h4 class="heading-custom page_header_h4">Route Recent SMS OR Email</h4>
    </div>
	<?php  $this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin, admin-table-responsive border-round-bottom'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model1->search(),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(			//'id',
			
			array(	
				'header'=>'Sister Concern Name',				
				'value'=>'$data->sister',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'user_name',
			//'user_department',
			array('header'=>'Domain Name',				
				'value'=>'$data->domain',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(
           		'class'=>'CButtonColumn',
           		'template'=>'{update},{delete}',
           		'buttons'=>array(
	             	'update'=>array(
	                  'label'=>'UPDATE',
	              	),
	             	'delete'=>array(
	                'label'=>'DELETE',                  
	                'visible'=>'Yii::app()->user->username=="admin"?true:false',
	              	),
            	),           
           //'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/image/delete.png',
           //'updateButtonImageUrl'=>Yii::app()->baseUrl.'/image/update.jpg',
        	),
		),
	)); 
?>	
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
   	const choices = new Choices('[data-trigger]',
    {
        searchEnabled: true,
        itemSelectText: '',
    });

</script>