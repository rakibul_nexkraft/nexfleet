<?php
/* @var $this SisterDomainController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sister Domains',
);

$this->menu=array(
	array('label'=>'Create SisterDomain', 'url'=>array('create')),
	array('label'=>'Manage SisterDomain', 'url'=>array('admin')),
);
?>

<h1>Sister Domains</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
