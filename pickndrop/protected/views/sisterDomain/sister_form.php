<div id="route-request-list" class="seat-request-admin">   
	<div class="center s002 searchbar update-form-background2">
        <?php $form = $this->beginWidget('CActiveForm', array( 
        	'id' => 'sister-form',    
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
           	'htmlOptions' => array('onsubmit' => 'return false',),
        )); ?>
        <p class="note" style="text-align: left;">Fields with <span class="required">*</span> are required.</p>
        <div class="inner-form seat-request-form">
            <div class='col_half'>
                <div class="seat-request-label "><?php echo $form->labelEx($model, 'sister'); ?></div>
                <div class="input-field fouth-wrap ">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                   
                    <?php   	            
		             echo $form->textField($model,'sister',array('size'=>60,'maxlength'=>128));     
                    ?>
                </div>
                <?php echo $form->error($model, 'sister'); ?>
            </div>
            <div class='col_half col_last'>
                <div class="seat-request-label "><?php echo $form->labelEx($model, 'domain'); ?></div>
                <div class="input-field first-wrap ">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                  <?php echo $form->textField($model,'domain',array('size'=>60,'maxlength'=>128)); ?>

                </div>
                <?php echo $form->error($model, 'domain'); ?>
            </div>
        </div>
        <div class="inner-form seat-request-form" style="justify-content: flex-end">
            <div class=''>
                <div class='input-field first-wrap searchbar-search-button'>
                    <?php echo CHtml::submitButton("Submit", array('class' => 'btn-search button-pink', 'id' => 'search-button', 'onclick'=>'sisterCreate()')); ?>
                </div>
            </div>
        </div>
        
         <?php $this->endWidget(); ?>
</div>
<script type="text/javascript">
	function sisterCreate(){
		$("#sister-form").ajaxSubmit({
	        url: '<?php echo Yii::app()->createAbsoluteUrl("/sisterDomain/CreateData");?>', 
	        type: 'post', 
	        success:  function(data) {                       
	            if(data==1) {
                    bsModalOpen("Sister concern domain is saved successfully!");
	            }
	            else {
	                $("#sister-div").html(data);            
	            }                   
	        }
    	});
	}
</script>