<?php
/* @var $this SisterDomainController */
/* @var $model SisterDomain */

$this->breadcrumbs=array(
	'Sister Domains'=>array('index'),
	$model->id,
);

?>
<div id="route-request-list">
    <div class="center s002 ">
        <!-- Floating Menu Button -->
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">
                <!--                <i class="floating-menu-label vs-icon notranslate icon-scale vs-button--icon  feather icon-settings null"></i>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                          array('label'=>'List Sister Domain', 'url'=>array('/sisterDomain/sisterDomain')),
                    ),
                ));

                ?>
            </div>
        </div>
    </div>
    <div class="col_full page_header_div">
        <h4 class="heading-custom page_header_h4">View Sister Concern Domain  ID#<?php echo $model->id; ?></h4>
    </div>

<div class="s002">
	<div class = 'table-responsive bottommargin overflow-auto col_full'>

	<?php $this->widget('zii.widgets.CDetailView', array(
		'htmlOptions' => array('class' => 'table cart'),
		'data'=>$model,
		'attributes'=>array(
			'id',
			'sister',
			'domain',
			'created_by',
			'created_time',
			'updated_by',
			'updated_time',
		),
	)); ?>
	</div>
</div>
