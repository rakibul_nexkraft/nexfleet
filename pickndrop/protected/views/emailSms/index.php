<?php
/* @var $this EmailSmsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Email Sms',
);

//$this->menu=array(
//array('label'=>'Create EmailSms', 'url'=>array('create')),
//array('label'=>'Manage EmailSms', 'url'=>array('admin')),
//);
?>


<div id="route-request-list" class="email-sms">
<!-- 
    <div class="center s002">


        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">

                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Send Email & SMS', 'url' => array('/emailSms/create')),
                        array('label' => 'Create Template', 'url' => array('/template/create')),
                        array('label' => 'Template List', 'url' => array('/template/index')),
                    ),
                ));
                ?>
            </div>
        </div>


       

    </div> -->
    <div class="center s002 searchbar">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'enableAjaxValidation' => true,
            'method' => 'get',
        )); ?>
        <div class="inner-form seat-request-form">
            <div class="col_one_third seat-request-form-box">
                <div class="seat-request-label col_one_third">Template</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php $temp = Template::model()->findAll(array('order' => 'id ASC')); ?>
                    <select data-trigger="" name="EmailSms[template_id]">
                        <option value='' selected="selected">A Template Select</option>
                        <?php foreach ($temp as $key => $value) {

                            echo "<option value='" . $value['id'] . "'>" . $value['title'] . "</option>";


                        }
                        ?>
                    </select>

                </div>
            </div>

            <div class="col_one_third seat-request-form-box">
                <div class="seat-request-label col_one_third">Route</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php $route = Routes::model()->findAll(array('order' => 'id ASC')); ?>
                    <select data-trigger="" name="EmailSms[route_id]">
                        <option value=''>A Route Select</option>
                        <option value='0'>ALL</option>
                        <?php foreach ($route as $key => $value) {
                            if (isset($_GET['route_id']) && $_GET['route_id'] == $value['id']) {
                                echo "<option value='" . $value['id'] . "' selected>" . $value['route_no'] . "</option>";
                            } else {
                                echo "<option value='" . $value['id'] . "'>" . $value['route_no'] . "</option>";
                            }

                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class='col_one_third col_last' style="display: flex;justify-content: flex-end;margin-top: -1rem;">
                <div class='input-field first-wrap seat-request-input-box' style="width: 10%;">
                    <?php echo CHtml::submitButton("Search", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="col_full page_header_div heading-label-responsive border-round-top">
        <h4 class="heading-custom page_header_h4">Email & Sms List</h4>
    </div>

    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'email-sms-grid',
        'itemsCssClass' => 'table cart',
        'rowCssClass' => array('cart_item'),
        'htmlOptions' => array('class' => 'table-responsive bottommargin table-overflow-x admin-table-responsive border-round-bottom'),
        'dataProvider' => $model->search(),
        //'filter'=>$model,
        'columns' => array(
            array(
                'name' => 'id',
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
                'htmlOptions' => array('class' => 'table_data_left'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'route_id',
                'type' => 'raw',
                'value' => '$data->route_id==0?"ALL":Stoppage::routeNo($data->route_id)',
                'htmlOptions' => array('class' => 'table_data_left'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'template_id',
                'type' => 'raw',
                'value' => '$data->template($data->template_id)',
                'htmlOptions' => array('class' => 'table_data_left'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'sub',
                'type' => 'raw',
                'value' => '$data->sub',
                'htmlOptions' => array('class' => ' table_data_left'),
                'headerHtmlOptions' => array('class' => 'cart-product-price '),
            ),
            array(
                'name' => 'body',
                'type' => 'raw',
                'value' => 'nl2br($data->body)',
                'htmlOptions' => array('class' => ' table_data_left'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'type',
                'type' => 'raw',
                'value' => '$data->type($data->type)',
                'htmlOptions' => array('class' => 'table_data_left'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            //'created_by',
            //'created_time',
            /*
            'updated_by',
            'updated_time',
            'type',
            */
            //array(
            // 'class'=>'CButtonColumn',
            //),
        ),
    )); ?>
    <div class="center s002 export_to_excel_button button-pink">
        <?php
        echo CHtml::normalizeUrl(CHtml::link('Export to Excel',
            array('excel', 'criteria' => $_GET['EmailSms'])));
        ?>
    </div>

</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',
        });

</script>