<?php
/* @var $this EmailSmsController */
/* @var $model EmailSms */
/* @var $form CActiveForm */

?>

<div class="center s002 searchbar" style="margin-bottom: 40px;">


    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'email-sms-form',
        'enableAjaxValidation' => false,
        'htmlOptions'=>array(
            'style'=>'margin: 2rem;',
        ),
    )); ?>

    <p class="note" style="float: left;">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="inner-form seat-request-form">
        <div class="col_one_fourth seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'template_id'); ?></div>
            <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                        </path>
                    </svg>
                </div>
                <?php $temp = Template::model()->findAll(array('order' => 'id ASC')); ?>
                <select data-trigger="" name="EmailSms[template_id]" id="template" onchange="templateView()">
                    <option value='' selected="selected">A Template Select</option>
                    <?php foreach ($temp as $key => $value) {

                        echo "<option value='" . $value['id'] . "'>" . $value['title'] . "</option>";


                    }
                    ?>
                </select>

            </div>
        </div>

        <div class="col_one_fourth seat-request-form-box">

            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'sub'); ?></div>
            <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'sub'); ?>
                <?php echo $form->error($model, 'sub'); ?>
            </div>
        </div>
        <div class="col_one_fourth seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'route_id'); ?></div>

            <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                        </path>
                    </svg>
                </div>
                <?php $route = Routes::model()->findAll(array('order' => 'id ASC')); ?>
                <select data-trigger="" name="EmailSms[route_id]" id="routeList">
                    <option value=''>A Route Select</option>
                    <option value='10000'>ALL</option>
                    <?php foreach ($route as $key => $value) {
                        if (isset($_GET['route_id']) && $_GET['route_id'] == $value['id']) {
                            echo "<option value='" . $value['id'] . "' selected>" . $value['route_no'] . "</option>";
                        } else {
                            echo "<option value='" . $value['id'] . "'>" . $value['route_no'] . "</option>";
                        }

                    }
                    ?>
                </select>

                <?php echo $form->error($model, 'route_id'); ?>
            </div>
            <div style="float: right;margin: -25px 0 0 0;">
                <i class="icon-plus" onclick="routeAdd()"></i>
                <i class="icon-tasks" onclick="selectRoutes()"></i>
            </div>
        </div>
        <div class="col_one_fourth seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'type'); ?></div>
            <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                        </path>
                    </svg>
                </div>
                <select data-trigger="" name="EmailSms[type]">
                    <option value='3' selected="selected">Email & SMS</option>
                    <option value='1'>Email</option>
                    <option value='2'>SMS</option>

                </select>

            </div>
        </div>
    </div>
    <!--<div class="inner-form route-form">

    </div>-->
    <div id="select_route" class="col_full">
        <table class="table table-responsive cart">
            <div>
                <button form='form' onclick='routeDelete(10000)'
                        style="width: 10%;float: right;background: #5f5f5f !important;color: #fff;padding: 10px;border-radius: 7px;margin-bottom: 10px;font-weight: 800;">
                    Clear List
                </button>
            </div>
            <thead class="tbl-header" style="background: #fff">
            <tr>
                <th>Route &nbsp;&nbsp;

                </th>
                <th>Stoppages</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="route_list">


            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div class="inner-form route-form">
        <div class="col_one_fourth route-form-box subject-input-form">
            <div class="seat-request-label col_one_third"></div>
            <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                <!--<div class="icon-wrap">

                </div>-->
                <?php echo $form->textArea($model, 'body', array('rows' => 15, 'cols' => 125, 'placeholder' => 'Enter template body here....')); ?>
                <?php echo $form->error($model, 'body'); ?>
            </div>
        </div>
    </div>


    <div class="col_full">
        <div class="input-field first-wrap form-save-button" style="margin: 0 -13px -6px 0;width: 12%;">
            <?php echo CHtml::submitButton("Save", array('class' => 'btn-search', 'id' => 'search-button')); ?>
        </div>

    </div>

    <?php $this->endWidget(); ?>

</div>

<!-- form -->
<style type="text/css">
    #select_route {
        display: none;
        background: #fff !important;
    }
</style>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',
        });

</script>
<script>
    function templateView() {
        var templateId = $("#template").val();
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/emailSms/template");?>', {templateId: templateId}, function (data) {
            var template = jQuery.parseJSON(data);
            if (template.hasOwnProperty('error')) {
                //openModel('<h3>' + template.error + '</h3>');
                bsModalOpen('<h3>' + template.error + '</h3>');
            }
            else {

                $("#EmailSms_sub").val(template.sub);
                $("#EmailSms_body").val(template.body);

            }

        });
    }

    function routeAdd() {

       /* var r = $("#routeList option:selected").text();
        var rid = $("#routeList option:selected").val();*/
       /* if (r=='0') {
            alert("A Route Select ");
        }*/
        var r = $("#routeList option:selected").text();
        var rid = $("#routeList option:selected").val();

        if (r == "A Route Select") {
            alert(r);
        }
        else {
            $.post('<?php echo Yii::app()->createAbsoluteUrl("/emailSms/routeList");?>', {
                r: r,
                rid: rid
            }, function (data) {
                $("#route_list").html(data);
            });
        }
    }

    function selectRoutes() {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/emailSms/routeView");?>', {}, function (data) {
            $("#route_list").html(data);
        });
        var v = jQuery("#select_route").toggle();
    }

    function routeDelete(key) {
        $.post('<?php echo Yii::app()->createAbsoluteUrl("/emailSms/routeDelete");?>', {rid: key}, function (data) {
            $("#route_list").html(data);
        });
    }


</script>