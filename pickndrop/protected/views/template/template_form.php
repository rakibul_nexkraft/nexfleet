<div class="s002 searchbar ">


    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'template-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('onsubmit' => 'return false'),
        )); 
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php /*echo $form->errorSummary($model); */
    ?>
    <div style="">
        <div class="inner-form zone-form">
            <div class="col_full">
                <div class="seat-request-label "><?php echo $form->labelEx($model, 'title'); ?></div>
                <div class="input-field first-wrap zone-search-field ">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
                </div>
                <?php echo $form->error($model, 'title'); ?>
            </div>
            <div class="col_full">
                <div class="seat-request-label"><?php echo $form->labelEx($model, 'sub'); ?></div>
                <div class="input-field first-wrap zone-search-field">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'sub', array('size' => 60, 'maxlength' => 255)); ?>
                </div>
                <?php echo $form->error($model, 'sub'); ?>
            </div>
        </div>
    </div>
    
        <div class="inner-form zone-form">
            <div class="col_full">
                <div class="seat-request-label "><?php echo $form->labelEx($model, 'body'); ?></div>
                <div class="input-field first-wrap zone-search-field " style="border: 0 !important;">
                    <div class="icon-wrap">
                        
                    </div>
                    <?php echo $form->textArea($model, 'body', array('rows' => 10, 'cols' => 134,'placeholder' => 'Enter template body here....', 'style'=>"border: 1px solid #ddd;border-radius: 5px;width: 100%;")); ?>
                </div>
               <?php echo $form->error($model, 'body'); ?>
            </div>
           
        </div>
   

    <div class="inner-form">
        <div class="col_full">
            <div class="input-field first-wrap form-save-button" style="border: none !important;">
                <?php echo CHtml::submitButton("Save", array('class' => 'btn-search button-pink', 'id' => 'search-button','onclick'=>'submitTempalte()')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<script>
    function submitTempalte(){
        $("#template-form").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/template/createData");?>', 
            type: 'post', 
            success:  function(data) { 
                if(data==1) {
                    //openModel("Data Save Successfully!");
                    bsModalOpen("Template is created successfully!");
                }
                else {
                    $("#template-div").html(data);
                }         
            }
        });
    }
</script>
