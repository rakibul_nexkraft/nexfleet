<?php
/* @var $this TemplateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Templates',
);


?>
<div id="route-request-list" class="email-sms">
    <!-- <div class="center s002 ">
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                 class="floating-menu dropdown-toggle animated fadeInRight">
                
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(

                        array('label' => 'Send Email & SMS', 'url' => array('/emailSms/create')),
                        array('label' => 'Create Template', 'url' => array('/template/create')),
                    ),
                ));

                ?>
            </div>

        </div>
    </div> -->

    <!-- Floating Menu Button -->


    <div class="center s002 searchbar">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'enableAjaxValidation' => true,
            'method' => 'get',
        )); ?>

        <div class="inner-form seat-request-form" style="padding-left: 10px;padding-right: 10px">
            <div class='col_one_third seat-request-form-box'>
                <div class="seat-request-label col_one_third">Id</div>
                <div class="input-field first-wrap col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'id', array('placeholder' => 'ID'));
                    ?>

                </div>
            </div>
            <div class='col_one_third seat-request-form-box'>
                <div class="seat-request-label col_one_third">Template Title</div>
                <div class="input-field fouth-wrap col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>

                    <?php echo $form->textField($model, 'title', array('placeholder' => 'Title')); ?>

                </div>
            </div>


            <div class='col_one_third col_last' style="display: flex;justify-content: flex-end;margin-top: -1rem;">
                <div class='input-field first-wrap' style="width: 10%;">
                    <?php echo CHtml::submitButton("Search", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">
        <h4 class="heading-custom page_header_h4">Templates List</h4>
    </div>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'stoppage-grid',
        'itemsCssClass' => 'table cart',
        'htmlOptions' => array('class' => 'table-responsive bottommargin table-overflow-x border-round-bottom'),
        'rowCssClass' => array('cart_item'),
        'dataProvider' => $model->search(),
        //'filter' => $model,
        'pager' => array(
            'firstPageLabel' => 'First',
            'lastPageLabel' => 'Last',
        ),
        'columns' => array(

            array('name' => 'id',
                'type' => 'raw',
                'value' => 'CHtml::link(CHtml::encode($data->id),array("view", "id"=>$data->id))',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array('name' => 'title',
                'type' => 'raw',
                'value' => '$data->title',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array('name' => 'sub',
                'type' => 'raw',
                'value' => '$data->sub',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            //'route_id',
            array(
                'name' => 'body',
                'type' => 'raw',
                'value' => 'nl2br($data->body)',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),


            array(
                'name' => 'created_by',
                'type' => 'raw',
                'value' => '$data->created_by',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),

            array(
                'name' => 'created_time',
                'type' => 'raw',
                'value' => '$data->created_time',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),


            //'updated_by',
            //'updated_time',

            //array(
            //'class'=>'CButtonColumn',
            //),
        ),
    ));
    ?>
    <div class="s002 export_to_excel_button export_to_excel_button_rout button-pink">
        <?php
        echo CHtml::normalizeUrl(CHtml::link('Export to Excel', array('excel', 'criteria' => $_GET['Template'])));
        ?>
    </div>
</div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>


<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',
        });

</script>
