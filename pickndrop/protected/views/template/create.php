<?php
/* @var $this TemplateController */
/* @var $model Template */

$this->breadcrumbs = array(
    'Templates' => array('index'),
    'Create',
);


?>
    <div class="center s002 ">
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                 class="floating-menu dropdown-toggle animated fadeInRight">
                <!--            <label class="floating-menu-label">Menu</label>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Send Email & SMS', 'url' => array('/emailSms/create')),
                        array('label' => 'Email & SMS List', 'url' => array('/template/create')),
                        array('label' => 'Template List', 'url' => array('/template/index')),
                    ),
                ));

                ?>
            </div>
        </div>
    </div>
    <!-- Floating Menu Button -->
    <div class="col_full page_header_div" style="border-bottom: 1px solid #f2ecec;">
        <h4 class="heading-custom page_header_h4">Create New Template</h4>
    </div>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>