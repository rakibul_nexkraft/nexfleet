<?php
/* @var $this TemplateController */
/* @var $model Template */
/* @var $form CActiveForm */
?>

<div class="s002 update-form-background">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'template-form',
        'enableAjaxValidation' => false,

    )); ?>
    <p class="note" style="float: left;">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="inner-form seat-request-form" style="margin: 0 0 0 -155px;">
        <div class="col_full">
            <div class="col_half seat-request-form-box">
                <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'title'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'title'); ?>
                </div>
            </div>
            <div class="col_half seat-request-form-box">
                <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'sub'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'sub', array('size' => 60, 'maxlength' => 255)); ?>
                    <?php echo $form->error($model, 'sub'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="inner-form" style="margin: 0px 1px 0 -267px;">
        <div class="col_one">
            <div class="seat-request-label col_one_third"></div>
            <div class="input-field first-wrap col_two_third">
                <!--<div class="icon-wrap">

                </div>-->
                <?php echo $form->textArea($model, 'body', array('rows' => 15, 'cols' => 125,'placeholder' => 'Enter template body here....')); ?>
                <?php echo $form->error($model, 'body'); ?>
            </div>
        </div>
    </div>
    <div class="inner-form">
        <div class="col_full" style="margin: 0 -77px -20px 120px;">
            <div class="input-field first-wrap form-save-button">
                <?php echo CHtml::submitButton("Save", array('class' => 'btn-search', 'id' => 'search-button')); ?>
            </div>

        </div>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->
