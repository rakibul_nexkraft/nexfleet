<?php
/* @var $this CommonfleetsController */
/* @var $model Commonfleets */

$this->breadcrumbs=array(
	'Seat Allocation'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>' Seat Allocations List', 'url'=>array('index')),
	array('label'=>'Manage Seat Allocations', 'url'=>array('admin')),
	array('label'=>'Routes List', 'url'=>array('routes/index')),
);
?>

<h4>New Seat Allocation</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model,'application_type'=>$application_type,'approve_status'=>$approve_status)); ?>