<br />
To: <?php echo $model->user_name ?><br />
<br />
Ref: Transport Requisition Request Notification - Serial No# <?php echo $model->id; ?><br />
<br />
<br />
Dear Concern,<br />
<br />
This is to let you know that your application for changing the route of staff bus has been received by Transport Department. 
You are requested to collect Boarder Pass from Transport Department for your desired route. You are further requested to return the existing Boarder Pass to Transport Department. 
Please find below your seat allocation details:<br />
<br />
Vehicle No: <?php echo $model->vehicle_reg_no; ?><br />
<br />
Vehicle Type: <?php echo $model->vehicletypes->type; ?><br />
<br />
New Route No: <?php echo $model->present_route; ?><br />
<br />
<br />
Regards,<br />
Transport Department<br />
<br />
<br />
P.S. This is a computer generated document, hence, required no signature.