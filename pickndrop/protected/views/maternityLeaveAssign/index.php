<?php
/* @var $this MaternityLeaveAssignController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Maternity Leave Assigns',
);

$this->menu=array(
	array('label'=>'Create Maternity Leave Assign', 'url'=>array('create')),
	array('label'=>'Manage Maternity Leave Assign', 'url'=>array('admin')),
);
?>

<h1>Maternity Leave Assign</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
