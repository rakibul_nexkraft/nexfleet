<?php
/* @var $this HomePageNotificationController */
/* @var $model HomePageNotification */

$this->breadcrumbs=array(
	'Home Page Notifications'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List HomePageNotification', 'url'=>array('index')),
	array('label'=>'Create HomePageNotification', 'url'=>array('create')),
	array('label'=>'Update HomePageNotification', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete HomePageNotification', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage HomePageNotification', 'url'=>array('admin')),
);
?>

<h1>View HomePageNotification #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'notice',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
