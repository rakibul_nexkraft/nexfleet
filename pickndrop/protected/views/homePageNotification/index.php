<?php
/* @var $this HomePageNotificationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Home Page Notifications',
);

$this->menu=array(
	array('label'=>'Create HomePageNotification', 'url'=>array('create')),
	array('label'=>'Manage HomePageNotification', 'url'=>array('admin')),
);
?>

<h1>Home Page Notifications</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
