
    <?php
    /* @var $this UserFeedbackController */
    /* @var $model UserFeedback */

    $this->breadcrumbs=array(
        'User Feedbacks'=>array('index'),
        $model->id,
    );

    $this->menu=array(
        array('label'=>'List UserFeedback', 'url'=>array('index')),
        array('label'=>'Create UserFeedback', 'url'=>array('create')),
        array('label'=>'Update UserFeedback', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Delete UserFeedback', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Manage UserFeedback', 'url'=>array('admin')),
    );
    ?>

<section style="background: #fff !important; text-align: center; box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03), 0 0.9375rem 1.40625rem rgba(4,9,20,0.03), 0 0.25rem 0.53125rem rgba(4,9,20,0.05), 0 0.125rem 0.1875rem rgba(4,9,20,0.03); border-radius: 5px; width: 60%; margin-left: 230px; margin-top: 80px;">
    <div >

        <h1>View UserFeedback #<?php echo $model->id; ?></h1>
        <div style="font-size: 12px;font-weight:400; padding-left: 25px; height: auto;">
            <?php $this->widget('zii.widgets.CDetailView', array(
                'htmlOptions' => array('class' => 'view-table'),
                'data'=>$model,
                'attributes'=>array(
                    'id',
                    'feedback',
                    'created_by',
                    'created_time',
                    'updated_by',
                    'updated_time',
                ),
            )); ?>
        </div>
    </div>

</section>