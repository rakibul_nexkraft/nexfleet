<?php
/* @var $this RouteFactorController */
/* @var $data RouteFactor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level_from')); ?>:</b>
	<?php echo CHtml::encode($data->level_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('level_to')); ?>:</b>
	<?php echo CHtml::encode($data->level_to); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('multiply')); ?>:</b>
	<?php echo CHtml::encode($data->multiply); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_time')); ?>:</b>
	<?php echo CHtml::encode($data->created_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_time')); ?>:</b>
	<?php echo CHtml::encode($data->updated_time); ?>
	<br />

	*/ ?>

</div>