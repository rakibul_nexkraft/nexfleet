<?php
/* @var $this RouteFactorController */
/* @var $model RouteFactor */

$this->breadcrumbs=array(
	'Route Factors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List RouteFactor', 'url'=>array('index')),
	array('label'=>'Manage RouteFactor', 'url'=>array('admin')),
);
?>

<h1>Create RouteFactor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>