<?php
/* @var $this RouteFactorController */
/* @var $model RouteFactor */
/* @var $form CActiveForm */
?>



<div class="clo_full s002 stoppage_update stoppage-uptade update-form-background">

  
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'route-factor-form',
	'enableAjaxValidation'=>false,
	)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php /*echo $form->errorSummary($model); */
    ?>


    <div class="inner-form seat-request-form">
        <div class="col_one_third seat-request-form-box">
                <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'level_from'); ?></div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'level_from', array('size' => 20, 'maxlength' => 20)); ?>
                </div>
                <?php echo $form->error($model, 'level_from'); ?>
        </div>

        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'level_to'); ?></div>
            <div class="input-field first-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'level_to', array('size' => 20, 'maxlength' => 20)); ?>
            </div>
            <?php echo $form->error($model, 'level_to'); ?>
        </div>


        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'multiply'); ?></div>
            <div class="input-field first-wrap seat-request-input-box col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
               <?php echo $form->textField($model, 'multiply', array('size' => 20, 'maxlength' => 20)); ?>
               
            </div>
            <?php echo $form->error($model, 'multiply'); ?>
        </div>
    </div>

    <div class="col_full" style="width: 15%;float: right;margin: 0 0 -32px 0;">
        <div class="input-field first-wrap searchbar-search-button">
            <?php echo CHtml::submitButton("Save", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
        </div>

    </div>

    <?php $this->endWidget(); ?>

</div>