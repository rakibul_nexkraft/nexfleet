<?php
/* @var $this RouteFactorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Route Factors',
);
?>
<div id="route-request-list">
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">
        <h4 class="heading-custom page_header_h4">Route Factors</h4>
    </div>

	<?php $this->widget('zii.widgets.grid.CGridView', array(
	    'id'=>'zone-grid',
	    'itemsCssClass' => 'table cart',
	    'htmlOptions' => array('class' => 'table-responsive bottommargin table-overflow-x zone-grid-responsive admin-table-responsive border-round-bottom'),
	    'rowCssClass'=>array('cart_item'),
	    'dataProvider'=>$model->search(),
	    'pager' => array(
	    'firstPageLabel'=>'First',
	    'lastPageLabel'=>'Last',
	     ),
	    'filter'=>$model,

	    'columns'=>array(
	         array(
	            'header' => 'ID',
	            'type' => 'raw',
	            'value' => '$data->id',
	            'htmlOptions'=>array('class'=>'cart-product-price'),
	                'headerHtmlOptions'=>array('class'=>'cart-product-price'),
	        ),
	        array(
	            'header' => 'Level From',
	            'type' => 'raw',
	            'value' => '$data->level_from',
	            'htmlOptions'=>array('class'=>'cart-product-price'),
	            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
	        ),
	       
	        array(
	            'header' => 'Level To',
	            'type' => 'raw',
	            'value' => '$data->level_to',
	            'htmlOptions'=>array('class'=>'cart-product-price'),
	            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
	        ),
	         array(
	            'header' => 'Base Fair Multiply By',
	            'type' => 'raw',
	            'value' => '$data->multiply',
	            'htmlOptions'=>array('class'=>'cart-product-price'),
	            'headerHtmlOptions'=>array('class'=>'cart-product-price'),
	        ),
	        
	       array(
	           'class'=>'CButtonColumn',
	           'template'=>'{update},{delete}',
	           'buttons'=>array(
	             'update'=>array(
	                  'label'=>'UPDATE',
	              ),
	             'delete'=>array(
	                  'label'=>'DELETE',                  
	                'visible'=>'Yii::app()->user->username=="admin"?true:false',
	              ),
	            ),
	           
	           //'deleteButtonImageUrl'=>Yii::app()->baseUrl.'/image/delete.png',
	           //'updateButtonImageUrl'=>Yii::app()->baseUrl.'/image/update.jpg',
	        ),
	    ),
	    
	)); ?>
</div>
