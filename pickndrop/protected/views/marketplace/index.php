
<!--<pre>< ?php print_r($userSeat->attributes) ?></pre>-->
<section id="page-title" class="page-title-center" >
    <div class="clearfix">
        <h1>Marketplace</h1>
        <span>Available Seat: <?php echo $seatCount; ?></span>

    </div>
</section>

<?php if (isset($userSeat)): ?>
    <section id="page-title" class="page-title-center page-title-nobg noborder" >
        <div class="col_half ">
            <ul class="list-group" style="text-align: left">
                <li class="list-group-item"><strong>MY ROUTE:</strong> <span class="" style="margin-top: 0;"><?php echo empty($userSeat['present_route'])?'Not Assigned':$userSeat['present_route']; ?></span></li>
                <li class="list-group-item"><strong>ROUTE DETAIL:</strong> <span class="" style="margin-top: 0;"><?php echo empty($userSeat['route_detail'])?'Not Assigned':$userSeat['route_detail']; ?></span></li>
            </ul>
        </div>
        <div class="col_half col_last">
            <div class="col_half" >
                <a class="button button-border button-rounded " style="width: 190px;height: 40px;" href="<?php echo Yii::app()->baseUrl;?>/index.php/marketplace/bookUserSeat">
                    Book Seat
                </a>
            </div>
            <div class="col_half col_last">
                <a class="button  button-border button-rounded " style="width: 190px;height: 40px;" href="<?php echo Yii::app()->baseUrl;?>/index.php/marketplace/sellUserSeat">
                    Sell Seat
                </a>
            </div>
            <div class="clear"></div>
            <!--<div class="col_half">
                <a class="button  button-border button-rounded " style="width: 190px;height: 40px;" href="<?php /*echo Yii::app()->baseUrl;*/?>/index.php/marketplace/sellHistory">
                    Sell History
                </a>
            </div>
            <div class="col_half col_last">
                <a class="button  button-border button-rounded " style="width: 190px;height: 40px;" href="<?php /*echo Yii::app()->baseUrl;*/?>/index.php/marketplace/bookingHistory">
                    Booking History
                </a>
            </div>-->
        </div>
    </section>
<?php else: ?>
    <section id="page-title" class="page-title-center page-title-nobg noborder" >
        <div class="clearfix">
            <span>Currently you have no allocated seat. </span>
            <ol class="breadcrumb">
                <li><?php echo CHtml::link("Request A Seat",array("seatRequest/index"),array("class"=>"btn btn-success"));?></li>
            </ol>
        </div>
    </section>
<?php endif; ?>