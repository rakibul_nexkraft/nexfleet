<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
<style>
    .fs-12{
        font-size: 12px;
    }
</style>
<div id="route-request-list" class="custom-box-design" style="margin-left: 22px;">
    <div class="center s002">

        <h4 class="heading-custom uppercase heading-block user-history-heading-text " style="background: transparent !important;">BOOKING HISTORY</h4>

    </div>

    <div class="table-responsive bottommargin" style="border: none !important; background: transparent !important;">

        <table class="table cart">
            <thead>
            <tr>
                <th class="cart-product-price">Booking Date</th>
                <th class="cart-product-price">ID</th>
                <th class="cart-product-price">Route</th>
                <th class="cart-product-price">Zone</th>
                <th class="cart-product-price">Release Date</th>
                <th class="cart-product-price">Travel Direction</th>
                <th class="cart-product-price">Price</th>
                <!--                    <th class="cart-product-price">Status</th>-->
                <th class="cart-product-price">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($model) > 0): ?>
                <?php foreach ($model as $index => $booking): ?>
                    <tr class="cart_item">
                        <td class="cart-product-price">
                            <?php
                            $created_time = date_create($booking->created_time);
                            echo date_format($created_time,"d/m/Y h:i A");
                            //                            echo $booking->created_time; ?>
                        </td>
                        <td class="cart-product-price"><?php echo $booking->id; ?></td>
                        <!--                        <td class="cart-product-price">< ?php echo 'Route No: '.$booking->route_id.'<br> Details: '.$booking->route_detail; ?></td>-->
                        <td class="cart-product-price"><?php echo $booking->route_id; ?></td>
                        <td class="cart-product-price"><?php echo $booking->zone_id; ?></td>
                        <td class="cart-product-price"><?php echo $booking->release_date; ?></td>
                        <td class="cart-product-price"><?php echo $booking->travel_direction; ?></td>
                        <td class="cart-product-price"><?php echo $booking->price; ?></td>
                        <!--                        <td class="cart-product-price">< ?php echo $booking->status?'<span class="button button-3d button-rounded button-green button-mini">Sold</span>':'<span class="button button-3d button-rounded button-red button-mini">Unsold</span>'; ?></td>-->
                        <td class="cart-product-price">
                            <a href="<?php echo Yii::app()->createUrl('marketplace/bookingDetails', array('id' => $booking->id)); ?>" class="button button-rounded button-green button-mini"><i class="icon-edit"></i><span>Show Detail</span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr style="text-align: center;">
                    <td colspan="7" class="empty" style="padding-top: 30px; padding-bottom: 30px; border: none !important; padding-left: 95px;"><span class="empty no-resultfound-text">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
        <div class="keys" style="display:none" title="/pickndrop/index.php/marketplace/sellHistory"></div>
    </div>
</div>


<div class="modal fade" id="marketplaceUpdateModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Request</h4>
            </div>
            <div class="modal-body">
                <div style='text-align: center;'>
                    <input type='radio' name='request' value='-1'> Cancel my seat release<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px' />
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>


<script>
    var showModal = function (marketplaceId) {
        console.log(marketplaceId);
        $('#marketplaceUpdateModal').modal({
            show: true
        });
    };
    function routeInformationUpdate(requestId){

        var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Choose Your Request</h5><div id='notice'></div><input type='radio' name='request' value='0'>Request For Approved<br><input type='radio' name='request' value='3'>Request For Cancel<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);

    }
    function submitRequestUpdated(requestId){
        var v=$("input[name='request']:checked").val();
        if(v==null){
            $("#notice").html("<h5 style='color:red;margin-bottom:5px;'>Please, Select A Request</h5>");
        }
        else{

            $.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>',{requestId:requestId,updateRequest:v},function(data){
                bsModalOpen(data);
                //alert(data);
            });

        }
    }

</script>