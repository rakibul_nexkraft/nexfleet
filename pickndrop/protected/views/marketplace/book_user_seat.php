<div class="s002">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'book-user-seat-form',
        'enableAjaxValidation'=>true,
        'htmlOptions'=>array(
                'onsubmit'=>'return validateForm();'
        )
    )); ?>

    <div class="inner-form">
        <div class="input-field first-wrap">
            <div class="icon-wrap">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                    </path>
                </svg>
            </div>
            <?php echo $form->textField($model,'start_point',array('placeholder'=>'Start point','id'=>"start_point",'list'=>'browsers'));
            ?>
        </div>
        <div class="input-field first-wrap">
            <div class="icon-wrap">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                    </path>
                </svg>
            </div>
            <?php echo $form->textField($model,'end_point',array('placeholder'=>'End point','id'=>"end_point",'list'=>'browsers')); ?>
        </div>
        <?php $stoppages=Stoppage::model()->findAll();?>
        <datalist id="browsers">
            <?php
            foreach ($stoppages as $key => $value) {
                echo "<option value='".$value['stoppage']."'>";
            }
            ?>
        </datalist>
        <?php
        $slot_data = $this->checkNextSlotAndCommuteDirection();
        $model->travel_direction = $slot_data->travel_direction;
        echo $form->hiddenField($model,'travel_direction',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1)); ?>
        <div class="input-field first-wrap">
            <div class="icon-wrap ">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                    </path>
                </svg>
            </div>
            <?php
            $model->release_date = $slot_data->date;
            echo $form->hiddenField($model,'release_date',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
            echo $form->textField($model,'release_date',array('placeholder'=>'Release Date','id'=>'depart','class'=>'datepicker', 'disabled'=>'disabled')); ?>
        </div>
        <div class="input-field fouth-wrap">
            <div class="icon-wrap">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                    </path>
                </svg>
            </div>
            <?php  $sql_vehicle_type = "SELECT * FROM tbl_vehicletypes";
            $command = Yii::app()->db->createCommand($sql_vehicle_type)->queryAll();
            ?>
            <select data-trigger="" name="Marketplace[vehicle_type_id]">
                <option value=''>Vehicle Type</option>
                <?php foreach($command as $key=>$value)
                    echo "<option value='".$value['id']."'>".$value['type']."</option>";
                ?>
            </select>
        </div>
        <div class="input-field first-wrap">
            <?php  echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<?php if($this->checkNextSlotAndCommuteDirection()->slot_open): ?>
    <?php if(isset($routes_rs)): ?>
        <div id="route-list">
            <div class="center s002 ">

                <h4 class="heading-custom">AVAILABLE SEATS</h4>

            </div>

            <div class="table-responsive bottommargin">

                <table class="table cart">
                    <thead>
                    <tr>
                        <th class="cart-product-price">Travel Direction</th>
                        <th class="cart-product-price">Route No</th>
                        <th class="cart-product-price">Route Detail</th>
                        <th class="cart-product-price">Vehicle Type</th>
                        <th class="cart-product-price">Total Seat</th>
                        <!--<th class="cart-product-price">Available Seat</th>-->
                        <!--<th class="cart-product-price">Waiting</th>-->
                        <th class="cart-product-price">Price</th>
                        <th class="cart-product-price"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($routes_rs)>0){
                        foreach($routes_rs as $key=>$value){

                            ?>
                            <tr class="cart_item">
                                <td  class="cart-product-price">
                                    <?php echo $value['travel_direction'];?>
                                </td >
                                <td  class="cart-product-price">
                                    <?php echo $value['route_no'];?>
                                </td >

                                <td  class="cart-product-price">
                                    <?php
                                    $stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='".$value['route_id']."' ORDER BY pickup_time";
                                    $stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();
                                    $stoppage_id=0;
                                    if(count($stoppage_rs)>0){
                                        foreach ($stoppage_rs as $key1 => $value_stoppage) {
                                            if($value_stoppage['stoppage']==$model->start_point || $value_stoppage['stoppage']==$model->end_point){
                                                echo "<span style='color:#47ae4b'>".$value_stoppage['stoppage']."</span>";
                                                $stoppage_id=$value_stoppage['id'];
                                            }
                                            else{
                                                echo $value_stoppage['stoppage'];
                                            }

                                            if(count($stoppage_rs)>$key1+1)echo "-";
                                        }
                                    }
                                    //echo $value['route_detail'];?>
                                </td >

                                <td  class="cart-product-price">
                                    <?php echo $value['type'];?>
                                </td >

                                <td  class="cart-product-price">
                                    <?php echo $total_seat=$value['actual_seat'];?>
                                </td >

                                <!--<td  class="cart-product-price">
                                    < ?php
                                    $borderPass=Routes::borderPass($value['route_no']);
                                    echo $av_seat=$total_seat-$borderPass;
                                    ?>

                                </td >-->

                                <!--<td  class="cart-product-price">
                                    < ?php $sql_queue="SELECT max(queue) as maxqueue FROM tbl_seat_request WHERE status=1 AND route_id='".$value['id']."' AND queue>=0";
                                    $queue_rs=Yii::app()->db->createCommand($sql_queue)->queryRow();
                                    if($av_seat>0){
                                        echo 0;
                                    }
                                    else{
                                        echo $queue_rs['maxqueue']+1;
                                    }

                                    ?>
                                </td >-->
                                <td  class="cart-product-price">
                                    <?php echo $value['price'];?>
                                </td >
                                <td  class="cart-product-price">
                                    <div class="input-field first-wrap">
                                        <?php  echo CHtml::submitButton("Continue",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'getRouteDetails('.$value["id"].','.$value["route_id"].','.$stoppage_id.',"'.$model->release_date.'")')); ?>
                                    </div>
                                </td >
                            </tr>
                        <?php }
                    }
                    else{


                        ?>
                        <tr>
                            <td  colspan="9" class="cart-product-price">
                                No Route Found!
                            </td >
                        </tr>
                    <?php } ?>

                    </tbody>

                </table>

            </div>

        </div>
    <?php endif;?>
<?php else: ?>
    <div class="col_full">
        <div class="input-field first-wrap">
            <?php
            echo '<div class="col_full"><div class="style-msg errormsg"><div class="sb-msg"><i class="icon-remove"></i><strong>Oh snap!</strong> The current slot is closed. Please check the next slot and try again. </div></div></div>';
            /*if($slot_data->slot_open){
                echo CHtml::submitButton("Submit",array('class'=>'btn-search','id'=>'search-button'));
            } else {
                echo "<span class='btn btn-danger w100'>".$slot_data->travel_direction."</span>";
            }*/ ?>
        </div>
    </div>
<?php endif; ?>
<div id="submit-information">
    <!--<div class="center s002 ">

	   		<h4 class="heading-custom">SUBMIT YOUR INFORMATION</h4>

	   	</div>
	   	<div class="accordion accordion-border clearfix">
	   		<div class="col_one_third">
	   			<div class="information-box">
	   				<div class="steering clearfix">
	   					<img  src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/car/steering.jpg" alt="Steering">
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">

	   					<div class="seat-location">1</div>
	   					<div class="seat-location">2</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location">3</div>
	   					<div class="seat-location">4</div>

	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">

	   					<div class="seat-location">5</div>
	   					<div class="seat-location">6</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location">7</div>
	   					<div class="seat-location">8</div>

	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">

	   					<div class="seat-location">9</div>
	   					<div class="seat-location">10</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location">11</div>
	   					<div class="seat-location">12</div>

	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">
	   					<div class="seat-location-white">13</div>
	   					<div class="seat-location-white">14</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location-white">15</div>
	   					<div class="seat-location-white">16</div>
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">
	   					<div class="seat-location-white">17</div>
	   					<div class="seat-location-white">18</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location-white">19</div>
	   					<div class="seat-location-white">20</div>
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">
	   					<div class="seat-location-white">21</div>
	   					<div class="seat-location-white">22</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location-white">23</div>
	   					<div class="seat-location-white">24</div>
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="min-hight"></div>
	   			</div>
	   		</div>
	   		<div class="col_one_third">
	   			<div class="information-box">
	   					<div class="center s002 ">

   							<h4 class="heading-custom">Route 01  Stoppages List</h4>

   						</div>
   						<div class="clearfix"></div>
   						<div class="stoppage-table">
   							<div class="table-responsive">

						<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-price"><strong>Stoppage</strong></th>
									<th class="cart-product-price"><strong>Time</strong></th>

								</tr>
							</thead>
							<tbody>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mirpur
									</th >

									<th  class="cart-product-price">
										7:30
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Framgate
									</th >

									<th  class="cart-product-price">
										8:00
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mohakhali
									</th >

									<th  class="cart-product-price">
										8:30
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mirpur
									</th >

									<th  class="cart-product-price">
										7:30
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Framgate
									</th >

									<th  class="cart-product-price">
										8:00
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mohakhali
									</th >

									<th  class="cart-product-price">
										8:30
									</th >

								</tr>

							</tbody>

						</table>

					</div>
   						</div>
	   			</div>
	   		</div>
	   		<div class="col_one_third col_last">
	   			<div class="information-box-last">
	   				<div class="driver-info">
		   				<div class="driver-info-row">
		   					<strong>Driver Name :</strong> Joynal Hassan
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Driver Pin :</strong>  012548963
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Driver Phone :</strong>  016848886958
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Route No:</strong>  02
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Price :</strong> 60
		   				</div>
	   				</div>

	   			</div>
	   			<div class="information-submit-button-div">
	   				<?php  //echo CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1')); ?>
	   			</div>
	   		</div>

	   	</div>-->

</div>

<!-- Modal -->
<div class="modal fade" id="bookingSuccessModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class=""  id="bookingMessage"  style="text-align: center">
                </div>
                <div class="clearfix"></div>
                <div id="bookingButton">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="closeValidationModal()">Close</button>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: false,
            itemSelectText: '',
        });

</script>
<script type="text/javascript">
    var validateForm = function () {
        var start_point = $('#start_point').val();
        var end_point = $('#end_point').val();
        if (start_point.trim() == '' && end_point.trim() == ''){
            $('#bookingMessage').html('<h4 style="margin: 30px auto">Please select <strong style="color: #089D49">Start point</strong> or <strong style="color: #089D49">End point</strong></h4>');
            $("#bookingSuccessModal").modal({
                backdrop: "static",
                show: true
            });
            return false;
        }
        return true;
    };
    var closeValidationModal = function () {
        $("#bookingSuccessModal").modal("hide");
    };

    function getRouteDetails(marketplaceId,routeId,stoppageId,releaseDate){
        console.log(marketplaceId,routeId,stoppageId,releaseDate);
        if(stoppageId==0){
            //openModel('<h3>No Stoppage Information Found!</h3>');
            bsModalOpen('<h3>Stoppage information not found!</h3>');
            return;
        }
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("marketplace/getRouteDetails");?>',
            data:{
                marketplaceId: marketplaceId,
                routeId:routeId,
                stoppageId:stoppageId,
                releaseDate:releaseDate
            },
            success: function(html){
                $('#submit-information').html(html);
            }
        });

    }
    function submitBookingRequest(marketplaceId, routeId, stoppageId, releaseDate){
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl("marketplace/submitBookingRequest");?>',
            data:{
                marketplaceId: marketplaceId,
                routeId:routeId,
                stoppageId:stoppageId,
                releaseDate:releaseDate
            },
            dataType: 'json',
            success: function(data){
                console.log(data);
                $('#bookingMessage').html('<h3>' + data.message + '</h3>');
                if(data.status){
                    var html =
                        '<a class="btn btn-success" href="<?php echo Yii::app()->createUrl('marketplace/bookingDetails')?>/' + data.id + '">View Details</a>' +
                        '<?php echo CHtml::link("Search Again",array("marketplace/bookUserSeat"),array("class"=>"modelRightButton btn btn-success"));?>';
                } else {
                    var html =
                        '<?php echo CHtml::link("Search Again",array("marketplace/bookUserSeat"),array("class"=>"modelRightButton btn btn-success"));?>';
                }
                $('#bookingButton').html(html);
                $("#bookingSuccessModal").modal({
                    backdrop: "static",
                    show: true
                });
            }
        });
    }
</script>

