<style>
    .w100{
        width: 100%;
    }
    .choices__item--selectable{
        padding: 0 20px 0 45px;
    }
    .bg-danger{
        background-color: #ff6771 !important;
    }
    .choices__list--dropdown{
        z-index: 1000;
    }
    .custom-checkbox-alignment{
        border: 0 !important;
        height: 24px !important;
        margin: auto 0 !important;
    }
</style>
<!--<pre>< ?php var_dump($staffInfo); ?></pre>-->
<div class="s002 topmargin">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'marketplace-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array(
            'onsubmit'=>'return validateForm();'
        )
    )); ?>

    <?php if (!empty($routeInfo)): ?>
        <div class="col_full">
            <div class="heading-block fancy-title nobottomborder title-bottom-border">
                <h4>Route Information</h4>
            </div>
        </div>
        <div class="clear"></div>
        <?php
        if (count($errorSold)){
            foreach ($errorSold as $e){
                echo
                    '<div class="col_full">
                        <div class="style-msg errormsg">
                            <div class="sb-msg">
                                <i class="icon-remove"></i><strong>Oh snap!</strong> You have already released your <strong>'.$e->direction.'</strong> seat for <strong>'.$e->release_date.'</strong>.
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>';
            }
        } ?>


        <div class="col_half ">
            <?php echo $form->labelEx($model,'zone_id'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->zone_id = $routeInfo['zone_id'];
                    $zone_list = Zone::model()->findAll(array('order' => 'name ASC'));
                    echo $form->hiddenField($model,'zone_id',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    echo $form->dropDownList($model,'zone_id', CHtml::listData($zone_list, 'id', 'name'),array('empty' => 'Select Zone', 'readOnly' => true, 'data-trigger'=>"", 'disabled'=>"disabled"));
                    ?>
                </div>
            </div>
        </div>

        <div class="col_half col_last">
            <?php echo $form->labelEx($model,'route_id'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->route_id = $routeInfo['id'];
                    echo $form->hiddenField($model,'route_id',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    ?>
                    <input type="text" value="<?php echo $routeInfo['route_detail']; ?>" readonly="readonly">
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="col_half">
            <?php echo $form->labelEx($model,'price'); ?>
            <div class="inner-form">
                <div class="input-field w100">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php
                    $model->price = $routeInfo['price'];
                    echo $form->textField($model,'price',array('placeholder'=>'','readOnly' => true, 'tabindex'=>-1));
                    ?>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <?php
        $next_slots = $this->getSellSlotAndDirection();
        ?>
        <?php foreach ($next_slots as $ks=>$slot_data):?>
            <div class="col_one_fifth">
                <label for="">CHECK</label>
                <div class="inner-form">
                    <div class="input-field w100" style="height: 42px;display: grid;">
                        <?php
                        $slot_str = implode(",",get_object_vars ( $slot_data ));
                        echo CHtml::checkBox('Marketplace[next_slots]['.$ks.']',false,array('class'=>'custom-checkbox-alignment', 'id'=>'Marketplace_next_slots_'.$ks, 'value'=>$slot_str));
                        ?>
                    </div>
                </div>
            </div>
            <div class="col_two_fifth">
                <?php echo $form->labelEx($model,'release_date'); ?>
                <div class="inner-form">
                    <div class="input-field w100">
                        <div class="icon-wrap">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                                </path>
                            </svg>
                        </div>
                        <?php
                        $model->release_date = $slot_data->date;
                        echo CHtml::textField('marketplace_released_'.$ks,$slot_data->date,array('class'=>'', 'readonly'=>true));
                        ?>

                    </div>
                </div>
            </div>
            <div class="col_two_fifth col_last">
                <?php echo $form->labelEx($model,'travel_direction'); ?>
                <div class="inner-form">
                    <div class="input-field w100">
                        <div class="icon-wrap">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                                </path>
                            </svg>
                        </div>
                        <?php
                        echo CHtml::textField('marketplace_direction'.$ks,$slot_data->travel_direction,array('class'=>'', 'readonly'=>true));
                        ?>

                    </div>
                </div>
            </div>
            <div class="clear"></div>



        <?php endforeach; ?>

        <div class="clear"></div>
        <div class="col_full">
            <div class="input-field first-wrap">
                <?php
                echo CHtml::submitButton("Submit",array('class'=>'btn-search','id'=>'search-button'));
                ?>
            </div>
        </div>
<!--        < ?php echo $slot_data->currtime ?>-->


        <!--< ?php if($slot_data->slot_open): ?>
            <div>
                < ?php
                $model->username = $userPin;
                echo $form->hiddenField($model,'username',array('placeholder'=>'', 'tabindex'=>-1));
                $model->price = $routeInfo['price'];
                echo $form->hiddenField($model,'price',array('placeholder'=>'', 'tabindex'=>-1));
                echo $form->hiddenField($model,'travel_direction',array('placeholder'=>'', 'tabindex'=>-1));
                echo $form->hiddenField($model,'release_date',array('placeholder'=>'', 'tabindex'=>-1));?>
            </div>
        < ?php endif; ?>

        <div class="col_full">
            <div class="input-field first-wrap">
                < ?php
                if($slot_data->slot_open){
                    echo CHtml::submitButton("Submit",array('class'=>'btn-search','id'=>'search-button'));
                } else {
                    echo "<span class='btn btn-danger w100'>".$slot_data->travel_direction."</span>";
                } ?>
            </div>
        </div>-->

    <?php else: ?>
        <div class="col_full">
            <div class="style-msg errormsg">
                <div class="sb-msg">
                    <i class="icon-remove"></i><strong>Oh snap!</strong> You have no allocated seat. Please request from here.
                    <a href="<?php echo Yii::app()->createAbsoluteUrl('seatRequest/index') ?>" class="btn btn-success btn-xs" style="float: right;">Request Seat</a></div>
            </div>
        </div>
    <?php endif; ?>
    <?php $this->endWidget(); ?>
</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: false,
            itemSelectText: '',
        });

</script>
<script>
    var validateForm = function () {
        var start_point = $('#start_point').val();
        var end_point = $('#end_point').val();
        var isValid = true;
        if (start_point.trim() == '' && end_point.trim() == ''){
            isValid = false;
        }
        if (!isValid){
            var elem = document.getElementById('bookingModalHeader');
            elem.parentNode.removeChild(elem);
            $('#bookingMessage').html('<h4 style="margin: 30px auto">Please select <strong style="color: #089D49">Start point</strong> or <strong style="color: #089D49">End point</strong></h4>');
            $('#bookingCloseButton').html('<button type="button" class="btn btn-success" data-dismiss="modal">Close</button>');
            $("#bookingSuccessModal").modal({
                backdrop: "static",
                show: true
            });
        }
        return isValid;
    };
</script>