
<!--<pre>< ?php print_r($userSeat->attributes) ?></pre>-->
<section id="page-title" class="page-title-center" >
    <div class="clearfix">
        <h1>Marketplace</h1>
        <span>Available Seat: <?php echo $seatCount; ?></span>

    </div>
</section>

<?php if (isset($userSeat)): ?>
<section id="page-title" class="page-title-center page-title-nobg noborder" >
    <div class="col_half ">
        <ul class="list-group" style="text-align: left">
            <li class="list-group-item"><strong>MY ROUTE:</strong> <span class="" style="margin-top: 0;"><?php echo $userSeat['present_route']; ?></span></li>
            <li class="list-group-item"><strong>ROUTE DETAIL:</strong> <span class="" style="margin-top: 0;"><?php echo $userSeat['route_detail']; ?></span></li>
        </ul>
    </div>
    <div class="col_half col_last">
        <a href="<?php echo Yii::app()->baseUrl;?>/index.php/marketplace/bookUserSeat">
            <div class="promo promo-dark promo-mini promo-center bgcolor bottommargin" style="width: 200px;margin: 0 auto;padding: 6px 10px;">
                <h3>Book Seat</h3>
            </div>
        </a>
        <a href="<?php echo Yii::app()->baseUrl;?>/index.php/marketplace/sellUserSeat">
            <div class="promo promo-dark promo-mini promo-center" style="width: 200px;margin: 0 auto;padding: 6px 10px;">
                <h3>Sell Seat</h3>
            </div>
        </a>
    </div>
</section>
<?php else: ?>
    <section id="page-title" class="page-title-center page-title-nobg noborder" >
        <div class="clearfix">
            <span>Currently you have no allocated seat. </span>
            <ol class="breadcrumb">
                <li><?php echo CHtml::link("Request A Seat",array("seatRequest/index"),array("class"=>"btn btn-success"));?></li>
            </ol>
        </div>
    </section>
<?php endif; ?>
