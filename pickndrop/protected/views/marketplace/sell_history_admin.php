<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
<!--<style>
    .fs-12{
        font-size: 12px;
    }
</style>-->
<div id="route-request-list">
    <div class="col_full page_header_div">

        <h4 class="heading-custom page_header_h4">Sell History</h4>

    </div>
    <!--    <pre>< ?php print_r($model) ?></pre>-->

    <!--    <div class="table-responsive bottommargin">-->

    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'sell-history-admin',
        'dataProvider' => $model->sell_history_admin(),
        'itemsCssClass' => 'table cart',
        'htmlOptions' => array('class' => 'table-responsive bottommargin table_sale_admin'),

        'rowCssClass' => array('cart_item'),
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'id',
                'type' => 'raw',
                'value' => '$data->id',
//                    'value' => 'CHtml::link(UHtml::markSearch($data,"username"),array("admin/view","id"=>$data->id))',
            ),
            array(
                'name' => 'username',
                'type' => 'raw',
//                    'value' => '$data->username',
                'value' => 'CHtml::link(CHtml::encode($data->username))',
//                    'value' => 'CHtml::link(CHtml::encode($data->username),array("marketplace/bookingDetails","id"=>$data->id))',
            ),
            array(
                'header' => 'Photo',
                'type' => 'raw',
                'value' => 'CHtml::image(SeatRequest::model()->photo($data->username),"USER IMAGE",array("width"=>"29px"))',
                'htmlOptions' => array('style' => 'text-align: center'),
                'headerHtmlOptions' => array('style' => 'text-align: center'),

            ),
            array(
                'header' => 'Selling Time',
                'name' => 'created_time',
                'type' => 'raw',
                'value' => function ($data) {
                    $created_time = date_create($data->created_time);
                    echo date_format($created_time, "d/m/Y h:i A");
                },
//                    'value' => '($data->id),array("admin/update","id"=>$data->id))',
//                    'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
            ),
            array(
                'name' => 'route_id',
                'type' => 'raw',
                'value' => '$data->route_id',
            ),
            array(
                'name' => 'zone_id',
                'type' => 'raw',
                'value' => '$data->zone_id',
            ),
            array(
                'name' => 'release_date',
                'type' => 'raw',
                'value' => '$data->release_date',
            ),
            array(
                'name' => 'travel_direction',
                'type' => 'raw',
                'value' => '$data->travel_direction',
            ),
            array(
                'name' => 'price',
                'type' => 'raw',
                'value' => '$data->price',
            ),
            array(
                'header' => 'Status',
                'name' => 'status',
                'type' => 'raw',
                'value' => function ($data) {
                    if ($data->status)
                        return '<span class="button button-3d button-rounded button-green button-mini button-sold">Sold</span>';
                    else
                        return '<span class="button button-3d button-rounded button-red button-mini">Unsold</span>';
                },
                'filter' => false
            ),
            /*array(
                'class'=>'CButtonColumn',
                'template'=>'{view}{update}',

            ),*/
        ),
    )); ?>

</div>
<div class="modal fade" id="marketplaceUpdateModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Request</h4>
            </div>
            <div class="modal-body">
                <div style='text-align: center;'>
                    <input type='radio' name='request' value='-1'> Cancel my seat release<br><input type='submit'
                                                                                                    onClick='submitRequestUpdated("+requestId+")'
                                                                                                    value='Submit'
                                                                                                    class='btn btn-success'
                                                                                                    style='margin-top:20px'/>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>


<script>
    var showModal = function (marketplaceId) {
        console.log(marketplaceId);
        $('#marketplaceUpdateModal').modal({
            show: true
        });
    };

    function routeInformationUpdate(requestId) {

        var html = "<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Choose Your Request</h5><div id='notice'></div><input type='radio' name='request' value='0'>Request For Approved<br><input type='radio' name='request' value='3'>Request For Cancel<br><input type='submit'  onClick='submitRequestUpdated(" + requestId + ")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);

    }

    function submitRequestUpdated(requestId) {
        var v = $("input[name='request']:checked").val();
        if (v == null) {
            $("#notice").html("<h5 style='color:red;margin-bottom:5px;'>Please, Select A Request</h5>");
        }
        else {

            $.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>', {
                requestId: requestId,
                updateRequest: v
            }, function (data) {
                bsModalOpen(data);
                //alert(data);
            });

        }
    }

</script>