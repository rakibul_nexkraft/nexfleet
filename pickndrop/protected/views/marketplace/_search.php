<?php
/* @var $this MarketplaceController */
/* @var $model Marketplace */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'id'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'id'); ?>
        </div>
    </div>

	<div class="fl">
        <div class="row">
            <?php echo $form->label($model,'route_id'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'route_id'); ?>
        </div>
    </div>

	<div class="fl">
        <div class="row">
            <?php echo $form->label($model,'zone_id'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'zone_id'); ?>
        </div>
    </div>

	<div class="fl">
        <div class="row">
            <?php echo $form->label($model,'price'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
        </div>
    </div>

	<div class="fl">
        <div class="row">
            <?php echo $form->label($model,'username'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128)); ?>
        </div>
    </div>

	<div class="fl">
        <div class="row">
            <?php echo $form->label($model,'status'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'status'); ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Search',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>


<?php $this->endWidget(); ?>

</div><!-- search-form -->