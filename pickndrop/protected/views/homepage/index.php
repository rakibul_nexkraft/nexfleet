<?php
/* @var $this HomepageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Homepages',
);

$this->menu=array(
	array('label'=>'Create Homepage', 'url'=>array('create')),
	array('label'=>'Manage Homepage', 'url'=>array('admin')),
);
?>

<div class="heading-block fancy-title nobottomborder topmargin theme-bottom-border">
    <div class="fleft">
        <h4>Homepage Comments</h4>
    </div>
    <div class="fright">
        <a class="button button-3d button-mini button-rounded button-green" href="<?php echo Yii::app()->createUrl("homepage/create"); ?>">Create Comment</a>
    </div>
    <div class="clear"></div>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'homepage-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'itemsCssClass' => 'table cart',
    'htmlOptions' => array('class' => 'table-responsive bottommargin'),
    'rowCssClass'=>array('cart_item'),
    'columns'=>array(
        'id',
        'name',
        'text',
        'title',
//        'image_url',
        'type',
        array(
            'class'=>'CButtonColumn',
        ),
    ),
)); ?>
