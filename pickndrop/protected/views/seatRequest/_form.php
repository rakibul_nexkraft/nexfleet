<?php
/* @var $this SeatRequestseatRequestController */
/* @var $model SeatRequest */
/* @var $form CActiveForm */


?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'seat-request-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<!--<div class="row">
		<?php echo $form->labelEx($model,'user_pin'); ?>
		<?php echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'user_pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'user_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_department'); ?>
		<?php echo $form->textField($model,'user_department',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'user_department'); ?>
	</div>-->
	<div class="row">
		<?php echo $form->labelEx($model,'zone'); ?>
		<?php $zone = Zone::model()->findAll(array('select'=>'id,name','order' => 'name ASC'));
        echo $form->dropDownList($model,'zone', CHtml::listData($zone,'id',  'name'),array('empty' => 'Select Zone...','style'=>'height:30px; width:190px;','class'=>'input-medium','onchange'=>'zoneRoute(this.value)'));?>
		<?php //echo $form->textField($model,'zone'); ?>
		<?php echo $form->error($model,'zone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'route_id'); ?>
		<?php //echo $form->textField($model,'route_id',array('readonly'=>'readonly')); 
		if($model->isNewRecord){
			$list='';
			echo $form->dropDownList($model,'route_id',$list,array('onchange'=>'routeStoppage(this.value)','class'=>'input-medium')); 
		}
		else{
			
			$k[]=Routes::model()->findByPk($model->route_id);
			$list=CHtml::listData($k,'id','route_detail');
			echo $form->dropDownList($model,'route_id',$list,array('onchange'=>'routeStoppage(this.value)','class'=>'input-medium'));
		}
		?>
		<?php echo $form->error($model,'route_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'stoppage_id'); ?>
		<?php //echo $form->textField($model,'route_id',array('readonly'=>'readonly')); 
		if($model->isNewRecord){
			$list='';
			echo $form->dropDownList($model,'stoppage_id',$list); 
		}
		else{
			
			$k1[]=Stoppage::model()->findByPk($model->stoppage_id);

			$list=CHtml::listData($k1,'id','stoppage');
			echo $form->dropDownList($model,'stoppage_id',$list);
		}
		?>
		<?php echo $form->error($model,'stoppage_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expected_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				'model'=>$model,'attribute'=>'expected_date',

				// additional javascript options for the date picker plugin
				'options'=>array('autoSize'=>true,
						  'mode'=>'datetime',
						  'dateFormat' => 'yy-mm-dd', // save to db format
						  'changeMonth'=>'true',
						  'changeYear'=>'true',
						  'yearRange'=>'2000:2020',
				),

			'htmlOptions'=>array('style'=>'width:150px;','autocomplete'=>'off'
			),
			)); ?>
		<?php //echo $form->textField($model,'expected_date'); ?>
		<?php echo $form->error($model,'expected_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>
<?php if(!$model->isNewRecord){?>
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array('0'=>'Request For Approved','3'=>'Request For Cancel')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
<?php } ?>
	<!--<div class="row">
		<?php echo $form->labelEx($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_time'); ?>
		<?php echo $form->textField($model,'created_time'); ?>
		<?php echo $form->error($model,'created_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'updated_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_time'); ?>
		<?php echo $form->textField($model,'updated_time'); ?>
		<?php echo $form->error($model,'updated_time'); ?>
	</div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); 
	//echo $this->createUrl('vehicles/allRegNo1');

?>

</div><!-- form -->
<script>
	function zoneRoute(z){
		$.post("<?php echo $this->createUrl('seatRequestseatRequest/zone');?>", {id: z}, function(result){
			var data=JSON.parse(result);    

			if (!data.hasOwnProperty('error')) {
				var listItem="<option value=''>Select Route</option>";
				for (i = 0; i < data.length; i++) {
                    //listItem+="<option value='"+data[i].id+"'>"+data[i].route_no+"-"+data[i].route_detail+"</option>";
                    listItem+="<option value='"+data[i].id+"'>"+data[i].route_detail+"</option>";
                    }
                    $('#SeatRequest_route_id').html(listItem);
				

			}
			else{
			var listItem="<option value=''></option>";
			$('#SeatRequest_route_id').html(listItem);
			}
  });
	}
function routeStoppage(r){
	
		$.post("<?php echo $this->createUrl('seatRequestseatRequest/route');?>", {id:r}, function(result){
			
			var data=JSON.parse(result);    

			if (!data.hasOwnProperty('error')) {
				var listItem="<option value=''>Select Stoppage</option>";
				for (i = 0; i < data.length; i++) {
                    //listItem+="<option value='"+data[i].id+"'>"+data[i].route_no+"-"+data[i].route_detail+"</option>";
                    listItem+="<option value='"+data[i].id+"'>"+data[i].stoppage+"</option>";
                    }
                    $('#SeatRequest_stoppage_id').html(listItem);
				

			}
			else{
			var listItem="<option value=''></option>";
			$('#SeatRequest_stoppage_id').html(listItem);
			}
  });
	}
</script>