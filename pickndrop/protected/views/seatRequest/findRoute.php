<style>
<?php

if(!isset($routes_rs)) {
?>

#findRoute{
	display: none;
}

<?php
}
?>
.toggle.toggle-bg .togglet i {
    left: 97%;
}

</style>

<div class="toggle toggle-bg" style="background: white;">

	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Pick And Drop

	</div>
	<div class="togglec" style="display: none;/*box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);*/border-radius: 5px;">
		<?php

		$pending_route = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_request WHERE user_pin='".Yii::app()->user->username."' AND (user_status<>2 OR status=4)")->queryRow();
		if($use_route) {
			$this->renderPartial('current_route_list',array(
				'model'=>$model,
				'use_route'=>$use_route			
			));
		}		
		else if($pending_route) {
		?>
		<div class="center s002 page_header_div">        
   			<h4 class="heading-custom page_header_h4 notopmargin" style="font-size: 16px !important">Please Cancel Your Previous Pending OR  Changing Route Request Before Apply a New Route Request.</h4>
		</div>

		<?php }
	
		else {
			
			$this->renderPartial('_routeSearch',array(
				'model'=>$model,
				'f'=>1
			));
		}
		?>
		<div id="route-list">

		</div>
		<div id="submit-info" style='margin-top: 20px;'>

		</div>
	</div>
</div>
<div class="toggle toggle-bg" style="background: white;">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Change Route</div>
		<div class="togglec" style="display: none; /*box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);*/border-radius: 5px;">
		<?php
		$pending_route = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_request WHERE user_pin='".Yii::app()->user->username."' AND (status=0 OR status=4)")->queryRow();		
		if($pending_route) {
		?>
		<div class="center s002 page_header_div notopmargin">
   			<h4 class="heading-custom page_header_h4 font" style="font-size: 16px !important">Please cancel your previous pending or changing route request before applying for a new changing request.</h4>
		</div>

		<?php }
		else if($use_route) {
			$this->renderPartial('current_route_list',array(
					'model'=>$model,
					'use_route'=>$use_route					
			));	
			$this->renderPartial('_routeSearch',array(
				'model'=>$model,
				'f'=>2
			));	
			
		}
		else {
		?>
		<div class="center s002 page_header_div notopmargin">
   			<h4 class="heading-custom page_header_h4" style="font-size: 16px !important">Sorry, you do not have approved route.</h4>
		</div>
		<?php }	?>
		<div id="route-list-1">
		<?php
		/*$this->renderPartial('findRouteList',array(
			'model'=>$model,
			'routes_rs' => $routes_rs,
			'f'=>2
		));*/
		?>
		</div>
		<div id="submit-info-1" style='margin-top: 20px;'>	   

		</div> 
   

	</div>
</div>
<div class="toggle toggle-bg" style="background: white;">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Seat Cancel</div>
	<div class="togglec" style="display: none; /*box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.20);*/border-radius: 5px;">
		<?php
		if($use_route){
			$this->renderPartial('cancel_seat',array(
					'model'=>$model,
					'use_route'=>$use_route,			
			));				
		}
		else {
		?>
		<div class="center s002 page_header_div notopmargin">
   			<h4 class="heading-custom page_header_h4" style="font-size: 16px !important">Sorry, you do not have approved route.</h4>
		</div>
		<?php }	?>
	</div>
</div>


<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seat Requests',
);
?>
<div id="route-request-list" class="seat-request-admin search-box-responsive">
	<div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">
        <h4 class="heading-custom page_header_h4">Request List</h4>
    </div>
	<?php  $this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin border-round-bottom'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model->search(),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(
			//'id',
			
			array(	
				 'header'=>'User',				
				'value'=>'$data->user_pin',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'user_name',
			//'user_department',
			array('header'=>'Route No',				
				'value'=>'$data->routeDetail($data->route_id,1)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Route Detail',	
			
				'type'=>'raw',
				'value'=>'$data->routeDetail($data->route_id,2,$data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//array('header'=>'Stoppage',
				
				//'type'=>'raw',
				//'value'=>'$data->stoppageDetail($data->stoppage_id)',
				//'htmlOptions'=>array('class'=>'cart-product-price'),
				//'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			//),
			array('header'=>'Pickup Time',
				'type'=>'raw',
				'value'=>'$data->stoppagePickupTime($data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'route_id',
			
			array('header'=>'Expected Date',				
				'value'=>'$data->expected_date',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			
		
			//'remarks',
			array(
				'header'=>'User Request Type',
				'type'=>'raw',
				'value'=>'$data->userStatus($data->user_status)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(
				'header'=>'Fleet Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status,$data->queue)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			/*array('header'=>'Queue',
	            'type'=>'raw',		
	            'value'=>'$data->queueCheck($data->queue,$data->status)',	
			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			array('header'=>'Cancel',
	            'type'=>'raw',
				'value'=>'$data->user_status == 2 ? false : CHtml::submitButton("Cancel",array("class"=>"btn-search button-pink","id"=>"search-button","onClick"=>"submitRequestCancel($data->id)","name"=>""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Detail',
	            'type'=>'raw',			
				'value'=>' CHtml::submitButton("Detail",array("class"=>"btn-search button-pink","id"=>"search-button","onClick"=>"requestDetail($data->id)"))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			/*array('header'=>'Apply Maternity Leave Use',
	            'type'=>'raw',			
				'value'=>'($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			/*array('header'=>'Update',
	            'type'=>'raw',			
				'value'=>'CHtml::submitButton("Update",array("class"=>"btn-search1","id"=>"search-button1","onClick"=>"routeInformationUpdate($data->id)","name"=>""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			//'status',
			/*'created_by',
			'created_time',
			'updated_by',
			'updated_time',
			*/
			/*array(
				'class'=>'CButtonColumn',
				'template'=>'{view}{update}{delete}',
				'deleteConfirmation'=>"js:'Do you really want to cancel request record with ID '+$(this).parent().parent().children(':nth-child(1)').text()+'?'",
				 'buttons'=>array(
	        		'delete' => array(
	            	'label'=>'Cancel Request', 

	        		),
	        	),
			),*/

		),
	)
	//array(
        //'htmlOptions' => array('class' => 'table cart')
    //)
	); 
	function userStatus($data){
		return ($data == 2 ? false : true);
	}
	
	?>
	
</div>
<script>
	function routeInformationUpdate(requestId){
		
		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Choose Your Request</h5><div id='notice'></div><input type='radio' name='request' value='0'>Request For Approved<br><input type='radio' name='request' value='3'>Request For Cancel<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);

	}
	function submitRequestUpdated(requestId){
		 var v=$("input[name='request']:checked").val();		
			if(v==null){
		    	$("#notice").html("<h5 style='color:red;margin-bottom:5px;'>Please, Select A Request</h5>");
			}
			else{

				$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>',{requestId:requestId,updateRequest:v},function(data){
                    bsModalOpen(data);
							//alert(data);
					});
				
			}
	}
	function submitRequestCancel(requestId){
        showDataProcess(true);
        $.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>',{requestId:requestId,updateRequest:2},function(data){
            $('#myModal').css('display','none');
            showDataProcess(false);
            bsModalOpen(data);
        });
				
			
	}
	function maternityUse(mid,fromDate,toDate){	

		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Do You Want to Use From Date: "+fromDate+" To Date: "+toDate+"? </h5><h5 style='margin-bottom:5px;'>Your Route Request Will be Canceled. </h5><div id='notice'></div><input type='submit'  onClick='maternityUpdated("+mid+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);
	
		}
	function maternityUpdated(maternityId){
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/maternityUpdated");?>',{maternityId:maternityId},function(data){
            bsModalOpen(data);
							//alert(data);
					});
				
			
	}	
		
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
   	const choices = new Choices('[data-trigger]',
    {
        searchEnabled: true,
        itemSelectText: '',
    });

</script>
<script type="text/javascript">
	function routeInformation(routeId,stoppageId,expectedDate,type,changeRoute=null){
		if(stoppageId==0){
            bsModalOpen('<h3>Please select stoppage!</h3>');
		}
		else if(expectedDate==''){
            bsModalOpen('<h3>Please select expected date!</h3>');
		}
		else{
			$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformation");?>',{route_id:routeId,stoppageId:stoppageId,expectedDate:expectedDate,changeRoute:changeRoute},function(data){
							if(type==1) {
								$('#submit-information1').html(data);
							}
							else {
								$('#submit-information').html(data);
							}
							flatpickr(".datepicker",{});
								const choices = new Choices('[data-trigger]',
							    {
							        searchEnabled: false,
							        itemSelectText: '',
							    });
					});
		}
	}
	function routeSubmit(routeId,stoppageId,expectedDate,changeRoute=null){

		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/userRouetRquest");?>',{route_id:routeId,stoppageId:stoppageId,expectedDate:expectedDate,changeRoute:changeRoute},function(data){
						if(data==1){
							data="<h3>Your Request Send To Addmin Successfully.</h3>";
							data="<h3>Please Wait For Admin Approval.</h3>";
							
							data=data+'<?php echo CHtml::link("View Seat Request List",array("seatRequest/requestList"),array("class"=>"btn btn-success"));?>';
                            bsModalOpen(data);
						}
						else{
                            bsModalOpen('<h3>'+data+'</h3>');
						}
						
				});
	}
	function updateSeatRequest(){
		var id=$('#SeatRequest-id').val();
		var expDate=$('#depart1').val();
		var seatRequestStoppageId=$('#SeatRequest-stoppage-id').val();
		var seatRequestStatus=$('#SeatRequest-status').val();
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/userRouteRquestUpdate");?>',{id:id,expDate:expDate,seatRequestStoppageId:seatRequestStoppageId,seatRequestStatus:seatRequestStatus},function(data){
						/*if(data==1){
							data="<h3>Your Request Send To Addmin Successfully.</h3>";
						//	data="<h3>Please Wait For Approved.</h3>";
							
							data=data+'<?php echo CHtml::link("View Seat Request List",array("seatRequest/requestList"),array("class"=>"btn btn-success"));?>';
							data=data+'<?php echo CHtml::link("Seat Request Page",array("seatRequest/index"),array("class"=>"modelRightButton btn btn-success"));?>';
                            bsModalOpen(data);
						}
						else{*/
                            bsModalOpen(data);
						//}
						
				});
		
		return false;
	}
	function findRoute(v){
		
		$('#findRoute').show();
	}
	function  searchRoute(v){
		
		$("#movements-form").ajaxSubmit(
		{
		url: '<?php echo Yii::app()->createAbsoluteUrl("/seatRequest/findRouteList");?>', 
		type: 'post', 
		success:  function(data) { 
        	$('#submit-route-list').html(data);
    		}

		}
	);
		return false;

	}
	function searchRouteChange(v){
		
		$("#change-route").ajaxSubmit(
		{
		url: '<?php echo Yii::app()->createAbsoluteUrl("/seatRequest/findRouteList", array("id" => 1));?>', 
		type: 'post', 
		success:  function(data) { 
        	$('#submit-route-list1').html(data);
    		}

		}
	);
		return false;

	}
	function pointSearch(p,f){
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/pointSearch");?>',{p:p,f:f},function(data){
			if(f==1)
			$('#route-list').html(data);
			if(f==2)
			$('#route-list-1').html(data);
		});

	}
	function pickndropForm(rid,f){
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeSubmit");?>',{rid:rid,f:f},function(data){
			if(f==1)
			$('#submit-info').html(data);
			if(f==2)
			$('#submit-info-1').html(data);
			flatpickr(".datepicker",{});
			const choices = new Choices('[data-trigger]',
							    {
							        searchEnabled: false,
							        itemSelectText: '',
							    });
		});
	}
	function userRouteSubmit(){

		var routeId = $('#route_submit').val();
		var stoppageId = $('#stoppage_submit').val();
		var expectedDate = $('#depart').val();
		var f = $('#flag').val();
		if(f==1){
			var changeRoute=null;
		}
		else{
			var changeRoute=1;
		}

		if(!stoppageId && !expectedDate){
			//openModel('<h3>Please, select a stoppage and Expected Date</h3>');
            bsModalOpen('Please, select a stoppage and Expected Date');
		}
		else if(!stoppageId){
			//openModel('<h3>Please, select a stoppage</h3>');
            bsModalOpen('Please, select a stoppage');
		}
		else if(!expectedDate){
			//openModel('<h3>Please, select Expected Date</h3>');
            bsModalOpen('Please, select Expected Date');
		}
		else{
			showDataProcess(true);
			$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/userRouetRquest");?>',{route_id:routeId,stoppageId:stoppageId,expectedDate:expectedDate,changeRoute:changeRoute},function(data){            
                    showDataProcess(false);
                    bsModalOpen(data);						
            });
		}
	}
	function requestDetail(id) {
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/requestDetail");?>',{id:id},function(data) {
               
						bsModalOpen(data, true);
            });
	}
</script>