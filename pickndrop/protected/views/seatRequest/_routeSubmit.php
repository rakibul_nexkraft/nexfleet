<div class="center s002 searchbar" style="padding-top:60px">
         <?php $form=$this->beginWidget('CActiveForm', array(
		   'id'=>'movements-form',
			'enableAjaxValidation'=>false,
			'htmlOptions' => array('onsubmit' =>'return false'),	
		)); ?>
		<input type="hidden" name="route" value="<?php echo $rid; ?>" id='route_submit'>
		<input type="hidden" name="f" value="<?php echo $f; ?>" id='flag'>
        <div class="inner-form seat-request-form">
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Route No.</div>
                <div class="input-field fouth-wrap col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
							<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">						
							</path>
				        </svg>
                    </div>
                   <?php 
		            $route=Routes::model()->findByPk($rid);
		            $model->route_id=$route['route_no'];
		            echo $form->textField($model,'route_id',array('readonly'=>'readonly'));		        ?>


                </div>
            </div>
             <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Stoppage</div>
                <div class="input-field fouth-wrap col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php $Stoppage = Stoppage::model()->findAllByAttributes(array('route_id'=>$rid)); 
                    //var_dump($Stoppage);exit();
                    ?>
                    <select data-trigger="" name="Stoppage[id]" id="stoppage_submit" >
                        <option value=''>Select A Stoppage</option>
                        <?php foreach ($Stoppage  as $key => $value) {

                            echo "<option value='" . $value['id'] . "'>" . $value['stoppage'] . "</option>";


                        }
                        ?>
                    </select>


                </div>
            </div>

            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Expected Date</div>
                <div class="input-field first-wrap col_two_third">
                    <div class="icon-wrap">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
				    </path>
                </svg>
                 </div>
                    <?php echo $form->textField($model,'expected_date',array('placeholder'=>'Expected Date','id'=>'depart','class'=>'datepicker')); ?>
                </div>
            </div>
            <div class='col_one_third search_button' style="margin: -44px 0 0 0;">
                <div class="seat-request-label col_one_third"></div>
                <div class='input-field first-wrap col_two_third'>
                    <?php echo CHtml::submitButton("Submit", array('class' => 'btn-search button-pink', 'id' => 'search-button','onclick'=>'userRouteSubmit()')); ?>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
