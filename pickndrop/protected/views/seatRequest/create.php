<?php
/* @var $this SeatRequestseatRequestController */
/* @var $model SeatRequest */

$this->breadcrumbs=array(
	'Seat Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Seat Request', 'url'=>array('index')),
	//array('label'=>'Manage Seat Request', 'url'=>array('admin')),
);
?>

<h1>Create Seat Request</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>