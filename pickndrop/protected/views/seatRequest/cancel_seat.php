
	<?php  $this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model->search_approved($use_route['rid']),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(
			//'id',
			
			array(	
				 'header'=>'User',				
				'value'=>'$data->user_pin',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'user_name',
			//'user_department',
			array('header'=>'Route No',				
				'value'=>'$data->routeDetail($data->route_id,1)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Route Detail',	
			
				'type'=>'raw',
				'value'=>'$data->routeDetail($data->route_id,2,$data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//array('header'=>'Stoppage',
				
				//'type'=>'raw',
				//'value'=>'$data->stoppageDetail($data->stoppage_id)',
				//'htmlOptions'=>array('class'=>'cart-product-price'),
				//'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			//),
			/*array('header'=>'Pickup Time',
				'type'=>'raw',
				'value'=>'$data->stoppagePickupTime($data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'route_id',
			
			array('header'=>'Expected Date',				
				'value'=>'$data->expected_date',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			
		
			//'remarks',*/
			array('header'=>'Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			/*array('header'=>'Queue',
	            'type'=>'raw',		
	            'value'=>'$data->queueCheck($data->queue,$data->status)',	
			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Apply Maternity Leave Use',
	            'type'=>'raw',			
				'value'=>'($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			array('header'=>'Cancel',
	            'type'=>'raw',			
				'value'=>'CHtml::submitButton("Cancel",array("class"=>"btn-search button-pink","id"=>"search-button","onClick"=>"submitRequestCancel($data->id)","name"=>""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			
		),
	)
	
	); 
	?>