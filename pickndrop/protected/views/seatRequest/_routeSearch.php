<style type="text/css">
	.choices__list--single .choices__item {
    width: 100%;
    padding-left: 20px;
}
</style>
<div class="s002 searchbar" >     									
   <?php $form = $this->beginWidget('CActiveForm', array(
				'id' => 'movements-form',
				'enableAjaxValidation' => false,	
				'htmlOptions' => array('onsubmit' => 'return searchRoute(this)'),		
	)); ?>       
    <div class="inner-form" style="margin: 21px 0 -18px 0px;
    padding: 12px;">
        <div class="input-field first-wrap" style="width:100%">
            <div class="icon-wrap">
			    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">						
					</path>
		        </svg>
            </div>   
            <!-- <input type="text" name="search" placeholder='Search Your Pick Point' onkeyup="pointSearch(this.value,<?php //echo $flag;?>)"> --> 	
            <!-- <input type="text" name="search" placeholder='Search Your Pick Point' onclick="pointSearch(this.value,<?php //echo $flag;?>)" id='search' list='browsers'> --> 	
          <!--   <?php //$this->widget('ext.ESelect2.ESelect2',array(
            //'model'=>$model,
            //'attribute'=>'id',
            //'data'=>array('1'=>2,'2'=>3),
            //'options'  => array(
                //'style'=>'width:100%',
                //'allowClear'=>true,
                //'placeholder'=>'Select Vehicle Reg No',
               // 'minimumInputLength' => 1,
            //),
        //));?> -->
         <?php 
         //$routes=Routes::model()->findAll();  
         $routes = Yii::app()->db->createCommand("SELECT r.*, vt.type, vt.facility FROM tbl_routes as r  INNER JOIN tbl_vehicles as v ON r.vehicle_reg_no=v.reg_no INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id INNER JOIN tbl_stoppage as s ON s.route_id=r.id GROUP BY r.id")->queryAll();
         $lavel = Yii::app()->user->level;
         $factor = Yii::app()->db->createCommand("SELECT * FROM tbl_route_factor WHERE level_from<='".$lavel."' AND level_to>='".$lavel."'")->queryRow();            
         ?>
            <select data-trigger="" name="Stoppage[id]" id="" onchange="pickndropForm(this.value,<?php echo $f; ?>)">
                <option value=''>Choose Pickup Point</option>
                    <?php foreach ($routes as $key => $value) {
                        $stoppage_rs = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$value['id']."' ORDER BY pickup_time")->queryAll();    	
                        	$stop = '[Route No: '.$value['route_no'].'] ';
    					if(count($stoppage_rs)>0) {
							foreach ($stoppage_rs as $key1 => $value_stoppage) {
							$stop.= $value_stoppage['stoppage'];
								if(count($stoppage_rs)>$key1+1)$stop.=  " -> ";
							} 
						}
                        echo "<option value='" . $value['id'] . "'>" . $stop ."-".$value['type']."-".$value['facility']. "-".($factor['multiply']*$value['price'])."TK</option>";
                       }
                        ?>
            </select>
           
	    </div>       
    </div>
     <!-- <datalist id="browsers">

	        <?php 
	        	/*$routes=Routes::model()->findAll();
	        	foreach ($routes as $key => $value) {
	        		 
	        		echo "<option value='".$value['id']."' width='200px'>";
	        	}*/
	        ?>
		</datalist> -->
    <?php $this->endWidget(); ?>
</div>