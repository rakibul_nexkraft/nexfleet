<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Seat Requests',
);
?>
<div id="route-request-list" class="seat-request-admin search-box-responsive">
	 <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">

        <h4 class="heading-custom page_header_h4">Route Request List</h4>
    </div>
  
    <?php 
    	 $this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive table-responsive2 bottommargin table-overflow-x admin-seat-request-table-responsive border-round-bottom'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model->search(),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(
			//'id',
			
			array(	
				 'header'=>'User',				
				'value'=>'$data->user_pin',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'user_name',
			//'user_department',
			array('header'=>'Route No',				
				'value'=>'$data->routeDetail($data->route_id,1)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Route Detail',	
			
				'type'=>'raw',
				'value'=>'$data->routeDetail($data->route_id,2,$data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//array('header'=>'Stoppage',
				
				//'type'=>'raw',
				//'value'=>'$data->stoppageDetail($data->stoppage_id)',
				//'htmlOptions'=>array('class'=>'cart-product-price'),
				//'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			//),
			array('header'=>'Pickup Time',
				'type'=>'raw',
				'value'=>'$data->stoppagePickupTime($data->stoppage_id)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			//'route_id',
			
			array('header'=>'Expected Date',				
				'value'=>'$data->expected_date',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			
		
			//'remarks',
			array('header'=>'User Status',
				'type'=>'raw',
				'value'=>'$data->userStatus($data->user_status)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Fleet Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status,$data->queue)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Fleet Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status,$data->queue)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(
				'header'=>'Applied Date & Time',
				'type'=>'raw',
				'value'=>'$data->created_time',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array('header'=>'Apprioval Date & Time',
				'type'=>'raw',
				'value'=>'$data->updated_time',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			/*array('header'=>'Queue',
	            'type'=>'raw',		
	            'value'=>'$data->queueCheck($data->queue,$data->status)',	
			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			/*array('header'=>'Apply Maternity Leave Use',
	            'type'=>'raw',			
				'value'=>'($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			/*array('header'=>'Update',
	            'type'=>'raw',			
				'value'=>'CHtml::submitButton("Update",array("class"=>"btn-search1","id"=>"search-button1","onClick"=>"routeInformationUpdate($data->id)","name"=>""))',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),*/
			//'status',
			/*'created_by',
			'created_time',
			'updated_by',
			'updated_time',
			*/
			/*array(
				'class'=>'CButtonColumn',
				'template'=>'{view}{update}{delete}',
				'deleteConfirmation'=>"js:'Do you really want to cancel request record with ID '+$(this).parent().parent().children(':nth-child(1)').text()+'?'",
				 'buttons'=>array(
	        		'delete' => array(
	            	'label'=>'Cancel Request', 

	        		),
	        	),
			),*/

		),
	)
	//array(
        //'htmlOptions' => array('class' => 'table cart')
    //)
	); 
	?>
</div>
 
<script>
	function routeInformationUpdate(requestId){
		
		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Choose Your Request</h5><div id='notice'></div><input type='radio' name='request' value='0'>Request For Approved<br><input type='radio' name='request' value='3'>Request For Cancel<br><input type='submit'  onClick='submitRequestUpdated("+requestId+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);

	}
	function submitRequestUpdated(requestId){
		 var v=$("input[name='request']:checked").val();		
			if(v==null){
		    	$("#notice").html("<h5 style='color:red;margin-bottom:5px;'>Please, Select A Request</h5>");
			}
			else{

				$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformationUpdate");?>',{requestId:requestId,updateRequest:v},function(data){
                    bsModalOpen(data);
							//alert(data);
					});
				
			}
	}
	function maternityUse(mid,fromDate,toDate){	

		var html="<div style='text-align: center;'><h5 style='margin-bottom:5px;'>Do You Want to Use From Date: "+fromDate+" To Date: "+toDate+"? </h5><h5 style='margin-bottom:5px;'>Your Route Request Will be Canceled. </h5><div id='notice'></div><input type='submit'  onClick='maternityUpdated("+mid+")'  value='Submit' class='btn btn-success' style='margin-top:20px'></div>";

        bsModalOpen(html);
	
		}
	function maternityUpdated(maternityId){
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/maternityUpdated");?>',{maternityId:maternityId},function(data){
            bsModalOpen(data);
							//alert(data);
					});
				
			
	}	
		
</script>