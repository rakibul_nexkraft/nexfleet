<style>
    .fs-12{
        font-size: 12px;
    }
    .td-right-border {
        border-right: 1px solid rgb(112, 112, 112, 0.05) !important;
    }
    .tr-bottom-border{
        border-bottom: 1px solid rgb(112, 112, 112, 0.05)
    }
    .tr-bottom-top{
        border-top: 1px solid rgb(112, 112, 112, 0.05)
    }
    .table-responsive {
        border: 0px solid #f2ecec; 
    }
</style>
<div id="route-request-list" class="custom-box-design booking-history responsive-booking-history">
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">

        <h4 class="heading-custom page_header_h4">Route Detail</h4>
    </div>
    <div class="table-responsive bottommargin responsive-sell-booking-table responsive-booking-history-table border-round-bottom">

        <table class="table cart">
            <!-- <thead>
            <tr>
                <th class="cart-product-price">Route No</th>
               
                <th class="cart-product-price">Route Detail</th>
                <th class="cart-product-price">Pickup Time</th>
                <th class="cart-product-price">Price</th>             
                <th class="cart-product-price">Expected Date</th>
                <th class="cart-product-price">User Status</th>
                <th class="cart-product-price">Fleet Status</th>                
                <th class="cart-product-price">Queue</th>
                <!-- <th class="cart-product-price">Fleet Status</th> -->

               
            <!-- </tr>
            </thead> -->
            <tbody>
            <?php if ($detail): ?>                
                    <tr class="cart_item tr-bottom-border tr-bottom-top">
                        <td class="cart-product-price">Route No : </td>                 
                        <td class="cart-product-price td-right-border"><?php echo $detail['route_no'];?></td>
                        <td class="cart-product-price">Route Detail : </td>
                        <td class="cart-product-price td-right-border"><?php echo SeatRequest::routeDetail($detail['route_id'],2,$detail['stoppage_id']); ?></td>                        
                    </tr>
                    <tr class="cart_item tr-bottom-border">
                        <td class="cart-product-price ">Vehicle NO : </td>
                        <td class="cart-product-price td-right-border"><?php echo $detail['reg_no']; ?></td>
                        <td class="cart-product-price ">vehicle Type: </td>
                        <td class="cart-product-price td-right-border"><?php echo $detail['type']; ?></td>  
                        
                    </tr>
                    <tr class="cart_item tr-bottom-border">
                        <td class="cart-product-price ">Driver Name : </td>
                        <td class="cart-product-price td-right-border"><?php echo $detail['name']; ?></td>
                        <td class="cart-product-price ">Driver Phone :  </td>
                        <td class="cart-product-price td-right-border"><?php echo $detail['phone']; ?></td>  
                        
                    </tr>
                     <tr class="cart_item tr-bottom-border">
                        <td class="cart-product-price ">Pickup Time : </td>
                        <td class="cart-product-price td-right-border"><?php echo SeatRequest::stoppagePickupTime($detail['stoppage_id']); ?></td>
                        <td class="cart-product-price ">Price : </td>
                        <td class="cart-product-price td-right-border"><?php echo $detail['price']; ?></td>  
                        
                    </tr>
                   
                    <tr class="cart_item tr-bottom-border">  
                        <td class="cart-product-price ">User Status : </td>                     
                        <td class="cart-product-price td-right-border"><?php echo SeatRequest::userStatus($detail['user_status']); ?></td>  
                        <td class="cart-product-price">Fleet Status : </td> 
                        <td class="cart-product-price td-right-border"><?php echo SeatRequest::status($detail['status']);?></td> 
                        
                    </tr>  
                     <tr class="cart_item tr-bottom-border">   
                        <td class="cart-product-price">Expec. Date :  </td>                   
                        <td class="cart-product-price td-right-border"><?php echo $detail['expected_date'];?></td>
                        <td class="cart-product-price">Queue : </td>
                        <td class="cart-product-price td-right-border"><?php echo SeatRequest::queueCheck($detail['queue'],$detail['status']);?></td>                       
                                       
                    </tr>
            <?php else: ?>
                <tr>
                    <td colspan="7" class="empty"><span class="empty">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
       
    </div>
</div>
