 <?php 
 if(isset($routes_rs)){ ?>
 	<div class="col_full page_header_div">
        <h4 class="heading-custom page_header_h4">ROUTES LIST</h4>
    </div>
 
   	
  
    <div class="table-responsive bottommargin tbl_rou">

						<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-price">Route No</th>
									<th class="cart-product-price">Route Detail</th>
									<th class="cart-product-price">Vehicle Type</th>
									<th class="cart-product-price">Total Seat</th>
									<th class="cart-product-price">Available Seat</th>
									<th class="cart-product-price">Waiting</th>
									<th class="cart-product-price">M.Leave</th>
									<th class="cart-product-price">Price</th>									
									<th class="cart-product-price"></th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(count($routes_rs)>0){
									foreach($routes_rs as $key=>$value){

								?>
								<tr class="cart_item">
									<td  class="cart-product-price">
										<?php echo $value['route_no'];?>
									</td >

									<td  class="cart-product-price">
										<?php 											
    									$stoppage_rs = Yii::app()->db->createCommand("SELECT * FROM tbl_stoppage WHERE route_id='".$value['id']."' ORDER BY pickup_time")->queryAll();    									
    									if(count($stoppage_rs)>0) {
											foreach ($stoppage_rs as $key1 => $value_stoppage) {					
												echo $value_stoppage['stoppage'];
												if(count($stoppage_rs)>$key1+1)echo " -> ";
											} 
										}
									?>
									</td >

									<td  class="cart-product-price">
										<?php echo $value['type'];?>
									</td >

									<td  class="cart-product-price">
										<?php echo $total_seat=$value['actual_seat'];?>
									</td >

									<td  class="cart-product-price">
										<?php											
    										 $borderPass=Routes::borderPass($value['route_no']);
    										 echo $av_seat=$total_seat-$borderPass;
										?>
										
									</td >

									<td  class="cart-product-price">
										<?php $sql_queue="SELECT max(queue) as maxqueue FROM tbl_seat_request WHERE status=1 AND route_id='".$value['id']."' AND queue>=0";
											$queue_rs=Yii::app()->db->createCommand($sql_queue)->queryRow();
											if($av_seat>0){
											echo 0;
												}
											else{
												echo $queue_rs['maxqueue']+1;
											}

										?>
									</td >
									<td  class="cart-product-price">
										<a href="#"  ><?php echo Routes::onMatLeave($value['route_no']);?></a>
									</td >
									<td  class="cart-product-price">
										<?php echo $value['price'];?>
									</td >
									<td  class="cart-product-price">
										<div class="input-field first-wrap">
		    							<?php 		    							
		    							echo CHtml::submitButton("Continue",array('id'=>'search-button','onClick'=>'pickndropForm('.$value["id"].','.$f.')'));	
		    							
		    							?>
										</div>
									</td >
								</tr>
								<?php } 
									}
									else{


								?>
								<tr>
									<td  colspan="9" class="cart-product-price">
										No Route Found!
									</td >
								</tr>
								<?php } ?>							
								
							</tbody>

						</table>

					</div>
			
	
		<?php } ?>