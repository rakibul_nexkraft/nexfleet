<?php
/* @var $this SeatRequestseatRequestController */
/* @var $model SeatRequest */

$this->breadcrumbs=array(
	'Seat Requests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Seat Request', 'url'=>array('index')),
	array('label'=>'Create Seat Request', 'url'=>array('create')),
);

/*Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('seat-request-grid', {
		data: $(this).serialize()
	});
	return false;
});
");*/
?>

<!--<h1>Manage Seat Requests</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'seat-request-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'user_pin',
		'user_name',
		'user_department',
		'route_id',
		'expected_date',
	
		'remarks',
		'status',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
		
		array(
			'class'=>'CButtonColumn',
			
		),
	),
)); ?>
