<?php
/* @var $this SeatRequestseatRequestController */
/* @var $model SeatRequest */

$this->breadcrumbs=array(
	'Seat Requests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Seat Request', 'url'=>array('index','user_pin'=>$user_pin)),
	array('label'=>'Create Seat Request', 'url'=>array('create','user_pin'=>$user_pin)),
	array('label'=>'Update Seat Request', 'url'=>array('update', 'id'=>$model->id,'user_pin'=>$user_pin)),
	//array('label'=>'Delete Seat Request', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Seat Request', 'url'=>array('admin')),
);
?>

<h1>View Seat Request #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'id',
		'user_pin',
		'user_name',
		'user_department',
		
		array('label'=>'Route Detail',
            'type'=>'raw',
			'value'=>$model->routeDetail($model->route_id),
		),
		array('label'=>'Stoppage',
            'type'=>'raw',
			'value'=>$model->stoppageDetail($model->stoppage_id),
		),
		array('label'=>'Pickup Time',
            'type'=>'raw',
			'value'=>$model->stoppagePickupTime($model->stoppage_id),
		),
		array('label'=>'status',
            'type'=>'raw',
			'value'=>SeatRequestAdmin::status($model->status),
		),
		array('label'=>'Queue',
            'type'=>'raw',			
			'value'=>SeatRequestAdmin::queueCheck($model->queue,$model->status),
		),
		'expected_date',
		'remarks',
		array('label'=>'status',
            'type'=>'raw',
			'value'=>SeatRequestAdmin::status($model->status),
		),

		
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
