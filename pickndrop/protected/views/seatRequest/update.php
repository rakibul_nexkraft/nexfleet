<?php
/* @var $this SeatRequestseatRequestController */
/* @var $model SeatRequest */

$this->breadcrumbs=array(
	'Seat Requests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Seat Request', 'url'=>array('index')),
	array('label'=>'Create Seat Request', 'url'=>array('create')),
	array('label'=>'View Seat Request', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Seat Request', 'url'=>array('admin')),
);
?>
<?php if(isset($_GET['user_pin'])){ ?>
<h1>Update Your Seat Request </h1>
<?php }
else { 
	?>
	<h1>Update Seat Request ID#<?php echo $model->id; ?></h1>
<?php
}
?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>