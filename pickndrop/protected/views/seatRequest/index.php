<div class="s002">
     									
     <?php $form=$this->beginWidget('CActiveForm', array(
		   'id'=>'movements-form',
			'enableAjaxValidation'=>false,
	)); ?>
     
    <div class="inner-form">
        <div class="input-field first-wrap">
            <div class="icon-wrap">
			    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">						
					</path>
		        </svg>
            </div>            											
            <?php echo $form->textField($model,'from',array('placeholder'=>'From','id'=>"search",'list'=>'browsers'));
            ?>
            											
	    </div>
	    <div class="input-field first-wrap">
           	<div class="icon-wrap">
			    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					<path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">						
					</path>
				</svg>
			</div>
			<?php echo $form->textField($model,'to',array('placeholder'=>'To','id'=>"search",'list'=>'browsers')); ?>
	    </div>
	    <?php $stoppages=Stoppage::model()->findAll();?>
	    <datalist id="browsers">
	        <?php 
	        	foreach ($stoppages as $key => $value) {			
	        		echo "<option value='".$value['stoppage']."'>";
	        	}
	        ?>
		</datalist>
		<div class="input-field first-wrap">
		    <div class="icon-wrap ">
			    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
				    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
				    </path>
                </svg>
			</div>
			<?php echo $form->textField($model,'expected_date',array('placeholder'=>'Expected Date','id'=>'depart','class'=>'datepicker')); ?>
		</div>
        <div class="input-field fouth-wrap">
		  	<div class="icon-wrap">
		        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
		            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">		            	
		            </path>
				</svg>
			</div>
			<?php  $sql_vehicle_type = "SELECT * FROM tbl_vehicletypes";
             	$command = Yii::app()->db->createCommand($sql_vehicle_type)->queryAll();
            ?>
            <select data-trigger="" name="SeatRequest[vehicle_type_id]">
            	<option value=''>Vehicle Type</option>
            	<?php foreach($command as $key=>$value)
				    echo "<option value='".$value['id']."'>".$value['type']."</option>";
				?>
			</select>
		</div>
		<div class="input-field first-wrap">
		    <?php  echo CHtml::submitButton("Search",array('class'=>'btn-search','id'=>'search-button')); ?>
		</div>
    </div>
    <?php $this->endWidget(); ?>
    </div>
    <?php if(isset($routes_rs)){ ?>
   <div id="route-list">
   	<div class="center s002 ">  

   		<h4 class="heading-custom">ROUTES LIST</h4>
   	
   	</div>
  
    <div class="table-responsive bottommargin">

						<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-price">Route No</th>
									<th class="cart-product-price">Route Detail</th>
									<th class="cart-product-price">Vehicle Type</th>
									<th class="cart-product-price">Total Seat</th>
									<th class="cart-product-price">Available Seat</th>
									<th class="cart-product-price">Waiting</th>
									<th class="cart-product-price">Leave</th>
									<th class="cart-product-price">Price</th>									
									<th class="cart-product-price"></th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(count($routes_rs)>0){
									foreach($routes_rs as $key=>$value){

								?>
								<tr class="cart_item">
									<td  class="cart-product-price">
										<?php echo $value['route_no'];?>
									</td >

									<td  class="cart-product-price">
										<?php 
											$stoppage_sql="SELECT * FROM tbl_stoppage WHERE route_id='".$value['id']."' ORDER BY pickup_time";
    										$stoppage_rs=Yii::app()->db->createCommand($stoppage_sql)->queryAll();
    										$stoppage_id=0;
    										if(count($stoppage_rs)>0){
												foreach ($stoppage_rs as $key1 => $value_stoppage) {
													if($value_stoppage['stoppage']==$model->from || $value_stoppage['stoppage']==$model->to){
														echo "<span style='color:#47ae4b'>".$value_stoppage['stoppage']."</span>";
														$stoppage_id=$value_stoppage['id'];
													}
													else{
														echo $value_stoppage['stoppage'];
													}

													if(count($stoppage_rs)>$key1+1)echo "-";
												} 
										    }
										//echo $value['route_detail'];?>
									</td >

									<td  class="cart-product-price">
										<?php echo $value['type'];?>
									</td >

									<td  class="cart-product-price">
										<?php echo $total_seat=$value['actual_seat'];?>
									</td >

									<td  class="cart-product-price">
										<?php											
    										 $borderPass=Routes::borderPass($value['route_no']);
    										 echo $av_seat=$total_seat-$borderPass;
										?>
										
									</td >

									<td  class="cart-product-price">
										<?php $sql_queue="SELECT max(queue) as maxqueue FROM tbl_seat_request WHERE status=1 AND route_id='".$value['id']."' AND queue>=0";
											$queue_rs=Yii::app()->db->createCommand($sql_queue)->queryRow();
											if($av_seat>0){
											echo 0;
												}
											else{
												echo $queue_rs['maxqueue']+1;
											}

										?>
									</td >
									<td  class="cart-product-price">
										<a href="#"  ><?php echo Routes::onMatLeave($value['route_no']);?></a>
									</td >
									<td  class="cart-product-price">
										<?php echo $value['price'];?>
									</td >
									<td  class="cart-product-price">
										<div class="input-field first-wrap">
		    							<?php  echo CHtml::submitButton("Continue",array('class'=>'btn-search1','id'=>'search-button1','onClick'=>'routeInformation('.$value["id"].',"'.$stoppage_id.'","'.$model->expected_date.'")')); ?>
										</div>
									</td >
								</tr>
								<?php } 
									}
									else{


								?>
								<tr>
									<td  colspan="9" class="cart-product-price">
										No Route Found!
									</td >
								</tr>
								<?php } ?>							
								
							</tbody>

						</table>

					</div>
			
		</div>
		<?php }?>
	 <div id="submit-information">
	   	<!--<div class="center s002 ">  

	   		<h4 class="heading-custom">SUBMIT YOUR INFORMATION</h4>
	   	
	   	</div>
	   	<div class="accordion accordion-border clearfix">
	   		<div class="col_one_third">
	   			<div class="information-box">
	   				<div class="steering clearfix">
	   					<img  src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/car/steering.jpg" alt="Steering">
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">

	   					<div class="seat-location">1</div>
	   					<div class="seat-location">2</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location">3</div>
	   					<div class="seat-location">4</div>
	   					
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">
	   					
	   					<div class="seat-location">5</div>
	   					<div class="seat-location">6</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location">7</div>
	   					<div class="seat-location">8</div>
	   					
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">
	   					
	   					<div class="seat-location">9</div>
	   					<div class="seat-location">10</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location">11</div>
	   					<div class="seat-location">12</div>
	   					
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">	   					
	   					<div class="seat-location-white">13</div>
	   					<div class="seat-location-white">14</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location-white">15</div>
	   					<div class="seat-location-white">16</div>	   					
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">	   					
	   					<div class="seat-location-white">17</div>
	   					<div class="seat-location-white">18</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location-white">19</div>
	   					<div class="seat-location-white">20</div>	   					
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="seat-plan">	   					
	   					<div class="seat-location-white">21</div>
	   					<div class="seat-location-white">22</div>
	   					<div class="seat-location-gap"></div>
	   					<div class="seat-location-white">23</div>
	   					<div class="seat-location-white">24</div>	   					
	   				</div>
	   				<div class="clearfix"></div>
	   				<div class="min-hight"></div>
	   			</div>
	   		</div>
	   		<div class="col_one_third">
	   			<div class="information-box">
	   					<div class="center s002 ">  

   							<h4 class="heading-custom">Route 01  Stoppages List</h4>
   	
   						</div>
   						<div class="clearfix"></div>
   						<div class="stoppage-table">
   							<div class="table-responsive">

						<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-price"><strong>Stoppage</strong></th>
									<th class="cart-product-price"><strong>Time</strong></th>
									
								</tr>
							</thead>
							<tbody>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mirpur
									</th >

									<th  class="cart-product-price">
										7:30
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Framgate
									</th >

									<th  class="cart-product-price">
										8:00
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mohakhali
									</th >

									<th  class="cart-product-price">
										8:30
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mirpur
									</th >

									<th  class="cart-product-price">
										7:30
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Framgate
									</th >

									<th  class="cart-product-price">
										8:00
									</th >

								</tr>
								<tr class="cart_item">
									<th  class="cart-product-price">
										Mohakhali
									</th >

									<th  class="cart-product-price">
										8:30
									</th >

								</tr>
								
							</tbody>

						</table>

					</div>
   						</div>
	   			</div>
	   		</div>
	   		<div class="col_one_third col_last">
	   			<div class="information-box-last">
	   				<div class="driver-info">
		   				<div class="driver-info-row">
		   					<strong>Driver Name :</strong> Joynal Hassan
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Driver Pin :</strong>  012548963
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Driver Phone :</strong>  016848886958
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Route No:</strong>  02
		   				</div>
		   				<div class="driver-info-row">
		   					<strong>Price :</strong> 60
		   				</div>
	   				</div>
	   				
	   			</div>
	   			<div class="information-submit-button-div">
	   				<?php  //echo CHtml::submitButton("Submit",array('class'=>'btn-search1','id'=>'search-button1')); ?>
	   			</div>
	   		</div>
	   		
	   	</div>-->

   </div>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
   	const choices = new Choices('[data-trigger]',
    {
        searchEnabled: false,
        itemSelectText: '',
    });

</script>
<script type="text/javascript">
	function routeInformation(routeId,stoppageId,expectedDate,changeRoute=null){
		if(stoppageId==0){
            bsModalOpen('<h3>Stoppage Not Selected!</h3>');
		}
		else if(expectedDate==''){
            bsModalOpen('<h3>Expected Date Not Selected!</h3>');
		}
		else{
			$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/routeInformation");?>',{route_id:routeId,stoppageId:stoppageId,expectedDate:expectedDate,changeRoute:changeRoute},function(data){
							$('#submit-information').html(data);
							flatpickr(".datepicker",{});
								const choices = new Choices('[data-trigger]',
							    {
							        searchEnabled: false,
							        itemSelectText: '',
							    });
					});
		}
	}
	function routeSubmit(routeId,stoppageId,expectedDate,changeRoute=null){

		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/userRouetRquest");?>',{route_id:routeId,stoppageId:stoppageId,expectedDate:expectedDate,changeRoute:changeRoute},function(data){
						if(data==1){
							data="<h3>Your Request Send To Addmin Successfully.</h3>";
							data="<h3>Please Wait For Admin Approval.</h3>";
							
							data=data+'<?php echo CHtml::link("View Seat Request List",array("seatRequest/requestList"),array("class"=>"btn btn-success"));?>';
							data=data+'<?php echo CHtml::link("Seat Request Page",array("seatRequest/index"),array("class"=>"modelRightButton btn btn-success"));?>';
                            bsModalOpen(data);
						}
						else{
                            bsModalOpen('<h3>'+data+'</h3>');
						}
						
				});
	}
	function updateSeatRequest(){
		var id=$('#SeatRequest-id').val();
		var expDate=$('#depart1').val();
		var seatRequestStoppageId=$('#SeatRequest-stoppage-id').val();
		var seatRequestStatus=$('#SeatRequest-status').val();
		$.post('<?php echo Yii::app()->createAbsoluteUrl("seatRequest/userRouteRquestUpdate");?>',{id:id,expDate:expDate,seatRequestStoppageId:seatRequestStoppageId,seatRequestStatus:seatRequestStatus},function(data){
						if(data==1){
							data="<h3>Your Request Send To Addmin Successfully.</h3>";
						//	data="<h3>Please Wait For Approved.</h3>";
							
							data=data+'<?php echo CHtml::link("View Seat Request List",array("seatRequest/requestList"),array("class"=>"btn btn-success"));?>';
							data=data+'<?php echo CHtml::link("Seat Request Page",array("seatRequest/index"),array("class"=>"modelRightButton btn btn-success"));?>';
                            bsModalOpen(data);
						}
						else{
                            bsModalOpen('<h3>'+data+'</h3>');
						}
						
				});
		
		return false;
	}
</script>

