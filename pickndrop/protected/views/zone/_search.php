<?php
/* @var $this ZoneController */
/* @var $model Zone */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <div class="fl">

        <div class="row">
            <?php echo $form->label($model,'name'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>500,'class'=>'input-medium')); ?>
        </div>

    </div>


    <div class="clearfix"></div>

    <div align="left">
        <?php $this->widget('bootstrap.widgets.TbButton',array(
            'label' => 'Search',
            'type' => 'primary',
            'buttonType'=>'submit',
            'size' => 'medium'
        ));
        ?>
    </div>
	<!--<div class="row">
		<?php /*echo $form->label($model,'id'); */?>
		<?php /*echo $form->textField($model,'id'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'name'); */?>
		<?php /*echo $form->textField($model,'name',array('size'=>60,'maxlength'=>500)); */?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'remark'); */?>
		<?php /*echo $form->textField($model,'remark',array('size'=>60,'maxlength'=>500)); */?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'created_by'); */?>
		<?php /*echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>128)); */?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'created_time'); */?>
		<?php /*echo $form->textField($model,'created_time'); */?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'updated_by'); */?>
		<?php /*echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>128)); */?>
	</div>

	<div class="row">
		<?php /*echo $form->label($model,'updated_time'); */?>
		<?php /*echo $form->textField($model,'updated_time'); */?>
	</div>-->

	<!--<div class="row buttons">
		<?php /*echo CHtml::submitButton('Search'); */?>
	</div>-->

<?php $this->endWidget(); ?>

</div><!-- search-form -->