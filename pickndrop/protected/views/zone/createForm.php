<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Zone
	</div>
	<div class="togglec" style="display: none;">
		<div id="zone-div">
		<?php		
			$this->renderPartial('/zone/zone_form',array(
				'model'=>$zone,				
			));		
		?>		
		</div>
	</div>
</div>
<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Route</div>
		<div class="togglec" style="display: none;">
		<div id="route-div">		
		<?php		
			$this->renderPartial('/routes/route_form',array(
				'model'=>$routes,				
			));		
		?>
		</div>
	</div>
</div>
<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Stoppage</div>
	<div class="togglec" style="display: none;">
		<div id="stoppage-div">
		<?php		
			$this->renderPartial('/stoppage/stoppage_form',array(
				'model'=>$stoppage,				
			));		
		?>	
		</div>	
	</div>
</div>
<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Create Route Factor
	</div>
	<div class="togglec" style="display: none;">
		<div id="route-factor-div">
		<?php		
			$this->renderPartial('/routeFactor/factor_form',array(
				'model'=>$route_factor,				
			));		
		?>		
		</div>
	</div>
</div>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script>
   	const choices = new Choices('[data-trigger]',
    {
        searchEnabled: true,
        itemSelectText: '',
    });
</script>
