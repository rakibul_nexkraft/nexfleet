<?php
/* @var $this ZoneController */
/* @var $model Zone */

$this->breadcrumbs=array(
	'Zones'=>array('index'),
	$model->name,
);

/*$this->menu=array(
	array('label'=>'List Zone', 'url'=>array('index')),
	array('label'=>'Create Zone', 'url'=>array('create')),
	array('label'=>'Update Zone', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Zone', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Zone', 'url'=>array('admin')),
);*/
?>
<div id="route-request-list">
    <div class="center s002 ">
        <!-- Floating Menu Button -->
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">
                <!--                <i class="floating-menu-label vs-icon notranslate icon-scale vs-button--icon  feather icon-settings null"></i>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'List Zone', 'url'=>array('/zone/index')),
                        array('label'=>'Create Zone', 'url'=>array('/zone/create')),

                    ),
                ));

                ?>
            </div>
        </div>
    </div>
    <div class="col_full page_header_div">
        <h4 class="heading-custom page_header_h4">View Zone ID#<?php echo $model->id; ?></h4>
    </div>

<div class="s002">
	<div class = 'table-responsive bottommargin overflow-auto col_full'>

	<?php $this->widget('zii.widgets.CDetailView', array(
		'htmlOptions' => array('class' => 'table cart'),
		'data'=>$model,
		'attributes'=>array(
			'id',
			'name',
			'remark',
			'created_by',
			'created_time',
			'updated_by',
			'updated_time',
		),
	)); ?>
	</div>
</div>