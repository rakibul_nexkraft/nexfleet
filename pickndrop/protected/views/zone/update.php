<?php
/* @var $this ZoneController */
/* @var $model Zone */

$this->breadcrumbs=array(
    'Zones'=>array('index'),
    $model->name=>array('view','id'=>$model->id),
    'Update',
);


?>
<div id="route-request-list">
    <div class="center s002 ">
        <!-- Floating Menu Button -->
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">
                <!--                <i class="floating-menu-label vs-icon notranslate icon-scale vs-button--icon  feather icon-settings null"></i>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'List Zone', 'url'=>array('/zone/index')),
                        array('label'=>'Create Zone', 'url'=>array('/zone/create')),

                    ),
                ));

                ?>
            </div>
        </div>
    </div>
    <div class="col_full page_header_div border-round-top" style="border-bottom: 1px solid #f2ecec;">
        <h4 class="heading-custom page_header_h4">Update Zone ID# <?php echo $model->id;?></h4>
    </div>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>