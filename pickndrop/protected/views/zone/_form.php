<?php
/* @var $this ZoneController */
/* @var $model Zone */
/* @var $form CActiveForm */
?>

<div class="s002 update-form-background border-round-bottom">


    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'zone-form',
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php /*echo $form->errorSummary($model); */
    ?>
    <div style="display: flex;">
        <div class="inner-form zone-form">
            <div>
                <div class="col_full zone-form-box">
                    <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'name'); ?></div>
                    <div class="input-field first-wrap zone-search-field col_two_third">
                        <div class="icon-wrap">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                                </path>
                            </svg>
                        </div>
                        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 500)); ?>
                    </div>
                </div>
                <div class="error-message-position">
                    <?php echo $form->error($model, 'name'); ?>
                </div>
            </div>
        </div>
        <div class="inner-form zone-form">
            <div class="col_full zone-form-box">
                <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'remark'); ?></div>
                <div class="input-field first-wrap zone-search-field col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'remark', array('size' => 60, 'maxlength' => 500)); ?>
                    <?php echo $form->error($model, 'remark'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="inner-form">
        <div class="col_full">
            <div class="input-field first-wrap form-save-button" style="border: none !important;">
                <?php echo CHtml::submitButton("Save", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
	