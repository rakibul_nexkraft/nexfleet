<?php
/* @var $this MaternityLeaveRequestController */
/* @var $model MaternityLeaveRequest */

$this->breadcrumbs=array(
	'Maternity Leave Requests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Request', 'url'=>array('create')),
	array('label'=>'Update Maternity Leave Request', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Maternity Leave Request', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage MaternityLeaveRequest', 'url'=>array('admin')),
);
?>

<h1>View MaternityLeaveRequest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_pin',
		'from_date',
		'to_date',
		'status',
		'created_by',
		'updated_by',
		'created_time',
		'update_time',
	),
)); ?>
