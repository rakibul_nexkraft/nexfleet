<?php
/* @var $this MaternityLeaveRequestController */
/* @var $model MaternityLeaveRequest */

$this->breadcrumbs=array(
	'Maternity Leave Requests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Maternity Leave Request', 'url'=>array('index')),
	array('label'=>'Create Maternity Leave Request', 'url'=>array('create')),
	array('label'=>'View Maternity Leave Request', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Maternity Leave Request', 'url'=>array('admin')),
);
?>

<h1>Update Maternity Leave Request <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>