<?php
/* @var $this MaternityLeaveRequestController */
/* @var $model MaternityLeaveRequest */

$this->breadcrumbs=array(
	'Maternity Leave Requests'=>array('index'),
	'Create',
);


?>

<h1>Create Maternity Leave Request</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>