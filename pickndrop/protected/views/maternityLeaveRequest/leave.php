
<div id="route-request-list custom-box-design responsive-leave-request">
    <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    <div class="center s002 responsive-leave-request border-round-top" >
        <h4 class="heading-custom page_header_h4 table-header-text ">Maternity Leave Requests List</h4>
    </div>




<?php 

$this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin border-round-bottom'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model_list->search(),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(
			//array(	
				// 'header'=>'ID',				
				//'value'=>'$data->id',			
				//'htmlOptions'=>array('class'=>'cart-product-price'),
				//'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			//),
			array(	
				 'header'=>'User Pin',				
				'value'=>'$data->user_pin',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(	
				 'header'=>'From Date',				
				'value'=>'$data->from_date',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(	
				 'header'=>'To Date',				
				'value'=>'$data->to_date',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		
			array(
				'header'=>'Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'
			),
		),		
	),
)); 
?>
</div>
