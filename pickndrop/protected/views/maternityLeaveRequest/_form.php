<?php
/* @var $this MaternityLeaveRequestController */
/* @var $model MaternityLeaveRequest */
/* @var $form CActiveForm */
?>


        <div class="s002 custom-box-design  leave-request responsive-leave-request">

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'maternity-leave-request-form',
                'enableAjaxValidation'=>false,
            )); ?>
            <p class="note" style="padding-left: 15px;">Fields with <span class="required">*</span> are required.</p>


            <?php echo $form->errorSummary($model); ?>
            <div class="leave-form">
                <div class="inner-form" style="margin: 0 0 -20px 0; padding: 12px;">
                    <div class="col_half" style="display: flex;">
                        <div class="input-field first-wrap" style="margin-right: -82px;">
                            <div class="icon-wrap ">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                                    </path>
                                </svg>
                            </div>
                            <?php echo $form->textField($model,'from_date',array('placeholder'=>'Maternity Leave From','id'=>'depart','class'=>'datepicker')); ?>
                        </div>
                    </div>
                </div>
                <div class="inner-form leave-inner-class-form">
                    <div class="col_half" style="display: flex;">


                        <div class="input-field first-wrap" style="margin-right: -82px;">
                            <div class="icon-wrap ">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                                    </path>
                                </svg>
                            </div>
                            <?php echo $form->textField($model,'to_date',array('placeholder'=>'Maternity Leave To','id'=>'depart','class'=>'datepicker')); ?>
                        </div>
                    </div>
                </div>
                <div class="inner-form leave-inner-class-form responsive-leave-inner-class-form">
                    <div class="col_half" style="display: flex;">
                        <div class="input-field first-wrap" style="margin-right: -82px;">
                            <?php  echo CHtml::submitButton("Submit",array('class'=>'btn-search button-pink','id'=>'search-button')); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>

    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
    <script>flatpickr(".datepicker",{});</script>
    <script>
        const choices = new Choices('[data-trigger]',
            {
                searchEnabled: false,
                itemSelectText: '',
            });

    </script>
<style>
   .w100{
       width: 100%;
   }
</style>