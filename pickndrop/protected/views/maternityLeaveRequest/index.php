<?php
/* @var $this MaternityLeaveRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Maternity Leave Requests',
);
?>

<div id="route-request-list">
   	<div class="center s002 ">
   		<h4 class="heading-custom">Maternity Leave Requests List</h4>
	</div>

<?php 

$this->widget('zii.widgets.grid.CGridView', array(
		'itemsCssClass' => 'table cart',
		'htmlOptions' => array('class' => 'table-responsive bottommargin'),
		//'id'=>'seat-request-grid',
		'rowCssClass'=>array('cart_item'),
		'dataProvider'=>$model->search(),
		//'filter'=>$model,
		//'htmlOptions'=>array('style'=>'text-align: center'),
		'columns'=>array(
			//array(	
				// 'header'=>'ID',				
				//'value'=>'$data->id',			
				//'htmlOptions'=>array('class'=>'cart-product-price'),
				//'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			//),
			array(	
				 'header'=>'User Pin',				
				'value'=>'$data->user_pin',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(	
				 'header'=>'From Date',				
				'value'=>'$data->from_date',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
			array(	
				 'header'=>'To Date',				
				'value'=>'$data->to_date',			
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),
			),
		
			array(
				'header'=>'Status',
				'type'=>'raw',
				'value'=>'$data->status($data->status)',
				'htmlOptions'=>array('class'=>'cart-product-price'),
				'headerHtmlOptions'=>array('class'=>'cart-product-price'),

			)
		
		//'created_by',
		/*
		'updated_by',
		'created_time',
		'update_time',
		*/
	),
)); 
?>
</div>