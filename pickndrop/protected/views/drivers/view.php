<?php
/* @var $this DriversController */
/* @var $model Drivers */

$this->breadcrumbs=array(
	'Drivers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Drivers List', 'url'=>array('index')),
	array('label'=>'New Driver', 'url'=>array('create')),
	array('label'=>'Update Driver', 'url'=>array('update', 'id'=>$model->id)),
	//array('label'=>'Delete Driver', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?','csrf' => true)),
	array('label'=>'Manage Drivers', 'url'=>array('admin')),
);
?>

<h4>View Driver : <?php echo $model->id; ?></h4>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'pin',
		'phone',
        'dvr_license_no',
		'dvr_license_type',
        'driver_rating',
		'issued_from',
		'issued_date',
		'valid_to',
        'blood_group',
		'active',
	),
)); ?>

<h4>View Training  Details </h4>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'criminalcases-grid',
        'dataProvider'=>$model->driversTraining(),

        //'id,caseno,region,district,program,court,next_date,next_date_fixed,panel_lawyer_id

        'columns'=>array(
            array(
                'name'=>'driver_pin',
                'header'=>'PIN',
            ),
            array(
                'name'=>'name',
                'header'=>'Driver Name',

            ),
            array(
                'name'=>'phone',
                'header'=>'Phone',

            ),
            array(
                'name'=>'defence_traning',
                'header'=>'Defence Traning',
                'type'=>'raw',
                'value'=>'$data["defence_traning"]?"Yes":"No"',
            ),
            array(
                'name'=>'defence_traning_date',
                'header'=>'Defence Traning Date',
            ),
            array(
                'name'=>'gen_awarness_traning',
                'header'=>'Gen Awarness Traning',
                'type'=>'raw',
                'value'=>'$data["gen_awarness_traning"]?"Yes":"No"',
            ),
            array(
                'name'=>'gen_awarness_traning_date',
                'header'=>'Gen Awarness Traning Date',
            ),
            array(
                'name'=>'google_map_training',
                'header'=>'Google Map Training',
                'type'=>'raw',
                'value'=>'$data["google_map_training"]?"Yes":"No"',
            ),
            array(
                'name'=>'google_map_training_date',
                'header'=>'Google Map Training Date',
            ),
        ),
    ));
    ?>
<h4>View Crash Report  Details </h4>
    <?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'criminalcases-grid',
        'dataProvider'=>$model->driversCrashReport(),

        //'id,caseno,region,district,program,court,next_date,next_date_fixed,panel_lawyer_id

        'columns'=>array(
            array(
                'name'=>'pin',
                'header'=>'PIN',
            ),
            array(
                'name'=>'name',
                'header'=>'Driver Name',

            ),
           
            array(
                'name'=>'vehicle_reg_no',
                'header'=>'Defence Traning',
                
            ),
            array(
                'name'=>'place',
                'header'=>'Place',
            ),
            array(
                'name'=>'accident_date',
                'header'=>'Accident Date',
                
            ),
            array(
                'name'=>'accident_time',
                'header'=>'Accident Time',
            ),
            array(
                'name'=>'reason',
                'header'=>'Reason',
               
            ),
            array(
                'name'=>'demagedetail',
                'header'=>'Demage Detail',
            ),
            array(
                'name'=>'amount',
                'header'=>'Amount',
            ),
        ),
    ));
    ?>

<h4>View Vechiles Registration  Details </h4>
<?php


$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'criminalcases-grid',
    'dataProvider'=>$model->driverslog(),

    //'id,caseno,region,district,program,court,next_date,next_date_fixed,panel_lawyer_id

    'columns'=>array(
        array(
            'name'=>'pin',
            'header'=>'PIN',
        ),
        array(
            'name'=>'name',
            'header'=>'Driver Name',
        ),
        array(
            'name'=>'vehicle_reg_no',
            'header'=>'Vehicle Reg No',
        ),
        array(
            'name'=>'type',
            'header'=>'Vehicle Type',
        ),
    ),
));

    
 ?>