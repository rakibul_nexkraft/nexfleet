<?php
/* @var $this DriversController */
/* @var $model Drivers */

$this->breadcrumbs=array(
	'Drivers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Drivers List', 'url'=>array('index')),
	array('label'=>'Manage Drivers', 'url'=>array('admin')),
);
?>

<h4>New Driver</h4>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>