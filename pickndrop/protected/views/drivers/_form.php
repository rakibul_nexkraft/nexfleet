<?php
/* @var $this DriversController */
/* @var $model Drivers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'drivers-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

<div class="container1">
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pin'); ?>
		<?php echo $form->textField($model,'pin'); ?>
		<?php echo $form->error($model,'pin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone'); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'dvr_license_no'); ?>
		<?php echo $form->textField($model,'dvr_license_no'); ?>
		<?php echo $form->error($model,'dvr_license_no'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'dvr_license_type'); ?>
		<?php echo $form->textField($model,'dvr_license_type'); ?>
		<?php echo $form->error($model,'dvr_license_type'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'issued_from'); ?>
		<?php echo $form->textField($model,'issued_from'); ?>
		<?php echo $form->error($model,'issued_from'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'issued_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'issued_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->issued_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
		<?php echo $form->error($model,'issued_date'); ?>
	</div>
	    
	<div class="row">
		<?php echo $form->labelEx($model,'valid_to'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'valid_to',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->valid_to,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
		<?php echo $form->error($model,'valid_to'); ?>
	</div>

  
	
</div>
<div class="container2">
	
	
      <?php echo $form->labelEx($model,'blood_group'); ?>
    <?php echo $form->textField($model,'blood_group'); ?>
    <?php echo $form->error($model,'blood_group'); ?>

    <!-- <div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->textField($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?> 
	</div> -->

    <div class="row">
        <?php echo $form->labelEx($model,'active'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'active', $this->active_status,
        array('empty' => 'Select status...')); ?>
        <?php echo $form->error($model,'active'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'defence_traning'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'defence_traning', array('0'=>'No','1'=>'Yes')); ?>
        <?php echo $form->error($model,'defence_traning'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'gen_awarness_traning'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'gen_awarness_traning',  array('0'=>'No','1'=>'Yes')); ?>
        <?php echo $form->error($model,'gen_awarness_traning'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'google_map_training'); ?>
        <?php // echo $form->textField($model,'approve_status',array('size'=>11,'maxlength'=>11)); ?>
        <?php echo $form->dropDownList($model, 'google_map_training',  array('0'=>'No','1'=>'Yes')); ?>
        <?php echo $form->error($model,'google_map_training'); ?>
    </div>
    	<div class="row">
		<?php echo $form->labelEx($model,'defence_traning_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'defence_traning_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->defence_traning_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
		<?php echo $form->error($model,'defence_traning_date'); ?>
	</div>
		<div class="row">
		<?php echo $form->labelEx($model,'gen_awarness_traning_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'gen_awarness_traning_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->gen_awarness_traning_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
		<?php echo $form->error($model,'gen_awarness_traning_date'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'google_map_training_date'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'google_map_training_date',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->google_map_training_date,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
		<?php echo $form->error($model,'google_map_training_date'); ?>
	</div>
</div>
	<!--div class="row buttons">
		<?php // echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div-->
	
	<div class="clearfix"></div>
	
	<div align="left">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Save',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->