
<?php
/* @var $this DriversController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Drivers',
);

$this->menu=array(
	array('label'=>'New Driver', 'url'=>array('create')),
	array('label'=>'Manage Drivers', 'url'=>array('admin')),
);
?>

<h4>Drivers</h4>
<div class="search-form">
	<div class="wide form">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'pin'); ?> 
				<?php echo $form->textField($model,'pin'); ?>
			</div>
		</div>

		<div class="fl">
			<div class="row">

				<?php echo $form->label($model,'training'); ?> 
				<?php echo $form->dropDownList($model, 'training',  array('1'=>'Defensive Driving Training','2'=>'Gender Awareness Training','3'=>'Google Map Training','4'=>'All Training'),array('empty'=>'Select Option')); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'training_date_from'); ?> 
				<?php //echo $form->textField($model,'training_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'training_date_from',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->training_date_from,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
			</div>
		</div>
		<div class="fl">
			<div class="row">
				<?php echo $form->label($model,'training_date_to'); ?> 
				<?php //echo $form->textField($model,'training_date'); ?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,'attribute'=>'training_date_to',     

                    // additional javascript options for the date picker plugin
                    'options'=>array('autoSize'=>true,
                        'dateFormat'=>'yy-mm-dd',
                        'defaultDate'=>$model->training_date_to,
                        'changeYear'=>true,
                        'changeMonth'=>true,
                    ),
                )); ?>
			</div>
		</div>

			<div class="clearfix"></div>
		    
		    <div align="left">
		    <?php $this->widget('bootstrap.widgets.TbButton',array(
		        'label' => 'Search',
		        'type' => 'primary',
		        'buttonType'=>'submit', 
		        'size' => 'medium'
		        ));
		    ?>
		        
		    </div>

		<?php $this->endWidget(); ?>
	</div>

</div><!-- search-form -->

<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));*/ ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'drivers-grid',
	'dataProvider'=>$dataProvider,
    'filter'=>$model,
    'rowCssClassExpression'=>'$data->color',
	'columns'=>array(
		'id',
		array(
            'name' => 'name',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->name),array("view","id"=>$data->id))',
        ),
		'pin',
		'phone',
		array(
            'name' => 'defence_traning',
            'type'=>'raw',
            'value' => '($data->defence_traning==1)?"Yes":"NO"',
        ),
         'defence_traning_date',
		array(
            'name' => 'gen_awarness_traning',
            'type'=>'raw',
            'value' => '($data->gen_awarness_traning==1)?"Yes":"NO"',
        ),      
        'gen_awarness_traning_date',
        array(
            'name' => 'google_map_training',
            'type'=>'raw',
            'value' => '($data->google_map_training==1)?"Yes":"NO"',
        ),      
        'google_map_training_date',
	),
)); 


echo CHtml::normalizeUrl(CHtml::link('Export to Excel',array('excelTraining','criteria'=>$_GET['Drivers'])));

$this->widget('application.extensions.print.printWidget', array(
    //'coverElement' => '.container', //main page which should not be seen
    'cssFile' => '/fleet/themes/shadow_dancer/images/css/print.css',
    'printedElement' => '#drivers-grid', //element to be printed
    'title' => CHtml::image("/fleet/themes/shadow_dancer/images/logo1.png"),
	  'title' => 'Transport Department - Drivers List',
));
?>