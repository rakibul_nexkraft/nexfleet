<?php
/* @var $this RoutesController */
/* @var $model Routes */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

    <!--<div class="fl">
        <div class="row">
            <?php echo $form->label($model,'id'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'id'); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'route_no'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'route_no',array('size'=>20,'maxlength'=>20)); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'route_detail'); ?>
            <div class="clearfix"></div>
		    <?php echo $form->textField($model,'route_detail',array('size'=>60,'maxlength'=>127)); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'seat_capacity'); ?>
            <div class="clearfix"></div>
		    <?php echo $form->textField($model,'seat_capacity'); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'available_seat'); ?>
            <div class="clearfix"></div>
		    <?php echo $form->textField($model,'available_seat'); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'remarks'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>128)); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'created_by'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'created_time'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'created_time'); ?>
        </div>
    </div>

    <div class="fl">
        <div class="row">
            <?php echo $form->label($model,'active'); ?>
            <div class="clearfix"></div>
            <?php echo $form->textField($model,'active'); ?>
        </div>
    </div>-->
	<div class="row">
		<?php echo $form->label($model,'route_no'); ?><strong>: </strong>
		<?php //echo $form->textField($model,'route_no'); ?>
        <?php
        $this->widget('ext.ESelect2.ESelect2',array(
            'model'=>$model,
            'attribute'=>'route_no',
            'data'=>$model->getAllRouteNo(),
            'options'  => array(
                'style'=>'width:100%',
                'allowClear'=>true,
                'placeholder'=>'Select Route No',
                'minimumInputLength' => 1,
            ),
        ));
        ?>
	</div>

<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'route_no'); ?>
<!--		--><?php //echo $form->textField($model,'route_no',array('size'=>20,'maxlength'=>20)); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'route_detail'); ?>
<!--		--><?php //echo $form->textField($model,'route_detail',array('size'=>60,'maxlength'=>127)); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'seat_capacity'); ?>
<!--		--><?php //echo $form->textField($model,'seat_capacity'); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'available_seat'); ?>
<!--		--><?php //echo $form->textField($model,'available_seat'); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'remarks'); ?>
<!--		--><?php //echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>128)); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'created_by'); ?>
<!--		--><?php //echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>127)); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'created_time'); ?>
<!--		--><?php //echo $form->textField($model,'created_time'); ?>
<!--	</div>-->
<!---->
<!--	<div class="row">-->
<!--		--><?php //echo $form->label($model,'active'); ?>
<!--		--><?php //echo $form->textField($model,'active'); ?>
<!--	</div>-->

	<div class="clearfix"></div>
    
    <div align="left" style="margin-left: 70px;">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
        'label' => 'Search',
        'type' => 'primary',
        'buttonType'=>'submit', 
        'size' => 'medium'
        ));
    ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->