<?php
/* @var $this RoutesController */
/* @var $model Routes */
/* @var $form CActiveForm */
?>

<div class="s002 searchbar">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'routes-form',
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php /*echo $form->errorSummary($model);*/
    ?>

    <div class="inner-form seat-request-form">
        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'route_no'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'route_no', array('size' => 20, 'maxlength' => 20)); ?>
                </div>
                <?php echo $form->error($model, 'route_no'); ?>
        </div>

        <!--<div class="inner-form">
        <div class="col_half">
                <?php // echo $form->labelEx($model,'route_detail'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>		
		          <?php //echo $form->textField($model,'route_detail',array('size'=>60,'maxlength'=>127)); ?>
		          <?php // echo $form->error($model,'route_detail'); ?>
            </div>
        </div>
    </div>-->

        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'zone_id'); ?></div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php $zone_list = Zone::model()->findAll(array('order' => 'name ASC'));

                    ?>
                    <select data-trigger="" name="Routes[zone_id]">
                        <option value=''>A Zone Select</option>
                        <?php foreach ($zone_list as $key => $value) {
                            if (isset($model->zone_id) && !empty($model->zone_id) && $model->zone_id == $value['id']) {
                                echo "<option value='" . $value['id'] . "' selected>" . $value['name'] . "</option>";
                            } else {
                                echo "<option value='" . $value['id'] . "'>" . $value['name'] . "</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <?php echo $form->error($model, 'zone_id'); ?>

        </div>


        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'vehicle_reg_no'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'vehicle_reg_no', array('placeholder' => 'Vehicle Reg. No.', 'id' => "search", 'list' => 'browsers'));
                    ?>
                </div>
                <?php echo $form->error($model, 'vehicle_reg_no'); ?>

        </div>
    </div>

    <datalist id="browsers">
        <?php $vehicle = Vehicles::model()->findAll(array('condition' => 'active=1'));
        foreach ($vehicle as $key => $value) {
            echo "<option value='" . $value['reg_no'] . "'>";
        }
        ?>
    </datalist>
    <div class="inner-form seat-request-form">
        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'actual_seat'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'actual_seat'); ?>
                </div>
                <?php echo $form->error($model, 'actual_seat'); ?>
        </div>
        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'price'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'price'); ?>
                </div>
                <?php echo $form->error($model, 'price'); ?>
        </div>
        <div class="col_one_third seat-request-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model, 'market_price'); ?></div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'market_price'); ?>
                </div>
                <?php echo $form->error($model, 'market_price'); ?>
        </div>
    </div>

    <div class="col_full" style="width: 15%;float: right;margin: 0 0 -14px 0;">
        <div class="input-field first-wrap searchbar-search-button">
            <?php  echo CHtml::submitButton("Save",array('class'=>'btn-search button-pink','id'=>'search-button')); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>


<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',
        });

</script>