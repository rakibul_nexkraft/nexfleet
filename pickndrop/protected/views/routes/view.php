<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs = array(
    'Routes' => array('index'),
    $model->id,
);

//$this->menu=array(
//array('label'=>'Routes List ', 'url'=>array('index')),
//array('label'=>'New Route', 'url'=>array('create')),
//array('label'=>'Update Route', 'url'=>array('update', 'id'=>$model->id)),
//array('label'=>'Delete Route', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Manage Routes', 'url'=>array('admin')),
//array('label'=>'Add Stoppage', 'url'=>array('/stoppage/create','route_id'=>$model->id)),
//);
?>
<div id="route-request-list">
    <div class="center s002 ">
        <!-- Floating Menu Button -->
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">

                <!--                <i class="floating-menu-label vs-icon notranslate icon-scale vs-button--icon  feather icon-settings null"></i>-->
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'List Routes', 'url' => array('/routes/index')),
                        array('label' => 'Create Route', 'url' => array('/routes/create')),
                        array('label' => 'Create Stoppage', 'url' => array('/stoppage/create', 'route_id' => $model->id)),

                    ),
                ));

                ?>
            </div>
        </div>
    </div>

    <div class="col_full page_header_div">
        <h4 class="heading-custom page_header_h4">View Route ID#<?php echo $model->id; ?></h4>
    </div>
    <?php //echo $route_per_zone;?>
    <div class="s002">
        <div class='table-responsive bottommargin overflow-auto col_full'>
            <?php $this->widget('zii.widgets.CDetailView', array(
                'data' => $model,
                'htmlOptions' => array('class' => 'table cart'),
                'attributes' => array(
                    'id',
                    'route_no',
                    array(
                        'name' => 'route_detail',
                        'type' => 'raw',
                        'value' => SeatRequest::routeDetail($model->id, 3),
                    ),
                    'actual_seat',
                    array(
                        'name' => 'seat_capacity',
                        'type' => 'raw',
                        'value' => $model->borderPass($model->route_no),
                    ),
                    array(
                        'name' => 'available_seat',
                        'type' => 'raw',
                        'value' => $model->availableSeat($model->actual_seat, $model->borderPass($model->route_no), $model->route_no),
                    ),
                    array(
                        'name' => 'on_mat_leave',
                        'type' => 'raw',
                        'value' => $model->onMatLeave($model->route_no),
                    ),
                    array(
                        'name' => 'vehicle_reg_no',
                        'type' => 'raw',
                        'value' => $model->vehicle_reg_no,
                    ),
                    array(
                        'name' => 'zone_id',
                        'type' => 'raw',
                        'value' => $model->route_per_zone->name,
                    ),
                    'price',
                    'market_price',
                    //'remarks',
                    'created_by',
                    'created_time',
                    //'active',
                ),
            ));

            $sql = "SELECT * FROM tbl_stoppage where route_id='$model->id'";
            $sql_rs = Yii::app()->db->createCommand($sql)->queryAll();

            ?>
        </div>
    </div>
    <div class="col_full page_header_div">

        <h4 class="heading-custom page_header_h4">Stoppage List</h4>

    </div>
    <div class="table-responsive bottommargin">
        <table class="table cart route-view">

            <thead>
            <tr>
                <th class="cart-product-price">SI.</th>
                <th class="cart-product-price">Stoppage</th>
                <th class="cart-product-price">Pickup Time</th>
                <th class="cart-product-price">Created By</th>
                <th class="cart-product-price">Created Time</th>
                <th class="cart-product-price">Updated By</th>
                <!--<th class="cart-product-price">Updated Time</th>
                <th class="cart-product-price">Update</th>-->
            </tr>
            </thead>
            <tbody>
            <?php if (count($sql_rs) > 0) { ?>
                <?php $i = 1;
                foreach ($sql_rs as $key => $value) { ?>
                    <tr class="cart_item">

                        <td class="cart-product-price"><?php echo $i++; ?></td>
                        <td class="cart-product-price"><?php echo $value['stoppage']; ?></td>
                        <td class="cart-product-price"><?php echo $value['pickup_time']; ?></td>
                        <td class="cart-product-price"><?php echo $value['created_by']; ?></td>
                        <td class="cart-product-price"><?php echo $value['created_time']; ?></td>
                        <!--<td class="cart-product-price"><?php //echo $value['updated_by'];?></td>
				<td class="cart-product-price"><?php //echo $value['updated_time'];?></td>-->
                        <td class="cart-product-price"><?php echo CHtml::link('Update', array("/stoppage/update/", "id" => $value['id'])) ?></a></td>
                    </tr>
                <?php }
            } else { ?>
                <tr class="cart_item">
                    <td class="cart-product-price" colspan="6"><?php echo "No Stoppage Found!"; ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
    <div class="col_full page_header_div">

        <h4 class="heading-custom page_header_h4">Maternity Leave List</h4>

    </div>

    <?php
    $date = date("Y-m-d");
    $sql = "SELECT * FROM tbl_commonfleets where present_route='$model->route_no' AND mt_leave_from<='$date' AND mt_leave_to>='$date' order by mt_leave_to";
    $sql_rs = Yii::app()->db->createCommand($sql)->queryAll();
    //var_dump($sql_rs);

    ?>
    <div class="table-responsive bottommargin">
        <table class="table cart">
            <?php if (count($sql_rs) > 0){ ?>
            <thead>
            <tr class="cart_item">
                <th class="cart-product-price">SI.</th>
                <th class="cart-product-price">User Pin</th>
                <th class="cart-product-price">User Name</th>
                <th class="cart-product-price">User Depart.</th>
                <th class="cart-product-price">User Level</th>
                <th class="cart-product-price">Designation</th>
                <th class="cart-product-price">Cell</th>
                <th class="cart-product-price">From</th>
                <th class="cart-product-price">To</th>

            </tr>
            </thead>
            <tbody>
            <?php $i = 1;
            foreach ($sql_rs as $key => $value) { ?>
                <tr>
                    <td class="cart-product-price"><?php echo $i++; ?></td>
                    <td class="cart-product-price"><?php echo $value['user_pin']; ?></td>
                    <td class="cart-product-price"><?php echo $value['user_name']; ?></td>
                    <td class="cart-product-price"><?php echo $value['user_dept']; ?></td>
                    <td class="cart-product-price"><?php echo $value['user_level']; ?></td>
                    <td class="cart-product-price"><?php echo $value['user_designation']; ?></td>
                    <td class="cart-product-price"><?php echo $value['user_cell']; ?></td>
                    <td class="cart-product-price"><?php echo $value['mt_leave_from']; ?></td>
                    <td class="cart-product-price"><?php echo $value['mt_leave_to']; ?></td>

                </tr>
            <?php }
            }
            else { ?>
                <tr>
                    <td class="cart-product-price" colspan="9"><?php echo "No Maternity Leave Found!"; ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </div>
</div>