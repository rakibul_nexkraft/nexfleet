<?php
/* @var $this RoutesController */
/* @var $model Routes */

$this->breadcrumbs=array(
    'Routes'=>array('index'),
    $model->id=>array('view','id'=>$model->id),
    'Update',
);


?>
<div id="route-request-list">
    <!-- <div class="center s002 ">        
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">
                
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
              /*  $this->widget('zii.widgets.CMenu', array(
                    'items'=>array(
                        array('label'=>'List Zone', 'url'=>array('/routes/index')),
                        array('label'=>'Create Zone', 'url'=>array('/routes/create')),

                    ),
                ));*/

                ?>
            </div>
        </div>
    </div> -->
    <div class="col_full page_header_div" style="border-bottom: 1px solid #f2ecec;">
        <h4 class="heading-custom page_header_h4">Update Route ID# <?php echo $model->id;?></h4>
    </div>


<!--<h4>Update Routes : <?php /*echo $model->id; */?></h4>

<h4 style="float: right;">Update Routes : <?php /*echo $model->id; */?></h4>-->


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>