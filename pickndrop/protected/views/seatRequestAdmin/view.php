<?php
/* @var $this SeatRequestAdminController */
/* @var $model SeatRequestAdmin */

$this->breadcrumbs = array(
    'Seat Request Admins' => array('index'),
    $model->id,
);

//$this->menu=array(
//array('label'=>'List Seat Request Admin', 'url'=>array('index')),
//array('label'=>'Create Seat Request Admin', 'url'=>array('create')),
//array('label'=>'Update Seat Request Admin', 'url'=>array('update', 'id'=>$model->id)),
//array('label'=>'Delete Seat Request Admin', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
<div id="route-request-list">
   <!--  <div class="center s002">
       
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                 class="floating-menu dropdown-toggle animated fadeInRight">
                <
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Seat Request List', 'url' => array('/seatRequestAdmin/index')),
                        array('label' => 'Update', 'url' => array('/seatRequestAdmin/update', 'id' => $model->id)),
                        array('label' => 'New Seat Request', 'url' => array('/seatRequestAdmin/create')),
                    ),
                ));

                ?>
            </div>
        </div>
    </div> -->
    <div class="col_full page_header_div heading-label-responsive">
        <h4 class="heading-custom page_header_h4">View Request List ID#<?php echo $model->id; ?></h4>
    </div>

    <div class="s002 admin-dashboard-table ">
        <div class="table-responsive table-responsive2 bottommargin col_full ">
            <?php
            $this->widget('zii.widgets.XDetailView', array(
                'htmlOptions' => array('class' => 'table cart'),
                'data' => $model,
                'attributes' => array(
                    'group2' => array(
                        'ItemColumns' => 2,
                        'attributes' => array(
                            'id',
                            'user_pin',
                            'user_name',
                            array(
                                'label' => 'Photo',
                                'type' => 'raw',
                                'value' => CHtml::image(SeatRequest::model()->photo($model->user_pin), "USER IMAGE", array('width' => '29px')),

                            ),
                            'user_department',
                            'user_level',
                            'user_cell',
                            'email',
                            'route_id',
                            //array('label'=>'Route Detail',
                            //'type'=>'raw',
                            //'value'=>$model->routeDetail($model->route_id),
                            //),
                            array('label' => 'Stoppage',
                                'type' => 'raw',
                                'value' => SeatRequest::model()->stoppageDetail($model->stoppage_id),
                            ),
                            array('label' => 'Pickup Time',
                                'type' => 'raw',
                                'value' => SeatRequest::model()->stoppagePickupTime($model->stoppage_id),
                            ),
                            'expected_date',
                            'remarks',
                            array('name' => 'status',
                                'type' => 'raw',
                                'value' => $model->status($model->status),
                            ),
                            'created_by',
                            'created_time',
                            'updated_by',
                            'updated_time',
                        ),
                    ),
                ),
            )); ?>
        </div>
    </div>
