<?php
/* @var $this SeatRequestAdminController */
/* @var $model SeatRequestAdmin */
/* @var $form CActiveForm */
?>
<div class="s002 update-form-background">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'seat-request-admin-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php /*echo $form->errorSummary($model); */
	?>
	<div class="inner-form route-form">
	    <div class="col_one_third route-form-box">
            <div>
                <?php echo $form->labelEx($model,'user_pin'); ?>
                <div class="input-field first-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php //echo $form->textField($model,'user_pin',array('size'=>60,'maxlength'=>128));
                    echo $form->textField($model, 'user_pin');
                    /* array('onblur'=>CHtml::ajax(array('type'=>'GET',
                              'dataType'=>'json',


                         'url'=>array("CallHrUser"),

                     'success'=>"js:function(string){
                                     $('#SeatRequestAdmin_user_name').val(string.StaffName);
                                     $('#SeatRequestAdmin_user_level').val(string.JobLevel);
                                     //var myarr = string.ProjectName;
                                     //var myvar = myarr.replace(' Department','');

                                     $('#SeatRequestAdmin_user_department').val(string.ProjectName);
                                     $('#SeatRequestAdmin_user_cell').val(string.MobileNo);
                                     $('#SeatRequestAdmin_email').val(string.EmailID);



                                 }"

                         ))),
                         array('empty' => 'Select one of the following...')

                     );*/

                    ?>
                </div>
                <?php echo $form->error($model,'user_pin'); ?>
            </div>
	 </div>
	 <div class="col_one_third route-form-box">
        <div>
            <?php echo $form->labelEx($model,'user_name'); ?>
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>128)); ?>
            </div>
            <?php echo $form->error($model,'user_name'); ?>
        </div>
         
        </div>
       <div class="col_one_third route-form-box">

            <div>
                <?php echo $form->labelEx($model,'user_department'); ?>
                <div class="input-field first-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model,'user_department',array('size'=>60,'maxlength'=>128)); ?>
                </div>
                <?php echo $form->error($model,'user_department'); ?>

            </div>
        </div>		
	</div>
	<div class="inner-form route-form">
	    <div class="col_one_third route-form-box">

            <div>
                <?php echo $form->labelEx($model,'user_level'); ?>

                <div class="input-field first-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model,'user_level'); ?>

                </div>
                <?php echo $form->error($model,'user_level'); ?>
            </div>
	 	</div>
		<div class="col_one_third route-form-box">

            <div>
                <?php echo $form->labelEx($model,'user_cell'); ?>

                <div class="input-field first-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model,'user_cell',array('size'=>60,'maxlength'=>128)); ?>
                </div>
                <?php echo $form->error($model,'user_cell'); ?>

            </div>
        </div>
       <div class="col_one_third route-form-box">

            <div>
                <?php echo $form->labelEx($model,'email'); ?>


                <div class="input-field first-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128)); ?>
                </div>
                <?php echo $form->error($model,'email'); ?>

            </div>
        </div>		
	</div>
	<div class="inner-form route-form">
	    <div class="col_one_third route-form-box">

            <div>
                <?php echo $form->labelEx($model,'zone'); ?>
                <div class="input-field fouth-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php    $zone_list = Zone::model()->findAll(array('order' => 'name ASC'));


                    ?>
                    <select data-trigger="" name="SeatRequestAdmin[zone]" onchange='zoneRoute(this.value)' id='zone'>
                        <option value=''>A Zone Select </option>
                        <?php foreach($zone_list as $key=>$value){
                            if(isset($model->zone)&&!empty($model->zone)&&$model->zone==$value['id']){
                                echo "<option value='".$value['id']."' selected>".$value['name']."</option>";
                            }
                            else{
                                echo "<option value='".$value['id']."'>".$value['name']."</option>";
                            }
                        }
                        ?>
                    </select>
                </div>
                <?php echo $form->error($model,'zone'); ?>
            </div>
	 </div>
		<div class="col_one_third route-form-box">

            <div>
                <?php echo $form->labelEx($model,'route_id'); ?>


                <div class="input-field fouth-wrap">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="SeatRequestAdmin[route_id]" onchange='routeStoppage(this.value)' id='route-list'>

                        <?php
                        $k=Routes::model()->findAll();
                        foreach ($k as $key => $value) {

                            echo "<option value=".$value['id']." selected>".$value['route_no']."</option>";
                        }

                        if(!$model->isNewRecord){
                            $k=Routes::model()->findByPk($model->route_id);
                            echo "<option value=".$k['id']." selected>".$k['route_no']."</option>";
                        }
                        else{

                            echo  "<option value='' selected>A Route Select </option>";
                        }
                        ?>
                    </select>
                </div>
                <?php echo $form->error($model,'route_id'); ?>
            </div>
	 </div>       
        
       <div class="col_one_third route-form-box">
           <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model,'stoppage_id'); ?></div>
            
			<div class="input-field fouth-wrap col_two_third">
				<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
                        </path>
                    </svg>
                </div>
                <select data-trigger="" name="SeatRequestAdmin[stoppage_id]" onchange='routeStoppage(this.value)' id='stoppage'>
                    
                   <?php 
                    $k1=Stoppage::model()->findAll();
                    foreach ($k1 as $key => $value) {                                     
                            echo "<option value=".$value['id']." selected>".$value['stoppage']."</option>";
                    }
                   if(!$model->isNewRecord){
                		$k1=Stoppage::model()->findByPk($model->stoppage_id);                     
                        echo "<option value=".$k1['id']." selected>".$k1['stoppage']."</option>";
                    }  
                    else{
                       echo "<option value='' selected>A Stoppage Select </option>"; 
                    }              
                    ?>
                </select>
               
				<?php echo $form->error($model,'stoppage_id'); ?>
                    
               
               
	 	</div>
	 </div>
	</div>
	<div class="inner-form route-form">
	    <div class="col_one_third route-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model,'expected_date'); ?></div>
		
			<div class="input-field first-wrap col_two_third">
		 		 <div class="icon-wrap ">
				    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
					    <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">				    	
					    </path>
	                </svg>
				</div>
                <?php echo $form->textField($model,'expected_date',array('placeholder'=>'Expected Date','id'=>'depart2','class'=>'datepicker')); ?>        		
				  <?php echo $form->error($model,'expected_date'); ?>
			</div>
	 	</div>
		<div class="col_one_third route-form-box">
            <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model,'remarks'); ?></div>
		
            <div class="input-field first-wrap col_two_third">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">                       
                        </path>
                    </svg>
                </div>      
                 <?php echo $form->textField($model,'remarks',array('size'=>60,'maxlength'=>256)); ?>
				<?php echo $form->error($model,'remarks'); ?>
            </div>
         
        </div>
       <div class="col_one_third route-form-box">
           <div class="seat-request-label col_one_third"><?php echo $form->labelEx($model,'action'); ?></div>
		
		
            <div class="input-field fouth-wrap col_two_third">
				<div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">                        
                        </path>
                    </svg>
                </div>
                <select data-trigger="" name="SeatRequestAdmin[status]"  >
                    <option value='0' <?php if($model->action==0) echo "selected"?>>Pending</option>
                    <option value='1' <?php if($model->action==1) echo "selected"?>>Approved</option>
                    <!-- <option value='3' <?php if($model->status==3) echo "selected"?>>Request For Cancel</option> -->
                    <option value='-1' <?php if($model->action==-1) echo "selected"?>>Cancel</option>
                   
                </select>
               
				<?php echo $form->error($model,'status'); ?>
                    
               
               
	 		</div>
	 	</div>
    </div>		
	<div class="inner-form route-form">
     <div class="col_full">
            <div class="input-field first-wrap form-save-button">
                <?php  echo CHtml::submitButton("Save",array('class'=>'btn-search button-pink','id'=>'search-button')); ?>
            </div>
         
        </div>
    </div>	
<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
	function zoneRoute(){
		var z=$("#zone").val();		
		$.post("<?php echo $this->createUrl('SeatRequestAdmin/zone');?>", {id: z}, function(result){
			var data=JSON.parse(result);    
			
			if (!data.hasOwnProperty('error')) {
				var listItem="<option value=''>Select Route</option>";
				for (i = 0; i < data.length; i++) {
                    //listItem+="<option value='"+data[i].id+"'>"+data[i].route_no+"-"+data[i].route_detail+"</option>";
                    listItem+="<option value='"+data[i].id+"'>"+data[i].route_no+"</option>";
                    }
                   // $('#route-list').html(listItem);
				

			}
			else{
			var listItem="<option value=''></option>";
			//$('#route-list').html(listItem);
			}
			const choices = new Choices('#route-list',
			    {	removeItemButton: true,   }).setChoices([
        							{ value: 'Four', label: 'Label Four', disabled: true },
        							{ value: 'Five', label: 'Label Five' },
        							{ value: 'Six', label: 'Label Six', selected: true },
      								], 'value', 'label', false);
			  });
	}
function routeStoppage(r){
	
		$.post("<?php echo $this->createUrl('SeatRequestAdmin/route');?>", {id:r}, function(result){
			
			var data=JSON.parse(result);    

			if (!data.hasOwnProperty('error')) {
				var listItem="<option value=''>Select Stoppage</option>";
				for (i = 0; i < data.length; i++) {
                    //listItem+="<option value='"+data[i].id+"'>"+data[i].route_no+"-"+data[i].route_detail+"</option>";
                    listItem+="<option value='"+data[i].id+"'>"+data[i].stoppage+"</option>";
                    }
                    $('#SeatRequestAdmin_stoppage_id').html(listItem);
				

			}
			else{
			var listItem="<option value=''></option>";
			$('#SeatRequestAdmin_stoppage_id').html(listItem);
			}
  });
	}
</script>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker",{});</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
    {
        searchEnabled: true,
        itemSelectText: '',
        
    });

</script>