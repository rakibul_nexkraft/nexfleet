<style type="text/css">
    .status {
        width: 100px;
    }
</style>
<?php
/* @var $this SeatRequestAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Seat Request Admin',
);

//$this->menu=array(
//array('label'=>'Create Seat Request Admin', 'url'=>array('create')),
//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
<div id="route-request-list" class="seat-request-admin search-box-responsive">

    <div class="center s002 searchbar update-form-background2" style="padding: 0 20px;">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'enableAjaxValidation' => true,
            'method' => 'get',
        )); ?>

        <div class="inner-form seat-request-form">
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">Route No</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="SeatRequestAdmin[route_id]">

                        <?php
                        echo "<option value='' selected>A Route Select </option>";
                        $k = Routes::model()->findAll();
                        foreach ($k as $key => $value) {
                            if (isset($_GET['SeatRequestAdmin']['route_id']) && $_GET['SeatRequestAdmin']['route_id'] == $value['id']) {
                                echo "<option value=" . $value['id'] . " selected>" . $value['route_no'] . "</option>";
                            } else {
                                echo "<option value=" . $value['id'] . ">" . $value['route_no'] . "</option>";
                            }
                        }

                        ?>
                    </select>


                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">User Pin</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'user_pin', array('size' => 60, 'maxlength' => 128)); ?>

                </div>

            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">User Name</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'user_name', array('size' => 60, 'maxlength' => 128)); ?>

                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box col_last'>
                <div class="seat-request-label">Level</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'user_level', array('size' => 60, 'maxlength' => 128)); ?>

                </div>
            </div>
        </div>

        <div class="inner-form seat-request-form">
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">Department</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'user_department', array('size' => 60, 'maxlength' => 128)); ?>

                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">Expected Date</div>
                <div class="input-field first-wrap seat-request-input-box">
                    <div class="icon-wrap ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'expected_date', array('placeholder' => 'Expected Date', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div>
              <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">User Status</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="SeatRequestAdmin[user_status]">
                        <option value='' selected>Select Status</option>
                        <option value='0' <?php echo (isset($_GET['SeatRequestAdmin']['user_status']) &&  $_GET['SeatRequestAdmin']['user_status'] === '0') ? "selected" : ""; ?>>
                            Seat Request
                        </option>
                        <option value='1' <?php echo (isset($_GET['SeatRequestAdmin']['user_status']) && $_GET['SeatRequestAdmin']['user_status'] == 1) ? "selected" : ""; ?>>
                            Route Change Request
                        </option>
                        <option value='2' <?php echo (isset($_GET['SeatRequestAdmin']['user_status']) && $_GET['SeatRequestAdmin']['user_status'] == 2) ? "selected" : ""; ?>>
                            Cancel Request
                        </option>
                        
                    </select>
                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box  col_last'>
                <div class="seat-request-label">Fleet Status</div>
                <div class="input-field fouth-wrap seat-request-input-box">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="SeatRequestAdmin[status]">
                        <option value='' selected>Select Status</option>
                        <option value='0' <?php echo (isset($_GET['SeatRequestAdmin']['status']) && $_GET['SeatRequestAdmin']['status'] === '0') ? "selected" : ""; ?>>
                            Pending
                        </option>
                        <option value='1' <?php echo (isset($_GET['SeatRequestAdmin']['status']) && $_GET['SeatRequestAdmin']['status'] == 1) ? "selected" : ""; ?>>
                            Approved
                        </option>
                        <option value='-1' <?php echo (isset($_GET['SeatRequestAdmin']['status']) && $_GET['SeatRequestAdmin']['status'] == -1) ? "selected" : ""; ?>>
                            Cancel
                        </option>
                        <!-- <option value='3' <?php echo (isset($_GET['SeatRequestAdmin']['status']) && $_GET['SeatRequestAdmin']['status'] == 3) ? "selected" : ""; ?>>
                            Cancel Request
                        </option>
                        <option value='4' <?php echo (isset($_GET['SeatRequestAdmin']['status']) && $_GET['SeatRequestAdmin']['status'] == 4) ? "selected" : ""; ?>>
                            Route Change Request
                        </option> -->
                    </select>


                </div>
            </div>
        </div>
        <div class="inner-form seat-request-form" style="justify-content: flex-end;">
           
                <?php
                if (isset($_GET['SeatRequestAdmin']['title']) && !empty($_GET['SeatRequestAdmin']['title'])) {
                    echo "<input type='hidden' name='SeatRequestAdmin[title]' value='" . $_GET['SeatRequestAdmin']['title'] . "'>";
                }
                if (isset($_GET['SeatRequestAdmin']['previous']) && !empty($_GET['SeatRequestAdmin']['previous'])) {
                    echo "<input type='hidden' name='SeatRequestAdmin[previous]' value='" . $_GET['SeatRequestAdmin']['previous'] . "'>";
                }
                ?>
                <div class='seat-request-form-box'>
                    <div class='input-field first-wrap searchbar-search-button'>
                        <?php echo CHtml::submitButton("Search", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
                    </div>
                </div>
           
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">
        <?php
        if (isset($_GET['SeatRequestAdmin']['title']) && !empty($_GET['SeatRequestAdmin']['title'])) {
            echo '<h4 class="heading-custom">' . $_GET['SeatRequestAdmin']['title'] . '</h4>';
        } else {
            ?>

            <h4 class="heading-custom page_header_h4">Request List</h4>
        <?php } ?>

    </div>
    <?php
    if (isset($_GET['previous']) && !empty($_GET['previous'])) {
        $previous = $_GET['previous'];
    } else {
        $previous = null;
    }

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'seat-request-admin-grid',
        'itemsCssClass' => 'table cart',
        'htmlOptions' => array('class' => 'table-responsive table-responsive2 bottommargin table-overflow-x admin-seat-request-table-responsive border-round-bottom'),

        'rowCssClass' => array('cart_item'),
        'dataProvider' => $model->search(),
        //'filter' => $model,
        'columns' => array(

            //'id',

            array(
                'name' => 'id',
                'type' => 'raw',
                //'value'=>'$data->id',
                'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'user_pin',
                'type' => 'raw',
                'value' => '$data->user_pin',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
          /*  array(
                'header' => 'Photo',
                'type' => 'raw',
                'value' => 'CHtml::image(SeatRequest::model()->photo($data->user_pin))',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/
            array(
                'name' => 'user_name',
                'type' => 'raw',
                'value' => '$data->user_name',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'user_department',
                'type' => 'raw',
                'value' => '$data->user_department',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'user_level',
                'type' => 'raw',
                'value' => '$data->user_level',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),

          /*  array(
                'name' => 'user_cell',
                'type' => 'raw',
                'value' => '$data->user_cell',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'email',
                'type' => 'raw',
                'value' => '$data->email',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/

            array(
                'name' => 'route_id',
                'type' => 'raw',
                'value' => 'SeatRequest::routeDetail($data->route_id,3)',
                'filter' => Stoppage::model()->getAllRouteNo(),
                'htmlOptions' => array('class' => 'cart-product-price', 'title' => 'Route Detail'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'header' => 'Available Seat',
                'type' => 'raw',
                'value' => 'SeatRequest::availableSeat($data->route_id)',
                'filter' => Stoppage::model()->getAllRouteNo(),
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'expected_date',
                'type' => 'raw',
                'value' => '$data->expected_date',

                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(  
                    'header'=>'Vehicle Type', 
                    'type' => 'raw',            
                    'value'=>'SeatRequest::routeDetail($data->route_id,4)',                                           
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
            array(  
                    'header'=>'Bill Amount', 
                    'type' => 'raw',            
                    'value'=>'SeatRequest::routeDetail($data->route_id,5,$data->user_level)',                                           
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),


            //'remarks',

            //'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',
           /* array(
                'name' => 'queue',
                'type' => 'raw',
                'value' => '$data->queueCheck($data->queue,$data->status)',
                'filter' => array('-1' => 'Assigned', '0' => 'Not Approved', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/
             'name',
            'phone',            
            'reg_no',
            array(
                'name'=>'user_status',
                'type' => 'raw',
                'value'=>'SeatRequest::userStatus($data->user_status)',                        
                'htmlOptions'=>array('class'=>'cart-product-price'),
                'headerHtmlOptions'=>array('class'=>'cart-product-price'),
            ),
            array(
                'name' => 'status',
                'type' => 'raw',
                'value' => '$data->status == 0 ? "Pending" : ($data->status == 1 ? "Approved" : "Cancel")',                
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'action',
                'type' => 'raw',
                'value' => 'CHtml::dropDownList("$model","$data->action",array("0"=>"Pending","1"=>"Approved","-1"=>"Cancel"),$htmlOptions=array("onChange"=>"status($data->id,$(this).val(),$data->status)","class"=>"status"))',                
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
           
           /* array('name' => 'maternity_leave',
                'type' => 'raw',
                'value' => '($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
                'filter' => array('0' => 'NO', '1' => 'YES'),
                'htmlOptions' => array('class' => 'cart-product-price', 'title' => 'Maternity Seat Use'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/


           /* array(
                'class' => 'CButtonColumn',
                'template' => '{view}{update}',


            ),*/
        ),
    )); ?>
</div>
<div class="s002 export_to_excel_button export_to_excel_button_rout export-to-excel-button-responsive button-pink">
    <?php

    echo CHtml::normalizeUrl(CHtml::link('Export to Excel', array('excel', 'criteria' => $_GET['SeatRequestAdmin'])));
    ?>
</div>
<script>
    function status(id, value, preValue) {

        var message = "Do you want to change seat request status for seat request ID# " + id + "?";

        var con = confirm(message);
        if (con === true) {
            showDataProcess(true);
            jQuery.post('<?php echo $this->createUrl('statusChange');?>', {
                id: id,
                value: value,
                preValue: preValue
            }, function (data) {
                showDataProcess(false);
                //var size_to_value=JSON.parse(data);
                //alert(data);
                //openModel(data);
                bsModalOpen(data);
            });

        }
        else {
            //$(this).val()=preValue;
            //alert(preValue);
            ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }


    }
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {});</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',

        });

</script>