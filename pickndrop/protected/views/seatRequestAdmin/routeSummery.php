<?php 

$rs_to_day_new_seat_request['request'];
$rs_previous_new_seat_request['request'];

$rs_to_day_new_cancel_seat_request['request'];
$rs_previous_new_cancel_seat_request['request'];

$rs_to_day_cancel_seat['request'];
$rs_previous_cancel_seat['request'];

$rs_to_day_change_seat_request['request'];
$rs_previous_change_seat_request['request'];

$rs_to_day_approved['request'];
$rs_previous_approved['request'];


$rs_to_day_maternity_request['request'];
$rs_previous_maternity_request['request'];

$rs_to_day_maternity_approved['request'];
$rs_previous_maternity_approved['request'];

?>
<div id="route-list">
   	<div class="center s002 ">  

   		<h4 class="heading-custom">ROUTES <?php echo $route_no;?> SUMMERY</h4>
   	
   	</div>
  
    <div class="table-responsive bottommargin">
    					<table class="table cart">
							<thead>
								<tr>
									<th class="cart-product-price" colspan="2">Seat Reuest Pending</th>
									<th class="cart-product-price" colspan="2">Seat Reuest Approved</th>
									<th class="cart-product-price" colspan="2">Route  Change Reuest</th>
									<th class="cart-product-price" colspan="2">Seat  Cancel</th>
									<th class="cart-product-price" colspan="2">Route  Cancel Request</th>
									<th class="cart-product-price" colspan="2">Maternity Leave Request</th>	
									<th class="cart-product-price" colspan="2">Maternity Leave Approved</th>	
								</tr>
							</thead>
							<tbody>								
								<tr class="cart_item">
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>		
									<td  class="cart-product-price">Today</td>
									<td  class="cart-product-price">Previous</td>	
								</tr>
								<tr class="cart_item">
									<td  class="cart-product-price">
										<?php echo CHtml::link($rs_to_day_new_seat_request['request'],array("/seatRequestAdmin/index",'SeatRequestAdmin[status]'=>0,'SeatRequestAdmin[route_id]'=>$id,"SeatRequestAdmin[previous]"=>2,'SeatRequestAdmin[created_time]'=>date('Y-m-d'),'SeatRequestAdmin[title]'=>'Today Seat Request Pending')); 
										/*$form=$this->beginWidget('CActiveForm', array(
										    'action'=>Yii::app()->createUrl('/seatRequestAdmin/index'),
										    'enableAjaxValidation'=>true,
										    'method'=>'get',
										));?> 
										<input  name="SeatRequestAdmin[status]"  type="hidden" value="<?php echo $rs_to_day_new_seat_request['request'];?>">
										<input  name="SeatRequestAdmin[created_time]"  type="hidden" value="<?php echo date("Y-m-d");?>">
										
										<?php echo CHtml::submitButton($rs_to_day_new_seat_request['request'],array('class'=>'','id'=>''));

										 $this->endWidget();*/ ?>
											
										</td>
									<td  class="cart-product-price"><?php 
										 echo CHtml::link($rs_previous_new_seat_request['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>0,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>1,'SeatRequestAdmin[title]'=>'Previous Seat Request Pending')); 

									//echo $rs_previous_new_seat_request['request'];?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_to_day_approved['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>1,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>4,'SeatRequestAdmin[title]'=>'Today Seat Request Approved'));

									?></td>
									<td  class="cart-product-price"><?php 
									echo CHtml::link($rs_previous_approved['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>1,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>3,'SeatRequestAdmin[title]'=>'Previous Seat Request Approved'));
									?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_to_day_change_seat_request['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>4,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>2,'SeatRequestAdmin[title]'=>'Today  Route Change Request'));
									?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_previous_change_seat_request['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>4,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>1,'SeatRequestAdmin[title]'=>'Previous  Route Change Request'));
									?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_to_day_cancel_seat['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>-1,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>4,'SeatRequestAdmin[title]'=>'Today Cancel Seat'));

									?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_previous_cancel_seat['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>-1,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>3,'SeatRequestAdmin[title]'=>'Previous Cancel Seat'));

									?>
										

									</td>
									<td  class="cart-product-price"><?php
									echo CHtml::link($rs_to_day_new_cancel_seat_request['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>3,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>4,'SeatRequestAdmin[title]'=>'Today Cancel Seat Request'));
										?></td>
									<td  class="cart-product-price"><?php 							
									echo CHtml::link($rs_previous_new_cancel_seat_request['request'],array("/seatRequestAdmin/index","SeatRequestAdmin[status]"=>3,"SeatRequestAdmin[route_id]"=>$id,"SeatRequestAdmin[previous]"=>3,'SeatRequestAdmin[title]'=>'Previous Cancel Seat Request'));
									?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_to_day_maternity_request['request'],array("/maternityLeaveRequestAdmin/index","MaternityLeaveRequestAdmin[status]"=>0,"MaternityLeaveRequestAdmin[route]"=>$route_no,"MaternityLeaveRequestAdmin[previous]"=>2,'MaternityLeaveRequestAdmin[title]'=>'Today Maternity Leve Request'));

									?></td>
									<td  class="cart-product-price"><?php 
										echo CHtml::link($rs_previous_maternity_request['request'],array("/maternityLeaveRequestAdmin/index","MaternityLeaveRequestAdmin[status]"=>0,"MaternityLeaveRequestAdmin[route]"=>$route_no,"MaternityLeaveRequestAdmin[previous]"=>1,'MaternityLeaveRequestAdmin[title]'=>'Previous Maternity Leve Request'));
										?>

									</td>
									<td  class="cart-product-price"><?php 
									echo CHtml::link($rs_to_day_maternity_approved['request'],array("/maternityLeaveRequestAdmin/index","MaternityLeaveRequestAdmin[status]"=>1,"MaternityLeaveRequestAdmin[route]"=>$route_no,"MaternityLeaveRequestAdmin[previous]"=>4,'MaternityLeaveRequestAdmin[title]'=>'Today Maternity Leve Approved'));
										

									?></td>
									<td  class="cart-product-price"><?php 
									echo CHtml::link($rs_previous_maternity_approved['request'],array("/maternityLeaveRequestAdmin/index","MaternityLeaveRequestAdmin[status]"=>1,"MaternityLeaveRequestAdmin[route]"=>$route_no,"MaternityLeaveRequestAdmin[previous]"=>3,'MaternityLeaveRequestAdmin[title]'=>'Previous Maternity Leve Approved'));

									?></td>
										
								</tr>
													
								
							</tbody>

						</table>

					</div>
			
		</div>
