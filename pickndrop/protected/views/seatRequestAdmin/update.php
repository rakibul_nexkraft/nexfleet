<?php
/* @var $this SeatRequestAdminController */
/* @var $model SeatRequestAdmin */

$this->breadcrumbs = array(
    'Seat Request Admins' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

//$this->menu=array(
//array('label'=>'List Seat Request Admin', 'url'=>array('index')),
//array('label'=>'Create Seat Request Admin', 'url'=>array('create')),
//array('label'=>'View Seat Request Admin', 'url'=>array('view', 'id'=>$model->id)),
//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
   <!--  <div class="center s002 ">

       
        <div class="dropdown">
            <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                 class="floating-menu dropdown-toggle animated fadeInRight">
               
                <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
            </div>
            <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        array('label' => 'Seat Request List', 'url' => array('/seatRequestAdmin/index')),
                        array('label' => 'View', 'url' => array('/seatRequestAdmin/view', 'id' => $model->id)),
                        array('label' => 'New Seat Request', 'url' => array('/seatRequestAdmin/create')),
                    ),
                ));

                ?>
            </div>
        </div>
    </div> -->
    <div class="col_full page_header_div" style="border-bottom: 1px solid #f2ecec;">
        <h4 class="heading-custom page_header_h4">Update Seat Request <?php echo $model->id; ?></h4>
    </div>


<?php echo $this->renderPartial('_form', array('model' => $model)); ?>