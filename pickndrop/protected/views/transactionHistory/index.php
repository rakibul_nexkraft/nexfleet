<?php
/* @var $this TransactionHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Transaction Histories',
);

$this->menu=array(
	array('label'=>'Create TransactionHistory', 'url'=>array('create')),
	array('label'=>'Manage TransactionHistory', 'url'=>array('admin')),
);
?>

<h1>Transaction Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
