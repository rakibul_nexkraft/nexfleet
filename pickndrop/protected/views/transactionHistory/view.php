<?php
/* @var $this TransactionHistoryController */
/* @var $model TransactionHistory */

$this->breadcrumbs=array(
	'Transaction Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TransactionHistory', 'url'=>array('index')),
	array('label'=>'Create TransactionHistory', 'url'=>array('create')),
	array('label'=>'Update TransactionHistory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TransactionHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TransactionHistory', 'url'=>array('admin')),
);
?>

<h1>View TransactionHistory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_pin',
		'sale_id',
		'booking_id',
		'credit',
		'debit',
		'payment_method',
		'transaction_id',
		'balance',
		'created_by',
		'created_time',
		'updated_by',
		'updated_time',
	),
)); ?>
