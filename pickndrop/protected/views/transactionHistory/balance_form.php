
<div class="s002 searchbar">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'id'=>'transaction-history-form',
            'enableAjaxValidation'=>false,
            'htmlOptions' => array('onsubmit' => 'return false'),
        )); ?>  
        <p class="note">Fields with <span class="required">*</span> are required.</p>

 <div class="inner-form seat-request-form">
        <div class="col_full">
            <div class="seat-request-label">Payment Method*</div>
            <div class="input-field first-wrap seat-request-input-box bkash-input">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'payment_method', array('placeholder' => 'bKash','value'=>'bKash','readonly'=>'readonly')); ?>               
            </div>
             <?php echo $form->error($model, 'payment_method'); ?>
        </div>
        <div class="col_full">
            <div class="seat-request-label">Transaction ID*</div>
            <div class="input-field fouth-wrap seat-request-input-box transaction-input">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                        </path>
                    </svg>
                </div>
                <?php echo $form->textField($model, 'transaction_id', array('placeholder' => 'Transaction ID')); ?>                    
            </div>
            <?php echo $form->error($model, 'transaction_id'); ?>
        </div>
        <div class="col_full">
            <div class="seat-request-label">Amount*</div>
            <div class="input-field first-wrap seat-request-input-box credit-input">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                        </path>
                    </svg>
                </div>
               <?php echo $form->textField($model, 'credit', array('placeholder' => 'Amount')); ?>              
            </div>
            <?php echo $form->error($model, 'credit'); ?>
        </div>
        <div class='seat-request-form-box'>
            <div class='input-field first-wrap searchbar-search-button'>
                    <?php echo CHtml::submitButton("Save", array('class' => 'btn-search button-pink', 'id' => 'search-button','onclick'=>'submitTransaction()')); ?>
            </div>
        </div>        
    </div>

   
<?php $this->endWidget(); ?>
</div> 
    <script>
    function submitTransaction(){
        $("#transaction-history-form").ajaxSubmit({
            url: '<?php echo Yii::app()->createAbsoluteUrl("/transactionHistory/createData");?>', 
            type: 'post', 
            success:  function(data) { 
                if(data==1) {
                    //openModel("Data Save Successfully!");
                    bsModalOpen("Your transaction is save successfully!");
                }
                else {
                    $("#transactionHistory-div").html(data);
                }         
            }
        });
    }
</script>

