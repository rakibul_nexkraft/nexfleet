<?php
/* @var $this TransactionHistoryController */
/* @var $model TransactionHistory */

$this->breadcrumbs=array(
	'Transaction Histories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TransactionHistory', 'url'=>array('index')),
	array('label'=>'Create TransactionHistory', 'url'=>array('create')),
	array('label'=>'View TransactionHistory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TransactionHistory', 'url'=>array('admin')),
);
?>

<h1>Update TransactionHistory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>