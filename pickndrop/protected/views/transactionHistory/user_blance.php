<style type="text/css">
    .status {
        width: 100px;
    }   
.credit-input {
    width: 150px;
}
.bkash-input{
    width: 150px;
}
.transaction-input{
    width: 150px;
}
</style>
<?php
/* @var $this SeatRequestAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Transaction Histories',
);
?>
<?php if (Yii::app()->user->name!='Guest' && isset(Yii::app()->user->picmarket_user) && Yii::app()->user->picmarket_user==1) { ?>
<div class="toggle toggle-bg toggle-section-responsive">
	<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Add Balance
	</div>
	<div class="togglec" style="display: none;">
		<div id="transactionHistory-div">
		<?php		
			$this->renderPartial('/transactionHistory/balance_form',array(				
				'model'=>$model,				
			));		
		?>		
		</div>
	</div>
</div>
<?php }  ?>

<div id="route-request-list" class="seat-request-admin search-box-responsive">  
	 <div class="center s002 searchbar update-form-background2" style="padding: 0 20px;">
        <?php $form = $this->beginWidget('CActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'enableAjaxValidation' => true,
            'method' => 'get',
        )); ?>      

        <div class="inner-form seat-request-form">
        	<?php if(Yii::app()->user->name!='Guest' && isset(Yii::app()->user->picmarket_user) && Yii::app()->user->picmarket_user!=1) { ?>
        		<div class='col_one_fourth seat-request-form-box'>
            	 	<div class="seat-request-label">User</div>
                <div class="input-field first-wrap seat-request-input-box bkash-input">
                    <div class="icon-wrap ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
	                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
	                        </path>
                    	</svg>
                    </div>
                    <?php if(isset($_GET['TransactionHistory']['user_pin'])&& $_GET['TransactionHistory']['user_pin']=="iFleet"){
                    	$model->user_pin = 'iFleet';
                    }?>
                    <?php echo $form->textField($model, 'user_pin', array('placeholder' => 'User Pin')); ?>
                </div>                
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                    <div class="seat-request-label">Transaction Id</div>
                <div class="input-field first-wrap seat-request-input-box bkash-input">
                    <div class="icon-wrap ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>                    
                    <?php echo $form->textField($model, 'transaction_id', array('placeholder' => 'Transaction Id')); ?>
                </div>                
            </div>
        	<?php } ?>
            <div class='col_one_fourth seat-request-form-box'>
            	 <div class="seat-request-label">From Date</div>
                <div class="input-field first-wrap seat-request-input-box bkash-input">
                    <div class="icon-wrap ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'from_date', array('placeholder' => 'From Date', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
                
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label">To Date</div>
                <div class="input-field first-wrap seat-request-input-box bkash-input">
                    <div class="icon-wrap ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'to_date', array('placeholder' => 'To Date', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div> 
              <div class='seat-request-form-box'>
                <div class='input-field first-wrap searchbar-search-button'>
                    <?php echo CHtml::submitButton("Search", array('class' => 'btn-search button-pink', 'id' => 'search-button')); ?>
                </div>
            </div>  
                     
        </div>

     
        <?php $this->endWidget(); ?>
    </div> 

    <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top">
            <h4 class="heading-custom page_header_h4">Your Transaction History</h4>
    </div>
    <?php
   

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'seat-request-admin-grid',
        'itemsCssClass' => 'table cart',
        'htmlOptions' => array('class' => 'table-responsive table-responsive2 bottommargin table-overflow-x admin-seat-request-table-responsive border-round-bottom'),

        'rowCssClass' => array('cart_item'),
        'dataProvider' => $model->search(),
        //'filter' => $model,
        'columns' => array(
            'id',
           /* array(
                'name' => 'id',
                'type' => 'raw',
                //'value'=>'$data->id',
                'value' => 'CHtml::link(CHtml::encode($data->id), array("view", "id"=>$data->id))',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/
            array(
                'name' => 'user_pin',
                'type' => 'raw',
                'value' => '$data->user_pin == "0" ? "iFleet" : $data->user_pin ',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
       
            array(
                'name' => 'sale_id',
                'type' => 'raw',
                'value' => '$data->sale_id==0 ?"No" : "Yes"',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'booking_id',
                'type' => 'raw',
                'value' => '$data->booking_id==0 ?"No" : "Yes"',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'credit',
                'type' => 'raw',
                'value' => '$data->credit',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'debit',
                'type' => 'raw',
                'value' => '$data->debit',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),     

            array(
                'name' => 'payment_method',
                'type' => 'raw',
                'value' => '$data->payment_method',                
                'htmlOptions' => array('class' => 'cart-product-price', 'title' => 'Route Detail'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'transaction_id',
                'type' => 'raw',
                'value' => '$data->transaction_id',                
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'balance',
                'type' => 'raw',
                'value' => '$data->balance',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(  
                    'name'=>'created_time', 
                    'type' => 'raw',            
                    'value'=>'$data->created_time',                                           
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
        ),
    )); ?>
</div>
<div class="s002 export_to_excel_button export_to_excel_button_rout export-to-excel-button-responsive button-pink">
    <?php

    echo CHtml::normalizeUrl(CHtml::link('Export to Excel', array('excel', 'criteria' => $_GET['TransactionHistory'])));
    ?>
</div> 
<script>
    function status(id, value, preValue) {

        var message = "Do you want to change seat request status for seat request ID# " + id + "?";

        var con = confirm(message);
        if (con === true) {
            jQuery.post('<?php echo $this->createUrl('statusChange');?>', {
                id: id,
                value: value,
                preValue: preValue
            }, function (data) {
                //var size_to_value=JSON.parse(data);
                //alert(data);
                //openModel(data);
                bsModalOpen(data);
            });

        }
        else {
            //$(this).val()=preValue;
            //alert(preValue);
            ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }


    }
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {});</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',

        });

</script>