<?php
/* @var $this TransactionHistoryController */
/* @var $model TransactionHistory */

$this->breadcrumbs=array(
	'Transaction Histories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TransactionHistory', 'url'=>array('index')),
	array('label'=>'Manage TransactionHistory', 'url'=>array('admin')),
);
?>

<h1>Create TransactionHistory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>