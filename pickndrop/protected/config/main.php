<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
error_reporting(1);
@ini_set('Display_errors', 1);
//@ini_set('memory_limit', '-1');
Return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'theme'=>'user',
	'name'=>'iFleet Pick and Drop',
    'defaultController' => 'site/index',
    'timeZone' => 'Asia/Dhaka',

	// preloading 'log' component
	'preload'=>array('log','input','bootstrap', 'chartjs'),

	// autoloading model and component classes
	'import'=>array(
    'application.models.*',
	'application.components.*',
    'application.modules.user.models.*',
    'application.modules.user.components.*',
    'application.modules.api.components.*',
	'application.extensions.EchMultiSelect.*',
    'application.extensions.EHttpClient.*',
	'ext.ESelect2.ESelect2',
	'ext.phpexcel.EExcelView',
	//'application.extensions.crontab.*',
	 'ext.yii-mail.YiiMailMessage',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
		 'generatorPaths' => array(
          'bootstrap.gii'
       ),
			'class'=>'system.gii.GiiModule',
			'password'=>'pass123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		), 
		
		 'user'=>array(
            # encrypting method (php hash function)
            //'preload'=>array('bootstrap'),
            /*'components'=>array(
                'bootstrap' => array(
                'class' => 'ext.bootstrap.components.Bootstrap',
                'responsiveCss' => true,
                ),
                'input'=>array(
                'class'         => 'CmsInput',
                'cleanPost'     => true,
                'cleanGet'      => true,
                ),
            ),*/
            'hash' => 'md5',
 
            # send activation email
            'sendActivationMail' => true,
 
            # allow access for non-activated users
            'loginNotActiv' => false,
 
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
 
            # automatically login from registration
            'autoLogin' => true,
 
            # registration path
            'registrationUrl' => array('/user/registration'),
 
            # recovery password path
            'recoveryUrl' => array('/user/recovery'),
 
            # login form path
            'loginUrl' => array('/user/login'),
 
            # page after login
            'returnUrl' => array('/site/index'),
 
            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
        'api',
        /*'pickanddrop'=>array(
        	# encrypting method (php hash function)
            'hash' => 'md5',
 
            # send activation email
            'sendActivationMail' => true,
 
            # allow access for non-activated users
            'loginNotActiv' => false,
 
            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,
 
            # automatically login from registration
            'autoLogin' => true,
 
            # registration path
            'registrationUrl' => array('/pickanddrop/registration'),
 
            # recovery password path
            'recoveryUrl' => array('/pickanddrop/recovery'),
 
            # login form path
            'loginUrl' => array('/pickanddrop/login'),
 
            # page after login
            'returnUrl' => array('/pickanddrop/profile'),
 
            # page after logout
            'returnLogoutUrl' => array('/pickanddrop/login'),
        ),*/
		
	),

	// application components
	'components'=>array(

        'curl' => array(
            'class' => 'ext.curl.Curl',
            //'options' => array(/.. additional curl options ../)
        ),
        'chartjs' => array('class' => 'ext.chartjs.components.ChartJs'),
        'clientScript' => array('scriptMap' => array('jquery.js' => false, )),

/*        'request'=>array(
            'enableCsrfValidation'=>false,
        ),*/
		
        //'bootstrap' => array(
    	  // 'class' => 'ext.bootstrap.components.Bootstrap',
    	    //'responsiveCss' => true,
    	//),

        
		
		
		'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/user/login'),
        ),
        /*'pickanddrop'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
            'allowAutoLogin'=>true,
            'loginUrl' => array('/pickanddrop/login'),
        ),*/

        'mailer' => array(
            'class' => 'application.extensions.mailer.EMailer',
            'pathViews' => 'application.views.email',
            'pathLayouts' => 'application.views.email.layouts'
        ),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
		/*array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
        array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),        
        array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
        array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
        array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
        array('api/driverPin', 'pattern'=>'api/<model:\w+>/<driverpin:\d+>/driver', 'verb'=>'GET'),
        array('api/userPin', 'pattern'=>'api/<model:\w+>/<userpin:\d+>/user', 'verb'=>'GET'),        
        array('api/address', 'pattern'=>'api/<model:\w+>/address', 'verb'=>'POST'),
        array('api/vehicle', 'pattern'=>'api/<model:\w+>/vehicle', 'verb'=>'POST'),
       */
           array('/api/<controller>/view', 'pattern'=>'/api/<controller>/view/<id:\w+>', 'verb'=>'GET'),
           array('/api/<controller>/cancel', 'pattern'=>'/api/<controller>/cancel/<user:\w+>/<id:\d+>', 'verb'=>'GET'),
           array('/api/<controller>/list', 'pattern'=>'/api/<controller>/<user:\w+>/<page:\d+>', 'verb'=>'GET'),
           array('/api/<controller>/routelist', 'pattern'=>'/api/<controller>/routelist', 'verb'=>'GET'),
           array('/api/<controller>/bSRouteinfo', 'pattern'=>'/api/<controller>/<sid:\w+>/selectroure', 'verb'=>'GET'),
            array('/api/<controller>/create', 'pattern'=>'/api/<controller>', 'verb'=>'POST'),
         
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database

        /*'db'=>array(
            'connectionString' => 'mysql:host=35.185.149.147;dbname=ifleetdb',
            'emulatePrepare' => true,
            'username' => 'ifleetdbuser',
            'password' => 'ifleetdb@321',
            'charset' => 'utf8',
            'tablePrefix'=>'tbl_',
            'enableParamLogging'=>true,
        ),*/
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=pickndrop',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '1234',
			'charset' => 'utf8',
			'tablePrefix'=>'tbl_',
			'enableParamLogging'=>true,
		),
            /* 'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=ifleetdb1',
            'emulatePrepare' => true,
            'username' => 'ifleetdb1',
            'password' => 'FleetDb@!234%',
            'charset' => 'utf8',
            'tablePrefix'=>'tbl_',
            'enableParamLogging'=>true,
        ),*/

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		
		  'assetManager' => array(
            'linkAssets' => true,
        ),

		'bracmail'=>array(
            'class' => 'BracMail',
        ),
        /*'print'=>array(
            'class' => 'Print',
        ),*/
        'printMessage' => array(
            'class' => 'application.modules.api.components.PrintMessage',
        ),
		'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType'=>'smtp',
            'transportOptions'=>array(
                'host'=>'a2plcpnl0464.prod.iad2.secureserver.net',
                'username'=>'sending@nexkraft.com',
                'password'=>'123@456@',
                'encryption'=>'ssl',
                'port'=>'465',           
            ),
            'viewPath' => 'application.views.seatRequest',       
        ),
		/*'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				array(
					'class'=>'CWebLogRoute',
				),
			),
		),*/

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'shouman.d@brac.net',
		'mydeskdb' => array(
            //live
            //'host' => '192.168.2.17',
            //'port' => '3306',
            //'user' => 'deskuser',
            //'pass' => 'desk@123',
            //'dbname' => 'ifleetdb'
            //local
            'host' => '35.185.149.147',
            'port' => '3306',
            'user' => 'bractech',
            'pass' => '99.9%available',
            'dbname' => 'flowdb'
        ),
	),
);
