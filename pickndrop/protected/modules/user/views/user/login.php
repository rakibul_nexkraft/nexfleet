<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
$this->breadcrumbs=array(
    UserModule::t("Login"),
);
?>
    <style type="text/css">

        h1.login-heading-1 {
            font-weight: bold;
            margin: 0;
        }

        p.overlay-text {
            font-size: 14px;
            font-weight: 100;
            line-height: 20px;
            letter-spacing: 0.5px;
            margin: 20px 0 30px;
        }

        span.info-text {
            font-size: 12px;
        }

        a.loginlink {
            color: #333;
            font-size: 14px;
            text-decoration: none;
            margin: 15px 0;
        }
        a.nk-login-button {
            border-radius: 20px;
            border: 1px solid #b70068 !important;
            background-color: #b70068 !important;
            color: #FFFFFF !important;
            font-size: 12px;
            font-weight: bold;
            padding: 12px 45px;
            letter-spacing: 1px;
            text-transform: uppercase;
            transition: transform 80ms ease-in;
        }

        .button.button-3d:hover {
            background-color: #b70068 !important;
            opacity: 0.9;
        }

        button:active {
            transform: scale(0.95);
        }

        button:focus {
            outline: none;
        }

        button.ghost {
            background-color: transparent;
            border-color: #FFFFFF;
        }

        form {
            background-color: #FFFFFF;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 50px;
            height: 100%;
            text-align: center;
        }

        .login-form-input {
            background-color: #eee;
            border: none;
            padding: 12px 15px;
            margin: 8px 0;
            width: 100%;
        }

        .login-form-container {
            background-color: #fff;
            border-radius: 10px;
            /*box-shadow: 0 14px 28px rgba(0,0,0,0.25),0 10px 10px rgba(0,0,0,0.22);*/
            box-shadow: 0 0px 10px rgba(0,0,0,0.25), 0 4px 6px rgba(0,0,0,0.22);
            position: relative;
            overflow: hidden;
            width: 800px;/* 800px */
            max-width: 100%;
            min-height: 540px;/* 480px */
        }
        .form-login-centered{
            height: 100%;
            display: flex;
            align-items: center;
            margin-top: -60px;
            justify-content: center;
        }

        .form-container {
            position: absolute;
            top: 0;
            height: 100%;
            transition: all 0.6s ease-in-out;
        }

        .sign-in-container {
            left: 0;
            width: 50%;
            z-index: 2;
        }

        .login-form-container.right-panel-active .sign-in-container {
            transform: translateX(100%);
        }

        .sign-up-container {
            left: 0;
            width: 50%;
            opacity: 0;
            z-index: 1;
        }

        .login-form-container.right-panel-active .sign-up-container {
            transform: translateX(100%);
            opacity: 1;
            z-index: 5;
            animation: show 0.6s;
        }

        @keyframes show {
            0%, 49.99% {
                opacity: 0;
                z-index: 1;
            }

            50%, 100% {
                opacity: 1;
                z-index: 5;
            }
        }

        .overlay-container {
            position: absolute;
            top: 0;
            left: 50%;
            width: 50%;
            height: 100%;
            overflow: hidden;
            transition: transform 0.6s ease-in-out;
            z-index: 100;
        }

        .login-form-container.right-panel-active .overlay-container{
            transform: translateX(-100%);
        }

        .overlay {
            background: #ff4bb6;
            background: linear-gradient(to right, #ec0e8c, #ff4bb6);
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 0 0;
            color: #FFFFFF;
            position: relative;
            left: -100%;
            height: 100%;
            width: 200%;
            transform: translateX(0);
            transition: transform 0.6s ease-in-out;
        }

        .login-form-container.right-panel-active .overlay {
            transform: translateX(50%);
        }

        .overlay-panel {
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 0 40px;
            text-align: center;
            top: 0;
            height: 100%;
            width: 50%;
            transform: translateX(0);
            transition: transform 0.6s ease-in-out;
        }

        .overlay-left {
            transform: translateX(-20%);
        }

        .login-form-container.right-panel-active .overlay-left {
            transform: translateX(0);
        }

        .overlay-right {
            right: 0;
            transform: translateX(0);
        }

        .login-form-container.right-panel-active .overlay-right {
            transform: translateX(20%);
        }

        .social-container {
            margin: 20px 0;
            display: flex;
        }

        .social-container a {
            border: 1px solid #DDDDDD;
            border-radius: 50%;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            margin: 0 5px;
            height: 40px;
            width: 40px;
        }
        .height-100{
            height:100%;
        }
        .width-100{
            width: 100%;
        }
        .nk-bg-signup{
            background: #00A2B6 !important;
        }
        .nk-bg-login{
            background: #3EC1E6 !important;
        }
        .nk-color-signup{
            color: #00A2B6 !important;
        }
        .nk-color-login{
            color: #ec0e8c !important;
        }
        .nk-login-pink{
            background-color: #ec0e8c !important;
        }
        .errorSummary{
            margin-top: 1rem;
        }
        .errorSummary p{
            margin-bottom: 10px;
        }
        .sing-google{
            background-color: #fff;
            color: #757575;
            border-radius: 1px;
            -webkit-border-radius: 1px;
            -moz-border-radius: 1px;
            box-shadow: 0 2px 8px 0 rgba(0,0,0,.25);
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            transition: background-color .218s,border-color .218s,box-shadow .218s;
            -webkit-transition: background-color .218s,border-color .218s,box-shadow .218s;
            -moz-transition: background-color .218s,border-color .218s,box-shadow .218s;
            position: relative;
            text-align: right;
            vertical-align: middle;
            cursor: pointer;
            min-width: 105px;
            font-size: 14px;
        }
        .link-google {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: 0.25rem;
            transition: background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        .sing-google:hover, .sing-google:focus {
            box-shadow: 0 0 3px 3px rgba(66,133,244,.3);
            -webkit-box-shadow: 0 0 3px 3px rgba(66,133,244,.3);
            -moz-box-shadow: 0 0 3px 3px rgba(66,133,244,.3);
        }
        .link-google:focus, .link-google:hover {
            text-decoration: none;
        }
    </style>
   
        <!-- <form id="login-form" method="post" action='http://sso.brac.net/auth/isoap/login'>
             <?php if(isset($_GET['message']) && !empty($_GET['message'])) echo "<h4 style='color:red'>".$_GET['message']."</h4>"; ?>
             <label for="username">Username</label>
             <input class="required" type="text" id="user" name="user"/>
             <label for="password">Password</label>
             <input class="required password" type="password" id="password" name="password"/>
             <input type="hidden" id="site" name="site" value="http://www.nexkraft.com/pickndrop"/>
             <button type='submit' form='login-form'>submit</button>
        </form> -->
<?php /*echo CHtml::link('ADMIN','http://sso.brac.net/auth/isoap/login/session?site=http://www.nexkraft.com/pickndrop'); */
?>

    <section id="content">

        <div class="content-wrap nopadding">

            <div class="section nobg full-screen nopadding nomargin">

                <div class="row height-100">
                    <div class="col-md-12 form-login-centered">
                        <div class="login-form-container" id="login-form-container">
                            <div class="form-container sign-up-container"></div>
                            <div class="form-container sign-in-container">

                                <?php echo CHtml::beginForm(); ?>

                                    <duv style="display: flex; margin-left: 25px">

                                        <img src="<?php echo Yii::app()->baseUrl; ?>/../common/assets/images/ifleet-logo-2.png" alt="BRAC Logo" width="80px" height="120px">
                                        <span class="txt" style="font-size: 38px;color: #868686;font-weight:bold;padding-top: 40px">iFleet</span>
                                    </duv>
                                    <span class="info-text">Users with BRAC Gmail id must login from here</span>
                                    <a href="http://sso.brac.net/auth/isoap/login/session?site=http://www.nexkraft.com/pickndrop" id="glogin" class="link-google sing-google" style="margin-top: 10px;padding-top: 8px;    margin-bottom: 20px;">
                                        <i style="float: left"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="60" height="22px" viewBox="40 0 48 48" class="abcRioButtonSvg"><g><path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path><path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path><path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path><path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path><path fill="none" d="M0 0h48v48H0z"></path></g></svg></i>
                                         <span style="font-size: 14px; float: left; margin-right: 18px;">Sign in  </span>             
                                    </a>
                                    <h1 class="nk-color-login login-heading-1">Sign in</h1>
                                    <span class="info-text">Only for BRAC sister concern user</span>
                                    <?php if(Yii::app()->user->hasFlash('loginMessage')): ?>
                                        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
                                    <?php endif; ?>

                                    <?php echo CHtml::errorSummary($model); ?>
                                    <!--<p class="text-danger">< ?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>-->

                                    <?php echo CHtml::activeTextField($model,'username',array('class'=>'login-form-input not-dark', 'placeholder'=>'Enter Username')) ?>
                                    <div class="text-xs-left" style="width: 100%"></div>
                                    <?php echo CHtml::activePasswordField($model,'password',array('class'=>'login-form-input not-dark', 'placeholder'=>'Enter password')) ?>
                                    <div class="text-xs-left" style="width: 100%"></div>
                                    <div class="checkbox mb-3 width-100">
                                        <label>
                                            <?php echo CHtml::activeCheckBox($model,'rememberMe'); ?><?php echo CHtml::activeLabelEx($model,'rememberMe'); ?></span>
                                        </label>
                                    </div>

                                    <div class="col_full nobottommargin">
                                        <button class="button button-3d nk-login-pink nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                                        <div class="clearfix"></div>
                                    </div>
                                    <!--<div class="text-xs-center" style="margin-top: 1rem">
                                        <a href="< ?php echo Yii::app()->createAbsoluteUrl("/user/registration");?>" class="fleft">New User Registration</a>
                                    </div>
                                    <div class="text-xs-center" style="margin-top: 1rem">
                                        <a href="#" class="fright">Forgot Password?</a>
                                    </div>-->
                                <?php echo CHtml::endForm(); ?>
                            </div>
                            <div class="overlay-container">
                                <div class="overlay">
                                    <div class="overlay-panel overlay-left"></div>
                                    <div class="overlay-panel overlay-right">
                                        <h1 class="login-heading-1" style="color: white;">Hello!</h1>
                                        <p class="overlay-text">If your are not registered yet please click on the link below and start your journey with us</p>
                                        <a href='<?php echo Yii::app()->createAbsoluteUrl("/user/registration");?>' class="ghost cursor-pointer nk-login-button">Sign Up</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </section><!-- #content end -->

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>