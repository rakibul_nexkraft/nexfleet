<div class="form border-round-bottom" style="padding: 2rem;background: white;">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>

    <div class="col_full nobottommargin">
        <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
    </div>
    <div class="col_full bottommargin">
        <!--<p class="note">< ?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>-->
        <?php /*echo $form->errorSummary(array($model,$profile));*/
        ?>
    </div>

    <div class="panel panel-default divcenter noradius noborder clearfix">
        <div class="panel-body">
            <div class="col_half">
                <?php echo $form->labelEx($model,'username'); ?>
                <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'username'); ?>
            </div>
            <div class="col_half col_last">
                <?php echo $form->labelEx($model,'password'); ?>
                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>
            <div class="clear"></div>
            <div class="col_full">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
            <div class="clear"></div>
            <div class="col_half">
                <?php echo $form->labelEx($model,'superuser'); ?>
                <?php echo $form->dropDownList($model,'superuser',User::itemAlias('AdminStatus'), array('class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'superuser'); ?>
            </div>
            <div class="col_half col_last">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status',User::itemAlias('UserStatus'), array('class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
            <?php
            $profileFields=$profile->getFields();
            if ($profileFields) {
                foreach($profileFields as $key=>$field) {
                    ?>
                    <div class="col_half  <?php echo $key%2?'col_last':''; ?>">
                        <?php echo $form->labelEx($profile,$field->varname); ?>
                        <?php
                        if ($widgetEdit = $field->widgetEdit($profile)) {
                            echo $widgetEdit;
                        } elseif ($field->range) {
                            echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                        } elseif ($field->field_type=="TEXT") {
                            echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class' => 'form-control not-dark'));
                        } else {
                            echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class' => 'form-control not-dark'));
                        }
                        ?>
                        <?php echo $form->error($profile,$field->varname); ?>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="col_full" style="text-align: right;">
                <?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class' => 'button button-rounded button-pink nomargin')); ?>
                <a class="button button-rounded  button-pink" style="color: #fff !important;" href="<?php echo $model->isNewRecord ? Yii::app()->createUrl("user/user") : Yii::app()->createUrl("user/user/view", array("id"=>$model->id)) ?>">Cancel</a>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->