<div class="form page_header_div" style="margin-bottom: 40px !important;">

<?php echo $profileId ?>
    <h4 class="heading-custom page_header_h4" style="border-bottom: 1px solid #f2ecec;"><?php echo UserModule::t("Change Password"); ?></h4>

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'changepassword-form',
        'enableAjaxValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    ));
    ?>

    <div class="col_full nobottommargin" style="margin-top: 40px;padding: 2rem;">
        <p class="note" style="margin-bottom: 0 !important;"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
    </div>
    <div class="col_full bottommargin">
        <!--<p class="note">< ?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>-->
        <?php echo $form->errorSummary($model); ?>
    </div>

    <div class="panel panel-default divcenter noradius noborder clearfix">
        <div class="panel-body">

                <div class="col_full" style="display: flex;">
                    <div class="col_half" style="width: 35%;margin-top: 7px;"><?php echo $form->labelEx($model,'oldPassword'); ?></div>
                    <div class="col_half" style="margin-left: -25rem;"><?php echo $form->passwordField($model,'oldPassword',array('class' => 'form-control not-dark')); ?></div>
                    <?php echo $form->error($model,'oldPassword'); ?>
                </div>
                <div class="clear"></div>
                <div class="col_full" style="display: flex;">
                    <div class="col_half" style="width: 35%;margin-top: 7px;"><?php echo $form->labelEx($model,'password'); ?></div>
                    <div class="col_half" style="margin-left: -25rem;"><?php echo $form->passwordField($model,'password',array('class' => 'form-control not-dark')); ?></div>
                    <?php echo $form->error($model,'password'); ?>
                    <p class="hint">
                    <?php /*echo UserModule::t("Minimal password length 4 symbols."); */?>
                </p>
                </div>
                <div class="clear"></div>
                <div class="col_full" style="display: flex;">
                    <div class="col_half" style="width: 35%;margin-top: 7px;"><?php echo $form->labelEx($model,'verifyPassword'); ?></div>
                    <div class="col_half" style="margin-left: -25rem;"><?php echo $form->passwordField($model,'verifyPassword',array('class' => 'form-control not-dark')); ?></div>
                    <?php echo $form->error($model,'verifyPassword'); ?>
                </div>
                <div class="clear"></div>

            <div class="col_full" style="text-align: right;">
                <?php echo CHtml::submitButton(UserModule::t("Save"), array('class' => 'button button-3d button-green nomargin')); ?>
                <a class="button button-3d button-rounded button-red" style="color: #fff !important;" href="<?php echo Yii::app()->createUrl("user/profile") ?>">Cancel</a>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>