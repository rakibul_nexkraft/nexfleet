<div class="form">
    <?php if(Yii::app()->user->hasFlash('profileMessage')): ?>
        <div class="style-msg successmsg">
            <div class="sb-msg">
                <button type="button" class="close" data-dismiss="successmsg" aria-hidden="true">×</button>
                <i class="icon-thumbs-up"></i><?php echo Yii::app()->user->getFlash('profileMessage'); ?>
            </div>
        </div>
    <?php endif; ?>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'profile-form',
        'enableAjaxValidation'=>true,
        'htmlOptions' => array('enctype'=>'multipart/form-data'),
    ));
    ?>



    <!--<div class="col_full nobottommargin">
        <p class="note"><?php /*echo UserModule::t('Fields with <span class="required">*</span> are required.'); */?></p>
    </div>-->
    <div class="col_full bottommargin">
        <!--<p class="note">< ?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>-->
        <?php echo $form->errorSummary(array($model,$profile)); ?>
    </div>
    <div class="col_full nobottommargin" style="border-radius: 10px !important;">
        <h4 class="heading-custom page_header_h4" style="border-bottom: 1px solid #f2ecec;border-radius: 10px;"><?php echo UserModule::t("Edit Profile"); ?></h4>
    </div>

    <div class="panel panel-default divcenter noradius noborder clearfix" style="border-radius: 10px !important;">
        <div class="panel-body">
            <div class="col_full nobottommargin">
                <p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
            </div>
            <div class="clear"></div>
            <div class="col_half">
                <?php echo $form->labelEx($model,'username'); ?>
                <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20, 'readonly'=>true, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'username'); ?>
            </div>
            <div class="col_half col_last">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'class' => 'form-control not-dark')); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
            <div class="clear"></div>
            <?php
            $profileFields=$profile->getFields();
            if ($profileFields) {
                foreach($profileFields as $key=>$field) {
                    ?>
                    <div class="col_half  <?php echo $key%2?'col_last':''; ?>">
                        <?php echo $form->labelEx($profile,$field->varname); ?>
                        <?php
                        if ($widgetEdit = $field->widgetEdit($profile)) {
                            echo $widgetEdit;
                        } elseif ($field->range) {
                            echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                        } elseif ($field->field_type=="TEXT") {
                            echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50, 'class' => 'form-control not-dark'));
                        } else {
                            echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class' => 'form-control not-dark'));
                        }
                        ?>
                        <?php echo $form->error($profile,$field->varname); ?>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="col_full" style="text-align: right;">
                <?php echo CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class' => 'button button-3d button-green nomargin')); ?>
                <a class="button button-3d button-rounded button-red" style="color: #fff !important;" href="<?php echo Yii::app()->createUrl("user/profile") ?>">Cancel</a>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>


</div><!-- form -->
