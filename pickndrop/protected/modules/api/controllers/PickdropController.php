<?php

class PickdropController extends Controller
{
	public function actionView()

	{
		$data='';
		if(empty($_GET['id'])) {
			Yii::app()->printMessage->getPrint(0,$data,"User pin cannot be empty");
			return;
		}
		else {
			$route_info = Yii::app()->db->createCommand("SELECT t.*,r.route_no,r.price,r.route_detail,d.phone,d.name,v.reg_no,d.pin, vt.type FROM tbl_seat_request as t INNER JOIN tbl_routes as r ON r.id=t.route_id INNER JOIN tbl_vehicles as v ON v.reg_no=r.vehicle_reg_no INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin INNER JOIN tbl_vehicletypes as vt ON vt.id=v.vehicletype_id  INNER JOIN tbl_commonfleets as c ON c.present_route = r.route_no WHERE c.user_pin='".$_GET['id']."' AND c.application_type ='Pick and Drop' AND t.queue='-1' AND t.status='1'")->queryRow(); 
			if($route_info) {
				$data[] = array(
					'route_no' => $route_info['route_no'],
					'route_detail' => SeatRequest::routeDetailStoppage($route_info['route_id']),
					'vehicle_no' => $route_info['reg_no'],
					'vehicle_type' => $route_info['type'],
					'driver_name' =>  $route_info['name'],
					'driver_name' =>  $route_info['name'],
					'driver_phone' => $route_info['phone'],
					'pickup_time' => SeatRequest::stoppagePickupTime($route_info['stoppage_id']),
					'price' => $route_info['price'],
					'user_status' =>SeatRequest::userStatus($route_info['user_status']), 
					'fleet_status' =>SeatRequest::status($route_info['status']), 
					'expected_date' =>$route_info['expected_date'], 
					'queue' => SeatRequest::queueCheck($route_info['queue'],$route_info['status']),

				);

				Yii::app()->printMessage->getPrint(1,$data,"",0);
				return;
			}
			else {
				Yii::app()->printMessage->getPrint(0,$data,"Sorry, you have no approved route.");
				return;
			}
			

		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}