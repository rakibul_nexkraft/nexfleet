<?php

class SaleController extends Controller
{
	public function actionView()

	{
		$data='';
		if(empty($_GET['id'])) {
			Yii::app()->printMessage->getPrint(0,$data,"User pin cannot be empty");
			return;
		}
		else {
		    $use_route = Yii::app()->db->createCommand("SELECT r.* FROM  tbl_commonfleets as c INNER JOIN  tbl_routes as r ON r.route_no=c.present_route WHERE c.application_type<>'Cancel Seat' AND c.user_pin='".$_GET['id']."'")->queryRow();
			if($use_route) {
				$data[] = array(
					'route_id' => $use_route['id'],
					'route_no' => $use_route['route_no'],
					'price' => $use_route['market_price'],
				);

				Yii::app()->printMessage->getPrint(1,$data,"",0);
				return;
			}
			else {
				Yii::app()->printMessage->getPrint(0,$data,"Sorry, you have no approved route.");
				return;
			}
			

		}
	}
	public function actionCreate()
	{
		if(empty($_POST['SeatSale']['user_pin'])) {
			 Yii::app()->printMessage->getPrint(0,"","User pin cannot be empty.");
			 return;
		}
		if($_POST['SeatSale']['travel_direction'] == 1 && date('Y-m-d', strtotime("+1 day")) == $_POST['SeatSale']['from_date'] && date('H:i:s')<"20:00:00") {    		
    	}
    	else if($_POST['SeatSale']['travel_direction'] == 2 && date('Y-m-d') == $_POST['SeatSale']['from_date'] && date('H:i:s')<"16:30:00") {   		   		
    	}
    	else {
    		Yii::app()->printMessage->getPrint(0,"","Check Your Date.");
			 return;
    	}    	
		$model = new SeatSale;        
		if(isset($_POST['SeatSale']))
		{
			$model->attributes=$_POST['SeatSale'];			          
        	$model->created_by = $_POST['SeatSale']['user_pin'];           
        	$model->created_time = date("Y-m-d H:i:s");
			$check_sale = Yii::app()->db->createCommand("SELECT * FROM tbl_seat_sale WHERE user_pin='".$model->user_pin."' AND route_id='".$model->route_id."' AND from_date='".$model->from_date."' AND travel_direction='".$model->travel_direction."'")->queryRow();
			if($check_sale) {
				Yii::app()->printMessage->getPrint(0,"","Your All Ready Sale This Seat.");
			 	return;				
			}			
			if($model->save()){
				$data = array("success"=>"Your Data Save Successfully.");		
				Yii::app()->printMessage->getPrint(1,$data,"");			
				return;
			}
		}
		Yii::app()->printMessage->getPrint(0,"","Please, Try Again!");			
		return;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}