<?php

/**
 * This is the model class for table "{{transaction_history}}".
 *
 * The followings are the available columns in table '{{transaction_history}}':
 * @property integer $id
 * @property string $user_pin
 * @property integer $sale_id
 * @property integer $booking_id
 * @property double $credit
 * @property double $debit
 * @property string $payment_method
 * @property string $transaction_id
 * @property double $balance
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class TransactionHistory extends CActiveRecord
{
	public $from_date;
	public $to_date;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TransactionHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transaction_history}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin,balance, created_by', 'required'),
			array('sale_id, booking_id, sale_booking_id', 'numerical', 'integerOnly'=>true),
			array('credit, debit, balance', 'numerical'),
			array('user_pin, payment_method, transaction_id, created_by, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time,to_date,from_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, sale_id, booking_id, credit, debit, payment_method, transaction_id, balance, created_by, created_time, updated_by, updated_time,to_date,from_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_pin' => 'User Pin',
			'sale_id' => 'Sale',
			'booking_id' => 'Booking',
			'credit' => 'Credit',
			'debit' => 'Debit',
			'payment_method' => 'Payment Method',
			'transaction_id' => 'Transaction Id',
			'balance' => 'Balance',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'sale_booking_id'=>'Sale Booking id',
			'to_date' =>'To Date',
			'from_date' =>'From Date'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		if(Yii::app()->user->name!='Guest' && isset(Yii::app()->user->picmarket_user) && Yii::app()->user->picmarket_user==1){
			$this->user_pin = Yii::app()->user->username;
		}
		if($this->user_pin == 'iFleet')$this->user_pin=0;		
		$criteria->compare('id',$this->id);
		$criteria->compare('user_pin',$this->user_pin);
		$criteria->compare('sale_id',$this->sale_id);
		$criteria->compare('booking_id',$this->booking_id);
		$criteria->compare('credit',$this->credit);
		$criteria->compare('debit',$this->debit);
		$criteria->compare('payment_method',$this->payment_method,true);
		$criteria->compare('transaction_id',$this->transaction_id,true);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		if(!empty($this->from_date) && empty($this->to_date))
		{
			$criteria->addCondition('DATE(created_time) >= "'.$this->from_date.'"');
		}
		elseif(!empty($this->to_date) && empty($this->from_date))
		{
			$criteria->addCondition('DATE(created_time)  <= "'.$this->to_date.'"');
		}
		elseif(!empty($this->to_date) && !empty($this->from_date))
		{
			
			$criteria->addCondition('DATE(created_time) BETWEEN "'.$this->from_date.'" AND "'.$this->to_date.'"');
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
	        	'pageSize'=>20,
	    	),
	    	'sort'=>array('defaultOrder'=>'id DESC')
		));
	}
}