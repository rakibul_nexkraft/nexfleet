<?php

/**
 * This is the model class for table "{{maternity_leave_assign}}".
 *
 * The followings are the available columns in table '{{maternity_leave_assign}}':
 * @property integer $id
 * @property integer $seat_request_id
 * @property string $user_pin
 * @property string $from
 * @property string $to
 * @property string $maternity_user_pin
 * @property integer $meternity_request_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 * @property integer $status
 */
class MaternityLeaveAssign extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaternityLeaveAssign the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{maternity_leave_assign}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, from, to, maternity_user_pin, meternity_request_id, created_by', 'required'),
			array('seat_request_id, meternity_request_id, status', 'numerical', 'integerOnly'=>true),
			array('user_pin, maternity_user_pin, created_by, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, seat_request_id, user_pin, from, to, maternity_user_pin, meternity_request_id, created_by, created_time, updated_by, updated_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'seat_request_id' => 'Seat Request ID',
			'user_pin' => 'User Pin',
			'from' => 'From',
			'to' => 'To',
			'maternity_user_pin' => 'Maternity User Pin',
			'meternity_request_id' => 'Meternity Request ID',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('seat_request_id',$this->seat_request_id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('to',$this->to,true);
		$criteria->compare('maternity_user_pin',$this->maternity_user_pin,true);
		$criteria->compare('meternity_request_id',$this->meternity_request_id);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}