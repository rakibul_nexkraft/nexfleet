<?php

/**
 * This is the model class for table "{{stoppage}}".
 *
 * The followings are the available columns in table '{{stoppage}}':
 * @property integer $id
 * @property integer $route_id
 * @property string $stoppage
 * @property string $pickup_time
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Stoppage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Stoppage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{stoppage}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('route_id, stoppage,pickup_time, created_by', 'required'),
			array('route_id', 'numerical', 'integerOnly'=>true),
			array('stoppage, created_by, updated_by', 'length', 'max'=>128),
			array('pickup_time, created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, route_id, stoppage, pickup_time, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'route_id' => 'Route',
			'stoppage' => 'Stoppage',
			'pickup_time' => 'Pickup Time',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		if(!empty($this->pickup_time)){
			$this->pickup_time=str_replace('.',':',$this->pickup_time).":00";
			
		}
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('stoppage',$this->stoppage,true);
		$criteria->compare('pickup_time',$this->pickup_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function routeNo($id){
		$detail=Routes::model()->findByPk($id);
		return $detail['route_no'];
	}
	public function getAllRouteNo() {
        $data         = array();
        $routesModel = Routes::model()->findAll();
        foreach($routesModel as $get){
            $data[$get->id] = $get->route_no;
        }
        return $data;
    }
}