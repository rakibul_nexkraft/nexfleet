<?php

/**
 * This is the model class for table "{{seat_sale}}".
 *
 * The followings are the available columns in table '{{seat_sale}}':
 * @property integer $id
 * @property string $user_pin
 * @property integer $route_id
 * @property double $price
 * @property string $from_date
 * @property string $to_date
 * @property integer $travel_direction
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class SeatSale extends CActiveRecord
{
	public $route_no;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SeatSale the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seat_sale}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, route_id, price, from_date, travel_direction, created_by', 'required'),
			array('route_id, travel_direction', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('user_pin, route_no, created_by, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, route_id, price, from_date, to_date, travel_direction, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_pin' => 'User Pin',
			'route_id' => 'Route',
			'price' => 'Price',
			'from_date' => 'From Date',
			'to_date' => 'To Date',
			'travel_direction' => 'Travel Direction',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'route_no' => 'Route No',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('price',$this->price);
		$criteria->compare('from_date',$this->from_date,true);
		$criteria->compare('to_date',$this->to_date,true);
		$criteria->compare('travel_direction',$this->travel_direction);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}