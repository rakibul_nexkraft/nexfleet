<?php

/**
 * This is the model class for table "{{vehiclesisters}}".
 *
 * The followings are the available columns in table '{{vehiclesisters}}':
 * @property string $reg_no
 * @property integer $vehicletype_id
 * @property string $engine_no
 * @property string $cc
 * @property string $vmodel
 * @property string $model_code
 * @property string $remarks
 * @property string $chassis_no
 * @property string $country
 * @property string $purchase_date
 * @property integer $purchase_price
 * @property string $supplier
 * @property integer $seat_capacity
 * @property string $load_capacity
 * @property string $ac
 * @property integer $driver_pin
 * @property string $location
 * @property string $driver_name
 * @property integer $helper_pin
 * @property string $helper_name
 * @property string $created_time
 * @property string $created_by
 * @property integer $active
 */
class Vehiclesisters extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Vehiclesisters the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vehiclesisters}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('    reg_no', 'required'),
			array('vehicletype_id, purchase_price, seat_capacity, driver_pin, helper_pin, active', 'numerical', 'integerOnly'=>true),
			array('reg_no, engine_no, model_code, remarks, chassis_no, country, supplier, location, driver_name, helper_name, created_by', 'length', 'max'=>127),
			array('cc, vmodel, load_capacity', 'length', 'max'=>20),
			array('ac', 'length', 'max'=>5),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('reg_no, vehicletype_id, engine_no, cc, vmodel, model_code, remarks, chassis_no, country, purchase_date, purchase_price, supplier, seat_capacity, load_capacity, ac, driver_pin, location, driver_name, helper_pin, helper_name, created_time, created_by, active', 'safe', 'on'=>'search'),
			 array('created_time', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false, 'on'=>'insert'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'vehicletypes'=>array(self::BELONGS_TO, 'Vehicletypes', 'vehicletype_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'reg_no' => 'Reg No',
			'vehicletype_id' => 'Vehicletype',
			'engine_no' => 'Engine No',
			'cc' => 'Cc',
			'vmodel' => 'Vmodel',
			'model_code' => 'Model Code',
			'remarks' => 'Remarks',
			'chassis_no' => 'Chassis No',
			'country' => 'Country',
			'purchase_date' => 'Purchase Date',
			'purchase_price' => 'Purchase Price',
			'supplier' => 'Supplier',
			'seat_capacity' => 'Seat Capacity',
			'load_capacity' => 'Load Capacity',
			'ac' => 'Ac',
			'driver_pin' => 'Driver Pin',
			'location' => 'Location',
			'driver_name' => 'Driver Name',
			'helper_pin' => 'Helper Pin',
			'helper_name' => 'Helper Name',
			'created_time' => 'Created Time',
			'created_by' => 'Created By',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('reg_no',$this->reg_no,true);
		$criteria->compare('vehicletype_id',$this->vehicletype_id);
		$criteria->compare('engine_no',$this->engine_no,true);
		$criteria->compare('cc',$this->cc,true);
		$criteria->compare('vmodel',$this->vmodel,true);
		$criteria->compare('model_code',$this->model_code,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('chassis_no',$this->chassis_no,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('purchase_date',$this->purchase_date,true);
		$criteria->compare('purchase_price',$this->purchase_price);
		$criteria->compare('supplier',$this->supplier,true);
		$criteria->compare('seat_capacity',$this->seat_capacity);
		$criteria->compare('load_capacity',$this->load_capacity,true);
		$criteria->compare('ac',$this->ac,true);
		$criteria->compare('driver_pin',$this->driver_pin);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('driver_name',$this->driver_name,true);
		$criteria->compare('helper_pin',$this->helper_pin);
		$criteria->compare('helper_name',$this->helper_name,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>45,
            ),
		));
	}
}