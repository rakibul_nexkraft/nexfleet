<?php


/**
 * This is the model class for table "{{seat_booking}}".
 *
 * The followings are the available columns in table '{{seat_booking}}':
 * @property integer $id
 * @property integer $sale_id
 * @property string $user_pin
 * @property string $date_from
 * @property string $date_to
 * @property integer $direction_travale
 * @property integer $route_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class SeatBooking extends CActiveRecord
{	public $route_no;
	public $price;
	public $total_spend;
	public $total_book;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SeatBooking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seat_booking}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, date_from,  direction_travale, route_id, created_by,price', 'required'),
			array('sale_id, direction_travale, route_id, total_spend,total_book,status', 'numerical', 'integerOnly'=>true),
			array('user_pin, created_by, updated_by,route_no,price,phone,transfer_user', 'length', 'max'=>128),
			array('created_time, updated_time, date_to, date_from, status', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sale_id, user_pin, date_from, date_to, direction_travale, route_id, transfer_user, created_by, created_time, updated_by, updated_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sale_id' => 'Sale',
			'user_pin' => 'User Pin',
			'date_from' => 'Date From',
			'date_to' => 'Date To',
			'direction_travale' => 'Direction Travale',
			'route_id' => 'Route',
			'phone'=>'Phone',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'transfer_user' => 'Transfer User',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sale_id',$this->sale_id);
		$criteria->compare('user_pin',$this->user_pin,true);
		$criteria->compare('date_from',$this->date_from,true);
		$criteria->compare('date_to',$this->date_to,true);
		$criteria->compare('direction_travale',$this->direction_travale);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('transfer_user',$this->transfer_user);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function shohochorReport(){

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sale_id',$this->sale_id);
		$criteria->compare('user_pin',$this->user_pin);
		//$criteria->compare('date_from',$this->date_from,true);
		//$criteria->compare('date_to',$this->date_to,true);
		$criteria->select = 'count(user_pin) as total_book, SUM(price) as total_spend,user_pin';		
		$criteria->group='user_pin';
		
		if(!empty($this->date_from) && empty($this->date_to))
		{
			
			$criteria->addCondition('date_from >= "'.$this->date_from.'"');
		}
		elseif(!empty($this->date_to) && empty($this->date_from))
		{	
			var_dump($this->date_to);exit();
			$criteria->addCondition('date_from <= "'.$this->date_to.'"');
		}
		elseif(!empty($this->date_to) && !empty($this->date_from))
		{
			//$criteria->condition = "updated_time  >= '$this->from_date' and updated_time <= '$this->to_date'";
			$criteria->addCondition('date_from BETWEEN "'.$this->date_from.'" AND "'.$this->date_to.'"');
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>45,
				
			),
		));
	}

	
}