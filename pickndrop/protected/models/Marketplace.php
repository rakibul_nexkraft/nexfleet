<?php

/**
 * This is the model class for table "{{marketplace}}".
 *
 * The followings are the available columns in table '{{marketplace}}':
 * @property integer $id
 * @property integer $route_id
 * @property integer $zone_id
 * @property string $price
 * @property string $username
 * @property integer $status
 * @property integer $queue
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 * @property string $release_date
 * @property string $travel_direction
 */
class Marketplace extends CActiveRecord
{
    public $zone_id;
    public $checkTravel;
    public $vehicle_type_id;
    public $start_point;
    public $end_point;
    public $checked;
    public $next_slots = array();
    public $from;
    public $to;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Marketplace the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{marketplace}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('route_id, zone_id, price, username', 'required'),
			array('route_id, zone_id, status, start_point, end_point, queue', 'numerical', 'integerOnly'=>true),
			array('price', 'length', 'max'=>10),
			array('username', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, route_id, zone_id, price, username, status, from,to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'route_id' => 'Route ID',
			'zone_id' => 'Zone ID',
			'price' => 'Seat Price',
			'username' => 'Username',
			'status' => 'Status',
            'start_point'=>'Starting Point',
            'end_point'=>'Dropping Point',
            'queue'=>'Queue'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('username',$this->username=Yii::app()->user->username,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function sell_history_admin()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('route_id',$this->route_id);
        $criteria->compare('zone_id',$this->zone_id);
        $criteria->compare('price',$this->price,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}