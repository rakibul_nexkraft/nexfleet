<?php

/**
 * This is the model class for table "{{seat_request}}".
 *
 * The followings are the available columns in table '{{seat_request}}':
 * @property integer $id
 * @property string $user_pin
 * @property string $user_name
 * @property string $user_department
 * @property integer $user_level
 * @property string $user_cell
 * @property string $email
 * @property integer $route_id
 * @property string $expected_date
 * @property string $remarks
 * @property integer $status
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
date_default_timezone_set("Asia/Dhaka");
class SeatRequestAdmin extends CActiveRecord
{	public $zone;
	public $previous;
	public $page_title;
	public $phone;
	public $name;
	public $reg_no;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SeatRequestAdmin the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{seat_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_pin, user_name, user_department, user_level, route_id, expected_date, created_by', 'required'),
			array('user_level, zone, route_id, status,user_status,queue,stoppage_id,maternity_leave,previous,action', 'numerical', 'integerOnly'=>true),
			array('user_pin, user_name, user_department, user_cell, email, created_by, updated_by, 	user_designation,residence_address,page_title,phone,name,reg_no', 'length', 'max'=>128),
			array('remarks', 'length', 'max'=>256),
			array('created_time, updated_time,zone, user_designation,residence_address,stoppage_id,phone,name,reg_no', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_pin, user_name, user_department, user_level, user_cell, email, route_id, expected_date, remarks, status, created_by, created_time, updated_by, updated_time,zone,queue,	user_designation,residence_address,stoppage_id,maternity_leave,phone,name,reg_no', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID No.',
			'user_pin' => 'PIN',
			'user_name' => 'Name',
			'user_department' => 'Department',
			'user_level' => 'Level',
			'user_cell' => 'Cell',
			'email' => 'Email',
			'route_id' => 'Route Detail',
			'expected_date' => 'Expected Date',
			'remarks' => 'Remarks',
			'status' => 'Previous Fleet Action',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'zone'=>'Zone',
			'queue'=>'Queue',
			'user_designation'=>'User Designation',
			'residence_address'=>'Residence Address',
			'stoppage_id'=>'Stoppage',
			'maternity_leave'=>'M.S.Use',
			'previous'=>'Previous',
			'page_title'=>'page_title',
			'user_status'=>'User Request Type',
			'phone' =>'Driver Phone',
			'name' => 'Driver Name',
			'reg_no' =>'Vehicle Reg. No',
			'action' => 'Current iFleet Action',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($status=null)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria=new CDbCriteria;
		if(!empty($status) && $status="pending"){
			$this->action=0;
		}
		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.user_pin',$this->user_pin,true);
		$criteria->compare('t.user_name',$this->user_name,true);
		$criteria->compare('t.user_department',$this->user_department,true);
		$criteria->compare('t.user_level',$this->user_level);
		$criteria->compare('t.user_cell',$this->user_cell,true);
		$criteria->compare('t.email',$this->email,true);
		$criteria->compare('t.route_id',$this->route_id);
		$criteria->compare('t.expected_date',$this->expected_date,true);
		$criteria->compare('t.remarks',$this->remarks,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('t.user_status',$this->user_status);
		$criteria->compare('t.action',$this->action);
		$criteria->compare('t.queue',$this->queue);
		$criteria->compare('t.created_by',$this->created_by,true);
		$criteria->compare('t.created_time',$this->created_time,true);
		$criteria->compare('t.updated_by',$this->updated_by,true);
		$criteria->compare('t.updated_time',$this->updated_time,true);
		$criteria->compare('t.maternity_leave',$this->maternity_leave,true);
		$criteria->compare('t.action',$this->action);
		//$criteria->order="t.id DESC";
		$criteria->select="t.*,d.phone,d.name,v.reg_no";
		$criteria->join="INNER JOIN tbl_routes as r ON r.id=t.route_id INNER JOIN tbl_vehicles as v ON v.reg_no=r.vehicle_reg_no INNER JOIN tbl_drivers as d ON v.driver_pin=d.pin";
		if(!empty($this->previous) && $this->previous==1){
			$date=date('Y-m-d');
			$criteria->addCondition('DATE(t.created_time)< "'.$date.'"');
		}
		if(!empty($this->previous) && $this->previous==2){
			$date=date('Y-m-d');
			$criteria->addCondition('DATE(t.created_time)="'.$date.'"');
		}
		if(!empty($this->previous) && $this->previous==3){
			$date=date('Y-m-d');
			$criteria->addCondition('DATE(t.updated_time)< "'.$date.'"');
		}
		if(!empty($this->previous) && $this->previous==4){
			$date=date('Y-m-d');
			$criteria->addCondition('DATE(t.updated_time)="'.$date.'"');
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
	        	'pageSize'=>40,
	    	),
	    	'sort'=>array('defaultOrder'=>'t.id DESC')
		));
	}
	public function routeDetail($id){
		$detail=Routes::model()->findByPk($id);
		return "Route Number: ".$detail['route_no'].", Route: ".$detail['route_detail'];
	}
	public function status($status){
		if($status==0) return "Pending";
		if($status==1) return "Approved";
		if($status==2) return "Queue";
		if($status==3) return "Cancel Request";
		if($status==-1) return "Cancel";
	}
	public function queueCheck($queue,$status){
		if($status==1 && $queue==-1){
			return "Assigned";
		}
		else if($status==1 && $queue!=0){
			return $queue;
		}
		else {
			return "";
		}

	}
	public function assignCheck($id){return $id;}
	public function sendSMS($user_cell, $message) {
		 
		  if (strlen($user_cell) == 10)
            $user_cell = "0" . $user_cell;
        else
            $user_cell = $user_cell;

        $client = new EHttpClient(
            'http://mydesk.brac.net/smsc/create', array(
                'maxredirects' => 0,
                'timeout' => 30
            )
        );
        $client->setParameterPost(array(
            'token' => '286cf3ae67c80ee7d034bb68167ab8455ea443a2',
            'message' => $message,
            'to_number' => $user_cell,
            'app_url' => 'http://ifleet.brac.net/',
        ));       

        return $response = $client->request('POST');
        
    }
}