<?php

/**
 * This is the model class for table "{{booking}}".
 *
 * The followings are the available columns in table '{{booking}}':
 * @property integer $id
 * @property integer $zone_id
 * @property integer $route_id
 * @property string $price
 * @property integer $status
 * @property string $sold_by
 * @property string $booked_by
 * @property string $release_date
 * @property string $travel_direction
 * @property integer $marketplace_id
 * @property string $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class Booking extends CActiveRecord
{
    public $vehicle_reg_no;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Booking the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{booking}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('zone_id, route_id, price, status, sold_by, booked_by, release_date, travel_direction, marketplace_id', 'required'),
			array('zone_id, route_id, status, marketplace_id', 'numerical', 'integerOnly'=>true),
			array('price', 'length', 'max'=>10),
			array('sold_by, booked_by', 'length', 'max'=>128),
			array('travel_direction', 'length', 'max'=>50),
			array('updated_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, zone_id, route_id, price, status, sold_by, booked_by, release_date, travel_direction, marketplace_id, created_by, created_time, updated_by, updated_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'zone_id' => 'Zone',
			'route_id' => 'Route',
			'price' => 'Price',
			'status' => 'Status',
			'sold_by' => 'Sold By',
			'booked_by' => 'Booked By',
			'release_date' => 'Release Date',
			'travel_direction' => 'Travel Direction',
			'marketplace_id' => 'Marketplace',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sold_by',$this->sold_by,true);
		$criteria->compare('booked_by',$this->booked_by,true);
		$criteria->compare('release_date',$this->release_date,true);
		$criteria->compare('travel_direction',$this->travel_direction,true);
		$criteria->compare('marketplace_id',$this->marketplace_id);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by);
		$criteria->compare('updated_time',$this->updated_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function booking_history_admin(){
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('zone_id',$this->zone_id);
        $criteria->compare('route_id',$this->route_id);
        $criteria->compare('price',$this->price,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('sold_by',$this->sold_by,true);
        $criteria->compare('booked_by',$this->booked_by,true);
        $criteria->compare('release_date',$this->release_date,true);
        $criteria->compare('travel_direction',$this->travel_direction,true);
        $criteria->compare('marketplace_id',$this->marketplace_id);
        $criteria->compare('created_by',$this->created_by);
        $criteria->compare('created_time',$this->created_time,true);
        $criteria->compare('updated_by',$this->updated_by);
        $criteria->compare('updated_time',$this->updated_time,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}