<?php

/**
 * This is the model class for table "{{pickdrop_notification}}".
 *
 * The followings are the available columns in table '{{pickdrop_notification}}':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $username
 * @property string $type
 * @property integer $is_notified
 * @property string $web_url
 * @property string $key_id
 */
class PickdropNotification extends CActiveRecord
{
    const MARKETPLACE_BUY_DETAIL_URL = "marketplace/bookingDetails/";
    const MARKETPLACE_SELL_DETAIL_URL = "marketplace/sellingDetails/";
    const PICKDROP_REQUEST_DETAIL_URL = "";

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PickdropNotification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pickdrop_notification}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_notified', 'numerical', 'integerOnly'=>true),
			array('title, key_id', 'length', 'max'=>45),
			array('description', 'length', 'max'=>500),
			array('username, type', 'length', 'max'=>100),
			array('web_url', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, description, username, type, is_notified, web_url, key_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'username' => 'Username',
			'type' => 'Type',
			'is_notified' => 'Is Notified',
			'web_url' => 'Web Url',
			'key_id' => 'Key',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('is_notified',$this->is_notified);
		$criteria->compare('web_url',$this->web_url,true);
		$criteria->compare('key_id',$this->key_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}