<?php

/**
 * This is the model class for table "{{sister_domain}}".
 *
 * The followings are the available columns in table '{{sister_domain}}':
 * @property integer $id
 * @property string $sister
 * @property string $domain
 * @property integer $created_by
 * @property string $created_time
 * @property string $updated_by
 * @property string $updated_time
 */
class SisterDomain extends CActiveRecord
{
	public $date_to;
	public $date_from;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SisterDomain the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sister_domain}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sister, domain', 'required'),			
			array('sister, created_by, domain, updated_by', 'length', 'max'=>128),
			array('created_time, updated_time, date_to, date_from', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sister, domain, created_by, created_time, updated_by, updated_time, date_to, date_from', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sister' => 'Sister Concern Name',
			'domain' => 'Domain Name',
			'created_by' => 'Created By',
			'created_time' => 'Created Time',
			'updated_by' => 'Updated By',
			'updated_time' => 'Updated Time',
			'date_from' =>'Date From',
			'date_to' =>'Date To',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sister',$this->sister);
		$criteria->compare('domain',$this->domain);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('created_time',$this->created_time,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_time',$this->updated_time,true);

		if(!empty($this->date_from) && empty($this->date_to))
		{
			$criteria->addCondition('DATE(created_time) >= "'.$this->date_from.'"');
		}
		elseif(!empty($this->date_to) && empty($this->date_from))
		{
			$criteria->addCondition('DATE(created_time)  <= "'.$this->date_to.'"');
		}
		elseif(!empty($this->date_to) && !empty($this->date_from))
		{
			
			$criteria->addCondition('DATE(created_time) BETWEEN "'.$this->date_from.'" AND "'.$this->date_to.'"');
		}
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}