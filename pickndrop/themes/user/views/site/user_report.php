<?php
/* @var $this SeatRequestseatRequestController */
/* @var $dataProvider CActiveDataProvider */

?>
<!-- <style>
    .fs-12{
        font-size: 12px;
    }
</style>
<div id="route-request-list" class="custom-box-design user-report-responsive" style="margin-left: 22px;">
    <div class="center s002 " >

        <h4 class="heading-custom heading-block user-history-heading-text " style="background: transparent !important;" >Sister Concern Services Status</h4>

    </div>

    <div class="table-responsive bottommargin user-report-table-responsive" style="border: none !important; background: transparent !important;">

        <table class="table cart">
            <thead>
                <tr>                    
                    <th class="cart-product-price">User Name</th>
                    <th class="cart-product-price">Sister Concern Name</th>   
                    <th class="cart-product-price">Domian Name</th>         
                    <th class="cart-product-price">Status</th>
                    <!-- <th class="cart-product-price">User Pin</th> -->
                    <!-- <th class="cart-product-price">Date</th> -->
                    <!-- <th class="cart-product-price">Status</th> -->
                    <!--<th class="cart-product-price">Action</th>-->
               <!--  </tr>
            </thead>
            <tbody>
            <?php 
            if (count($sister_user) > 0): ?>
                <?php foreach ($sister_user as $key=> $value): ?>
                    <tr class="cart_item">
                        <td class="cart-product-price">
                            <?php echo $value['username']; ?>
                        </td>
                        <td class="cart-product-price"><?php 
                        $domain = explode('@',$value['email']);        
                        $domain = SisterDomain::model()->findByAttributes(array('domain'=>$domain[1]));
                        echo $domain['sister'];

                        ?>
                        	
                        </td>
                        <td class="cart-product-price">
                            <?php 
                                echo $domain['domain'];
                            ?>
                            
                        </td>
                        <td class="cart-product-price">
                            <?php echo ($value['application_type']=='Cancel Seat') ? 'Inactive':'Active';
                            ?>
                        	
                        </td>                       
                        <td class="cart-product-price"><?php 
                            echo ($value['application_type']=='Cancel Seat') ? $value['cancel_date'] : $value['expected_date'];
                            ?> 
                        	
                        </td>
                      
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7" class="empty"><span class="empty">No results found.</span></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
       
    </div>
</div> -->

<!-- <style type="text/css">
    .status {
        width: 100px;
    }
</style> -->
<?php
/* @var $this SeatRequestAdminController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Seat Request Admin',
);

//$this->menu=array(
//array('label'=>'Create Seat Request Admin', 'url'=>array('create')),
//array('label'=>'Manage Seat Request Admin', 'url'=>array('admin')),
//);
?>
<div id="route-request-list" class="seat-request-admin search-box-responsive">   
    <div class="center s002 searchbar update-form-background2">
    <?php 
         $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>

        <div class="inner-form seat-request-form">
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Name</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="Commonfleets[sister]">

                        <?php
                        echo "<option value='' selected>Select Name </option>";
                        $domain = SisterDomain::model()->findAll();
                        foreach ($domain as $key => $value) {
                            if (isset($_GET['Commonfleets']['sister']) && $_GET['Commonfleets']['sister'] == $value['sister']) {
                                echo "<option value=" . $value['sister'] . " selected>" . $value['sister'] . "</option>";
                            } else {
                                echo "<option value=" . $value['sister'] . ">" . $value['sister'] . "</option>";
                            }
                        }

                        ?>
                    </select>


                </div>
            </div>
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Domain</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z">
                            </path>
                        </svg>
                    </div>
                    <select data-trigger="" name="Commonfleets[domain]">

                        <?php
                        echo "<option value='' selected>Select Domain </option>";             
                       
                        foreach ($domain as $key => $value) {
                            if (isset($_GET['Commonfleets']['domain']) && $_GET['Commonfleets']['domain'] == $value['domain']) {
                                echo "<option value=" . $value['domain'] . " selected>" . $value['domain'] . "</option>";
                            } else {
                                echo "<option value=" . $value['domain'] . ">" . $value['domain'] . "</option>";
                            }
                        }

                        ?>
                    </select>


                </div>
            </div>                     
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Date From</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                       <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                    <?php echo $form->textField($model, 'from_date', array('placeholder' => 'Date Form', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div>
              <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Date To</div>
                <div class="input-field first-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z">
                            </path>
                        </svg>
                    </div>
                   <?php echo $form->textField($model, 'to_date', array('placeholder' => 'Date To', 'id' => 'depart2', 'class' => 'datepicker')); ?>

                </div>
            </div> 
        </div>

        <div class="inner-form seat-request-form">   
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third">Active</div>
                <div class="input-field fouth-wrap seat-request-input-box col_two_third">
                    <div class="icon-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z">
                            </path>
                        </svg>
                    </div>
                    <?php var_dump($_GET['Commonfleets']['cancel']);?>
                    <select data-trigger="" name="Commonfleets[active]">

                        <?php
                        echo "<option value='' selected>Select Status</option>";
                      
                         echo "<option value=1 ". (isset($_GET['Commonfleets']['cancel']) && $_GET['Commonfleets']['cancel'] == 1 ? "selected" :"") ."selected>Active</option>";
                            echo "<option value=2 ". (isset($_GET['Commonfleets']['cancel']) && $_GET['Commonfleets']['cancel'] == 2 ? "selected" :"") .">Inactive</option>";
                            
                       

                        ?>
                    </select>


                </div>
            </div>                 
         
            <div class='col_one_fourth seat-request-form-box'>
                <div class="seat-request-label col_one_third"></div>
                <div class='input-field first-wrap searchbar-search-button col_two_third'>
                    <?php echo CHtml::submitButton("Search", array('class' => 'btn-search', 'id' => 'search-button')); ?>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <div class="center s002 page_header_div page_header_div2 update-form-background2">
            <h4 class="heading-custom page_header_h4">Pick And Drop</h4>
    </div>
    <?php

    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'seat-request-admin-grid',
        'itemsCssClass' => 'table cart',
        'htmlOptions' => array('class' => 'table-responsive table-responsive2 bottommargin table-overflow-x admin-seat-request-table-responsive'),

        'rowCssClass' => array('cart_item'),
        'dataProvider' => $model->pickNDropReport(),
        //'filter' => $model,
        'columns' => array(            
            'user_pin',
            'user_email',
             array(
                'name' => 'sister',
                'type' => 'raw',
                'value' => '$data->sister',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),  
             array(
                'name' => 'domain',
                'type' => 'raw',
                'value' => '$data->domain',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),          
            array(
                'header' => 'Status',
                'type' => 'raw',
                'value' => '$data->application_type=="Cancel Seat" ? "Inactive":"Active"',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
             array(
                'header' => 'Action',
                'type' => 'raw',
                'value' => '$data->application_type=="Cancel Seat" ? date("$data->updated_time") : "$data->expected_date"',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),


          /*  array(
                'header' => 'Photo',
                'type' => 'raw',
                'value' => 'CHtml::image(SeatRequest::model()->photo($data->user_pin))',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/
            /*array(
                'name' => 'user_name',
                'type' => 'raw',
                'value' => '$data->user_name',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'user_department',
                'type' => 'raw',
                'value' => '$data->user_department',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'user_level',
                'type' => 'raw',
                'value' => '$data->user_level',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/

          /*  array(
                'name' => 'user_cell',
                'type' => 'raw',
                'value' => '$data->user_cell',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'email',
                'type' => 'raw',
                'value' => '$data->email',
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/

          /*  array(
                'name' => 'route_id',
                'type' => 'raw',
                'value' => 'SeatRequest::routeDetail($data->route_id,3)',
                'filter' => Stoppage::model()->getAllRouteNo(),
                'htmlOptions' => array('class' => 'cart-product-price', 'title' => 'Route Detail'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'header' => 'Available Seat',
                'type' => 'raw',
                'value' => 'SeatRequest::availableSeat($data->route_id)',
                'filter' => Stoppage::model()->getAllRouteNo(),
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'expected_date',
                'type' => 'raw',
                'value' => '$data->expected_date',

                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(  
                    'header'=>'Vehicle Type', 
                    'type' => 'raw',            
                    'value'=>'SeatRequest::routeDetail($data->route_id,4)',                                           
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
            array(  
                        'header'=>'Bill Amount', 
                        'type' => 'raw',            
                        'value'=>'SeatRequest::routeDetail($data->route_id,5,$data->user_level)',                                           
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),
*/

            //'remarks',

            //'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',
           /* array(
                'name' => 'queue',
                'type' => 'raw',
                'value' => '$data->queueCheck($data->queue,$data->status)',
                'filter' => array('-1' => 'Assigned', '0' => 'Not Approved', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/
            /*array(
                'name' => 'status',
                'type' => 'raw',
                'value' => 'CHtml::dropDownList("$model","$data->status",array("0"=>"Pending","1"=>"Approved","3"=>"Cancel Request","-1"=>"Cancel","4"=>"Route Change Request"),$htmlOptions=array("onChange"=>"status($data->id,$(this).val(),$data->status)","class"=>"status"))',
                'filter' => array('0' => 'Pending', '1' => 'Approved', '-1' => 'Cancel', '3' => 'Cancel For Request', '4' => 'Route Change Request'),
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/
           /* array('name' => 'maternity_leave',
                'type' => 'raw',
                'value' => '($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
                'filter' => array('0' => 'NO', '1' => 'YES'),
                'htmlOptions' => array('class' => 'cart-product-price', 'title' => 'Maternity Seat Use'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),*/

/*
            array(
                'class' => 'CButtonColumn',
                'template' => '{view}{update}',


            ),*/
        ),
    )); ?>
</div>

<div class="s002 export_to_excel_button export_to_excel_button_rout export-to-excel-button-responsive">
    <?php

    echo CHtml::normalizeUrl(CHtml::link('Export to Excel', array('excel', 'criteria' => $_GET['SeatRequestAdmin'])));
    ?>
</div>
<script>
    function status(id, value, preValue) {

        var message = "Do you want to change seat request status for seat request ID# " + id + "?";

        var con = confirm(message);
        if (con === true) {
            jQuery.post('<?php echo $this->createUrl('statusChange');?>', {
                id: id,
                value: value,
                preValue: preValue
            }, function (data) {
                //var size_to_value=JSON.parse(data);
                //alert(data);
                bsModalOpen(data);
            });

        }
        else {
            //$(this).val()=preValue;
            //alert(preValue);
            ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }


    }
</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/flatpickr.js"></script>
<script>flatpickr(".datepicker", {});</script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/extention/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: true,
            itemSelectText: '',

        });

</script>


