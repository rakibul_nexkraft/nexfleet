<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>AOS.init();</script>

<style>
    .list-group .list-group-item:first-child{
        border-top: 0;
        border-left:0;
        border-right: 0;
    }
    .list-group .list-group-item{
        border-left:0;
        border-right: 0;
    }
</style>
<!-- Content
============================================= -->
<section style="background: #f1f1f1">
<section style="margin-bottom: 25px;background: #f1f1f1;margin-top: 10px;">
	 <div class="col_full">
       
        <div class="col-sm-4 col-md-4 user-dashboard-box-container" data-aos="zoom-in-up" data-aos-duration="1000">
            <div class="thumbnail-user use-dashboard-box1 " >
                <div style="background: #6057c4;width: 12px !important;" class="col_one_fourth left-badge-user"></div>
                <div class="caption center circle">
                     <?php echo CHtml::link('<svg class="circle-shadow circle" data-aos="slide-left" data-aos-duration="1000" height="161" width="160">
                            <circle  cx="80" cy="80" r="80" stroke="rgba(53,217,125,0.07)" stroke-width="1" fill="#6057c4" ></circle>
                            <text  x="12" y="90" fill="white" class="user-dashboard-circle">Requisitions</text>
                        </svg>',"https://mydesk.brac.net/fleet/transport/user");
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 user-dashboard-box-container" data-aos="zoom-in-up" data-aos-duration="1000">
            <div class="thumbnail-user use-dashboard-box2"  >
                <div style="background: #49916c; width: 12px !important;" class="col_one_fourth left-badge-user"></div>
                <div class="caption center circle">
                    <?php echo CHtml::link('<svg class="circle-shadow circle" data-aos="slide-left" data-aos-duration="1000" height="161" width="160">
                            <circle cx="80" cy="80" r="80" stroke="rgba(53,217,125,0.07)" stroke-width="1" fill="#49916c"></circle>
                            <text x="12" y="90" fill="white" class="user-dashboard-circle">Pick & Drop</text>
                        </svg>',array('/seatRequest/findRoute')); 
                    ?>

                </div>
            </div>
        </div>
         <div class="col-sm-4 col-md-4 user-dashboard-box-container" data-aos="zoom-in-up" data-aos-duration="1000">
            <div class="thumbnail-user use-dashboard-box3 ">
                <div style="background: #d9366d; width: 12px !important;" class="col_one_fourth left-badge-user"></div>
                <div class="caption center circle" >
                     <?php echo CHtml::link('<svg class="circle-shadow circle" data-aos="slide-left" data-aos-duration="1000" height="161" width="160" >
                            <circle cx="80" cy="80" r="80" stroke="rgba(53,217,125,0.07)" stroke-width="1" fill="#d9366d"></circle>
                            <text x="12" y="90" fill="white" class="user-dashboard-circle">Shohochor</text>
                        </svg>',array('/site/userShohochor')); 
                    ?>
                </div>
            </div>
        </div>
    </div> 
    <div class="clear"></div>


    <div class="section-space">

        <div class="col-sm-4 col-md-4 user-dashboard-chart-container" data-aos="fade-down" data-aos-duration="1000">
            <p class="user-dashboard-chart-label">IFleet Requisition Status</p>
            <div class="col-sm-4 col-md-4" style="margin-top: -45px">
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                    google.charts.load("current", {packages:["corechart"]});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        google.charts.load('current', {packages: ['corechart', 'bar']});
                        google.charts.setOnLoadCallback(drawBasic);

                        function drawBasic() {

                            var data = google.visualization.arrayToDataTable([
                                ['Genre', 'Approved', 'Not Approved', 'Pending', { role: 'annotation' } ],
                                ['08-02-2020', 78, 2, 5, ''],
                                ['09-02-2020', 75, 3, 0, ''],
                                ['10-02-2020', 80, 3, 5, ''],
                                ['11-02-2020', 75, 5, 2, ''],
                                ['12-02-2020', 83, 0, 2, ''],
                            ]);

                            var options = {
                                legend: { position: 'top', maxLines: 3},
                                bar: { groupWidth: '75%' },
                                isStacked: true,
                                allowHtml: true,
                                 width: 400,
                                height:300, // example height, to make the demo charts equal size
                                bar: {groupWidth: "20" },
                                colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
                                backgroundColor: 'transparent',
                                datalessRegionColor: '#37474f',
                                legend: {position: 'top', alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '10'} },

                            };

                            var chart = new google.visualization.ColumnChart(
                                document.getElementById('chart_div1'));

                            chart.draw(data, options);
                        }
                    }
                </script>
                <div id="chart_div1" class="chart_div1"></div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 user-dashboard-chart-container" data-aos="fade-down" data-aos-duration="1000">
            <p class="user-dashboard-chart-label">Vehicle Typewise Occupancy Rate (P&D)</p>
            <div style=" margin-top: -45px">
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                    google.charts.load("current", {packages:["corechart"]});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Task', 'Hours per Day'],
                            ['Bus',     99],
                            ['Car',      77],                           
                            ['Microbus',  129],
                            
                        ]);
                        var options = {
                            pieHole: 0.4,
                            allowHtml: true,
                            height:300, // example height, to make the demo charts equal size
                            width: 500,
                            bar: {groupWidth: "40" },
                            colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
                            backgroundColor: 'transparent',
                            datalessRegionColor: '#37474f',
                            legend: {position: 'center', alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '16'} },
                        };
                        var chart = new google.visualization.PieChart(document.getElementById('donutchart_demo'));
                        chart.draw(data, options);
                    }
                </script>
                <div id="donutchart_demo" class="donutchart"></div>
            </div>
        </div>
        <div class="col-sm-4 col-md-4 user-dashboard-chart-container" data-aos="fade-down" data-aos-duration="1000">
            <p class="user-dashboard-chart-label">Available Seats (P&D)</p>
            <div style="margin-top: -45px">
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                    google.charts.load("current", {packages:["corechart"]});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Task', 'Hours per Day'],
                            ['Bus',     75],
                            ['Car',      12],                         
                            ['Microbus',  50],
                            
                        ]);
                        var cssClassNames = {
                        };
                        var options = {
                            pieHole: 0.4,
                            allowHtml: true,
                            cssClassNames : cssClassNames,
                            height:300, // example height, to make the demo charts equal size
                            width: 500,
                            bar: {groupWidth: "40" },
                            colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
                            backgroundColor: 'transparent',
                            datalessRegionColor: '#37474f',
                            legend: {position: 'center', alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '16'} },
                        };
                        var chart = new google.visualization.PieChart(document.getElementById('donutchart3'));
                        chart.draw(data, options);
                    }
                </script>
                <div id="donutchart3" class="donutchart"></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <section style="margin-bottom: -65px">
        <div class="col_full">
            <div class="col-md-6 item" data-aos="zoom-in-up" data-aos-duration="1000" style="padding-top: 50px;">
                <div class="box-container" style="background: #fff">
                    <div class="box-header ">
                        <p class="page_header_h4"><i class="fa fa-align-left" style="padding-right: 5px"></i> Summary</p>
                    </div>
                    <div class="box-body list-group" style="padding: 2rem 3rem;">

                        <div class="list-item-height">
                            <div class="list-item-line"></div>
                            <div class="list-item-circle"  style="border-color: #00de96"></div>
                            <li class="list-item-style">Requisition No <strong style="font-size: 16px">#420500</strong> has been accepted </li>
                        </div>

                        <div class="list-item-height">
                            <div class="list-item-line"></div>
                            <div class="list-item-circle" style="border-color: #ff7676"></div>
                            <li class="list-item-style">Requisition No  <strong style="font-size: 16px">#329291</strong> has been served </li>
                        </div>

                        <div class="list-item-height">
                            <div class="list-item-line"></div>
                            <div class="list-item-circle" style="border-color: #0497ec"></div>
                            <li class="list-item-style">Vehicle No DM-Cha-22-3333 has completed <strong style="font-size: 16px">629KM</strong> run </li>
                        </div>
<!-- 
                        <div class="list-item-height">
                            <div class="list-item-line"></div>
                            <div class="list-item-circle" style="border:5px solid #80e214"></div>
                            <li class="list-item-style" style="border-bottom: none !important; color: #80e214;">200 vehicle driver</li>
                        </div> -->

                        <div class="list-item-height">
                            <div class="list-item-line"></div>
                            <div class="list-item-circle" style="border-color: #c425a9"></div>
                            <li class="list-item-style"><strong style="font-size: 16px">200</strong> vehicles involved </li>
                        </div>

                        <!-- <div class="list-item-height">
                            <div class="list-item-line"></div>
                            <div class="list-item-circle" style="border:5px solid#fd397a"></div>
                            <li class="list-item-style" style="border-bottom: none !important; color: #fd397a;">2012 iFleet uses year starting</li>
                        </div> -->

                        <!-- <div class="list-item-height">
                            <div class="list-item-line" ></div>
                            <div class="list-item-circle" style="border:5px solid #5d78ff"></div>
                            <li class="list-item-style" style="border-bottom: none !important; color: #5d78ff;"><strong style="font-size: 50px">500</strong> vehicle servicing done till <strong style="font-size: 20px">2019</strong></li>
                        </div> -->

                    </div>
                </div>
            </div>
            <style>
                #chart_div_demo > div > div {
                    margin:0 auto;
                }
            </style>
            <div class="col-md-6" data-aos="zoom-in-up" data-aos-duration="1000"  style="padding-top: 50px; ">
                <div class="box-container" style="background: #fff">
                    <div class="box-header ">
                        <p class="page_header_h4"><i class="fa fa-align-left" style="padding-right: 5px"></i> Trend</p>
                    </div>
                    <div class="box-body list-group" style="padding: 2rem 3rem;">
                        <div id="chart_div_demo" style="height: 248px;"></div>
                    </div>
                </div>
<!--                <div class="user-dashboard-image-container" style="margin-top: 40px;">-->
<!--                    -->
<!--                <p class="user-dashboard-chart-label"></p>-->
<!---->
<!--                    <!-- <img src="< ?php echo Yii::app()->theme->baseUrl; ?>/images/userdashboard.jpg" alt="Abed Sir's image" class="user-dashboard-image">-->
<!--                    <p style="padding:5px; font-size: 12px">Sir Fazle was honored with numerous national and international awards for his contributions in social development, including the LEGO Prize (2018), Laudato Si' Award (2017), Thomas Francis, Jr Medal in Global Public Health (2016), World Food Prize (2015), Spanish Order of Civil Merit (2014), Leo Tolstoy International Gold Medal (2014), WISE Prize for Education (2011) among others.In both 2014 and 2017</p> -->
<!--                </div>-->

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clear"></div>
    </section>
</section>
<section style="background: #f1f1f1; padding-top: 50px;">
	 <div class="col_full">
        <div class="col-md-6" data-aos="zoom-in-up" data-aos-duration="1000" >
            <div class="box-container" style="background: #fff">
                <div class="box-header ">
                    <p class="page_header_h4"><i class="fa fa-bell "></i>Notice</p>
                </div>
                <div class="box-body">
                    <ul class="list-group col_four_fifth" style="padding: 20px; width: 100% !important; line-height: 30px ">
                        <?php
                        foreach($notice as $key => $value) {
                            echo '<li class="list-group-item dashboard-notice-text"><i class=" fa fa-hand-o-right dashboard-notice-icon " ></i> &nbsp  '.$value['notice'].date('Y-m-d', strtotime($value['created_time'])).' </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6" data-aos="zoom-in-up" data-aos-duration="1000" >
            <div class="box-container dashboard-feedback">
                <div class="box-header ">
                    <p class="page_header_h4"><i class="fa fa-edit"></i> Send Feedback</p>
                </div>
                <div class="box-body">
                    
                    <?php   if(isset($_GET['feed_body']) && $_GET['feed_body']==0) { ?>
                      <h6 style="padding: 20px; color: red"> Feedback can not be blank!</h6>
                    <?php } ?>
                  
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id' => 'billing-form',
                        'action' => array('/userFeedback/create'),
                        'enableAjaxValidation' => false,
                        'htmlOptions'=>array('class' => 'nobottommargin'),

                    )); ?>

                    <div class="col_full" style="padding: 20px">
                        <textarea placeholder="Enter your feedback" style="border: 1px solid #c1c1c1;" class="sm-form-control" id="shipping-form-message" name="UserFeedback[feedback]" rows="6" cols="30" required></textarea>
                    </div>
                    <button class="button button-rounded button-pink" style="margin-bottom: 10px;margin-left: 10px; float: right; margin-right: 21px; margin-top: -30px">Submit</button>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div> 
    <div class="clear"></div>
</section>
</section>


    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Requisitions', 'Pick & Drop'],
          ['2013',  1000,      400],
          ['2014',  1170,      460],
          ['2015',  590,       1120],
          ['2016',  700,       1120],
          ['2017',  660,       1120],
          ['2018',  900,      540],
          ['2019',  1030,      540]
        ]);

        var options = {
  hAxis: {
    titleTextStyle: {color: '#607d8b'}, 
    gridlines: { count:0}, 
    textStyle: { color: '#000', fontName: 'Roboto', fontSize: '12', bold: true}
  },
  vAxis: {
    minValue: 0, 
    gridlines: {color:'#c1c1c1', count:4}, 
    baselineColor: 'transparent'
  },
  legend: {position: 'top', alignment: 'center', textStyle: {color:'#607d8b', fontName: 'Roboto', fontSize: '20'} },
  colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4","#009688","#4caf50","#8bc34a","#cddc39"],
  areaOpacity: 0.24,
  lineWidth: 1,
  backgroundColor: 'transparent',
  chartArea: {
    backgroundColor: "transparent",
    width: '80%',
    height: '80%'
  },
      height:248, // example height, to make the demo charts equal size
      width:650,
      pieSliceBorderColor: '#263238',
      pieSliceTextStyle:  {color:'#607d8b' },
      pieHole: 0.9,
      bar: {groupWidth: "40" },
      colorAxis: {colors: ["#3f51b5","#2196f3","#03a9f4","#00bcd4"] },
      backgroundColor: 'transparent',
      datalessRegionColor: '#37474f',
      displayMode: 'regions'
    };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div_demo'));
        chart.draw(data, options);
      }
    </script>



