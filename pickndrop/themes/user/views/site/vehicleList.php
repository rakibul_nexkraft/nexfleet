<div>
    <?php

    $vehicleList = array();
    foreach($vehicles as $item){
        $vehicleList[$item['vehicle_reg_no']] = $item['vehicle_reg_no'];
    }


    $this->widget('ext.ESelect2.ESelect2',array(
        'name' => 'vehicleRegNo',
        'htmlOptions'=>array(
            'style'=>'width:200px',
        ),
        'options' => array(
            'allowClear'=>true,
            'placeholder'=>'Select Vehicle Reg No',
//            'minimumInputLength' => 1,
            'class' => 'js-example-basic-single js-states form-control'
        ),
        'data' => $vehicleList
    ));
    ?>
</div>