<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script>AOS.init();</script>


<style type="text/css">
    #all_route{
        display: none;
    }
    .cell-hover:hover{
        background: rgba(53,217,125,0.1);
    }
    .history-box{
        position: relative;
        border: 1px solid rgba(0,0,0,0.075);
        border-radius: 3px;
        text-align: center;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        background-color: #F5F5F5;
    }
    .history-extended{
        background-color: #FFF;
        text-align: left;
    }
    .history-header{
        /* border-bottom: 1px solid #dedede; */
        padding: 10px 20px;
        /*background-color: #2a57ff;*/
        /*background-image: linear-gradient(45deg, #fc3ba2, blue);*/
        /*box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);*/
        /* width: 95%; */
        font-family: Poppins,Helvetica,sans-serif;
        font-size: 1.2em;
        font-weight: 700;
        border-radius: 5px;
        margin: 0 auto;
        position: relative;
        top: 54px;
        color: white;
        z-index: 1;
    }
    .history-header > h3{
        color: white;
    }
    .tbl-header{
        /*box-shadow: 0 3px 2px -2px rgba(0,0,0,.3); */
        /* border-radius: 5px; */
        /* border: 1px solid #dedede; */
        background-color: #f2f2f2;
    }
    .heading-block:after {
        display: none;
    }
</style>

<!-- <section id="page-title" class="page-title-center page-title-nobg noborder nopadding">
    <div class="col_one_fourth border-custom pick_drop_summery cursor-pointer">
        <div class="col_full heading-block bg-grad-available nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">Route Allocation </br> Request</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm">10000.00</div>
        </div>
    </div>
    <div class="col_one_fourth border-custom">
        <div class="col_full heading-block bg-grad-onduty nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">Today&apos;s Booking</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm"><?php echo $user_data['total_booking'] ?></div>
        </div>
    </div>
    <div class="col_one_fourth border-custom">
        <div class="col_full heading-block bg-grad-pickandrop nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">Today&apos;s Selling</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm"><?php echo $user_data['total_selling'] ?></div>
        </div>
    </div>
    <div class="col_one_fourth col_last border-custom">
        <div class="col_full heading-block bg-grad-workshop nobottommargin center">
            <div class="heading-text text-nowrap text-truncate" style="margin: auto;">iFleet Revenue</div>
        </div>
        <div class="col_full center nomargin">
            <div style="font-weight: 700;font-style: italic;text-shadow: 1px 1px 1px #e1e1e1;font-size: 22px;text-align: center;margin-top: 16px !important;margin-bottom: 16px !important;" class="allmargin-sm">10000.00</div>
        </div>
    </div>
    <div class="clear"></div>
</section>
<section id="all_route">
      <div class="col_full ">
        <label for="" style="font-size: 1.5rem;width:100%" class="route_list center">Route List</label>
        <table class="table cart1">
            
            <tbody>
                
               <?php
               $routes=Routes::model()->findAll();
              // echo $route_number=count($routes);
               $i=0;
               $j=0;
               foreach ($routes as $route){
                 $strlen=strlen($route['route_no']);
                 if($i%10==0||$i==0){
                    $j=1;
                ?>
                 
                    <tr>
                    <?php } ?>
                        <td style="padding: 0px 22px;" class="cell-hover">
                            <a href="javascript:void(0)" role="button" onclick="routeSummery('<?php echo $route['route_no'];?>')">
                                <svg height="50" width="45">
                                   <circle cx="22" cy="26" r="20" stroke="rgba(53,217,125,0.07)" stroke-width="1" fill="rgba(53,217,125,0.7)"></circle>
                                    <text x="<?php echo $r=17-(2*$strlen-1);?>" y="30" fill="white"><?php echo $route['route_no'];?> </text>
                                </svg>
                            </a>
                        </td>
             <?php //$k=($i%10=0+1);
                if((($i%10)+1)%10==0){   $j=0;  ?>
                 
                    </tr>
                    <?php } 
                $i++;
          
               } 
               if($j==0){echo "</tr>";}

               ?>

            </tbody>
        </table>
    </div>
</section> -->
<section class="responsive-section">
    <div class="col_half admin-dashboard-chart-responsive-label">
        <h3 class="center_style" style="display: flex;color: #444;margin-bottom: 0;">
            <span style="flex: 1;color: #444 !important">Number of Seats Sold</span>
            <?php echo CHtml::dropDownList('chartView','',array('1' => 'Weekly', '2' => 'Monthly','3' => 'Quarterly','4' => 'Biyearly','5' => 'Yearly'),array('onChange'=>'chartView(this.value)','class'=>'form-control','style'=>'flex:1')); ?>
        </h3>
        <div class="page_header_div" style="margin-top: 20px;text-align: center;padding: 30px;border-radius: 5px">
            <canvas id="barChartCanvas" class="admin-dashboard-chart-responsive"  style="width:100%;height: 300px"></canvas>
        </div>
    </div>
    <div class="col_half col_last admin-dashboard-marketplace">
        <h3 class="center_style center_style_responsive">Earning From Market Place</h3>

        <div class="col-sm-12 col-md-11" data-aos="slide-down" data-aos-duration="1000">
            <div class="thumbnail thumbnail-box thumbnail-box-background--blue" style="box-shadow: 0 0 3px 1px #796df1 !important;">
                <div class="thumbnail-box-container">
                    <div class="thumbnail-box--earning thumbnail-box--earning-background--blue">10</div>
                    <div class="thumbnail-box--title">iFleet Earnings</div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-11" data-aos="slide-down" data-aos-duration="1000">
            <div class="thumbnail thumbnail-box thumbnail-box-background--red" style="box-shadow: 0 0 3px 1px #fd397a !important;">
                <div class="thumbnail-box-container">
                    <div class="thumbnail-box--earning thumbnail-box--earning-background--red">10</div>
                    <div class="thumbnail-box--title"">User Earnings</div>
                </div>
            </div>
        </div>
        
        
    </div>    
    <div class="clear"></div>
</section>
<!-- <section id="section-about" class="page-section section nobg nomargin nopadding topmargin">

    <div class="heading-block bottommargin-sm center history-header">
        <h3>Top Booking &amp; Selling</h3>
    </div>
    <div class="col_full" style="padding: 20px 20px 0;border: 1px solid #dedede;border-radius: 5px;">
        <div class="col_half topmargin-sm">-->
            <!--<label for="" style="font-size: 1.5rem">Last 5 Booking</label>-->
            <!--<div class="heading-block fancy-title nobottomborder" style="margin-bottom: 10px;"><!--title-bottom-border-->
                <!--<h4>Last 5 Booking</h4>
            </div>
            <table class="table table-responsive table-responsive2 table-hover">
                <thead class="tbl-header">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($recent_booking) == 0){
                    echo
                        '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '<tr>'.
                        '</tr>';
                }
                foreach ($recent_booking as $booking){
                    echo
                        '<tr>'.
                        '<td>'.$booking['release_date'].'</td>'.
                        '<td>'.$booking['route_id'].'</td>'.
                        '<td>'.$booking['travel_direction'].'</td>'.
                        '<td>'.$booking['price'].'</td>'.
                        '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
        <div class="col_half col_last topmargin-sm">
            <div class="heading-block fancy-title nobottomborder" style="margin-bottom: 10px;">
                <h4>Last 5 Selling</h4>
            </div>
            <table class="table table-responsive table-hover">
                <thead class="tbl-header">
                <tr>
                    <th>Date</th>
                    <th>Route</th>
                    <th>Travel Direction</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (count($recent_sell) == 0){
                    echo
                        '<tr>'.
                        '<td colspan="4" style="text-align: left"><i>No data found</i></td>'.
                        '</tr>';
                }
                foreach ($recent_sell as $sell){
                    echo
                        '<tr>'.
                        '<td>'.$sell['release_date'].'</td>'.
                        '<td>'.$sell['route_id'].'</td>'.
                        '<td>'.$sell['travel_direction'].'</td>'.
                        '<td>'.$sell['price'].'</td>'.
                        '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
        <div class="clear"></div>
    </div>
</section> -->
<section id="section-about" class="page-section section nobg nomargin nopadding topmargin admin-dashboard-table" data-aos="fade-in" data-aos-duration="1000">
    <div class="col_full">
        <div class="center s002 page_header_div page_header_div2 update-form-background2 border-round-top notopmargin" style="border-bottom: 1px solid #efefef;">
            <h4 class="heading-custom page_header_h4">Request List</h4>
        </div>
        <div class="col_full">
        <?php 
       $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'seat-request-admin-grid',
        'itemsCssClass' => 'table cart db_table_cart',
        'htmlOptions' => array('class' => 'table-responsive table-responsive2 bottommargin table-overflow-x'),
        
        'rowCssClass'=>array('cart_item'),
        'dataProvider'=>$model->search('pending'),        
        'columns'=>array(      
            'id',
            array(
                    'name'=>'user_pin',
                    'type' => 'raw',
                    'value'=>'CHtml::link(CHtml::encode($data->user_pin), array("/seatRequestAdmin/view", "id"=>$data->id))',
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
            /*array(
                    'header'=>'Photo',
                    'type' => 'raw',
                    'value'=>'CHtml::image(SeatRequest::model()->photo($data->user_pin))',
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),*/
            array(
                    'name'=>'user_name',
                    'type' => 'raw',
                    'value'=>'$data->user_name',
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
            array(
                    'name'=>'user_department',
                    'type' => 'raw',
                    'value'=>'$data->user_department',
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),
            array(
                    'name'=>'user_level',
                    'type' => 'raw',
                    'value'=>'$data->user_level',
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),

          /*  array(
                        'name'=>'user_cell',
                        'type' => 'raw',
                        'value'=>'$data->user_cell',
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),
            array(
                        'name'=>'email',
                        'type' => 'raw',
                        'value'=>'$data->email',
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),*/

            array(
                        'name'=>'route_id',
                        'type' => 'raw',
                        'value'=>'SeatRequest::routeDetail($data->route_id,3)',
                        'filter'=>Stoppage::model()->getAllRouteNo(),
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),
            array(
                        'header'=>'Available Seat',
                        'type' => 'raw',
                        'value'=>'SeatRequest::availableSeat($data->route_id)',
                        'filter'=>Stoppage::model()->getAllRouteNo(),
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),
            array(
                        'name'=>'expected_date',
                        'type' => 'raw',
                        'value'=>'$data->expected_date',

                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),
            array(
                        'header'=>'Vehicle Type',
                        'type' => 'raw',
                        'value'=>'SeatRequest::routeDetail($data->route_id,4)',
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),
            array(
                        'header'=>'Bill Amount',
                        'type' => 'raw',
                        'value'=>'SeatRequest::routeDetail($data->route_id,5,$data->user_level)',
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),

            //'remarks',

            //'created_by',
            //'created_time',
            //'updated_by',
            //'updated_time',
           /* array(
                        'name'=>'queue',
                        'type' => 'raw',
                        'value'=>'$data->queueCheck($data->queue,$data->status)',
                        'filter'=>array('-1'=>'Assigned','0'=>'Not Approved','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5'),
                        'htmlOptions'=>array('class'=>'cart-product-price'),
                        'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                    ),*/
             'name',
            'phone',            
            'reg_no',
            array(
                'name'=>'user_status',
                'type' => 'raw',
                'value'=>'SeatRequest::userStatus($data->user_status)',                        
                'htmlOptions'=>array('class'=>'cart-product-price'),
                'headerHtmlOptions'=>array('class'=>'cart-product-price'),
            ),
            array(
                'name' => 'status',
                'type' => 'raw',
                'value' => '$data->status == 0 ? "Pending" : ($data->status == 1 ? "Approved" : "Cancel")',                
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),
            array(
                'name' => 'action',
                'type' => 'raw',
                'value' => 'CHtml::dropDownList("$model","$data->action",array("0"=>"Pending","1"=>"Approved","-1"=>"Cancel"),$htmlOptions=array("onChange"=>"status($data->id,$(this).val(),$data->status)","class"=>"status"))',                
                'htmlOptions' => array('class' => 'cart-product-price'),
                'headerHtmlOptions' => array('class' => 'cart-product-price'),
            ),

            /*array('name'=>'maternity_leave',
                    'type'=>'raw',
                    'value'=>'($data->maternity_leave==1?"YES":($data->maternity_leave==0?"NO":""))',
                    'filter'=>array('0'=>'NO','1'=>'YES'),
                    'htmlOptions'=>array('class'=>'cart-product-price'),
                    'headerHtmlOptions'=>array('class'=>'cart-product-price'),
                ),*/


           /* array(
                'class'=>'CButtonColumn',
                'template'=>'{view}{update}',
                'buttons'=>array(
                        'update'=>array(
                            'url'=>'Yii::app()->createAbsoluteUrl(
                                "/seatRequestAdmin/update",
                                array("id"=>$data->id)
                            )',
                                ),
                                'view'=>array(
                                    'url'=>'Yii::app()->createAbsoluteUrl(
                                        "/seatRequestAdmin/view",
                                        array("id"=>$data->id)
                                    )',
                                ),
                            ),
                        ),*/
                    ),
                ));
            ?>
            <div class="clear"></div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(".pick_drop_summery" ).click(function() {
            $("#all_route").toggle("slow");

        });
     $(".route_list" ).click(function() {
            $("#all_route").slideUp("slow");

        });
    
   function routeSummery(route){
    $.post('<?php echo Yii::app()->createAbsoluteUrl("/seatRequestAdmin/routeSummery");?>',{route:route},function(data){                   
                            //openModel('<h3>'+data+'</h3>');
                        bsModalOpen('<h3>'+data+'</h3>');
                        
                });
    
   }
var label1, data1;
var Chart1 = function(v){
    if(v==1){
        label1 = ["1st Day","2nd Day","3rd Day","4th Day","5th Day","6th Day","7th Day"];
        data1  = [65,59,90,81,56,55,50];
    }
    if(v==2){
        label1 = ["1st Week","2nd Week","3rd Week","4th Week","5th Week"];
        data1  = [65,59,90,81,56];
    }
    if(v==3){
        label1 = ["1st Quarter","2nd Quarter","3rd Quarter","4th Quarter"];
        data1  = [65,59,90,81];
    }
    if(v==4){
        label1 = ["January","February","March","April","May","June"];
        data1  = [65,59,90,81,56,90];
    }
     if(v==5){
        label1 = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        data1  = [65,59,90,81,56,90,65,59,90,81,56,90];
    }
    var barChartData = {
        labels : label1 ,
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,1)",
                data : data1 
            }
        ]

    };       
                      
    var globalGraphSettings = {animation : Modernizr.canvas};

    function showBarChart(){
        var ctx='';
        var ctx = document.getElementById("barChartCanvas").getContext("2d");
        new Chart(ctx).Bar(barChartData,globalGraphSettings);
     
    }                     

 showBarChart();
};
Chart1(1);

function chartView(v){
    $('#barChartCanvas').replaceWith('<canvas id="barChartCanvas" width="400" height="400"></canvas>');
    Chart1(v);
}
</script>


<script>
    function status(id,value,preValue){
        
        var message ="Do you want to change seat request status for seat request ID# "+id+"?";
    
        var con=confirm(message);
        if(con===true){
            showDataProcess(true);
            jQuery.post('<?php echo $this->createUrl('/seatRequestAdmin/statusChange');?>',{id:id,value:value,preValue:preValue},function(data){
                $('#myModal').css('display','none');
                //var size_to_value=JSON.parse(data);
                //alert(data);
                //openModel(data);
                showDataProcess(false);
                 bsModalOpen(data);
            });
   
        }
        else{
            //$(this).val()=preValue;
            //alert(preValue);
             ///$(this).val($(this).data(preValue));
            // alert($(this).val());
        }
        

    }
</script>