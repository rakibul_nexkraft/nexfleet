<div class="center s002 ">
<!--  <div class="col_full .col_last">
    <h4 class="heading-custom"><a href="http://sso.brac.net/"> New Requsition</a></h4>
  </div>-->
    <!-- Floating Menu Button -->
    <div class="dropdown">
        <div id="dropdownFloatingButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="floating-menu dropdown-toggle animated fadeInRight">
            <!--                <i class="floating-menu-label vs-icon notranslate icon-scale vs-button--icon  feather icon-settings null"></i>-->
            <p class="floating-menu-label"><i class="floating-menu-label fa fa-gear fa-spin floating-menu-icon"></i> Menu</p>
        </div>
        <div class="floating-popup dropdown-menu" aria-labelledby="dropdownFloatingButton">
            <div class="col_full .col_last">
                <a href="http://sso.brac.net/"> New Requsition</a>
            </div>
        </div>
    </div>
</div>
<div class="col_full">
  <div class="col_half col-responsive">
      <div class="box-container responsive-box-container" style="background: #fff">
          <div class="box-header">
              <p class="page_header_h4"><i class="fa fa-line-chart"></i> iFleet Requisition Status</p>
          </div>
          <div class="box-body">
              <div id="i_requisition" class="responsive-chart" style="width: 500px; height: 400px;"></div>
          </div>
      </div>
  </div>
  <div class="col_half col-responsive col_last">
      <div class="box-container responsive-box-container" style="background: #fff">
          <div class="box-header">
              <p class="page_header_h4"><i class="fa fa-line-chart"></i> My Requisition Status</p>
          </div>
          <div class="box-body">
              <div id="m_requisition" class="responsive-chart" style="width: 500px; height: 400px;"></div>
          </div>
      </div>
  </div>    
  <div class="clear"></div>
</div>

<div class="col_full custom-box-design table-cantainer" >
	<div class="heading-block bottommargin-sm center history-header ">
    <h3 style="padding-top: 15px">Your Last Five Approved Requisitions</h3>
  </div>
  <div class="col_full responsive-table" style="padding: 20px 20px 0;">
    <table class="table table-approved-req table-responsive table-hover">
      <thead class="tbl-header">
                <tr>
                    <th>ID</th>
                    <th>Vehicle Reg No</th>
                    <th>Start Point</th>
                    <th>Start Point</th>
                    <th>Start Date & Time</th>
                    <th>End Date & Time</th>                    
                </tr>
                </thead>
                <tbody>
                <?php
                //var_dump($u_req_app_five);
                if (count($u_req_app_five) <= 0){
                    echo
                        '<td colspan="7" style="text-align: left"><i>No data found</i></td>'.
                        '<tr>'.
                        '</tr>';
                }
                foreach ($u_req_app_five as $value){
                    echo
                        '<tr>'.
                        '<td>'.$value['id'].'</td>'.
                        '<td>'.$value['vehicle_reg_no'].'</td>'.
                        '<td>'.$value['start_point'].'</td>'.
                        '<td>'.$value['end_point'].'</td>'.
                        '<td>'.$value['start_date'].'</br>'.$value['start_time'].'</td>'.
                        '<td>'.$value['end_date'].'</br>'.$value['end_time'].'</td>'.
                        '</tr>';
                } ?>
                </tbody>
            </table>
        </div>
<div class="clear"></div>
</div>
 <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Approved',     <?php echo $pichar_array['i_req_app'];?>],
          ['Not Approved', <?php echo $pichar_array['i_req_n_app'];?>],
          ['Pending',  <?php echo $pichar_array['i_req_p_app'];?>],
          
        ]);

        var options = {
          title: '',
          is3D: true,
          colors: ['#FF33FC', '#e6693e', '#4F0F4E'],
        };

        var chart = new google.visualization.PieChart(document.getElementById('i_requisition'));
        chart.draw(data, options);
      }
      
      google.charts.setOnLoadCallback(drawChart1);
      function drawChart1() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Approved',     <?php echo $pichar_array['u_req_app'];?>],
          ['Not Approved',      <?php echo $pichar_array['u_req_n_app'];?>],
          ['Pending',  <?php echo $pichar_array['u_req_p_app'];?>],
          
        ]);



        var options = {
          title: '',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('m_requisition'));
        chart.draw(data, options);
      }
    </script>
</script>



