<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />


    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/demoStyle.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsiveUser.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/customStyle.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
     <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" rel="stylesheet" />
     <!--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">-->
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"/>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/toastr.css" type="text/css" />

    <!-- External JavaScripts
    ============================================= -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/toastr.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/echarts.min.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>AOS.init();</script>



    <!-- Document Title
    ============================================= -->
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style>
        .margin-rl-auto{
            margin: 0 auto !important;
        }

        .revo-slider-emphasis-text {
            font-size: 58px;
            font-weight: 700;
            letter-spacing: 1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }

        .tp-video-play-button { display: none !important; }

        .tp-caption { white-space: nowrap; }
        .right-margin{
            margin-right:20px; 
        }
    </style>

</head>

<body class="stretched">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix" style="background: #f1f1f1; overflow: hidden">

        <!-- Header
		============================================= -->
        <header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo" style="border: none; display: flex">
                        <a href="#" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/brac-logo-big.png" alt="iFleet Logo"></a>
                        <a href="#" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/brac-logo-big.png" alt="iFleet Logo"></a>

<!--                        <div class="" style="padding-left: 350px; padding-top: 10px;">
                            <h3>BRAC Fleet Management</h3>
                            <p class="subtitle-user">Dashboard</p>
                            <div style="width: 70%; height: 1px; background: #000; margin-top: -58px; margin-left: 40px;"></div>
                        </div>-->
                    </div><!-- #logo end -->
                     <!-- Logout and user info
                    ============================================= -->
                    <style type="text/css">

                    #header.sticky-header:not(.static-sticky) #top-search1{
                            margin: 9px 0px 9px 50px;
                        }
                     #header.sticky-header:not(.static-sticky) #top-search1 .left-margin-admin{
                             padding: 2px;
                        }
                    .left-margin-admin{
                            margin-left: 20px;
                    }
                    
                    </style>
                      <div id="top-search1" class="header-text">
                        Welcome To BRAC Fleet Management
                                  
                        <?php 
                            if(isset(Yii::app()->user->username) && !empty(Yii::app()->user->username)) { 
                                $user_info = User::model()->findByAttributes(array('username'=>Yii::app()->user->username,'picmarket_user'=>0,'status'=>1));
                                if(!empty($user_info) && $user_info['superuser']=='1') {                             
                        ?>
                        <span class="left-margin-admin"> 
                        <?php
                                echo CHtml::link('Admin Panel',array("/site/adminDashboard"),array('class'=>'button button-pink button-mini button-rounded', 'style'=>"color: white !important;"));
                        ?>
                        </span>
                        <?php   } 
                                elseif(!empty($user_info) && $user_info['superuser']=='0') {
                        ?>
                        <span class="left-margin-admin"> 
                        <?php
                                echo CHtml::link('iFleet Application',substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,'pickndrop')).'index.php/user/login?pin='.Yii::app()->user->username.'&fleet=1',array('class'=>'button button-pink button-mini button-rounded', 'style'=>"color: white !important;"));
                        ?>
                        </span>
                        <?php
                                } else{}
                            }  
                        ?> 
                        
                     </div> 
                     <div id="top-search" class="top-search">
                        <?php if(Yii::app()->user->name!='Guest') { 
                            Yii::app()->user->picmarket_user=1;
                            ?>                        
                        <span class="right-margin">           
                        <?php echo Yii::app()->user->firstname . " " .Yii::app()->user->lastname; ?> &nbsp;&nbsp;|
                        </span>
                        <?php if(isset(Yii::app()->user->department)&& (Yii::app()->user->department)) { ?>
                        <span class="right-margin">                     
                        <?php echo Yii::app()->user->department;?> &nbsp;&nbsp;|
                        </span>
                        <?php } //else echo "Unknown"; ?>
                        <?php if(isset(Yii::app()->user->designation)&& (Yii::app()->user->designation)) { ?>
                        <span class="right-margin">
                        <?php echo Yii::app()->user->designation;?> &nbsp;&nbsp;|
                        </span>
                        <?php } //else echo "Unknown"; ?>
                        <?php
                            echo CHtml::link('<i class="icon-lock2"></i>',array('/user/logout'));
                        }
                        ?>
                    </div><!-- #logout end -->
                    
                </div>

            </div>

        </header><!-- #header end -->

        <!-- Header
		============================================= -->
        <!--<header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <div id="logo">
                        <a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="< ?php echo Yii::app()->theme->baseUrl; ?>/images/brac-logo-big.png" alt="iFleet Logo"></a>
                        <a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="< ?php echo Yii::app()->theme->baseUrl; ?>/images/brac-logo-big.png" alt="iFleet Logo"></a>
                    </div>
                    <nav id="primary-menu">

                        <ul>
                            <li class="current"><a href="index.html"><div>Home</div></a></li>
                            <li class=""><a href="index.html"><div>Service</div></a></li>
                            <li class=""><a href="index.html"><div>Reservation</div></a></li>
                            <li class=""><a href="index.html"><div>Marketplace</div></a></li>
                        </ul>

                        <div id="top-cart">
                            <a href="#" id="top-cart-trigger"></a>
                        </div>

                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div>

                    </nav>

                </div>

            </div>

        </header>-->

        <!-- Content
        ============================================= -->
        <?php echo $content;?><!-- #content end -->

        <!-- Footer
		============================================= -->
        <footer class="dark" style="background: url('<?php echo Yii::app()->theme->baseUrl; ?>/images/footer-bg.jpg') repeat fixed; background-size: 100% 100%;">
            <div class="container-fluid">
                <div class="footer-widgets-wrap clearfix" style="padding: 20px 10px 0;">
                    <div class="row clearfix" id="footerSection" style="margin-top: 0;padding-top: 0;">
                        <div class="col-md-offset-1 col-sm-offset-1 col-md-3 col-sm-3 txt-left">
                            <div class="widget clearfix">
                                <h4 style="font-size: 2rem">iFleet</h4>
                                <div style="background: url('<?php echo Yii::app()->theme->baseUrl; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
                                    <address>
                                        <strong>Headquarters:</strong><br>
                                        BRAC Center<br>
                                        Mohakhali, Dhaka<br>
                                    </address>
                                </div>
                            </div>
                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                        </div>
                        <div class="col-md-offset-1 col-sm-offset-1 col-md-3 col-sm-3 txt-center">
                            <div class="widget widget_links clearfix">
                                <h4 style="font-size: 2rem">Important Links</h4>
                                <ul>
                                    <li><?php echo CHtml::link('Requisitions',array('/site/userRequisitions'));?></li>
                                    <li><?php echo CHtml::link('Pick & Drop',array('seatRequest/findRoute'));?></li>
                                    <li><?php echo CHtml::link('Billing',array('transactionHistory/userBlance'));?></li>
                                </ul>
                            </div>
                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                        </div>
                        <div class="col-md-offset-1 col-sm-offset-1 col-md-3 col-sm-3 txt-right">
                            <div class="widget clearfix">
                                <h4 style="font-size: 2rem">Any query? Contact us</h4>
                                <div>
                                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> <a class="br-link" href="tel:+8801730374291"> (88) 017 303 74291</a><br>
                                    <!--<abbr title="Fax"><strong>Fax:</strong></abbr> (88) XX YYYY XYXY<br>-->
                                    <abbr title="Email Address"><strong>Email:</strong></abbr> <a class="br-link" href="mailto:mehedi.he@brac.net"> mehedi.he@brac.net</a>
                                </div>
                            </div>
                        </div>
                    </div>




                    <!--<div class="col_one_third">
                        <div class="widget clearfix">
                            <h4 style="font-size: 2rem">iFleet</h4>
                            <div style="background: url('< ?php echo Yii::app()->theme->baseUrl; ?>/images/world-map.png') no-repeat center center; background-size: 100%;">
                                <address>
                                    <strong>Headquarters:</strong><br>
                                    BRAC Center<br>
                                    Mohakhali, Dhaka<br>
                                </address>
                            </div>
                        </div>
                    </div>
                    <div class="col_one_third">
                        <div class="widget widget_links clearfix">
                            <h4 style="font-size: 2rem">Important Links</h4>
                            <ul>
                                <li>< ?php echo CHtml::link('Requisitions',array('/site/userRequisitions'));?></li>
                                <li>< ?php echo CHtml::link('Pick & Drop',array('seatRequest/findRoute'));?></li>
                                <li>< ?php echo CHtml::link('Shohochor',array('/site/userShohochor'));?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col_one_third col_last">
                        <div class="widget clearfix">
                            <h4 style="font-size: 2rem">Any query? Contact us</h4>
                            <div>
                                <abbr title="Phone Number"><strong>Phone:</strong></abbr> (88) 017 303 74291<br>
                                <abbr title="Fax"><strong>Fax:</strong></abbr> (88) XX YYYY XYXY<br>
                                <abbr title="Email Address"><strong>Email:</strong></abbr> mehedi.he@brac.net
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
            <div class="notopborder" style="background: #2d333a;">
                <div>
                    <div class="container-fluid">
                        <div class="footer-widgets-wrap clearfix" style="padding: 0">
                            <div class="row clearfix" id="footerSection" style="margin-top: 0;padding-top: 0;">
                                <!--<div class="col-md-offset-1 col-sm-offset-1 col-md-3 col-sm-3 txt-left">
                                    <div class="widget clearfix">
                                        <div class="clear-bottommargin-sm">
                                            <div class="row clearfix">
                                                <div class="col-md-12">
                                                    <div class="copyrights-menu copyright-links clearfix" style="margin-top: 6px;margin-bottom: 6px">
                                                        <a class="br-link" href="tel:+8801730374291"><i class="fa fa-phone-square" style="color: #ee2c9e;"></i> (88) 017 303 74291</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="visible-sm visible-xs bottommargin-sm"></div>
                                </div>-->
                                <div class="col-md-offset-1 col-sm-offset-1 col-md-5 col-sm-5 txt-center">
                                    <div class="widget clearfix">
                                        <div class="clear-bottommargin-sm">
                                            <div class="row clearfix">
                                                <div class="col-md-12">
                                                    <?php $year = date('Y'); ?>
                                                    <div class="copyrights-menu copyright-links clearfix" style="margin-top: 6px;margin-bottom: 6px">
                                                        &copy; <?php echo $year;?>. All Rights Reserved by <span style="color: #ee2c9e;">iFleet</span>.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="visible-sm visible-xs bottommargin-sm"></div>
                                </div>
                                <div class="col-md-offset-3 col-sm-offset-3 col-md-3 col-sm-3 txt-right">
                                    <div class="copyrights-menu copyright-links" style="margin-top: 6px;margin-bottom: 6px;">
                                        Made with
                                        <svg width="14" height="14" viewBox="0 -2 24 24">
                                            <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" fill="red"></path>
                                        </svg>
                                        by<a class="link" href="http://www.nexkraft.com" style="color: #fff !important;">NexKraft Limited</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer><!-- #footer end -->

        <!-- Go To Top
        ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>


    <!-- Footer Scripts
    ============================================= -->


    <!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js"></script>

</body>
</html>