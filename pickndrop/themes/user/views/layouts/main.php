
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	

	<!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/style.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/responsiveUser.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/customStyle.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/demoStyle.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" rel="stylesheet" />
	 <!--<link rel="icon" href="demo_icon.gif" type="image/gif" sizes="16x16">-->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/plugins.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>AOS.init();</script>
   

	<!-- Document Title
	============================================= -->
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style>
        .margin-rl-auto{
            margin: 0 auto !important;
        }

        .sidepanel-close-btn{
            display: block !important;
            position: absolute;
            z-index: 12;
            top: 0;
            left: auto;
            right: 0;
            width: 40px;
            height: 40px;
            font-size: 18px;
            line-height: 40px;
            text-align: center;
            background-color: rgba(0,0,0,0.1);
            border-radius: 0 0 0 2px;
            color: white;
        }
        .nav-tree a{
            padding-left: 20px !important;
        }
        .nav-tree li{
            margin-bottom: 10px;
        }
        .nav-tree li.active{
            border: 1px solid #1ABC9C !important;
            border-radius: 3px;
        }
        .right-margin{
            margin-right:20px; 
        }
        .body-overlay_custom{
            opacity: 0;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgb(0, 0, 0);
            z-index: 1100;
            pointer-events: none;
            -webkit-backface-visibility: hidden;
            -webkit-transition: opacity .2s ease;
            -o-transition: opacity .2s ease;
            transition: opacity .2s ease;
        }
        .spin-icon{
            -webkit-animation: spin 1.5s linear infinite;
            animation: spin 1.5s linear infinite;
        }
    </style>

</head>

<body class="stretched side-panel-left">
     
    <div class="body-overlay"></div>
    <div class="body-overlay_custom">
        <div style="display: flex;height: 100%;align-items: center;justify-content: center;">
            <i class="fa fa-cog spin-icon" style="color: #ff008a;font-size: 20px;line-height: 20px;"></i>
        </div>
    </div>

    <div id="side-panel" class="dark">

        <div id="side-panel-trigger-close" class="side-panel-trigger"><a class="sidepanel-close-btn" href="#"><i class="icon-line-cross"></i></a></div>

        <div class="side-panel-wrap">

            <div class="widget clearfix">

                <h4 style="margin-left: 20px">Pages</h4>

                <nav class="nav-tree nobottommargin">
                    <?php
                    $this->widget('zii.widgets.CMenu', array(
                        'items'=>array(
                            array('label'=>'<i class="icon-home"></i>Home', 'url'=>array('site/test'), 'linkOptions'=>array(), 'visible'=>!Yii::app()->user->isGuest,),
                            array('label'=>'<i class="icon-wallet"></i>Pick &amp; Drop', 'url'=>array('site/index'), 'visible'=>!Yii::app()->user->isGuest),
                            array('label'=>'<i class="icon-briefcase"></i>Marketplace', 'url'=>substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop")).'marketplace/index.php', 'visible'=>!Yii::app()->user->isGuest),
                        ),
                        'encodeLabel' => false,
                        'htmlOptions'=>array('class'=>''),
                        'activeCssClass'=>'active',
                    ));
                    ?>
                </nav>

            </div>           

        </div>

    </div>

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix margin-rl-auto" >

        <style>
            .landing-form-overlay{position: relative}

            /* Profile sidebar */
            .profile-sidebar {
                padding: 20px 0 10px 0;
                background: #fff;
            }
            .profile-section-container{
                /*background: #394d61;*/
                background: #fff;
                color: #555;
            }

            .profile-img-box-flex{
                padding: 10px 0;
            }
            .profile-img-box{
                height: 150px;
                width: 150px;
                display: flex;
                margin: 0 auto;
            }

            .profile-img-box img{
                -webkit-border-radius: 50% !important;
                -moz-border-radius: 50% !important;
                border-radius: 50% !important;
            }

            /*.profile-userpic img {
                float: none;
                margin: 0 auto;
                width: 50%;
                height: 50%;
                -webkit-border-radius: 50% !important;
                -moz-border-radius: 50% !important;
                border-radius: 50% !important;
            }*/

            .profile-usertitle {
                /*text-align: center;*/
            }

            .profile-usertitle-name {
                color: #0b3e7e;
                font-size: 18px;
                font-weight: 600;
                margin-bottom: 7px;
                position: relative;
                padding: 6px 0;
            }
            .profile-usertitle-name a{
                color: #555 !important;
            }
            .profile-usertitle-name a:hover{
                color: #4b81e1;
            }

            .profile-usertitle-job {
                text-transform: uppercase;
                color: #555;
                font-size: 12px;
                font-weight: 600;
                margin-bottom: 15px;
            }
            .profile-usertitle .table > tbody > tr:first-child > th, .profile-usertitle .table > tbody > tr:first-child > td {
                /*background: #aaaaaa2b;*/
                background: #fff1fa;
            }
            .profile-usertitle .table > tbody > tr:nth-child(2) > th, .profile-usertitle .table > tbody > tr:nth-child(2) > td {
                /*background: #a4a4a430;*/
                background: #fff1fa;
            }
            .profile-usertitle .table > tbody > tr:nth-child(3) > th, .profile-usertitle .table > tbody > tr:nth-child(3) > td {
                /*background: #a0a0a036;*/
                background: #fff1fa;
            }
            .profile-usertitle .table > tbody > tr:nth-child(4) > th, .profile-usertitle .table > tbody > tr:nth-child(4) > td {
                /*background: #a0a0a036;*/
                background: #fff1fa;
            }

            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                padding: 12px 12px;
                border: 0;
            }

            .profile-usertitle-name a#profile-edit-btn span{
                display: none;
                position: absolute;
                right: -16px;
                top: 0;
                margin-right: 20px;
            }
            .profile-usertitle-name:hover a#profile-edit-btn span{
                display: block;
            }
            .cart-product-price img{
                width: 28px;
                margin: -6px auto;
            }
            .driver-info-row img{
                width: 40%;
            }

            @media (max-width: 991px) {
                #side-panel-trigger {
                    right: 16px;
                }
                #logo{
                    text-align: left;
                }
            }
            .txt{
                position: relative;
                float: left;
                font-family: 'Raleway', sans-serif;
                font-size: 36px;
                line-height: 100%;
                margin-right: 40px;
            }
            .icon-zone{
                position: relative;
                top: -2px;
                margin-right: 6px;
                text-align: center;
                width: 14px;
            }
            .profile-img-square {
                padding: 4px;
                background: #fff5fc;
                box-shadow: 0 0 4px rgba(0, 0, 0, 0.22);
            }
            .profile-img-square img {
                border-radius: 2px;
            }
        </style>

        <!-- Header
		============================================= -->
        <header id="header">

            <div id="header-wrap">

                <div class="container-fluid clearfix">

                    <!--<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>-->

                    <!-- Logo
                    ============================================= -->
                    <div id="logo" style="display: flex;align-items: center;">

                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="<?php echo Yii::app()->baseUrl; ?>/../common/assets/images/brac-logo-big.png" alt="BRAC Logo"></a>
                        <a href="<?php echo Yii::app()->baseUrl; ?>" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="<?php echo Yii::app()->baseUrl; ?>/../common/assets/images/ifleet-logo-2.png" alt="BRAC Logo"></a>
<!--                        <span class="txt" style="font-size: 38px;color: #868686;font-weight:bold;margin-left: 10px;">iFleet</span>-->


                    </div><!-- #logo end -->
                    <!-- Logout and user info
                    ============================================= -->
                     <div id="top-search" class="top-search">
                        <?php if(Yii::app()->user->name!='Guest') { ?>
                        <span class="right-margin">
                        <?php echo Yii::app()->user->firstname . " " .Yii::app()->user->lastname; ?>
                        </span>
                            <span class="right-margin">|</span>
                        <?php if(isset(Yii::app()->user->department)&& (Yii::app()->user->department)) { ?>
                        <span class="right-margin">
                        <?php echo Yii::app()->user->department;?>
                        </span>
                                <span class="right-margin">|</span>
                        <?php } //else echo "Unknown"; ?>
                        <?php if(isset(Yii::app()->user->designation)&& (Yii::app()->user->designation)) { ?>
                        <span class="right-margin">
                        <?php echo Yii::app()->user->designation;?>
                        </span>
                                <span class="right-margin">|</span>
                        <?php } //else echo "Unknown"; ?>
                        <?php
                            echo CHtml::link('<i class="icon-lock2"></i>',array('/user/logout'));
                        }
                        ?>

                    </div><!-- #logout end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <!--<nav id="primary-menu">
                        <div id="side-panel-trigger" class="side-panel-trigger">
                            <a href="#" style="font-size: 2.5rem;">
                                <i class="icon-reorder"></i>
                            </a>
                        </div>
                    </nav><!-- #primary-menu end -->
                </div>
            </div>

        </header><!-- #header end -->
		<div class="clearfix" ></div>

	    <?php if (Yii::app()->user->name!='Guest' && isset(Yii::app()->user->picmarket_user) && Yii::app()->user->picmarket_user==1): ?>
		<!-- Content
		============================================= -->
		<section id="content">

            <div class="container-fluid clearfix" style="background: #f1f1f1">

                <div id="side-navigation">

                    <div class="col_one_fifth left-side-bar" style="box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03), 0 0.9375rem 1.40625rem rgba(4,9,20,0.03), 0 0.25rem 0.53125rem rgba(4,9,20,0.05), 0 0.125rem 0.1875rem rgba(4,9,20,0.03);">

                        <div class="profile-section-container">
                            <div class="profile-userpic profile-img-box-flex">
                                <div class="profile-img-square">
                                    <!--<img src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/magazine/thumb/person1_web.jpg" style="" class="img-responsive" alt="User image">-->
                                    <?php
                                    $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/".Yii::app()->user->username.".png";
                                    if (!file_exists("../photo/".Yii::app()->user->username.".png")) {
                                        $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/"."person1_web.jpg";
                                    }
                                    ?>
                                    <img src="<?php echo $image_url; ?>" style="" class="img-responsive" alt="User image">
                                </div>
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">
                                    <a href="<?php echo Yii::app()->createUrl("/user/profile"); ?>"><?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname;?></a>
                                    <?php if(Yii::app()->user->myDeskUser==0){


                                        ?>
                                    <a id="profile-edit-btn" href="<?php echo Yii::app()->createUrl("/user/profile/edit"); ?>"><span><i class="icon-edit"></i></span></a>
                                    <?php } ?>
                                </div>
                                <table class="table table-user-menu  table-responsive margin-top" style="border: 0;">
                                    <tr>
                                        <th class="profile-usertitle-job">Phone</th>
                                        <td><?php echo Yii::app()->user->phone;?></td>
                                    </tr>
                                    <tr>
                                        <th class="profile-usertitle-job">Pin</th>
                                        <td><?php echo Yii::app()->user->username;?></td>
                                    </tr>
                                    <tr>
                                        <th class="profile-usertitle-job">Route No.</th>
                                        <td><?php   $r = Yii::app()->db->createCommand("SELECT * FROM tbl_commonfleets WHERE user_pin ='".Yii::app()->user->username."' AND application_type<>'Cancel Seat'")->queryRow();
                                            echo $r = !empty($r) ? $r['present_route'] : "Not Approve";
                                        ?></td>
                                    </tr>

                                    <tr>
                                        <th class="profile-usertitle-job">Balance &nbsp; &nbsp;<?php echo CHtml::link('<i class="fa fa-plus" style="font-size: 12px; color: #ec0e8c;"></i>',array('/transactionHistory/userBlance'));?>

                                        </th>
                                        <td><?php 
                                            $b = Yii::app()->db->createCommand("SELECT * FROM tbl_transaction_history WHERE user_pin = '".Yii::app()->user->username."' ORDER BY id DESC")->queryRow();
                                            echo $balance = empty($b) ? 0.0 : $b['balance'];

                                        ?></td>
                                        
                                    </tr>
                                    

                                </table>
                            </div>
                        </div>


                        <div class="" >
                            <div class="menubox menu_background">
                            <?php $this->widget('zii.widgets.CMenu',array(
                                'items'=>array(
                                    array('label'=>'<i class="fa fa-home user-icon-style"></i> Home', 'url'=>array('/'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    array('label'=>'<i class="icon-screen user-icon-style" ></i> My Dashboard', 'url'=>array('/site/userSummary'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    array('label'=>'<i class="fa fa-map-marker user-icon-style"></i>Pick And Drop', 'url'=>array('/seatRequest/findRoute'),                                     'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    array('label'=>'<i class=" 	fa fa-user user-icon-style"></i>Shohochor', 'url'=>array('/site/userShohochor'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),
                                    array('label'=>'<i class="fa fa-calendar-o user-icon-style"></i>Leave', 'url'=>array('maternityLeaveRequest/leave'),
                                        'visible'=>!Yii::app()->user->isGuest,
                                    ),                                 
                                    array('label'=>' <i class="fa fa-history user-icon-style "></i>History<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<i class="fa fa-truck "></i> Route Request List', 'url'=>array('/seatRequest/requestList')),
                                            array('label'=>'<i class="fa fa-list"></i> Sell History', 'url'=>array('seatSale/saleHistory')),
                                            array('label'=>'<i class="fa fa-list"></i> Booking History', 'url'=>array('seatBooking/bookingHistory')),
                                        )
                                    ),                                   
                                ),
                                'encodeLabel' => false,
                                'htmlOptions'=>array('class'=>'left-url'),
                                'submenuHtmlOptions' => array(
                                    'class' => 'cursor-pointer',
                                ),
                                'activeCssClass'=>'ui-tabs-active',
                            ));?>
                        </div>
                        </div>
                    </div>

                    <div class="col_four_fifth col_last middle_margin">
                        <div class="topmargin-sm">
                            <div id="snav-content">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- The Modal -->
                <div class="modal" id="myModal">
                        <div class="modal-dialog modal-dialog-scrollable">
                          <div class="modal-content">

                            <!-- Modal Header -->
                           <!--  <div class="modal-header">
                              <h1 class="modal-title"></h1>
                              <?php //echo CHtml::button('x',array('data-dismiss'=>'modal','class'=>'close btn btn-success','onClick'=>'closedModel()'));
                                ?>
                              <div class="clearfix"></div>
                            </div> -->

                            <!-- Modal body -->
                            <div class="modal-body">
                              <h3>Some text to enable scrolling..</h3>


                            </div>
                            <div class="clearfix"></div>
                            <!-- Modal footer -->
                            <!-- <div class="modal-footer">
                                <?php //echo CHtml::button('Close',array('data-dismiss'=>'modal','class'=>'btn btn-success','onClick'=>'closedModel()'));
                                ?>

                            </div> -->

                          </div>
                        </div>
                      </div>
                <!-- End Model-->
            </div>
		</section><!-- #content end -->

        <!--ADMIN USER-->
        <?php elseif (Yii::app()->user->name!='Guest' && isset(Yii::app()->user->picmarket_user) && Yii::app()->user->picmarket_user!='1'):

        ?>
		<!-- Content
		============================================= -->
		<section id="content">

            <div class="container-fluid clearfix"  style="background: #f1f1f1">
                <div id="side-navigation" >
                    <!--<div class=''><button onClick="hideMenu()">Hide Menu</button></div>-->
                    <div class="col_one_fifth left-side-bar" >
                        <div class="" >
                            <div class="profile-userpic profile-img-box-flex">
                                <!-- <div class="profile-img-box" > -->
                                <!-- <div class="profile-img-square" >
                                    <img src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/magazine/thumb/person1_web.jpg" style="" class="img-responsive" alt="User image">

                                </div> -->
                                 <div class="profile-img-square">
                                    <!--<img src="<?php //echo Yii::app()->theme->baseUrl; ?>/images/magazine/thumb/person1_web.jpg" style="" class="img-responsive" alt="User image">-->
                                    <?php
                                    $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/".Yii::app()->user->username.".png";
                                    if (!file_exists("../photo/".Yii::app()->user->username.".png")) {
                                        $image_url=substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop"))."photo/"."person1_web.jpg";
                                    }
                                    ?>
                                    <img src="<?php echo $image_url; ?>" style="" class="img-responsive" alt="User image">
                                </div>
                            </div>
                            <div class="profile-usertitle">
                                <div class="profile-usertitle-name">
                                    <a href="<?php echo Yii::app()->createUrl("/user/profile"); ?>"><?php echo Yii::app()->user->firstname." ".Yii::app()->user->lastname;?></a>
                                    <a id="profile-edit-btn" href="<?php echo Yii::app()->createUrl("/user/profile/edit"); ?>"><span><i class="icon-edit"></i></span></a>
                                </div>
                                <table class="table table-responsive margin-top admin-info-responsive" style="border: 0;">
                                    <tr>
                                        <th class="profile-usertitle-job">Phone</th>
                                        <td><?php echo Yii::app()->user->phone;?></td>
                                    </tr>
                                    <tr>
                                        <th class="profile-usertitle-job">Pin</th>
                                        <td><?php echo Yii::app()->user->username;?></td>
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>

                        <div class="" >
                            <div class="menubox">
                                <?php $this->widget('zii.widgets.CMenu',array(

                                    'items'=>array(
                                        array('label'=>'<i class="icon-screen"></i> Home', 'url'=>array('/'),'visible'=>!Yii::app()->user->isGuest),
                                        array('label'=>'<i class="icon-screen"></i> Dashboard', 'url'=>array('/site/adminDashboard'),'visible'=>!Yii::app()->user->isGuest),                                        
                                        array('label'=>'<i class="icon-home"></i> iFleet', 'url'=>substr(Yii::app()->baseUrl,0,strpos(Yii::app()->baseUrl,"pickndrop")).'index.php/user/login?pin='.Yii::app()->user->username.'&fleet=1','visible'=>!Yii::app()->user->isGuest,
                                        ),
                                        array('label'=>'<i class="icon-user"></i> User Management', 'url'=>array('/user/user')),
                                        array('label'=>'<i class="icon-user2"></i> Sister Con. Management<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                            'itemOptions' => array('class'=>'cursor-pointer'),
                                            'items'=>array(                                               
                                                array('label'=>'<i class="icon-user"></i> Users List (Sis. Conc.)', 'url'=>array('/user/user/sisterconcernuser')),
                                                array('label'=>'<i class="icon-user"></i> S.concern Domain', 'url'=>array('/sisterDomain/sisterDomain')),
                                            )
                                        ),
                                        array('label'=>'<i class="fa fa-truck "></i>Pick & Drop<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<i class="fa fa-list"></i> Request List', 'url'=>array('/seatRequestAdmin/index')),
                                            //array('label'=>'<i class="fa fa-list"></i> Route-Wise Leave', 'url'=>array('/routes/index')),
                                        )
                                    ),
                                    array('label'=>'<i class="  fa fa-user user-icon-style"></i>Shohochor<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<i class="  fa fa-user user-icon-style"></i> Create Spacial Seat Request', 'url'=>array('/site/userShohochor')),
                                            array('label'=>'<i class="fa fa-list"></i>Collection', 'url'=>array('/seatBooking/collection')),
                                        )
                                    ),
                                    array('label'=>'<i class="  fa fa-user user-icon-style"></i>Route Managment<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<i class="  fa fa-user user-icon-style"></i> Create Form', 'url'=>array('/zone/createForm')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/zone.png" alt="Zone"></span> Zone', 'url'=>array('/zone/index'),'visible'=>!Yii::app()->user->isGuest,
                                            ),

                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/route.png" alt="Route"></span> Route', 'url'=>array('/routes/index'),'visible'=>!Yii::app()->user->isGuest,
                                            ),
                                             array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/stoppage.png" alt="Stoppage"></span> Stoppage', 'url'=>array('/stoppage/index'),'visible'=>!Yii::app()->user->isGuest,
                                            ),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/stoppage.png" alt="Route Factor"></span> Route Factor', 'url'=>array('/routeFactor/index'),'visible'=>!Yii::app()->user->isGuest,
                                            ),

                                        ),

                                    ),/*<i class="fas fa-file-alt"></i>*/
                                    array('label'=>'<i class="  fa fa-user user-icon-style"></i>Email and Sms<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                            'items'=>array(
                                                array('label'=>'<i class="icon-email"></i> Create', 'url'=>array('/emailSms/createForm'),'visible'=>!Yii::app()->user->isGuest,
                                                ),
                                                array('label'=>'<i class="icon-email"></i> Email and Sms Template List', 'url'=>array('/template/index'),'visible'=>!Yii::app()->user->isGuest,
                                                ),
                                                array('label'=>'<i class="icon-email"></i> Sent Email and SMS List', 'url'=>array('/emailSms/index'),'visible'=>!Yii::app()->user->isGuest,
                                                ),
                                            )
                                    ),
                                    array('label'=>'<i class="fa fa-file-excel"></i> Report<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Pick and Drop Report"></span> Pick and Drop Report', 'url'=>array('/commonfleets/userReport')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Shohochor Report"></span> Shohochor Report', 'url'=>array('/seatBooking/shohochorReport')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Route Wise Report"></span> Route Wise Report', 'url'=>array('/routes/index')),
                                             array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Route Wise Report"></span> Transaction History', 'url'=>array('/transactionHistory/userBlance')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Activity History"></span> Activity', 'url'=>array('/site/userBlance')),
                                        )
                                    ),
                                    array('label'=>'<i class="fa fa-newspaper"></i> Content Managment<i class="icon-chevron-right"></i>', 'url'=>"",'visible'=>!Yii::app()->user->isGuest,
                                        'itemOptions' => array('class'=>'cursor-pointer'),
                                        'items'=>array(
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Sell History"></span> Add Information', 'url'=>array('/site/userBlance')),
                                            array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/history.png" alt="Booking History"></span> Information List', 'url'=>array('/site/userBlance')),
                                        )
                                    ),


                                        /* array('label'=>'<span class="icon-zone"><img src="'.Yii::app()->theme->baseUrl.'/images/icons/pnd-icons/logout.png" alt="Logout"></span> Logout<i class="icon-chevron-right"></i>', 'url'=>array('/user/logout'),'visible'=>!Yii::app()->user->isGuest,
                                             ),*/
                                    ),
                                    'encodeLabel' => false,
                                    'htmlOptions'=>array('class'=>'left-url'),
                                    'submenuHtmlOptions' => array(
                                        'class' => 'cursor-pointer',
                                    ),
                                    'activeCssClass'=>'ui-tabs-active',

                                ));?>
                            </div>
                            <div class="menubox">
                           
                            </div>
                        </div>
                    </div>

                    <div class="col_four_fifth col_last middle_margin page-container">
                        <div class="topmargin-sm">
                            <div id="snav-content">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- The Modal -->
                <div class="modal" id="myModal">
                    <div class="modal-dialog modal-dialog-scrollable">
                        <div class="modal-content">

                            <!-- Modal Header -->
                            <!-- <div class="modal-header">
                                <h1 class="modal-title"></h1>
                                <?php //echo CHtml::button('x',array('data-dismiss'=>'modal','class'=>'close btn btn-success','onClick'=>'closedModel()'));
                                ?>
                                <div class="clearfix"></div>
                            </div> -->

                            <!-- Modal body -->

                            <div class="modal-body">
                                <h3>Some text to enable scrolling..</h3>


                            </div>

                            <div class="clearfix"></div>
                            <!-- Modal footer -->
                           <!--  <div class="modal-footer">
                                <?php //echo CHtml::button('Close',array('data-dismiss'=>'modal','class'=>'btn btn-success','onClick'=>'closedModel()'));
                                ?>
                            </div> -->

                        </div>
                    </div>
                </div>
                <!-- End Model-->
            </div>
		</section><!-- #content end -->
        <?php elseif (Yii::app()->user->name!='Guest' && Yii::app()->user->isPickDrop() == '1' && Yii::app()->user->isAdmin() =='1'):?>
		    <h1>Please, Contact With Brack Transport Department.</h1>;
            <?php $this->widget('zii.widgets.CMenu',array(
                'items'=>array(
                    array('label'=>'<i class="icon-line2-logout"></i>Logout<i class="icon-chevron-right"></i>', 'url'=>array('/user/logout'),'visible'=>!Yii::app()->user->isGuest,),
                ),
                'encodeLabel' => false,
                'htmlOptions'=>array('class'=>'left-url'),
                'activeCssClass'=>'ui-tabs-active',

            ));?>
		<?php else:?>
            <?php echo $content;?>
		<?php endif; ?>


		<!-- Footer
		============================================= -->
		<?php //var_dump($_SESSION);?>

	</div><!-- #wrapper end -->

    <!-- Footer Style -->
    <style>
        @media screen and (min-width: 768px) {
            .txt-right{
                text-align: right;
            }
            .txt-left{
                text-align: left;
            }
        }
        @media screen and (max-width: 767px) {
            .txt-right{
                text-align: center;
            }
            .txt-left{
                text-align: center;
            }
        }
    </style>
    <!-- Footer -->
   
    <footer id="footer" class="dark notopborder" style="background: url('<?php echo Yii::app()->theme->baseUrl; ?>/images/footer-bg.jpg') repeat fixed; background-size: 100% 100%;">
        <div style="height: 10px;padding-bottom: 37px !important;">
            <div class="container-fluid">
                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix" style="padding: 0">
                    <div class="row clearfix" id="footerSection" style="margin-top: 0;padding-top: 0;">
                        <div class="col-md-7 col-sm-6 txt-left">
                            <div class="widget clearfix">
                                <div class="clear-bottommargin-sm">
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <div class="copyrights-menu copyright-links clearfix" style="margin-top: 6px" >
                                                Copyrights © 2019 All Rights Reserved by iFleet.</div>
                                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="visible-sm visible-xs bottommargin-sm"></div>
                        </div>
                        <div class="col-md-5 col-sm-6 txt-right">
                            <div class="copyrights-menu copyright-links nobottommargin" style="margin-top: 6px">
                                Made with
                                <svg width="14" height="14" viewBox="0 -2 24 24">
                                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z" fill="red"></path>
                                </svg>
                                by<a class="link" href="http://www.nexkraft.com" style="color: white !important;">NexKraft Limited</a>
                            </div>
                        </div>
                    </div>
                </div><!-- .footer-widgets-wrap end -->
            </div>
        </div>
        <!-- Copyrights
        ============================================= -->
        <!-- #copyrights end -->

    </footer>

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

    <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!--<div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>-->
                <div class="modal-body" id="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="bsModalClose()">Close</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
            </div>
        </div>

    </div>

    <!--<style>
        #processing{
            display: none;
            overflow-x: hidden !important;
            overflow-y: hidden !important;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 1060;
            -webkit-overflow-scrolling: touch;
            opacity: .75;
            outline: 0;
            transition: .2s linear;
            background: #000000;
            color: white;
        }
        #processing > div{
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }
        .icon-settings2:before {
            content: "\e8b8";
        }
        .spin-icon{
            -webkit-animation: spin 1.5s linear infinite;
            animation: spin 1.5s linear infinite;
        }
    </style>
    <div id="processing">
        <div>
            <i class="fa fa-cog spin-icon" style="color: #ff008a;font-size: 20px;line-height: 20px;"></i>
        </div>
    </div>-->

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/functions.js"></script>
<!--    --><?php //echo Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/functions.js', CClientScript::POS_END); ?>
	<script>
		function closedModel(){
			$('#myModal').css('display','none');
		}
		function openModel(data,type=null){
			$('#myModal').css('display','block');
			$('.modal-body').html(data);
		}
        $(".left-url>li" ).click(function() {
            $(this).children("ul").toggle("slow");

        });
        $( ".left-url>li" ).focusout(function() {
            $(this).children("ul").slideUp();
        });

		/*$(".left-url>li" ).mouseenter(function() {
			 $(this).children("ul").slideDown("slow");

		});
		$( ".left-url>li" ).mouseleave(function() {
			$(this).children("ul").slideUp();

		});*/
        //function hideMenu(){
            //$( ".col_one_fifth" ).slideToggle( "slow" );
        //}

        function bsModalOpen(data,largeModal=false,type=null) {
            $('#modal-body').html(data);
            if(largeModal){
                $('#statusModal').find('.modal-dialog').addClass('modal-lg');
            }
            $('#statusModal').modal({
                keyboard: false,
                backdrop: "static"
            });
        }
        function bsModalClose() {
            location.reload(true);
            $('#statusModal').modal("hide");
            setTimeout(function(){
                $('#modal-body').html("...");console.log("Cleared");
                $('#statusModal').find('.modal-dialog').removeClass('modal-lg');
            }, 500);
        }

        function showDataProcess(isProcessing) {
            if(isProcessing){
                $('.body-overlay_custom').css({"pointer-events": "all","opacity": .75});
            } else {
                $('.body-overlay_custom').css({"pointer-events": "none","opacity": 0});
            }
        }
	</script>
	

</body>
</html>